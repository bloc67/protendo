<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
/* *************** */
/* the class definitions */
/* *************** */
class NewdefBoardindex extends ProtendoBoardIndex
{
	public function template_boardindex()
	{
		global $txt, $settings;

		echo '
		<div id="boardindex">';
	}


	function template_boardindex_below()
	{
		echo '
		</div>';
	}
}

?>