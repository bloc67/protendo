<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// Initialize the template... mainly little settings.
function pre_template()
{
	global $context, $settings, $options, $txt, $scripturl;

	$settings['theme_protendo_id'] = 'newdef';
	$settings['subtheme'] = 'Newdef';
}

/* *************** */
/* the class definitions */
/* *************** */
class NewdefIndex extends ProtendoIndex
{
	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '
		<div id="fixedtopheader">
			<div class="mainwidth">
				<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" style="height: 32px; float: left;" /></a></h1>';

		$this->theme_menu();
		echo '<br class="clear" />
			</div>
		</div>
		<div id="topheader">
			<div class="mainwidth">
				<div id="topframe">		
					<div class="bwgrid" id="myheader">
						<div class="bwcell11">
								<h1 id="logoheader"><a href="' , $scripturl , '" title=""><img src="' , !empty($settings['header_logo_url']) ? $settings['header_logo_url'] : $settings['images_url'] . '/logo.png', '" alt="" /></a></h1>
						</div>
						<div class="bwcell4 breadcrumb" id="rframe">
							<span id="sidebar_toggle_container">
								<span id="sidebar_toggle" class="triangle_' , !empty($options['usersidebar']) ? 'right' : 'left' , '"></span>
								<span style="white-space: nowrap; position: absolute; "></span>
							</span>';

		if(!empty($context['user']['avatar']['image']))
			echo '
						<span class="floatright mini_avatar" style="margin-top: 2px; background-image: url(' , $context['user']['avatar']['href'] , ');"></span>';
		if ($context['user']['is_logged'])
		{
			echo '	
							<a id="pms" href="' , $scripturl , '?action=pm">', $txt['pm_short'], '&nbsp;<span class="' , $context['user']['unread_messages']>0 ? 'notice' : 'noticegrey' , '">' . $context['user']['unread_messages'] . '</span>' , '</a>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
							<a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">' , $txt['approve_members_waiting'] , $context['unapproved_members']>0 ? '&nbsp;<span class="notice">'.$context['unapproved_members'].'</span>' : $context['unapproved_members'] , '</a>';

			echo '
							<a href="', $scripturl, '?action=unread">', $txt['unread'] , '&nbsp;<span class="', $context['unread_topics_user']>0 ? 'notice' : 'noticegrey' , '">'. $context['unread_topics_user'] . '</span></a>';
			
			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
							<a href="', $scripturl, '?action=moderate;area=reports">', $txt['mc_reported_posts'] , '&nbsp;<span class="notice">' , $context['open_mod_reports'], '</span></a>';
		}
		echo '	
						</div>
					</div>
					<div class="insertbg" >
						<div class="drophead">', $this->theme_menu(), '
						' , !empty($context['bigarea']) ?  $context['bigarea'] : '', '
						</div>
					</div>
					<div class="padding_both" id="userframe"><div class="' , !empty($options['usersidebar']) ? 'showme' : 'hideme' , '" id="sbar">
						<div>' , $this->theme_user_below() , '</div>
					</div>
				</div>';

		if(!empty($context['insecure']))
			echo '
				<div class="info2" style="margin: 5px 0;">'. $context['insecure'] . '</div>';

		// Is the forum in maintenance mode?
		if ($context['in_maintenance'] && $context['user']['is_admin'])
			echo '
				<div class="info2" style="margin: 5px 0;">'. $txt['maintain_mode_on'] . '</div>';
			
		echo '
				<div id="mainframe">
					<div id="main_content">';
		
		$this->theme_linktree();
	}

	function theme_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '	
					</div>
				</div>
			</div>
		</div>
		<div id="bottomfooter">
			<div class="mainwidth smalltext" style="padding-top: 2em; padding-bottom: 2em; ">
				', theme_copyright();

		// Show the load time?
		if ($context['show_load_time'])
			echo '
					| ', $txt['page_created'], $context['load_time'], $txt['seconds_with'], $context['load_queries'], $txt['queries'];

		echo '
			</div>
		</div>';
	}
	function theme_extra_settings($boardtype)
	{
		return;
	}
}

?>