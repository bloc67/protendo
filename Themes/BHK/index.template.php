<?php

// Initialize the template... mainly little settings.
function pre_template()
{
	global $context, $settings, $options, $txt, $scripturl;

	$settings['use_default_images'] = 'never';
	$settings['doctype'] = 'html';
	$settings['theme_version'] = '1.0';
	$settings['message_index_preview'] = true;

	$settings['subtheme'] = 'BHK';

	$settings['fixedheader'] = '
		<script type="text/javascript">
			/* fixed header */
			window.addEvent(\'domready\',function() {
				new SmoothScroll({duration:50});
				var ss = new ScrollSpy({
					min: 70,
					onEnter: function(position,enters) {
						$(\'topheader\').className = \'blue\';
					},
					onLeave: function(position,leaves) {
						$(\'topheader\').className = \'\';
					},
					container: window
				});
			});		
		</script>';	// any extra buttons we need from the theme, we set here
	/*
	$settings['theme_buttons'] = array(
		'boardindex' => array(
			'testme' => array('text' => 'progress', 'lang' => true, 'url' => $scripturl . '?action=profile;' ),
		),
	);
	*/
}

class BHKIndex extends ProtendoIndex
{
	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '<a id="top"></a>
	<div class="bwtable" id="mtable"><div class="bwrow">
		<div class="bwcol" id="sides" style="position: relative;">
			' , $this->theme_menu() , '
		</div>
		<div class="bwcol" id="mains"><div class="padding_both" >
		<div id="topheader">
			<div class="mainwidth">
				<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" /></a></h1>
				<hr style="clear: both;">';

		if(!empty($context['insecure']))
			echo '
				<div class="info2" style="margin: 5px 0;">'. $context['insecure'] . '</div>';

		// Is the forum in maintenance mode?
		if ($context['in_maintenance'] && $context['user']['is_admin'])
			echo '
				<div class="info2" style="margin: 5px 0;">'. $txt['maintain_mode_on'] . '</div>';
			
		echo '
				<div class="floatleft">' , $this->theme_linktree() , '</div>
				<div class="floatright" style="position:relative;">
					<div class="floatright quickbutton_single">
					<span id="sidebar_toggle_container">
						<span id="sidebar_toggle" class="triangle_' , !empty($options['usersidebar']) ? 'right' : 'left' , '"></span>
						<span style="white-space: nowrap; position: absolute; "></span>
					</span>';

		if ($context['user']['is_logged'])
		{
			echo '	
						<a id="pms" href="' , $scripturl , '?action=pm">', $txt['pm_short'], '<span class="' , $context['user']['unread_messages']>0 ? 'notice' : 'noticegrey' , '">' . $context['user']['unread_messages'] . '</span>' , '</a>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
						<a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">' , $txt['approve_members_waiting'] , $context['unapproved_members']>0 ? '<span class="notice">'.$context['unapproved_members'].'</span>' : $context['unapproved_members'] , '</a>';

			echo '
						<a href="', $scripturl, '?action=unread">', $txt['unread'] , '<span class="', $context['unread_topics_user']>0 ? 'notice' : 'noticegrey' , '">'. $context['unread_topics_user'] . '</span></a>';
			
			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
						<a href="', $scripturl, '?action=moderate;area=reports">', $txt['mc_reported_posts'] , '<span class="notice">' , $context['open_mod_reports'], '</span></a>';
		}
		echo '	</div>
				</div>
			</div>
		</div>
		<div id="main_content">
			<div class="mainwidth" style="padding: 0;"><div class="' , !empty($options['usersidebar']) ? 'showme' : 'hideme' , '" id="sbar">
				<div>' , $this->theme_user_below() , '</div>
			</div></div>
		';

		if(function_exists('template_inserted'))
			template_inserted();

		echo '
			<div class="mainwidth">';
	}

	function template_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '	
			</div>
		</div>
		<div id="bottomfooter">
			<div class="mainwidth smalltext" style="padding: 1em;">
				', theme_copyright();

		echo '
			</div>
		</div>
		</div></div>
		<div class="bwcol" id="rites">&nbsp; </div>
	</div></div>';
	}
}

?>