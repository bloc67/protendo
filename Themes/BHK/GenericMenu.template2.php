<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines
 * @copyright 2011 Simple Machines
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.0
 */

function template_generic_menu_dropdown()
{
	global $context, $settings, $options, $scripturl, $txt, $modSettings;


	// What one are we rendering?
	$context['cur_menu_id'] = isset($context['cur_menu_id']) ? $context['cur_menu_id'] + 1 : 1;
	$menu_context = &$context['menu_data_' . $context['cur_menu_id']];

	echo '
<div id="adm_menu">';

	foreach ($menu_context['sections'] as $section_id => $section)
	{
		foreach ($section['areas'] as $i => $area)
		{
			$url = $i;
			break;
		}
		
		if($section_id == $menu_context['current_section'])
		{	
			echo '
	<ul class="current">
		<li><a id="' , $section_id , '" href="' . $menu_context['base_url'] . ';area=' . $url . '">', $section['title'],'</a></li>
	</ul>
	<ul class="sub_active">';

			foreach ($section['areas'] as $i => $area)
			{
				// Not supposed to be printed?
				if (empty($area['label']))
					continue;

				echo '
		<li>';

				// Is this the current area, or just some area?
				if ($i == $menu_context['current_area'])
				{
					echo '
			<a class="chosen" href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

					if (empty($context['tabs']))
						$context['tabs'] = isset($area['subsections']) ? $area['subsections'] : array();
				}
				else
					echo '
			<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

				echo '
		</li>';
			}
			echo '
	</ul>';
		}
		else
		{
			echo '
	<ul class="select">
		<li><a id="' , $section_id , '" href="' . $menu_context['base_url'] . ';area=' . $url . '">', $section['title'],'</a>
			<ul class="sub">';

			foreach ($section['areas'] as $i => $area)
			{
				// Not supposed to be printed?
				if (empty($area['label']))
					continue;

				echo '
				<li>';

				// Is this the current area, or just some area?
				if ($i == $menu_context['current_area'])
				{
					echo '
					<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

					if (empty($context['tabs']))
						$context['tabs'] = isset($area['subsections']) ? $area['subsections'] : array();
				}
				else
					echo '
					<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

				echo '
				</li>';
			}
			echo '
			</ul>
		</li>
	</ul>';
		}
	}
	echo '
</div>
<script type="text/javascript">
	window.addEvent(\'domready\', function(){
		var mm = $(\'adm_menu\');
		if (window.scrollTop > 10) {
			mm.style.position = \'fixed\';
			mm.style.top = \'0\';
		} else {
			mm.style.position = \'relative\';
		}
	});
</script>
';

	// This is the main table - we need it so we can keep the content to the right of it.
	echo '
<div style="clear: both; overflow: hidden;" id="admarea">';

	// It's possible that some pages have their own tabs they wanna force...
	if (!empty($context['tabs']))
		echo '
	<div>' , template_generic_menu_tabs($menu_context) , '</div>';
}

function template_generic_menu_dropdown_below()
{
	global $context, $settings, $options;

	echo '
</div>';
}

function template_generic_menu_tabs(&$menu_context)
{
	global $context, $settings, $options, $scripturl, $txt, $modSettings;

	// Handy shortcut.
	$tab_context = &$menu_context['tab_data'];

	// Exactly how many tabs do we have?
	foreach ($context['tabs'] as $id => $tab)
	{
		// Can this not be accessed?
		if (!empty($tab['disabled']))
		{
			$tab_context['tabs'][$id]['disabled'] = true;
			continue;
		}

		// Did this not even exist - or do we not have a label?
		if (!isset($tab_context['tabs'][$id]))
			$tab_context['tabs'][$id] = array('label' => $tab['label']);
		elseif (!isset($tab_context['tabs'][$id]['label']))
			$tab_context['tabs'][$id]['label'] = $tab['label'];

		// Has a custom URL defined in the main admin structure?
		if (isset($tab['url']) && !isset($tab_context['tabs'][$id]['url']))
			$tab_context['tabs'][$id]['url'] = $tab['url'];
		// Any additional paramaters for the url?
		if (isset($tab['add_params']) && !isset($tab_context['tabs'][$id]['add_params']))
			$tab_context['tabs'][$id]['add_params'] = $tab['add_params'];
		// Has it been deemed selected?
		if (!empty($tab['is_selected']))
			$tab_context['tabs'][$id]['is_selected'] = true;
		// Does it have its own help?
		if (!empty($tab['help']))
			$tab_context['tabs'][$id]['help'] = $tab['help'];
		// Is this the last one?
		if (!empty($tab['is_last']) && !isset($tab_context['override_last']))
			$tab_context['tabs'][$id]['is_last'] = true;
	}

	// Find the selected tab
	foreach ($tab_context['tabs'] as $sa => $tab)
	{
		if (!empty($tab['is_selected']) || (isset($menu_context['current_subsection']) && $menu_context['current_subsection'] == $sa))
		{
			$selected_tab = $tab;
			$tab_context['tabs'][$sa]['is_selected'] = true;
		}
	}

	echo '
	<ul class="quickbuttons" style="overflow: hidden; padding-left: 5px;">';
	
	// Print out all the items in this tab.
	foreach ($tab_context['tabs'] as $sa => $tab)
	{
		if (!empty($tab['disabled']) || empty($tab['label']))
			continue;

		if (!empty($tab['is_selected']))
			echo '
		<li><a class="chosen inside" href="', isset($tab['url']) ? $tab['url'] : $menu_context['base_url'] . ';area=' . $menu_context['current_area'] . ';sa=' . $sa, $menu_context['extra_parameters'], isset($tab['add_params']) ? $tab['add_params'] : '', '"><span class="firstlevel">', $tab['label'], '</span></a></li>';
		else
			echo '
		<li><a class="inside" href="', isset($tab['url']) ? $tab['url'] : $menu_context['base_url'] . ';area=' . $menu_context['current_area'] . ';sa=' . $sa, $menu_context['extra_parameters'], isset($tab['add_params']) ? $tab['add_params'] : '', '"><span class="firstlevel">', $tab['label'], '</span></a></li>';
	}

	echo '
	</ul><hr>';
}

?>