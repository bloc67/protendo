<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
/* *************** */
/* the class definitions */
/* *************** */
class ProtendoBoardIndex
{
	function theme_main()
	{
		$this->theme_boardindex();
		$this->theme_boards();
		$this->theme_infocenter();
		$this->theme_boardindex_below();
	}

	function theme_boardindex()
	{
		global $txt, $settings;

		echo '
		<div id="boardindex">';
	}

	function theme_mycustom()
	{
		global $txt;

		echo '
		<div class="infobox2">Hey,this is a custom subtemplate in the grid.Theme Only!!</div>';
	}

	function theme_newsfader()
	{
		global $context, $settings, $scripturl, $txt, $options;

		if(!empty($options['boardindex_fader']))
			return;

		echo '
		<h2 class="mainheader">' , !$context['fader_options']['quicknews'] ? $txt['recent_news'] : $txt['news'] , '</h2>
		<div id="news_fader" style="height: ' , !empty($settings['newsfader_height']) && is_numeric($settings['newsfader_height']) ? $settings['newsfader_height'] : 350 , 'px; ">';

		if(empty($settings['newsfader_boardtypes']))
		{
			foreach ($context['news_lines'] as $news)
				echo '
			<div class="items" style="padding-bottom: 25px;height: ' , !empty($settings['newsfader_height']) && is_numeric($settings['newsfader_height']) ? $settings['newsfader_height'] : 350 , 'px;">', $news, '</div>';

		}
		else
		{
			foreach($context['frontblog'] as $orig)
				echo '
		<div class="bwgrid items" style="padding-bottom: 25px;">
			<div class="bwcell8" style="width: ' , !empty($settings['newsfader_widthleft']) && is_numeric($settings['newsfader_widthleft']) && $settings['newsfader_widthleft']<101 ? $settings['newsfader_widthleft'] : 50 , '%; ">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" class="imgstyle2" style="width: 100%; display: block; background: url(' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , ') no-repeat; background-size: cover; height: ' , !empty($settings['newsfader_height']) && is_numeric($settings['newsfader_height']) ? $settings['newsfader_height'] : 350 , 'px;">&nbsp;</a>
			</div>
			<div class="bwcell8" style="width: ' , !empty($settings['newsfader_widthleft']) && is_numeric($settings['newsfader_widthleft']) && $settings['newsfader_widthleft']<101 ? 100-$settings['newsfader_widthleft'] : 50 , '%;">
				<div style="padding: 0 0 0 1.5em;">
					<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a></h3>
					<div class="post">
						' , $orig['body'] , '... 
					</div>
				</div>
			</div>
		</div>
		';
		}
		echo '</div><hr style="clear: both;" />';
	}

	function show_boards()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		foreach($context['categories'] as $cat)
		{
			if(!$cat['is_collapsed'])
			{
				echo '
			<ul class="quickbuttons cats" style="float: right; ">';
				
				if(!isset($context['single_category']))
					echo '
				<li><a title="' . $txt['single_cat_title'] . ' &quot;' . $cat['name'] . '&quot;" class="Tip" href="' , $scripturl , '?action=forum;cat=' , $cat['id'] , '"><span>' , $txt['show_single_category'] , '</span></a></li>';
				else
					echo '
				<li><a class="Tip chosen" title="' . $txt['all_cat_title'] . '" class="Tip" href="' , $scripturl , '?action=forum"><span>' , $txt['show_all_categories'] , '</span></a></li>';

				if (!$context['user']['is_guest'] && !empty($cat['show_unread']))
					echo '
				<li><a title="' . $txt['unread_title'] . ' &quot;' . $cat['name'] . '&quot;" class="Tip" href="', $scripturl, '?action=unread;c=', $cat['id'], '"><span>', $txt['view_unread_category'], '</span></a></li>
				<li><a title="' . $txt['markread_title'] . ' &quot;' . $cat['name'] . '&quot;" class="Tip" href="', $scripturl, '?action=markasread;c=', $cat['id'], ';' , $context['session_var'] , '=' , $context['session_id'] , '"><span>', $txt['mark_read_short'], '</span></a></li>';

				echo '
			</ul>
			<h2 class="mainheader">' . $cat['link'] . '</h2>
			<div class="headertexts">
				<div class="bwgrid">
					<div class="bwcell1">&nbsp;</div>
					<div class="bwcell6" style="text-align: left;">
						' . $txt['board'] . '
					</div>
					<div class="bwcell3" style="text-align: center;">
						' . $txt['topics'] . ' | ' . $txt['posts'] . '
					</div>
					<div class="bwcell6" style="text-align: center;"> 
						', $txt['last_post'], '
					</div>
				</div>
			</div>';

				$alt=true;
			
				foreach($cat['boards'] as $bd)
				{
					// decide the helping texts, easier to figure out in advance.
					if($bd['new'] || $bd['children_new'])
						$alttext = $txt['new_posts'];
					elseif($bd['is_redirect'])
						$alttext = $txt['redirect_board'];
					else
						$alttext = $txt['old_posts'];

					echo '
			<div class="windowbg' , $alt ? '2' : '' , ' bxwindows">
				<div class="bwgrid">
					<div class="bwcell1" id="board' . $bd['id'] . '">
						<a href="' . $bd['href'] . '" title="' . $alttext . '" class="Tip ' , ($bd['new'] || $bd['children_new']) ? 'on' . ($bd['children_new'] ? '2' : '') : 'off' , $bd['is_redirect'] ? ' redirect' : '', '">&nbsp;</a>
					</div>
					<div class="bwcell7"><div style="padding: 0 1em;">
						' , !empty($options['boardindex_rss']) ? '<a href="' . $scripturl . '?action=.xml;type=rss2;board=' . $bd['id'] .'" class="social_icons rss_icon floatleft">&nbsp;</a>' : '' , '
						<h3>
							<a href="' . $bd['href'] . '"><b>' . $bd['name'] . '</b></a>
							' , !empty($bd['id_plugin']) ? '<span class="new2">' . $modSettings['plugins'][$bd['id_plugin']]['menu']['title'] . '</span>' : '' , '
						</h3>
						' , !empty($bd['description']) ? '<div class="middletext greytext">' . $bd['description'] . '</div>' : '' , '
					</div></div>
					<div class="bwcell3" style="margin-top: 5px; white-space: nowrap;">' . $bd['topics'] . ' | ' . $bd['posts'] . '</div>
					<div class="bwcell5">
						<div style="padding: 0 0 0 1em;">'; 
				
					if (!empty($bd['last_post']['id']))
						echo '
							<div class="bwgrid">
								<div class="bwcell3">
									<a href="' . $bd['last_post']['href'] .'"><span class="arrow" style="margin: 10px auto 0 auto;"></span></a>
								</div>
								<div class="bwcell13 middletext">
									<strong>'. $bd['last_post']['member']['link'] , ' ', $txt['in'], ' ', $bd['last_post']['link'], '</strong>
									<br />', $bd['last_post']['time'],'
								</div>
							</div>';
					else
						echo '
							<div class="bwgrid">
								<div class="bwcell3">
									<span class="arrow" style="margin: 10px auto 0 auto;"></span>
								</div>
								<div class="bwcell13 middletext" style="margin-top: 10px;">
									', $txt['not_applicable'] , '
								</div>
							</div>';

					echo '
						</div>
					</div>';

					// Show the "Child Boards: ". (there's a link_children but we're going to bold the new ones...)
					if (!empty($bd['children']))
					{
						// Sort the links into an array with new boards bold so it can be imploded.
						$children = array();
						/* Each child in each board's children has:
								id, name, description, new (is it new?), topics (#), posts (#), href, link, and last_post. */
						foreach ($bd['children'] as $child)
						{
							if (!$child['is_redirect'])
								$child['link'] = '<a href="' . $child['href'] . '" ' . ($child['new'] ? 'class="new_posts" ' : '') . 'title="' . ($child['new'] ? $txt['new_posts'] : $txt['old_posts']) . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')">' . $child['name'] . ($child['new'] ? '</a> <a href="' . $scripturl . '?action=unread;board=' . $child['id'] . '" title="' . $txt['new_posts'] . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')"><img src="' . $settings['lang_images_url'] . '/new.gif" class="new_posts" alt="" />' : '') . '</a>';
							else
								$child['link'] = '<a href="' . $child['href'] . '" title="' . comma_format($child['posts']) . ' ' . $txt['redirects'] . '">' . $child['name'] . '</a>';

							// Has it posts awaiting approval?
							if ($child['can_approve_posts'] && ($child['unapproved_posts'] || $child['unapproved_topics']))
								$child['link'] .= ' <a href="' . $scripturl . '?action=moderate;area=postmod;sa=' . ($child['unapproved_topics'] > 0 ? 'topics' : 'posts') . ';brd=' . $child['id'] . ';' . $context['session_var'] . '=' . $context['session_id'] . '" title="' . sprintf($txt['unapproved_posts'], $child['unapproved_topics'], $child['unapproved_posts']) . '" class="moderation_link">(!)</a>';

							$children[] = $child['new'] ? '<strong>' . $child['link'] . '</strong>' : $child['link'];
						}
						echo '
				</div>
				<div class="bwgrid">
					<div class="bwcell16 middletext">
						<hr />
						<h4 style="display: inline-block; padding-left: 6%;"><strong style="padding-left: 1em;">', $txt['parent_boards'], '</strong></h4>: ', implode(' | ', $children), '
					</div>';
					}

					echo ' 
				</div>
			</div>'; 
					
					$alt = !$alt;
				}
				echo '<br>';
			}
			else
				echo '
			<h2 class="mainheader header_collapsed">' . $cat['link'] . '</h2>';
			
		}
		if ($context['user']['is_logged'])
			echo '
			<div style="text-align: center;  ">
				<a class="quickbutton_single" href="', $scripturl, '?action=markasread;sa=all;' , $context['session_var'] , '=' , $context['session_id'] , '"><span>', $txt['mark_as_read'], '</span></a>
			</div>';
	}

	function theme_boards()
	{
		global $options;

		if(!empty($options['boardindex_tabs']))
			$this->show_board_tabs();
		else
			$this->show_boards();
	}

	function show_board_tabs()
	{	
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		echo '
		<ul id="board_tabs" class="horiz_nav">';
		$alt = true;
		foreach($context['categories'] as $cat)
		{
			if(!empty($options['boardindex_chosentab']))
				$chosen = $options['boardindex_chosentab'];
			else
				$chosen = $cat['id'];

			if(!$cat['is_collapsed'])
			{
				echo '
			<li tab="cat_div'. $cat['id'] .'" id="cat_tab' , $cat['id'] , '"' , $cat['id']==$chosen ? ' class="active"' : '' , '>' . $cat['name'] . '</li>';
			}
		}
		echo '
		</ul>
		<div id="cat_boards">
			<div class="headertexts">
				<div class="bwgrid">
					<div class="bwcell1">&nbsp;</div>
					<div class="bwcell6" style="text-align: left;">
						' . $txt['board'] . '
					</div>
					<div class="bwcell3" style="text-align: center;">
						' . $txt['topics'] . ' | ' . $txt['posts'] . '
					</div>
					<div class="bwcell6" style="text-align: center;"> 
						', $txt['last_post'], '
					</div>
				</div>
			</div>
		</div>
		<div id="cat_div_parent">';

		foreach($context['categories'] as $cat)
		{
			if(!$cat['is_collapsed'])
			{
				$alt=true;
				echo '
				<div class="cat_divs" id="cat_div' , $cat['id'] , '"' , $cat['id']!=$chosen ? ' style="display: none;"' : '' , '>';

				foreach($cat['boards'] as $bd)
				{
					// decide the helping texts, easier to figure out in advance.
					if($bd['new'] || $bd['children_new'])
						$alttext = $txt['new_posts'];
					elseif($bd['is_redirect'])
						$alttext = $txt['redirect_board'];
					else
						$alttext = $txt['old_posts'];

					echo '
			<div class="windowbg' , $alt ? '2' : '' , '">
				<div class="bwgrid">
					<div class="bwcell1">
						<a href="' . $bd['href'] . '" title="' . $alttext . '" class="Tip ' , ($bd['new'] || $bd['children_new']) ? 'on' . ($bd['children_new'] ? '2' : '') : 'off' , $bd['is_redirect'] ? ' redirect' : '', '">&nbsp;</a>
					</div>
					<div class="bwcell7"><div style="padding: 0 1em;">
						' , !empty($options['boardindex_rss']) ? '<a href="' . $scripturl . '?action=.xml;type=rss2;board=' . $bd['id'] .'" class="social_icons rss_icon floatleft">&nbsp;</a>' : '' , '
						<h3>
						<a href="' . $bd['href'] . '"><b>' . $bd['name'] . '</b></a></h3>
						' , !empty($bd['description']) ? '<div class="middletext greytext">' . $bd['description'] . '</div>' : '' , '
					</div></div>
					<div class="bwcell3" style="margin-top: 5px; white-space: nowrap;">' . $bd['topics'] . ' | ' . $bd['posts'] . '</div>
					<div class="bwcell5">
						<div style="padding: 0 0 0 1em;">'; 
				
					if (!empty($bd['last_post']['id']))
						echo '
							<div class="bwgrid">
								<div class="bwcell3">
									<a href="' . $bd['last_post']['href'] .'"><span class="arrow" style="margin: 10px auto 0 auto;"></span></a>
								</div>
								<div class="bwcell13 middletext">
									<strong>'. $bd['last_post']['member']['link'] , ' ', $txt['in'], ' ', $bd['last_post']['link'], '</strong>
									<br />', $bd['last_post']['time'],'
								</div>
							</div>';
					else
						echo '
							<div class="bwgrid">
								<div class="bwcell3">
									<span class="arrow" style="margin: 10px auto 0 auto;"></span>
								</div>
								<div class="bwcell13 middletext" style="margin-top: 10px;">
									', $txt['not_applicable'] , '
								</div>
							</div>';

					echo '
						</div>
					</div>';

					// Show the "Child Boards: ". (there's a link_children but we're going to bold the new ones...)
					if (!empty($bd['children']))
					{
						// Sort the links into an array with new boards bold so it can be imploded.
						$children = array();
						/* Each child in each board's children has:
								id, name, description, new (is it new?), topics (#), posts (#), href, link, and last_post. */
						foreach ($bd['children'] as $child)
						{
							if (!$child['is_redirect'])
								$child['link'] = '<a href="' . $child['href'] . '" ' . ($child['new'] ? 'class="new_posts" ' : '') . 'title="' . ($child['new'] ? $txt['new_posts'] : $txt['old_posts']) . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')">' . $child['name'] . ($child['new'] ? '</a> <a href="' . $scripturl . '?action=unread;board=' . $child['id'] . '" title="' . $txt['new_posts'] . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')"><img src="' . $settings['lang_images_url'] . '/new.gif" class="new_posts" alt="" />' : '') . '</a>';
							else
								$child['link'] = '<a href="' . $child['href'] . '" title="' . comma_format($child['posts']) . ' ' . $txt['redirects'] . '">' . $child['name'] . '</a>';

							// Has it posts awaiting approval?
							if ($child['can_approve_posts'] && ($child['unapproved_posts'] || $child['unapproved_topics']))
								$child['link'] .= ' <a href="' . $scripturl . '?action=moderate;area=postmod;sa=' . ($child['unapproved_topics'] > 0 ? 'topics' : 'posts') . ';brd=' . $child['id'] . ';' . $context['session_var'] . '=' . $context['session_id'] . '" title="' . sprintf($txt['unapproved_posts'], $child['unapproved_topics'], $child['unapproved_posts']) . '" class="moderation_link">(!)</a>';

							$children[] = $child['new'] ? '<strong>' . $child['link'] . '</strong>' : $child['link'];
						}
						echo '
				</div>
				<div class="bwgrid">
					<div class="bwcell16 middletext">
						<hr />
						<h4 style="display: inline-block; padding-left: 6%;"><strong style="padding-left: 1em;">', $txt['parent_boards'], '</strong></h4>: ', implode(' | ', $children), '
					</div>';
					}

					echo ' 
				</div>
			</div>'; 
					
					$alt = !$alt;
				}
				echo '
				</div>';
			}
		}
		echo '
		</div>
		<script type="text/javascript">
			var cat_hideall = function(){
				$$(\'#cat_div_parent .cat_divs\').setStyle(\'display\', \'none\');
			}
			var tab_clearall = function(){
				$$(\'#board_tabs li\').set(\'class\', \'passive\');
			}
			window.addEvent("domready", function() {
				$$(\'#board_tabs li\').each(function(element, index){
				  element.addEvent(\'click\',function(event){
					event.stop();
					tab = element.get(\'tab\');
					div = element.get(\'id\');
					cat_hideall();
					tab_clearall();
					element.set(\'class\', \'active\');
					$(tab).setStyle(\'display\', \'block\');
					smf_setThemeOption(\'boardindex_chosentab\', div.substring(7) , '.$settings['theme_id'].', \''.$context['session_id'].'\' , \''.$context['session_var'].'\', \'\');
				  });
				});
			});
		</script>';
		
		if ($context['user']['is_logged'])
			echo '
			<div style="text-align: center;  ">
				<a class="quickbutton_single" href="', $scripturl, '?action=markasread;sa=all;' , $context['session_var'] , '=' , $context['session_id'] , '"><span>', $txt['mark_as_read'], '</span></a>
			</div>';
	}

	function theme_infocenter()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		// Here's where the "Info Center" starts...
		echo '
		<div id="infocenter">
			<div class="floatright" style="margin: 6px 10px 0 1em;">' , show_toggle('ifcenter') , '</div>
			<h2 class="mainheader">', $txt['info_center_title'],  '</h2>
			<div id="ifcenter"' , !empty($options['reveal_ifcenter']) ? ' style="display: none;"' : '' , '>';

		// This is the "Recent Posts" bar.
		if (!empty($settings['number_recent_posts']) && (!empty($context['latest_posts']) || !empty($context['latest_post'])))
		{
			echo '
				<h3 class="headertexts"><a href="', $scripturl, '?action=recent">', $txt['recent_posts'], '</a></h3>
				<div class="hslice windowbg" id="recent_posts_content">
					<div class="entry-title" style="display: none;">', $context['forum_name_html_safe'], ' - ', $txt['recent_posts'], '</div>
					<div class="entry-content" style="display: none;">
						<a rel="feedurl" href="', $scripturl, '?action=.xml;type=webslice">', $txt['subscribe_webslice'], '</a>
					</div>';

			// Only show one post.
			if ($settings['number_recent_posts'] == 1)
			{
				// latest_post has link, href, time, subject, short_subject (shortened with...), and topic. (its id.)
				echo '
					<strong><a href="', $scripturl, '?action=recent">', $txt['recent_posts'], '</a></strong>
					<p id="infocenter_onepost" class="middletext">
						', $txt['recent_view'], ' &quot;', $context['latest_post']['link'], '&quot; ', $txt['recent_updated'], ' (', $context['latest_post']['time'], ')<br />
					</p>';
			}
			// Show lots of posts.
			elseif (!empty($context['latest_posts']))
			{
				echo '
					<ul class="horiz_nav">';

				/* Each post in latest_posts has:
						board (with an id, name, and link.), topic (the topic's id.), poster (with id, name, and link.),
						subject, short_subject (shortened with...), time, link, and href. */
				$alt = true;
				foreach ($context['latest_posts'] as $post)
				{
					echo '
						<li' , !empty($post['board']['id_plugin']) ? ' class="pluginboard"' : '' , ' style="display: block; padding-left: 15px;">
							' , $post['new'] ? '<span class="new_indicator"></span>' : '' , '
							<span class="floatright">', $post['time'], '</span> 
							<a href="', $post['href'], '" title="' , $txt['preview'], ' :: ' , $post['preview'], '" class="TipBig"><b>' , $post['subject'] , '</b></a> ' , $txt['in'] , ' ' , $post['board']['link'] , ' ' , $txt['by'], ' ', $post['poster']['link'], '  
						</li>';
					$alt = !$alt;
				}
				echo '
					</ul>';
			}
			echo '
				</div>';
		}

		// Show information about events, birthdays, and holidays on the calendar.
		if ($context['show_calendar'])
		{
			echo '
				<h3 class="headertexts"><a href="', $scripturl, '?action=calendar' . '">', $context['calendar_only_today'] ? $txt['calendar_today'] : $txt['calendar_upcoming'], '</a></h3>
				<div class="windowbg">';

			// Holidays like "Christmas", "Chanukah", and "We Love [Unknown] Day" :P.
			if (!empty($context['calendar_holidays']))
					echo '
					<span class="holiday">', $txt['calendar_prompt'], ' ', implode(', ', $context['calendar_holidays']), '</span><br />';

			// People's birthdays. Like mine. And yours, I guess. Kidding.
			if (!empty($context['calendar_birthdays']))
			{
					echo '
					<span class="birthday">', $context['calendar_only_today'] ? $txt['birthdays'] : $txt['birthdays_upcoming'], '</span> ';
			/* Each member in calendar_birthdays has:
					id, name (person), age (if they have one set?), is_last. (last in list?), and is_today (birthday is today?) */
			foreach ($context['calendar_birthdays'] as $member)
					echo '
					<a href="', $scripturl, '?action=profile;u=', $member['id'], '">', $member['is_today'] ? '<strong>' : '', $member['name'], $member['is_today'] ? '</strong>' : '', isset($member['age']) ? ' (' . $member['age'] . ')' : '', '</a>', $member['is_last'] ? '<br />' : ', ';
			}
			// Events like community get-togethers.
			if (!empty($context['calendar_events']))
			{
				echo '
					<span class="event">', $context['calendar_only_today'] ? $txt['events'] : $txt['events_upcoming'], '</span> ';
				/* Each event in calendar_events should have:
						title, href, is_last, can_edit (are they allowed?), modify_href, and is_today. */
				foreach ($context['calendar_events'] as $event)
					echo '
						', $event['can_edit'] ? '<a href="' . $event['modify_href'] . '" title="' . $txt['calendar_edit'] . '"><img src="' . $settings['images_url'] . '/icons/modify_small.gif" alt="*" /></a> ' : '', $event['href'] == '' ? '' : '<a href="' . $event['href'] . '">', $event['is_today'] ? '<strong>' . $event['title'] . '</strong>' : $event['title'], $event['href'] == '' ? '' : '</a>', $event['is_last'] ? '<br />' : ', ';
			}
			echo '
				</div>';
		}

		echo '
				<h3 class="headertexts">
					<a href="', $scripturl, '?action=stats">', $txt['forum_stats'], '</a>
				</h3>
				<div class="windowbg">
					<span class="largetext">', $context['common_stats']['total_posts'], '</span> 
						<span class="greytext">', $txt['posts_made'], ' &nbsp;&nbsp; </span><span class="largetext">', $context['common_stats']['total_topics'], '</span> <span class="greytext">', $txt['topics'], ' &nbsp;&nbsp; </span><span class="largetext">', $context['common_stats']['total_members'], '</span> <span class="greytext">', $txt['members'], '</span> 
					<div class="greytext">', !empty($settings['show_latest_member']) ? $txt['latest_member'] . ': <strong> ' . $context['common_stats']['latest_member']['link'] . '</strong>' : '', '
					', (!empty($context['latest_post']) ? ' | ' . $txt['latest_post'] . ': <strong>&quot;' . $context['latest_post']['link'] . '&quot;</strong>  ( ' . $context['latest_post']['time'] . ' )<br />' : ''), '
					<a href="', $scripturl, '?action=recent">', $txt['recent_view'], '</a>', $context['show_stats'] ? '
					<a href="' . $scripturl . '?action=stats">' . $txt['more_stats'] . '</a>' : '', '
					</div>
					<hr />
					<h4><b>
					', $context['show_who'] ? '<a href="' . $scripturl . '?action=who">' : '', comma_format($context['num_guests']), ' ', $context['num_guests'] == 1 ? $txt['guest'] : $txt['guests'], ', ' . comma_format($context['num_users_online']), ' ', $context['num_users_online'] == 1 ? $txt['user'] : $txt['users'];

		// Handle hidden users and buddies.
		$bracketList = array();
		if ($context['show_buddies'])
			$bracketList[] = comma_format($context['num_buddies']) . ' ' . ($context['num_buddies'] == 1 ? $txt['buddy'] : $txt['buddies']);
		if (!empty($context['num_spiders']))
			$bracketList[] = comma_format($context['num_spiders']) . ' ' . ($context['num_spiders'] == 1 ? $txt['spider'] : $txt['spiders']);
		if (!empty($context['num_users_hidden']))
			$bracketList[] = comma_format($context['num_users_hidden']) . ' ' . $txt['hidden'];

		if (!empty($bracketList))
			echo ' (' . implode(', ', $bracketList) . ')';

		echo $context['show_who'] ? '</a>' : '', '
					</b></h4>';

		// Assuming there ARE users online... each user in users_online has an id, username, name, group, href, and link.
		if (!empty($context['users_online']))
		{
			echo '
					<span class="greytext smalltext">', sprintf($txt['users_active'], $modSettings['lastActive']), ':</span>
					<div style="padding: 4px 10px 4px 0;">';
			// list the recent 20 with avatars..
			$co=0;
			foreach($context['users_online'] as $user)
			{
				if($co<20)
					echo '<a href="' , $user['href'] , '" title="' , $user['name'] , '" class="Tip"><img class="mini_avatar"  src="' , $user['avatar'] , '" alt="*" /></a>&nbsp;';
				
				$co++;
			}
			echo '
					</div>';
			
		}
		echo '
					<hr><div class="greytext">', $txt['most_online_today'], ': ', comma_format($modSettings['mostOnlineToday']), '
						&nbsp;&nbsp;&nbsp;', $txt['most_online_ever'], ': ', comma_format($modSettings['mostOnline']), ' (', timeformat($modSettings['mostDate']), ')				
					</div>
				</div>
			</div>
		</div>';
		
		mootools_createtoggle('ifcenter');
	}

	function theme_boardindex_below()
	{
		echo '
		</div>';
	}
}

?>