<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// Displays a sortable listing of all members registered on the forum.
function template_main()
{
	global $context, $settings, $options, $scripturl, $txt;

	// Build the memberlist button array.
	$memberlist_buttons = array(
			'view_all_members' => array('text' => 'view_all_members', 'image' => 'mlist.gif', 'lang' => true, 'url' => $scripturl . '?action=mlist' . ';sa=all', 'active'=> true),
			'mlist_search' => array('text' => 'mlist_search', 'image' => 'mlist.gif', 'lang' => true, 'url' => $scripturl . '?action=mlist' . ';sa=search'),
		);

	echo '
	<div class="main_section" id="memberlist">
		<span class="floatright">', template_button_strip($memberlist_buttons ), '</span>
		<h4 class="mainheader">', $txt['members_list'], '</h4>';
		
	if (!isset($context['old_search']))
			echo '
		<div class="info2">', $context['letter_links'], '</div>';
		
	echo '
		<div class="pagelinks">', $context['page_index'], '</div>
		<div id="mlist">
			<table class="table_grid" cellspacing="0" style="width:100%;">
			<thead>
				<tr>';

	// Display each of the column headers of the table.
	foreach ($context['columns'] as $column)
	{
		// We're not able (through the template) to sort the search results right now...
		if (isset($context['old_search']))
			echo '
					<th scope="col" class="headertexts" ', isset($column['width']) ? ' width="' . $column['width'] . '"' : '', '>
						', $column['label'], '</th>';
		// This is a selected column, so underline it or some such.
		elseif ($column['selected'])
			echo '
					<th scope="col" class="headertexts" style="width: auto;" nowrap="nowrap">
						<a href="' . $column['href'] . '" rel="nofollow">' . $column['label'] . ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" /></a></th>';
		// This is just some column... show the link and be done with it.
		else
			echo '
					<th scope="col" class="headertexts" ', isset($column['width']) ? ' width="' . $column['width'] . '"' : '>
						', $column['link'], '</th>';
	}
	echo '
				</tr>
			</thead>
			<tbody>';

	// Assuming there are members loop through each one displaying their data.
	if (!empty($context['members']))
	{
		$alt = true;
		foreach ($context['members'] as $member)
		{
			echo '
				<tr ', empty($member['sort_letter']) ? '' : ' id="letter' . $member['sort_letter'] . '"', ' class="windowbg' , $alt ? '2' : '' , '">
					<td>
						<span class="label_' , $member['online']['is_online'] ? 'online' : 'offline' , '">' , $member['online']['text'] , '</span>
					</td>
					<td>
					' , $member['link'] ,'
					</td>
					<td>', $member['show_email'] == 'no' ? '' : '<a href="' . $scripturl . '?action=emailuser;sa=email;uid=' . $member['id'] . '" rel="nofollow"><span class="social_icons email_icon"></span></a>', '</td>';

		// ICQ?
		if (!isset($context['disabled_fields']['facebook']))
			echo '
					<td>', $member['facebook']['link'], '</td>';
		else
			echo '<td></td>';

		// AIM?
		if (!isset($context['disabled_fields']['skype']))
			echo '
					<td>', $member['skype']['link'], '</td>';
		else
			echo '<td></td>';

		// YIM?
		if (!isset($context['disabled_fields']['twitter']))
			echo '
					<td>', $member['twitter']['link'], '</td>';
		else
			echo '<td></td>';

		// MSN?
		if (!isset($context['disabled_fields']['google']))
			echo '
					<td>', $member['google']['link'], '</td>';
		else
			echo '<td></td>';

		// Group and date.
		echo '
					<td>', empty($member['group']) ? $member['post_group'] : $member['group'], '</td>
					<td>', $member['registered_date'], '</td>';

		if (!isset($context['disabled_fields']['posts']))
		{
			echo '
					<td style="white-space: nowrap" width="15">', $member['posts'], '</td>';

		}
		else
			echo '<td></td>';

		echo '
				<td><img src="' , $member['avatar']['href'] , '" style="height: 35px;"></td>
			</tr>';
			$alt = !$alt;
		}
	}
	// No members?
	else
		echo '
				<tr>
					<td colspan="', $context['colspan'], '" class="windowbg">', $txt['search_no_results'], '</td>
				</tr>';

	// Show the page numbers again. (makes 'em easier to find!)
	echo '
			</tbody>
			</table>
		</div>';

	echo '
		<div class="pagelinks" style="overflow: hidden;">
			<div class="floatleft">', $context['page_index'], '</div>';

	// If it is displaying the result of a search show a "search again" link to edit their criteria.
	if (isset($context['old_search']))
		echo '
			<div class="floatright">
				<a href="', $scripturl, '?action=mlist;sa=search;search=', $context['old_search_value'], '">', $txt['mlist_search_again'], '</a>
			</div>';
	echo '
		</div>
	</div>';

}

// A page allowing people to search the member list.
function template_search()
{
	global $context, $settings, $options, $scripturl, $txt;

	// Build the memberlist button array.
	$memberlist_buttons = array(
			'view_all_members' => array('text' => 'view_all_members', 'image' => 'mlist.gif', 'lang' => true, 'url' => $scripturl . '?action=mlist' . ';sa=all'),
			'mlist_search' => array('text' => 'mlist_search', 'image' => 'mlist.gif', 'lang' => true, 'url' => $scripturl . '?action=mlist' . ';sa=search', 'active' => true),
		);

	// Start the submission form for the search!
	echo '
	<form action="', $scripturl, '?action=mlist;sa=search" method="post" accept-charset="', $context['character_set'], '">
		<span class="floatright">', template_button_strip($memberlist_buttons), '</span>
		<h3 class="mainheader">
			', $txt['mlist_search'], '
		</h3>
		<div id="memberlist" class="windowbg">';
	// Display the input boxes for the form.
	echo '	<div id="memberlist_search" class="clear">
				<div>
					<div id="mlist_search" class="flow_hidden" style="overflow: hidden;">
						<div id="search_term_input"><br />
							<strong>', $txt['search_for'], ':</strong>
							<input type="text" name="search" value="', $context['old_search'], '" size="35" class="input_text" /> 
						</div>
						<div class="content" style="margin: 1em 0; overflow: hidden;">
						<span class="floatleft">';

	$count = 0;
	foreach ($context['search_fields'] as $id => $title)
	{
		echo '
							<label for="fields-', $id, '"><input type="checkbox" name="fields[]" id="fields-', $id, '" value="', $id, '" ', in_array($id, $context['search_defaults']) ? 'checked="checked"' : '', ' class="input_check" />', $title, '</label><br />';
	// Half way through?
		if (round(count($context['search_fields']) / 2) == ++$count)
			echo '
						</span>
						<span class="floatleft">';
	}
		echo '
						</span>
						</div>
						<input type="submit" name="submit" value="' . $txt['search'] . '" class="button_submit" />
					</div>
				</div>
			</div>
		</div>
	</form>';
}

?>