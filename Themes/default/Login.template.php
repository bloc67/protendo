<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// This is just the basic "login" form.
function template_login()
{
	global $context, $settings, $options, $scripturl, $modSettings, $txt;

	echo '
		<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/sha1.js"></script>

		<form action="', $scripturl, '?action=login2" name="frmLogin" id="frmLogin" method="post" accept-charset="', $context['character_set'], '" ', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\');"' : '', '>
		<h3 class="mainheader">', $txt['login'], '</h3>
			<div class="content">';

	// Did they make a mistake last time?
	if (!empty($context['login_errors']))
		foreach ($context['login_errors'] as $error)
			echo '
				<p class="info">', $error, '</p>';

	// Or perhaps there's some special description for this time?
	if (isset($context['description']))
		echo '
				<p class="info">', $context['description'], '</p>';

	// Now just get the basic information - username, password, etc.
	echo '
				<dl class="settings">
					<dt>', $txt['username'], ':</dt>
					<dd><input type="text" name="user" value="', $context['default_username'], '"  /></dd>
					<dt>', $txt['password'], ':</dt>
					<dd><input type="password" name="passwrd" value="', $context['default_password'], '"  /></dd>
					<dt>', $txt['mins_logged_in'], ':</dt>
					<dd><input type="text" name="cookielength" size="4" maxlength="4" value="', $modSettings['cookieTime'], '"', $context['never_expire'] ? ' disabled="disabled"' : '', ' class="input_text" /></dd>
					<dt>', $txt['always_logged_in'], ':</dt>
					<dd><input type="checkbox" name="cookieneverexp"', $context['never_expire'] ? ' checked="checked"' : '', ' class="input_check" onclick="this.form.cookielength.disabled = this.checked;" /></dd>';
	// If they have deleted their account, give them a chance to change their mind.
	if (isset($context['login_show_undelete']))
		echo '
					<dt class="alert">', $txt['undelete_account'], ':</dt>
					<dd><input type="checkbox" name="undelete" class="input_check" /></dd>';
	echo '
				</dl>
				<hr style="clear: both;" >
				<p><input type="submit" value="', $txt['login'], '" class="button_submit" /></p>
				<p class="smalltext"><a href="', $scripturl, '?action=reminder">', $txt['forgot_your_password'], '</a></p>
				<input type="hidden" name="hash_passwrd" value="" />
			</div>
		</form>
		<script type="text/javascript"><!-- // --><![CDATA[
			document.forms.frmLogin.', isset($context['default_username']) && $context['default_username'] != '' ? 'passwrd' : 'user', '.focus();
		// ]]></script>';
}

// Tell a guest to get lost or login!
function template_kick_guest()
{
	global $context, $settings, $options, $scripturl, $modSettings, $txt;

	// This isn't that much... just like normal login but with a message at the top.
	echo '
	<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/sha1.js"></script>
	<form action="', $scripturl, '?action=login2" method="post" accept-charset="', $context['character_set'], '" name="frmLogin" id="frmLogin"', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\');"' : '', '>
		<section class="inputscreen">
			<h3 class="mainheader">', $txt['login'], '</h3>
			<div class="content">
				<dl class="settings">
					<dt>', $txt['username'], ':</dt>
					<dd><input type="text" name="user" /></dd>
					<dt>', $txt['password'], ':</dt>
					<dd><input type="password" name="passwrd"  /></dd>
					<dt>', $txt['mins_logged_in'], ':</dt>
					<dd><input type="text" name="cookielength" size="4" maxlength="4" value="', $modSettings['cookieTime'], '" class="input_text" /></dd>
					<dt>', $txt['always_logged_in'], ':</dt>
					<dd><input type="checkbox" name="cookieneverexp" class="input_check" onclick="this.form.cookielength.disabled = this.checked;" /></dd>
				</dl>
				<hr style="clear: both;" >
				<p><input type="submit" value="', $txt['login'], '" class="button_submit" /></p>
				<em class="smalltext"><a href="', $scripturl, '?action=reminder">', $txt['forgot_your_password'], '</a></em>
			</div>
			<input type="hidden" name="hash_passwrd" value="" />
		</section>
	</form>
		<script type="text/javascript"><!-- // --><![CDATA[
			document.forms.frmLogin.user.focus();
		// ]]></script>';
}

// This is for maintenance mode.
function template_maintenance()
{
	global $context, $settings, $options, $scripturl, $txt, $modSettings;

	// Display the administrator's message at the top.
	echo '
<form id="admin_login" action="', $scripturl, '?action=login2" method="post" accept-charset="', $context['character_set'], '"', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\');"' : '', '>
		<h4 class="mainheader">', $txt['admin_login'], '</h4>
		<div class="content">
			<dl class="settings">
				<dt>', $txt['username'], ':</dt>
				<dd><input type="text" name="user" /></dd>
				<dt>', $txt['password'], ':</dt>
				<dd><input type="password" name="passwrd" /></dd>
				<dt>', $txt['mins_logged_in'], ':</dt>
				<dd><input type="text" name="cookielength" size="4" maxlength="4" value="', $modSettings['cookieTime'], '" class="input_text" /></dd>
				<dt>', $txt['always_logged_in'], ':</dt>
				<dd><input type="checkbox" name="cookieneverexp" class="input_check" /></dd>
			</dl>
			<hr style="clear: both;" >
			<p class="centertext"><input type="submit" value="', $txt['login'], '" class="button_submit" /></p>
		</div>
		<input type="hidden" name="hash_passwrd" value="" />
</form>';
}

// This is for the security stuff - makes administrators login every so often.
function template_admin_login()
{
	global $context, $settings, $options, $scripturl, $txt;

	// Since this should redirect to whatever they were doing, send all the get data.
	echo '
<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/sha1.js"></script>
<form action="', $scripturl, $context['get_data'], '" method="post" accept-charset="', $context['character_set'], '" name="frmLogin" id="frmLogin" onsubmit="hashAdminPassword(this, \'', $context['user']['username'], '\', \'', $context['session_id'], '\');">
		<h3 class="mainheader"> ', $txt['login'], '</h3>
		<div class="content">';

	if (!empty($context['incorrect_password']))
		echo '
			<div class="infobox2">', $txt['admin_incorrect_password'], '</div>';

	echo '
			<dl class="settings">
				<dt><strong>', $txt['password'], ':</strong></dt>
				<dd><input type="password" name="admin_pass"  /></dd>
			</dl>
			<hr style="clear: both;" >
			<input type="submit" value="', $txt['login'], '" class="button_submit" />',	$context['post_data'], '
		</div>
	<input type="hidden" name="admin_hash_pass" value="" />
</form>
<script type="text/javascript"><!-- // --><![CDATA[
	document.forms.frmLogin.admin_pass.focus();
// ]]></script>';
}

// Activate your account manually?
function template_retry_activate()
{
	global $context, $settings, $options, $txt, $scripturl;

	// Just ask them for their code so they can try it again...
	echo '
		<form action="', $scripturl, '?action=activate;u=', $context['member_id'], '" method="post" accept-charset="', $context['character_set'], '">
			<h3 class="mainheader">', $context['page_title'], '</h3>
			<div class="content">';

	// You didn't even have an ID?
	if (empty($context['member_id']))
		echo '
				<dl class="settings">
					<dt>', $txt['invalid_activation_username'], ':</dt>
					<dd><input type="text" name="user"  /></dd>';

	echo '
					<dt>', $txt['invalid_activation_retry'], ':</dt>
					<dd><input type="text" name="code"  /></dd>
				</dl>
				<hr style="clear: both;" >
				<p><input type="submit" value="', $txt['invalid_activation_submit'], '" class="button_submit" /></p>
			</div>
		</form>';
}

// Activate your account manually?
function template_resend()
{
	global $context, $settings, $options, $txt, $scripturl;

	// Just ask them for their code so they can try it again...
	echo '
		<form action="', $scripturl, '?action=activate;sa=resend" method="post" accept-charset="', $context['character_set'], '">
			<h3 class="headerbg">', $context['page_title'], '</h3>
			<div class="content">
				<dl class="settings">
					<dt>', $txt['invalid_activation_username'], ':</dt>
					<dd><input type="text" name="user" value="', $context['default_username'], '"  /></dd>
				</dl>
				<p>', $txt['invalid_activation_new'], '</p>
				<dl class="settings">
					<dt>', $txt['invalid_activation_new_email'], ':</dt>
					<dd><input type="text" name="new_email"  /></dd>
					<dt>', $txt['invalid_activation_password'], ':</dt>
					<dd><input type="password" name="passwd"  /></dd>
				</dl>';

	if ($context['can_activate'])
		echo '
				<p>', $txt['invalid_activation_known'], '</p>
				<dl class="settings">
					<dt>', $txt['invalid_activation_retry'], ':</dt>
					<dd><input type="text" name="code" /></dd>
				</dl>';

	echo '
				<hr style="clear: both;" >
				<p><input type="submit" value="', $txt['invalid_activation_resend'], '" class="button_submit" /></p>
			</div>
		</form>';
}

?>