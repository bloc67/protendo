<?php
// Version: 2.0; Themes

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['themeadmin_explain'] = 'Designet gjør utseende på forumet forskjellig. Disse innstillingene påvirker valg av temaer, og hvilke temaer gjester og andre medlemmer bruk.';

$txt['theme_allow'] = 'Tillater medlemmer å velge sine egne temaer.';
$txt['theme_guests'] = 'Standard for Forumet';
$txt['theme_select'] = 'velg...';
$txt['theme_reset'] = 'Nullstill alle til';
$txt['theme_nochange'] = 'Ingen endring';
$txt['theme_forum_default'] = 'Forum standard';

$txt['theme_remove'] = 'slett';
$txt['theme_remove_confirm'] = 'Er du sikker på at du vil slette dette designet for godt?';

$txt['theme_install'] = 'Installér et nytt design';
$txt['theme_install_file'] = 'Fra en fil';
$txt['theme_install_dir'] = 'Fra en mappe på serveren';
$txt['theme_install_error'] = 'Den mappen eksisterer ikke, eller inneholder ikke et design!';
$txt['theme_install_write_error'] = 'Mappen Themes må være skrivbar før du fortsetter!';
$txt['theme_install_go'] = 'Installér!';
$txt['theme_install_new'] = 'Lag en kopi av standard med navnt';
$txt['theme_install_new_confirm'] = 'Er du sikker på at du vil installere dette nye temaet?';
$txt['theme_install_writable'] = 'Advarsel - du kan ikke opprette eller installere et nytt design ettersom mappen Themes ikke er skrivbar!';
$txt['theme_installed'] = 'Installering fullført!';
$txt['theme_installed_message'] = 'ble installert.';

$txt['theme_latest'] = 'Nyeste og mest populære temaer';
$txt['theme_latest_fetch'] = 'Henter nyeste design fra www.simplemachines.org...';

$txt['theme_pick'] = 'Velg et design...';
$txt['theme_preview'] = 'Forhåndsvis designet';
$txt['theme_set'] = 'Bruk dette designet';
$txt['theme_user'] = 'medlem bruker dette designet.';
$txt['theme_users'] = 'medlemmer bruker dette designet.';
$txt['theme_pick_variant'] = 'Velg variant';

$txt['theme_edit'] = 'Rediger design';
$txt['theme_edit_style'] = 'Rediger stilarket. (farger, skrifttyper, etc.)';
$txt['theme_edit_index'] = 'Rediger indeks malen. (hovedmal)';
$txt['theme_edit_no_save'] = 'Endringer i denne filen kan ikke lagres fordi den er ikke skrivbar!  Sørg for at filen er 777 eller har de riktige rettighetene';
$txt['theme_edit_save'] = 'Lagre endringer';

$txt['theme_global_description'] = 'Dette er forumets basisdesign, som betyr at ditt design vil endres etter administrator sine innstillinger og hvilken seksjon du viser.';

$txt['theme_url_config'] = 'Nettadresser til design og innstillinger';
$txt['theme_variants'] = 'Temavarianter';
$txt['theme_options'] = 'Innstillinger og alternativer for design';
$txt['actual_theme_name'] = 'Navn på designet: ';
$txt['actual_theme_dir'] = 'Mappe til designet: ';
$txt['actual_theme_url'] = 'Nettadresse til designet: ';
$txt['actual_images_url'] = 'Nettadresse til bildene for designet: ';
$txt['current_theme_style'] = 'Dette temas stil: ';

$txt['theme_variants_default'] = 'Standard temavariant';
$txt['theme_variants_user_disable'] = 'Deaktiver brukervalg av varianter';

$txt['site_slogan'] = 'Nettstedets slagord';
$txt['site_slogan_desc'] = 'Legg til egen tekst for et slagord her. Den vil erstatte Protendo logoen.';
$txt['forum_width'] = 'Forum bredde';
$txt['forum_width_desc'] = 'Sett forumets bredde. Eksempler: 950px, 80%, 1240px.';

$txt['enable_random_news'] = 'Aktiver tilfeldige nyheter i forumet topptekst';
$txt['show_group_key'] = 'Vis gruppenøkkelen på forumets hovedside';
$txt['additional_options_collapsable'] = 'Aktiver sammenslåbart ekstra valg ved innleggskriving';
$txt['allow_no_censored'] = 'Tillat medlemmer å slå av sensurering';
$txt['who_display_viewing'] = 'Vise hvem som leser emner og seksjonsforsider?';
$txt['who_display_viewing_off'] = 'Ikke vis';
$txt['who_display_viewing_numbers'] = 'Ja, men kun antall';
$txt['who_display_viewing_names'] = 'Ja, vis medlemsnavn';
$txt['disable_recent_posts'] = 'Deaktiver nyeste innlegg';
$txt['enable_single_post'] = 'Aktiver enkelt innlegg';
$txt['enable_multiple_posts'] = 'Aktiver flere innlegg';
$txt['enable_inline_links'] = 'Aktiver linker på én linje';
$txt['inline_desc'] = 'Ved å aktivere dette vil veien hvor du er i forumet være på én linje, istedet for å se ut som et tre.';
$txt['show_stats_index'] = 'Vis statistikk på forumets hovedside';
$txt['latest_members'] = 'Vis nyeste medlem på forumets hovedside';
$txt['last_modification'] = 'Vis redigeringsopplysninger på redigerte innlegg';
$txt['user_avatars'] = 'Vis brukernes avatarer i emnevisning';
$txt['user_text'] = 'Vis personlig tekst i emnevisning';
$txt['gender_images'] = 'Vis kjønn i emnevisning';
$txt['news_fader'] = 'Vis nyhetsfader på forumets hovedside';
$txt['member_list_bar'] = 'Vis medlemsliste på forumets hovedside';
$txt['current_pos_text_img'] = 'Vis gjeldende posisjon på forumet som lenk istedet for tekst';
$txt['show_view_profile_button'] = 'Vis knapp til profilen under innlegg';
$txt['enable_mark_as_read'] = 'Aktiver og vis knapper for \'Merk som lest\'';
$txt['header_logo_url'] = 'Nettadresse til logo';
$txt['header_logo_url_desc'] = '(la stå tomt for å vise forumnavn eller standard logo.)';
$txt['number_recent_posts'] = 'Antall innlegg som skal vises på forumets hovedside';
$txt['number_recent_posts_desc'] = 'For å deaktivere visning av nyeste innlegg, skriv 0 som verdi.';
$txt['hide_post_group'] = 'Skjul tittel på innleggsgruppe for gruppemedlemmer';
$txt['hide_post_group_desc'] = 'Ved å aktivere dette vil ikke medlemmets innleggsgruppe vises om de er satt til en gruppe som ikke tar hensyn til innlegg (som moderator, eller annet).';

$txt['theme_options_defaults'] = 'Dette er basisverdiene for noen medlemsspesifikke innstillinger.  Endring av disse vil kun påvirke nye medlemmer og gjester.';
$txt['theme_options_title'] = 'Endre eller tilbakestill basisinnstillinger';

$txt['themeadmin_title'] = 'Design og layoutinnstillinger';
$txt['themeadmin_description'] = 'Her kan du endre innstillingene for ditt design, oppdatere designvalg, nullstille medlemsalternativer og mye mer.';
$txt['themeadmin_admin_desc'] = 'Denne siden lar deg endre basisdesignet, nullstille medlemmer til å bruke et spesifikt design og endre andre innstillinger relatert til designvalg. Du kan også installere design fra denne siden.<br /><br />Ikke glem å se på designinnstillingene for layoutalternativer.';
$txt['themeadmin_list_desc'] = 'Fra her kan du se en liste over design som allerede er installert, endre mapper og innstillinger og avinstallere dem.';
$txt['themeadmin_reset_desc'] = 'Under kan du se et skjermbilde for å endre gjeldende designspesifikke innstillinger for alle medlemmene. Du vil kun se design som har sine egne innstillinger.';
$txt['themeadmin_edit_desc'] = 'Endre stilarket og kildekoden til dine installerte design. Se i dokumentasjonen for mer informasjon.';

$txt['themeadmin_list_heading'] = 'Oversikt over designinnstillinger';
$txt['themeadmin_list_tip'] = 'Husk på at layoutinnstillingene kan variere avhengig av design. Klikk på designetsnavn under for å endre basisalternativene, mappe eller adresseinnstillinger eller å finne andre alternativer.';
$txt['themeadmin_list_theme_dir'] = 'Designmappe (Maler)';
$txt['themeadmin_list_invalid'] = '(denne mappen er ikke korrekt!)';
$txt['themeadmin_list_theme_url'] = 'Nettdresse til ovenstående mappe';
$txt['themeadmin_list_images_url'] = 'Nettadresse til bildemappe';
$txt['themeadmin_list_reset'] = 'Nullstill designadresser og -mapper';
$txt['themeadmin_list_reset_dir'] = 'Sti til mappen Themes';
$txt['themeadmin_list_reset_url'] = 'Nettadresse til samme mappe';
$txt['themeadmin_list_reset_go'] = 'Forsøk å tilbakestille alle temaer';

$txt['themeadmin_reset_tip'] = 'Hver design har sine egne alternative som dine medlemmer kan velge. Disse alternativene inkluderer blant annet Hurtigsvar, avatarer og signaturer, layoutalternativer og andre innstillinger.  Her kan du endre basisinnstillingene eller nullstille alle.<br /><br />Merk at noen design kan bruke basisinnstillingene, som betyr at de ikke bruker egne alternativer.';
$txt['themeadmin_reset_defaults'] = 'Konfigurer alternativene for gjest og ny bruker for dette temaet';
$txt['themeadmin_reset_defaults_current'] = 'gjeldende innstillinger.';
$txt['themeadmin_reset_members'] = 'Nullstill alle medlemmenes gjeldende innstillinger for dette design';
$txt['themeadmin_reset_remove'] = 'Slett alle medlemmenes innstillinger og bruk basisinnstillingene';
$txt['themeadmin_reset_remove_current'] = 'medlemmer som bruker sine egne innstillinger.';
// Don't use entities in the below string.
$txt['themeadmin_reset_remove_confirm'] = 'Er du sikker på at du vil slette alle designinnstillingene?\\nDette vil også nullstille noen egendefinerte profilfelter.';
$txt['themeadmin_reset_options_info'] = 'Alternativene under vil føre til at <em>alle</em> får nullstilte alternativer.  For å endre et alternativ, velg &quot;endre&quot; i boksen ved siden av, og deretter velg en verdi for den.  For å bruke basisinnstillinga, velg  &quot;slett&quot;.  Ellers, bruk &quot;uendret&quot; for å la stå uendret.';
$txt['themeadmin_reset_options_change'] = 'Endre';
$txt['themeadmin_reset_options_none'] = 'Uendret';
$txt['themeadmin_reset_options_remove'] = 'Slett';

$txt['themeadmin_edit_browse'] = 'Bla igjennom filene til designet.';
$txt['themeadmin_edit_style'] = 'Endre stilarkett for designet.';
$txt['themeadmin_edit_copy_template'] = 'Kopier oppsett fra designet dette er basert på.';
$txt['themeadmin_edit_exists'] = 'eksisterer allerede';
$txt['themeadmin_edit_do_copy'] = 'kopier';
$txt['themeadmin_edit_copy_warning'] = 'Når Protendo trenger et oppsett eller en språkfil som ikke er i gjeldende design, vil det søke etter designet det er basert på, eller etter basisdesignet.<br />Om du ikke vil endre et oppsett, er det best at du ikke kopierer det.';
$txt['themeadmin_edit_copy_confirm'] = 'Er du sikker på at du vil kopiere dette designet?';
$txt['themeadmin_edit_overwrite_confirm'] = 'Er du sikker på at du vil erstatte dette oppsettet med det som allerede eksisterer?\\nDette vil OVERSKRIVE alle endringer du har utføt!';
$txt['themeadmin_edit_no_copy'] = '<em>(kan ikke kopiere)</em>';
$txt['themeadmin_edit_filename'] = 'Filnavn';
$txt['themeadmin_edit_modified'] = 'Sist endret';
$txt['themeadmin_edit_size'] = 'Størrelse';
$txt['themeadmin_edit_bytes'] = 'B';
$txt['themeadmin_edit_kilobytes'] = 'KB';
$txt['themeadmin_edit_error'] = 'Filen du prøvde å lagre, opprettet følgende feilmelding:';
$txt['themeadmin_edit_on_line'] = 'På begynnelsen av linje';
$txt['themeadmin_edit_preview'] = 'Forhåndsvis';
$txt['themeadmin_selectable'] = 'Temaer som brukerne kan velge';
$txt['themeadmin_themelist_link'] = 'Vis listen over temaer';

?>