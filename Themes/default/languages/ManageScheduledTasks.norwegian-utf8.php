<?php
// Version: 2.0; ManageScheduledTasks

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['scheduled_tasks_title'] = 'Planlagte oppgavr';
$txt['scheduled_tasks_header'] = 'Alle planlagte oppgaver';
$txt['scheduled_tasks_name'] = 'Oppgave';
$txt['scheduled_tasks_next_time'] = 'Neste forfall';
$txt['scheduled_tasks_regularity'] = 'Intervall';
$txt['scheduled_tasks_enabled'] = 'Aktivert';
$txt['scheduled_tasks_run_now'] = 'Utfør nå';
$txt['scheduled_tasks_save_changes'] = 'Lagre endringer';
$txt['scheduled_tasks_time_offset'] = '<strong>Merk:</strong> Alle tidsangivelser nedenfor er <em>server tid</em> og tar ikke hensyn til eventuelle tidsforskyvningsinnstillinger i Protendo.';
$txt['scheduled_tasks_were_run'] = 'Alle valgte oppgaver ble fullført';

$txt['scheduled_tasks_na'] = 'N/A';
$txt['scheduled_task_approval_notification'] = 'Varslingsliste godkjenninger';
$txt['scheduled_task_desc_approval_notification'] = 'Send ut e-poster til alle moderatorer med en liste over innlegg som venter på godkjenning.';
$txt['scheduled_task_auto_optimize'] = 'Optimaliser databasen';
$txt['scheduled_task_desc_auto_optimize'] = 'Optimaliser databasen for å løse fragmenteringsproblemer.';
$txt['scheduled_task_daily_maintenance'] = 'Daglig vedlikehold';
$txt['scheduled_task_desc_daily_maintenance'] = 'Kjører viktig daglig vedlikehold på forumet - bør ikke deaktiveres.';
$txt['scheduled_task_daily_digest'] = 'Daglig oppsummering av meldinger';
$txt['scheduled_task_desc_daily_digest'] = 'Send e-post med daglig sammendrag av meldinger til abonnementene.';
$txt['scheduled_task_weekly_digest'] = 'Ukentlig oppsummering av meldinger';
$txt['scheduled_task_desc_weekly_digest'] = 'Send e-post med ukentlig sammendrag av meldinger til abonnementene.';
$txt['scheduled_task_fetchSMfiles'] = 'Hent Simple Machines filer';
$txt['scheduled_task_desc_fetchSMfiles'] = 'Henter javascript filer som inneholder meldinger om oppdateringer og annen informasjon.';
$txt['scheduled_task_birthdayemails'] = 'Sender e-post på bursdager';
$txt['scheduled_task_desc_birthdayemails'] = 'Sender ut e-poster og gratulerer medlemmene med bursdagen.';
$txt['scheduled_task_weekly_maintenance'] = 'Ukentlig vedlikehold';
$txt['scheduled_task_desc_weekly_maintenance'] = 'Kjører viktig ukentlig vedlikehold på forumet - bør ikke deaktiveres.';
$txt['scheduled_task_paid_subscriptions'] = 'Sjekker betalte abonnementer';
$txt['scheduled_task_desc_paid_subscriptions'] = 'Sender ut nødvendige påminnelser om forfalte abonnement og fjerner utgåtte abonnementer.';

$txt['scheduled_task_reg_starting'] = 'Starter %1$s';
$txt['scheduled_task_reg_repeating'] = 'gjenta hver %1$d %2$s';
$txt['scheduled_task_reg_unit_m'] = 'minutt(er)';
$txt['scheduled_task_reg_unit_h'] = 'time(er)';
$txt['scheduled_task_reg_unit_d'] = 'dag(er)';
$txt['scheduled_task_reg_unit_w'] = 'uke(r)';

$txt['scheduled_task_edit'] = 'Rediger planlagt oppgaver';
$txt['scheduled_task_edit_repeat'] = 'Gjenta oppgave hvert';
$txt['scheduled_task_edit_pick_unit'] = 'Velg enhet';
$txt['scheduled_task_edit_interval'] = 'Intervall';
$txt['scheduled_task_edit_start_time'] = 'Starttid';
$txt['scheduled_task_edit_start_time_desc'] = 'Tid dagens første tilfelle skal begynne (time:minutt)';
$txt['scheduled_task_time_offset'] = 'Merk: Starttidspunktet bør være utlignet mot den aktuelle servertid. Nåværende servertid er: %1$s';

$txt['scheduled_view_log'] = 'Vis logg';
$txt['scheduled_log_empty'] = 'Det er foreløpig ingen oppgave i loggen.';
$txt['scheduled_log_time_run'] = 'Tid utført';
$txt['scheduled_log_time_taken'] = 'Tid';
$txt['scheduled_log_time_taken_seconds'] = '%1$d sekunder';
$txt['scheduled_log_empty_log'] = 'Tøm logg';

?>