<?php
// Version: 2.0; PersonalMessage

global $context;

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['pm_inbox'] = 'Postkontor for PMer';
$txt['send_message'] = 'Send melding';
$txt['pm_add'] = 'Legg til';
$txt['make_bcc'] = 'Legg til blindkopi';
$txt['pm_to'] = 'Til';
$txt['pm_bcc'] = 'Blindkopi';
$txt['inbox'] = 'Innboks';
$txt['conversation'] = 'Samtale';
$txt['messages'] = 'Meldinger';
$txt['sent_items'] = 'Sendte meldinger';
$txt['new_message'] = 'Ny melding';
$txt['delete_message'] = 'Slett meldinger';
// Don't translate "PMBOX" in this string.
$txt['delete_all'] = 'Slette alle personlige meldinger i din PMBOX';
$txt['delete_all_confirm'] = 'Er du sikker på at du vil slette alle meldinger?';
$txt['recipient'] = 'Mottaker';
// Don't translate the word "SUBJECT" here, as it is used to format the message - use numeric entities as well.
$txt['new_pm_subject'] = 'Ny personlig melding: SUBJECT';
// Don't translate SENDER or MESSAGE in this language string; they are replaced with the corresponding text - use numeric entities too.
$txt['pm_email'] = 'Du har nettopp mottatt en personlig melding fra SENDER på ' . $context['forum_name'] . '.' . "\n\n" . ': Husk at dette kun er en varsling. Ikke svar på denne mailen.' . "\n\n" . 'Meldingen som er sendt til deg er:' . "\n\n" . 'MESSAGE';
$txt['pm_multiple'] = '(flere mottakere: \'navn1, navn2\')';
// Use numeric entities in the below string.
$txt['instant_reply'] = 'Svar på denne personlige meldingen her:';

$txt['delete_selected_confirm'] = 'Er du sikker på at du vil slette de valgte personlige meldingene?';

$txt['sent_to'] = 'Sendt til';
$txt['reply_to_all'] = 'Svar til alle';
$txt['delete_conversation'] = 'Slett samtale';

$txt['pm_capacity'] = 'Kapasitet';
$txt['pm_currently_using'] = '%1$s meldinger, %2$s%% full.';
$txt['pm_sent'] = 'Din melding ble sendt uten feil.';

$txt['pm_error_user_not_found'] = 'Kunne ikke finne medlemmet \'%1$s\'.';
$txt['pm_error_ignored_by_user'] = 'Bruker \'%1$s\' har blokkert din personlige melding.';
$txt['pm_error_data_limit_reached'] = 'Meldingen kunne ikke sendes til \'%1$s\' fordi medlemmet ikke har plass til flere meldinger';
$txt['pm_error_user_cannot_read'] = 'Bruker \'%1$s\' kan ikke motta personlige meldinger.';
$txt['pm_successfully_sent'] = 'Meldingen er sendt til \'%1$s\'.';
$txt['pm_send_report'] = 'Send rapport';
$txt['pm_save_outbox'] = 'Lagre en kopi i min utboks';
$txt['pm_undisclosed_recipients'] = 'Skjult mottakere';
$txt['pm_too_many_recipients'] = 'Du kan ikke sende PM til flere enn %1$d mottaker(e) på én gang.';

$txt['pm_read'] = 'Les';
$txt['pm_replied'] = 'Besvart';

// Message Pruning.
$txt['pm_prune'] = 'Rydd i meldinger';
$txt['pm_prune_desc1'] = 'Slett personlige meldinger eldre enn';
$txt['pm_prune_desc2'] = 'dager.';
$txt['pm_prune_warning'] = 'Er du sikker på at du vil slette personlige meldinger?';

// Actions Drop Down.
$txt['pm_actions_title'] = 'Flere handlinger';
$txt['pm_actions_delete_selected'] = 'Slett valgte';
$txt['pm_actions_filter_by_label'] = 'Filtrer på overskrift';
$txt['pm_actions_go'] = 'Ok';

// Manage Labels Screen.
$txt['pm_apply'] = 'Bruk';
$txt['pm_manage_labels'] = 'Behandle merkelapp';
$txt['pm_labels_delete'] = 'Er du sikker på at du vil slette de valgte merkelappene?';
$txt['pm_labels_desc'] = 'Her kan du legge til, endre og slette merkelapper som brukes for dine personlige meldinger.';
$txt['pm_label_add_new'] = 'Lag ny merkelapp';
$txt['pm_label_name'] = 'Navn på merkelapp';
$txt['pm_labels_no_exist'] = 'Du har ikke laget noen merkelapper ennå!';

// Labeling Drop Down.
$txt['pm_current_label'] = 'Merkelapp';
$txt['pm_msg_label_title'] = 'Melding på merkelapp';
$txt['pm_msg_label_apply'] = 'Legg til merkelapp';
$txt['pm_msg_label_remove'] = 'Fjern merkelapp';
$txt['pm_msg_label_inbox'] = 'Innboks';
$txt['pm_sel_label_title'] = 'Valgt merkelapp';

// Sidebar Headings.
$txt['pm_labels'] = 'Merkelapper';
$txt['pm_messages'] = 'Meldinger';
$txt['pm_actions'] = 'Handlinger';
$txt['pm_preferences'] = 'Innstillinger';

$txt['pm_is_replied_to'] = 'Du har videresendt eller besvart denne meldingen.';

// Reporting messages.
$txt['pm_report_to_admin'] = 'Rapporter til admin';
$txt['pm_report_title'] = 'Rapporter personlig melding';
$txt['pm_report_desc'] = 'Fra denne siden kan du rapportere den personlige meldingen du mottok til administrasjonen på forumet. Vennligst skriv en begrunnelse for hvorfor du rapporterer meldingen. Begrunnelsen vil bli sendt sammen med innholdet i selve meldingen.';
$txt['pm_report_admins'] = 'Administrator rapporten skal sendes til';
$txt['pm_report_all_admins'] = 'Send til alle administratorer på forumet';
$txt['pm_report_reason'] = 'Begrunnelse for hvorfor du rapporterer denne meldingen';
$txt['pm_report_message'] = 'Rapporter melding';

// Important - The following strings should use numeric entities.
$txt['pm_report_pm_subject'] = '[RAPPORT] ';
// In the below string, do not translate "{REPORTER}" or "{SENDER}".
$txt['pm_report_pm_user_sent'] = '{REPORTER} har rapportert  den personlige meldingen nedenfor, sendt av {SENDER}, på grunn av:';
$txt['pm_report_pm_other_recipients'] = 'Andre mottakere av meldingen er:';
$txt['pm_report_pm_hidden'] = '%1$d skjulte mottakere';
$txt['pm_report_pm_unedited_below'] = 'Nedenfor gjengis orginal-innholdet i den personlige meldingen som er blitt rapportert:';
$txt['pm_report_pm_sent'] = 'Sendt:';

$txt['pm_report_done'] = 'Takk for at du sendte inn denne rapporten. Du vil snart motta tilbakemelding fra administrasjonen.';
$txt['pm_report_return'] = 'Tilbake til innboksen';

$txt['pm_search_title'] = 'Søk i personlige meldinger';
$txt['pm_search_bar_title'] = 'Søk i PMer';
$txt['pm_search_text'] = 'Søk etter';
$txt['pm_search_go'] = 'Søk';
$txt['pm_search_advanced'] = 'Avansert søk';
$txt['pm_search_user'] = 'Etter medlem';
$txt['pm_search_match_all'] = 'Treff på alle ord';
$txt['pm_search_match_any'] = 'Treff på minst ett ord';
$txt['pm_search_options'] = 'Alternativer';
$txt['pm_search_post_age'] = 'Medlingens alder';
$txt['pm_search_show_complete'] = 'Vis hele meldingen i søkeresultatet.';
$txt['pm_search_subject_only'] = 'Søk kun i tittel og forfatter.';
$txt['pm_search_between'] = 'mellom';
$txt['pm_search_between_and'] = 'og';
$txt['pm_search_between_days'] = 'dager';
$txt['pm_search_order'] = 'Sorter søkeresultat etter';
$txt['pm_search_choose_label'] = 'Velg merkelapper å søke etter, eller søk etter alle';

$txt['pm_search_results'] = 'Søkeresulater';
$txt['pm_search_none_found'] = 'Ingen meldinger funnet';

$txt['pm_search_orderby_relevant_first'] = 'Mest relevante først';
$txt['pm_search_orderby_recent_first'] = 'Nyeste først';
$txt['pm_search_orderby_old_first'] = 'Eldste først';

$txt['pm_visual_verification_label'] = 'Bekreftelse';
$txt['pm_visual_verification_desc'] = 'Vennligst skriv inn koden i bildet ovenfor, for å sende denne personlige meldingen.';

$txt['pm_settings'] = 'Endre innstillinger';
$txt['pm_change_view'] = 'Vis endring';

$txt['pm_manage_rules'] = 'Behandle regler';
$txt['pm_manage_rules_desc'] = 'Meldingregler tillater deg å automatisk sortere innkommende meldinger avhengig av et sett med kriterier du definerer. Nedenfor er alle reglene du har satt opp. Hvis du vil redigere en regel bare å klikke på regelen.';
$txt['pm_rules_none'] = 'Du har ennå ikke satt opp en meldingsregler.';
$txt['pm_rule_title'] = 'Regel';
$txt['pm_add_rule'] = 'Legg til ny regel';
$txt['pm_apply_rules'] = 'Legg til regler nå';
// Use entities in the below string.
$txt['pm_js_apply_rules_confirm'] = 'Er du sikker på at du ønsker å bruke gjeldende regler på alle personlige meldinger?';
$txt['pm_edit_rule'] = 'Endre regel';
$txt['pm_rule_save'] = 'Lagre regel';
$txt['pm_delete_selected_rule'] = 'Slett valgte regler';
// Use entities in the below string.
$txt['pm_js_delete_rule_confirm'] = 'Er du sikker på at du ønsker å slette valgte regler?';
$txt['pm_rule_name'] = 'Navn';
$txt['pm_rule_name_desc'] = 'Navn for å huske denne regelen';
$txt['pm_rule_name_default'] = '[NAVN]';
$txt['pm_rule_description'] = 'Beskrivelse';
$txt['pm_rule_not_defined'] = 'Legg til noen kriterier for å begynne å sette opp denne regelens beskrivelse.';
$txt['pm_rule_js_disabled'] = '<span class="alert"><strong>Merk:</strong>Du synes å ha javascript deaktivert. Vi anbefaler deg å aktivere JavaScript for å bruke denne funksjonen.</span>';
$txt['pm_rule_criteria'] = 'Kriterie';
$txt['pm_rule_criteria_add'] = 'Legg til kriterie';
$txt['pm_rule_criteria_pick'] = 'Velg kriterie';
$txt['pm_rule_mid'] = 'Avsenders navn';
$txt['pm_rule_gid'] = 'Avsenders gruppe';
$txt['pm_rule_sub'] = 'Meldingens tittel inneholder';
$txt['pm_rule_msg'] = 'Meldingens tekst inneholder';
$txt['pm_rule_bud'] = 'Avsenderen er en venn';
$txt['pm_rule_sel_group'] = 'Velg gruppe';
$txt['pm_rule_logic'] = 'Når kriterier velges';
$txt['pm_rule_logic_and'] = 'Alle kriterier må være innfridd';
$txt['pm_rule_logic_or'] = 'Eventuelle krav kan oppfylles';
$txt['pm_rule_actions'] = 'Handling';
$txt['pm_rule_sel_action'] = 'Velg en handling';
$txt['pm_rule_add_action'] = 'Legg til handling';
$txt['pm_rule_label'] = 'Merk meldingen med';
$txt['pm_rule_sel_label'] = 'Velg merkelapp';
$txt['pm_rule_delete'] = 'Slett melding';
$txt['pm_rule_no_name'] = 'Du glemte å gi et navn på regelen.';
$txt['pm_rule_no_criteria'] = 'En regel må ha minst ett kriterie og et handlings sett.';
$txt['pm_rule_too_complex'] = 'Den regelen du lager er for lang for Protendo å lagre. Prøv å bryte den opp i mindre regler.';

$txt['pm_readable_and'] = '<em>og</em>';
$txt['pm_readable_or'] = '<em>eller</em>';
$txt['pm_readable_start'] = 'Hvis ';
$txt['pm_readable_end'] = '.';
$txt['pm_readable_member'] = 'Meldingen er fra &quot;{MEMBER}&quot;';
$txt['pm_readable_group'] = 'Senderen er fra gruppen &quot;{GROUP}&quot;';
$txt['pm_readable_subject'] = 'Meldingens tittel inneholder &quot;{SUBJECT}&quot;';
$txt['pm_readable_body'] = 'Meldingens tekst inneholder &quot;{BODY}&quot;';
$txt['pm_readable_buddy'] = 'Avsenderen er en venn';
$txt['pm_readable_label'] = 'Legg til merkelapp &quot;{LABEL}&quot;';
$txt['pm_readable_delete'] = 'Slett meldingen';
$txt['pm_readable_then'] = '<strong>da</strong> ';

?>