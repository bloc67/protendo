<?php
// Version: 2.0; Manual

/* Everything in this file is for the Simple Machines help manual
   If you are looking at translating the manual into another language
   please visit the Simple Machines website for tools to assist! */
$txt['manual_smf_user_help'] = 'Protendo brukerhjelp';

$txt['manual_welcome'] = 'Velkommen til %s, drevet av Simple Machines&reg; Forum (Protendo) programvare!';
$txt['manual_introduction'] = 'Protendo er et elegant, effektiv, kraftig og gratis forumløsning som dette nettstedet kjører. Det lar brukere kommunisere og diskutere på et gitt emne i en smart og organisert måte. Videre har den en rekke kraftige funksjoner som brukere kan dra nytte av. Hjelp for mange av Protendo funksjoner finnes enten ved å klikke på spørsmålstegn ved siden av den aktuelle delen eller ved å velge en av lenkene på denne siden. Disse lenkene vil ta deg til Protendo offisielle dokumentasjonssider på Simple Machines offisielle nettsted.';
$txt['manual_docs_and_credits'] = 'For mer om hvordan bruker Protendo, se <a href="%1$s" target="_blank" class="new_win">Simple Machines Documentation Wiki</a> og sjekk ut <a href="%2$s">Medvirkende</a> for å finne ut hvem som har gjort Protendo hva det er i dag.';

$txt['manual_section_registering_title'] = 'Registrering';
$txt['manual_section_logging_in_title'] = 'Innlogging';
$txt['manual_section_profile_title'] = 'Profiler';
$txt['manual_section_search_title'] = 'Søk';
$txt['manual_section_posting_title'] = 'Legge inn innlegg';
$txt['manual_section_bbc_title'] = 'Formateringskoder (BBC)';
$txt['manual_section_personal_messages_title'] = 'Personlige meldinger';
$txt['manual_section_memberlist_title'] = 'Medlemsliste';
$txt['manual_section_calendar_title'] = 'Kalender';
$txt['manual_section_features_title'] = 'Funksjoner';

$txt['manual_section_registering_desc'] = 'Mange fora kreve at brukerne registrere deg for å få full tilgang.';
$txt['manual_section_logging_in_desc'] = 'Når du er registrert, må brukerne logge inn for å få tilgang på sin konto.';
$txt['manual_section_profile_desc'] = 'Hvert medlem har sin egen personlige profil.';
$txt['manual_section_search_desc'] = 'Søking er et ekstremt nyttig verktøy for å finne informasjon i innlegg og emner.';
$txt['manual_section_posting_desc'] = 'Hele poenget med et forum, la brukerne få uttrykke seg.';
$txt['manual_section_bbc_desc'] = 'Innlegg kan krydres litt ved å formatere tekste.';
$txt['manual_section_personal_messages_desc'] = 'Brukere kan sende personlige meldinger til hverandre.';
$txt['manual_section_memberlist_desc'] = 'Medlemsliste viser alle brukerne på forumer.';
$txt['manual_section_calendar_desc'] = 'Brukere kan holde oversikt over hendelser, helligdager og bursdager med kalenderen.';
$txt['manual_section_features_desc'] = 'Her er en liste over de mest populære funksjonene i Protendo.';

?>