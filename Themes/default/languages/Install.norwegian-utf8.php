<?php
// Version: 2.0; Install

// These should be the same as those in index.language.php.
$txt['lang_character_set'] = 'UTF-8';
$txt['lang_rtl'] = false;

$txt['install_step_welcome'] = 'Velkommen';
$txt['install_step_writable'] = 'Sjekker skrivetilgangen';
$txt['install_step_forum'] = 'Forum innstillinger';
$txt['install_step_databaseset'] = 'Databaseinnstillinger';
$txt['install_step_databasechange'] = 'Setter opp databasen';
$txt['install_step_admin'] = 'Administrator konto';
$txt['install_step_delete'] = 'Avslutter installasjonen';

$txt['smf_installer'] = 'Installasjonsprogram for Protendo';
$txt['installer_language'] = 'Språk';
$txt['installer_language_set'] = 'Sett';
$txt['congratulations'] = 'Gratulerer! Installasjonen er nå fullført!';
$txt['congratulations_help'] = 'Hvis du på noe tidspunkt trenger hjelp, eller at Protendo ikke fungerer korrekt, kan du besøke <a href="http://www.simplemachines.org/community/index.php">Simple Machines sitt forum</a> og be om hjelp hvis du trenger det.';
$txt['still_writable'] = 'Installasjonsmappen din er fremdeles skrivbar. Det er gjerne en god idé å endre rettighetene for den mappen sånn at den ikke er skrivbar av hensyn til sikkerheten.';
$txt['delete_installer'] = 'Klikk her for å slette filen install.php';
$txt['delete_installer_maybe'] = '<em>(fungerer ikke på alle servere.)</em>';
$txt['go_to_your_forum'] = 'Nå kan du gå inn og se på <a href="%1$s">ditt helt nye forum</a> som er klar til bruk. Du bør først logge inn, slik at du får tilgang til å utføre Administratoroppgaver.';
$txt['good_luck'] = 'Lykke til!<br />Simple Machines';

$txt['install_welcome'] = 'Velkommen';
$txt['install_welcome_desc'] = 'Velkommen til Protendo. Dette skriptet vil veilede deg gjennom prosessen for installasjon av %1$s. Vi vil samle noen detaljer om forumet ditt i de neste trinnene, og etter et par minutter vil forumet være klar for bruk.';
$txt['install_all_lovely'] = 'Vi har gjennomført noen innledende tester på serveren din og alt ser ut til å være i orden. Bare klikk på knappen &quot;Fortsett&quot; nedenfor for å komme i gang.';

$txt['user_refresh_install'] = 'Forum oppdatert';
$txt['user_refresh_install_desc'] = 'Under installering fant scriptet ut (med detaljer du har lagt ved) at en eller flere tabeller som skulle opprettes allerede eksisterte.<br />Tabeller som manglet i din installasjon er blitt laget, men ingen data er blitt fjernet fra eksisterende tabeller.';

$txt['default_topic_subject'] = 'Velkommen til Protendo!';
$txt['default_topic_message'] = 'Velkommen til Simple Machines Forum!<br /><br />Vi håper du vil trives med ditt forum. Hvis du har noen problemer, ta gjerne kontakt og [url=http://www.simplemachines.org/community/index.php]spør oss om assistanse[/url].<br /><br />Takk!<br />Simple Machines';
$txt['default_board_name'] = 'Generelt';
$txt['default_board_description'] = 'Diskuter alt mulig i dette forumet.';
$txt['default_category_name'] = 'Generell kategori';
$txt['default_time_format'] = '%d.%m.%Y, %H:%i:%s';
$txt['default_news'] = 'Protendo - Nylig installert!';
$txt['default_karmaLabel'] = 'Karma:';
$txt['default_karmaSmiteLabel'] = '[dårlig]';
$txt['default_karmaApplaudLabel'] = '[bra]';
$txt['default_reserved_names'] = 'Admin\\nWebmaster\\nGjest\\nroot';
$txt['default_smileyset_name'] = 'Alienine\'s sett';
$txt['default_aaron_smileyset_name'] = 'Aaron\'s sett';
$txt['default_akyhne_smileyset_name'] = 'Akyhne\'s sett';
$txt['default_theme_name'] = 'Protendo Standard design - Curve';
$txt['default_core_theme_name'] = 'Core design';
$txt['default_classic_theme_name'] = 'Klassisk YaBB SE design';
$txt['default_babylon_theme_name'] = 'Babylon design';

$txt['default_administrator_group'] = 'Administrator';
$txt['default_global_moderator_group'] = 'Global moderator';
$txt['default_moderator_group'] = 'Moderator';
$txt['default_newbie_group'] = 'Nybegynner';
$txt['default_junior_group'] = 'Juniormedlem';
$txt['default_full_group'] = 'Medlem';
$txt['default_senior_group'] = 'Seniormedlem';
$txt['default_hero_group'] = 'Supermedlem';

$txt['default_smiley_smiley'] = 'Smiler';
$txt['default_wink_smiley'] = 'Blunker';
$txt['default_cheesy_smiley'] = 'Osteaktig';
$txt['default_grin_smiley'] = 'Gliser';
$txt['default_angry_smiley'] = 'Sint';
$txt['default_sad_smiley'] = 'Trist';
$txt['default_shocked_smiley'] = 'Sjokkert';
$txt['default_cool_smiley'] = 'Kult';
$txt['default_huh_smiley'] = 'Hææ?';
$txt['default_roll_eyes_smiley'] = 'Øyerulling';
$txt['default_tongue_smiley'] = 'Rekke tunge';
$txt['default_embarrassed_smiley'] = 'Flaut';
$txt['default_lips_sealed_smiley'] = 'Hemmelig';
$txt['default_undecided_smiley'] = 'Tvilende';
$txt['default_kiss_smiley'] = 'Kysser';
$txt['default_cry_smiley'] = 'Gråter';
$txt['default_evil_smiley'] = 'Ondskapsfull';
$txt['default_azn_smiley'] = 'Djevelsk';
$txt['default_afro_smiley'] = 'Afro';
$txt['default_laugh_smiley'] = 'Ler';
$txt['default_police_smiley'] = 'Politi';
$txt['default_angel_smiley'] = 'Angel';

$txt['error_message_click'] = 'Klikk her';
$txt['error_message_try_again'] = 'for å prøve dette steget en gang til.';
$txt['error_message_bad_try_again'] = 'for å installere uansett, men merk at du er <em>sterkt</em> frarådet fra å gjøre det.';

$txt['install_settings'] = 'Vanlige innstillinger';
$txt['install_settings_info'] = 'Noen småting du må skrive inn ;).';
$txt['install_settings_name'] = 'Navn på forumet';
$txt['install_settings_name_info'] = 'Dette er navnet på forumet ditt, eks. &quot;Mitt Protendo-forum&quot;.';
$txt['install_settings_name_default'] = 'Mitt Protendo-forum';
$txt['install_settings_url'] = 'Nettadressen til forumet';
$txt['install_settings_url_info'] = 'Dette er adressen til forumet ditt <strong>uten \'/\' på slutten!</strong>.<br />I de fleste tilfellene trenger du ikke å redigere denne boksen, da det som står der fra før, normalt er korrekt.';
$txt['install_settings_compress'] = 'Gzip utdata';
$txt['install_settings_compress_title'] = 'Komprimerer utdata for å spare båndbredde.';
// In this string, you can translate the word "PASS" to change what it says when the test passes.
$txt['install_settings_compress_info'] = 'Denne funksjonen fungerer ikke på alle servere, men du kan spare mye båndbredde på dette.<br />Klikk <a href="install.php?obgz=1&amp;pass_string=GODKJENT" onclick="return reqWin(this.href, 200, 60);">her</a> for å teste om det fungerer. (Det skal bare stå "GODKJENT".)';
$txt['install_settings_dbsession'] = 'Database-baserte sesjoner';
$txt['install_settings_dbsession_title'] = 'Lagre sessjoner i databasen i stedet for i filer.';
$txt['install_settings_dbsession_info1'] = 'Dette er nesten alltid det beste, fordi det gjør bruk av sesjoner mer pålitelig.';
$txt['install_settings_dbsession_info2'] = 'Dette er nesten alltid det beste, men det vil kanskje ikke virke på denne serveren.';
$txt['install_settings_utf8'] = 'UTF-8 Tegnsett';
$txt['install_settings_utf8_title'] = 'Bruk UTF-8 som standard tegnsett';
$txt['install_settings_utf8_info'] = 'Denne funksjonen vil sette både database og forum til å bruke et internasjonalt tegnsett, UTF-8. Dette kan være nyttig når flere språk med forskjellig tegnsett benyttes.';
$txt['install_settings_stats'] = 'Tillat henting av statistikk data';
$txt['install_settings_stats_title'] = 'La Simple Machines hente inn enkle statistiske data hver måned';
$txt['install_settings_stats_info'] = 'Hvis dette er aktivert vil det tillate Simple Machines å besøke ditt nettsted en gang i måneden for å hente enkle statistiske data. Dette vil hjelpe oss å forbedre hva slags konfigurajoner Protendo støtter. For mer informasjon vennligst besøk vår <a href="http://www.simplemachines.org/about/stats.php" target="_blank">informasjonsside</a>.';
$txt['install_settings_proceed'] = 'Fortsett';

$txt['db_settings'] = 'Database serverinnstillinger';
$txt['db_settings_info'] = 'Dette er innstillingene skal brukes til databaseserveren. Hvis du ikke kjenner verdiene, bør du spørre din leverandør hva de er.';
$txt['db_settings_type'] = 'Databasetype';
$txt['db_settings_type_info'] = 'Flere typer databaser ble oppdaget – hvilken type ønsker du å bruke.';
$txt['db_settings_server'] = 'Servernavn';
$txt['db_settings_server_info'] = 'Dette er oftest localhost - så om du ikke vet, prøv localhost.';
$txt['db_settings_username'] = 'Brukernavn';
$txt['db_settings_username_info'] = 'Skriv inn brukernavnet du trenger for å koble til databasen.<br />Om du ikke vet hva det er, prøv brukernavnet til FTP-kontoen din. Oftest er disse to like.';
$txt['db_settings_password'] = 'Passord';
$txt['db_settings_password_info'] = 'Her skal du skrive inn passordet du trenger for å koble til databasen.<br />Om du ikke vet hva det er, prøv passordet du bruker på FTP-kontoen din.';
$txt['db_settings_database'] = 'Databasenavn';
$txt['db_settings_database_info'] = 'Skriv inn navnet på databasen du vil bruke som Protendo kan lagre sine data i.';
$txt['db_settings_database_info_note'] = 'Om databasen ikke eksisterer, vil dette installasjonsprogrammet prøve å opprette databasen.';
$txt['db_settings_database_file'] = 'Database filnavn';
$txt['db_settings_database_file_info'] = 'Dette er navnet på filen der Protendo lagrer data. Vi anbefaler at du bruker tilfeldig generert navn for dette og sette stien til denne filen til å være det utenfor offentlig område på nettserveren din.';
$txt['db_settings_prefix'] = 'Tabellprefiks';
$txt['db_settings_prefix_info'] = 'Prefiksen for hver tabell i databasen. <strong>Ikke installer to fora med samme prefiks!</strong><br />Denne verdien muliggjør flere installasjoner i én database.';
$txt['db_sqlite_warning'] = 'Anbefales bare for småe, lave volum og/eller intranett-type fora';
$txt['db_populate'] = 'Setter opp databasen';
$txt['db_populate_info'] = 'Innstillingene er nå lagret og databasen installert med alle data som kreves for å få forumet oppe og gå. Oppsummering av installasjonen:';
$txt['db_populate_info2'] = 'Klikk &quot;Fortsett&quot; for å gå videre til siden for opprettelse av administrator kontoen.';
$txt['db_populate_inserts'] = 'Satt inn %1$d rader.';
$txt['db_populate_tables'] = 'Opprettet %1$d tabeller.';
$txt['db_populate_insert_dups'] = 'Ignorert %1$d dupliserte innsettelser.';
$txt['db_populate_table_dups'] = 'Ignorert %1$d dupliserte tabeller.';

$txt['user_settings'] = 'Opprette din konto';
$txt['user_settings_info'] = 'Dette installasjonsprogrammet vil nå opprette en ny administratorkonto for deg.';
$txt['user_settings_username'] = 'Ditt brukernavn';
$txt['user_settings_username_info'] = 'Velg et brukernavn du vil logge inn med.<br />Dette kan ikke endres senere, men du kan endre ditt visningsnavn.';
$txt['user_settings_password'] = 'Passord';
$txt['user_settings_password_info'] = 'Skriv inn ditt foretrukne passord her, og husk godt på det!';
$txt['user_settings_again'] = 'Passord';
$txt['user_settings_again_info'] = '(bare for bekreftelse.)';
$txt['user_settings_email'] = 'E-postadresse';
$txt['user_settings_email_info'] = 'Skriv inn din e-postadresse. <strong>Dette må være en gyldig e-postadresse.</strong>';
$txt['user_settings_database'] = 'Passord for databasen';
$txt['user_settings_database_info'] = 'Av sikkerhetsgrunner kreves det at du skriver inn passordet for databasen når du oppretter en administratorkonto.';
$txt['user_settings_skip'] = 'Hopp over';
$txt['user_settings_skip_sure'] = 'Er du sikker på at du ønsker å oppe over opprettelse av admin-kontoen';
$txt['user_settings_proceed'] = 'Fullfør';

$txt['ftp_checking_writable'] = 'Kontrollerer at filene er skrivbare';
$txt['ftp_setup'] = 'Informasjon for FTP-tilkobling';
$txt['ftp_setup_info'] = 'Dette installasjonsprogrammet kan koble til med FTP for å rette på filene som må være skrivbare, men som ikke allerede er det. Dersom dette ikke fungerer for deg, må du selv gå inn og manuelt gjøre filene skrivbare. Merk at denne funksjonen ikke støtter SSL-tilgang for øyeblikket.';
$txt['ftp_server'] = 'Server';
$txt['ftp_server_info'] = 'Dette skal være servernavn og portnummer for FTP-serveren.';
$txt['ftp_port'] = 'Port';
$txt['ftp_username'] = 'Brukernavn';
$txt['ftp_username_info'] = 'Brukernavnet som brukes til innlogging. <em>Dette vil ikke lagres noe sted.</em>';
$txt['ftp_password'] = 'Passord';
$txt['ftp_password_info'] = 'Passordet som brukes til innlogging. <em>Dette vil ikke lagres noe sted.</em>';
$txt['ftp_path'] = 'Installasjons-sti';
$txt['ftp_path_info'] = 'Dette er den <em>relative</em> stien du bruker på din FTP-server.';
$txt['ftp_path_found_info'] = 'Stien i feltet ovenfor ble automatisk funnet.';
$txt['ftp_connect'] = 'Koble til';
$txt['ftp_setup_why'] = 'Hva er dette steget godt for?';
$txt['ftp_setup_why_info'] = 'Noen filer må være skrivbare for at Protendo skal fungere ordentlig. Dette steget lar innstalleringsscriptet gjøre disse filens skrivbare. Imidlertid, i noen tilfeller vil ikke dettte virke - i så fall sett følgende filer til 777 (skrivbare, 755 på noen servere):';
$txt['ftp_setup_again'] = 'for å på nytt teste om disse filene er skrivbare.';

$txt['error_php_too_low'] = 'Advarsel! Det ser ut som du har en PHP-versjon installert på webserveren som <strong>ikke møter Protendos minimumskrav</strong>.<br />Om du ikke er leverandøren av nettserveren, må du be din leverandør om å oppgradere, eller bruke en annen leverandør - uansett, bør den oppgraderes til en nyere versjon.<br /><br />Dersom du vet at du faktisk bruker en PHP-versjon som er tilstrekkelig, må du gjerne fortsette. Men du er sterkt frarådet fra å gjøre det.';
$txt['error_missing_files'] = 'Kunne ikke finne viktige installasjonsfiler i samme mappe som dette scriptet!<br /><br />Kontroller at du lastet opp alle filene i installasjonspakken, inklusive sql-filen, og gjør så et nytt forsøk.';
$txt['error_session_save_path'] = 'Informer din leverandør om at følgende streng i php.ini er ugyldig: <strong>session.save_path</strong>. Den må endres til en mappe som <strong>eksisterer</strong> og er <strong>skrivbar</strong> for brukeren PHP kjører som.<br />';
$txt['error_windows_chmod'] = 'Du er på en Windows-server og noen av de viktigste filene er ikke skrivbare. Snakk med din leverandør og be dem ta av <strong>Skrivebeskyttelsen</strong> for brukeren som PHP kjører som.';
$txt['error_ftp_no_connect'] = 'Kunne ikke koble til FTP-serveren med den informasjonen du skrev inn.';
$txt['error_db_file'] = 'Kan ikke finne kilden til databaseskriptet! Vennligst sjekk at filen %1$s is er i rot mappen på forumet.';
$txt['error_db_connect'] = 'Kan ikke koble til database-serveren med de oppgitte dataene.<br /><br />Hvis du ikke er sikker på hva som skal skrives inn, vennligst kontakt din leverandør.';
$txt['error_db_too_low'] = 'Versjonen på databaseserver er svært gammel, og oppfyller ikke Protendo minimumskrav.<br /><br />Spør din vert om enten å oppgradere eller flytte deg til en ny server, og hvis de ikke vil, prøv en annen leverandør.';
$txt['error_db_database'] = 'Installasjonsprogrammet kunne ikke få tilgang til &quot;<em>%1$s</em>&quot; databasen. På noen servere må du opprette databasen i administratorpanelet før Protendo kan bruke den. Noen legger gjerne til noen prefikser, som f.eks. brukernavnet ditt til navn på databasene.';
$txt['error_db_queries'] = 'Noen av spørringene ble ikke kjørt korrekt. Dette kan være forårsaket av ikke støttet (utvikling eller gamle) versjonen av databaseprogramvaren.<br /><br />Teknisk informasjon om spørringene:';
$txt['error_db_queries_line'] = 'Linje nr.';
$txt['error_db_missing'] = 'Installasjonsprogrammet kunne ikke oppdage noen database støtte i PHP. Spør din leverandør for å sikre at PHP er kompilert med den ønskede databasen, eller at de ønskede utvidelsene blir lastet.';
$txt['error_db_script_missing'] = 'Installasjonsprogrammet fant ingen installasjonsskriptfiler for den oppdagede databasen. Vennligst sjekk at du har lastet de nødvendige installasjonsskriptfilene til forum mappen, for eksempel &quot;%1$s&quot;';
$txt['error_session_missing'] = 'Installasjonsprogrammet kunne ikke finne støtte for sesjoner i serverens PHP-installasjon. Vennligst be din leverandør om å forsikre seg om at PHP ble kompilert med støtte for sesjoner (faktisk må det kompileres eksplisitt for å ikke støtte det.)';
$txt['error_user_settings_again_match'] = 'Du skrev inn to helt forskjellige passord!';
$txt['error_user_settings_no_password'] = 'Passordet ditt må være minst 4 tegn langt.';
$txt['error_user_settings_taken'] = 'Beklager, et medlem har allerede registrert seg med det brukernavnet og/eller passordet.<br /><br />En ny konto har ikke blitt opprettet.';
$txt['error_user_settings_query'] = 'En database-feil oppstod under opprettelsen av administratorkontoen. Denne feilen var:';
$txt['error_subs_missing'] = 'Kunne ikke finne fil med navn Sources/Subs.php . Kontrollere at den ble lastet opp og prøv så på nytt.';
$txt['error_db_alter_priv'] = 'Databasekontoen du har oppgitt har ikke tillatelse til ALTER, CREATE, og/eller DROP tabeller i databasen, dette er nødvendig for Protendo skal fungere korrekt.';
$txt['error_versions_do_not_match'] = 'Installasjonsprogrammet har oppdaget at en annen versjon av Protendo er allerede installert med denne informasjonen. Dersom du heller vil oppgradere, må du bruke oppgraderingsprogrammet, og ikke installasjonsprogrammet.<br /><br />Hvis ikke, fyll ut en annen informasjon eller opprett en sikkerhetskopi for deretter å slette alle data i databasen.';
$txt['error_mod_security'] = 'Installasjonsprogrammet oppdaget at mod_security er installert på serveren. Denne funksjonen vil blokkere utfyllingsformer før Protendo får sagt noe. Protendo har en innebygget kontrollør som vil fungere mer effektivt enn mod_security og vil ikke blokkere utfyllingsformene.<br /><br /><a href="http://www.simplemachines.org/redirect/mod_security">Mer informasjon om deaktivering av mod_security</a> (området er på engelsk)';
$txt['error_mod_security_no_write'] = 'Installasjonsprogrammet har oppdaget at modulen mod_security er installert på nettserveren din. Mod_security vil blokkere sendte skjemaer før Protendo rekker å reagere. Protendo har en innebygd sikkerhetsskanner som arbeider mer effektivt enn mod_security og som ikke blokkerer sending av skjemaer.<br /><br /><a href="http://www.simplemachines.org/redirect/mod_security">For mer informasjon om deaktivering av mod_security</a><br /><br />Alternativt kan du bruke en ftp-klient til å chmod filen .htaccess i forummappen til å være skrivbar (777), og deretter friske opp denne siden.';
$txt['error_utf8_version'] = 'Den gjeldende versjonen av din database støtter ikke bruk av UTF-8 tegnsett. Du kan fortsatt installere Protendo uten problemer, men da uten støtte for UTF-8. Hvis du ønsker å bytte over til UTF-8 i fremtiden (for eksempel etter at databaseserveren for forumet ditt har blitt oppgradert til versjon >= %1$s), kan du konvertere forumet til UTF-8 gjennom administrator panelet.';
$txt['error_valid_email_needed'] = 'Du har ikke skrevet en gyldig e-postadresse.';
$txt['error_already_installed'] = 'Installasjonsprogrammet har oppdaget at du allerede har Protendo installert. Det anbefales på det sterkeste at du <strong>IKKE</strong> forsøker å overskrive en eksisterende installasjon. Fortsetter du med installasjonen <strong>kan det resultere i tap eller skade på eksisterende data</strong>.<br /><br />Hvis du ønsker å oppgradere kan du gå til <a href="http://www.simplemachines.org">Simple Machines nettside</a> nettside <em>oppgraderings</em> pakken.<br /><br />Hvis du ønsker å overskrive eksisterende installasjon, inkludert alle data, anbefales det at du sletter eksisterende databasetabeller og erstatte settings.php og prøv igjen.';
$txt['error_warning_notice'] = 'Advarsel!';
$txt['error_script_outdated'] = 'Dette installasjonsskriptet er utdatert! Den gjeldende versjonen av Protendo er %1$s men dette installasjonsskriptet er for %2$s.<br /><br />
	Det anbefales at du går til <a href="http://www.simplemachines.org">Simple Machines</a> nettside for å sikre at du installerer den nyeste versjonen.';
$txt['error_db_filename'] = 'Du må angi et navn til databasefilen til SQLite.';
$txt['error_db_prefix_numeric'] = 'Den valgte databasetype støtter ikke bruk av numeriske prefikser.';
$txt['error_invalid_characters_username'] = 'Ugyldig tegn benyttet i brukernavn.';
$txt['error_username_too_long'] = 'Brukernavnet må være mindre enn 25 tegn langt.';
$txt['error_username_left_empty'] = 'Feltet med brukernavn er ikke fylt ut.';
$txt['error_db_filename_exists'] = 'Databasen som du prøver å opprette eksisterer allerede. Vennligst slette den aktuelle databasefilen eller angi et annet navn.';
$txt['error_db_prefix_reserved'] = 'Prefikset som du skrev er et reservert prefiks. Vennligst skriv inn et annet prefiks.';

$txt['upgrade_upgrade_utility'] = 'Protendo oppgraderingsverktøy';
$txt['upgrade_warning'] = 'Advarsel!';
$txt['upgrade_critical_error'] = 'Kritisk feil!';
$txt['upgrade_continue'] = 'Fortsett';
$txt['upgrade_skip'] = 'Hopp over';
$txt['upgrade_note'] = 'Merk!';
$txt['upgrade_step'] = 'Steg';
$txt['upgrade_steps'] = 'Steg';
$txt['upgrade_progress'] = 'Fremdrift';
$txt['upgrade_overall_progress'] = 'Samlet fremdrift';
$txt['upgrade_step_progress'] = 'Gjeldende fremdrift';
$txt['upgrade_time_elapsed'] = 'Tid brukt';
$txt['upgrade_time_mins'] = 'minutter';
$txt['upgrade_time_secs'] = 'sekunder';

$txt['upgrade_incomplete'] = 'Ufullstendig';
$txt['upgrade_not_quite_done'] = 'Ikke helt ferdig ennå!';
$txt['upgrade_paused_overload'] = 'Oppgraderingen er satt på pause for å unngå overbelastning på serveren. Ikke bekymre deg, ingenting er galt - bare klikk på knappen <label for="contbutt">fortsette</label> nedenfor for å fortsette.';

$txt['upgrade_ready_proceed'] = 'Takk for at du velger å oppgradere til Protendo %1$s. Alle filer ser ut til å være på plass, og vi er klar til å fortsette.';

$txt['upgrade_error_script_js'] = 'Oppgraderingsskriptet kan ikke finne filen script.js eller den er utdatert. Sjekk at stien til mappen Theme er riktige. Du kan laste ned et verktøy for å sjekke innstillingene fra <a href="http://www.simplemachines.org">Simple Machines nettside</a>';

$txt['upgrade_warning_lots_data'] = 'Oppgraderingsskriptet har oppdaget at Forumet inneholder store mengder av data som trenger oppgradering. Denne prosessen kan ta ganske lang tid avhengig av server og størrelsen på forumet, og for veldig store fora (~ 300 000 meldinger) kan det ta flere timer å fullføre.';
$txt['upgrade_warning_out_of_date'] = 'Dette oppgraderingsskriptet er utdatert! Den gjeldende versjonen av Protendo er <em id="smfVersion" style="white-space: nowrap;">??</em>, men dette oppgraderingsskriptet er for <em id="yourVersion" style="white-space: nowrap;">%1$s</em>.<br /><br />det anbefales at du går til <a href="http://www.simplemachines.org">Simple Machines</a> nettsted for å sikre at du oppgraderer til nyeste versjon.';

?>