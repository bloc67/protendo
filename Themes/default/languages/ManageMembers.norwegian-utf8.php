<?php
// Version: 2.0; ManageMembers

global $context;

$txt['groups'] = 'Grupper';
$txt['viewing_groups'] = 'Viser medlemsgrupper';

$txt['membergroups_title'] = 'Behandle medlemsgrupper';
$txt['membergroups_description'] = 'Medlemsgrupper er grupper av medlemmer som har like rettighetsinnstillinger, utseende eller tilgangsrettigheter. Noen medlemsgrupper er basert på antall innlegg medlemmet har skrevet. Du kan sette medlemsgruppe for et medlem ved å velge profilen og klikke på Kontoinnstillinger.';
$txt['membergroups_modify'] = 'Rediger';

$txt['membergroups_add_group'] = 'Legg til gruppe';
$txt['membergroups_regular'] = 'Vanlige grupper';
$txt['membergroups_post'] = 'Innleggsbaserte grupper';

$txt['membergroups_group_name'] = 'Navn på medlemsgruppe';
$txt['membergroups_new_board'] = 'Synlige forum';
$txt['membergroups_new_board_desc'] = 'Forum denne medlemsgruppa kan se.';
$txt['membergroups_new_board_post_groups'] = '<em>Merk: Vanligvis trenger ikke innleggsbaserte grupper rettigheter, ettersom de vanlige gruppene medlemmene tilhører gir den tilgang som er nødvendig.</em>';
$txt['membergroups_new_as_inherit'] = 'arve fra';
$txt['membergroups_new_as_type'] = 'etter type';
$txt['membergroups_new_as_copy'] = 'eller basert på';
$txt['membergroups_new_copy_none'] = '(ingen)';
$txt['membergroups_can_edit_later'] = '(dette kan redigeres etterpå.)';

$txt['membergroups_edit_group'] = 'Rediger gruppe';
$txt['membergroups_edit_name'] = 'Navn på gruppe';
$txt['membergroups_edit_inherit_permissions'] = 'Arve rettigheter';
$txt['membergroups_edit_inherit_permissions_desc'] = 'Velg &quot;Nei&quot; slik at gruppen har egen rettigheter.';
$txt['membergroups_edit_inherit_permissions_no'] = 'Nei - Bruk unike rettigheter';
$txt['membergroups_edit_inherit_permissions_from'] = 'Arve fra';
$txt['membergroups_edit_hidden'] = 'Synlighet';
$txt['membergroups_edit_hidden_no'] = 'Synlig';
$txt['membergroups_edit_hidden_boardindex'] = 'Synlig - Bortsett fra i nøkkelgruppe';
$txt['membergroups_edit_hidden_all'] = 'Usynlig';
// Do not use numeric entities in the below string.
$txt['membergroups_edit_hidden_warning'] = 'Er du sikker på at du ønsker å forby tildeling av denne gruppen som brukernes primær gruppe?\\n\\nDette vil bare begrense tilordning til flere grupper, og vil bare oppdatere alle nåværende &quot;primær&quot; medlemmer å ha det som en ekstra gruppe.';
$txt['membergroups_edit_desc'] = 'Gruppebeskrivelse';
$txt['membergroups_edit_group_type'] = 'Gruppetype';
$txt['membergroups_edit_select_group_type'] = 'Velg gruppetype';
$txt['membergroups_group_type_private'] = 'Privat <span class="smalltext">(Medlemskap må tildeles)</span>';
$txt['membergroups_group_type_protected'] = 'Beskyttet <span class="smalltext">(Kun administratorer kan administrere og tildele)</span>';
$txt['membergroups_group_type_request'] = 'Etter anmodning <span class="smalltext">(Bruker kan be om medlemskap)</span>';
$txt['membergroups_group_type_free'] = 'Fri <span class="smalltext">(Bruker kan forlate og bli med i grupper som bruker selv vil)</span>';
$txt['membergroups_group_type_post'] = 'Innleggsbasert <span class="smalltext">(Medlemskap er basert på antall innlegg)</span>';
$txt['membergroups_min_posts'] = 'Minste antall innlegg';
$txt['membergroups_online_color'] = 'Farge i påloggetliste';
$txt['membergroups_star_count'] = 'Antall stjerne-bilder';
$txt['membergroups_star_image'] = 'Filnavn til stjerne-bildet';
$txt['membergroups_star_image_note'] = 'du kan bruke $language for språket til brukeren';
$txt['membergroups_max_messages'] = 'Maks antall PM';
$txt['membergroups_max_messages_note'] = '(0 = ubegrenset)';
$txt['membergroups_edit_save'] = 'Lagre';
$txt['membergroups_delete'] = 'Slett';
$txt['membergroups_confirm_delete'] = 'Er du sikker på at du vil slette denne gruppen?!';

$txt['membergroups_members_title'] = 'Viser alle medlemmer fra medlemsgruppen';
$txt['membergroups_members_group_members'] = 'Gruppemedlemmer';
$txt['membergroups_members_no_members'] = 'Denne gruppa er tom';
$txt['membergroups_members_add_title'] = 'Legg til et medlem i denne gruppen';
$txt['membergroups_members_add_desc'] = 'Liste over medlemmer som skal legges til';
$txt['membergroups_members_add'] = 'Legg til medlemmer';
$txt['membergroups_members_remove'] = 'Fjern fra gruppen';
$txt['membergroups_members_last_active'] = 'Sist aktiv';
$txt['membergroups_members_additional_only'] = 'Legg kun til som ekstra gruppe.';
$txt['membergroups_members_group_moderators'] = 'Moderatorergrupper';
$txt['membergroups_members_description'] = 'Beskrivelse';
// Use javascript escaping in the below.
$txt['membergroups_members_deadmin_confirm'] = 'Er du sikker på at du vil fjerne deg fra administratorgruppen?';

$txt['membergroups_postgroups'] = 'Innleggsbaserte grupper';
$txt['membergroups_settings'] = 'Innstillinger for medlemsgrupper';
$txt['groups_manage_membergroups'] = 'Grupper med rettighet til å endre medlemsgrupper';
$txt['membergroups_select_permission_type'] = 'Velg rettighetsprofil';
$txt['membergroups_images_url'] = '{design adresse}/images/';
$txt['membergroups_select_visible_boards'] = 'Vis fora';
$txt['membergroups_members_top'] = 'Medlemmer';
$txt['membergroups_name'] = 'Navn';
$txt['membergroups_stars'] = 'Stjerner';

$txt['admin_browse_approve'] = 'Medlemmer som venter på godkjenning av konto';
$txt['admin_browse_approve_desc'] = 'Her kan du behandle alle medlemmer som venter på godkjenning av konto.';
$txt['admin_browse_activate'] = 'Medlemmer som venter på aktivering av konto';
$txt['admin_browse_activate_desc'] = 'Her listes alle medlemmer som fremdeles ikke har aktivert sin medlemskonto.';
$txt['admin_browse_awaiting_approval'] = 'Venter på godkjenning (%1$d)';
$txt['admin_browse_awaiting_activate'] = 'Venter på aktivering (%1$d)';

$txt['admin_browse_username'] = 'Brukernavn';
$txt['admin_browse_email'] = 'E-postadresse';
$txt['admin_browse_ip'] = 'IP-adresse';
$txt['admin_browse_registered'] = 'Registrert';
$txt['admin_browse_id'] = 'ID';
$txt['admin_browse_with_selected'] = 'Med valgte';
$txt['admin_browse_no_members_approval'] = 'Det er for øyeblikket ingen medlemmer som venter på godkjenning.';
$txt['admin_browse_no_members_activate'] = 'Det er for øyeblikket ingen medlemmer som ikke har aktivert sin konto.';

// Don't use entities in the below strings, except the main ones. (lt, gt, quot.)
$txt['admin_browse_warn'] = 'alle medlemmer som er valgt?';
$txt['admin_browse_outstanding_warn'] = 'alle medlemmer dette påvirker?';
$txt['admin_browse_w_approve'] = 'Godkjenn';
$txt['admin_browse_w_activate'] = 'Aktivér';
$txt['admin_browse_w_delete'] = 'Slett';
$txt['admin_browse_w_reject'] = 'Avvis';
$txt['admin_browse_w_remind'] = 'Gi påminnelse';
$txt['admin_browse_w_approve_deletion'] = 'Godkjenn (slette kontoer)';
$txt['admin_browse_w_email'] = 'og send e-post';
$txt['admin_browse_w_approve_require_activate'] = 'Godkjenn og krev aktivering';

$txt['admin_browse_filter_by'] = 'Filtrer på';
$txt['admin_browse_filter_show'] = 'Viser';
$txt['admin_browse_filter_type_0'] = 'Ikke aktiverte nye kontoer';
$txt['admin_browse_filter_type_2'] = 'Ikke aktiverte endringer av e-post';
$txt['admin_browse_filter_type_3'] = 'Ennå ikke godkjente nye kontoer';
$txt['admin_browse_filter_type_4'] = 'Ennå ikke godkjente slettinger av kontoer';
$txt['admin_browse_filter_type_5'] = 'Ennå ikke godkjente "mindreårlige" kontoer';

$txt['admin_browse_outstanding'] = 'Fremragende medlemmer';
$txt['admin_browse_outstanding_days_1'] = 'Med alle medlemmer som registrerte seg for mer enn';
$txt['admin_browse_outstanding_days_2'] = 'dager siden';
$txt['admin_browse_outstanding_perform'] = 'Utfør følgende handling';
$txt['admin_browse_outstanding_go'] = 'Utfør handling';

$txt['check_for_duplicate'] = 'Sjekk for duplikater';
$txt['dont_check_for_duplicate'] = 'Ikke sjekk for duplikater';
$txt['duplicates'] = 'Duplikater';

$txt['not_activated'] = 'Ikke aktivert';

?>