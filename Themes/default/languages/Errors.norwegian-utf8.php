<?php
// Version: 2.0; Errors

global $scripturl, $modSettings;

$txt['no_access'] = 'Du har ikke tilgang til å vise denne delen av forumet';
$txt['wireless_error_notyet'] = 'Beklager, men denne delen er for tiden ikke tilgjengelig for bruker over trådløse nettverk.';

$txt['mods_only'] = 'Bare moderatorer kan slette direkte, vennligst bruke redigeringsfunksjonen for å slette innlegget.';
$txt['no_name'] = 'Du skrev ikke inn et navn. Det er påkrevd.';
$txt['no_email'] = 'Du skrev ikke inn en e-postadresse. Det er påkrevd.';
$txt['topic_locked'] = 'Emnet er låst. Du har ikke tilgang til å skrive eller redigere innlegg...';
$txt['no_password'] = 'Passordfeltet er tomt';
$txt['already_a_user'] = 'Brukernavnet du prøvde å registrere eksisterer allerede.';
$txt['cant_move'] = 'Du har ikke tilgang til å flytte emner...';
$txt['login_to_post'] = 'For å skrive innlegg må du være logget inn. Hvis du ikke har en konto enda kan du klikke <a href="?action=register">her</a> for å registrere deg.';
$txt['passwords_dont_match'] = 'Passordene er ikke identiske.';
$txt['register_to_use'] = 'Beklager, men du må registrere deg før du kan bruke denne funksjonen.';
$txt['password_invalid_character'] = 'Passordet du skrev inn, inneholder ugyldige tegn.';
$txt['name_invalid_character'] = 'Navnet du skrev inn, inneholder ugyldige tegn.';
$txt['email_invalid_character'] = 'E-postadressen du skrev inn, inneholder ugyldige tegn.';
$txt['username_reserved'] = 'Brukernavnet du prøvde å registrere inneholder et reservert navn \\\'%1$s\\\'. Prøv et annet brukernavn.';
$txt['numbers_one_to_nine'] = 'Dette feltet godtar kun tall mellom 0 og 9.';
$txt['not_a_user'] = 'Den brukeren du prøver å vise finnes ikke.';
$txt['not_a_topic'] = 'Det emnet du prøver å lese, eksisterer ikke på forumet.';
$txt['not_approved_topic'] = 'Dette emnet er ikke godkjent ennå.';
$txt['email_in_use'] = 'Den e-postadressen (%1$s) brukes av et registrert medlem allerede. Hvis du føler dette er en feil, gå til logg inn siden og bruke passordet påminnelse med den adressen.';

$txt['didnt_select_vote'] = 'Du valgte ikke et alternativ i avstemninga.';
$txt['poll_error'] = 'Enten eksisterer ikke avstemninga, avstemninga har blitt stengt eller du prøvde å stemme to ganger.';
$txt['members_only'] = 'Dette er kun for medlemmer...';
$txt['locked_by_admin'] = 'Dette emnet ble låst av en administrator. Du kan ikke åpne det.';
$txt['not_enough_posts_karma'] = 'Beklager, du har ikke nok innlegg til å endre karma - du trenger minst %1$d.';
$txt['cant_change_own_karma'] = 'Beklager, men du har ikke tilgang til å redigere din egen karma.';
$txt['karma_wait_time'] = 'Beklager, du kan ikke gjenta en karma handling uten å vente %1$s %2$s.';
$txt['feature_disabled'] = 'Beklager, denne funksjonen er deaktivert.';
$txt['cant_access_upload_path'] = 'Fikk ikke tilgang til mappen hvor vedleggene lagres!';
$txt['file_too_big'] = 'Filen er for stor. Maks størrelse på vedlegg er %1$d KB.';
$txt['attach_timeout'] = 'Vedlegget kunne ikke lagres. Dette kan være fordi opplastningshastigheten er for treg eller at filen er større en det serveren tillater. Ta kontakt med administrator for mer informasjon.';
$txt['filename_exists'] = 'Beklager! Det er allerede et vedlegg med samme filnavn som den du prøvde å laste opp. Vennligst endre navn på filen og prøv igjen.';
$txt['bad_attachment'] = 'Ditt vedlegg ble ikke godkjent i  sikkerhetssjekken og kan ikke lastes opp. Ta kontakt med forumets administrator.';
$txt['ran_out_of_space'] = 'Lagringsområde for for vedlegg er fult. Prøv med en mindre fil og/eller ta konakt med administrator.';
$txt['couldnt_connect'] = 'Kunne ikke koble til serveren eller kunne ikke finne filen';
$txt['no_board'] = 'Forumet du spesifiserte eksisterer ikke';
$txt['cant_split'] = 'Du har ikke tilgang til å splitte emner';
$txt['cant_merge'] = 'Du har ikke tilgang til å slå sammen emner';
$txt['no_topic_id'] = 'Du skrev inn en ugyldig emne-ID.';
$txt['split_first_post'] = 'Du kan ikke splitte et emne fra det første innlegget.';
$txt['topic_one_post'] = 'Dette emnet inneholder kun ett innlegg og kan ikke splittes';
$txt['no_posts_selected'] = 'ingen innlegg ble valgt';
$txt['selected_all_posts'] = 'Kan ikke splitte emnet. Du har valgt alle innleggene.';
$txt['cant_find_messages'] = 'Kunne ikke finne innlegg';
$txt['cant_find_user_email'] = 'Kan ikke finne brukerens e-postadresse.';
$txt['cant_insert_topic'] = 'Kunne ikke sette inn emnet';
$txt['already_a_mod'] = 'Du har valgt et brukernavn som brukes av en eksisterende moderator. Velg et annet brukernavn';
$txt['session_timeout'] = 'Din brukersesjon løp ut på tid mens du holdt på. Vennligst logg inn og prøv på nytt.';
$txt['session_verify_fail'] = 'Innloggingskontrollen feilet. Vennligst logg ut, logg inn igjen, og prøv på nytt.';
$txt['verify_url_fail'] = 'Kunne ikke bekrefte gyldighet for referansesiden. Vennligst gå tilbake og prøv på nytt.';
$txt['guest_vote_disabled'] = 'Gjester kan ikke stemme i denne avstemningen.';

$txt['cannot_access_mod_center'] = 'Du har ikke adgang til moderatorsenteret.';
$txt['cannot_admin_forum'] = 'Du har ikke tilgang til å administrere forumet.';
$txt['cannot_announce_topic'] = 'Du har ikke tilgang til å kunngjøre emner i dette forumet.';
$txt['cannot_approve_posts'] = 'Du har ikke tillatelse til å godkjenne elementer.';
$txt['cannot_post_unapproved_attachments'] = 'Du har ikke tillatelse til å poste vedlegg som ikke er godkjent.';
$txt['cannot_post_unapproved_topics'] = 'Du har ikke tillatelse til å poste emner som ikke er godkjent.';
$txt['cannot_post_unapproved_replies_own'] = 'Du har ikke tillatelse til å poste svar til dine emner som ikke er godkjent.';
$txt['cannot_post_unapproved_replies_any'] = 'Du har ikke tillatelse til å poste svar til andre brukeres emner som ikke er godkjent.';
$txt['cannot_calendar_edit_any'] = 'Du kan ikke redigere kalender-hendelse.';
$txt['cannot_calendar_edit_own'] = 'Du har ikke nødvendige rettigheter til å redigere dine egne hendelser.';
$txt['cannot_calendar_post'] = 'Beklager, men det går ikke an å opprette hendelser.';
$txt['cannot_calendar_view'] = 'Beklager, men du har ikke tilgang til å vise kalenderen.';
$txt['cannot_remove_any'] = 'Beklager, men du har ikke rettigheter til å fjerne emner.';
$txt['cannot_remove_own'] = 'Beklager, men du kan ikke fjerne dine egne emner i dette forumet.';
$txt['cannot_edit_news'] = 'Du har ikke tilgang til å redigere nyhetene på dette forumet.';
$txt['cannot_pm_read'] = 'Beklager, men du kan ikke lese dine personlige meldinger.';
$txt['cannot_pm_send'] = 'Beklager, men du har ikke tilgang til å sende personlige meldinger.';
$txt['cannot_karma_edit'] = 'Du har ikke tilgang til å redigere andre medlemmers karma.';
$txt['cannot_lock_any'] = 'Du har ikke tilgang til å stenge emner her.';
$txt['cannot_lock_own'] = 'Beklager, men du har ikke tilgang til å stenge dine egne emner her.';
$txt['cannot_make_sticky'] = 'Du har ikke tilgang til å gjøre dette emnet prioritert.';
$txt['cannot_manage_attachments'] = 'Du har ikke tilgang til å administrere vedlegg eller avatarer.';
$txt['cannot_manage_bans'] = 'Du har ikke tilgang til å forandre lista over utestengte medlemmer.';
$txt['cannot_manage_boards'] = 'Du har ikke tilgang til å administrere forum og kategorier.';
$txt['cannot_manage_membergroups'] = 'Du har ikke tilgang til å redigere eller tildele medlemsgrupper.';
$txt['cannot_manage_permissions'] = 'Du har ikke tilgang til å administrere rettigheter.';
$txt['cannot_manage_smileys'] = 'Du har ikke tilgang til å administrere smilefjes.';
$txt['cannot_mark_any_notify'] = 'Du har ikke den nødvendige tilgang til å motta varsling på dette emnet.';
$txt['cannot_mark_notify'] = 'Beklager, men du har ikke tilgang til å motta varsling fra denne seksjonen.';
$txt['cannot_merge_any'] = 'Du har ikke tilgang til å slå sammen emner i ett av de valgte forumene.';
$txt['cannot_moderate_forum'] = 'Du har ikke tilgang til å moderere dette forumet.';
$txt['cannot_moderate_board'] = 'Du har ikke tilgang til å moderere dette forumet';
$txt['cannot_modify_any'] = 'Du har ikke tilgang til å redigere innlegg.';
$txt['cannot_modify_own'] = 'Beklager, men du har ikke tilgang til å redigere dine egne innlegg.';
$txt['cannot_modify_replies'] = 'Selv om det er et svar til ditt emne, kan du ikke redigere det.';
$txt['cannot_move_own'] = 'Du har ikke tilgang til å flytte dine egne emner i dette forumet.';
$txt['cannot_move_any'] = 'Du har ikke tilgang til å flytte emner i dette forumet.';
$txt['cannot_poll_add_own'] = 'Beklager, men du har ikke tilgang til å legge til avstemning til dine egne emner i dette forumet.';
$txt['cannot_poll_add_any'] = 'Du har ikke tilgang til å legge til avstemning til dette emnet.';
$txt['cannot_poll_edit_own'] = 'Du kan ikke redigere denne avstemninga, selv om det er din egen.';
$txt['cannot_poll_edit_any'] = 'Du har blitt nektet tilgang til å redigere avstemninger i dette forumet.';
$txt['cannot_poll_lock_own'] = 'Du har ikke tilgang til å stenge dine egne avstemninger i dette forumet.';
$txt['cannot_poll_lock_any'] = 'Beklager, men du har ikke tilgang til å stenge avstemninger.';
$txt['cannot_poll_post'] = 'Du har ikke tilgang til å lage avstemninger i denne seksjonen.';
$txt['cannot_poll_remove_own'] = 'Du har ikke tilgang til å fjerne avstemningen fra ditt emne.';
$txt['cannot_poll_remove_any'] = 'Du har ikke tilgang til å fjerne avstemninger i dette forumet.';
$txt['cannot_poll_view'] = 'Du har ikke tilgang til å vise avstemninger i dette forumet.';
$txt['cannot_poll_vote'] = 'Beklager, men du kan ikke stemme i avstemninger i dette forumet.';
$txt['cannot_post_attachment'] = 'Du har ikke tilgang til å legge til vedlegg her.';
$txt['cannot_post_new'] = 'Beklager, men du kan ikke lage nye emner i dette forumet.';
$txt['cannot_post_reply_any'] = 'Du har ikke tilgang til å skrive svar på emner i dette forumet.';
$txt['cannot_post_reply_own'] = 'Du har ikke tilgang til å lage svar til dine egne emner i dette forumet.';
$txt['cannot_profile_remove_own'] = 'Beklager, men du har ikke tilgang til å slette din egen medlemskonto.';
$txt['cannot_profile_remove_any'] = 'Du har ikke tilgang til å slette medlemskonto for andre medlemmer!';
$txt['cannot_profile_extra_any'] = 'Beklager, men du har ikke de nødvendige rettighetene til å redigere din egen profil.';
$txt['cannot_profile_identity_any'] = 'Du har ikke tilgang til å redigere kontoinnstillinger.';
$txt['cannot_profile_title_any'] = 'Du kan ikke redigere medlemmers egendefinerte titler.';
$txt['cannot_profile_extra_own'] = 'Beklager , men du har ikke de nødvendige rettighetene til å redigere data i din profil.';
$txt['cannot_profile_identity_own'] = 'Du kan ikke endre din identitet på det nåværende tidspunkt.';
$txt['cannot_profile_title_own'] = 'Du har ikke tilgang til å endre din egendefinerte tittel.';
$txt['cannot_profile_server_avatar'] = 'Du har ikke tilgang til å velge en lokal avatar.';
$txt['cannot_profile_upload_avatar'] = 'Du har ikke tilgang til å laste opp en avatar.';
$txt['cannot_profile_remote_avatar'] = 'Du har ikke tilgang til å bruke en egendefinert avatar.';
$txt['cannot_profile_view_own'] = 'Beklager så mye, men du kan ikke vise din egen profil.';
$txt['cannot_profile_view_any'] = 'Beklager så mye, men du kan ikke vise profiler.';
$txt['cannot_delete_own'] = 'Du har ikke tilgang til å slette dine egne innlegg i dette forumet.';
$txt['cannot_delete_replies'] = 'Beklager, men du kan ikke fjerne disse innleggene, selv om de er svar på ditt eget emne.';
$txt['cannot_delete_any'] = 'Du kan ikke slette vilkårlige innlegg i dette forumet.';
$txt['cannot_report_any'] = 'Du har ikke tilgang til å rapportere innlegg i dette forumet.';
$txt['cannot_search_posts'] = 'Beklager, men du kan ikke søke etter innlegg i dette forumet.';
$txt['cannot_send_mail'] = 'Du har ikke de nødvendige rettighetene til å sende ut e-post til alle.';
$txt['cannot_issue_warning'] = 'Beklager, du har ikke de nødvendige rettighetene til å utstede advarsler til medlemmer.';
$txt['cannot_send_topic'] = 'Beklager, administratoren har deaktivert at du kan sende emner i dette forumet.';
$txt['cannot_split_any'] = 'Å dele opp emner er ikke tillatt på dette forumet.';
$txt['cannot_view_attachments'] = 'Det virker som du ikke har tilgang til å laste ned eller vise vedlegg i dette forumet.';
$txt['cannot_view_mlist'] = 'Du kan ikke vise medlemslista fordi du har ikke tilgang til det.';
$txt['cannot_view_stats'] = 'Du har ikke tilgang til å vise forumstatistikk.';
$txt['cannot_who_view'] = 'Beklager, du har ikke de nødvendige rettighetene til å vise hvem som er pålogget.';

$txt['no_theme'] = 'Det designet eksisterer ikke.';
$txt['theme_dir_wrong'] = 'Det er noe feil ved mappen til standarddesignet, vennligst klikk denne teksten for å korrigere.';
$txt['registration_disabled'] = 'Beklager, men registrering er for øyeblikket deaktivert.';
$txt['registration_no_secret_question'] = 'Beklager , det er ikke satt opp noe hemmelig spørsmål for dette medlemmet.';
$txt['poll_range_error'] = 'Beklager, men avstemningen må vare mer enn 0 dager.';
$txt['delFirstPost'] = 'Du har ikke tilgang til å slette første innlegget i et emnet.<p>Dersom du vil slette emnet, klikk på <i>Slett emnet</i> eller be en administrator/moderator gjøre det for deg.</p>';
$txt['parent_error'] = 'Kunne ikke opprette forumet!';
$txt['login_cookie_error'] = 'Du kunne ikke logge inn. Kontroller dine informasjonskapsel-innstillinger (cookies).';
$txt['incorrect_answer'] = 'Beklager, men du svarte ikke korrekt på spørsmålet. Klikk på tilbakeknappen og prøv på nytt, eller klikk tilbake to ganger for å bruke den vanlige metoden for å få tilbake ditt passord.';
$txt['no_mods'] = 'Fant ingen moderatorer!';
$txt['parent_not_found'] = 'Feil i forumstrukturen: Kunne ikke finne overliggende forum';
$txt['modify_post_time_passed'] = 'Du kan ikke redigere dette innlegget fordi tidsfristen for redigering er utløpt.';

$txt['calendar_off'] = 'Du kan ikke gå inn på kalenderen fordi den er deaktivert.';
$txt['invalid_month'] = 'Ugyldig verdi i måned.';
$txt['invalid_year'] = 'Ugyldig verdi i årstall.';
$txt['invalid_day'] = 'Ugyldig dagenz verdi.';
$txt['event_month_missing'] = 'Måneden for hendelsen mangler.';
$txt['event_year_missing'] = 'Året for hendelsen mangler.';
$txt['event_day_missing'] = 'Dagen for hendelsen mangler.';
$txt['event_title_missing'] = 'Tittelen på hendelsen mangler.';
$txt['invalid_date'] = 'Ugyldig dato.';
$txt['no_event_title'] = 'Ingen tittel på hendelsen ble skrevet inn.';
$txt['missing_event_id'] = 'Mangler ID på hendelse.';
$txt['cant_edit_event'] = 'Du har ikke tilgang til å redigere denne hendelsen.';
$txt['missing_board_id'] = 'ID på forumet mangler.';
$txt['missing_topic_id'] = 'ID på emnet mangler.';
$txt['topic_doesnt_exist'] = 'Emnet eksisterer ikke.';
$txt['not_your_topic'] = 'Det er ikke du som har startet dette emnet.';
$txt['board_doesnt_exist'] = 'Det forumet eksisterer ikke.';
$txt['no_span'] = 'En hendelse kan ikke strekke seg over flere dager.';
$txt['invalid_days_numb'] = 'Ugyldig antall dager for hendelsen.';

$txt['moveto_noboards'] = 'Det finnes ingen forum du kan flytte emnet til!';

$txt['already_activated'] = 'Din medlemskonto er allerede aktivert.';
$txt['still_awaiting_approval'] = 'Din medlemskonto venter fremdeles på godkjennelse fra administrator.';

$txt['invalid_email'] = 'Ugyldig e-postadresse / e-postområde.<br />Eksempel på en gyldig adresse: slem.slemmesen@uskikkelig.com.<br />Eksempel på et gyldig e-postområde: *@*.uskikkelig.com';
$txt['invalid_expiration_date'] = 'Utløpsdatoen er ugyldig';
$txt['invalid_hostname'] = 'Ugyldig vert / vertområde.<br />Eksempel på en gyldig vert: proxy4.uskikkelig.com<br />Eksempel på et gyldig vertområde: *.uskikkelig.com';
$txt['invalid_ip'] = 'Ugyldig IP-adresse / IP-område.<br />Eksempel på en gyldig IP-adresse: 127.0.0.1<br />Eksempel på gyldig IP-område: 127.0.0-20.*';
$txt['invalid_tracking_ip'] = 'Ugyldig IP-adresse / IP-område.<br />Eksempel på en gyldig IP-adresse: 127.0.0.1<br />Eksempel på gyldig IP-område: 127.0.0.*';
$txt['invalid_username'] = 'Medlemsnavnet ble ikke funnet';
$txt['no_ban_admin'] = 'Du kan ikke utestenge administratorer. Du må først frata dem admin-status!';
$txt['no_bantype_selected'] = 'Ingen utestengingstype ble valgt';
$txt['ban_not_found'] = 'Utestenging ble ikke funnet';
$txt['ban_unknown_restriction_type'] = 'Type utestenging er ukjent';
$txt['ban_name_empty'] = 'Navnet på utestengningen er tom';
$txt['ban_name_exists'] = 'Navnet på denne utestengningen  (%1$s) finnes allerede. Vennligst velg et annet navn.';
$txt['ban_trigger_already_exists'] = 'Denne utestengningstrigger (%1$s) finnes allerede i %2$s.';

$txt['recycle_no_valid_board'] = 'Det er ikke valgt et gyldig forum som papirkurv for emner';

$txt['login_threshold_fail'] = 'Beklager, du har brukt opp dine innloggingsforsøk. Vennligst prøv igjen senere.';
$txt['login_threshold_brute_fail'] = 'Beklager, men du har nådd grensen for antall innloggingsforsøk.  Vent 30 sekunder og prøv igjen senere.';

$txt['who_off'] = 'Du kan ikke vise hvem som er pålogget fordi funksjonen er deaktivert.';

$txt['merge_create_topic_failed'] = 'Kunne ikke lage nytt emne.';
$txt['merge_need_more_topics'] = 'For å slå sammen emner, må du ha valgt minst to emner.';

$txt['postWaitTime_broken'] = 'Det siste innlegget fra din IP var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';
$txt['registerWaitTime_broken'] = 'Du registrerte deg jo allerede for %1$d sekunder siden!';
$txt['loginWaitTime_broken'] = 'Du må vente cirka %1$d sekunder før du kan logge inn igjen, beklager.';
$txt['pmWaitTime_broken'] = 'Den siste personlig meldingen fra din IP var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';
$txt['reporttmWaitTime_broken'] = 'Det siste emne skrevet fra din IP var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';
$txt['sendtopcWaitTime_broken'] = 'Det siste innlegget som sendtes fra din IP var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';
$txt['sendmailWaitTime_broken'] = 'Den siste e-post sendt fra din IP var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';
$txt['searchWaitTime_broken'] = 'Din siste søk var mindre enn %1$d sekunder siden. Vennligst prøv igjen senere.';

$txt['email_missing_data'] = 'Du må skrive noe i både emnefeltet og meldingsboksen.';

$txt['topic_gone'] = 'Emnet eller forumet du leter etter er enten borte eller du har ikke tilgang til det.';
$txt['theme_edit_missing'] = 'Filen du prøver å redigere... finner vi ikke!';

$txt['attachments_no_write'] = 'Lagringsområdet for opplastinger er ikke skrivbart. Ditt vedlegg eller din avatar kan ikke lagres.';
$txt['attachments_limit_per_post'] = 'Du kan ikke laste opp mer enn %1$d vedlegg per innlegg';

$txt['no_dump_database'] = 'Bare administratorer kan ta sikkerhetskopi av databasen!';
$txt['pm_not_yours'] = 'Den personlige meldingen du prøver å sitere fra er ikke din egen, eller eksisterer ikke. Gå tilbake og prøv på nytt.';
$txt['mangled_post'] = 'En feil oppstod under behanding av inndata, vennligst gå tilbake og prøv på nytt.';
$txt['quoted_post_deleted'] = 'Innlegget du prøver å hente et sitat fra, eksisterer ikke, ble slettet eller er ikke lenger tilgjengelig for deg.';
$txt['pm_too_many_per_hour'] = 'Du har overskredet grensen på %1$d personlige meldinger per time.';
$txt['labels_too_many'] = 'Beklager, men %1$s meldinger har allerede det maksimale antall etiketter som er tillatt!';

$txt['register_only_once'] = 'Beklager, men du kan ikke registrere flere medlemskontoer samtidig fra samme PC.';
$txt['admin_setting_coppa_require_contact'] = 'Du må enten angi en postadresse eller ett faksnummer hvis det kreves godkjennelse av foreldre/foresatte.';

$txt['error_long_name'] = 'Navnet du prøvde å bruke var for langt.';
$txt['error_no_name'] = 'Du sendte ikke med noe navn.';
$txt['error_bad_name'] = 'Navnet du sendte med kan ikke brukes, fordi det er identisk med eller inneholder deler av et reservert navn.';
$txt['error_no_email'] = 'Du sendte ikke med noen epost-adresse.';
$txt['error_bad_email'] = 'Epost-adressen du sendte med er ugyldig.';
$txt['error_no_event'] = 'Du sendte ikke med noe navn på hendelsen.';
$txt['error_no_subject'] = 'Emne ble ikke fylt ut.';
$txt['error_no_question'] = 'Ingen spørsmål er fylt inn for denne avstemningen.';
$txt['error_no_message'] = 'Meldingen inneholder ingen tekst.';
$txt['error_long_message'] = 'Din tekst er lengre enn maksimum tillatt lengde (%1$d tegn ).';
$txt['error_no_comment'] = 'Kommentarfeltet er tomt.';
$txt['error_session_timeout'] = 'Du ble automatisk logget ut i mellomtiden. Prøv å sende på nytt.';
$txt['error_no_to'] = 'Du har ikke spesifisert noen mottakere.';
$txt['error_bad_to'] = 'En eller flere mottakere i "til"-feltet ble ikke funnet.';
$txt['error_bad_bcc'] = 'En eller flere mottakere i "bcc"-feltet ble ikke funnet.';
$txt['error_form_already_submitted'] = 'Du har allerede sendt dette innlegget! Du har kanskje dobbletklikket, eller prøvd å oppdatere siden.';
$txt['error_poll_few'] = 'Du må ha minst to valg-muligheter!';
$txt['error_need_qr_verification'] = 'Vennligst fyll ut bekreftelsesfeltet nedenfor for å fullføre innlegget.';
$txt['error_wrong_verification_code'] = 'Bokstavene du skrev inn stemmer ikke med bokstavene som ble vist i bildet.';
$txt['error_wrong_verification_answer'] = 'Du svarte ikke riktig på verifiseringsspørsmålene.';
$txt['error_need_verification_code'] = 'Vennligst skriv inn bekreftelseskoden nedenfor for å fortsette til resultatene.';
$txt['error_bad_file'] = 'Beklager men den angitte filen kunne ikke åpnes: %1$s';
$txt['error_bad_line'] = 'Linjen som er angitt er ugyldig.';

$txt['smiley_not_found'] = 'Smilefjes ble ikke funnet.';
$txt['smiley_has_no_code'] = 'Det ble ikke gitt noen kode for dette smilefjeset.';
$txt['smiley_has_no_filename'] = 'Det ble ikke gitt noe filnavn for dette smilefjeset.';
$txt['smiley_not_unique'] = 'Et smilefjes med det navnet finnes allerede.';
$txt['smiley_set_already_exists'] = 'Et smilefjes med den adressen finnes allerede';
$txt['smiley_set_not_found'] = 'Smilefjes-sett ble ikke funnet';
$txt['smiley_set_path_already_used'] = 'Adressen til smilefjes-settet er allerede brukt av et annet sett.';
$txt['smiley_set_unable_to_import'] = 'Kan ikke importere smilefjes-sett. Mappen er enten ugyldig eller ikke tilgjengelig.';

$txt['smileys_upload_error'] = 'Opplasting av fil mislyktes.';
$txt['smileys_upload_error_blank'] = 'Alle smilefjes-sett må ha et bilde!';
$txt['smileys_upload_error_name'] = 'Alle smilefjes må ha det samme filnavnet!';
$txt['smileys_upload_error_illegal'] = 'Ikke tillatt format.';

$txt['search_invalid_weights'] = 'Vekting av søk er ikke konfigurert korrekt. Vennligst rapporter denne feilen til en administrator.';
$txt['unable_to_create_temporary'] = 'Søkefunksjonen kunne ikke opprette midlertidige tabeller. Vennligst prøv igjen eller rapporter denne feilen til en administrator.';

$txt['package_no_file'] = 'Kunne ikke finne filen!';
$txt['packageget_unable'] = 'Kunne ikke koble til serveren. Prøv <a href="%1$s" target="_blank" class="new_win">denne nettadressen</a> istedet.';
$txt['not_on_simplemachines'] = 'Beklager, men pakkene kan kun lastes ned fra simplemachines.org sin server.';
$txt['package_cant_uninstall'] = 'Denne pakka er enten aldri blitt innstallert eller er avinnstallert allerede - du kan ikke avinnstallere den nå.';
$txt['package_cant_download'] = 'Du kan ikke laste ned eller innstallere nye pakker fordi mappe for nedlastede pakker er skrivebeskyttet!';
$txt['package_upload_error_nofile'] = 'Du har ikke valgt en pakke å laste opp.';
$txt['package_upload_error_failed'] = 'Kunne ikke laste opp pakken, sjekk mapperettighetene!';
$txt['package_upload_error_exists'] = 'Filen du prøver å laste opp finnes allerede. Vennligst slett den først og deretter prøv igjen.';
$txt['package_upload_error_supports'] = 'Pakkebehandleren tillater foreløpig kun disse filtypene: %1$s.';
$txt['package_upload_error_broken'] = 'Opplasting av pakken mislyktes på grunn av følgende feil:<br />&quot;%1$s&quot;';

$txt['package_get_error_not_found'] = 'Finner ikke pakken du prøver å installere. Du kan prøve manuelt laste opp pakken til mappen Packages.';
$txt['package_get_error_missing_xml'] = 'Pakken du prøver å installere mangler filen package-info.xml, som må være i rotmappen i pakken.';
$txt['package_get_error_is_zero'] = 'Selv om pakken ble lastet ned til serveren synes den å være tom. Vennligst sjekk at begge mappene Packages og undermappen &quot;temp&quot; er skrivbar. Hvis du fortsatt opplever dette problemet bør du prøve å pakke ut pakken på din PC og laste de utpakkede filene til en undermappe i mappen Packages og prøv igjen. For eksempel, hvis pakken heter shout.tar.gz bør du:<br />1) Last ned pakken til din lokale PC og pakk ut filene.<br />2) Bruk en FTP-klient og opprett en ny mappe i mappen &quot;Packages&quot; i dette eksemplet skal mappen hete "shout".<br />3) Last opp alle filene fra den utpakkede pakken til denne mappen.<br />4) Gå tilbake til pakkebehandleren og pakken blir automatisk funnet av Protendo.';
$txt['package_get_error_packageinfo_corrupt'] = 'Protendo was unable to find any valid information within the package-info.xml file included within the Package. There may be an error with the modification, or the package may be corrupt.';

$txt['no_membergroup_selected'] = 'Ingen medlemsgruppe er valgt';
$txt['membergroup_does_not_exist'] = 'Medlemsgruppa finnes ikke eller er ugyldig.';

$txt['at_least_one_admin'] = 'Det må være minst én administrator på Forumet!';

$txt['error_functionality_not_windows'] = 'Beklager, denne funksjonaliteten er for øyeblikket ikke tilgjengelig for servere som kjører Windows.';

// Don't use entities in the below string.
$txt['attachment_not_found'] = 'Vedlegget ble ikke funnet';

$txt['error_no_boards_selected'] = 'Ingen gyldige forum ble valgt!';
$txt['error_invalid_search_string'] = 'Glemte du å skrive inn noe å søke etter?';
$txt['error_invalid_search_string_blacklist'] = 'Søket inneholdt for trivielt ord. Vennligst prøv igjen med en annen spørring.';
$txt['error_search_string_small_words'] = 'Hvert ord må være minst to tegn.';
$txt['error_query_not_specific_enough'] = 'Søket var ikke spesifikt nok. Forsøk med litt lengre søkeord.';
$txt['error_no_messages_in_time_frame'] = 'Fant ingen innlegg i valgt tidsrom.';
$txt['error_no_labels_selected'] = 'Ingen merkelapper ble valgt!';
$txt['error_no_search_daemon'] = 'Kan ikke få tilgang til søkefunksjon';

$txt['profile_errors_occurred'] = 'Følgende feil oppsto ved forsøk på å lagre profilen din';
$txt['profile_error_bad_offset'] = 'Tidsavvik er utenfor gyldig område';
$txt['profile_error_no_name'] = 'Navnefeltet var tomt';
$txt['profile_error_name_taken'] = 'Valgt brukernavn er allerede i bruk';
$txt['profile_error_name_too_long'] = 'Det valgte navnet er for langt. Det bør ikke være større enn 60 tegn langt';
$txt['profile_error_no_email'] = 'E-postfeltet var tomt';
$txt['profile_error_bad_email'] = 'Du har ikke skrevet en gyldig e-postadresse';
$txt['profile_error_email_taken'] = 'En annen bruker er allerede registrert med den e-postadressen';
$txt['profile_error_no_password'] = 'Du skrev ikke inn passordet ditt';
$txt['profile_error_bad_new_password'] = 'Passordene du skrev inn var ikke like';
$txt['profile_error_bad_password'] = 'Passordet du skrev inn var ikke korrekt';
$txt['profile_error_bad_avatar'] = 'Avataren du har valgt er for stor, eller er ikke en avatar';
$txt['profile_error_password_short'] = 'Passordet ditt må være minst ' . (empty($modSettings['password_strength']) ? 4 : 8) . ' tegn langt.';
$txt['profile_error_password_restricted_words'] = 'Passordet ditt må ikke inneholde ditt brukernavn, e-postadresse eller andre vanlige begreper.';
$txt['profile_error_password_chars'] = 'Passordet ditt må inneholde en blanding av store og små bokstaver og tall.';
$txt['profile_error_already_requested_group'] = 'Du har allerede en utestående forespørsel til denne gruppen!';
$txt['profile_error_openid_in_use'] = 'Det er allerede en annen bruker med denne OpenID-autentiseringen';

$txt['mysql_error_space'] = ' - sjekk tilgjengelig lagringsplass i databasen, eller kontakt server-administrator.';

$txt['icon_not_found'] = 'Symbol-bildet ble ikke funnet i standarddesignet. Forsikre deg om at bildet er lastet opp, og prøv på nytt.';
$txt['icon_after_itself'] = 'Symbolet kan ikke plasseres etter seg selv!';
$txt['icon_name_too_long'] = 'Filnavnene for ikonene kan ikke inneholde mer enn 16 tegn';

$txt['name_censored'] = 'Beklager, navnet du forsøkte å benytte, %1$s, inneholder ord som er blitt sensurert. Vennligst prøv et annet navn.';

$txt['poll_already_exists'] = 'Et emne kan kun ha tilknyttet én avstemning !';
$txt['poll_not_found'] = 'Det er ingen avsteming tilknyttet dette emnet!';

$txt['error_while_adding_poll'] = 'Følgende feil oppsto da avstemningen ble forsøkt lagt til';
$txt['error_while_editing_poll'] = 'Følgende feil oppsto da avstemningen ble forsøkt redigert';

$txt['loadavg_search_disabled'] = 'På grunn av stor belastning på serveren er søkefunksjonen midlertidig utilgjengelig.';
$txt['loadavg_generic_disabled'] = 'Beklager, på grunn av stor belastning på serveren er denne funksjonen midlertidig utilgjengelig.';
$txt['loadavg_allunread_disabled'] = 'Det er for øyeblikket høyt belastning på serveren, så den finner ikke alle de emnene du ikke har lest.';
$txt['loadavg_unreadreplies_disabled'] = 'Serveren er for tiden høyt belastet. Vennligst prøv igjen om kort tid.';
$txt['loadavg_show_posts_disabled'] = 'Dette medlemmets innlegg er ikke tilgjengelig på grunn av stor belastning på serveren. Vennligst prøv igjen om kort tid.';
$txt['loadavg_unread_disabled'] = 'Det er for øyeblikket høyt belastning på serveren, så den klarer ikke å liste alle emnene du ikke har lest.';

$txt['cannot_edit_permissions_inherited'] = 'Du kan ikke redigere en arvet rettighet direkte, du må enten redigere den overordnede gruppen eller redigere medlemsgruppen arvegang.';

$txt['mc_no_modreport_specified'] = 'Du må spesifisere hvilken rapport du vil vise.';
$txt['mc_no_modreport_found'] = 'Den angitte rapporten eksisterer enten ikke eller er ikke tilgjengelig for deg';

$txt['st_cannot_retrieve_file'] = 'Kunne ikke hente filen %1$s';
$txt['admin_file_not_found'] = 'Kunne ikke laste den forespurte filen: %1$s.';

$txt['themes_none_selectable'] = 'Minst ett tema må kunne velges.';
$txt['themes_default_selectable'] = 'Forumets standardtema må være et valgbart tema.';
$txt['ignoreboards_disallowed'] = 'Alternativet til å ignorere kategorier har ikke blitt aktivert.';

$txt['mboards_delete_error'] = 'Ingen kategori er valgt!';
$txt['mboards_delete_board_error'] = 'Ingen Forum er valgt!';

$txt['mboards_parent_own_child_error'] = 'Kan ikke være overordnet seg selv!';
$txt['mboards_board_own_child_error'] = 'Kan ikke lage et forum på sitt eget underforum!';

$txt['smileys_upload_error_notwritable'] = 'Følgende smilefjesmapper er ikke skrivbare: %1$s';
$txt['smileys_upload_error_types'] = 'Smilefjesbilder kan kun være følgende filtyper: %1$s.';

$txt['change_email_success'] = 'Din e-postadresse har blitt endret, og en ny e-post for aktivering har blitt sendt til den.';
$txt['resend_email_success'] = 'En ny e-post med aktivering er blitt sendt.';

$txt['custom_option_need_name'] = 'Profilalternativet må ha et navn!';
$txt['custom_option_not_unique'] = 'Feltnavnet er ikke unikt!';

$txt['warning_no_reason'] = 'Du må oppgi en grunn for å endre status på advarselen til et medlem.';
$txt['warning_notify_blank'] = 'Du valgte å varsle brukeren, men har ikke fylt ut emne/meldingsfeltet.';

$txt['cannot_connect_doc_site'] = 'Kunne ikke koble til Simple Machines Online brukehåndbok. Vennligst sjekk at din serverkonfigurasjon tillater eksterne tilkoblinger og prøv igjen senere.';

$txt['movetopic_no_reason'] = 'Du må oppgi en grunn for å flytte emnet, eller må du fjern muligheten for å flytte emner';

// OpenID error strings
$txt['openid_server_bad_response'] = 'Den forespurte identifikator kom ikke tilbake med riktig informasjon.';
$txt['openid_return_no_mode'] = 'Identitetsleverandøren svarte ikke med Open ID-modus.';
$txt['openid_not_resolved'] = 'Identitetsleverandøren godkjenner ikke din forespørsel.';
$txt['openid_no_assoc'] = 'Kunne ikke finne den forespurte tilknytningen til identitetsleverandøren.';
$txt['openid_sig_invalid'] = 'Signaturen fra identitetsleverandøren er ugyldig.';
$txt['openid_load_data'] = 'Kunne ikke laste inn data fra dine innloggingsforespørsel. Vennligst prøv igjen.';
$txt['openid_not_verified'] = 'OpenID adressen er ennå ikke bekreftet, vennligst logg på for å bekrefte.';

$txt['error_custom_field_too_long'] = '&quot;%1$s&quot; feltet kan ikke være større enn %2$d tegn.';
$txt['error_custom_field_invalid_email'] = '&quot;%1$s&quot; feltet må være en gyldig e-postadresse.';
$txt['error_custom_field_not_number'] = '&quot;%1$s&quot; feltet må være numerisk.';
$txt['error_custom_field_inproper_format'] = '&quot;%1$s&quot; feltet er et ugyldig format.';
$txt['error_custom_field_empty'] = '&quot;%1$s&quot; feltet kan ikke være tomt.';

$txt['email_no_template'] = 'Malen for e-post &quot;%1$s&quot; ble ikke funnet.';

$txt['search_api_missing'] = 'Søke API ble ikke funnet! Vennligst kontakt administratoren for å kontrollere at de har lastet de riktige filene.';
$txt['search_api_not_compatible'] = 'Det valgte søke API forumet bruker er utdatert - går tilbake til standard søk. Sjekk filene %1$s.';

// Restore topic/posts
$txt['cannot_restore_first_post'] = 'Du kan ikke gjenopprette det første innlegget i et emne.';
$txt['parent_topic_missing'] = 'Den overordnede temaet for innlegget du prøver å gjenopprette har blitt slettet.';
$txt['restored_disabled'] = 'Gjenoppretting av emner har blitt deaktivert.';
$txt['restore_not_found'] = 'Følgende meldinger kan ikke gjenopprettes, det opprinnelige temaet kan ha blitt fjernet:<ul style="margin-top: 0px;">%1$s</ul>Du må flytte disse manuelt.';

$txt['error_invalid_dir'] = 'Mappen du skrev inn er ugyldig.';

$txt['error_sqlite_optimizing'] = 'Sqlite optimaliserer databasen, Forumet kan ikke brukes før den er ferdig. Vennligst prøv å laste siden på nytt om ett øyeblikk.';
$txt['error_why_register_pattern'] = 'Your response on why you want to register on this site was too short - or you used a well known spamming phrase. <br>Please rephrase to register successfully.';

?>