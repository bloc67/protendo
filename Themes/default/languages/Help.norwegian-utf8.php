<?php
// Version: 2.0; Help

global $helptxt;

$txt['close_window'] = 'Lukk vinduet';

$helptxt['manage_boards'] = '	<strong>Forumoversikt</strong><br />
	På denne siden kan du opprette/sortere/fjerne fora og tilhørende
	kategorier. For eksempel, om du har en side som dekker flere felter, som informasjon innenfor
	sport, biler og musikkm ville dette vært kategoriene du hadde opprettet. Under disse
	kategoriene ville du ha opprettet &quot;underkategorier&quot; eller &quot;fora&quot;
	for emenene under hver del. Det er et enkelt hierarki, med følgende oppsett: <br />
	<ul class="normallist">
		<li>
			<strong>Sport</strong>
			&nbsp;- En kategori
		</li>
		<ul class="normallist">
			<li>
				<strong>Vinteridrett</strong>
				&nbsp;- Et forum under kategorien &quot;Sport&quot;
			</li>
			<ul class="normallist">
				<li>
					<strong>Hopp</strong>
					&nbsp;- Et delforum til forumet &quot;Vinteridrett&quot;
				</li>
			</ul>
			<li><strong>Sommeridrett</strong>
			&nbsp;- Et forum under kategorien &quot;Sport&quot;</li>
		</ul>
	</ul>
	Kategorier lar deg dele inn forumet i hovedemner (&quot;Biler, sport&quot;)
	og &quot;foraene&quot;, i hver kategori, er emner medlemmene kan diskutere i. 
	Et medlem interessert i Volvo, ville da ha laget et emne under &quot;Biler->Volvo&quot;.
	Kategorier lar folk raskt og enkelt finne hvor de har mest interesse av å snakke om: 
	I steden for bare en &quot;butikk&quot; har du både &quot;elektro-&quot; og
	&quot;klesbutikker&quot; du kan gå i. Dette gjør det enkelt når du leter
	etter et plasma-TV, for da kan du gå inn i elektrobutikk-&quot;kategorien&quot;
	isteden for klesbutikken (der det ikke akkurat er store sjansen for at du får kjøpt
	et plasma-TV).<br />
	Som nevnt ovenfor, er en seksjon et nøkkelemne under verdt hovedemne.
	Dersom du vil diskutere &quot;Volvo&quot;, vil du da gå inn under kategorien &quot;Biler&quot;
	og inn i forumet &quot;Volvo&quot; for å skrive dine meninger i det forumet.<br />
	Administrative funksjoner på denne siden, er å opprette nye fora under hver
	kategori, stokke om på dem (sette &quot;Volvo&quot; under &quot;Fiat&quot;) eller å
	fjerne et forum fullstendig.';

$helptxt['edit_news'] = '	<ul class="normallist">
		<li>
			<strong>Nyheter</strong><br />
			Dette lar deg skrive inn en tekst for nyhetselementer synlige på forumets hovedside. 
			Legg til hva du vil (f.eks. &quot;Ikke glem kampen på søndag!&quot;). Hver nyhetssak skal i hver sin boks, og de vises i tilfeldig rekkefølge.\';
		</li>
		<li>
			<strong>Nyhetsbrev</strong><br />
			Denne delen lar deg sende ut nyhetsbrev til medlemmene av forumet via personlig melding eller e-post. Først velger du gruppene som du ønsker skal motta nyhetsbrevet, og de du ikke ønsker at skal motta nyhetsbrevet. Hvis du ønsker, kan du legge til flere medlemmer og e-postadresser som skal motta nyhetsbrev. Til slutt setter du inn meldingen du vil sende, og velg om du vil at den skal sendt til medlemmene som en personlig melding eller en e-post.
		</li>
		<li>
			<strong>Innstillinger</strong><br />
				Denne delen inneholder noen få innstillinger som er relatert til nyheter og nyhetsbrev, inkludert å velge hvilke grupper kan redigere forumnyheter eller send nyhetsbrev. Det er også hvor innstilling for å konfigurere om du vil at nyhetsmating skal være aktivert på forumet, samt en innstilling for å konfigurere lengde (hvor mange tegn som vises) for hvert nytt innlegg fra en nyhetsmating.
		</li>
	</ul>\';
';

$helptxt['view_members'] = '	<ul class="normallist">
		<li>
			<strong>Vise alle medlemmer</strong><br />
			Viser alle medlemmer på forumet. Du blir presentert for en liste med linker.
			Der du kan klikke på hvert navn for å få mer detaljer om hvert medlem.
			Som administrator kan du endre på opplysningene. Du har full kontroll over
			dine medlemmer, inkludert tilgang til å slette medlemskontoen til hver medlem fra forumet.<br /><br />
		</li>
		<li>
			<strong>Venter på godkjenning</strong><br />
			Denne vises kun om du har aktivert godkjennelse av admin for hver nye registrering. Den som ønsker å opprette en konto på forumet, 
			vil ikke bli fullverdige medlemmer før en administrator har godkjent medlemskapet. Denne delen vil liste opp alle de medlemmene som
			fremdeles venter på godkjenning, sammen med sin e-postadresse og IP-adresse. Du kan velge om du vil godta eller avvise (slette) 
			hvert medlem i lista ved å sette et kryss i boksen ved siden av medlemmet og velge handling basert på rullegardinslisten på bunnen
			av siden. Når du avviser et medlem, kan du velge å slette medlemmet ved å enten gi medlemmet beskjed eller ikke om din avgjørelse.<br /><br />
		</li>
		<li>
			<strong>Venter på aktivering</strong><br />
			Denne delen vises kun om du har aktivert funksjonen med å aktivere medlemskontoene via en link i 
			e-post, eller ved at du som administrator selv aktiverer medlemskapet. Denne delen vil liste opp alle medlemmene som
			ennå ikke har aktivert sin medlemskonto. Fra dette bilde kan du enten godta, avvise eller sende påminnelse 
			til medlemmer som ikke er aktivert. Du kan også informere medlemmet via e-post om din avgjørelse.<br /><br />
		</li>
	</ul>\'';

$helptxt['ban_members'] = '<strong>Steng ute medlemmer</strong><br />
	Protendo gir deg muligheten til å &quot;utestenge&quot; medlemmer, for å unngå folk som har
	brutt regler på forumet, f.eks. spamming, flodding eller annet, til å få tilgang til forumet. 
	Som administrator kan du se hvert medlems IP-adresse i innleggene. I &quot;Utestengte medlemmer&quot; skriver du bare inn IP-adressen, lagrer 
	og de kan ikke lenger skrive på forumet fra den IP\'en.<br /> Du kan også utestenge medlemmer via e-postadressen.';

$helptxt['featuresettings'] = '<strong>Funksjoner og alternativer</strong><br />
	Det er flere funksjoner i denne delen som du kan endre til å passe dine egne preferanser.';

$helptxt['securitysettings'] = '<strong>Sikkerhet og moderering</strong><br />
	Denne seksjonen inneholder innstillinger relatert til sikkerhet og moderering av forumet ditt.';

$helptxt['modsettings'] = '<strong>Modifikasjonsinnstillinger</strong><br />
	Denne delen inneholder de innstillingene som modifikasjonene installerer på forumet ditt.';

$helptxt['number_format'] = '<strong>Standard tallformat</strong><br />
	Du kan bruke denne innstillingen for å justere formatet for tall på forumet. Standardformatet for forumet er:<br />
	<div style="margin-left: 2ex;">1,234.00</div><br />
	Kommategnet \',\' blir brukt som skilletegn for tusen, mens \'.\' brukes for desimaler. Dette må gjerne endres til den norske standarden: 1 234,00, uten tusenskilletegn og med komma for desimaler.';

$helptxt['time_format'] = '<strong>Standard tidsformat</strong><br />
	Du har tilgang til å endre utseendet for dato og tid på forumet. Det er mange bokstaver, men det er et ganske enkelt oppsett.
	Konverteringene følger PHP sin strftime-funksjon og blir beskrevet under (flere detaljer finnes på <a href="http://www.php.net/manual/function.strftime.php" target="_blank" class="new_win">php.net</a>).<br />
	<br />
	Følgende tegn blir gjenkjent som formater: <br />
	<span class="smalltext">
	&nbsp;&nbsp;%a - forkortet navn på ukedag<br />
	&nbsp;&nbsp;%A - fullstendig navn på ukedag<br />
	&nbsp;&nbsp;%b - forkortet navn på måned<br />
	&nbsp;&nbsp;%B - fullstendig navn på måned<br />
	&nbsp;&nbsp;%d - dato i måneden (01 til 31) <br />
	&nbsp;&nbsp;%D<strong>*</strong> - samme som %m/%d/%y <br />
	&nbsp;&nbsp;%e<strong>*</strong> - dato i måneden (1 til 31) <br />
	&nbsp;&nbsp;%H - timevisning med 24-timersklokke (fra 00 til 23) <br />
	&nbsp;&nbsp;%I - timevisning med 12-timersklokke (fra 01 til 12 bør suppleres med %p for am/pm) <br />
	&nbsp;&nbsp;%m - måned med tall (01 til 12) <br />
	&nbsp;&nbsp;%M - minuttvisning <br />
	&nbsp;&nbsp;%p - visning av &quot;am&quot; eller &quot;pm&quot; for 12-timersklokke<br />
	&nbsp;&nbsp;%R<strong>*</strong> - tiden med 24-timersklokke <br />
	&nbsp;&nbsp;%S - sekundervisning <br />
	&nbsp;&nbsp;%T<strong>*</strong> - gjeldende tid, samme som %H:%M:%S <br />
	&nbsp;&nbsp;%y - årstall med to siffer (00 til 99) <br />
	&nbsp;&nbsp;%Y - årstall med fire siffer<br />
	&nbsp;&nbsp;%Z - tidssone, navn eller forkortelse<br />
	&nbsp;&nbsp;%% - gir deg tegnet \'%\' <br />
	<br />
	<em>* Vil ikke fungere på en Windows-basert server.</em></span>';

$helptxt['live_news'] = '<strong>Siste nytt fra Simple Machines</strong><br />
	Denne boksen vil vise siste kunngjøringer fra <a href="http://www.simplemachines.org/" target="_blank" class="new_win">www.simplemachines.org</a>.
	Du bør sjekke denne boksen for oppdateringer, nye versjoner og viktig informasjon fra Simple Machines.';

$helptxt['registrations'] = '<strong>Registreringsbehandling</strong><br />
	Denne delen inneholder alle funksjonene som er nødvendige for å behandle nye registreringer på forumet. 
	Den inneholder opptil fire seksjoner basert på forumets innstillinger. De er som følger:<br /><br />
	<ul class="normallist">
		<li>
			<strong>Registrér nytt medlem</strong><br />
			Fra dette bildet kan du velge å registrere en medlemkonto for et medlem. Dette kan være nyttig om forumet
			er stengt for nye medlemmer eller om du, som administrator, ønsker å opprette en testkonto.
			Dersom medlemskapet krever aktivering, vil medlemmet bli tilsendt en aktiveringslink i en e-post,
			som vedkommende må klikke på. Du kan også velge å sende medlemmet en e-post
			med nytt passord til den registrerte e-postadressen..<br /><br />
		</li>
		<li>
			<strong>Endre registreringserklæring</strong><br />
			Dette lar deg endre teksten til registreringserklæringen som vises nå et medlem registrerer seg på forumet.
			Du kan legge til eller fjerne alt fra basiserklæringen, som følger med Protendo.<br /><br />
		</li>
		<li>
			<strong>Definér reserverte navn</strong><br />
			Bruk denne delen til å spesifisere ord og navn du ikke vil dine medlemmer skal bruke.<br /><br />
		</li>
		<li>
			<strong>Instillinger</strong><br />
			Denne delen er kun synlig om du har tilgang til å administrere forumet.
			Fra dette bildet kan du velge registreringsmetode som er brukt på forumet, samt andre relaterte innstillinger.
		</li>
	</ul>';

$helptxt['modlog'] = '<strong>Moderatorlogg</strong><br />
	Denne delen lar administratorer kontrollere og sjekke alle moderatorhandlinger som moderatorene på forumet har utført. 
	For å unngå at moderatorene kan slette sine spor, kan ingen av disse loggførte hendelsene slettes før etter 24
	timer er gått siden handlingen ble utført.';
$helptxt['adminlog'] = '<strong>Administratorlogg</strong><br />
	Denne seksjonen tillater administratorer på forumet å spore noen av de administrative tiltakene som er utført på forumet. For å sikre at administratorene
	ikke fjerner referanser til hvilke handlinger de har utført, kan ikke oppføringene slettes før det har gått 24 timer etter at handlingen ble utført.';
$helptxt['warning_enable'] = '<strong>Advarselssystem for brukere</strong><br />
	Denne funksjonen gjør det mulig for medlemmer av admin og moderatorteamet å utstede advarsler til medlemmene - og å bruke advarselsnivået til å avgjøre hva en bruker har av tillatelser på forumet. Ved å aktivere denne funksjonen blir Innstillinger for moderering tilgjengelig for å definere hvilke grupper som kan tildele advarsler til medlemmene. Advarselsnivåer kan justeres fra medlemmenes profiler. Følgende tilleggsfunksjoner er tilgjengelig: 
	<ul class="normallist"> 
		<li> 
			<strong>Advarselsnivå for når holde øye med bruker</strong><br />
			Denne innstillingen definerer advarselsnivå nå en begynner å &quot;holde øye&quot; med medlemmet. Alle medlemmer som blir &quot;holdt øye med&quot; vil vises i det aktuelle området på moderatorsenteret.
		</li>
		<li> 
			<strong>Advarselsnivå for innleggsmoderering</strong><br /> 
			Medlemmer som passerer verdien på innstillingen vil få alle sine innlegg moderert og godkjent før de vises på forumet. Dette vil overstyre eventuelle tillatelser som eksistere på delforum relatert til innleggsmoderering.
		</li>
		<li> 
			<strong>Advarselsnivå for å sperre bruker</strong><br />
			Dersom ett medlem passerer verdien på denne innstillingen, mister medlemmet alle rettigheter til å skrive innlegg.
		</li>
		<li> 
			<strong>Maksimum antall advarselspoeng per dag</strong><br /> 
			Denne innstillingen begrenser antall poeng en moderator kan legge til eller fjerne fra ett medlem i en tjuefire timers periode. Dette kan brukes til å begrense hva en moderator kan gjøre i løpet av perioden. Denne innstillingen kan deaktiveres ved å sette verdi til null. Merk at alle medlemmer med administratorrettigheter ikke er berørt av denne verdien.
		</li>
	</ul> ';
$helptxt['error_log'] = '<strong>Loggførte feilmeldinger</strong><br />
	Denne delen loggfører alle viktige feilmeldinger som har oppstått under bruk av forumet ditt.
	Loggen lister opp alle feilmeldingene etter dato som kan sorteres ved å klikke på pilen ved siden av hver dato.
	Du kan også filtrere feilmeldingene ved å klikke på ikonet ved siden av hver feilmelding.
	Dette lar deg filtrere etter medlem. Når du har filtrering aktivert, vil kun resultater som passer til filtreringen bli vist.';
$helptxt['theme_settings'] = '<strong>Designinnstillinger</strong><br />
	Denne delen lar deg endre innstillingene for et spesielt design. Disse innstillingene inkluderer alternativer som designmappe og adresseinformasjon, men også innstillinger som påvirker layouten til et design på forumet. De fleste design vil ha et variert utvalg av brukerdefinerbare innstillinger, som lar deg tilpasse designet til forumet på best mulig måte.';
$helptxt['smileys'] = '<strong>Smilefjes</strong><br />
	Her kan du legge til og fjerne smilefjes og smileysett. Merk deg at om et smilefjes er i et sett, er det i alle sett, ellers kan det virke
	forvirrende for medlemmer som bruker forskjellige smileysett.<br /><br />

	Du kan også endre innleggsikoner fra denne delen, om du har dem aktivert i innstillingene dine.';
$helptxt['calendar'] = '<strong>Kalender</strong><br />
	Her kan du stille på innstillingene for kalenderen, samt legge til og fjerne helligdager som kommer frem i kalenderen.';

$helptxt['serversettings'] = '<strong>Serverinnstillinger</strong><br />
	Her kan du endre på hovedinnstillingene til forumet. Denne delen inkluderer database- og adresseinnstillinger, samt 
	annen viktig konfigurasjon som mailinstillinger og cache. Tenk deg nøye igjennom før du stiller på noe her, 
	fordi om du gjør en feil, kan du gjøre forumet ustabilt.';
$helptxt['manage_files'] = '<ul class="normallist">
		<li>
			<strong> Bla gjennom filer </strong><br />
			Bla gjennom alle vedleggene, avatarer og miniatyrbilder lagret av Protendo.<br /><br />
		</li><li>
			<strong> Vedlegginnstillinger </strong><br />
			Konfigurerer hvor vedlegg blir lagret og setter begrensninger på type filvedlegg <br /><br />
		</li><li>
			<strong>Avatarinnstillinger </strong><br />
			Konfigurer hvor avatarer blir lagret og behandle skalering av avatarer.<br /><br />
		</li><li>
			<strong>Filvedlikehold</strong><br />
			Sjekker og reparere eventuelle feil i vedleggsmappen og sletter valgt vedlegg.<br /><br />
		</li>
	</ul>';

$helptxt['topicSummaryPosts'] = 'Dette lar deg stille inn antall tidligere innlegg som skal vises i emneoversikten på svarbildet.';
$helptxt['enableAllMessages'] = 'Sett dette til det <em>maksimale</em> antallet innlegg et emne kan ha for å vise "Alle"-linken. Ved å sette den lavere enn &quot;Antall innlegg per side i et emne&quot; vil bety at den linken aldri blir vist, mens om du setter den for høyt kan det gjøre forumet ditt saktere.';
$helptxt['enableStickyTopics'] = 'Prioriterte emner er slike som forblir på toppen av emnelista. Dette blir oftest brukt til viktig informasjon. Selv om du kan endre på rettighetene, er det normalt bare moderatorer og administratorer som kan gjøre emner prioriterte.';
$helptxt['allow_guestAccess'] = 'Ved å fjerne avhukingen i denne boksen vil du kun gi gjestene muligheten til å logge inn, registrere, motta passordpåminnelse osv. på forumet ditt. Men dette er ikke det samme som å nekte gjestene tilgang til forumet.';
$helptxt['userLanguage'] = 'Ved aktivering av denne funksjonen vil du la medlemmene få velge språkpakken de vil bruke på forumet. Dette vil ikke overstyre basisinnstillingen.';
$helptxt['trackStats'] = 'Statistikk:<br />Denn vil la medlemmene se de nyeste innleggene og de mest aktive emnene på forumet. Denne vil også vise ulike oversikter, som flest brukere pålogget, nye medlemmer og nye emner.<hr />
		Sidevisninger:<br />Legger til en ekstra kolonne på statistikksiden med antall sidevisninger forumet ditt har hatt.';
$helptxt['titlesEnable'] = 'Ved å aktivere egendefinerte titler kan du la medlemmer, med relevante rettigheter, opprette en spesiell tittel som passer til seg selv. Denne vil vises like under visningsnavnet.<br /> <em>For eksempel:</em>:<br />Hans<br />En kul fyr';
$helptxt['topbottomEnable'] = 'Dette vil legge til knapper for å g&aring til toppen eller bunnen av siden uten å bla seg igjennom hele siden.';
$helptxt['onlineEnable'] = 'Dette vil legge til en indikator som vil vise om et medlem er innlogget eller utlogget';
$helptxt['todayMod'] = 'Dette vil vise &quot;I dag&quot; eller &quot;I går&quot; isteden for datoen.<br /><br /> <strong>Eksempler:</strong><br /><br /> <dt> <dt>Deaktivert</dt> <dd>3. oktober 2009 kl 12:59:18</dd> <dt>Kun i dag</dt> <dd>I dag kl 12:59:18</dd> <dt>I dag &amp; I går</dt> <dd>I går kl 09:36:55</dd> </dt> ';
$helptxt['disableCustomPerPage'] = 'Huk av dette alternativet for å forhindre at brukerne tilpasser antall innlegg og emner som vises per side i emneoversikten og på emnevisningen.';
$helptxt['enablePreviousNext'] = 'Dette vil vise en link til neste og forrige emne.';
$helptxt['pollMode'] = 'Dette vil velge om du vil aktivere avstemninger eller ikke. Dersom avstemninger er deaktivert, vil eksisterende avstemninger skjules fra emneoversikten. Du kan velge å vise dem som vanlige emner uten avstemning ved å velge 
		&quot;Vis avstemninger som emner&quot;.<br /><br />For å bestemme over hvem som kan opprette avstemninger, vise avstemninger og lignende, kan du bestemme det under rettighetene. Husk på dette om avstemninger ikke fungerer.';
$helptxt['enableVBStyleLogin'] = 'Dette vil vise et mer kompakt innloggingsfelt på hver side av forumet for gjestene.';
$helptxt['enableCompressedOutput'] = 'Dette vil komprimere utdata for å spare båndbredde, men det krever at serveren din har zlib installert.';
$helptxt['disableTemplateEval'] = 'Som standard blir maler evaluert i stedet for inkludert. Dersom en mal inneholder feil, er dette med til å vise mer nyttig informasjon om feilen.<br /><br />
	I store fora kan imidlertid denne inkluderingsprosessen være betydelig tregere. Om du er en avanserte bruker, kan du velge å deaktivere den funksjonen.';
$helptxt['databaseSession_enable'] = 'Dette alternativet aktiverer bruk av sesjoner - som er best for servere med mange brukere, men det hjelper på timeout-meldinger og kan gjøre forumet raskere.';
$helptxt['databaseSession_loose'] = 'Ved å aktivere dette, vil du begrense båndbreddeforbruket for dine medlemmer, og gjøre slik at det å klikke tilbake ikke vil oppfriske siden - det negative med dette er at ikonene vil ikke oppdateres, samt endel andre ting. (med mindre du klikker på oppdater siden istedet for å gå tilbake til den.)';
$helptxt['databaseSession_lifetime'] = 'Dette er antall sekunder en sesjon varer før den timer ut. Om den blir inaktiv, kalles det for at den har &quot;timet ut&quot;. Alt over 2400 sekunder er anbefalt.';
$helptxt['enableErrorLogging'] = 'Dette vil loggføre alle feilmeldingene, som for eksempel en mislykket innlogging, slik at du kan se hva som gikk galt.';
$helptxt['enableErrorQueryLogging'] = 'Dette vil inkludere at hele spørringen som ble sendt til databasen går inn i feilloggen.  Dette krever at feilloggen er slått på.<br /><br /><strong>Merk:  Dette påvirker evnen til å filtrere feilloggen på feilmeldingene.</strong>';
$helptxt['allow_disableAnnounce'] = 'Dette vil la medlemmene få deaktivere varsling ved kunngjøringer som blir laget når du velger &quot;Kunngjør emnet&quot;.';
$helptxt['disallow_sendBody'] = 'Dette slår av funksjonen med å sende innholdet i svar og innlegg i e-postmeldinger ved varsling.<br /><br />Oftest, når medlemmer svarer på mailen, vil webmasteren motta svarene.';
$helptxt['compactTopicPagesEnable'] = 'Dette vil bare vise et utvalg av sidene for et langt emne.<br /><em>Eksempel:</em>
		&quot;3&quot; for å vise: 1 ... 4 [5] 6 ... 9 <br />
		&quot;5&quot; for å vise: 1 ... 3 4 [5] 6 7 ... 9';
$helptxt['timeLoadPageEnable'] = 'Dette vil vise hvor lang tid det tok for Protendo å laste fram siden på bunnen av siden.';
$helptxt['removeNestedQuotes'] = 'Dette vil kun vise sitatene fra det opprinnelige innlegg, og ikke de andre innleggene det siteres fra.';
$helptxt['simpleSearch'] = 'Dette vil vise en enkel søkeboks og ha en link til for å gi tilgang til avansert søk.';
$helptxt['max_image_width'] = 'Dette lar seg sette en maksimumsstørrelse for vedlagte bilder. Bilder som er mindre enn denne grensen vil ikke bli påvirket av denne endringen.';
$helptxt['mail_type'] = 'Denne innstillingen lar deg velge PHPs basisinnstillinger eller innstillingene for SMTP. PHP støtter ikke autentisering via SMTP (som mange leverandører krever nå), så om du ønsker dette, bør du velge SMTP. Vær oppmerksom på at SMTP kan være tregere og noen servere vil ikke godta brukernavn og passord.<br /><br />Du trenger ikke å fylle inn innstillingene for SMTP om du bruker PHPs basisinnstilling.';
$helptxt['attachment_manager_settings'] = 'Vedlegg er filer som medlemmene kan laste opp, og legge ved et innlegg.<br /><br />
		<strong>Kontroller filendelse på vedlegg</strong>:<br /> Ønsker du å sjekke filtype i vedleggene?<br />
		<strong>Tillatte filendelser på vedlegg</strong>:<br /> Du kan angi de tillatte filtypene på vedlagte filer.<br />
		<strong>Vedleggsmappe</strong>:<br /> Sti til mappen med vedlegg<br />(eksempel: /home/dittnettområde/public_html/forum/attachments)<br />
		<strong>Maksimumsstørrelse for mappe</strong> (i KB):<br /> Velg hvor stor vedleggsmappen skal være, inkludert alle filer i den.<br />
		<strong>Maksimumsstørrelse for vedlegg i innlegg</strong> (in KB):<br /> Velg maksimal filstørrelse for alle vedlegg per innlegg. Hvis dette er lavere enn maksimumsstørrelse per vedlegg, vil dette være grensen.<br />
		<strong>Maksimumsstørrelse per vedlegg</strong> (i KB):<br /> Velg maks filstørrelse per vedlegg.<br />
		<strong>Maks antall vedlegg per innlegg</strong>:<br /> Velg maks antall vedlegg som kan legges ved ett innlegg.<br />
		<strong>Vise bildevedlegg som bilde under innlegget</strong>:<br /> Hvis den opplastede filen er et bilde, vil dette vise bilde under selve innlegget.<br />
		<strong>Endre bildestørrelse ved visning under innlegg</strong>:<br /> Dersom det ovennevnte alternativet er valgt, vil dette lagre vedlegget som et eget (mindre) vedlegg som miniatyrbilde for å redusere båndbredde.<br />
		<strong>Maks bredde og høyde på miniatyrbilder</strong>:<br /> Brukes kun sammen med alternativet &quot;Endre bildestørrelse ved visning under innlegg&quot;. Maksimal bredde og høyde som bilder vil bli endret ned til. De vil bli endret proporsjonalt.';
$helptxt['attachment_image_paranoid'] = 'Dersom du velger dette alternativet vil vedlagte bilder bli sikkerhetskontrollert veldig nøye. Advarsel! Denne omfattende kontrollen kan mislykkes, også på bilder som er OK. Det anbefales kun å bruke dette alternativet sammen med re-koding av bilder, så har Protendo en mulighet til å re-kode bilde dersom sikkerhetskontrollen mislykkes. Dersom Re-koding ikke er aktivert og bilde ikke går igjennom sikkerhetskontrollen, vil alle vedleggene bli avvist. Går bildevedleggene gjennom kontrollen, vil de bli renset og lastes opp.';
$helptxt['attachment_image_reencode'] = 'Dersom du velger dette alternativet vil vedlagte bilder re-kodes. Re-koding av bilder gir bedre sikkerhet. Merk: Re-koding av animerte bilder gjør disse til statiske bilder. <br /> Denne funksjonen er bare mulig hvis GD modulen er installert på serveren.';
$helptxt['avatar_paranoid'] = 'Dersom du velger dette alternativet vil det bli utførte en nøye sikkerhetskontroll av avatarer. Advarsel! Denne omfattende kontrollen kan mislykkes, også på bilder som er OK. Det anbefales kun å bruke dette alternativet sammen med re-koding av avatar, så har Protendo en mulighet til å re-kode bilde dersom sikkerhetskontrollen mislykkes. Dersom Re-koding ikke er aktivert og bilde ikke går igjennom sikkerhetskontrollen, vil avataren bli avvist. Går avataren gjennom kontrollen, vil den bli renset og lastes opp.';
$helptxt['avatar_reencode'] = 'Dersom du velger dette alternativet vil avatarer bli re-kodet. Re-koding av bilder gir bedre sikkerhet. Merk: Re-koding av animerte bilder gjør disse til statiske bilder. <br /> Denne funksjonen er bare mulig hvis GD modulen er installert på serveren. ';
$helptxt['karmaMode'] = 'Karma er en funksjon som kan vise populariteten til et medlem. Medlemmer, med gyldige rettigheter,
                kan si at et medlem har en &quot;bra&quot; eller &quot;dårlig&quot; karma basert på oppførsel og innlegg.
                Du kan stille inn antall innlegg som er påkrevd for å gi karma til medlemmer, tidspunktet mellom 
                hvor ofte slikt kan deles ut, og om adminstratorer må vente også. Dersom du har trøbbel med denne funksjonen, 
                sjekk igjenom rettighetene.';
$helptxt['cal_enabled'] = 'Kalenderen brukes for å vise fødselsdager eller for å vise viktige hendelser relatert til ditt forum.<br /><br />
		<strong>Vise dager som koblinger til \'Legg til hendelse\'</strong>:<br />Dette vil la dine medlemmer legge til hendelser for den aktuelle dagen ved å klikke på datoen<br />
		<strong>Vise ukenummere</strong>:<br />Vise hvilken uke datoene er i.<br />
		<strong>Maks antall forestående dager som skal vises på forumoversikten</strong>:<br />Dersom dette er satt til 7, vil hendelser for den neste uken bli vist.<br />
		<strong>Vis helligdager</strong>:<br />Viser dagens helligdager under kalenderdelen på forumoversikten.<br />
		<strong>Vis fødselsdager</strong>:<br />Viser dagens bursdagsbarn under kalenderdelen på forumoversikten.<br />
		<strong>Vis hendelser</strong>:<br />Viser dagens hendelser under kalenderdelen på forumoversikten.<br />
		<strong>Standard forum for hendelser</strong>:<br />Hva vil du skal være forumet for hendelser?<br />
		<strong>Tillat hendelser som ikke knyttes mot et emne</strong>:<br />Lar medlemmer legge til hendelser uten at de trenger å være tilknyttet et emne.<br />
		<strong>Minimum år</strong>:<br />Skriv inn det &quot;første&quot; året i kalenderen.<br />
		<strong>Maksimum år</strong>:<br />Skriv inn det &quot;siste&quot; året i kalenderen.<br />
		<strong>Fødselsdagsfarge</strong>:<br />Skriv inn fargen du vil ha på fødselsdagene.<br />
		<strong>Hendelsesfarge</strong>:<br />Skriv inn fargen du vil ha på hendelsene<br />
		<strong>Helligdagsfarge</strong>:<br />Skriv inn fargen du vil ha på helligdagene<br />
		<strong>Tillat hendelser over flere dager</strong>:<br />Huk av for å tillate hendelser med varighet over flere dager.<br />
		<strong>Maks antall dager en hendelse kan vare</strong>:<br />Skriv inn maksimalt antall dager en hendelse kan vare.<br /><br />
		Husk at bruk av kalenderen (legge til/endre hendelser osv.) er kontrollert av rettighetene som blir endret på rettighetsbildet.';
$helptxt['localCookies'] = 'Protendo bruker informasjonskapsler for å oppbevare innloggingsinformasjon på brukerens datamaskin.
	Informasjonskapslsene kan gjerne lagres for hele domenet (server.no) eller bare for forumet (server.no/forum/).<br />
	Merk dette alternativet om du har problemer med brukere som blir utlogget automatisk.<hr />
	Informasjonskapsler som lagres for hele domenet er mindre sikkert hvis brukt på delte webservere (som på Tripod eller home.no).<hr />
	Informasjonskapsler kun for forumet vil ikke fungere utenfor mappa til forumet, så om forumet ditt befinner seg på www.server.no/forum, kan f.eks. ikke en side som www.server.no/index.php få tak i kontoinformasjonen din. Om du bruker SSI.php, som følger med forumet, er det anbefalt at du bruker informasjonskapsler lagret for hele domenet.';
$helptxt['enableBBC'] = 'Ved å aktivere dette alternativet vil du tillate dine medlemmer å bruke BBC-koder på forumet, som vil la dem få legge til bilder til sine innlegg, formatere skriften og mye mer.';
$helptxt['time_offset'] = 'Ikke alle forumadministratorer tilhører samme tidssone som serveren forumet er hostet på. Bruk dette alternativet for å skrive inn tidsavviket (i timer) i forhold til det forumet skal bruke fra serverens tidssone. Negative verdier og verdier med desimal (.) er tillatte.';
$helptxt['default_timezone'] = 'Serveren tidssone forteller PHP hvor serveren er plassert. Du bør påse at dette er riktig, fortrinnsvis til land/by hvor serveren er lokalisert. Du kan finne mer informasjon på <a href="http://www.php.net/manual/en/timezones.php" target="_blank">PHP sin nettside</a>. ';
$helptxt['spamWaitTime'] = 'Ikke alle forumadministratorer tilhører samme tidssone som serveren forumet er hostet på. Bruk dette alternativet for å skrive inn tidsavviket (i timer) i forhold til det forumet skal bruke fra serverens tidssone. Negative verdier og verdier med desimal (.) er tillatte.';

$helptxt['enablePostHTML'] = 'Dette vil tillate bruk av basis HTML-tagger i innlegg:
	<ul style="margin-bottom: 0;">
		<li>&lt;b&gt;, &lt;u&gt;, &lt;i&gt;, &lt;s&gt;, &lt;em&gt;, &lt;ins&gt;, &lt;del&gt;</li>
		<li>&lt;a href=&quot;&quot;&gt;</li>
		<li>&lt;img src=&quot;&quot; alt=&quot;&quot; /&gt;</li>
		<li>&lt;br /&gt;, &lt;hr /&gt;</li>
		<li>&lt;pre&gt;, &lt;blockquote&gt;</li>
	</ul>';

$helptxt['themes'] = 'Her kan du velge hvilket basisdesign som skal brukes, hvilket design gjestene vil bruke og andre alternativer. Klikk på et design for å endre innstillingene for det.';
$helptxt['theme_install'] = 'Dette lar deg installere nye design på forumet. Du kan gjøre dette fra en allerede opprettet mappe, ved å laste opp en innpakket fil for designet eller ved å kopiere basisdesignet.<br /><br />Merk deg at den innpakkede filen eller mappa må ha en fil med navnet <tt>theme_info.xml</tt> i seg.';
$helptxt['enableEmbeddedFlash'] = 'Dette alternativet vil la dine medlemmer aktivere Flash direkte i sine innlegg på samme måte som bilder. Dette kan være en stor sikkerhetsrisiko, skjønt bare noen få har klart å utnytte denne.
	BRUK KUN PÅ EGET ANSVAR!';
// !!! Add more information about how to use them here.
$helptxt['xmlnews_enable'] = 'Lar brukere linke til <a href="%s?action=.xml;sa=news">siste nytt</a>
	og lignende data. Det er også anbefalt at du begrenser størrelsen på data ettersom nå RSS-data blir vist i Trillian eller andre klienter, blir det kuttet ned.';
$helptxt['hotTopicPosts'] = 'Skriv inn antall innlegg for et emne til å bli karakterisert som aktivt eller veldig aktivt.';
$helptxt['globalCookies'] = 'Gjør innloggingsinformasjonskapsler tilgjengelige over ulike subdomener.  For eksempel om...<br />
	Siden din befinner seg på http://www.simplemachines.no/,<br />
	og forumet ditt befinner seg på http://forum.simplemachines.no/<br />
	vil dette alternativet la deg få tilgang til forumets informasjonskapsel på siden din. Ikke aktiver dette om det er flere subdomener (f.eks. hacker.simplemachines.no) som ikke er under din kontroll.';
$helptxt['secureCookies'] = 'Aktivering av dette valget vil tvinge at cookies som opprettet for dette forumet blir merket som sikre. Aktiver dette alternativet kun hvis du bruker HTTPS på hele nettstedet, ellers vil oppretting av cookies feile!';
$helptxt['securityDisable'] = 'Dette vil <em>deaktivere</em> den ekstra passordkontrollen for administratordelen. Dette er ikke anbefalt!';
$helptxt['securityDisable_why'] = 'Dette er ditt gjeldende passord. (samme som du bruker for å logge inn.)<br /><br />Ved å skrive dette inn forsikrer deg om at de endringene du utfører blir utført av <strong>deg</strong>.';
$helptxt['emailmembers'] = 'I denne meldingen kan du bruke ulike &quot;variabler&quot;.  Disse er:<br />
	{\\$board_url} - Adressen til forumet ditt.<br />
	{\\$current_time} - Gjeldende klokkeslett.<br />
	{\\$member.email} - Medlemmets e-postadresse.<br />
	{\\$member.link} - Link til medlemmets profil.<br />
	{\\$member.id} - Medlemmets bruker-ID.<br />
	{\\$member.name} - Medlemmets navn. (For personifisering)<br />
	{\\$latest_member.link} - Link til profilen til det nyeste medlemmet.<br />
	{\\$latest_member.id} - Bruker-ID til det nyeste medlemmet.<br />
	{\\$latest_member.name} - Navnet til det nyeste medlemmet.';
$helptxt['attachmentEncryptFilenames'] = 'Krypterte filnavn på vedlegg lar deg ha flere vedlegg med samme navn, trygg bruk av .php-filer som vedlegg og øker sikkerheten. Det vil, på en annen side, være vanskelig å gjenopprette databasen over vedleggene om noe drastisk skulle skje.';

$helptxt['failed_login_threshold'] = 'Still inn antall mislykkete innloggingsforsøk før brukeren blir videresendt til skjermbildet for passordpåminnelse.';
$helptxt['oldTopicDays'] = 'Dersom dette alternativet er aktivert vil medlemmet få en advarsel om vedkommende legger inn et emnet som ikke har blitt svart på etter x antall dager, som du bestemmer her. Skriv inn 0 for å deaktivere dette alternativet.';
$helptxt['edit_wait_time'] = 'Antall sekunder et innlegg kan endres før endringstidspunktet blir loggført.';
$helptxt['edit_disable_time'] = 'Antall minutter som skal gå for et medlem ikke kan endre sitt innlegg. Skriv inn 0 for å deaktivere. <br /><br /><em>Merk: Dette vil ikke ha noen effekt på medlemmer som har rettigheter til å endre innlegg.</em>';
$helptxt['posts_require_captcha'] = 'This setting will force users to enter a code shown on a verification image each time they make a post to the board. Only users with a post count below the number set will need to enter the code - this should help combat automated spamming scripts.';
$helptxt['enableSpellChecking'] = '(Dette fungerer kun på engelsk) Aktiver stavekontroll. Du MÅ ha korrekt pspell-bibliotek installert på din server. Din server ' . (function_exists('pspell_new') ? 'HAR' : 'HAR IKKE') . ' dette satt opp.';
$helptxt['disable_wysiwyg'] = 'Denne innstillingen forhindrer alle brukere fra å bruke WYSIWYG (&quot;What You See Is What You Get&quot; (&quot;Slik du ser det, slik blir det&quot;)) tekstbehandleren på sidene for å legge inn innlegg.';
$helptxt['lastActive'] = 'Skriv inn tidsperioden som skal sees på etter aktive brukere. Basisinstillingen er 15 minutter.';

$helptxt['customoptions'] = 'Denne seksjonen definerer alternativene en bruker kan velge fra nedtrekkslisten. Det er noen viktige punkter å merke seg i denne seksjonen: 
	<ul class="normallist"> 
		<li><strong>Standard valg::</strong>
			Uansett hvilken alternativboks som har &quot;radioknappen&quot; ved siden av seg, vil være standardvalget for brukeren når de er i profilen sin.
		</li>
		<li>
			<strong>Fjern valg:</strong>
			For å fjerne et alternativ sletter du bare teksten i boksen for dette alternativet - alle brukere som har valgt dette alternativet vil få det slettet.
		</li> 
		<li>
			<strong>Reorganisere valgmulighetene:</strong> 
			Du kan endre rekkefølgen på alternativene ved å flytte teksten mellom boksene. Men - en viktig merknad - du må <strong>ikke</strong> endre teksten når rekkefølgen endres, ellers vil brukerdata gå tapt.
		</li>
	</ul>';

$helptxt['autoOptDatabase'] = 'Dette alternativet vil optimalisere databasen med jevne mellomrom basert på hva du skriver inn her. Skriver du inn 1 blir den optimalisert hver dag. Du kan også skrive inn hvor mange brukere online det kan være på det meste sånn at det ikke overbelaster serveren og skaper problemer for for mange brukere.';
$helptxt['autoFixDatabase'] = 'Dette vil automatisk reparere ødelagte tabeller og fortsette som om ingenting har skjedd. Dette kan være nyttig fordi eneste måten å fikse dette er å REPARERE tabellen. På denne måten vil ikke forumet ditt gå ned uten at du merker det da du vil få en e-post nå dette skjer.';

$helptxt['enableParticipation'] = 'Dette vil vise et lite ikon på emner et medlem har skrevet i.';

$helptxt['db_persist'] = 'Dette beholder tilkoblingen aktiv for å øke ytelsen. Dersom du ikke er på en dedikert server, kan dette skape problemer hos din leverandør.';
$helptxt['ssi_db_user'] = 'Valgfri innstilling, til å bruke et annet database brukernavn og passord når SSI.php brukes.';

$helptxt['queryless_urls'] = 'Dette endrer utseendet på adressene slik at de er mer vennlige for søkemotorene. Adressene vil se ut som f.eks. index.php/topic,1.html.

<br /><br />

Dette funksjonen vil ' . (isset($_SERVER['SERVER_SOFTWARE']) && (strpos($_SERVER['SERVER_SOFTWARE'], 'Apache') !== false || strpos($_SERVER['SERVER_SOFTWARE'], 'lighttpd') !== false) ? '' : 'ikke') . ' fungere på din server.';
$helptxt['countChildPosts'] = 'Aktivering av dette alternativet vil bety at emner og innlegg i et forums delforum vil telles med totalene på hovedsiden.<br /><br />Dette vil gjøre forumet ditt litt treigere, men det vil bety at om det kun er innlegg og emner i delforumet, vil det ikke stå 0 på hovedsiden.';
$helptxt['fixLongWords'] = 'Dette alternativet deler opp ord lenger enn et bestemt antall tegn slik at de ikke ødelegger (så mye ...) forumets utseende. Dette alternativet bør ikke settes til en verdi under 40 tegn. Denne funksjonen vil ikke fungere med fora som bruke UTF-8 og PHP versjon under 4.4.0. Denne ' . (empty($GLOBALS['context']['utf8']) || version_compare(PHP_VERSION, '4.4.0') != -1 ? 'VIL' : 'VIL IKKE') . ' virke på din server.';
$helptxt['allow_ignore_boards'] = 'Markering av dette alternativet tillater brukere å velge forum de ønsker å ignorere.';

$helptxt['who_enabled'] = 'Dette alternativet lar deg aktivere eller deaktivere muligheten til å se hvem som er på forumet og hva de driver med.';

$helptxt['recycle_enable'] = 'Oppretter en papirkurv der emner og innlegg som er slettet blir lagt inn.';

$helptxt['enableReportPM'] = 'Dette alternativet lar dine medlemmer rapportere personlige meldinger de mottar til administrasjonen. Dette kan være nyttig for å kontrollere misbruk av PM-systemet.';
$helptxt['max_pm_recipients'] = 'Dette alternativet lar deg sette det maksimale antallet mottakere en PM kan ha fra et annet medlem. Dette kan bli brukt til å forhindre reklamemisbruk av PM-systemet. Merk at medlemmer med rettigheter til å sende ut nyhetsbrev, er fritatt fra denne begrensningen. Sett til null for ingen grense..';
$helptxt['pm_posts_verification'] = 'Denne innstillingen tvinger brukerne til å oppgi en verifiseringskode hver gang de sender en personlig melding. Brukere med antall innlegg lavere enn det angitte tallet må taste inn koden - dette skal bidra til å bekjempe automatisert spamming skript.';
$helptxt['pm_posts_per_hour'] = 'Dette vil begrense antallet personlige meldinger som en bruker kan sende i løpet av en 60 minutters periode. Gjelder ikke for administratorer og moderatorer.';

$helptxt['default_personal_text'] = 'Angir standard tekst som nye bruker vil få som sin &quot;personlige tekst.&quot;';

$helptxt['modlog_enabled'] = 'Loggf&osaslh;rer alle moderatorhandlinger.';

$helptxt['guest_hideContacts'] = 'Hvis dette alternativet er aktivert vil du skjule e-postadresser og annen kontaktinformasjon for gjestene på forumet.';

$helptxt['registration_method'] = 'Dette alternativet bestemmer hvilken registeringsmetode du vil bruke for nye medlemmer til ditt forum. Du kan velge mellom:<br /><br />
	<ul class="normallist">
		<li>
			<strong>Registrering deaktivert</strong><br />
				Deaktiverer registeringsprosessen, som betyr at ingen nye medlemmer kan registrere seg på forumet ditt.<br />
		</li><li>
			<strong>Øyeblikkelig registering</strong><br />
				Nye medlemmer kan logge inn og skrive innlegg øyeblikkelig etter registering på forumet.<br />
		</li><li>
			<strong>Medlem må aktivere seg</strong><br />
				Når dette alternativet er aktivert må ethvert medlem som registrerer seg, klikke på en aktiveringslink som er sendt til medlemmets e-postadresse før de er medlemmer på forumet<br />
		</li><li>
			<strong>Medlem må godkjennes</strong><br />
				Dette alternativet vil gjøre slik at alle nye medlemmer må godkjennes av administrator før de blir medlemmer.
		</li>
	</ul>';
$helptxt['register_openid'] = '<strong>Godkjenn med OpenID</strong><br /> OpenID er en metode for å bruke ett brukernavn på tvers av ulike nettsteder, for å forenkle online opplevelsen. For å bruke OpenID må du først opprette en OpenID konto - en liste over tilbydere finner du på <a href="http://openid.net/" target="_blank">OpenID offisielle nettside</a><br /><br /> Når du har en OpenID konto, skriver du inn din unike identifikasjonsnettadresse i OpenID boksen og klikker på knappen send. Du vil deretter bli ledet til din OpenID leverandørs område for å verifisere din identitet før de sender deg tilbake til dette nettstedet igjen.<br /><br /> Første gangen du besøk dette nettstedet vil du bli bedt om å bekrefte et par detaljer før du blir godkjent, etter dette kan du logge inn på denne siden og endre dine profilinnstillinger ved hjelp av bare OpenID.<br /><br /> For mer informasjon vennligst gå til <a href="http://openid.net/" target="_blank">OpenID offisielle nettside</a> ';

$helptxt['send_validation_onChange'] = 'Om dette alternativet er aktivert, vil et medlem som endrer sin e-postadresse motta en aktiveringslink til sin nye e-postadresse for å re-aktivere medlemskontoen sin';
$helptxt['send_welcomeEmail'] = 'Om dette alternativet er aktivert, vil en e-post bli sendt til ethvert nytt medlem for å ønske dem velkommen til forumet';
$helptxt['password_strength'] = 'Denne innstillingen bestemmer sikkerhetsgraden til passordene medlemmene dine bruker. Desto sikrere passordene er, desto vanskeligere vil det være å ta over et medlems brukerkonto.
	Alternativene er:
	<ul class="normallist">
		<li><strong>Lav:</strong> Passordet må bestå av minimum fire (4) tegn.</li>
		<li><strong>Middels:</strong> Passordet må bestå av minimum åtte (8) tegn og kan ikke inneholde deler av e-postadressen eller medlemmets brukernavn.</li>
		<li><strong>Høy:</strong> Samme som for middels, bare at passordet må inneholde en blanding av både store og små bokstaver, samt minst ett siffer.</li>
	</ul>';

$helptxt['coppaAge'] = 'Verdien i dette feltet vil bestemme minimumsalderen for nye medlemmer til å få øyeblikkelig adgang til forumet. Ved registering må de bekrefte at de er over denne gitte alderen og dersom de ikke er, vil de enten bli nektet adgang til forumet eller settes på vent i påvente av godkjennelse fra foreldre/foresatte - avhengig av hvilken begrensning som er valgt.
	Dersom verdien er satt til 0 vil de resterende aldersbestemte begrensningene bli ignorert.';
$helptxt['coppaType'] = 'Dersom aldersgrense er satt vil denne innstillingen bestemme hva som skal skje om et medlem under aldersgrensen prøver å registrere seg på forumet ditt. Det er to muligheter:
	<ul class="normallist"> 
		<li>
			<strong>Nekte registeringen:</strong><br />
				Ethvert medlem under aldersgrensen vil få sin registering annulert.<br />
		</li><li>
			<strong>Kreve godkjenning fra foreldre/foresatte</strong><br />
				Ethvert nytt medlem under aldersgrensen vil kunne få tilgang til forumet som "Under godkjenning" og vil få tilgang til en utfyllingsform som foreldrene må fylle ut og godkjenne for å gi tilgang til det aktuelle medlemmets adgang på forumet. De vil også få kontaktinformasjon til administrator slik at de kan sende denne utfyllingsformen via e-post eller telefaks.
		</li>
	</ul>';
$helptxt['coppaPost'] = 'Kontaktinformasjonen er påkrevd for utfyllingsformer som gir adgang for mindreårige registeringer til adminstratoren. Disse detaljene vil bli vist til alle nye mindreårige og er påkrevd en godkjennelse fra foreldre/foresatte. Skriv inn iallfall en e-postadresse eller telefaksnummer du kan nås på.';

$helptxt['allow_hideOnline'] = 'Med dette alternativet kan du tillate medlemmer å skjule sin innloggingsstatus for andre medlemmer (untatt for admin). Dersom deaktivert, kan kun medlemmer med moderatortilgang skjule sin innloggingsstatus. Merk at dette ikke har påvirkning på allerede innloggede medlemmer - det vil bare stoppe dem fra å skjule seg selv ved en senere anledning.';
$helptxt['make_email_viewable'] = 'Dersom dette alternativet er aktivert, vil brukernes e-postadresser, som til vanlig er skjult for vanlig medlemmer og gjester, være offentlig tilgjengelig på forumet. Aktivering av dette vil sette brukerne i større fare for å bli offer for spam som et resultat av at e-postskannere besøker forumet. Merk at denne innstillingen ikke overstyrer brukernes innstilling for å skjule sin egen e-postadresse. Aktivering av denne innstillingen er <strong>ikke</strong> anbefalt. ';
$helptxt['meta_keywords'] = 'Disse nøkkelordene vises i kildekoden på alle sidene for å indikere for søkemotorer (osv) hva nettsiden inneholder. Det bør være en kommaseparert liste av ord, og ikke bruke HTML.';

$helptxt['latest_support'] = 'Dette panelet viser deg noen av de vanligste problemene og spørsmålene rundt ditt serveroppsett. Ingen grunn til bekymring, dette blir ikke loggført eller noe.<br /><br />Dersom dette feltet kun sier &quot;Henter hjelpinformasjon&quot;, kan ditt system ikke koble til <a href="http://www.simplemachines.org/" target="_blank">www.simplemachines.org</a>.';
$helptxt['latest_packages'] = 'Her kan du se noen av de mest populære modifiseringspakker, med raske og enkle installasjoner.<br /><br />Dersom denne seksjonen ikke viser noe, kan ditt system ikke koble til <a href="http://www.simplemachines.org/" target="_blank">www.simplemachines.org</a>.';
$helptxt['latest_themes'] = 'Denne delen vil vise de nyeste og mest populære design fra <a href="http://www.simplemachines.org/" target="_blank">www.simplemachines.org</a>. Skjønt det vil ikke komme opp noe fornuftig om ditt system ikke kan koble til <a href="http://www.simplemachines.org/" target="_blank">www.simplemachines.org</a>.';

$helptxt['secret_why_blank'] = 'For din egen sikkerhet, er svaret på ditt hemmelige spørsmål (på samme måte som ditt passord) kryptert på en slik måte at Protendo kan kun fortelle deg om du har skrevet det riktig, slik at det aldri kan fortelle deg (eller noen andre) svaret eller passordet ditt.';
$helptxt['moderator_why_missing'] = 'Siden lokale moderatorer kun er for et forum om gangen, blir du nødt for å velge moderatorer fra <a href="javascript:window.open(\'%s?action=manageboards\'); self.close();">forumoversikten</a>.';

$helptxt['permissions'] = 'Rettigheter lar deg enten tillate eller nekte medlemsgrupper fra å ulike handlinger på forumet.<br /><br />Du kan endre flere fora på en gang ved å bruke avhukingsboksene eller du kan se på rettighetene til en spesifikk gruppe ved å klikke på "Endre"';
$helptxt['permissions_board'] = 'Dersom et forum er satt til "Global", vil det bety at det forumet ikke har noen spesielle rettighetsregler. "Lokal" betyr at det har sine egne rettighetsregler i tillegg til de globale. Dette gir deg tilgang til å sette ulike rettighetsregler for hvert forum, slik at du ikke trenger å stille dem inn for hvert eneste.';
$helptxt['permissions_quickgroups'] = 'Dette lar seg bruke &quot;basis&quot; rettighetsoppsett - standard, betyr "ingenting spesielt", begrenset, betyr "som gjester", moderator, betyr "det moderator har" og til sist "vedlikehold", som er nesten like mange rettigheter som en administrator.';
$helptxt['permissions_deny'] = 'Å nekte rettigheter kan væ nyttig når du vil ta bort noen rettigheter fra spesifikke medlemmer.  Du kan legge til en ekstra medlemsgruppe med "nektet" tilgang for medlemmene du vil nekte tilgang for.<br /><br />Vær forsiktig da en nektet rettighet vil forbli nektet uansett hvilke andre medlemsgrupper medlemmet er i.';
$helptxt['permissions_postgroups'] = 'Aktivering av rettigheter for medlemsgrupper basert på antall innlegg gir deg tilgang til å sette spesiell tilgang for medlemmer som har skrevet et visst antall innlegg. Rettighetene for de innleggsbaserte gruppene vil bli <em>lagt til</em> i tillegg til rettighetene i medlemmenes vanlige medlemsgruppe.';
$helptxt['membergroup_guests'] = 'Medlemsgruppen "Uregistrerte gjester", er alle brukere som ikke er innlogget.';
$helptxt['membergroup_regular_members'] = 'Medlemsgruppen "Ugrupperte medlemmer", er innloggede medlemmer uten en primær medlemsgruppe innstilt i sin profil.';
$helptxt['membergroup_administrator'] = 'Administratoren kan, per definisjon, gjøre akkurat hva vedkommende vil i hvert eneste forum. Det er ingen rettighetsinnstillinger for administratoren.';
$helptxt['membergroup_moderator'] = 'Medlemsgruppen Moderator, er en spesiell medlemsgruppe. Rettigheter og innstillinger satt for denne gruppen har kun virkning for moderatorene <em>på foraene de kan styre over</em>. Utenfor de fora er de kun vanlige medlemmer.';
$helptxt['membergroups'] = 'I Protendo er det to typer grupper dine medlemmer kan være en del av. Disse er:
	<ul class="normallist">
		<li><strong>Vanlige grupper:</strong> En vanlig gruppe er en gruppe som medlemmene ikke blir automatisk satt til. For å sette et medlem til en slik gruppe, går du inn på Kontoinnstillinger på medlemmets profil. Her kan du sette medlemmet til hvilken som helst av de vanlige gruppene du mener de skal høre til i.</li>
		<li><strong>Innleggsbaserte grupper:</strong> Det som skiller disse fra vanlige grupper, er at disse kan du ikke sette medlemmer til. Istedet blir medlemmene automatisk satt til en av disse gruppene alt etter hvor mange innlegg som kreves for hver gruppe.</li>
	</ul>';

$helptxt['calendar_how_edit'] = 'Du kan endre disse hendelsene ved å klikke på den røde stjerna ved siden av navnet.';

$helptxt['maintenance_backup'] = 'Denne delen lar deg lagre en kopi av alle innlegg, innstillinger, medlemmer og annen informasjon fra ditt forum til en veldig stor fil.<br /><br />Det er anbefalt at du gjør dette ofte av sikkerhetshensyn, kanskje ukentlig eller hver måned.';
$helptxt['maintenance_rot'] = 'Dette lar deg slette gamle emner <strong>for godt!</strong> Det er anbefalt at du lager en sikkerhetskopi av dette først i tilfelle du slettet noe du ikke burde gjort.<br /><br />Vær forsiktig med bruken av dette alternativet.';
$helptxt['maintenance_members'] = 'Dette tillater deg <strong>fullstendig</strong> og <strong>ugjenkallelig</strong> å fjerne medlemskontoer fra forumet ditt. Det anbefales på det <strong>sterkeste</strong> at du tar en sikkerhetskopi først, i tilfelle du fjerne noe du ikke mente å fjerne.<br /><br />Bruk dette alternativet med forsiktighet. ';

$helptxt['avatar_server_stored'] = 'Dette tillater at dine medlemmer velger avatarer lagret på serveren. De er i utgangspunktet lagret på samme sted som Protendo, i avatar mappen.<br />Tips: Dersom du oppretter mapper i denne mappen, kan du lagre avatarer i &quot;kategorier&quot; av avatarer. ';
$helptxt['avatar_external'] = 'Med denne aktivert, kan dine medlemmer skriver inn en nettadresse til sin egen avatar. Ulempen med dette er at i noen tilfeller kan de bruke avatarer som er altfor store eller bilder du ikke ønsker på forumet ditt.';
$helptxt['avatar_download_external'] = 'Med denne aktivert, vil den eksterne avataren, som brukeren skriver inn, bli lastet ned til forumet ditt. Dersom adressen er korrekt, vil avataren bli behandlet som en opplastet avatar.';
$helptxt['avatar_upload'] = 'Dette alternativet er nesten det samme som &quot;Tillat medlemmer å velge en ekstern avatar&quot;, bortsett fra at du har bedre kontroll over avatars, og bedre tid til å endre størrelsen på dem, og dine medlemmer trenger ikke å ha et sted å lagre avatarene.<br /><br />Ulempen er at det kan ta mye plass på serveren din. ';
$helptxt['avatar_download_png'] = 'PNG-filer er større, men gir bedre komprimering av kvalitet. Dersom dette alternativet ikke er valgt, vil JPG-filer bli brukt istedenfor - disse er gjerne mindre, men kan tilby dårligere kvalitet under komprimering.';

$helptxt['disableHostnameLookup'] = 'Dette vil deaktivere oppsporing av vertsnavn, som i noen tilfeller kan være en treig prosess. Men dette kan gjøre utestengelser mindre effektive.';

$helptxt['search_weight_frequency'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren teller antall innlegg som treffer og deler på antall innlegg innenfor emnet.';
$helptxt['search_weight_age'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren rangerer alderen for nyeste innlegg som treffer innenfor emnet. Jo nyere innlegg, desto høyere poengsum.';
$helptxt['search_weight_length'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren er basert på emnestørrelse. Jo flere innlegg som er i emnet, desto høyere poengsum.';
$helptxt['search_weight_subject'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren ser etter om kriteriene får treff i tittelen på emner.';
$helptxt['search_weight_first_message'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren ser etter om et treff for kriteriene finnes i det første innlegget i et emne.';
$helptxt['search_weight_sticky'] = 'Vektleggingsfaktorer blir brukt for å beregne relevansen til et søkeresultat basert på kriteriene. Endre disse faktorene slik at de passer til det som er viktig for ditt forum. Dersom ditt forum fokuserer mye på nyheter, vil du kanskje ha en høy verdi på "Alder for innlegg". Alle verdier er relative i relasjon til hver andre og bør kun inneholde positive tall.<br /><br />Denne faktoren ser etter om emnet er prioritert og øker poengsummen dersom emnet er det.';
$helptxt['search'] = 'Juster alle innstillinger for søkefunksjoner her.';
$helptxt['search_why_use_index'] = 'En søkeoversikt kan drastisk forbedre ytelsen til søk på ditt forum. Spesielt nå det blir flere og flere innlegg på forumet. Da vil et søk uten søkeoversikt ta lenger tid og øke presset på din database. Dersom ditt forum har flere enn 50 000 innlegg, er det nesten anbefalt at du bruker søkeoversikt for å oppnå høyest ytelse på ditt forum.<br /><br /><b>Merk!</B> En søkeoversikt kan ta opp endel plass. Et fulltekstsøk er en innebygget oversikt i MySQL. Denne er ganske kompakt (omtrent like stor som størrelsen på tabellen for innlegg), men mange ord blir ikke lagt til og kan i noen tilfeller føre til treigere søk. Den egendefinerte oversikten er ofte større (avhengig av dine innstillinger kan den være opptil tre ganger så stor som tabellen for innlegg), men ytelsen er bedre enn for en fulltekstoversikt og mye mer stabil.';

$helptxt['see_admin_ip'] = 'IP-adressene er synlige for administratorer og moderatorer som hjelpemiddel i administrasjon og vil gjøre det enklere å spore opp de som kun er ute etter å skape dårlig stemning. Husk at IP-adressene ikke alltid sporer helt til identiteten for vedkommende, da IP-adresser ofte endres for hver person.<br /><br />Medlemmene kan se sin egen IP.';
$helptxt['see_member_ip'] = 'Din IP-adresse er kun synlig for deg og moderatorer. Husk at denne informasjonen ikke sporer tilbake til din identitet og at IP-adresser ofte forandrer seg.<br /><br />Du kan ikke se andre medlemmers IP-adresser og de kan heller ikke se din.';
$helptxt['whytwoip'] = 'Protendo bruker ulike metoder for å detektere brukerens IP-adresse. Som regel gir disse metodene den samme adresse, men i noen tilfeller blir mer enn én adresse påvist. I dette tilfellet logger Protendo begge adressene, og begge blir eventuelt brukt i en sjekk for utestengelse (eller lignende). Du kan klikke på begge adresse for å spore IP og eventuelt legge til en utestengelse om nødvendig.';

$helptxt['ban_cannot_post'] = 'Begrensningen "Kan ikke skrive" vil sette forumet i skrivebeskyttet modus for det utestengte medlemmet. Vedkommende kan ikke opprette nye emner, svare på emner, sende personlige meldinger eller avgi stemme i avstemninger. Det bannlyste medlemmet kan på en annen side lese personlige meldinger og emner.<br /><br />En advarsel vil bli vist til medlemmet at vedkommende et utestengt på den måten.';

$helptxt['posts_and_topics'] = '	<ul class="normallist">
		<li>
			<strong>Innlegginnstillinger</strong><br />
			Endrer innstillinger relatert til det med å skrive innlegg og måten innlegg er vist. Du kan også aktive stavekontroll fra her.
		</li><li>
			<strong>Bulletin Board kode</strong><br />
			Aktiverer koden som kan vise foruminnlegg med korrekt utseende. Du kan også velge hvilke koder som skal være tillatt.
		</li><li>
			<strong>Sensurerte ord</strong>
			For å kunne ha litt kontroll på språkbruken kan du sensurere spesielle ord på forumet. Denne funksjonen lar deg erstatte forbudte ord med mer uskyldige versjoner.
		</li><li>
			<strong>Emneinnstillinger</strong>
			Endrer innstillinger relatert til emner. Antall emner per side, prioriterte emner aktivert eller ei, antall innlegg i et emne for å kalle det aktivt osv.
		</li>
	</ul>';
$helptxt['spider_group'] = 'Ved å velge en restriktiv gruppe, når en gjest blir oppdaget som en søkerobot, vil den automatisk bli tildelt alle &quot;nekte&quot; tillatelser i denne gruppen i forhold til de normale tillatelsene for en gjest. Du kan bruke dette til å gi mindre tilgang for en søkemotor enn du ville for en vanlig gjest. Du kan for eksempel opprette en ny gruppe kalt &quot;Søkerobot&quot; og velg den her. Deretter kan du nekte denne gruppen å se profiler for å hindre at søkeroboter indeksere medlemmenes profiler.<br />Merknad: Gjenkjenning av søkemotorer er ikke perfekt og kan simuleres å være en bruker, så denne funksjonen garantert ikke å begrense adgangen kun for søkemotorene du har lagt til. ';
$helptxt['show_spider_online'] = 'Denne innstillingen lar deg velge om søkerobotene skal vises i oversikter over påloggede brukere og på siden  &quot;Hvem er pålogget&quot;. Alternativene er: <ul class="normallist"> <li> <strong>Overhode ikke</strong><br /> Roboter blir vist som gjest til alle brukerne. </li><li> <strong>Vis antall indekseringsroboter</strong><br /> Viser antall roboter som for øyeblikket besøker forumet. </li><li> <strong>Vis navn på indekseringsrobotene</strong><br /> Alle søkeroboter vil bli vist med navn, slik at brukerne kan se hvor mange av hver søkerobot som for tiden besøker forumet – dette gjelde både visning på forsiden og på sidne over hvem som er pålogget. </li><li> <strong>Vis navn på indekseringsrobotene – kun Admin</strong><br /> Samme som over, bortsett fra at bare administratorer kan se navn på robotene – for alle andre brukere blir robotene vist som gjester. </li> </ul> ';

$helptxt['birthday_email'] = 'Velg hvilken bursdagsmelding som skal brukes ved sending av e-post til brukere som har bursdag. En forhåndsvisning vil bli vist i e-post emne og e-post meldingsfeltene.<br /><strong>Merk:</strong> Dette alternativet aktiverer ikke automatisk sending av e-post på bursdager. Hvis du vil aktivere sending av bursdagshilsen på e-post, gå til siden <a href="%1$s?action=admin;area=scheduledtasks;%3$s=%2$s" target="_blank" class="new_win">Planlagte oppgaver</a> og aktivere sending av e-post på bursdager. ';
$helptxt['pm_bcc'] = 'Når du sender en personlig melding kan du velge å legge til mottaker som &quot;Blindkopi&quot;. Mottakere som mottar blindkopier er ikke synlig for andre mottakere av meldingen.';

$helptxt['move_topics_maintenance'] = 'Dette vil tillate deg å flytte alle innleggene fra ett forum til et annet forum.';
$helptxt['maintain_reattribute_posts'] = 'Du kan bruke denne funksjonen til å tildele gjesteinnlegg på forumet til et registrert medlem. Dette er nyttig hvis for eksempel en bruker slettet sin konto og forandret så mening og ønsker så å få sine gamle innlegg tilbake til sin konto igjen.';
$helptxt['chmod_flags'] = 'Du kan manuelt sette rettighetene du ønsker for de valgte filene. For å gjøre dette, skriv inn chmod verdi som en numerisk (oktett) verdi. Merk - disse verdiene vil ikke ha innvirkning på Microsoft Windows-operativsystemer.';

$helptxt['postmod'] = 'Denne seksjonen tillater moderatorer (med tilstrekkelige rettigheter) å godkjenne innlegg og emner før de vises.';

$helptxt['field_show_enclosed'] = 'Omslutter inndata fra bruker mellom tekst eller html. Dette tillater deg å legge til flere direktemeldingstilbydere, bilder på et emblem osv. For eksempel:<br /><br /> &lt;a href="http://website.com/{INPUT}"&gt;&lt;img src="{DEFAULT_IMAGES_URL}/icon.gif" alt="{INPUT}" /&gt;&lt;/a&gt;<br /><br /> Merk: Du kan bruke følgende variabler:<br /> <ul class="normallist"> <li>{INPUT} - Inndata spesifiserte av brukeren.</li> <li>{SCRIPTURL} - Nettadressen på forumet.</li> <li>{IMAGES_URL} - Nettadressen til bildemappen i brukerens gjeldende tema.</li> <li>{DEFAULT_IMAGES_URL} - Nettadressen til bildemappen i standardtemaet.</li> </ul> ';

$helptxt['custom_mask'] = 'Inndatamasken er viktig for ditt forums sikkerhet. Validering av inndata fra en bruker kan bidra til å sikre at data ikke blir brukt på en måte du ikke forventer. Her er noen tips på enkle regulære uttrykk.<br /><br /> <div class="smalltext" style="margin: 0 2em"> &quot;[A-Za-z]+&quot; - Matche alle store og små bokstaver.<br /> &quot;[0-9]+&quot; - Match alle numeriske tegn.<br /> &quot;[A-Za-z0-9]{7}&quot; - Matche alle store og små bokstaver og numeriske tegn sju ganger.<br /> &quot;[^0-9]?&quot; - Forbyr like tall.<br /> &quot;^([A-Fa-f0-9]{3}|[A-Fa-f0-9]{6})$&quot; - Tillater kun 3 eller 6 heksakodekarakter.<br /> </div><br /><br /> ytterligere kan spesielle metategn ?+*^$ and {xx} defineres. <div class="smalltext" style="margin: 0 2em"> ? - Ingen eller ett treff av tidligere uttrykk.<br /> + - Ett eller flere av forrige uttrykk.<br /> * - Ingen eller flere av forrige uttrykk.<br /> {xx} - Et eksakt tall fra forrige uttrykk.<br /> {xx,} - Et eksakt eller flere tall fra tidligere uttrykk.<br /> {,xx} - Et eksakt eller et mindre tall fra tidligere uttrykk.<br /> {xx,yy} - Nøyaktig treff mellom de to tallene fra tidligere uttrykk.<br /> ^ - På start av strengen.<br /> $ - På slutt av strengen.<br /> \\ - Hoppe over neste tegn.<br /> </div><br /><br /> Mer informasjon og avanserte teknikker finnes på Internett. ';

?>