<?php
// Version: 2.0; ManagePermissions

$txt['permissions_title'] = 'Endre rettigheter';
$txt['permissions_modify'] = 'Rediger';
$txt['permissions_view'] = 'Vis';
$txt['permissions_allowed'] = 'Tillat';
$txt['permissions_denied'] = 'Nektet';
$txt['permission_cannot_edit'] = '<strong>Merk:</strong> Du kan ikke redigere rettighetene i denne profilen, det er en forhåndsdefinert profil inkluderes i forumet som standard. Hvis du ønsker å endre tillatelsene til denne profilen må du først lage en duplikat profil. Du kan utføre denne oppgaven ved å klikke <a href="%1$s">her</a>.';

$txt['permissions_for_profile'] = 'Rettigheter for profilen';
$txt['permissions_boards_desc'] = 'Listen nedenfor viser hvilke sett av rettigheter som er tildelt hver kategori på forumet ditt. Du kan redigere tildelt profilrettigheter ved enten å klikke på navnet eller velg &quot;rediger alle&quot; fra bunnen av siden. Hvis du vil redigere selve profilen er det bare å klikke på profilnavnet.';
$txt['permissions_board_all'] = 'Rediger alle';
$txt['permission_profile'] = 'Rettighetsprofil';
$txt['permission_profile_desc'] = 'Hvilke <a href="%1$s">rettigheter</a> skal forumet bruke.';
$txt['permission_profile_inherit'] = 'Arve fra overordnet forum';

$txt['permissions_profile'] = 'Profil';
$txt['permissions_profiles_desc'] = 'Rettighetsprofiler er tildelt den enkelte kategorier som lar deg enkelt administrere dine sikkerhetsinnstillinger. Fra dette området kan du opprette, redigere og slette rettighetsprofiler.';
$txt['permissions_profiles_change_for_board'] = 'Rediger Rettighetsprofil for: &quot;%1$s&quot;';
$txt['permissions_profile_default'] = 'Standard';
$txt['permissions_profile_no_polls'] = 'Ingen avstemninger';
$txt['permissions_profile_reply_only'] = 'Kun svar';
$txt['permissions_profile_read_only'] = 'Kun lesbar';

$txt['permissions_profile_rename'] = 'Gi nytt navn';
$txt['permissions_profile_edit'] = 'Rediger profiler';
$txt['permissions_profile_new'] = 'Ny profil';
$txt['permissions_profile_new_create'] = 'Opprett';
$txt['permissions_profile_name'] = 'Profilnavn';
$txt['permissions_profile_used_by'] = 'Brukt av';
$txt['permissions_profile_used_by_one'] = '1 Forum';
$txt['permissions_profile_used_by_many'] = '%1$d Forumer';
$txt['permissions_profile_used_by_none'] = 'Ingen fora';
$txt['permissions_profile_do_edit'] = 'Redigér';
$txt['permissions_profile_do_delete'] = 'Slett';
$txt['permissions_profile_copy_from'] = 'Kopier rettigheter fra';

$txt['permissions_includes_inherited'] = 'Arvet gruppe';

$txt['permissions_all'] = 'alle';
$txt['permissions_none'] = 'ingen';
$txt['permissions_set_permissions'] = 'Aktiver rettigheter';

$txt['permissions_advanced_options'] = 'Avanserte alternativer';
$txt['permissions_with_selection'] = 'Med valgte';
$txt['permissions_apply_pre_defined'] = 'Bruk den forhåndsdefinerte rettighetsprofilen';
$txt['permissions_select_pre_defined'] = 'Velg en forhåndsdefinert profil';
$txt['permissions_copy_from_board'] = 'Kopier rettigheter fra denne seksjonen';
$txt['permissions_select_board'] = 'Velg seksjon';
$txt['permissions_like_group'] = 'Sett rettigheter som denne gruppa';
$txt['permissions_select_membergroup'] = 'Velg medlemsgruppe';
$txt['permissions_add'] = 'Legg til rettighet';
$txt['permissions_remove'] = 'Slett rettighet';
$txt['permissions_deny'] = 'Ikke tillat rettighet';
$txt['permissions_select_permission'] = 'Velg en rettighet';

// All of the following block of strings should not use entities, instead use \\" for &quot; etc.
$txt['permissions_only_one_option'] = 'Du kan bare velge et alternativ for å redigere rettighetene';
$txt['permissions_no_action'] = 'Ingenting valgt';
$txt['permissions_deny_dangerous'] = 'Du er nå iferd med å nekte en eller flere rettigheter.\\nDette kan v&ære farlig og gi uventede resultater om du ikke er helt sikker på at ingen er <i>tilfeldigvis</i> i den gruppa eller de gruppene du prøver å nekte tilgang til.\\n\\nEr du sikker på at du vil fortsette?';

$txt['permissions_modify_group'] = 'Endre gruppe';
$txt['permissions_general'] = 'Generelle rettigheter';
$txt['permissions_board'] = 'Globale seksjonbaserte rettigheter';
$txt['permissions_board_desc'] = '<strong>Merk</strong>: Å endre disse forumrettighetene vil påvirke alle kategorier tildelt rettighetsprofil &quot;Standard&quot;. Kategorier som ikke bruker &quot;Standard&quot; profilen vil ikke bli påvirket av endringer på denne siden';
$txt['permissions_commit'] = 'Lagre endringer';
$txt['permissions_on'] = 'på seksjonen';
$txt['permissions_local_for'] = 'Lokale rettigheter for gruppe';
$txt['permissions_option_on'] = 'T';
$txt['permissions_option_off'] = 'I';
$txt['permissions_option_deny'] = 'N';
$txt['permissions_option_desc'] = 'For hver rettighet du setter enten Tillatt (T), Ikke tillatt (I) eller <span style="color: red;">Nektet (N)</span>.<br /><br />Husk på at du ikke bare justerer rettighetene for et medlem, men en hel medlemsgruppe, sånn at alle i den medlemsgruppa vil ha samme rettighetene.<br />Av denne grunn bruk innstillinga Nektet forsiktig og bare <strong>om nødvendig</strong>. Ikke tillatt på en annen side, nekter bare med mindre det ikke er andre innstillinger.';
$txt['permissions_change_view'] = 'Endre visning';
$txt['permissions_view_simple'] = 'Enkel';
$txt['permissions_view_classic'] = 'Klassisk';

$txt['permissiongroup_general'] = 'Generelt';
$txt['permissionname_view_stats'] = 'Vis forumstatistikk';
$txt['permissionhelp_view_stats'] = 'Forumstatistikk er en side som oppsummerer all statistikk på forumet, som antall medlemmer, antall innlegg til dagen og en hel rekke topp 10-lister. Ved å aktivere denne rettigheten vil denne linken komme frem: ([Mer statistikk]).';
$txt['permissionname_view_mlist'] = 'Vis medlemsliste og grupper';
$txt['permissionhelp_view_mlist'] = 'Medlemslisten viser alle medlemmene som er registrert på forumet ditt. Listen kan sorteres og søkes på. Medlemslisten blir lenket til fra både forumets hovedside og statistikksiden, ved å klikke på antall medlemmer.';
$txt['permissionname_who_view'] = 'Vis Hvem er pålogget';
$txt['permissionhelp_who_view'] = 'Hvem er pålogget viser alle medlemmene som er innlogget og hva de driver med. Denne rettigheten vil kun fungere om du har aktivert alternativet i &quot;Installerte modifikasjoner innstillinger og alternativer&quot;. Du kan få tilgang til &quot;Hvem er pålogget&quot; ved å klikke på linken i seksjonen Brukere pålogget på forumets hovedside.';
$txt['permissionname_search_posts'] = 'Søke etter innlegg';
$txt['permissionhelp_search_posts'] = 'Søkerettigheten gir brukeren tilgang til å søke i alle seksjoner han/hun har tilgang til. Når søkerettigheten er aktivert vil en &quot;Søk&quot; knapp være synlig på forumets meny.';
$txt['permissionname_karma_edit'] = 'Redigere andres karma';
$txt['permissionhelp_karma_edit'] = 'Karma er en funksjon som viser populariteten til et medlem. For å kunne bruke denne funksjonen, må den være aktivert i &quot;Installerte modifikasjoner innstillinger og alternativer&quot;. Denne rettigheten vil gi en medlemsgruppe tilgang til å avgi stemme. Denne rettigheten har ingen effekt på gjester.';

$txt['permissiongroup_pm'] = 'Personlige meldinger';
$txt['permissionname_pm_read'] = 'Lese personlige meldinger';
$txt['permissionhelp_pm_read'] = 'Denne rettigheten gir brukeren tilgang til menyen for Personlige meldinger og å lese sine personlige meldinger. Uten denne rettigheten er brukeren ikke istand til å sende private meldinger.';
$txt['permissionname_pm_send'] = 'Sende personlige meldinger';
$txt['permissionhelp_pm_send'] = 'Sende personlige meldinger til registrerte medlemmer. Krever at brukeren har tilgang til å &quot;Lese personlige meldinger&quot;.';

$txt['permissiongroup_calendar'] = 'Kalender';
$txt['permissionname_calendar_view'] = 'Vise kalenderen';
$txt['permissionhelp_calendar_view'] = 'Kalenderen viser for hver måned fødselsdager, hendelser og ferier. Denne rettigheten gir tilgang til denne kalenderen. Når denne rettigheten er aktivert, vil en kalenderknapp bli vist på forumets meny øverst og ikke minst vil kommende fødselsdager og hendelser bli vist nederst på forumets hovedside. Kalenderen må være aktivert fra &quot;Installerte modifikasjoner innstillinger og alternativer&quot;.';
$txt['permissionname_calendar_post'] = 'Opprette hendelser i kalenderen';
$txt['permissionhelp_calendar_post'] = 'En hendelse er et emnet koblet til en spesiell dato eller datoer. Opprettelse av hendelser kan skje fra kalenderen. En hendelse kan kun bli opprettet om brukeren som oppretter hendelsen har tilgang til å lage nye emner.';
$txt['permissionname_calendar_edit'] = 'Redigere hendelser i kalenderen';
$txt['permissionhelp_calendar_edit'] = 'En hendelse er et tema knyttet til en bestemt dato eller datointervall. Hendelsen kan redigeres ved å klikke på den røde stjernen (*) ved siden hendelsen i kalendervisning. For å kunne redigere en hendelse, må brukeren ha tilstrekkelige rettigheter til å redigere den første meldingen om emnet som er knyttet til hendelsen.';
$txt['permissionname_calendar_edit_own'] = 'Egne hendelser';
$txt['permissionname_calendar_edit_any'] = 'Alle hendelser';

$txt['permissiongroup_maintenance'] = 'Forumadministrasjon';
$txt['permissionname_admin_forum'] = 'Administrere forumet og databasen';
$txt['permissionhelp_admin_forum'] = 'Denne rettigheten gir medlemmet tilgang til å redigere forumets innstillinger, design innstillinger, behandle pakker og bruke forumets database og vedlikeholdsverktøy. Bruk denne rettigheten med omtanke, ettersom den er veldig sårbar for forumet.';
$txt['permissionname_manage_boards'] = 'Administrere forumer og kategorier';
$txt['permissionhelp_manage_boards'] = 'Denne rettigheten gir medlemmet tilgang til å opprette, redigere og fjerne fora og kategorier.';
$txt['permissionname_manage_attachments'] = 'Administrere vedlegg og avatarer';
$txt['permissionhelp_manage_attachments'] = 'Denne rettigheten gir medlemmet tilgang til administrasjon av vedlegg, hvor avatarer og vedlegg kan bli fjernet.';
$txt['permissionname_manage_smileys'] = 'Administrere smilefjes og innleggs-symboler';
$txt['permissionhelp_manage_smileys'] = 'Dette gir tilgang til å administrere smilefjes. Her kan du legge til, redigere og fjerne smilefjes og sett med smilefjes. Hvis du har aktivert tilpasset meldingsikoner du kan også legge til og redigere meldingsikoner med denne tillatelsen.';
$txt['permissionname_edit_news'] = 'Redigere nyheter';
$txt['permissionhelp_edit_news'] = 'Nyhetsfunksjonen gjør slik at en tilfeldig nyhetsak vil komme frem på hver side på forumet. For å kunne bruke nyhetsfunksjonen, må den være aktivert i forumets innstillinger.';
$txt['permissionname_access_mod_center'] = 'Åpne moderatorsentret';
$txt['permissionhelp_access_mod_center'] = 'Med denne rettigheten kan alle medlemmer av denne gruppen få tilgang til modereringssenteret, hvor de vil ha tilgang til funksjonalitet for å lette moderering. Merk at dette gir ikke i seg selv noen modereringsprivilegier.';

$txt['permissiongroup_member_admin'] = 'Administrasjon av medlemmer';
$txt['permissionname_moderate_forum'] = 'Redigere på forumets medlemmer';
$txt['permissionhelp_moderate_forum'] = 'Denne rettigheten inkluderer alle viktige moderatorfunksjoner:<ul class="normallist"><li>tilgang til å vise registeringserklæring</li><li>tilgang til å vise/slette medlemsbildet</li><li>utvidet profilinformasjon som å kunne spore IP/bruker og (skjult) innlogging</li><li>aktivere kontoer</li><li>motta godkjennelsesvarsler og godkjenne kontoer</li><li>immun mot ignorerte PMer</li><li>andre småting</li></ul>';
$txt['permissionname_manage_membergroups'] = 'Administrere medlemsgrupper';
$txt['permissionhelp_manage_membergroups'] = 'Denne rettigheten gir medlemmet tilgang til å redigere medlemsgrupper og tildele disse til medlemmer.';
$txt['permissionname_manage_permissions'] = 'Administrere rettigheter';
$txt['permissionhelp_manage_permissions'] = 'Denne rettigheten gir medlemmet tilgang til å redigere alle rettigheter til en medlemsgruppe, globalt eller for enkelt-forumer.';
$txt['permissionname_manage_bans'] = 'Administrere utestengelser ';
$txt['permissionhelp_manage_bans'] = 'Denne rettigheten gir medlemmet tilgang til å legge til eller fjerne brukernavn, IP-adresser, ISP og epost-adresser på en liste over utestengte medlemmer. Den gir også tilgang til å se på og fjerne elementer i loggen , hvor bannlyste medlemmer har forsøkt å logge inn.';
$txt['permissionname_send_mail'] = 'Sende en e-post til medlemmene';
$txt['permissionhelp_send_mail'] = 'Sender en e-post til alle forumets medlemmer, eller bare noen spesielle medlemsgrupper på mail eller som PM. (PM alternativet krever tilgang til å &quot;sende private meldinger&quot;).';
$txt['permissionname_issue_warning'] = 'Gi advarsler til medlemmer';
$txt['permissionhelp_issue_warning'] = 'Gi en advarsel til medlemmer av forumet og endre medlemmets advarselnivå. Krever at advarselssystem må være aktivert.';

$txt['permissiongroup_profile'] = 'Medlemsprofiler';
$txt['permissionname_profile_view'] = 'Vise profilen og statistikk for medlemmet';
$txt['permissionhelp_profile_view'] = 'Denne rettigheten gir medlemmer tilgangen til å klikke på et brukernavn for å se profilen til medlemmet og noe statistikk, samt alle innleggene for medlemmet.';
$txt['permissionname_profile_view_own'] = 'Egen profil';
$txt['permissionname_profile_view_any'] = 'Alle profiler';
$txt['permissionname_profile_identity'] = 'Redigere innstillinger for konto';
$txt['permissionhelp_profile_identity'] = 'Innstillingene for kontoen er de vanlige innstillingene på profilen som passord, e-postadresse, medlemsgruppe og språk.';
$txt['permissionname_profile_identity_own'] = 'Egen profil';
$txt['permissionname_profile_identity_any'] = 'Alle profiler';
$txt['permissionname_profile_extra'] = 'Redigere ekstrainnstillinger for profil';
$txt['permissionhelp_profile_extra'] = 'Ekstrainntillingene for profilen inkluderer innstillinger for avatar, alternativer for design, varsling og personlige meldinger.';
$txt['permissionname_profile_extra_own'] = 'Egen profil';
$txt['permissionname_profile_extra_any'] = 'Alle profiler';
$txt['permissionname_profile_title'] = 'Redigere egendefinert tittel';
$txt['permissionhelp_profile_title'] = 'Den egendefinerte tittelen blir vist på emnesidene under brukernavnet på den som har en egendefinert tittel.';
$txt['permissionname_profile_title_own'] = 'Egen profil';
$txt['permissionname_profile_title_any'] = 'Alle profiler';
$txt['permissionname_profile_remove'] = 'Slett konto';
$txt['permissionhelp_profile_remove'] = 'Denne rettigheten gir medlemmet tilgang til å slette sin egen konto om det er satt til &quot;Egen konto&quot;.';
$txt['permissionname_profile_remove_own'] = 'Egen konto';
$txt['permissionname_profile_remove_any'] = 'Alle kontoer';
$txt['permissionname_profile_server_avatar'] = 'Velge en avatar fra serveren';
$txt['permissionhelp_profile_server_avatar'] = 'Om dette er aktivert vil du tillate medlemmene å velge en avatar som er lagret på serveren.';
$txt['permissionname_profile_upload_avatar'] = 'Laste opp en avatar til serveren';
$txt['permissionhelp_profile_upload_avatar'] = 'Denne rettigheten vil tillate et medlem å laste opp sin avatar til serveren.';
$txt['permissionname_profile_remote_avatar'] = 'Velge en ekstern avatar';
$txt['permissionhelp_profile_remote_avatar'] = 'Siden avatarer kan påvirke negativt på nedlastningstiden, er det mulig å nekte medlemsgrupper å bruke avatarer fra eksterne avatarer.';

$txt['permissiongroup_general_board'] = 'Generelt';
$txt['permissionname_moderate_board'] = 'Forum-moderator';
$txt['permissionhelp_moderate_board'] = 'Ved å være forum-moderator vil noen småknapper komme frem. Rettighetene inkluderer å svare til stengte emner, redigere når en avstemning skal utløpe og vise resultater i en avstemning selv om den ikke er utløpt. ';

$txt['permissiongroup_topic'] = 'Emner';
$txt['permissionname_post_new'] = 'Opprette nye emner';
$txt['permissionhelp_post_new'] = 'Denne rettigheten gjør slik at medlemmene kan opprette nye emner, men ikke å svare på emnene.';
$txt['permissionname_merge_any'] = 'Slå sammen alle emner';
$txt['permissionhelp_merge_any'] = 'Slå sammen to eller flere emner til ett. Sorteringen av innlegg blir etter dato, noe som kan føre til at rekkefølgen blir forandret deretter. Et medlem kan kun slå sammen emner i de forumene medlemmet har tilgang til å gjøre dette. For å kunne slå sammen flere emner på en gang, må knappene for hurtigmod være aktivert i profilen.';
$txt['permissionname_split_any'] = 'Dele opp alle emner';
$txt['permissionhelp_split_any'] = 'Dele et emne til to ulike emner.';
$txt['permissionname_send_topic'] = 'Sende emner til venner';
$txt['permissionhelp_send_topic'] = 'Denne rettigheten gir medlemmet muligheten til å sende en e-post med et tips til emnet ved å skrive inn mottakerens e-postadresse og legge til en kommentar.';
$txt['permissionname_make_sticky'] = 'Gjøre emner viktige';
$txt['permissionhelp_make_sticky'] = 'Viktige emner er emner som alltid er øverst i en forum. De brukes ofte til kunngjøringer eller andre viktige beskjeder.';
$txt['permissionname_move'] = 'Flytte emner';
$txt['permissionhelp_move'] = 'Flytte emner fra en forum til en annen. Medlemmene kan bare velge forumer de har tilgang til.';
$txt['permissionname_move_own'] = 'Egne emner';
$txt['permissionname_move_any'] = 'Alle emner';
$txt['permissionname_lock'] = 'Stenge emner';
$txt['permissionhelp_lock'] = 'Denne rettigheten gjør slik at medlemmer kan stenge et emne. Dette kan gjøre slik at ingen kan svare på emnet. Kun medlemmer med en &quot;forumsmoderator&quot; tilgang kan fremdeles svare i stengte emner.';
$txt['permissionname_lock_own'] = 'Egne emner';
$txt['permissionname_lock_any'] = 'Alle emner';
$txt['permissionname_remove'] = 'Slette emner';
$txt['permissionhelp_remove'] = 'Sletter hele emner fra forumet. Merk at denne rettigheten ikke gir tilgang til kun å slette innlegg fra et emne!';
$txt['permissionname_remove_own'] = 'Egne emner';
$txt['permissionname_remove_any'] = 'Alle emner';
$txt['permissionname_post_reply'] = 'Svare på emner';
$txt['permissionhelp_post_reply'] = 'Denne rettigheten tillater medlemmet å svare på emner.';
$txt['permissionname_post_reply_own'] = 'Egne emner';
$txt['permissionname_post_reply_any'] = 'Alle emner';
$txt['permissionname_modify_replies'] = 'Redigere svar til egne emner';
$txt['permissionhelp_modify_replies'] = 'Denne rettigheten gir medlemmet som startet et emne til å redigere alle svarene i sitt emne.';
$txt['permissionname_delete_replies'] = 'Slette svar til egne emner';
$txt['permissionhelp_delete_replies'] = 'Denne rettigheten gir medlemmet som startet et emne til å slette alle svarene i sitt emne.';
$txt['permissionname_announce_topic'] = 'Sett emne som kunngjøring';
$txt['permissionhelp_announce_topic'] = 'Denne rettigheten gir medlemmet tilgang til å sende en epost om kunngjøring, til alle medlemmer eller noen få medlemsgrupper.';

$txt['permissiongroup_post'] = 'Innlegg';
$txt['permissionname_delete'] = 'Slette innlegg';
$txt['permissionhelp_delete'] = 'Sletter innlegg. Dette gir ikke tilgang til å slette det første innlegget i et emne.';
$txt['permissionname_delete_own'] = 'Egne innlegg';
$txt['permissionname_delete_any'] = 'Alle innlegg';
$txt['permissionname_modify'] = 'Redigere innlegg';
$txt['permissionhelp_modify'] = 'Redigerer innlegg';
$txt['permissionname_modify_own'] = 'Egne innlegg';
$txt['permissionname_modify_any'] = 'Alle innlegg';
$txt['permissionname_report_any'] = 'Rapportere innlegg til moderator';
$txt['permissionhelp_report_any'] = 'Denne rettigheten legger til en link til hvert innlegg som gir medlemmene tilgang til å rapportere innlegg til moderator. Ved rapportering blir alle moderatorer for det forumet tilsendt en e-post med en link til det rapporterte innlegget og en kommentar fra medlemmet som rapporterte.';

$txt['permissiongroup_poll'] = 'Avstemninger';
$txt['permissionname_poll_view'] = 'Vise avstemninger';
$txt['permissionhelp_poll_view'] = 'Denne rettigheten gir medlemmet tilgang til å vise avstemninger. Without this permission, the user will only see the topic.';
$txt['permissionname_poll_vote'] = 'Stemme i avstemninger';
$txt['permissionhelp_poll_vote'] = 'Denne rettigheten gir et registrert medlem til å avgi stemme i avstemninger. Denne gjelder ikke for gjester.';
$txt['permissionname_poll_post'] = 'Lage avstemninger';
$txt['permissionhelp_poll_post'] = 'Denne rettigheten gir et medlem å lage nye avstemninger. Siden avstemninger er en spesiell type emner kan den ikke brukes uten at medlemmet har tilgang til rettigheten \'Opprette nye emner\'.';
$txt['permissionname_poll_add'] = 'Legge til avstemninger til emner';
$txt['permissionhelp_poll_add'] = 'Denne rettigheten gir medlemmer tilgang til å legge til en avstemning til et emne som allerede eksisterer. Denne rettigheten krever at medlemmet har tilgang til å redigere det første innlegget i et emne.';
$txt['permissionname_poll_add_own'] = 'Egne emner';
$txt['permissionname_poll_add_any'] = 'Alle emner';
$txt['permissionname_poll_edit'] = 'Redigere avstemninger';
$txt['permissionhelp_poll_edit'] = 'Denne rettigheten gir medlemmer tilgang til å redigere alternativene for en avstemning og nullstille antall stemmer. For å kunne redigere maks antall stemmer og varighet må medlemmet også ha rettigheten &quot;forummoderator&quot;.';
$txt['permissionname_poll_edit_own'] = 'Egne avstemninger';
$txt['permissionname_poll_edit_any'] = 'Alle avstemninger';
$txt['permissionname_poll_lock'] = 'Stenge avstemninger';
$txt['permissionhelp_poll_lock'] = 'Stengte avstemninger kan ikke motta flere stemmer.';
$txt['permissionname_poll_lock_own'] = 'Egne avstemninger';
$txt['permissionname_poll_lock_any'] = 'Alle avstemninger';
$txt['permissionname_poll_remove'] = 'Slette avstemninger';
$txt['permissionhelp_poll_remove'] = 'Denne rettigheten tillater sletting av avstemninger.';
$txt['permissionname_poll_remove_own'] = 'Egne avstemninger';
$txt['permissionname_poll_remove_any'] = 'Alle avstemninger';

$txt['permissiongroup_approval'] = 'Moderering av innlegg';
$txt['permissionname_approve_posts'] = 'Godkjenne elementer som venter på moderering';
$txt['permissionhelp_approve_posts'] = 'Denne rettigheten lar brukeren godkjenne alle ikke-godkjente elementer på et forum.';
$txt['permissionname_post_unapproved_replies'] = 'Legge til svar på emner, men ikke vis før godkjent';
$txt['permissionhelp_post_unapproved_replies'] = 'Denne tillatelsen lar brukeren svar på emner. Svarene vil ikke bli vist før den er godkjent av en moderator.';
$txt['permissionname_post_unapproved_replies_own'] = 'Egne emner';
$txt['permissionname_post_unapproved_replies_any'] = 'Alle emner';
$txt['permissionname_post_unapproved_topics'] = 'Legge til nye emner, men ikke vis før godkjent';
$txt['permissionhelp_post_unapproved_topics'] = 'Denne rettigheten lar brukeren starte et nytt tema som vil kreve godkjenning før de blir vist.';
$txt['permissionname_post_unapproved_attachments'] = 'Legge til vedlegg, men skjules til godkjent';
$txt['permissionhelp_post_unapproved_attachments'] = 'Denne tillatelsen lar brukeren legge ved filer til innlegg. Vedlagte filer vil da kreve godkjenning før den vises for andre brukere.';

$txt['permissiongroup_notification'] = 'Varsling';
$txt['permissionname_mark_any_notify'] = 'Be om varsling ved svar';
$txt['permissionhelp_mark_any_notify'] = 'Denne funksjonen gir medlemmer tilgang til å få varsel på e-post når noen svarer til et emne de har abonnert på.';
$txt['permissionname_mark_notify'] = 'Be om varsling ved nye emner';
$txt['permissionhelp_mark_notify'] = 'Varsling ved nye emner er en funksjon som lar medlemmer få varsel på e-post om noen lager nye emner i forumer de har abonnert på.';

$txt['permissiongroup_attachment'] = 'Vedlegg';
$txt['permissionname_view_attachments'] = 'Vise vedlegg';
$txt['permissionhelp_view_attachments'] = 'Vedlegg er filer som er vedlagt til skrevne innlegg. Denne funksjonen kan aktiveres og konfigureres i &quot;Rediger installerte modifikasjoner&quot;. Siden vedlegg ikke blir hentet fra direkte, kan du beskytte dem fra å bli nedlastet fra medlemmer som ikke har denne rettigheten.';
$txt['permissionname_post_attachment'] = 'Legge ved filer';
$txt['permissionhelp_post_attachment'] = 'Vedlegg er filer som er vedlagt til skrevne innlegg. Et innlegg kan inneholde flere vedlegg.';

$txt['permissiongroup_simple_view_basic_info'] = 'Bruke de grunnleggende forum funksjonalitetene';
$txt['permissiongroup_simple_use_pm_system'] = 'Kontakte medlemmer ved brukav det personlige meldingssystemet';
$txt['permissiongroup_simple_post_calendar'] = 'Legge til hendelser på kalenderen';
$txt['permissiongroup_simple_edit_profile'] = 'Tilpasse sin profil';
$txt['permissiongroup_simple_delete_account'] = 'Slette sin egen konto';
$txt['permissiongroup_simple_use_avatar'] = 'Velg eller laste opp en avatar';
$txt['permissiongroup_simple_moderate_general'] = 'Moderere hele forumet';
$txt['permissiongroup_simple_administrate'] = 'Utføre administrative oppgaver';

$txt['permissionname_simple_calendar_edit_own'] = 'Redigere sine egne kalenderhendelser';
$txt['permissionname_simple_calendar_edit_any'] = 'Redigere andres kalenderhendelser';
$txt['permissionname_simple_profile_view_own'] = 'Se sin egen profil';
$txt['permissionname_simple_profile_view_any'] = 'Se andres profiler';
$txt['permissionname_simple_profile_identity_own'] = 'Redigere egne kontoinnstillinger';
$txt['permissionname_simple_profile_identity_any'] = 'Redigere andres kontoinnstillinger';
$txt['permissionname_simple_profile_extra_own'] = 'Redigere egne ekstra profilalternativer';
$txt['permissionname_simple_profile_extra_any'] = 'Rediger andres ekstra profilalternativer';
$txt['permissionname_simple_profile_title_own'] = 'Velge en egendefinert tittel for seg selv';
$txt['permissionname_simple_profile_title_any'] = 'Rediger andres egendefinerte titler';
$txt['permissionname_simple_profile_remove_own'] = 'Slette sin egen konto';
$txt['permissionname_simple_profile_remove_any'] = 'Slette andres kontoer';

$txt['permissiongroup_simple_make_unapproved_posts'] = 'Legge til emner og svar til forumet <span style="text-decoration: underline">bare</span> etter at de har blitt godkjent';
$txt['permissiongroup_simple_make_posts'] = 'Legge til emner og svar til forumet';
$txt['permissiongroup_simple_post_polls'] = 'Lage nye spørreundersøkelser';
$txt['permissiongroup_simple_participate'] = 'Se flere foruminnhold';
$txt['permissiongroup_simple_modify'] = 'Endre egne innlegg';
$txt['permissiongroup_simple_notification'] = 'Be om varsling';
$txt['permissiongroup_simple_attach'] = 'Legge til vedlegg';
$txt['permissiongroup_simple_moderate'] = 'Moderere forumet';

$txt['permissionname_simple_post_unapproved_replies_own'] = 'Legge til svar på egne emne - men krever godkjenning';
$txt['permissionname_simple_post_unapproved_replies_any'] = 'Legge til svar på hvilket som helst emne - men krever godkjenning';
$txt['permissionname_simple_post_reply_own'] = 'Legge til svar på et emne de startet';
$txt['permissionname_simple_post_reply_any'] = 'Legge til svar på ethvert emne';
$txt['permissionname_simple_move_own'] = 'Flytte egne emner';
$txt['permissionname_simple_move_any'] = 'Flytte andre sine emner';
$txt['permissionname_simple_lock_own'] = 'Låse egne emner';
$txt['permissionname_simple_lock_any'] = 'Låse andre sine emner';
$txt['permissionname_simple_remove_own'] = 'Fjerne egne emner';
$txt['permissionname_simple_remove_any'] = 'Fjerne andre sine emner';
$txt['permissionname_simple_delete_own'] = 'Slette egne innlegg';
$txt['permissionname_simple_delete_any'] = 'Slette andre sine innlegg';
$txt['permissionname_simple_modify_own'] = 'Endre egne innlegg';
$txt['permissionname_simple_modify_any'] = 'Endre andre sine innlegg';
$txt['permissionname_simple_poll_add_own'] = 'Legge til avstemning til emne de har opprettet';
$txt['permissionname_simple_poll_add_any'] = 'Legge til avstemning i alle emner';
$txt['permissionname_simple_poll_edit_own'] = 'Redigere avstemning de selv har opprettet';
$txt['permissionname_simple_poll_edit_any'] = 'Redigere alle avstemninger';
$txt['permissionname_simple_poll_lock_own'] = 'Låse egne avstemninger';
$txt['permissionname_simple_poll_lock_any'] = 'Låse andre sine avstemninger';
$txt['permissionname_simple_poll_remove_own'] = 'Fjerne egne avstemninger';
$txt['permissionname_simple_poll_remove_any'] = 'Fjerne andre sine avstemninger';

$txt['permissionicon'] = '';

$txt['permission_settings_title'] = 'Rettighetsinnstillinger';
$txt['groups_manage_permissions'] = 'Medlemsgrupper med tilgang til å behandle rettigheter';
$txt['permission_settings_submit'] = 'Lagre';
$txt['permission_settings_enable_deny'] = 'Aktivere funksjonen til å nekte rettigheter';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['permission_disable_deny_warning'] = 'Ved å slå av denne funksjonen vil &quot;Nektet&quot;-rettigheter bli endret til &quot;Ikke tillatt&quot;.';
$txt['permission_by_board_desc'] = 'Her kan du stille inn om et forum bruker globale rettigheter eller en spesifikk rettighetsprofil. Ved å bruke lokale rettigheter menes det kun for det forumet. Du kan sette rettighet for hver medlemsgruppe.';
$txt['permission_settings_desc'] = 'Her kan du stille inn hvem som har tilgang til å endre rettigheter og hvor avansert rettighetssysteme skal være.';
$txt['permission_settings_enable_postgroups'] = 'Aktivere rettigheter for innleggsbaserte grupper';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['permission_disable_postgroups_warning'] = 'Deaktivering av denne innstillingen vil fjerne rettighetene som er satt for innleggsbaserte grupper.';

$txt['permissions_post_moderation_desc'] = 'Fra denne siden kan du enkelt endre hvilke grupper som får sine innlegg moderert for en bestemt rettighetsprofil.';
$txt['permissions_post_moderation_deny_note'] = 'Merk: Selv om du har avanserte rettigheter aktivert kan du ikke bruke &quot;nekte&quot; rettigheter fra denne siden. Vennligst rediger rettighetene direkte hvis du ønsker å bruke en nekte-rettighet.';
$txt['permissions_post_moderation_select'] = 'Velg profil';
$txt['permissions_post_moderation_new_topics'] = 'Nye emner';
$txt['permissions_post_moderation_replies_own'] = 'Egne svar';
$txt['permissions_post_moderation_replies_any'] = 'Alle svar';
$txt['permissions_post_moderation_attachments'] = 'Vedlegg';
$txt['permissions_post_moderation_legend'] = 'Forklaring';
$txt['permissions_post_moderation_allow'] = 'Kan opprette';
$txt['permissions_post_moderation_moderate'] = 'Kan opprette, men krever godkjenning';
$txt['permissions_post_moderation_disallow'] = 'Kan ikke opprette';
$txt['permissions_post_moderation_group'] = 'Gruppe';

$txt['auto_approve_topics'] = 'Legge til nye emner, uten å kreve godkjenning';
$txt['auto_approve_replies'] = 'Legge til svar på emner, uten å kreve godkjenning';
$txt['auto_approve_attachments'] = 'Legge til vedlegg, uten å kreve godkjenning';

?>