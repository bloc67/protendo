<?php
// Version: 2.0; Login

global $context;

// Important! Before editing these language files please read the text at the top of index.english.php.
// Registration agreement page.
$txt['registration_agreement'] = 'registreringserklæring';
$txt['agreement_agree'] = 'Jeg godtar betingelsene i avtalen.';
$txt['agreement_agree_coppa_above'] = 'Jeg godtar betingelsene i avtalen og er minst %1$d år.';
$txt['agreement_agree_coppa_below'] = 'Jeg godtar betingelsene i avtalen og er yngre enn %1$d år.';

// Registration form.
$txt['registration_form'] = 'Registreringsskjema';
$txt['need_username'] = 'Du må skrive inn et brukernavn.';
$txt['no_password'] = 'Du må skrive inn et passord.';
$txt['incorrect_password'] = 'Feil passord';
$txt['choose_username'] = 'Velg et brukernavn';
$txt['maintain_mode'] = 'Vedlikeholdsmodus';
$txt['registration_successful'] = 'Registreringen var vellykket';
$txt['now_a_member'] = 'Gratulerer! Du er nå medlem av forumet.';
// Use numeric entities in the below string.
$txt['your_password'] = 'og passordet ditt er';
$txt['valid_email_needed'] = 'Skriv inn en gyldig e-postadresse, %1$s.';
$txt['required_info'] = 'Påkrevd informasjon';
$txt['identification_by_smf'] = 'Benyttes kun for identifisering internt i forumsystemet.';
$txt['additional_information'] = 'Tilleggsinformasjon';
$txt['warning'] = 'Advarsel!';
$txt['only_members_can_access'] = 'Bare registrerte medlemmer har tilgang til denne seksjonen.';
$txt['login_below'] = 'Vennligst logg inn nedenfor eller';
$txt['register_an_account'] = 'Registrer deg som medlem';
$txt['login_with_forum'] = 'på %1$s.';
// Use numeric entities in the below two strings.
$txt['may_change_in_profile'] = 'Du må gjerne endre passordet etter at du er logget inn ved å gå til profilen din, eller ved å gå til denne siden etter du er innlogget:';
$txt['your_username_is'] = 'Brukernavnet ditt er: ';

$txt['login_hash_error'] = 'Passord-sikkerheten har nylig blitt oppgradert. Vennligst skriv inn ditt passord på nytt.';

$txt['ban_register_prohibited'] = 'Beklager, men du har ikke tillatelse å registrere deg på forumet.';
$txt['under_age_registration_prohibited'] = 'Beklager, men brukere med alder under %1$d år får ikke lov å regisrere seg på dette forumet.';

$txt['activate_account'] = 'Aktivering av konto';
$txt['activate_success'] = 'Din konto har blitt aktivert. Du kan nå logge inn.';
$txt['activate_not_completed1'] = 'Din e-postadresse må godkjennes før du kan logge inn.';
$txt['activate_not_completed2'] = 'Trenger du en ny aktiveringsmail?';
$txt['activate_after_registration'] = 'Takk for din registrering. Du vil motta en e-post snart med en link for å aktivere kontoen din. Hvis du ikke mottar en e-post etter en tid, sjekke spam-mappen.';
$txt['invalid_userid'] = 'Brukeren eksisterer ikke';
$txt['invalid_activation_code'] = 'Ugyldig aktiveringskode';
$txt['invalid_activation_username'] = 'Brukernavn eller e-post';
$txt['invalid_activation_new'] = 'Dersom du registrerte deg med feil e-postadresse, skriv inn en ny adresse og ditt passord her.';
$txt['invalid_activation_new_email'] = 'Ny e-postadresse';
$txt['invalid_activation_password'] = 'Gammelt passord';
$txt['invalid_activation_resend'] = 'Send aktiveringskode på nytt';
$txt['invalid_activation_known'] = 'Dersom du allerede har en aktiveringskode, skriv den inn her.';
$txt['invalid_activation_retry'] = 'Aktiveringskode';
$txt['invalid_activation_submit'] = 'Aktiver';

$txt['coppa_no_concent'] = 'Administrator har ennå ikke mottatt foreldres/foresattes samtykke for ditt medlemskap.';
$txt['coppa_need_more_details'] = 'Trenger du mer detaljer?';

$txt['awaiting_delete_account'] = 'Ditt medlemskap er markert for sletting!<br />Hvis du vil gjenopprette ditt medlemskap, vennligst kryss av for &quot;Gjenopprett mitt medlemskap&quot;, og logg inn på nytt.';
$txt['undelete_account'] = 'Gjenopprett mitt medlemskap';

// Use numeric entities in the below three strings.
$txt['change_password'] = 'Nytt passord';
$txt['change_password_login'] = 'Dine innloggingsdata på';
$txt['change_password_new'] = 'er forandret og ditt passord er blitt endret. Nedenfor er dine nye innloggingsdata.';

$txt['in_maintain_mode'] = 'Dette forumet er under vedlikehold.';

// These two are used as a javascript alert; please use international characters directly, not as entities.
$txt['register_agree'] = 'Du må lese og akseptere bekreftelsen for å fullføre registreringen.';
$txt['register_passwords_differ_js'] = 'De to passordene du skrev inn var ikke like!';

$txt['approval_after_registration'] = 'Takk for at du registrerte deg. Admin må nå godkjenne ditt medlemskap før du kan logge inn til kontoen din, du vil snart motta en mail som forteller hva admin har bestemt.';

$txt['admin_settings_desc'] = 'Her kan du endre diverse innstillinger vedrørende registrering av nye medlemmer.';

$txt['setting_enableOpenID'] = 'Tillat brukere å registrere seg med OpenID';

$txt['setting_registration_method'] = 'Metode for registrering av nye medlemmer';
$txt['setting_registration_disabled'] = 'Registrering deaktivert';
$txt['setting_registration_standard'] = 'Øyeblikkelig registering';
$txt['setting_registration_activate'] = 'E-post aktivisering';
$txt['setting_registration_approval'] = 'Admin godkjenning';
$txt['setting_notify_new_registration'] = 'Varsle administratorer når et nytt medlem registres';
$txt['setting_send_welcomeEmail'] = 'Send velkomst-e-post til nye medlemmer';

$txt['setting_coppaAge'] = 'Aldersgrense for å tillate registrering uten restriksjoner';
$txt['setting_coppaAge_desc'] = '(Sett denne til 0 for å deaktivere aldersgrense)';
$txt['setting_coppaType'] = 'Handling som utføres hvis en bruker under aldersgrensen registrerer seg';
$txt['setting_coppaType_reject'] = 'Avvis registreringen';
$txt['setting_coppaType_approval'] = 'Krev samtykke fra foreldre/foresatte';
$txt['setting_coppaPost'] = 'Postadresse samtykkeskjema skal sendes';
$txt['setting_coppaPost_desc'] = 'Gjelder kun dersom aldersgrense er aktivert';
$txt['setting_coppaFax'] = 'Faks-number samtykkeskjema skal sendes til';
$txt['setting_coppaPhone'] = 'Kontakt-telefon foreldre/foresatte kan ringe for spørsmål angående aldersgrense';

$txt['admin_register'] = 'Registrer nytt medlem';
$txt['admin_register_desc'] = 'Fra her kan du registrere nye medlemmer til forumet og hvis ønskelig sende dem en mail med opplysningene.';
$txt['admin_register_username'] = 'Nytt brukernavn';
$txt['admin_register_email'] = 'E-postadresse';
$txt['admin_register_password'] = 'Passord';
$txt['admin_register_username_desc'] = 'Brukernavn for det nye medlemmet';
$txt['admin_register_email_desc'] = 'E-postadresse til medlemmet';
$txt['admin_register_password_desc'] = 'Passord for nytt medlem';
$txt['admin_register_email_detail'] = 'Send e-post med nytt passord til medlemmet';
$txt['admin_register_email_detail_desc'] = 'E-postadresse er påkrevd uansett';
$txt['admin_register_email_activate'] = 'Krev aktivering av brukerkonto';
$txt['admin_register_group'] = 'Primær medlemsgruppe';
$txt['admin_register_group_desc'] = 'Medlemsgruppe medlemmet vil primært høre til';
$txt['admin_register_group_none'] = '(ingen primær medlemsgruppe)';
$txt['admin_register_done'] = 'Medlem %1$s ble vellykket registrert!';

$txt['coppa_title'] = 'Forum med aldersgrense';
$txt['coppa_after_registration'] = 'Takk for at du registrerte deg på ' . $context['forum_name_html_safe'] . '.<br /><br />Fordi du er under {MINIMUM_AGE} år, er det lovpålagt 
	å innhente dine foreldres eller foresattes samtykke før du kan begynne å bruke ditt medlemskap. For å tilrettelegge for aktivering av medlemskapet vennligst skriv ut skjemaet nedenfor:';
$txt['coppa_form_link_popup'] = 'Vis skjema i nytt vindu';
$txt['coppa_form_link_download'] = 'Last ned skjema som tekstfil';
$txt['coppa_send_to_one_option'] = 'Deretter må dine foreldre/foresatte fylle ut og sende inn skjemaet via:';
$txt['coppa_send_to_two_options'] = 'Deretter må dine foreldre/foresatte fylle ut og sende inn skjemaet via en av:';
$txt['coppa_send_by_post'] = 'Post, til følgende adresse:';
$txt['coppa_send_by_fax'] = 'Faks, til følgende nummer:';
$txt['coppa_send_by_phone'] = 'Alternativt, få dem til å kontakte administrator på telefon {PHONE_NUMBER}.';

$txt['coppa_form_title'] = 'Samtykkeskjema for registrering på ' . $context['forum_name_html_safe'];
$txt['coppa_form_address'] = 'Adresse';
$txt['coppa_form_date'] = 'Dato';
$txt['coppa_form_body'] = 'Jeg {PARENT_NAME},<br /><br />gir {CHILD_NAME} (barnets navn) tilatelse til å bli registrert som medlem av forumet: ' . $context['forum_name_html_safe'] . ', med brukernavn: {USER_NAME}.<br /><br />Jeg er klar over at personlig informasjon skrevet inn av {USER_NAME} kan bli synlig for andre brukere av forumet.<br /><br />Signatur:<br />{PARENT_NAME} (foreldre/foresatte).';

$txt['visual_verification_sound_again'] = 'Spill igjen';
$txt['visual_verification_sound_close'] = 'Steng vindu';
$txt['visual_verification_sound_direct'] = 'Problemer med å høre dette? Prøv en direkte link til det.';

// Use numeric entities in the below.
$txt['registration_username_available'] = 'Brukernavn er tilgjengelig';
$txt['registration_username_unavailable'] = 'Brukernavn er ikke tilgjengelig';
$txt['registration_username_check'] = 'Sjekk om brukernavnet er tilgjengelig';
$txt['registration_password_short'] = 'Passordet er for kort';
$txt['registration_password_reserved'] = 'Passordet inneholder ditt brukernavn eller epost';
$txt['registration_password_numbercase'] = 'Passordet må bestå av både store og små bokstaver, og tall';
$txt['registration_password_no_match'] = 'Passordene stemmer ikke';
$txt['registration_password_valid'] = 'Passord er gyldig';

$txt['registration_errors_occurred'] = 'Følgende feil ble oppdaget i registreringen. Rett de før du forsetter:';

$txt['authenticate_label'] = 'Godkjennings metode';
$txt['authenticate_password'] = 'Passord';
$txt['authenticate_openid'] = 'OpenID';
$txt['authenticate_openid_url'] = 'OpenID Godkjennings URL';

?>