<?php
// Version: 2.0; ManageCalendar

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['calendar_desc'] = 'Her kan du endre alt som har med kalenderen å gjøre.';

// Calendar Settings
$txt['calendar_settings_desc'] = 'Her kan du aktivere kalenderen, og velge hvilke innstillinger den skal bruke.';
$txt['save_settings'] = 'Lagre innstillinger';
$txt['groups_calendar_view'] = 'Medlemsgrupper med rett til å vise kalenderen';
$txt['groups_calendar_post'] = 'Medlemsgrupper med rett til å lage nye hendelser';
$txt['groups_calendar_edit_own'] = 'Medlemsgrupper med rett til å redigere sine egne hendelser';
$txt['groups_calendar_edit_any'] = 'Medlemsgrupper med rett til å redigere andres hendelser';
$txt['setting_cal_enabled'] = 'Aktiver kalenderen';
$txt['setting_cal_daysaslink'] = 'Vis dager som lenker til \'ny hendelse\'';
$txt['setting_cal_days_for_index'] = 'Maks antall forestående dager som skal vises på forum-oversikten';
$txt['setting_cal_showholidays'] = 'Vis helligdager';
$txt['setting_cal_showbdays'] = 'Vis fødselsdager';
$txt['setting_cal_showevents'] = 'Vis hendelser';
$txt['setting_cal_show_never'] = 'Aldri';
$txt['setting_cal_show_cal'] = 'Kun i kalender';
$txt['setting_cal_show_index'] = 'Kun på forumoversikten';
$txt['setting_cal_show_all'] = 'Vis på forumoversikt og i kalender';
$txt['setting_cal_defaultboard'] = 'Standard forum for hendelser';
$txt['setting_cal_allow_unlinked'] = 'Tillat hendelser som ikke knyttes mot et emne';
$txt['setting_cal_minyear'] = 'Minimum år';
$txt['setting_cal_maxyear'] = 'Maksimum år';
$txt['setting_cal_allowspan'] = 'Tillat hendelser som strekker seg over flere dager';
$txt['setting_cal_maxspan'] = 'Maks antall dager en hendelse kan strekke seg over';
$txt['setting_cal_showInTopic'] = 'Vis tilknyttede hendelser i emnevisningen';

// Adding/Editing/Viewing Holidays
$txt['manage_holidays_desc'] = 'Her kan du legge til og slette helligdager fra forumkalenderen.';
$txt['current_holidays'] = 'Gjeldende helligdager';
$txt['holidays_title'] = 'Helligdag';
$txt['holidays_title_label'] = 'Tittel';
$txt['holidays_delete_confirm'] = 'Er du sikker på at du vil slette disse helligdagene?';
$txt['holidays_add'] = 'Legg til helligdag';
$txt['holidays_edit'] = 'Rediger helligdag';
$txt['holidays_button_add'] = 'Legg til';
$txt['holidays_button_edit'] = 'Rediger';
$txt['holidays_button_remove'] = 'Slett';
$txt['holidays_no_entries'] = 'Det er for tiden ingen helligdager konfigurert.';
$txt['every_year'] = 'Hvert år';

?>