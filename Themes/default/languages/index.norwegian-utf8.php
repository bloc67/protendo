<?php
// Version: 2.0; Index

global $forum_copyright, $forum_version, $webmaster_email, $scripturl, $context, $boardurl;

// Locale (strftime, pspell_new) and spelling. (pspell_new, can be left as '' normally.)
// For more information see:
//   - http://www.php.net/function.pspell-new
//   - http://www.php.net/function.setlocale
// Again, SPELLING SHOULD BE '' 99% OF THE TIME!!  Please read this!
$txt['lang_locale'] = 'no_NO.utf8';
$txt['lang_dictionary'] = 'no';
$txt['lang_spelling'] = '';

// Ensure you remember to use uppercase for character set strings.
$txt['lang_character_set'] = 'UTF-8';
// Character set and right to left?
$txt['lang_rtl'] = false;
// Capitalize day and month names?
$txt['lang_capitalize_dates'] = true;

$txt['days'] = array('søndag', 'mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag', 'lørdag');
$txt['days_short'] = array('søn', 'man', 'tir', 'ons', 'tor', 'fre', 'lør');
// Months must start with 1 => 'January'. (or translated, of course.)
$txt['months'] = array(1 => 'januar', 'februar', 'mars', 'april', 'mai', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'desember');
$txt['months_show'] = array(1 => 'Januar', 2=> 'Februar', 3=> 'Mars', 4=>'April', 5=>'Mai', 6=>'Juni', 7=>'Juli', 8=>'August', 9=>'September', 10=>'Oktober', 11=>'November', 12=>'Desember');
$txt['months_titles'] = array(1 => 'januar', 'februar', 'mars', 'april', 'mai', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'Desember');
$txt['months_short'] = array(1 => 'jan', 'feb', 'mar', 'apr', 'mai', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'des');

$txt['time_am'] = 'am';
$txt['time_pm'] = 'pm';

$txt['newmessages0'] = 'er ny';
$txt['newmessages1'] = 'er nye';
$txt['newmessages3'] = 'Ny';
$txt['newmessages4'] = ', ';

$txt['forum'] = 'Forum';
$txt['admin'] = 'Administrator';
$txt['moderate'] = 'Moderér';

$txt['save'] = 'Lagre';

$txt['modify'] = 'Rediger';
$txt['forum_index'] = '%1$s - Hovedside';
$txt['members'] = 'medlemmer';
$txt['board_name'] = 'Navn på forum';
$txt['posts'] = 'Innlegg';

$txt['member_postcount'] = 'Innlegg';
$txt['no_subject'] = '(Uten tittel)';
$txt['view_profile'] = 'Vis profil';
$txt['guest_title'] = 'Gjest';
$txt['author'] = 'Skrevet av';
$txt['on'] = 'på';
$txt['remove'] = 'Slett';
$txt['start_new_topic'] = 'Start nytt emne';

$txt['login'] = 'Logg inn';
// Use numeric entities in the below string.
$txt['username'] = 'Brukernavn';
$txt['password'] = 'Passord';

$txt['username_no_exist'] = 'Brukernavnet eksisterer ikke.';
$txt['no_user_with_email'] = 'Det er ingen brukernavn knyttet til denne e-postadressen.';

$txt['board_moderator'] = 'Moderatorer av forum';
$txt['remove_topic'] = 'Slett emnet';
$txt['topics'] = 'Emner';
$txt['modify_msg'] = 'Rediger innlegg';
$txt['name'] = 'Navn';
$txt['email'] = 'E-post';
$txt['subject'] = 'Tittel';
$txt['message'] = 'Melding';
$txt['redirects'] = 'Omdirigeringer';
$txt['quick_modify'] = 'Endre integrert';

$txt['choose_pass'] = 'Velg passord';
$txt['verify_pass'] = 'Bekreft passord';
$txt['position'] = 'Posisjon';

$txt['profile_of'] = 'Viser profilen til';
$txt['total'] = 'Totalt';
$txt['posts_made'] = 'innlegg';
$txt['website'] = 'Hjemmeside';
$txt['register'] = 'Registrer';
$txt['warning_status'] = 'Status advarsler';
$txt['user_warn_watch'] = 'Brukeren er på moderators overvåkingsliste';
$txt['user_warn_moderate'] = 'Brukerens innlegg er lagt i godkjenningskøen';
$txt['user_warn_mute'] = 'Brukeren er utestengt fra å legge til innlegg';
$txt['warn_watch'] = 'Overvåket';
$txt['warn_moderate'] = 'Modereret';
$txt['warn_mute'] = 'Ignorert';

$txt['message_index'] = 'Oversikt';
$txt['news'] = 'Nyheter';
$txt['home'] = 'Hovedside';
$txt['recent_news'] = 'Siste Nytt';

$txt['lock_unlock'] = 'Steng/åpne emne';
$txt['post'] = 'Legg til innlegg';
$txt['error_occured'] = 'En feil har inntruffet!';
$txt['at'] = 'ved';
$txt['logout'] = 'Logg ut';
$txt['started_by'] = 'Startet av';
$txt['replies'] = 'Svar';
$txt['last_post'] = 'Nyeste innlegg';
$txt['admin_login'] = 'Logg inn som administrator';
// Use numeric entities in the below string.
$txt['topic'] = 'Emne';
$txt['help'] = 'Hjelp';
$txt['notify'] = 'Varsling';
$txt['unnotify'] = 'Stopp varsling';
$txt['notify_request'] = 'Ønsker du å få en varsling på e-post om noen svarer på dette emnet?';
// Use numeric entities in the below string.
$txt['regards_team'] = 'Med vennlig hilsen,' . "\n" . 'administratorene på ' . $context['forum_name'] . '.';
$txt['notify_replies'] = 'Varsling ved svar';
$txt['move_topic'] = 'Flytt emne';
$txt['move_to'] = 'Flytt til';
$txt['pages'] = 'Sider';
$txt['users_active'] = 'Brukere aktive siste %1$d minutter';
$txt['personal_messages'] = 'Personlige meldinger';
$txt['reply_quote'] = 'Svar med sitat';
$txt['reply'] = 'Svar';
$txt['reply_noun'] = 'Svar';
$txt['approve'] = 'Godkjenn';
$txt['approve_all'] = 'godkjenn alle';
$txt['awaiting_approval'] = 'Venter på godkjenning';
$txt['attach_awaiting_approve'] = 'Vedlegg venter på godkjenning';
$txt['post_awaiting_approval'] = 'Merk: Denne meldingen avventer godkjennelse av en moderator.';
$txt['there_are_unapproved_topics'] = 'Det er %1$s emner og %2$s innlegg som venter godkjenning i dette forumet. Klikk <a href="%3$s">her</a> for å vise dem alle.';

$txt['msg_alert_none'] = 'Ingen meldinger...';
$txt['msg_alert_you_have'] = 'du har';
$txt['msg_alert_messages'] = 'meldinger';
$txt['remove_message'] = 'Slett melding';

$txt['online_users'] = 'Brukere pålogget';
$txt['personal_message'] = 'Personlig melding';
$txt['jump_to'] = 'Gå til';
$txt['go'] = 'OK';
$txt['are_sure_remove_topic'] = 'Er du sikker på at du vil slette dette emnet?';
$txt['yes'] = 'Ja';
$txt['no'] = 'Nei';

$txt['search_end_results'] = 'Ingen flere treff';
$txt['search_on'] = 'på';

$txt['search'] = 'Søk';
$txt['all'] = 'Alle';

$txt['back'] = 'Tilbake';
$txt['password_reminder'] = 'passord-påminnelse';
$txt['topic_started'] = 'Emne startet av';
$txt['title'] = 'Tittel';
$txt['post_by'] = 'Skrevet av';
$txt['memberlist_searchable'] = 'Søkbar liste over alle registrerte medlemmer.';
$txt['welcome_member'] = 'Velkommen, ';
$txt['admin_center'] = 'Administratorpanel';
$txt['last_edit'] = 'Siste redigering';
$txt['notify_deactivate'] = 'Ønsker du å deaktivere varsling for dette emnet?';

$txt['recent_posts'] = 'Nyeste innlegg';

$txt['location'] = 'Bosted';
$txt['gender'] = 'Kjønn';
$txt['date_registered'] = 'Registreringsdato';

$txt['recent_view'] = 'Vis nyeste innleggene fra forumet.';
$txt['recent_updated'] = 'er det sist oppdaterte emnet';

$txt['male'] = 'Mann';
$txt['female'] = 'Kvinne';

$txt['error_invalid_characters_username'] = 'Ugyldig tegn benyttet i brukernavn.';

$txt['welcome_guest'] = 'Velkommen, <strong>%1$s</strong>. Vennligst <a href="' . $scripturl . '?action=login">logg inn</a> eller <a href="' . $scripturl . '?action=register">registrer deg</a>.';
$txt['login_or_register'] = 'Vennligst <a href="' . $scripturl . '?action=login">logg inn</a> eller <a href="' . $scripturl . '?action=register">registrer</a>.';
$txt['welcome_guest_activate'] = '<br />Har du ikke mottatt <a href="' . $scripturl . '?action=activate">e-post for aktivering</a>?';
$txt['hello_member'] = 'Hei,';
// Use numeric entities in the below string.
$txt['hello_guest'] = 'Velkommen,';
$txt['welmsg_hey'] = 'Hei,';
$txt['welmsg_welcome'] = 'Velkommen,';
$txt['welmsg_please'] = 'Vennligst';
$txt['select_destination'] = 'Vennligst velg en destinasjon';

// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['posted_by'] = 'Skrevet av';

$txt['icon_smiley'] = 'Glad';
$txt['icon_angry'] = 'Sint';
$txt['icon_cheesy'] = 'Osteaktig';
$txt['icon_laugh'] = 'Ler';
$txt['icon_sad'] = 'Trist';
$txt['icon_wink'] = 'Blunker';
$txt['icon_grin'] = 'Gliser';
$txt['icon_shocked'] = 'Sjokkert';
$txt['icon_cool'] = 'Kult';
$txt['icon_huh'] = 'Hæ';
$txt['icon_rolleyes'] = 'Øyerulling';
$txt['icon_tongue'] = 'Rekker tunge';
$txt['icon_embarrassed'] = 'Flau';
$txt['icon_lips'] = 'Hemmelig';
$txt['icon_undecided'] = 'Tvilende';
$txt['icon_kiss'] = 'Kysser';
$txt['icon_cry'] = 'Gråter';
$txt['icon_issue'] = 'Sak';
$txt['icon_assigned'] = 'Tilegnet';
$txt['icon_solved'] = 'Løst';
$txt['icon_feedback'] = 'Tilbakemelding';

$txt['moderator'] = 'Moderator';
$txt['moderators'] = 'Moderatorer';

$txt['mark_board_read'] = 'Merk alle emner i dette forumet som lest';
$txt['views'] = 'Visninger';
$txt['new'] = 'Ny(e)';

$txt['view_all_members'] = 'Vis alle medlemmer';
$txt['view'] = 'Vis';

$txt['viewing_members'] = 'Viser medlemmer %1$s til %2$s';
$txt['of_total_members'] = 'av %1$s medlemmer';

$txt['forgot_your_password'] = 'Glemt passordet?';

$txt['date'] = 'Dato';
// Use numeric entities in the below string.
$txt['from'] = 'Fra';
$txt['check_new_messages'] = 'Se etter nye meldinger';
$txt['to'] = 'Til';

$txt['board_topics'] = 'Emner';
$txt['members_title'] = 'Medlemmer';
$txt['members_list'] = 'Medlemsliste';
$txt['new_posts'] = 'Nye innlegg';
$txt['old_posts'] = 'Ingen nye innlegg';
$txt['redirect_board'] = 'Omadresser emne';

$txt['sendtopic_send'] = 'Send';
$txt['report_sent'] = 'Din rapport er sendt.';

$txt['time_offset'] = 'Tidsavvik';
$txt['or'] = 'eller';

$txt['no_matches'] = 'Beklager, ingen treff ble funnet';

$txt['notification'] = 'Varsling';

$txt['your_ban'] = 'Beklager %1$s, du er utestengt fra dette forumet!';
$txt['your_ban_expires'] = 'Denne utestengingen er satt til å utløpe %1$s.';
$txt['your_ban_expires_never'] = 'Denne utestengningen er satt til å vare for alltid';
$txt['ban_continue_browse'] = 'Du kan fortsette å surfe på forumet som en gjest.';

$txt['mark_as_read'] = 'Marker alle innlegg som lest';

$txt['hot_topics'] = 'Aktivt emne (minst %1$d svar)';
$txt['very_hot_topics'] = 'Veldig aktivt emne (minst %1$d svar)';
$txt['locked_topic'] = 'Steng emne';
$txt['normal_topic'] = 'Vanlig emne';
$txt['participation_caption'] = 'Emne du har postet til';

$txt['go_caps'] = 'OK';

$txt['print'] = 'Skriv ut';
$txt['profile'] = 'Profil';
$txt['topic_summary'] = 'Emneoversikt';
$txt['not_applicable'] = '--';
$txt['message_lowercase'] = 'melding';
$txt['name_in_use'] = 'Dette navnet er allerede tatt i bruk av et annet medlem.';

$txt['total_members'] = 'Medlemmer totalt';
$txt['total_posts'] = 'Innlegg totalt';
$txt['total_topics'] = 'Emner totalt';

$txt['mins_logged_in'] = 'Innloggingstid i minutter';

$txt['preview'] = 'Forhåndsvisning';
$txt['always_logged_in'] = 'Forbli alltid innlogget';

$txt['logged'] = 'Loggført';
// Use numeric entities in the below string.
$txt['ip'] = 'IP';

$txt['www'] = 'WWW';

$txt['by'] = 'av';

$txt['hours'] = 'timer';
$txt['days_word'] = 'dager';

$txt['newest_member'] = ', vårt nyeste medlem.';

$txt['search_for'] = 'Søk etter';

$txt['aim'] = 'AIM';
// In this string, please use +'s for spaces.
$txt['aim_default_message'] = 'Hei.+Er+du+der?';
$txt['aim_title'] = 'AOL Instant Messenger';
$txt['icq'] = 'ICQ';
$txt['icq_title'] = 'ICQ Messenger';
$txt['msn'] = 'MSN';
$txt['msn_title'] = 'MSN Messenger';
$txt['yim'] = 'YIM';
$txt['yim_title'] = 'Yahoo Instant Messenger';

$txt['maintain_mode_on'] = 'Husk at forumet er under vedlikehold.';
$txt['maintenance'] = 'Maintenance mode';
$txt['approvals'] = 'Approvals: ';
$txt['reports'] = 'Reports: ';
$txt['site'] = 'Site';

$txt['read'] = 'Lest';
$txt['times'] = 'ganger';

$txt['forum_stats'] = 'Forum-statistikk';
$txt['latest_member'] = 'Nyeste medlem';
$txt['total_cats'] = 'Kategorier totalt';
$txt['latest_post'] = 'Nyeste innlegg';

$txt['you_have'] = 'Du har';
$txt['click'] = 'Klikk';
$txt['here'] = 'her';
$txt['to_view'] = 'for å vise dem.';

$txt['total_boards'] = 'Forum totalt';

$txt['print_page'] = 'Skriv ut side';

$txt['valid_email'] = 'Dette må være en gyldig e-postadresse.';

$txt['geek'] = 'Jeg er en nerd!!';
$txt['info_center_title'] = 'Info-senter';

$txt['send_topic'] = 'Tips en venn';

$txt['sendtopic_title'] = 'Tips en venn om emnet &quot;%1$s&quot;.';
$txt['sendtopic_sender_name'] = 'Ditt navn';
$txt['sendtopic_sender_email'] = 'Din e-postadresse';
$txt['sendtopic_receiver_name'] = 'Mottakers navn';
$txt['sendtopic_receiver_email'] = 'Mottakers e-postadresse';
$txt['sendtopic_comment'] = 'Legg til kommentar';

$txt['allow_user_email'] = 'Tillat medlemmer å sende e post til meg';

$txt['check_all'] = 'Marker alle';

// Use numeric entities in the below string.
$txt['database_error'] = 'Feil i databasen';
$txt['try_again'] = 'Prøv på nytt. Om du kommer tilbake til denne feilmeldingen, rapporter feilen til en administrator.';
$txt['file'] = 'Fil';
$txt['line'] = 'Linje';
// Use numeric entities in the below string.
$txt['tried_to_repair'] = 'Protendo har oppdaget og automatisk rettet en feil i din database. Om problemene fortsetter, eller du fremdeles får disse e-postmeldingene, ta kontakt med din nettleverandør.';
$txt['database_error_versions'] = '<strong>Merk:</strong> Det ser ut som din database trenger en oppgradering. Dine forumfiler er av versjon %1$s, mens databasen din inneholder versjon %2$s. For å forsøke å korrigere feilen nevnt ovenfor er det anbefalt at du kjører nyeste utgave av upgrade.php.';
$txt['template_parse_error'] = 'PHP-feil i designfilene!';
$txt['template_parse_error_message'] = 'Det kan se ut som noe har gått virkelig galt med designet du vil vise på forumet. Problemet er gjerne bare midlertidig, så du må gjerne komme tilbake og prøve på nytt. Dersom problemet fortsetter, ta kontakt med administrator.<br /><br />Du kan også prøve å <a href="javascript:location.reload();">oppdatere denne siden</a>.';
$txt['template_parse_error_details'] = 'Det var et problem under lasting av <tt><strong>%1$s</strong></tt> design- eller språkfil.  Kontrollér syntaksen og prøv på nytt - husk at enslige anførselstegn (<tt>\'</tt>) må ofte ha en ekstra skråstrek foran (<tt>\\</tt>).  For å se mer spesifikk informasjon om feil fra PHP, prøv å <a href="' . $boardurl . '%1$s">åpne filen direkte</a>.<br /><br />Du må gjerne prøve å <a href="javascript:location.reload();">oppdatere denne siden</a> or <a href="' . $scripturl . '?theme=1">bruker standard tema</a>.';

$txt['today'] = '<strong>I dag</strong> kl. ';
$txt['yesterday'] = '<strong>I går</strong> kl. ';
$txt['new_poll'] = 'Ny avstemning';
$txt['poll_question'] = 'Spørsmål';
$txt['poll_vote'] = 'Lagre avstemning';
$txt['poll_total_voters'] = 'Stemmer totalt';
$txt['shortcuts'] = 'snarveier: trykk alt+s for å lagre eller alt+p for å forhåndsvise';
$txt['shortcuts_firefox'] = 'snarveier: trykk shift+alt+s for å lagre eller shift+alt+p for å forhåndsvise';
$txt['poll_results'] = 'Vis resultater';
$txt['poll_lock'] = 'Steng avstemning';
$txt['poll_unlock'] = 'Åpne avstemning';
$txt['poll_edit'] = 'Rediger avstemning';
$txt['poll'] = 'avstemning';
$txt['one_day'] = '1 dag';
$txt['one_week'] = '1 uke';
$txt['one_month'] = '1 måned';
$txt['forever'] = 'Alltid';
$txt['quick_login_dec'] = 'Logg inn med brukernavn, passord og innloggingstid';
$txt['one_hour'] = '1 time';
$txt['moved'] = 'FLYTTET';
$txt['moved_why'] = 'Skriv inn en kort beskrivelse for<br />hvorfor emnet flyttes.';
$txt['board'] = 'Seksjon';
$txt['in'] = 'i';
$txt['sticky_topic'] = 'Prioritert emne';

$txt['delete'] = 'Slett';

$txt['delete2'] = 'slette';
$txt['your_pms'] = 'Dine personlige meldinger';

$txt['kilobyte'] = 'kB';

$txt['more_stats'] = '[Mer statisikk]';

// Use numeric entities in the below three strings.
$txt['code'] = 'Kode';
$txt['code_select'] = '[Velg]';
$txt['quote_from'] = 'Sitat fra';
$txt['quote'] = 'Sitat';

$txt['merge_to_topic_id'] = 'ID til målemne';
$txt['split'] = 'Splitt emne';
$txt['merge'] = 'Slå sammen emner';
$txt['subject_new_topic'] = 'Tittel på nytt emne';
$txt['split_this_post'] = 'Splitt kun dette innlegget.';
$txt['split_after_and_this_post'] = 'Splitt emne fra og med dette innlegget.';
$txt['select_split_posts'] = 'Velg innlegg som skal splittes.';
$txt['new_topic'] = 'Nytt emne';
$txt['split_successful'] = 'Emnet ble splittet i to emner.';
$txt['origin_topic'] = 'Opprinnelig emne';
$txt['please_select_split'] = 'Velg hvilke innlegg du vil splitte.';
$txt['merge_successful'] = 'Emnene ble slått sammen.';
$txt['new_merged_topic'] = 'Nytt sammenslått emne';
$txt['topic_to_merge'] = 'Emnet som skal sammenslås';
$txt['target_board'] = 'Plasseres i forum';
$txt['target_topic'] = 'Plasseres i emne';
$txt['merge_confirm'] = 'Er du sikker på at du vil slå sammen';
$txt['with'] = 'med';
$txt['merge_desc'] = 'Denne funksjonen vil slå sammen innleggene i to emner til ett emne. Innleggene vil da bli sortert etter dato og derfor kan rekkefølgen på innleggene være forandret, slik at det eldste innlegget vil komme først i det nye emnet.';

$txt['set_sticky'] = 'Prioritér emnet';
$txt['set_nonsticky'] = 'Ikke prioritér emnet';
$txt['set_lock'] = 'Steng emnet';
$txt['set_unlock'] = 'Åpne emnet';

$txt['search_advanced'] = 'Avansert søk';

$txt['security_risk'] = 'ALVORLIG SIKKERHETSRISIKO:';
$txt['not_removed'] = 'Du har ikke slettet ';
$txt['not_removed_extra'] = '%1$s er en sikkerhetskopi av %2$s som ikke ble generert av Protendo. Det kan nås direkte og brukes til å få uautorisert tilgang til forumet ditt. Du bør slette den umiddelbart.';

$txt['cache_writable_head'] = 'Ytelsesadvarsel';
$txt['cache_writable'] = 'Mappe formellomlagring er ikke skrivbar - dette vil påvirke ytelsen til forumet ditt.';

$txt['page_created'] = 'Siden ble generert på ';
$txt['seconds_with'] = ' sekunder med ';
$txt['queries'] = ' spørringer.';

$txt['report_to_mod_func'] = 'Bruk denne funksjonen til å informere moderatorene og administratorene om et støtende eller feilplassert innlegg.<br /><em>Vær oppmersom på at din e-postadresse blir synlig for moderatorene ved bruk av denne funksjonen.</em>';

$txt['online'] = 'Innlogget';
$txt['offline'] = 'Utlogget';
$txt['pm_online'] = 'Personlig melding (innlogget)';
$txt['pm_offline'] = 'Personlig melding (utlogget)';
$txt['status'] = 'Status';

$txt['go_up'] = 'Til toppen';
$txt['go_down'] = 'Til bunnen';

$forum_copyright = '<a href="http://www.bjornhkristiansen.com">%1$s</a> &copy;2013-2014';

$txt['birthdays'] = 'Fødselsdager:';
$txt['events'] = 'Hendelser:';
$txt['birthdays_upcoming'] = 'Kommende fødselsdager:';
$txt['events_upcoming'] = 'Kommende hendelser:';
// Prompt for holidays in the calendar, leave blank to just display the holiday's name.
$txt['calendar_prompt'] = '';
$txt['calendar_month'] = 'Måned:';
$txt['calendar_year'] = 'År:';
$txt['calendar_day'] = 'Dag:';
$txt['calendar_event_title'] = 'Tittel på hendelsen:';
$txt['calendar_event_options'] = 'Hendelses alternativer';
$txt['calendar_post_in'] = 'Lag emne i:';
$txt['calendar_edit'] = 'Rediger hendelse';
$txt['event_delete_confirm'] = 'Slett denne hendelsen?';
$txt['event_delete'] = 'Slett hendelse';
$txt['calendar_post_event'] = 'Legg til hendelse';
$txt['calendar'] = 'Kalender';
$txt['calendar_link'] = 'Tilknytt kalender';
$txt['calendar_upcoming'] = 'Kommende hendelser';
$txt['calendar_today'] = 'Dagens kalender';
$txt['calendar_week'] = 'Uke';
$txt['calendar_week_title'] = 'Uke %1$d av %2$d';
$txt['calendar_numb_days'] = 'Antall dager:';
$txt['calendar_how_edit'] = 'hvordan redigerer du disse hendelsene?';
$txt['calendar_link_event'] = 'Tilknytt hendelse';
$txt['calendar_confirm_delete'] = 'Er du sikker på at du vil slette denne hendelsen?';
$txt['calendar_linked_events'] = 'Tilknyttede hendelser';
$txt['calendar_click_all'] = 'klikk for å se alle %1$s';

$txt['moveTopic1'] = 'Legg til videresendings-emne';
$txt['moveTopic2'] = 'Endre tittel på emnet';
$txt['moveTopic3'] = 'Ny tittel';
$txt['moveTopic4'] = 'Rediger tittel på alle innlegg';
$txt['move_topic_unapproved_js'] = 'Advarsel! Dette emnet er ennå ikke godkjent.\\n\\nDet anbefales ikke at du oppretter en omdirigerings emne med mindre du har tenkt å godkjenne innlegget umiddelbart etter flyttingen.';

$txt['theme_template_error'] = 'Kunne ikke laste utseende til \'%1$s\'.';
$txt['theme_language_error'] = 'Kunne ikke laste språkfilen til \'%1$s\'.';

$txt['parent_boards'] = 'Delforum';

$txt['smtp_no_connect'] = 'Kunne ikke koble til SMTP verten';
$txt['smtp_port_ssl'] = 'SMTP-porten er stilt inn feil; den bør være 465 for SSL-servere.';
$txt['smtp_bad_response'] = 'Kunne ikke motta respons fra mailserver';
$txt['smtp_error'] = 'Det oppsto et problem ved sending av e-post. Feilmelding: ';
$txt['mail_send_unable'] = 'Kunne ikke sende e-post til adressen \'%1$s\'';

$txt['mlist_search'] = 'Søk etter medlemmer';
$txt['mlist_search_again'] = 'Søk på nytt';
$txt['mlist_search_email'] = 'Søk på e-postadresse';
$txt['mlist_search_messenger'] = 'Søk på Messenger kallenavn';
$txt['mlist_search_group'] = 'Søk på posisjon';
$txt['mlist_search_name'] = 'Søk på navn';
$txt['mlist_search_website'] = 'Søk på hjemmeside';
$txt['mlist_search_results'] = 'Søkeresultater';
$txt['mlist_search_by'] = 'Søk på %1$s';
$txt['mlist_menu_view'] = 'Se medlemsliste';

$txt['attach_downloaded'] = 'lastet ned';
$txt['attach_viewed'] = 'vist';
$txt['attach_times'] = 'ganger';

$txt['settings'] = 'Innstillinger';
$txt['never'] = 'Aldri';
$txt['more'] = 'flere';

$txt['hostname'] = 'Vertsnavn';
$txt['you_are_post_banned'] = 'Beklager %1$s, du er utestengt fra å skrive innlegg og personlige meldinger på dette forumet.';
$txt['ban_reason'] = 'Årsak';

$txt['tables_optimized'] = 'Tabellene i databasen ble optimalisert';

$txt['add_poll'] = 'Legg til avstemning';
$txt['poll_options6'] = 'Du kan bare velge opptil %1$s alternativer.';
$txt['poll_remove'] = 'Fjern avstemning';
$txt['poll_remove_warn'] = 'Er du sikker på at du vil fjerne avstemningen fra dette emnet?';
$txt['poll_results_expire'] = 'Resultatene vil vises når avstemningen er avsluttet';
$txt['poll_expires_on'] = 'Avstemning avsluttes';
$txt['poll_expired_on'] = 'Avstemning er avsluttet';
$txt['poll_change_vote'] = 'Fjern stemme';
$txt['poll_return_vote'] = 'Innstillinger for stemmegivning';
$txt['poll_cannot_see'] = 'Du kan ikke se resultatene av denne avstemningen for øyeblikket.';

$txt['quick_mod_approve'] = 'Godkjenn valgte';
$txt['quick_mod_remove'] = 'Slett valgt(e)';
$txt['quick_mod_lock'] = 'Steng/åpne valgte';
$txt['quick_mod_sticky'] = 'Prioritert/Ikke prioritert valgt';
$txt['quick_mod_move'] = 'Flytt valgt(e) til';
$txt['quick_mod_merge'] = 'Slå sammen valgt(e)';
$txt['quick_mod_markread'] = 'Marker valgt(e) som lest';
$txt['quick_mod_go'] = 'Ok!';
$txt['quickmod_confirm'] = 'Er du sikker på at du vil gjøre dette?';

$txt['spell_check'] = 'Stavekontroll';

$txt['quick_reply'] = 'Hurtigsvar';
$txt['quick_reply_desc'] = 'Med <em>hurtigsvar</em> kan du kan skrive et innlegg når du ser på et emne uten å laste en ny side. Du kan fortsatt bruke BB koder og smilefjes som du ville gjort i et vanlig svar.';
$txt['quick_reply_warning'] = 'Advarsel: Dette emnet er stengt! Kun administratorer og moderatorer kan legge inn svar.';
$txt['quick_reply_verification'] = 'Etter innsending av innlegget ditt vil du bli henvist til den vanlige siden for å verifisere ditt innlegg %1$s.';
$txt['quick_reply_verification_guests'] = '(kreves for alle gjester)';
$txt['quick_reply_verification_posts'] = '(kreves for alle brukere med mindre enn %1$d innlegg)';
$txt['wait_for_approval'] = 'Merk: dette innlegget vil ikke vises før det er blitt godkjent av en moderator.';

$txt['notification_enable_board'] = 'Er du sikker på at du vil aktivere varsling om nye emner i dette forumet?';
$txt['notification_disable_board'] = 'Er du sikker på at du vil deaktivere varsling om nye emner i dette forumet?';
$txt['notification_enable_topic'] = 'Er du sikker på at du vil aktivere varsling om nye svar i dette emnet?';
$txt['notification_disable_topic'] = 'Er du sikker på at du vil deaktivere varsling om nye svar i dette emnet?';

$txt['report_to_mod'] = 'Rapporter til moderator';
$txt['issue_warning_post'] = 'Utstede en advarsel på grunn av denne meldingen';

$txt['unread_topics_visit'] = 'Nye uleste emner';
$txt['unread_topics_visit_none'] = 'Ingen uleste emner siden ditt siste besøk.  <a href="' . $scripturl . '?action=unread;all">Klikk her for å søke etter alle uleste emner</a>.';
$txt['unread_topics_all'] = 'Alle uleste emner';
$txt['unread_replies'] = 'Oppdaterte emner';

$txt['who_title'] = 'Hvem er pålogget';
$txt['who_and'] = ' og ';
$txt['who_viewing_topic'] = ' leser dette emnet.';
$txt['who_viewing_board'] = ' er inne på dette forumet.';
$txt['who_member'] = 'Medlem';

// No longer used by default theme, but for backwards compat
$txt['powered_by_php'] = 'Bygget på PHP';
$txt['powered_by_mysql'] = 'Bygget på MySQL';
$txt['valid_css'] = 'CSS godkjent!';

// Current footer strings
$txt['valid_html'] = 'HTML 4.01 godkjent!';
$txt['valid_xhtml'] = 'XHTML 1.0 godkjent!';
$txt['wap2'] = 'WAP2';
$txt['rss'] = 'RSS';
$txt['xhtml'] = 'XHTML';
$txt['html'] = 'HTML';

$txt['guest'] = 'gjest';
$txt['guests'] = 'gjester';
$txt['user'] = 'medlem';
$txt['users'] = 'medlemmer';
$txt['hidden'] = 'skjult(e)';
$txt['buddy'] = 'venn';
$txt['buddies'] = 'venner';
$txt['most_online_ever'] = 'Flest pålogget noen sinne';
$txt['most_online_today'] = 'Flest pålogget idag';

$txt['merge_select_target_board'] = 'Velg hvilket forum det sammenslåtte emnet skal plasseres i';
$txt['merge_select_poll'] = 'Velg hvilken avstemning det sammenslåtte emnet skal ha';
$txt['merge_topic_list'] = 'Velg emner som skal slås sammen';
$txt['merge_select_subject'] = 'Velg tittel på sammenslått emne';
$txt['merge_custom_subject'] = 'Annen tittel';
$txt['merge_enforce_subject'] = 'Endre tittel på alle innlegg';
$txt['merge_include_notifications'] = 'Inkluder varslinger?';
$txt['merge_check'] = 'Slå sammen?';
$txt['merge_no_poll'] = 'Ingen avstemning';

$txt['response_prefix'] = 'Sv: ';
$txt['current_icon'] = 'Gjeldende symbol';
$txt['message_icon'] = 'Ikon for innlegg';

$txt['smileys_current'] = 'Gjeldende smilefjes-sett';
$txt['smileys_none'] = 'Ingen smilefjes';
$txt['smileys_forum_board_default'] = 'Forvalg for forum';

$txt['search_results'] = 'Søkeresultater';
$txt['search_no_results'] = 'Beklager, ingen treff ble funnet';

$txt['totalTimeLogged1'] = 'Total tid innlogget: ';
$txt['totalTimeLogged2'] = ' dager, ';
$txt['totalTimeLogged3'] = ' timer og ';
$txt['totalTimeLogged4'] = ' minutter.';
$txt['totalTimeLogged5'] = 'd';
$txt['totalTimeLogged6'] = 't ';
$txt['totalTimeLogged7'] = 'm';

$txt['approve_thereis'] = 'Det er';
$txt['approve_thereare'] = 'Det er';
$txt['approve_member'] = 'ett medlem';
$txt['approve_members'] = 'medlemmer';
$txt['approve_members_waiting'] = 'som venter på godkjenning.';

$txt['notifyboard_turnon'] = 'Ønsker du å få varsling på e-post når noen lager et nytt emne i dette forumet?';
$txt['notifyboard_turnoff'] = 'Er du sikker på at du ikke vil motta varsling på e-post om nye emner i dette forumet?';

$txt['activate_code'] = 'Din aktiveringskode er';

$txt['find_members'] = 'Søk etter medlemmer';
$txt['find_username'] = 'Navn, brukernavn, eller e-postadresse';
$txt['find_buddies'] = 'Vis kun venner?';
$txt['find_wildcards'] = 'Tillatte jokertegn: *, ?';
$txt['find_no_results'] = 'Ingen treff funnet';
$txt['find_results'] = 'Resultater';
$txt['find_close'] = 'Lukk';

$txt['unread_since_visit'] = 'Vis uleste innlegg siden forrige besøk.';
$txt['show_unread_replies'] = 'Vis nye svar på dine innlegg.';

$txt['change_color'] = 'Endre farge';

$txt['quickmod_delete_selected'] = 'Slett valgte';

// In this string, don't use entities. (&amp;, etc.)
$txt['show_personal_messages'] = 'Du har mottatt en eller flere nye personlige meldinger.\\nVil du vise dem nå (i nytt vindu)?';

$txt['previous_next_back'] = '&laquo; forrige';
$txt['previous_next_forward'] = 'neste &raquo;';

$txt['movetopic_auto_board'] = '[FORUM]';
$txt['movetopic_auto_topic'] = '[EMNE-LENKE]';
$txt['movetopic_default'] = 'Dette emnet har blitt flyttet til ' . $txt['movetopic_auto_board'] . ".\n\n" . $txt['movetopic_auto_topic'];

$txt['upshrink_description'] = 'Krymp eller utvid topp-feltet.';

$txt['mark_unread'] = 'Marker som ulest';

$txt['ssi_not_direct'] = 'Vennligst ikke lag lenker direkte mot SSI.php; du kan istedet benytte stien (%1$s) eller legge til ?ssi_function=etellerannet.';
$txt['ssi_session_broken'] = 'SSI.php klarte ikke å laste inn en sessjon! Dette kan skape problem med utlogging og andre funksjoner - vennligst forsikre deg om at SSI.php inkluderes før *alt* annet i dine scripts!';

// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['preview_title'] = 'Forhåndsvis innlegg';
$txt['preview_fetch'] = 'Henter forhåndsvisning...';
$txt['preview_new'] = 'Ny melding';
$txt['error_while_submitting'] = 'Følgende feil oppsto ved lagring av meldingen:';
$txt['error_old_topic'] = 'Advarsel: Det er ikke blitt lagt til noen emner her på minst %1$d dager.<br />Med mindre du er helt sikker på at du vil svare, kan du vurdere å starte et nytt emne.';

$txt['split_selected_posts'] = 'Valgte innlegg';
$txt['split_selected_posts_desc'] = 'Innleggene nedenfor vil danne et nytt emne etter splitting.';
$txt['split_reset_selection'] = 'tilbakestill valg';

$txt['modify_cancel'] = 'Avbryt';
$txt['mark_read_short'] = 'Merk som lest';

$txt['pm_short'] = 'Mine meldinger';
$txt['pm_menu_read'] = 'Les meldinger';
$txt['pm_menu_send'] = 'Send en melding';

$txt['hello_member_ndt'] = 'Hallo';

$txt['unapproved_posts'] = 'Ikke godkjente innlegg (Emner: %1$d, Innlegg: %2$d)';

$txt['ajax_in_progress'] = 'Laster...';

$txt['mod_reports_waiting'] = 'Det er for tiden %1$d moderator rapporter åpne.';

$txt['view_unread_category'] = 'Uleste innlegg';
$txt['verification'] = 'Bekreftelse';
$txt['visual_verification_description'] = 'Skriv tegn som vises i bildet';
$txt['visual_verification_sound'] = 'Lytt til tegn';
$txt['visual_verification_request_new'] = 'Be om et nytt bilde';

// Sub menu labels
$txt['summary'] = 'Oppsummering';
$txt['account'] = 'Innstillinger for medlemskonto';
$txt['forumprofile'] = 'Forum profil';

$txt['modSettings_title'] = 'Funksjoner og innstillinger';
$txt['package'] = 'Modifikasjonsbehandling';
$txt['errlog'] = 'Loggførte feilmeldinger';
$txt['edit_permissions'] = 'Tillatelser';
$txt['mc_unapproved_attachments'] = 'Ikke godkjente vedlegg';
$txt['mc_unapproved_poststopics'] = 'Ikke godkjente innlegg og emner';
$txt['mc_reported_posts'] = 'Rapportert innlegg';
$txt['modlog_view'] = 'Moderationslogg';
$txt['calendar_menu'] = 'Vis kalender';

//!!! Send email strings - should move?
$txt['send_email'] = 'Send e-post';
$txt['send_email_disclosed'] = 'Merk: Dette vil være synlig for mottakeren.';
$txt['send_email_subject'] = 'E-post emne';

$txt['ignoring_user'] = 'Du ignorerer denne brukeren.';
$txt['show_ignore_user_post'] = 'Vis meg innlegget.';

$txt['spider'] = 'Robot';
$txt['spiders'] = 'Roboter';
$txt['openid'] = 'OpenID';

$txt['downloads'] = 'Nedlastinger';
$txt['filesize'] = 'Filstørrelse';
$txt['subscribe_webslice'] = 'Abonner på Webslice';

// Restore topic
$txt['restore_topic'] = 'Gjenopprett emne';
$txt['restore_message'] = 'Gjenopprett';
$txt['quick_mod_restore'] = 'Gjenopprett utvalgte';

// Editor prompt.
$txt['prompt_text_email'] = 'Vennligst oppgi e-postadressen.';
$txt['prompt_text_ftp'] = 'Vennligst skriv inn ftp adressen.';
$txt['prompt_text_url'] = 'Vennligst skriv inn nettadressen du ønsker å lenke til.';
$txt['prompt_text_img'] = 'Skriv inn bilde plassering';

// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['autosuggest_delete_item'] = 'Slett element';

// Debug related - when $db_show_debug is true.
$txt['debug_templates'] = 'Maler: ';
$txt['debug_subtemplates'] = 'Undermaler: ';
$txt['debug_language_files'] = 'Språkfiler:';
$txt['debug_stylesheets'] = 'Stilark: ';
$txt['debug_files_included'] = 'Filer inkludert: ';
$txt['debug_kb'] = 'KB.';
$txt['debug_show'] = 'vis';
$txt['debug_cache_hits'] = 'Hurtigbuffer treff: ';
$txt['debug_cache_seconds_bytes'] = '%1$ss - %2$s bytes';
$txt['debug_cache_seconds_bytes_total'] = '%1$ss for %2$s bytes';
$txt['debug_queries_used'] = 'Spørringer brukt: %1$d.';
$txt['debug_queries_used_and_warnings'] = 'Spørringer brukt: %1$d, %2$d advarsler.';
$txt['debug_query_in_line'] = 'i <em>%1$s</em> linje <em>%2$s</em>, ';
$txt['debug_query_which_took'] = 'som tok %1$s sekunder.';
$txt['debug_query_which_took_at'] = 'som tok %1$s sekunder ved %2$s inn i forespørselen.';
$txt['debug_show_queries'] = '[Vis spørringer]';
$txt['debug_hide_queries'] = '[Skjul spørringer]';

$txt['replies'] = 'Svar';
$txt['unread'] = 'Uleste';

$txt['options'] = 'Flere valg';
$txt['tags'] = 'Tagger';
$txt['notags'] = 'Ingen tagger ble funnet.';
$txt['categories'] = 'Kategorier';
$txt['boards'] = 'Forum';
$txt['nocategories'] = 'Ingen blogg kategorier er definert ennå.';
$txt['byauthors'] = 'Forfattere';
$txt['content'] = 'Innhold';
$txt['comments'] = 'kommentarer';
$txt['wrote'] = 'skrev';
$txt['replies'] = 'Svar';
$txt['unread'] = 'Uleste';
$txt['last_item'] = 'siste innlegg';
$txt['in_slideshow'] = 'Show as slideshow!';
$txt['out_slideshow'] = 'Show in normal mode';

$txt['like'] = 'liker dette innlegget';
$txt['dislike'] = 'misliker dette innlegget';
$txt['likes_dislikes'] = 'Liker | Misliker';
$txt['likes'] = 'Antall likt';
$txt['dislikes'] = 'Antall mislikt';
$txt['skype'] = 'Skype';
$txt['facebook'] = 'Facebook';
$txt['google'] = 'Google';
$txt['twitter'] = 'Twitter';
$txt['why_register'] = 'Nevn en grunn for å registere seg her på dette webstedet.';
$txt['plugins_titles'] = 'Plugin Settings';
$txt['boardtypes'] = 'Boardtypes';
$txt['runtime_plugins'] = 'Runtime Plugins';
$txt['section_plugins_available'] = 'Available Section plugins';
$txt['runtime_plugins_available'] = 'Available Runtime plugins';
$txt['section_plugins'] = 'Section Plugins';
$txt['install'] = 'Install';
$txt['status0'] = 'Not active';
$txt['status1'] = 'Open';
$txt['status2'] = 'Assigned';
$txt['status3'] = 'Closed';
$txt['status4'] = 'Archived';

$txt['new_news'] = $txt['new_article'] = $txt['new_gallery'] = $txt['new_zine'] = $txt['new_text'] = $txt['new_blog'] = 'Ny';


$txt['progress'] = 'Progress';
$txt['myvote'] = 'Min stemme';
$txt['show_single_category'] = 'Vis dette';
$txt['show_all_categories'] = 'Vis alle';

$txt['articles'] = 'Artikler';
$txt['frontpage'] = 'Forside';
$txt['attachments'] = 'Vedlegg';
$txt['pullquote'] = 'Pullquote';
$txt['off'] = '- ikke brukt -';
$txt['spoiler'] = 'Spoiler';
$txt['spoiler_alt'] = 'Klikk for å vise spoiler';
$txt['like'] = 'Like';
$txt['dislike'] = 'unLike';
$txt['likethispost'] = ' people likes this post.';
$txt['no_one'] = 'None';
$txt['and_you'] = 'and you';
$txt['you'] = 'You';
$txt['unread_forum'] = 'Unread forum posts';
$txt['unread_nonforum'] = 'Unread blogs and comments';

$txt['single_cat_title'] = 'See only the category';
$txt['all_cat_title'] = 'See all categories (default)';
$txt['unread_title'] = 'See all unread posts in category';
$txt['markread_title'] = 'Mark as read all posts in';
$txt['first_post_preview'] = 'First post preview';

$txt['quickmod_header'] = 'Moderasjon';
$txt['option_cat_tabs'] = 'Vise Normalt';
$txt['option_cat_tabs_not'] = 'Vise Fliker';
$txt['options'] = 'Valg for denne delen';
$txt['show_rss'] = 'Vis RSS ikoner';
$txt['hide_rss'] = 'Skjul RSS ikoner';

$txt['show_tooltips'] = 'Skjule Forhåndsvisning';
$txt['hide_tooltips'] = 'Vise Forhåndsvisning';
$txt['rss_feed'] = 'Abonner på RSS for dette forumet';
$txt['show_fader'] = 'Skjule Nyheter';
$txt['hide_fader'] = 'Vise Nyheter';
$txt['hide_preview'] = 'Vise Forhåndsvisning';
$txt['show_preview'] = 'Skjule Forhåndsvisning';

$txt['show_blogtitles'] = 'Bare titler';
$txt['hide_blogtitles'] = 'Fulle innlegg';
$txt['messageindex_imagetop'] = 'Normale bilder';
$txt['messageindex_imageleft'] = 'Større bilder';

$txt['guest_sidebar'] = 'Vennlisgt <a href="' . $scripturl . '?action=login">logg inn</a> eller <a href="' . $scripturl . '?action=register">registrer deg</a> for å vise flere valg.';
$txt['blogs'] = 'Blogger';
$txt['most_read'] = 'Mest lest';
$txt['show_all_blogs'] = 'Vis alle brukere';
$txt['zoom'] = 'Vis 100%';

$txt['myscore'] = 'Min karakter(0-10)';
$txt['imdb'] = 'IMDB ID (tt12345)';
$txt['trailer'] = 'Youtube ID';
$txt['clip'] = 'Youtube ID';
$txt['spotify'] = 'Spotify ID';

$txt['year'] = 'år';
$txt['years'] = 'år';
$txt['month'] = 'måned';
$txt['months'] = 'måneder';
$txt['week'] = 'uke';
$txt['weeks'] = 'uker';
$txt['day'] = 'dag';
$txt['days'] = 'dager';
$txt['hour'] = 'time';
$txt['hours'] = 'timer';
$txt['minute'] = 'minutt';
$txt['minutes'] = 'minutter';
$txt['second'] = 'sekund';
$txt['seconds'] = 'sekunder';
$txt['ago'] = 'siden';
$txt['theremore'] = '%1$s flere kommentarer finnes.';
$txt['more_topics'] = 'Siste emner';

$txt['author_info'] = '%1$s ble med %2$s og har skrevet %3$s innlegg.';
$txt['author_info_website'] = '%1$s finner du også på <a href="%2$s"> %3$s</a>.';

$txt['imdb_rating'] = 'Karakter:';
$txt['imdb_year'] = 'År:';
$txt['imdb_directors'] = 'Regissør:';
$txt['imdb_actors'] = 'Skuespillere:';
$txt['imdb_writers'] = 'Forfattere:';
$txt['imdb_plot'] = 'Plot:';
$txt['imdb_runtime'] = 'Spilletid:';
$txt['imdb_genres'] = 'Genre:';

$txt['you_are_logged'] = 'You are logged in as ';
$txt['trackingcode'] = 'Javascript tracking code';
$txt['trackingcode_descr'] = 'Legg til tracking kode fra Google etc.';
$txt['modifying'] = 'Redigerer';

$txt['section'] = 'Seksjoner';

$txt['areyousure'] = 'Er du sikker at du vil';
$txt['areyousure2'] = 'Er du sikker';
$txt['show_userbar'] = 'Slå av og på';
$txt['width'] = 'Bredde';
$txt['height'] = 'Høyde';
$txt['community'] = 'Seksjoner';

?>