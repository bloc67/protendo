<?php
// Version: 2.0; Admin

global $settings, $scripturl;

$txt['admin_boards'] = 'Forumoversikt';
$txt['admin_users'] = 'Medlemmer';
$txt['admin_newsletters'] = 'Nyhetsbrev';
$txt['admin_edit_news'] = 'Nyheter';
$txt['admin_groups'] = 'Medlemsgrupper';
$txt['admin_members'] = 'Behandle medlemmer';
$txt['admin_members_list'] = 'Under er en liste over alle medlemmene som er registert på forumet.';
$txt['admin_next'] = 'Neste';
$txt['admin_censored_words'] = 'Sensurerte ord';
$txt['admin_censored_where'] = 'Skriv inn det ordet som skal sensureres til venstre og hva du vil erstatte det med til høyre.';
$txt['admin_censored_desc'] = 'På grunn av forumets natur er det noen ord som du kanskje vil forby dine medlemmer fra å bruke. Du kan skrive ordene du vil sensurere når medlemmene bruker det under.<br />Et tomt felt vil fjerne ordet helt.';
$txt['admin_reserved_names'] = 'Reserverte navn';
$txt['admin_template_edit'] = 'Endre forumdesignet';
$txt['admin_modifications'] = 'Modifikasjons-innstillinger';
$txt['admin_security_moderation'] = 'Sikkerhet og moderation';
$txt['admin_server_settings'] = 'Serverinnstillinger';
$txt['admin_reserved_set'] = 'Definér reserverte navn';
$txt['admin_reserved_line'] = 'Et reservert ord per linje.';
$txt['admin_basic_settings'] = 'Denne siden lar deg endre basisinnstillingene for forumet. Vær forsiktig med disse innstillingene, da de kan gjøre forumet ustabilt. Merk deg også at noen av innstillingene (f.eks. tidsformat) er basisinnstillinger som gjelder kun for gjester.';
$txt['admin_maintain'] = 'Aktiver vedlikeholdsmodus';
$txt['admin_title'] = 'Forumtittel';
$txt['admin_url'] = 'Forumadresse';
$txt['cookie_name'] = 'Navn på cookie (informasjonskapsel)';
$txt['admin_webmaster_email'] = 'E-postadresse til webmaster';
$txt['boarddir'] = 'Protendo-mappe';
$txt['sourcesdir'] = 'Mappe til Sources';
$txt['cachedir'] = 'Mappe for hurtiglager';
$txt['admin_news'] = 'Aktivér nyheter';
$txt['admin_guest_post'] = 'Tillat gjesteinnlegg';
$txt['admin_manage_members'] = 'Medlemmer';
$txt['admin_main'] = 'Hoved';
$txt['admin_config'] = 'Innstillinger';
$txt['admin_version_check'] = 'Detaljert versjonskontroll';
$txt['admin_smffile'] = 'Protendo-fil';
$txt['admin_smfpackage'] = 'Protendo-pakke';
$txt['admin_maintenance'] = 'Vedlikehold';
$txt['admin_image_text'] = 'Vise knapper som bilder istedet for tekst';
$txt['admin_credits'] = 'Medvirkende';
$txt['admin_agreement'] = 'Viser og krever registreringserklæring ved registrering';
$txt['admin_agreement_default'] = 'Vanlig';
$txt['admin_agreement_select_language'] = 'Språk som skal redigeres';
$txt['admin_agreement_select_language_change'] = 'Endre';
$txt['admin_delete_members'] = 'Slett valgte medlemmer';
$txt['admin_repair'] = 'Reparer alle fora og emner';
$txt['admin_main_welcome'] = 'Dette er ditt &quot;%1$s&quot;.  Her kan du redigere innstillinger, vedlikeholde forumet, lese loggføringer, installere modifikasjonspakker og en hel rekke andre ting.<div style="margin-top: 1ex;">Dersom det er problemer, se under "Vanlige feil og problemer". Dersom informasjonen ikke er til hjelp, må du gjerne <a href="http://www.simplemachines.org/community/index.php" target="_blank" class="new_win">sjekke forumet</a> etter løsninger.</div>Du kan også finne svar på dine spørsmål ved å klikke på <img src="' . $settings['images_url'] . '/helptopics.gif" alt="%2$s" title="%3$s" /> symbolet for tips om funksjoner og annen relatert informasjon.';
$txt['admin_news_desc'] = 'Skriv inn en nyhetsak per boks. B koder, slik som <span title="Dette er skikkelig fett!">[b]</span>, <span title="Jeg er tøff!">[i]</span> og <span title="Her skal det understrekes!">[u]</span> kan brukes i nyhetsakene, i tillegg til smileyer og basis HTML. Slett all tekst i en boks for å fjerne nyhetsaken.';
$txt['administrators'] = 'Administrator(er)';
$txt['admin_reserved_desc'] = 'Reserverte navn vil unngå at medlemmer registrerer seg med spesifikke brukernavn eller bruker ordene i deres visningsnavn. Velg et alternativ under før du lagrer.';
$txt['admin_activation_email'] = 'Send aktiverings e-post til nye medlemmer ved registrering';
$txt['admin_match_whole'] = 'Treff på hele ordet. Dersom ikke avkrysset, treff inni navn.';
$txt['admin_match_case'] = 'Treff på tegnsetting. Dersom ikke avkrysset, vil søket ikke ta hensyn til STORE eller små bokstaver.';
$txt['admin_check_user'] = 'Kontroller brukernavn.';
$txt['admin_check_display'] = 'Kontroller visningsnavn.';
$txt['admin_newsletter_send'] = 'Du kan sende e-post til alle fra denne siden. E-postadressene til de valgte medlemsgruppene skal komme under, men du må gjerne legge til eller fjerne e-postadressene etter eget ønske. Men sørg for at hver adresse er skilt ut etter: \'mottaker1; mottaker2\'.';
$txt['admin_fader_delay'] = 'Fadeforsinkelse (sekunder) mellom elementer i nyhetsfaderen';
$txt['admin_bbc'] = 'Vis BB-knapper ved innleggsskriving og på siden for å sende PM';

$txt['admin_backup_fail'] = 'Kunne ikke ta sikkerhetskopi av Settings.php - se til at Settings_bak.php er skrivbar og eksisterer.';
$txt['modSettings_info'] = 'Endre innstillinger som kontrollerer hvordan forumet fungerer.';
$txt['database_server'] = 'Serveren til databasen';
$txt['database_user'] = 'Brukernavn for databasen';
$txt['database_password'] = 'Passord for databasen';
$txt['database_name'] = 'Navn på databasen';
$txt['registration_agreement'] = 'Endre registreringserklæring';
$txt['registration_agreement_desc'] = 'Denne erklæringen vises når et medlem registrerer seg på forumet og må aksepteres før registreringen fortsetter.';
$txt['database_prefix'] = 'Prefiks på tabeller i databasen';
$txt['errors_list'] = 'Feilmeldinger på forumet';
$txt['errors_found'] = 'Følgende feilmeldinger ødelegger for forumet (tomt hvis ingen)';
$txt['errors_fix'] = 'Ønsker du å forsøke å reparere disse feilene?';
$txt['errors_do_recount'] = 'Alle feilene er rettet - ett bergingsområdet har blitt opprettet! Vennligst klikk knappen nedenfor for å telle hovedstatistikken på nytt.';
$txt['errors_recount_now'] = 'Telle statistikken på nytt';
$txt['errors_fixing'] = 'Reparerer feil på forumet';
$txt['errors_fixed'] = 'Alle feil reparert! Du må sjekke alle opprettede seksjoner, kategorier eller emner og finne ut hva du vil gjøre med dem.';
$txt['attachments_avatars'] = 'Vedlegg og avatarer';
$txt['attachments_desc'] = 'Her kan du administrere og behandle vedlagte filer. Du kan slette vedlegg etter størrelse og etter dato fra siden. Statistikk for vedlegg vises også under.';
$txt['attachment_stats'] = 'Vedleggstatistikk';
$txt['attachment_integrity_check'] = 'Sjekk vedlegg';
$txt['attachment_integrity_check_desc'] = 'Denne funksjonen vil sjekke integriteten og størrelser av vedlegg og filnavn oppført i databasen, og om nødvendig, fikse feilene som finnes.';
$txt['attachment_check_now'] = 'Kjør sjekk nå';
$txt['attachment_pruning'] = 'Slett vedlegg';
$txt['attachment_pruning_message'] = 'Melding å legge til innlegget';
$txt['attachment_pruning_warning'] = 'Er du sikker på at du vil slette disse vedleggene?\\nDette kan ikke angres!';
$txt['attachment_total'] = 'Totalt antall vedlegg';
$txt['attachmentdir_size'] = 'Total størrelse på mappe';
$txt['attachmentdir_size_current'] = 'Totale størrelse på gjeldende vedleggsmappe';
$txt['attachment_space'] = 'Totalt ledig plass i mappe';
$txt['attachment_space_current'] = 'Total plass tilgjengelig i gjeldende vedleggsmappe';
$txt['attachment_options'] = 'Alternativer for vedlegg';
$txt['attachment_log'] = 'Logg for vedlegg';
$txt['attachment_remove_old'] = 'Slett vedlegg eldre enn';
$txt['attachment_remove_size'] = 'Slett vedlegg større enn';
$txt['attachment_name'] = 'Navn på vedlegg';
$txt['attachment_file_size'] = 'Størrelse';
$txt['attachmentdir_size_not_set'] = 'Ingen maksimumsgrense er satt';
$txt['attachment_delete_admin'] = '[vedlegget ble slettet av en administrator.]';
$txt['live'] = 'Siste nytt fra Simple Machines...';
$txt['remove_all'] = 'Fjern alle';
$txt['approve_new_members'] = 'Administrator må godkjenne alle medlemmer';
$txt['agreement_not_writable'] = 'Advarsel - agreement.txt er ikke skrivbar. Endringer du gjør her vil ikke bli lagret';

$txt['version_check_desc'] = 'Dette viser versjonen til filene til din installasjon mot filene til den nyeste versjonen. Dersom noen av disse er utdatert, bør du laste ned og oppdatere til den nyeste versjonen fra <a href="http://www.simplemachines.org/">www.simplemachines.org</a>.';
$txt['version_check_more'] = '(mer detaljert)';

$txt['lfyi'] = 'Du kan ikke koble til simplemachines.org sin nyhetsfil.';

$txt['manage_calendar'] = 'Kalender';
$txt['manage_search'] = 'Søk';

$txt['smileys_manage'] = 'Smilefjes og innleggs-symboler';
$txt['smileys_manage_info'] = 'Installerer nye smileysett eller legger til smilefjes til eksisterende sett.';
$txt['package_info'] = 'Installerer nye funksjoner eller redigerer eksisterende med disse innstillingene.';
$txt['theme_admin'] = 'Design- og layoutinnstillinger';
$txt['theme_admin_info'] = 'Still inn og behandle dine design eller nullstill designinnstillinger.';
$txt['registration_center'] = 'Registreringsbehandling';
$txt['member_center_info'] = 'Vise medlemslista, søke etter medlemmer og behandle ikke-godkjente medlemmer og medlemmer som ikke har aktivert sin konto enda.';

$txt['viewmembers_name'] = 'Brukernavn (visningsnavn)';
$txt['viewmembers_online'] = 'Sist aktiv';
$txt['viewmembers_today'] = 'Idag';
$txt['viewmembers_day_ago'] = 'dag siden';
$txt['viewmembers_days_ago'] = 'dager siden';

$txt['display_name'] = 'Visningsnavn';
$txt['email_address'] = 'E-postadresse';
$txt['ip_address'] = 'IP-adresse';
$txt['member_id'] = 'ID';

$txt['unknown'] = 'ukjent';
$txt['security_wrong'] = 'Forsøk på administratorinnlogging!' . "\n" . 'Referanse: %1$s' . "\n" . 'Bruker agent: %2$s' . "\n" . 'IP: %3$s';

$txt['email_as_html'] = 'Send i HTML format.  (med dette aktivert kan du inkludere HTML-koder i din e-post.)';
$txt['email_parsed_html'] = 'Legg til &lt;br /&gt;s og &amp;nbsp;s til denne meldingen.';
$txt['email_variables'] = 'I denne meldingen kan du bruke noen &quot;variables&quot;.  Klikk <a href="' . $scripturl . '?action=helpadmin;help=emailmembers" onclick="return reqWin(this.href);" class="hjelp">her</a> for mer informasjon.';
$txt['email_force'] = 'Send denne til medlemmer selv om de har sikret seg mot kunngjøringer.';
$txt['email_as_pms'] = 'Send denne til disse medlemsgruppene som PM.';
$txt['email_continue'] = 'Fortsett';
$txt['email_done'] = 'ferdig.';

$txt['ban_title'] = 'Utestengte medlemmer';
$txt['ban_ip'] = 'Utesteng IP: (f.eks 192.168.12.213 or 128.0.*.*) - en oppføring per linje';
$txt['ban_email'] = 'Utesteng e-postadresse: (f.eks kjekkfyr@noen.no) - en oppføringper linje';
$txt['ban_username'] = 'Utesteng brukernavn: (f.eks tulling) - én oppføring per line';

$txt['ban_description'] = 'Her kan du stenge ute medlemmer etter IP, vertsnavn, brukernavn eller e-postadresse.';
$txt['ban_add_new'] = 'Legg til ny utestengelse';
$txt['ban_banned_entity'] = 'Detaljer for utestengelsen';
$txt['ban_on_ip'] = 'Utesteng IP (e.g. 192.168.10-20.*)';
$txt['ban_on_hostname'] = 'Utesteng vertsnavn (f.eks *.mil)';
$txt['ban_on_email'] = 'Utesteng e-postdomene (f.eks *@farligside.no)';
$txt['ban_on_username'] = 'Utesteng bruker';
$txt['ban_notes'] = 'Notater';
$txt['ban_restriction'] = 'Begrensning';
$txt['ban_full_ban'] = 'Full utestengelse';
$txt['ban_partial_ban'] = 'Delvis utestengelse';
$txt['ban_cannot_post'] = 'Kan ikke skrive';
$txt['ban_cannot_register'] = 'Kan ikke registere';
$txt['ban_cannot_login'] = 'Kan ikke logge inn';
$txt['ban_add'] = 'Legg til';
$txt['ban_edit_list'] = 'Liste over utestengelser';
$txt['ban_type'] = 'Utestengelsestype';
$txt['ban_days'] = 'dag(er)';
$txt['ban_will_expire_within'] = 'Vil være utestengt i';
$txt['ban_added'] = 'Lagt til';
$txt['ban_expires'] = 'Utgår';
$txt['ban_hits'] = 'Treff';
$txt['ban_actions'] = 'Handlinger';
$txt['ban_expiration'] = 'Utgår';
$txt['ban_reason_desc'] = 'Grunn for utestengelse som blir vist til utestengt medlem.';
$txt['ban_notes_desc'] = 'Notater som vil være til hjelp for andre administratorer.';
$txt['ban_remove_selected'] = 'Fjern valgte';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_remove_selected_confirm'] = 'Er du sikker på at du vil slette de valgte utestengelsene?';
$txt['ban_modify'] = 'Endre';
$txt['ban_name'] = 'Navn på utestengelse';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_edit'] = 'Endre utestengelse';
$txt['ban_add_notes'] = '<strong>Merk</strong>: etter å ha lagt til en utestengelse, kan du legge til flere treff for utestengelsen, som IP-adresser, vertsanavn og e-postadresser.';
$txt['ban_expired'] = 'Utgått / deaktivert';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_restriction_empty'] = 'Ingen begrensing er valgt.';

$txt['ban_triggers'] = 'Treff';
$txt['ban_add_trigger'] = 'Legg til treff';
$txt['ban_add_trigger_submit'] = 'Legg til';
$txt['ban_edit_trigger'] = 'Endre';
$txt['ban_edit_trigger_title'] = 'Endre treff for utestengelsen';
$txt['ban_edit_trigger_submit'] = 'Redigere';
$txt['ban_remove_selected_triggers'] = 'Fjern valgte utestengelsestreff';
$txt['ban_no_entries'] = 'Det er foreløpig ingen forbud som brukes.';

// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_remove_selected_triggers_confirm'] = 'Er du sikker på at du vil fjerne valgte utstengelsestreff?';
$txt['ban_trigger_browse'] = 'Bla igjennom treff';
$txt['ban_trigger_browse_description'] = 'Denne siden viser alle utestengelsesdetaljer.';

$txt['ban_log'] = 'Logg for utestengelser';
$txt['ban_log_description'] = 'Denne loggen viser forsøk fra utestengte medlemmer som vil komme inn på forumet (gjelder kun de som er fullstendig utestengt og de som ikke kan registere seg).';
$txt['ban_log_no_entries'] = 'Det er ingen loggoppføringer over forbudte elementer';
$txt['ban_log_ip'] = 'IP';
$txt['ban_log_email'] = 'E-postadresse';
$txt['ban_log_member'] = 'Medlem';
$txt['ban_log_date'] = 'Dato';
$txt['ban_log_remove_all'] = 'Fjern alle';
$txt['ban_log_remove_all_confirm'] = 'Er du sikker på at du vil fjerne alle elementene i loggen?';
$txt['ban_log_remove_selected'] = 'Fjern valgte';
$txt['ban_log_remove_selected_confirm'] = 'Er du sikker på at du vil fjerne valgte elementer i loggen?';
$txt['ban_no_triggers'] = 'Det er ingen triggere på forbudte elementer';

$txt['settings_not_writable'] = 'Disse innstillingene kan ikke bli endret fordi Settings.php er satt til skrivebeskyttet.';

$txt['maintain_title'] = 'Vedlikehold forumet';
$txt['maintain_info'] = 'Optimaliser tabeller, ta sikkerhetskopi, sjekk etter feil og rens seksjoner med disse verkt&oslash;yene.';
$txt['maintain_sub_database'] = 'Database';
$txt['maintain_sub_routine'] = 'Rutine';
$txt['maintain_sub_members'] = 'Medlemmer';
$txt['maintain_sub_topics'] = 'Emner';
$txt['maintain_done'] = 'Vedlikehold utført \'%1$s\'.';
$txt['maintain_no_errors'] = 'Gratulerer, ingen feil funnet!';

$txt['maintain_tasks'] = 'Planlagte oppgaver';
$txt['maintain_tasks_desc'] = 'Behandle alle oppgavene planlagt av Protendo.';

$txt['scheduled_log'] = 'Utførte oppgaver';
$txt['scheduled_log_desc'] = 'Lister alle oppgavene som er utført.';
$txt['admin_log'] = 'Administratorlogg';
$txt['admin_log_desc'] = 'Lister administrative oppgaver som har blitt utført av admin på forumet ditt.';
$txt['moderation_log'] = 'Moderasjonslogg';
$txt['moderation_log_desc'] = 'Lister moderasjonsaktiviteter som er utført av moderatorer på forumet ditt.';
$txt['spider_log_desc'] = 'Gjennomgå oppføringene relatert til søkemotorers aktivitet på forumet ditt.';
$txt['pruning_log_desc'] = 'Bruk disse verktøyene til å rydde i gamle oppføringer i de ulike loggene.';

$txt['mailqueue_title'] = 'E-post';

$txt['db_error_send'] = 'Send e-post ved tilkoblingsproblemer til MySQL-databasen?';
$txt['db_persist'] = 'Bruke en dedikert tilkobling?';
$txt['ssi_db_user'] = 'Database brukernavn til bruk i SSI-modus';
$txt['ssi_db_passwd'] = 'Database passord for å bruke i SSI-modus';

$txt['default_language'] = 'Språk brukt på forumet';

$txt['maintenance_subject'] = 'Emne på meldingen';
$txt['maintenance_message'] = 'Selve meldingen';

$txt['errlog_desc'] = 'Loggen over feil lagrer alle feilmeldinger som oppstår på forumet. Dette er en kronologisk liste over når feilene oppstod.  For å slette noen av meldingene fra loggen, huk av i boksen ved siden av, og trykk på %1$s knappen på bunnen av siden.';
$txt['errlog_no_entries'] = 'Det er foreløpig ingen feiloppføringer i loggen';

$txt['theme_settings'] = 'Designinnstillinger';
$txt['theme_current_settings'] = 'Gjeldende design';

$txt['dvc_your'] = 'Din versjon';
$txt['dvc_current'] = 'Nyeste versjon';
$txt['dvc_sources'] = 'Ressurser';
$txt['dvc_default'] = 'Hoveddesign';
$txt['dvc_templates'] = 'Gjeldende design';
$txt['dvc_languages'] = 'Språkfiler';

$txt['smileys_default_set_for_theme'] = 'Velg et smileysett for dette designet';
$txt['smileys_no_default'] = '(bruk forumets vanlige)';

$txt['censor_test'] = 'Test sensurerte ord';
$txt['censor_test_save'] = 'Test';
$txt['censor_case'] = 'Ta ikke hensyn til store eller små bokstaver.';
$txt['censor_whole_words'] = 'Kontroller kun hele ord.';

$txt['admin_confirm_password'] = '(bekreft)';
$txt['admin_incorrect_password'] = 'Feil passord';

$txt['date_format'] = '(ÅÅÅÅ-MM-DD)';
$txt['undefined_gender'] = 'Ikke definert';
$txt['age'] = 'Alder';
$txt['activation_status'] = 'Status for aktivering';
$txt['activated'] = 'Aktivert';
$txt['not_activated'] = 'Ikke aktivert';
$txt['primary'] = 'Primær';
$txt['additional'] = 'Ekstra';
$txt['messenger_address'] = 'Messengeradresse';
$txt['wild_cards_allowed'] = 'jokertegn som * og ? er tillatt';
$txt['search_for'] = 'Søk etter';
$txt['member_part_of_these_membergroups'] = 'Medlemmet er en del av følgende medlemsgrupper';
$txt['membergroups'] = 'Medlemsgrupper';
$txt['confirm_delete_members'] = 'Er du sikker på at du vil slette de valgte medlemmene?';

$txt['support_credits_title'] = 'Hjelp og medvirkende';
$txt['support_credits_info'] = 'Få hjelp med vanlige problemer og versjonsinformasjon du kan bruke som kilde når du har problemer med å få Protendo til å kjøre normalt.';
$txt['support_title'] = 'Hjelpinformasjon';
$txt['support_versions_current'] = 'Nyeste Protendo-versjon';
$txt['support_versions_forum'] = 'Forumets Protendo-versjon';
$txt['support_versions_php'] = 'PHP-versjon';
$txt['support_versions_db'] = '%1$s versjon';
$txt['support_versions_server'] = 'Serverversjon';
$txt['support_versions_gd'] = 'GD-versjon';
$txt['support_versions'] = 'Versjoninformasjon';
$txt['support_resources'] = 'Support ressurser';
$txt['support_resources_p1'] = 'Vår <a href="%1$s">Online brukerhåndbok</a> gir den viktigste dokumentasjon for Protendo. Protendo Online brukerhåndbok har mange dokumenter til å hjelpe deg å få svar på spørsmål og forklare <a href="%2$s">funksjoner</a>, <a href="%3$s">Innstillinger</a>, <a href="%4$s">Temaer</a>, <a href="%5$s">Pakker</a>, etc. Online brukerhåndbok dokumenterer hvert område av Protendo grundig og bør kunne gi deg svare på de fleste spørsmålene raskt.';
$txt['support_resources_p2'] = 'Hvis du ikke finner svar på dine spørsmål i Online brukerhåndbok, kan du søke i vårt <a href="%1$s">Support Forum</a> eller be om hjelp i enten vårt <a href="%2$s">Engelske</a> eller en av våre mange <a href="%3$s">internasjonale Support fora</a>. Protendo Support Forum kan brukes til <a href="%4$s">support</a>, <a href="%5$s">tilpasninger</a>, og mange andre ting som å diskuterer Protendo , finne en vert, og diskutere administrative problemer med andre forum administratorer.';

$txt['support_latest'] = 'Vanlige feil og problemer';
$txt['support_latest_fetch'] = 'Henter hjelpinformasjon...';

$txt['edit_permissions_info'] = 'Endre begrensninger og tilgjengelige funksjoner globalt eller til spesifikke seksjoner.';
$txt['membergroups_members'] = 'Ugrupperte medlemmer';
$txt['membergroups_guests'] = 'Uregistrerte gjester';
$txt['membergroups_guests_na'] = 'i/t';
$txt['membergroups_add_group'] = 'Legg til gruppe';
$txt['membergroups_permissions'] = 'Rettigheter';

$txt['permitgroups_restrict'] = 'Begrenset';
$txt['permitgroups_standard'] = 'Standard';
$txt['permitgroups_moderator'] = 'Moderator';
$txt['permitgroups_maintenance'] = 'Vedlikehold';
$txt['permitgroups_inherit'] = 'Hent fra andre';

$txt['confirm_delete_attachments_all'] = 'Er du sikker på at du vil slette alle vedlegg?';
$txt['confirm_delete_attachments'] = 'Er du sikker på at du vil slette valgt(e) vedlegg(ene)?';
$txt['attachment_manager_browse_files'] = 'Bla igjennom filer';
$txt['attachment_manager_repair'] = 'Vedlikehold';
$txt['attachment_manager_avatars'] = 'Avatarer';
$txt['attachment_manager_attachments'] = 'Vedlegg';
$txt['attachment_manager_thumbs'] = 'Miniatyrer';
$txt['attachment_manager_last_active'] = 'Sist aktiv';
$txt['attachment_manager_member'] = 'Medlem';
$txt['attachment_manager_avatars_older'] = 'Slett avatarer fra medlemmer som ikke har vært aktive på mer enn';
$txt['attachment_manager_total_avatars'] = 'Antall avatarer';

$txt['attachment_manager_avatars_no_entries'] = 'Det er foreløpig ingen avatarer.';
$txt['attachment_manager_attachments_no_entries'] = 'Det er ingen vedlegg.';
$txt['attachment_manager_thumbs_no_entries'] = 'Det er foreløpig ingen miniatyrbilder.';

$txt['attachment_manager_settings'] = 'Vedlegginnstillinger';
$txt['attachment_manager_avatar_settings'] = 'Avatarinnstillinger';
$txt['attachment_manager_browse'] = 'Bla igjennom filer';
$txt['attachment_manager_maintenance'] = 'Filvedlikehold';
$txt['attachment_manager_save'] = 'Lagre';

$txt['attachmentEnable'] = 'Vedleggsmodus';
$txt['attachmentEnable_deactivate'] = 'Deaktivere vedlegg';
$txt['attachmentEnable_enable_all'] = 'Aktivere alle vedlegg';
$txt['attachmentEnable_disable_new'] = 'Deaktivere nye vedlegg';
$txt['attachmentCheckExtensions'] = 'Kontroller filendelse på vedlegg';
$txt['attachmentExtensions'] = 'Tillatte filendelser på vedlegg';
$txt['attachmentRecodeLineEndings'] = 'Endre linjeavslutninger i tekstlige vedlegg';
$txt['attachmentShowImages'] = 'Vise bildevedlegg som bilde under innlegget';
$txt['attachmentEncryptFilenames'] = 'Krypter lagrede filnavn';
$txt['attachmentUploadDir'] = 'Vedleggsmappe<div class="smalltext"><a href="' . $scripturl . '?action=admin;area=manageattachments;sa=attachpaths">Konfigurere flere vedleggsmapper</a></div>';
$txt['attachmentUploadDir_multiple'] = 'Vedleggsmappe';
$txt['attachmentUploadDir_multiple_configure'] = '<a href="' . $scripturl . '?action=admin;area=manageattachments;sa=attachpaths">[Konfigurere flere vedleggsmapper]</a>';
$txt['attachmentDirSizeLimit'] = 'Maksimumsstørrelse for mappe';
$txt['attachmentPostLimit'] = 'Maksimumsstørrelse for vedlegg i innlegg';
$txt['attachmentSizeLimit'] = 'Maksimumsstørrelse per vedlegg';
$txt['attachmentNumPerPostLimit'] = 'Maks antall vedlegg per innlegg';
$txt['attachment_gd_warning'] = 'GD-modulen er for øyeblikket ikke installert. Bilde re-koding er ikke mulig.';
$txt['attachment_image_reencode'] = 'Re-kode potensielt farlige bildevedlegg';
$txt['attachment_image_reencode_note'] = '(krever modulen GD installert)';
$txt['attachment_image_paranoid_warning'] = 'Den omfattende sikkerhetskontroller kan resultere i at et stort antall avviste vedlegg.';
$txt['attachment_image_paranoid'] = 'Utfør omfattende sikkerhetskontroller på opplastet bildevedlegg';
$txt['attachmentThumbnails'] = 'Endre bildestørrelse ved visning under innlegget';
$txt['attachment_thumb_png'] = 'Lagre miniatyrbilder som PNG';
$txt['attachmentThumbWidth'] = 'Maksimumsbredde for miniatyr';
$txt['attachmentThumbHeight'] = 'Maksimumshøyde for miniatyr';

$txt['attach_dir_does_not_exist'] = 'Eksisterer ikke';
$txt['attach_dir_not_writable'] = 'Ikke skrivbar';
$txt['attach_dir_files_missing'] = 'Filer mangler (<a href="' . $scripturl . '?action=admin;area=manageattachments;sa=repair;%2$s=%1$s">Reparér</a>)';
$txt['attach_dir_unused'] = 'Ubrukt';
$txt['attach_dir_ok'] = 'OK';

$txt['attach_path_manage'] = 'Behandle vedleggsstier';
$txt['attach_paths'] = 'Vedleggsstier';
$txt['attach_current_dir'] = 'Gjeldende mappe';
$txt['attach_path'] = 'Stier';
$txt['attach_current_size'] = 'Gjeldende størrelse (KB)';
$txt['attach_num_files'] = 'Filer';
$txt['attach_dir_status'] = 'Status';
$txt['attach_add_path'] = 'Legg til sti';
$txt['attach_path_current_bad'] = 'Ugyldig vedleggssti.';

$txt['mods_cat_avatars'] = 'Avatarer';
$txt['avatar_directory'] = 'Avatarmappe';
$txt['avatar_url'] = 'Avatarsadresse';
$txt['avatar_dimension_note'] = '(0 = ingen grense)';
$txt['avatar_max_width_external'] = 'Maksimumsbredde for ekstern avatar';
$txt['avatar_max_height_external'] = 'Maksimumshø for ekstern avatar';
$txt['avatar_action_too_large'] = 'Om avataren er for stor...';
$txt['option_refuse'] = 'Ikke godta den';
$txt['option_html_resize'] = 'Bruk HTML for å endre størrelsen';
$txt['option_js_resize'] = 'Bruk Javascript for å endre størrelsen';
$txt['option_download_and_resize'] = 'Last den ned og endre størrelsen på den (krever modulen GD installert)';
$txt['avatar_max_width_upload'] = 'Maksimumsbredde på opplastede avatarer (0 = ubegrenset)';
$txt['avatar_max_height_upload'] = 'Maksimumshøyde på opplastede avatarer (0 = ubegrenset)';
$txt['avatar_resize_upload'] = 'Endre størrelsen på for store avatarer';
$txt['avatar_resize_upload_note'] = '(krever modulen GD installert)';
$txt['avatar_download_png'] = 'Bruk PNG for avatarene som har fått ny størrelse';
$txt['avatar_gd_warning'] = 'Modulen GD er ikke installert, derfor vil noen av avatarfunksjonene være deaktivert.';
$txt['avatar_external'] = 'Eksertne avatarer';
$txt['avatar_upload'] = 'Opplastbare avatarer';
$txt['avatar_server_stored'] = 'Lokale avatarer';
$txt['avatar_server_stored_groups'] = 'Medlemsgrupper som har tilgang til å velge en lokal avatar';
$txt['avatar_upload_groups'] = 'Medlemsgrupper som har tilgang til å laste opp en avatar til serveren';
$txt['avatar_external_url_groups'] = 'Medlemsgrupper som har tilgang til å skrive en ekstern adresse';
$txt['avatar_select_permission'] = 'Velg rettigheter for hver gruppe';
$txt['avatar_download_external'] = 'Laste ned avatar fra spesifisert adresse';
$txt['custom_avatar_enabled'] = 'Laste opp avatarer til...';
$txt['option_attachment_dir'] = 'Vedleggmappe';
$txt['option_specified_dir'] = 'Egen mappe...';
$txt['custom_avatar_dir'] = 'Opplastningsmappe';
$txt['custom_avatar_dir_desc'] = 'Dette skal ikke være det samme som hovedmappen på serveren.';
$txt['custom_avatar_url'] = 'Opplastningsadresse';
$txt['custom_avatar_check_empty'] = 'Den egendefinerte avatarmappen du har angitt kan være tom eller ugyldig. Vennligst påse at disse innstillingene er riktige.';
$txt['avatar_reencode'] = 'Endre potensielt farlig avatarer';
$txt['avatar_reencode_note'] = '(krever modulen GD installert)';
$txt['avatar_paranoid_warning'] = 'Den omfattende sikkerhetskontroller kan resultere i et stort antall avviste avatarer.';
$txt['avatar_paranoid'] = 'Utfør omfattende sikkerhetskontroller på opplastede avatarer';

$txt['repair_attachments'] = 'Behandle vedlegg';
$txt['repair_attachments_complete'] = 'Vedlikehold utført';
$txt['repair_attachments_complete_desc'] = 'Alle valgte feil har nå blitt reparert';
$txt['repair_attachments_no_errors'] = 'Ingen feil funnet!';
$txt['repair_attachments_error_desc'] = 'Følgende feil ble funnet under vedlikeholdet. Huk av i boksen ved siden av feilene du ønsker å reparere og klikk fortsett.';
$txt['repair_attachments_continue'] = 'Fortsett';
$txt['repair_attachments_cancel'] = 'Avbryt';
$txt['attach_repair_missing_thumbnail_parent'] = '%1$d miniatyrer mangler tilhørende vedlegg';
$txt['attach_repair_parent_missing_thumbnail'] = '%1$d vedlegg markert med manglende miniatyrer';
$txt['attach_repair_file_missing_on_disk'] = '%1$d vedlegg/avatarer er lagret i databasen, men eksisterer ikke lenger på disken';
$txt['attach_repair_file_wrong_size'] = '%1$d vedlegg/avatarer blir rapportert med feil filstørrelse';
$txt['attach_repair_file_size_of_zero'] = '%1$d vedlegg/avatarer blir rapportert med å ha en filstørrelse på null bytes. (disse vil nå bli slettet)';
$txt['attach_repair_attachment_no_msg'] = '%1$d vedlegg uten tilhørende innlegg';
$txt['attach_repair_avatar_no_member'] = '%1$d avatater uten tilhørende medlem';
$txt['attach_repair_wrong_folder'] = '%1$d vedlegg er i feil mappe';

$txt['news_title'] = 'Nyheter og nyhetsbrev';
$txt['news_settings_desc'] = 'Her kan du endre innstillingene og rettigheter relatert til nyheter og nyhetsbrev.';
$txt['news_settings_submit'] = 'Lagre';
$txt['news_mailing_desc'] = 'Fra dette bilde kan du sende melding til alle medlemmer som har registert seg og skrevet inn sin e-postadresse. Du må gjerne endre lista, eller sende melding til alle. Nyttig for oppdateringer eller nyheter.';
$txt['groups_edit_news'] = 'Grupper som har tilgang til å endre nyhetselementer';
$txt['groups_send_mail'] = 'Grupper som har tilgang til å sende ut nyhetsbrev';
$txt['xmlnews_enable'] = 'Aktivere XML/RSS-nyheter';
$txt['xmlnews_maxlen'] = 'Maksimal lengde på nyhetsak:<div class="smalltext">(0 for ingen grense, som ikke er så lurt.)</div>';
$txt['editnews_clickadd'] = 'Klikk her for å legge til ny nyhetsak.';
$txt['editnews_remove_selected'] = 'Fjern valgte';
$txt['editnews_remove_confirm'] = 'Er du sikker på at du vil fjerne valgt(e) nyhet(er)?';
$txt['censor_clickadd'] = 'Klikk her for å legge til nytt ord.';

$txt['layout_controls'] = 'Forum';
$txt['logs'] = 'Logg';
$txt['generate_reports'] = 'Opprett rapporter';

$txt['update_available'] = 'En oppdatering er tilgjengelig!';
$txt['update_message'] = 'Du bruker en utdatert versjon av Protendo, som inneholder noen småfeil som har blitt fikset.
        Det er anbefalt at du <a href="" id="update-link">oppdaterer forumet</a> til den nyeste versjonen så snart som mulig. Det tar kun noen minutter!';

$txt['manageposts'] = 'Innlegg og emner';
$txt['manageposts_title'] = 'Behandle innlegg og emner';
$txt['manageposts_description'] = 'Her kan du behandle innstillinger relatert til emner og innlegg.';

$txt['manageposts_seconds'] = 'sekunder';
$txt['manageposts_minutes'] = 'minutter';
$txt['manageposts_characters'] = 'tegn';
$txt['manageposts_days'] = 'dager';
$txt['manageposts_posts'] = 'innlegg';
$txt['manageposts_topics'] = 'emner';

$txt['manageposts_settings'] = 'Innlegginnstillinger';
$txt['manageposts_settings_description'] = 'Her kan du endre alt som er relatert til innlegg og skriving av innlegg.';
$txt['manageposts_settings_submit'] = 'Lagre';

$txt['manageposts_bbc_settings'] = 'BB-kode';
$txt['manageposts_bbc_settings_description'] = 'BB-kode er brukt til å formatere innlegg på forumet. For eksempel, du vil utheve ordet "hus", så kan du skrive [b]hus[/B]. Alle BB-koder har klammer rundt seg ( [ og ] ).';
$txt['manageposts_bbc_settings_title'] = 'BB-kodeinnstillinger';
$txt['manageposts_bbc_settings_submit'] = 'Lagre';

$txt['manageposts_topic_settings'] = 'Emneinnstillinger';
$txt['manageposts_topic_settings_description'] = 'Her kan du endre innstillinger som er relatert til emner.';
$txt['manageposts_topic_settings_submit'] = 'Lagre';

$txt['removeNestedQuotes'] = 'Fjern anførselstegn ved sitering';
$txt['enableEmbeddedFlash'] = 'Tillate kjøring av Flash-animasjoner i innlegg';
$txt['enableEmbeddedFlash_warning'] = 'kan være en sikkerhetsrisiko!';
$txt['enableSpellChecking'] = 'Aktivere stavekontroll';
$txt['enableSpellChecking_warning'] = 'dette fungerer ikke på alle servere!';
$txt['disable_wysiwyg'] = 'Deaktiver WYSIWYG tekstbehandler';
$txt['max_messageLength'] = 'Maksimalt antall tegn i innlegg';
$txt['max_messageLength_zero'] = '0 for ubegrenset.';
$txt['fixLongWords'] = 'Del opp ord lenger enn';
$txt['fixLongWords_zero'] = '0 for deaktivert.';
$txt['fixLongWords_warning'] = 'dette fungerer ikke på alle servere!';
$txt['topicSummaryPosts'] = 'Antall innlegg for å vise på emneoppsummering';
$txt['spamWaitTime'] = 'Tid (i sekunder) mellom innlegg fra samme IP';
$txt['edit_wait_time'] = 'Tidsbegrensing for usynlig redigering';
$txt['edit_disable_time'] = 'Maksimumstid for endring av innlegg';
$txt['edit_disable_time_zero'] = '0 for deaktivert';

$txt['enableBBC'] = 'Aktivere BB-kode?';
$txt['enablePostHTML'] = 'Aktiver <em>vanlige</em> HTML-koder i innlegg';
$txt['autoLinkUrls'] = 'Automatisk gjør adresser om til linker';
$txt['disabledBBC'] = 'Aktivere BB-kode';
$txt['bbcTagsToUse'] = 'Aktiverte BB-koder';
$txt['bbcTagsToUse_select'] = 'Velg kodene du vil tillate';
$txt['bbcTagsToUse_select_all'] = 'Velg alle';

$txt['enableStickyTopics'] = 'Aktivere prioriterte emner';
$txt['enableParticipation'] = 'Aktivere ikoner for deltakelser';
$txt['oldTopicDays'] = 'Tid før emnet er markert som gammelt ved svar';
$txt['oldTopicDays_zero'] = '0 for deaktivert';
$txt['defaultMaxTopics'] = 'Antall emner per side i oversikten';
$txt['defaultMaxMessages'] = 'Antall innlegg per side i et emne';
$txt['hotTopicPosts'] = 'Antall innlegg for et aktivt emne';
$txt['hotTopicVeryPosts'] = 'Antall innlegg for et veldig aktivt emne';
$txt['enableAllMessages'] = 'Maks antall innlegg for å vise &quot;Alle&quot; innlegg';
$txt['enableAllMessages_zero'] = '0 for å aldri vise &quot;Alle&quot;';
$txt['disableCustomPerPage'] = 'Deaktiver brukerdefinert emne/meldings telling per side';
$txt['enablePreviousNext'] = 'Aktivere linker for forrige/neste emne';

$txt['not_done_title'] = 'Ikke fullført ennå!';
$txt['not_done_reason'] = 'For å ikke overbelaste serveren, har det du jobber med blitt midlertidig stanset. Det skal fortsette innen noen sekunder. Dersom det ikke gjør det, klikk på fortsett under.';
$txt['not_done_continue'] = 'Fortsett';

$txt['general_settings'] = 'Generelt';
$txt['database_paths_settings'] = 'Database og stier';
$txt['cookies_sessions_settings'] = 'Informasjonskapsler og økter';
$txt['caching_settings'] = 'Internminne';
$txt['load_balancing_settings'] = 'Balansert maskinkraft';

$txt['language_configuration'] = 'Språk';
$txt['language_description'] = 'Denne delen lar deg redigere språk installert på forumet ditt, laste ned nye fra Simple Machines nettsted. Du kan også redigere språkrelaterte innstillinger her.';
$txt['language_edit'] = 'Rediger språk';
$txt['language_add'] = 'Nytt språk';
$txt['language_settings'] = 'Innstillinger';

$txt['advanced'] = 'Avansert';
$txt['simple'] = 'Simpel';

$txt['admin_news_select_recipients'] = 'Velg hvem som skal motta en kopi av nyhetsbrevet';
$txt['admin_news_select_group'] = 'Medlemsgrupper';
$txt['admin_news_select_group_desc'] = 'Velg grupper som skal motta dette nyhetsbrevet.';
$txt['admin_news_select_members'] = 'Medlemmer';
$txt['admin_news_select_members_desc'] = 'Andre medlemmer som skal motta nyhetsbrev.';
$txt['admin_news_select_excluded_members'] = 'Ekskluderte medlemmer';
$txt['admin_news_select_excluded_members_desc'] = 'Medlemmer som ikke skal motta nyhetsbrev.';
$txt['admin_news_select_excluded_groups'] = 'Ekskluderte grupper';
$txt['admin_news_select_excluded_groups_desc'] = 'Velg grupper som ikke skal motta nyhetsbrev.';
$txt['admin_news_select_email'] = 'E-postadresser';
$txt['admin_news_select_email_desc'] = 'En semi-kolon separert liste over e-postadresser som det skal sendes nyhetsbrevet til. (dvs. Adresse1; Adresse2)';
$txt['admin_news_select_override_notify'] = 'Overstyr meldingsinnstillingene';
// Use entities in below.
$txt['admin_news_cannot_pm_emails_js'] = 'Du kan ikke sende en personlig melding til en e-postadresse. Hvis du fortsetter vil alle e-postadresser bli ignorert.\\n\\nEr du sikker på at du ønsker å gjøre dette?';

$txt['mailqueue_browse'] = 'Bla gjennom køen';
$txt['mailqueue_settings'] = 'Innstillinger';

$txt['admin_search'] = 'Hurtigsøk';
$txt['admin_search_type_internal'] = 'Oppgave/Innstilling';
$txt['admin_search_type_member'] = 'Medlem';
$txt['admin_search_type_online'] = 'Online brukerhåndbok';
$txt['admin_search_go'] = 'Ok';
$txt['admin_search_results'] = 'Søkeresultater';
$txt['admin_search_results_desc'] = 'Søkeresultat: &quot;%1$s&quot;';
$txt['admin_search_results_again'] = 'Søk på nytt';
$txt['admin_search_results_none'] = 'Ingen treff funnet!';

$txt['admin_search_section_sections'] = 'Seksjon';
$txt['admin_search_section_settings'] = 'Innstillinger';

$txt['core_settings_title'] = 'Kjernefunksjoner';
$txt['mods_cat_features'] = 'Generelt';
$txt['mods_cat_security_general'] = 'Generelt';
$txt['antispam_title'] = 'Antispam';
$txt['mods_cat_modifications_misc'] = 'Diverse';
$txt['mods_cat_layout'] = 'Utforming';
$txt['karma'] = 'Karma';
$txt['moderation_settings_short'] = 'Moderering';
$txt['signature_settings_short'] = 'Signaturer';
$txt['custom_profile_shorttitle'] = 'Profilfelter';
$txt['pruning_title'] = 'Slette logger';

$txt['boardsEdit'] = 'Rediger forum';
$txt['mboards_new_cat'] = 'Opprett ny kategori';
$txt['manage_holidays'] = 'Behandle helligdager';
$txt['calendar_settings'] = 'Kalenderinnstillinger';
$txt['search_weights'] = 'Vektlegging';
$txt['search_method'] = 'Søkemetode';

$txt['smiley_sets'] = 'Stiler smilefjes';
$txt['smileys_add'] = 'Nytt smilefjes';
$txt['smileys_edit'] = 'Rediger smilefjes';
$txt['smileys_set_order'] = 'Sett rekkefølgen på smilefjes';
$txt['icons_edit_message_icons'] = 'Rediger innleggs-symboler';

$txt['membergroups_new_group'] = 'Legg til gruppe';
$txt['membergroups_edit_groups'] = 'Rediger medlemsgrupper';
$txt['permissions_groups'] = 'Rettigheter etter medlemsgruppe';
$txt['permissions_boards'] = 'Forumbaserte rettigheter';
$txt['permissions_profiles'] = 'Rediger profiler';
$txt['permissions_post_moderation'] = 'Moderering av innlegg';

$txt['browse_packages'] = 'Bla igjennom pakker';
$txt['download_packages'] = 'Last ned pakker';
$txt['installed_packages'] = 'Installerte pakker';
$txt['package_file_perms'] = 'Filrettigheter';
$txt['package_settings'] = 'Alternativer';
$txt['themeadmin_admin_title'] = 'Behandling og installasjon';
$txt['themeadmin_list_title'] = 'Designinnstillinger';
$txt['themeadmin_reset_title'] = 'Medlemsalternativer';
$txt['themeadmin_edit_title'] = 'Endre design';
$txt['admin_browse_register_new'] = 'Registrér nytt medlem';

$txt['search_engines'] = 'Søkemotorer';
$txt['spiders'] = 'Indekseringsroboter';
$txt['spider_logs'] = 'Logg';
$txt['spider_stats'] = 'Status';

$txt['paid_subscriptions'] = 'Betalte abonnementer';
$txt['paid_subs_view'] = 'Vis Abonnement';
$txt['admin_browse_why_register'] = 'Hvorfor registrere';

$txt['plugins_installed'] = 'Available plugins';
$txt['version'] = 'Versjon';
$txt['hide_menuitem_off'] = 'Vis i meny';
$txt['hide_menuitem_on'] = 'IKKE vis i meny';
$txt['sections'] = 'Seksjoner';
$txt['new_section']  = 'Create a new section';
$txt['section_descr'] = 'Sections allows you to goup together several boards using a single entry page. It can use any boardtype and each section has its own menu item.';
$txt['actions']  = 'Actions';
$txt['layout']  = 'Custom layout';
$txt['show_in_menu'] = 'Show section on the main menu';
$txt['show_plugins'] = 'Show the section\'s boardtypes as submenu';
$txt['choosepreset'] = 'Preset layouts are available once you selected the boards to this section.';
$txt['preset'] = 'Select plugin order, titles and preset layout';
$txt['zeropreset'] = '-- no preset -- (using the custom layout instead)';

$txt['cleararea'] = 'Clear';
$txt['cleararea2'] = 'Clear the textarea.';
$txt['plugintext'] = 'The boardtypes';
$txt['plugintext2'] = 'Selected boards';

$txt['editsection_help'] = 'There are additional keywords you can use when you are editing it manually, their use is listed below: <br>
	<ul>
		<li>{blog(16)} means to render the blog posts and use the grid system to divide into more columns.</li>
		<li>{single(blog)} means to render the FIRST blog post in full width, then the rest in a listing underneath.</li>
		<li>{listing(blog)} means to render the blog post as a listing underneath.</li>
	</ul>
	The grid system is a simple 16-part system, so if you like a 50% width, you need to use "8" as the column number. "4" means 25%, and of course "16" means full width. Theres a special number for 33%, simply
	use "33" on that. 
	<b>Remember to always save after each session, and do not turn on the presets again - your custom layout will then be erased.</b>';

$txt['show_custom_html'] = 'Show custom HTML';
$txt['show_section'] = 'Show specific section';
$txt['show_plugin'] = 'Show specific plugin';
$txt['choose'] = '-- choose --';

$txt['newsfader_option'] = 'Show newsfader';

?>