<?php
// Version: 2.0; Reports

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['generate_reports_desc'] = 'Fra denne side kan du lage ulike rapporter som kan hjelpe til administrasjonen av forumet. Bare følg instruksjonene under for å velge dine innstillinger.';
$txt['generate_reports_continue'] = 'Fortsett';
$txt['generate_reports_type'] = 'Velg type rapport';
$txt['gr_type_boards'] = 'Fora';
$txt['gr_type_desc_boards'] = 'Rapporter som viser gjeldende innstillinger og rettigheter for foraene på ditt forum.';
$txt['gr_type_board_perms'] = 'Forumrettigheter';
$txt['gr_type_desc_board_perms'] = 'Lager rapporter som viser rettighetene til hver medlemsgruppe basert på forum.';
$txt['gr_type_member_groups'] = 'Medlemsgrupper';
$txt['gr_type_desc_member_groups'] = 'Rapport som viser innstillingene for hver medlemsgruppe på ditt forum.';
$txt['gr_type_group_perms'] = 'Grupperettigheter';
$txt['gr_type_desc_group_perms'] = 'Rapport som viser rettighetene til hver medlemsgruppe på forumet.';
$txt['gr_type_staff'] = 'Ansatte';
$txt['gr_type_desc_staff'] = 'Rapporten viser en oversikt over hvem som har maktposisjoner på forumet.';

$txt['full_member'] = 'Fullstendig medlem';
$txt['global_boards'] = 'Globale fora';
$txt['results'] = 'Resultater';

// Board permissions
$txt['board_perms_permission'] = 'Rettighet';
$txt['board_perms_allow'] = 'Tillat';
$txt['board_perms_deny'] = 'Nekt';
$txt['board_perms_name_announce_topic'] = 'Kunngjøre emner';
$txt['board_perms_name_approve_posts'] = 'Godkjenne innlegg';
$txt['board_perms_name_delete_any'] = 'Slette alle innlegg';
$txt['board_perms_name_delete_own'] = 'Slette egne innlegg';
$txt['board_perms_name_delete_replies'] = 'Slette svar til egne emner';
$txt['board_perms_name_lock_any'] = 'Stenge alle emner';
$txt['board_perms_name_lock_own'] = 'Stenge egne emner';
$txt['board_perms_name_make_sticky'] = 'Gjøre emner prioriterte';
$txt['board_perms_name_mark_any_notify'] = 'Be om varsling på alle emner';
$txt['board_perms_name_mark_notify'] = 'Be om varsling på egne emner';
$txt['board_perms_name_merge_any'] = 'Slå sammen emner';
$txt['board_perms_name_moderate_board'] = 'Forum-moderator';
$txt['board_perms_name_modify_any'] = 'Redigere alle innlegg';
$txt['board_perms_name_modify_own'] = 'Redigere egne innlegg';
$txt['board_perms_name_modify_replies'] = 'Redigere svar til egne emner';
$txt['board_perms_name_move_any'] = 'Flytte alle emner';
$txt['board_perms_name_move_own'] = 'Flytte egne emner';
$txt['board_perms_name_poll_add_any'] = 'Legge til avstemning til alle emner';
$txt['board_perms_name_poll_add_own'] = 'Legge til avstemning til egne emner';
$txt['board_perms_name_poll_edit_any'] = 'Redigere alle avstemninger';
$txt['board_perms_name_poll_edit_own'] = 'Redigere egne avstemninger';
$txt['board_perms_name_poll_lock_any'] = 'Stenge alle avstemninger';
$txt['board_perms_name_poll_lock_own'] = 'Stenge egne avstemninger';
$txt['board_perms_name_poll_post'] = 'Opprett nye avstemninger';
$txt['board_perms_name_poll_remove_any'] = 'Fjerne alle avstemninger';
$txt['board_perms_name_poll_remove_own'] = 'Fjerne egne avstemninger';
$txt['board_perms_name_poll_view'] = 'Vise avstemninger';
$txt['board_perms_name_poll_vote'] = 'Avgi stemme i avstemninger';
$txt['board_perms_name_post_attachment'] = 'Bruke vedlegg';
$txt['board_perms_name_post_new'] = 'Opprett nye emner';
$txt['board_perms_name_post_reply_any'] = 'Svare i alle emner';
$txt['board_perms_name_post_reply_own'] = 'Svare i egne emner';
$txt['board_perms_name_post_unapproved_attachments'] = 'Sette inn ikke-godkjente vedlegg';
$txt['board_perms_name_post_unapproved_topics'] = 'Sette inn ikke-godkjente emner';
$txt['board_perms_name_post_unapproved_replies_any'] = 'Sette inn ikke-godkjente svar på alle emne';
$txt['board_perms_name_post_unapproved_replies_own'] = 'Sette inn ikke-godkjente svar i eget emne';
$txt['board_perms_name_remove_any'] = 'Slette alle emner';
$txt['board_perms_name_remove_own'] = 'Slette egne emner';
$txt['board_perms_name_report_any'] = 'Rappportere alle innlegg';
$txt['board_perms_name_send_topic'] = 'Tipse emner til venner';
$txt['board_perms_name_split_any'] = 'Dele opp alle emner';
$txt['board_perms_name_view_attachments'] = 'Vise vedlegg';

$txt['board_perms_group_no_polls'] = 'Dette forumet tillater ikke avstemninger';
$txt['board_perms_group_reply_only'] = 'Dette forumet tillater kun svar på emner';
$txt['board_perms_group_read_only'] = 'Dette forumet er kun lesbart';

// Membergroup info!
$txt['member_group_color'] = 'Farge';
$txt['member_group_min_posts'] = 'Minimum antall innlegg';
$txt['member_group_max_messages'] = 'Maksimum antall PM\'er';
$txt['member_group_stars'] = 'Stjerner';
$txt['member_group_settings'] = 'Innstillinger';
$txt['member_group_access'] = 'Forumtilgang';

// Board info.
$txt['none'] = 'Ingen';
$txt['board_category'] = 'Kategori';
$txt['board_parent'] = 'Hovedforum';
$txt['board_num_topics'] = 'Antall emner';
$txt['board_num_posts'] = 'Antall innlegg';
$txt['board_count_posts'] = 'Tell innlegg';
$txt['board_theme'] = 'Forumdesign';
$txt['board_override_theme'] = 'Overstyr forumdesign';
$txt['board_profile'] = 'Profil tillatelser';
$txt['board_moderators'] = 'Moderatorer';
$txt['board_groups'] = 'Grupper med tilgang';

// Group Permissions.
$txt['group_perms_name_access_mod_center'] = 'Adgang til moderasjonssenter';
$txt['group_perms_name_admin_forum'] = 'Administrer forum';
$txt['group_perms_name_calendar_edit_any'] = 'Endre alle hendelser';
$txt['group_perms_name_calendar_edit_own'] = 'Endre egne hendelser';
$txt['group_perms_name_calendar_post'] = 'Opprette nye hendelser';
$txt['group_perms_name_calendar_view'] = 'Vise hendelser';
$txt['group_perms_name_edit_news'] = 'Endre forumnyheter';
$txt['group_perms_name_issue_warning'] = 'Sende ut advarsler';
$txt['group_perms_name_karma_edit'] = 'Endre medlemskarma';
$txt['group_perms_name_manage_attachments'] = 'Behandle vedlegg';
$txt['group_perms_name_manage_bans'] = 'Behandle utestengelser';
$txt['group_perms_name_manage_boards'] = 'Behandle fora';
$txt['group_perms_name_manage_membergroups'] = 'Behandle medlemsgrupper';
$txt['group_perms_name_manage_permissions'] = 'Behandle rettigheter';
$txt['group_perms_name_manage_smileys'] = 'Behandle smilefjes og innleggs-symboler';
$txt['group_perms_name_moderate_forum'] = 'Forum-moderator';
$txt['group_perms_name_pm_read'] = 'Lese PM\'er';
$txt['group_perms_name_pm_send'] = 'Sende PM\'er';
$txt['group_perms_name_profile_extra_any'] = 'Endre ekstra innstillinger';
$txt['group_perms_name_profile_extra_own'] = 'Endre egne ekstra innstillinger';
$txt['group_perms_name_profile_identity_any'] = 'Endre kontoinnstillinger';
$txt['group_perms_name_profile_identity_own'] = 'Endre egne kontoinnstillinger';
$txt['group_perms_name_profile_server_avatar'] = 'Velge en lokal avatar';
$txt['group_perms_name_profile_upload_avatar'] = 'Laste opp en avatar';
$txt['group_perms_name_profile_remote_avatar'] = 'Velge en ekstern avatar';
$txt['group_perms_name_profile_remove_any'] = 'Slette en konto';
$txt['group_perms_name_profile_remove_own'] = 'Slette egen konto';
$txt['group_perms_name_profile_title_any'] = 'Endre egendefinert tittel';
$txt['group_perms_name_profile_title_own'] = 'Endre egen egendefinert tittel';
$txt['group_perms_name_profile_view_any'] = 'Vise alle profiler';
$txt['group_perms_name_profile_view_own'] = 'Vise egen profil';
$txt['group_perms_name_search_posts'] = 'Søke etter innlegg';
$txt['group_perms_name_send_mail'] = 'Sende en e-post til medlemmer';
$txt['group_perms_name_view_mlist'] = 'Vise medlemslista';
$txt['group_perms_name_view_stats'] = 'Vise forumstatistikk';
$txt['group_perms_name_who_view'] = 'Se hvem som er pålogget';

$txt['report_error_too_many_staff'] = 'Du har for mange ansatte, rapporteringen vil ikke fungere med flere enn 300 av dine medlemmer som ansatte!';
$txt['report_staff_position'] = 'Stilling';
$txt['report_staff_moderates'] = 'Moderator for';
$txt['report_staff_posts'] = 'Innlegg';
$txt['report_staff_last_login'] = 'Sist innlogget';
$txt['report_staff_all_boards'] = 'Alle fora';
$txt['report_staff_no_boards'] = 'Ingen fora';

?>