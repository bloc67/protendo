<?php
// Version: 2.0; ModerationCenter

global $scripturl;

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['moderation_center'] = 'Moderatorsenter';
$txt['mc_main'] = 'Hoved';
$txt['mc_posts'] = 'Innlegg';
$txt['mc_groups'] = 'Grupper';

$txt['mc_view_groups'] = 'Vis grupper';

$txt['mc_description'] = 'Dette er ditt &quot;Moderator senter&quot;. Herfra kan du utføre alle moderator handlingene tildelt deg fra Administrator. Denne siden inneholder en oppsummering av alle de siste hendelsene i Forumet. Du kan tilpasse oppsettet ved å klikke <a href="' . $scripturl . '?action=moderate;area=settings">her</a>.';
$txt['mc_group_requests'] = 'Forespørsler om gruppemedlemskap';
$txt['mc_unapproved_posts'] = 'Ikke godkjente innlegg';
$txt['mc_watched_users'] = 'Medlemmer som nylig er blitt holdt øye med';
$txt['mc_watched_topics'] = 'Emner som det holdes øye med';
$txt['mc_scratch_board'] = 'Moderators arbeidspanel';
$txt['mc_latest_news'] = 'Siste nytt fra Simple Machines...';
$txt['mc_recent_reports'] = 'Nylige rapporterte emner';
$txt['mc_warnings'] = 'Advarsler';
$txt['mc_notes'] = 'Moderator merknader';

$txt['mc_cannot_connect_sm'] = 'Du kan ikke koble deg til simplemachines.org for siste nyheter.';

$txt['mc_recent_reports_none'] = 'Det er ingen utestående rapporterte';
$txt['mc_watched_users_none'] = 'Det er for øyeblikket ingen det holdes øye med.';
$txt['mc_group_requests_none'] = 'Det er ingen åpne forespørsler om gruppemedlemskap.';

$txt['mc_seen'] = '%1$s sist sett %2$s';
$txt['mc_seen_never'] = '%1$s aldri sett';
$txt['mc_groupr_by'] = 'av';

$txt['mc_reported_posts_desc'] = 'Her kan du vurdere alle innlegg rapportert av brukerne.';
$txt['mc_reportedp_active'] = 'Aktive rapporterte';
$txt['mc_reportedp_closed'] = 'Gamle rapporterte';
$txt['mc_reportedp_by'] = 'av';
$txt['mc_reportedp_reported_by'] = 'Rapportert av';
$txt['mc_reportedp_last_reported'] = 'Siste rapporterte';
$txt['mc_reportedp_none_found'] = 'Ingen rapporterte funnet';

$txt['mc_reportedp_details'] = 'Detaljer';
$txt['mc_reportedp_close'] = 'Lukk';
$txt['mc_reportedp_open'] = 'Åpne';
$txt['mc_reportedp_ignore'] = 'Ignorér';
$txt['mc_reportedp_unignore'] = 'Ikke ignorér';
// Do not use numeric entries in the below string.
$txt['mc_reportedp_ignore_confirm'] = 'Er du sikker på at du ønsker å ignorere ytterligere rapportert om denne meldingen?\\n\\nDette vil slå av videre rapporteringer for alle moderatorer på forumet.';
$txt['mc_reportedp_close_selected'] = 'Lukk valgte';

$txt['mc_groupr_group'] = 'Medlemsgrupper';
$txt['mc_groupr_member'] = 'Medlem';
$txt['mc_groupr_reason'] = 'Årsak';
$txt['mc_groupr_none_found'] = 'Det er for tiden ingen forespørsler om medlemskap i grupper.';
$txt['mc_groupr_submit'] = 'Send';
$txt['mc_groupr_reason_desc'] = 'Årsak til å avvise %1$s\'s anmodning om å bli med i &quot;%2$s&quot;';
$txt['mc_groups_reason_title'] = 'Grunner for avvisning';
$txt['with_selected'] = 'Med valgte';
$txt['mc_groupr_approve'] = 'Godkjenne forespørsel';
$txt['mc_groupr_reject'] = 'Avvis forespørsel (Ingen årsak)';
$txt['mc_groupr_reject_w_reason'] = 'Avvis forespørsel med begrunnelse';
// Do not use numeric entries in the below string.
$txt['mc_groupr_warning'] = 'Er du sikker på at du ønsker å gjøre dette?';

$txt['mc_unapproved_attachments_none_found'] = 'Det er ingen vedlegg som venter på godkjenning';
$txt['mc_unapproved_replies_none_found'] = 'Det er for tiden ingen innlegg som venter på godkjenning';
$txt['mc_unapproved_topics_none_found'] = 'Det er for tiden ingen emner som avventer godkjenning';
$txt['mc_unapproved_posts_desc'] = 'Herfra kan du godkjenne eller slette innlegg som venter på moderering.';
$txt['mc_unapproved_replies'] = 'Svar';
$txt['mc_unapproved_topics'] = 'Emner';
$txt['mc_unapproved_by'] = 'av';
$txt['mc_unapproved_sure'] = 'Er du sikker på at du vil gjøre dette?';
$txt['mc_unapproved_attach_name'] = 'Tittel';
$txt['mc_unapproved_attach_size'] = 'Filstørrelse';
$txt['mc_unapproved_attach_poster'] = 'Avsender';
$txt['mc_viewmodreport'] = 'Moderatorrapport for %1$s av %2$s';
$txt['mc_modreport_summary'] = 'Det har vært %1$d rapport(er) om dette innlegget.  Den siste rapporten var %2$s.';
$txt['mc_modreport_whoreported_title'] = 'Medlemmer som har rapportert dette innlegget';
$txt['mc_modreport_whoreported_data'] = 'Rapportert av %1$s på %2$s.  Med følgende melding:';
$txt['mc_modreport_modactions'] = 'Handlinger utført av andre moderatorer';
$txt['mc_modreport_mod_comments'] = 'Moderatorkommentarer';
$txt['mc_modreport_no_mod_comment'] = 'Det er for øyeblikket ingen Moderatorkommentarer';
$txt['mc_modreport_add_mod_comment'] = 'Legg til kommentar';

$txt['show_notice'] = 'Kunngjøringstekst';
$txt['show_notice_subject'] = 'Tittel';
$txt['show_notice_text'] = 'Tekst';

$txt['mc_watched_users_title'] = 'Overvåket medlemmer';
$txt['mc_watched_users_desc'] = 'Her kan du holde styr på alle medlemmer som det &quot;holde øye med&quot; av moderatorene.';
$txt['mc_watched_users_post'] = 'Vis etter innlegg';
$txt['mc_watched_users_warning'] = 'Advarselsnivå';
$txt['mc_watched_users_last_login'] = 'Sist innlogget';
$txt['mc_watched_users_last_post'] = 'Siste innlegg';
$txt['mc_watched_users_no_posts'] = 'Det finnes ingen innlegg fra &quot;overvåkete&quot; medlemmer.';
// Don't use entities in the two strings below.
$txt['mc_watched_users_delete_post'] = 'Er du sikker på at du vil slette dette innlegget?';
$txt['mc_watched_users_delete_posts'] = 'Er du sikker på at du vil slette disse innleggene?';
$txt['mc_watched_users_posted'] = 'Skrevet';
$txt['mc_watched_users_member'] = 'Medlem';

$txt['mc_warnings_description'] = 'Fra denne seksjonen kan du se hvilke advarsler som har blitt utstedt til medlemmer av forumet. Du kan også legge til og endre varslingsmaler som brukes når du sender en advarsel til et medlem.';
$txt['mc_warning_log'] = 'Logg';
$txt['mc_warning_templates'] = 'Egendefinerte maler';
$txt['mc_warning_log_title'] = 'Viser advarselslogg';
$txt['mc_warning_templates_title'] = 'Egendefinerte advarselsmaler';

$txt['mc_warnings_none'] = 'Ingen advarsler har blitt utstedt ennå!';
$txt['mc_warnings_recipient'] = 'Mottaker';

$txt['mc_warning_templates_none'] = 'Ingen advarselsmaler er opprettet ennå';
$txt['mc_warning_templates_time'] = 'Tid opprettet';
$txt['mc_warning_templates_name'] = 'Mal';
$txt['mc_warning_templates_creator'] = 'Opprettet av';
$txt['mc_warning_template_add'] = 'Legg til mal';
$txt['mc_warning_template_modify'] = 'Rediger mal';
$txt['mc_warning_template_delete'] = 'Slett valgte';
$txt['mc_warning_template_delete_confirm'] = 'Er du sikker på at du vil slette de valgte malene?';

$txt['mc_warning_template_desc'] = 'Bruk denne siden til å fylle ut detaljene i malen. Merk at emnet for e-posten er ikke en del av malen. Merk: Meldingen sendes som PM og da kan du bruke BB koder i malen. Vær oppmerksom på at hvis du bruker {MESSAGE} variabelen, da vil ikke denne malen være tilgjengelig ved utstedelse av et generisk advarsel (dvs. en advarsel om ikke er knyttes til et innlegg).';
$txt['mc_warning_template_title'] = 'Tittel';
$txt['mc_warning_template_body_desc'] = 'Innholdet i meldingen . Merk at du kan bruke følgende snarveier i denne malen.<ul style="margin-top: 0px;"><li>{MEMBER} - Medlemsnavn.</li><li>{MESSAGE} - Lenke til det uakseptable innlegget. (hvis aktuelt)</li><li>{FORUMNAME} - Navn på Forumet.</li><li>{SCRIPTURL} - Nettadressen til forumet.</li><li>{REGARDS} - Standard  e-postsignatur.</li></ul>';
$txt['mc_warning_template_body_default'] = '{MEMBER},' . "\n\n" . 'Du har mottatt en advarsel for upassende aktivitet. Vennligst opphør disse aktivitetene og rette deg etter forumreglene ellers vil vi ta ytterligere tiltak.' . "\n\n" . '{REGARDS}';
$txt['mc_warning_template_personal'] = 'Personlig mal';
$txt['mc_warning_template_personal_desc'] = 'Hvis du velger dette alternativet vil bare du kunne se, redigere og bruke denne malen. Hvis ikke vil alle moderatorene kunne bruke denne malen.';
$txt['mc_warning_template_error_empty'] = 'Du må angi både tittel og varslingsmelding.';

$txt['mc_prefs'] = 'Innstillinger';
$txt['mc_settings'] = 'Endre innstillinger';
$txt['mc_prefs_title'] = 'Modereringsinnstillinger';
$txt['mc_prefs_desc'] = 'Denne delen lar deg sette noen personlige innstillinger på moderator relaterte aktiviteter, som for eksempel e-postmeldinger.';
$txt['mc_prefs_homepage'] = 'Elementer som skal vises på moderatorstartsiden';
$txt['mc_prefs_latest_news'] = 'SM Nyheter';
$txt['mc_prefs_show_reports'] = 'Vis antall åpne rapporterte i forumets topptekst';
$txt['mc_prefs_notify_report'] = 'Varsle om rapporterte emner';
$txt['mc_prefs_notify_report_never'] = 'Aldri';
$txt['mc_prefs_notify_report_moderator'] = 'Bare hvis det er et forum hvor jeg er moderator';
$txt['mc_prefs_notify_report_always'] = 'Alltid';
$txt['mc_prefs_notify_approval'] = 'Varsle av elementer som venter på godkjenning';

// Use entities in the below string.
$txt['mc_click_add_note'] = 'Legg til et nytt notat';
$txt['mc_add_note'] = 'Legg til';

?>