<?php
// Version: 2.0; Search

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['set_parameters'] = 'Still inn søkeparametere';
$txt['choose_board'] = 'Velg en seksjon du vil søke i, eller velg alle';
$txt['all_words'] = 'Treff på alle ordene';
$txt['any_words'] = 'Treff på ett eller flere av ordene';
$txt['by_user'] = 'Etter medlem';

$txt['search_post_age'] = 'Alder på innlegg';
$txt['search_between'] = 'mellom';
$txt['search_and'] = 'og';
$txt['search_options'] = 'Alternativer';
$txt['search_show_complete_messages'] = 'Vis resultater som innlegg';
$txt['search_subject_only'] = 'Søk kun i emnetitler';
$txt['search_relevance'] = 'Relevanse';
$txt['search_date_posted'] = 'Dato';
$txt['search_order'] = 'Sorter søk';
$txt['search_orderby_relevant_first'] = 'Mest relevante treff først';
$txt['search_orderby_large_first'] = 'Største emner først';
$txt['search_orderby_small_first'] = 'Minste emner først';
$txt['search_orderby_recent_first'] = 'Nyeste emner først';
$txt['search_orderby_old_first'] = 'Eldste emner først';
$txt['search_visual_verification_label'] = 'Bekreftelse';
$txt['search_visual_verification_desc'] = 'Vennligst skriv inn koden i bildet ovenfor for å bruke søk.';

$txt['search_specific_topic'] = 'Søk etter innlegg kun i emnet';

$txt['mods_cat_search'] = 'Søk';
$txt['groups_search_posts'] = 'Medlemsgrupper med tilgang til søkefunksjonen';
$txt['simpleSearch'] = 'Aktiver enkle søk';
$txt['search_results_per_page'] = 'Antall søkeresultater per side';
$txt['search_weight_frequency'] = 'Relativ vektlegging av søk for antall innlegg med treff innen ett emne';
$txt['search_weight_age'] = 'Relativ vektlegging av søk for alder på siste innlegg med treff';
$txt['search_weight_length'] = 'Relativ vektlegging av søk for emne-lengde';
$txt['search_weight_subject'] = 'Relativ vektlegging av søk for treff i tittel';
$txt['search_weight_first_message'] = 'Relativ vektlegging av søk for treff i første innlegg';
$txt['search_weight_sticky'] = 'Relativ vektlegging av søk for treff i prioriterte emner';

$txt['search_settings_desc'] = 'Her kan du endre basisinnstillinger for søkefunksjonen.';
$txt['search_settings_title'] = 'Søkefunksjon - innstillinger';

$txt['search_weights_desc'] = 'Her kan du endre de individuelle delene av relevans-graderingen.';
$txt['search_weights_title'] = 'Søk - vektlegging';
$txt['search_weights_total'] = 'Totalt';
$txt['search_weights_save'] = 'Lagre';

$txt['search_method_desc'] = 'Her kan du velge med metode søket skal utføres.';
$txt['search_method_title'] = 'Søk - metode';
$txt['search_method_save'] = 'Lagre';
$txt['search_method_messages_table_space'] = 'Plass brukt av forummeldinger i databasen';
$txt['search_method_messages_index_space'] = 'Plass brukt for å indeksere meldinger i databasen';
$txt['search_method_kilobytes'] = 'KB';
$txt['search_method_fulltext_index'] = 'Fulltekstsøk';
$txt['search_method_no_index_exists'] = 'eksisterer ikke';
$txt['search_method_fulltext_create'] = 'opprette en oversikt over fulltekstsøk';
$txt['search_method_fulltext_cannot_create'] = 'kunne ikke opprette fordi maksimallengde er på mer enn 65 535 eller tabelltypen er ikke MyISAM';
$txt['search_method_index_already_exists'] = 'er allerede opprettet';
$txt['search_method_fulltext_remove'] = 'fjerne fulltekstsøk';
$txt['search_method_index_partial'] = 'delvis opprettet';
$txt['search_index_custom_resume'] = 'fortsett';
// This string is used in a javascript confirmation popup; don't use entities.
$txt['search_method_fulltext_warning'] = 'For å kunne bruke fulltekstsøk, må du opprette en oversikt først!';

$txt['search_index'] = 'Søkeoversikt';
$txt['search_index_none'] = 'Ingen oversikt';
$txt['search_index_custom'] = 'Egendefinert oversikt';
$txt['search_index_sphinx'] = 'Sphinx';
$txt['search_index_sphinx_desc'] = 'Administratorpanelet tillater bare å veksle mellom søkeindekser. For ytterligere å justere Sphinx innstillinger, bruker sphinx_config.php verktøyet.';
$txt['search_index_label'] = 'Oversikt';
$txt['search_index_size'] = 'Størrelse';
$txt['search_index_create_custom'] = 'opprette egendefinert oversikt';
$txt['search_index_custom_remove'] = 'fjerne egendefinert oversikt';
// This string is used in a javascript confirmation popup; don't use entities.
$txt['search_index_custom_warning'] = 'For å kunne bruke egendefinert oversikt, må du opprette en slik oversikt først!';

$txt['search_force_index'] = 'Framtving bruk av søkeoversikt';
$txt['search_match_words'] = 'Treff bare hele ord';
$txt['search_max_results'] = 'Maksimalt antall resultater som skal vises';
$txt['search_max_results_disable'] = '(0: ingen grense)';
$txt['search_floodcontrol_time'] = 'Tid imellom hver gang en bruker kan søke';
$txt['search_floodcontrol_time_desc'] = '(0: ingen grense, i sekunder)';

$txt['search_create_index'] = 'Opprett oversikt';
$txt['search_create_index_why'] = 'Hvorfor opprette en oversikt?';
$txt['search_create_index_start'] = 'Opprett';
$txt['search_predefined'] = 'Forhåndsdefinert profil';
$txt['search_predefined_small'] = 'Lite plasskrevende oversikt';
$txt['search_predefined_moderate'] = 'Middels plasskrevende oversikt';
$txt['search_predefined_large'] = 'Svært plasskrevdene oversikt';
$txt['search_create_index_continue'] = 'Fortsett';
$txt['search_create_index_not_ready'] = 'Protendo oppretter nå en søkeoversikt over dine innlegg. For å unngå overbelastning på din server, har denne prosessen blitt midlertidig stanset. Men den kommer til å fortsette om bare noen få sekunder. Dersom ingenting skjer, klikk på fortsett under.';
$txt['search_create_index_progress'] = 'Fremdrift';
$txt['search_create_index_done'] = 'Egendefinert søkeoversikt opprettet!';
$txt['search_create_index_done_link'] = 'Fortsett';
$txt['search_double_index'] = 'Du har opprettet to oversikter i innleggstabellen. For best ytelse er det anbefalt at du sletter en av de oversiktene.';

$txt['search_error_indexed_chars'] = 'Ugyldig antall tegn. Minst 3 tegn er nødvendig for en nyttig indeks.';
$txt['search_error_max_percentage'] = 'Ugyldig antall prosent av ord som skal bli oversett. Bruk en verdi på minimum 5%.';
$txt['error_string_too_long'] = 'Søkestreng må være mindre enn %1$d tegn.';

$txt['search_adjust_query'] = 'Justér søkeparametre';
$txt['search_adjust_submit'] = 'Revider søk';
$txt['search_did_you_mean'] = 'Du ment kanskje å søke etter';

$txt['search_example'] = '<em>F.eks</em> Tolkien "Ringenes Herre" -film';

$txt['search_engines_description'] = 'Fra dette området kan du bestemme hvor detaljert du ønsker å spore søkemotorer når de indeksere forumet samt gjennomgang av søkemotorlogger.';
$txt['spider_mode'] = 'Sporingsnivå for søkemotorer<div class="smalltext">Merk: Jo høyere nivå på sporingen, jo mer ressurskrevende for serveren.</div>';
$txt['spider_mode_off'] = 'Deaktivert';
$txt['spider_mode_standard'] = 'Standard - Logger minimal indekseringsrobotaktivitet.';
$txt['spider_mode_high'] = 'Høy - Gir mer nøyaktig statistikk.';
$txt['spider_mode_vhigh'] = 'Svært høy – Samme som for &quot;Høy&quot; men logger også data om hver side som blir besøkt.';
$txt['spider_settings_desc'] = 'Du kan endre innstillinger for sporing fra denne siden. Merk, hvis du ønsker å aktivere automatisk sletting  av treff logger du kan sette dette opp <a href="%1$s">her</a>';

$txt['spider_group'] = 'Bruk restriktive rettigheter fra gruppen<div class="smalltext">Gjør det mulig å stoppe indeksering på noen av sidene.</div>';
$txt['spider_group_none'] = 'Deaktivert';

$txt['show_spider_online'] = 'Vis indekseringsroboter i listen over brukere som er pålogget';
$txt['show_spider_online_no'] = 'Overhodet ikke';
$txt['show_spider_online_summary'] = 'Vis antall indekseringsroboter';
$txt['show_spider_online_detail'] = 'Vis navn på indekseringsrobotene';
$txt['show_spider_online_detail_admin'] = 'Vis navn på indekseringsrobotene - kun admin';

$txt['spider_name'] = 'Navn';
$txt['spider_last_seen'] = 'Sist sett';
$txt['spider_last_never'] = 'Aldri';
$txt['spider_agent'] = 'Brukeragent';
$txt['spider_ip_info'] = 'IP adresser';
$txt['spiders_add'] = 'Legg til ny robot';
$txt['spiders_edit'] = 'Rediger robot';
$txt['spiders_remove_selected'] = 'Slett valgte roboter';
$txt['spider_remove_selected_confirm'] = 'Er du sikker på at du ønsker å fjerne disse robotene?\\n\\nAll tilhørende statistikk vil også bli slettet!';
$txt['spiders_no_entries'] = 'Det er foreløpig ingen konfigurert roboter.';

$txt['add_spider_desc'] = 'Fra denne siden kan du redigere parametrene for hvordan indekseringsrobot blir kategorisert. Hvis en gjests brukeragent/IP-adresse matcher det som er under, vil den bli oppdaget som en søkemotorrobot og spores etter forumet innstillinger.';
$txt['spider_name_desc'] = 'Navn som roboten vil bli referert som.';
$txt['spider_agent_desc'] = 'Brukeragent knyttet til denne roboten.';
$txt['spider_ip_info_desc'] = 'Kommaseparert liste med IP-adresser knyttet til denne roboten.';

$txt['spider'] = 'Robot';
$txt['spider_time'] = 'Tid';
$txt['spider_viewing'] = 'Leser';
$txt['spider_logs_empty'] = 'Det er foreløpig ingen loggoppføringer på indekseringsroboter.';
$txt['spider_logs_info'] = 'Merk at logging av hver eneste robothandling bare utføres hvis sporing er satt til enten &quot;Høy&quot; eller &quot;Svært høy&quot;. Detaljer for hver robothandling blir bare logget hvis sporing er satt til &quot;Svært høy&quot;.';
$txt['spider_disabled'] = 'Deaktivert';

$txt['spider_logs_delete'] = 'Slett oppføringer';
$txt['spider_logs_delete_older'] = 'Slett alle oppføringer eldre enn';
$txt['spider_logs_delete_day'] = 'dager.';
$txt['spider_logs_delete_submit'] = 'Slett';
// Don't use entities in the below string.
$txt['spider_logs_delete_confirm'] = 'Er du sikker på at du ønsker å slette alle loggoppføringene?';

$txt['spider_stats_select_month'] = 'Gå til måned';
$txt['spider_stats_page_hits'] = 'Side treff';
$txt['spider_stats_no_entries'] = 'Det er ingen statistikk over roboter tilgjengelig.';

?>