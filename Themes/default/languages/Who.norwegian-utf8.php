<?php
// Version: 2.0; Who

global $scripturl, $context;

$txt['who_hidden'] = '<em>Ingenting, eller noe du ikke kan se...</em>';
$txt['who_unknown'] = '<em>Ukjent handling</em>';
$txt['who_user'] = 'Bruker';
$txt['who_time'] = 'Tid';
$txt['who_action'] = 'Handling';
$txt['who_show1'] = 'Vis ';
$txt['who_show_members_only'] = 'Kun medlemmer';
$txt['who_show_guests_only'] = 'Kun gjester';
$txt['who_show_spiders_only'] = 'Kun indekseringsrobot';
$txt['who_show_all'] = 'Alle';
$txt['who_no_online_spiders'] = 'Det er ingen indekseringsrobot på siden.';
$txt['who_no_online_guests'] = 'Det er ingen gjester på siden.';
$txt['who_no_online_members'] = 'Det er ingen brukere på siden.';

$txt['whospider_login'] = 'Leser innloggingssiden.';
$txt['whospider_register'] = 'Leser registreringssiden.';
$txt['whospider_reminder'] = 'Leser påminnelsessiden.';

$txt['whoall_activate'] = 'Aktiverer medlemskontoen sin.';
$txt['whoall_buddy'] = 'Endrer vennelisten sin.';
$txt['whoall_coppa'] = 'Fylle ut foreldre/foresatte skjema.';
$txt['whoall_credits'] = 'Leser side for medvirkende.';
$txt['whoall_emailuser'] = 'Sender e-post til et annet medlem.';
$txt['whoall_groups'] = 'Viser medlemsgruppesiden.';
$txt['whoall_help'] = 'Leser <a href="' . $scripturl . '?action=help">hjelpesiden</a>.';
$txt['whoall_helpadmin'] = 'Leser en hjelpe-popup.';
$txt['whoall_pm'] = 'Sjekker sine private meldinger.';
$txt['whoall_login'] = 'Logger inn på forumet.';
$txt['whoall_login2'] = 'Logger inn på forumet.';
$txt['whoall_logout'] = 'Logger ut fra forumet.';
$txt['whoall_markasread'] = 'Markerer emner som lest.';
$txt['whoall_modifykarma_applaud'] = 'Gir ett medlem positiv karma.';
$txt['whoall_modifykarma_smite'] = 'Gir ett medlem negativ karma.';
$txt['whoall_news'] = 'Leser nyhetene.';
$txt['whoall_notify'] = 'Redigerer sine varslingsinnstillinger.';
$txt['whoall_notifyboard'] = 'Redigerer sine varslingsinnstillinger.';
$txt['whoall_openidreturn'] = 'Logger inn ved bruk av OpenID.';
$txt['whoall_quickmod'] = 'Modeerer ett Forum.';
$txt['whoall_recent'] = 'Viser en <a href="' . $scripturl . '?action=recent">liste over nyeste emner</a>.';
$txt['whoall_register'] = 'Registrerer seg på forumet.';
$txt['whoall_register2'] = 'Registrerer seg på forumet.';
$txt['whoall_reminder'] = 'Ber om en passordpåminnelse.';
$txt['whoall_reporttm'] = 'Rapporterer et emne til en moderator.';
$txt['whoall_spellcheck'] = 'Bruker stavekontrollen';
$txt['whoall_unread'] = 'Viser uleste emner siden siste besøk.';
$txt['whoall_unreadreplies'] = 'Viser uleste svar siden siste besøk.';
$txt['whoall_who'] = 'Viser siden <a href="' . $scripturl . '?action=who">Hvem er pålogget</a>.';

$txt['whoall_collapse_collapse'] = 'Sammenfolde en kategori.';
$txt['whoall_collapse_expand'] = 'Utvider en kategori.';
$txt['whoall_pm_removeall'] = 'Sletter alle sine private meldinger.';
$txt['whoall_pm_send'] = 'Sender en PM.';
$txt['whoall_pm_send2'] = 'Sender en PM.';

$txt['whotopic_announce'] = 'Kunngjøre emnet &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_attachapprove'] = 'Godkjenne et vedlegg.';
$txt['whotopic_dlattach'] = 'Viser et vedlegg.';
$txt['whotopic_deletemsg'] = 'Sletter en melding.';
$txt['whotopic_editpoll'] = 'Redigerer avstemning i &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_editpoll2'] = 'Redigerer avstemning i &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_jsmodify'] = 'Endre et innlegg i &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_lock'] = 'Stenger emnet &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_lockvoting'] = 'Stenger avstemning i &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_mergetopics'] = 'Slår sammen emnet "<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>" med et annet emne.';
$txt['whotopic_movetopic'] = 'Flytter emnet "<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>" til en annen seksjon.';
$txt['whotopic_movetopic2'] = 'Flytter emnet &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot; til en annen seksjon.';
$txt['whotopic_post'] = 'Skriver et innlegg i <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';
$txt['whotopic_post2'] = 'Skriver et innlegg i <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';
$txt['whotopic_printpage'] = 'Skriver ut emnet "<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>".';
$txt['whotopic_quickmod2'] = 'Modererer emnet <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';
$txt['whotopic_removepoll'] = 'Fjerner avstemning fra  &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot;.';
$txt['whotopic_removetopic2'] = 'Fjerner emne fra <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';
$txt['whotopic_sendtopic'] = 'Sender emnet &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot; til en venn.';
$txt['whotopic_splittopics'] = 'Splitter emnet &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot; i to emner.';
$txt['whotopic_sticky'] = 'Setter emnet som &quot;<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>&quot; prioritert.';
$txt['whotopic_vote'] = 'Avgir stemme i <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';

$txt['whopost_quotefast'] = 'Siterer et innlegg fra "<a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>".';

$txt['whoadmin_editagreement'] = 'Redigerer registreringsavtalen.';
$txt['whoadmin_featuresettings'] = 'Endrer forumets innstillinger og alternativer.';
$txt['whoadmin_modlog'] = 'Leser moderatorloggen.';
$txt['whoadmin_serversettings'] = 'Endrer forumets serverinnstillinger.';
$txt['whoadmin_packageget'] = 'Henter pakker.';
$txt['whoadmin_packages'] = 'Viser pakkebehandleren.';
$txt['whoadmin_permissions'] = 'Redigerer forumets rettigheter.';
$txt['whoadmin_pgdownload'] = 'Laster ned en pakke.';
$txt['whoadmin_theme'] = 'Redigerer designinnstillingene.';
$txt['whoadmin_trackip'] = 'Sporer en IP-adresse.';

$txt['whoallow_manageboards'] = 'Redigerer innstillingene for forum og kategorier.';
$txt['whoallow_admin'] = 'Er inne på <a href="' . $scripturl . '?action=admin">administrator-siden</a>.';
$txt['whoallow_ban'] = 'Redigerer lista over utestengte.';
$txt['whoallow_boardrecount'] = 'Teller på nytt alle forumets totaler.';
$txt['whoallow_calendar'] = 'Viser <a href="' . $scripturl . '?action=calendar">kalenderen</a>.';
$txt['whoallow_editnews'] = 'Redigerer nyhetene.';
$txt['whoallow_mailing'] = 'Sender en e-post fra forumet.';
$txt['whoallow_maintain'] = 'Utfører rutinemessig vedlikehold på forumet.';
$txt['whoallow_manageattachments'] = 'Behandler vedleggene.';
$txt['whoallow_moderate'] = 'Viser <a href="' . $scripturl . '?action=moderate">Moderatorsenteret</a>.';
$txt['whoallow_mlist'] = 'Viser <a href="' . $scripturl . '?action=mlist">medlemslisten</a>.';
$txt['whoallow_optimizetables'] = 'Optimaliserer databasens tabeller.';
$txt['whoallow_repairboards'] = 'Reparerer databasetabellene.';
$txt['whoallow_search'] = '<a href="' . $scripturl . '?action=search">søker</a> på forumet.';
$txt['whoallow_search2'] = 'Viser resultater etter et søk.';
$txt['whoallow_setcensor'] = 'Redigerer sensurteksten.';
$txt['whoallow_setreserve'] = 'Redigerer de reserverte navnene.';
$txt['whoallow_stats'] = 'Viser <a href="' . $scripturl . '?action=stats">forumstatistikk</a>.';
$txt['whoallow_viewErrorLog'] = 'Leser forumets logg over feil.';
$txt['whoallow_viewmembers'] = 'Viser en liste over medlemmer.';

$txt['who_topic'] = 'Leser emnet <a href="' . $scripturl . '?topic=%1$d.0">%2$s</a>.';
$txt['who_board'] = 'Viser forumet <a href="' . $scripturl . '?board=%1$d.0">%2$s</a>.';
$txt['who_index'] = 'Viser hovedsiden til forumet <a href="' . $scripturl . '">' . $context['forum_name'] . '</a>.';
$txt['who_viewprofile'] = 'Viser profilen til <a href="' . $scripturl . '?action=profile;u=%1$d">%2$s</a>.';
$txt['who_profile'] = 'Redigerer profilen til <a href="' . $scripturl . '?action=profile;u=%1$d">%2$s</a>.';
$txt['who_post'] = 'Lager et nytt emne i <a href="' . $scripturl . '?board=%1$d.0">%2$s</a>.';
$txt['who_poll'] = 'Lager en ny avstemning i <a href="' . $scripturl . '?board=%1$d.0">%2$s</a>.';

// Credits text
$txt['credits'] = 'Medvirkende';
$txt['credits_intro'] = 'Simple Machines ønsker å takke alle som bidro til å gjøre Protendo 2.0 til hva det er i dag, forme og dirigere vårt prosjekt, gjennom tykt og den tynt. Det ville ikke ha vært mulig uten dere. Dette inkluderer våre brukere og spesielt våre Charter medlemmer - takk for at du installerer og bruker vår programvare samt gir verdifulle tilbakemeldinger, feilrapporter, og meninger.';
$txt['credits_team'] = 'Teamet';
$txt['credits_special'] = 'Spesiell takk';
$txt['credits_and'] = 'og';
$txt['credits_anyone'] = 'Og for de vi kanskje har glemt, takk!';
$txt['credits_copyright'] = 'Opphavsrett';
$txt['credits_forum'] = 'Forum';
$txt['credits_modifications'] = 'Modifikasjoner';
$txt['credits_groups_ps'] = 'Prosjektstøtte';
$txt['credits_groups_dev'] = 'Utviklere';
$txt['credits_groups_support'] = 'Spesialister';
$txt['credits_groups_customize'] = 'Tilpassere';
$txt['credits_groups_docs'] = 'Dokumentasjon';
$txt['credits_groups_marketing'] = 'Markedsføring';
$txt['credits_groups_internationalizers'] = 'Lokaliserere';
$txt['credits_groups_servers'] = 'Servere Administratorer';
$txt['credits_groups_site'] = 'Nettsted Administratorer';
// Replace "English" with the name of this language pack in the string below.
$txt['credits_groups_translation'] = 'Norsk oversettelse';
$txt['credits_groups_translators'] = 'Oversettere';
$txt['credits_translators_message'] = 'Takk for din innsats som gjør det mulig for mennesker over hele verden til å bruke Protendo.';
$txt['credits_groups_consultants'] = 'Utviklingskonsulenter';
$txt['credits_groups_beta'] = 'Betatestere';
$txt['credits_beta_message'] = 'De uvurderlig få som utrettelig finne feil, gir tilbakemelding, og gjør utviklerne sprøe.';
$txt['credits_groups_founder'] = 'Grunnleggerne av Protendo';
$txt['credits_groups_orignal_pm'] = 'Original prosjektledere';

// List of people who have made more than a token contribution to this translation. (blank for English)
$txt['translation_credits'] = array('Scraphaugen', 'Roar', 'Akyhne');

?>