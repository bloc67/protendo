<?php
// Version: 2.0; Modlog

global $scripturl;

$txt['modlog_date'] = 'Dato';
$txt['modlog_member'] = 'Medlem';
$txt['modlog_position'] = 'Posisjon';
$txt['modlog_action'] = 'Handling';
$txt['modlog_ip'] = 'IP';
$txt['modlog_search_result'] = 'Søkeresultater';
$txt['modlog_total_entries'] = 'Totalt antall innlegg';
$txt['modlog_ac_approve_topic'] = 'Godkjente emner &quot;{topic}&quot; av &quot;{member}&quot;';
$txt['modlog_ac_approve'] = 'Godkjente melding &quot;{subject}&quot; i &quot;{topic}&quot; av &quot;{member}&quot;';
$txt['modlog_ac_lock'] = 'Låst emne &quot;{topic}&quot;';
$txt['modlog_ac_warning'] = 'Advart medlem {member} på grunn av &quot;{message}&quot;';
$txt['modlog_ac_unlock'] = 'Opplåst emne &quot;{topic}&quot;';
$txt['modlog_ac_sticky'] = 'Prioritert emne &quot;{topic}&quot;';
$txt['modlog_ac_unsticky'] = 'Fjerne prioritert emne &quot;{topic}&quot;';
$txt['modlog_ac_delete'] = 'Slettet emne &quot;{subject}&quot; av &quot;{member}&quot; fra &quot;{topic}&quot;';
$txt['modlog_ac_delete_member'] = 'Slettet medlem &quot;{name}&quot;';
$txt['modlog_ac_remove'] = 'Fjernet emne &quot;{topic}&quot; fra &quot;{board}&quot;';
$txt['modlog_ac_modify'] = 'Redigert emne &quot;{message}&quot; av &quot;{member}&quot;';
$txt['modlog_ac_merge'] = 'Sammenslått emner å opprette &quot;{topic}&quot;';
$txt['modlog_ac_split'] = 'Del emnet &quot;{topic}&quot; for å opprette emnet &quot;{new_topic}&quot;';
$txt['modlog_ac_move'] = 'Flyttet emne &quot;{topic}&quot; fra &quot;{board_from}&quot; til &quot;{board_to}&quot;';
$txt['modlog_ac_profile'] = 'Redigert profil til &quot;{member}&quot;';
$txt['modlog_ac_pruned'] = 'Fjernet innlegg eldre enn {days} dager';
$txt['modlog_ac_news'] = 'Redigert nyhetene';
$txt['modlog_enter_comment'] = 'Skrevet moderatorkommentarer';
$txt['modlog_moderation_log'] = 'Moderatorlogg';
$txt['modlog_moderation_log_desc'] = 'Nedenfor er en liste over alle moderatorhandlingene som er blitt utført av moderatorer på forumet.<br /><strong>Vennligst merk:</strong> Handlinger kan ikke fjernes fra denne loggen før de er minst tjuefire timer gamle.';
$txt['modlog_no_entries_found'] = 'Det er for øyeblikket ikke logget noen moderatorhandlinger.';
$txt['modlog_remove'] = 'Slett';
$txt['modlog_removeall'] = 'Fjern alle';
$txt['modlog_go'] = 'Ok';
$txt['modlog_add'] = 'Legg til';
$txt['modlog_search'] = 'Hurtigsøk';
$txt['modlog_by'] = 'Av';
$txt['modlog_id'] = '<em>Slettet - ID:%1$d</em>';

$txt['modlog_ac_add_warn_template'] = 'Opprettet advarselsmal: &quot;{template}&quot;';
$txt['modlog_ac_modify_warn_template'] = 'Redigert advarselsmal: &quot;{template}&quot;';
$txt['modlog_ac_delete_warn_template'] = 'Slettet advarselsmal: &quot;{template}&quot;';

$txt['modlog_ac_ban'] = 'Opprettet forbud triggere:';
$txt['modlog_ac_ban_trigger_member'] = '<em>Medlem:</em> {member}';
$txt['modlog_ac_ban_trigger_email'] = '<em>E-postadresse:</em> {email}';
$txt['modlog_ac_ban_trigger_ip_range'] = '<em>IP:</em> {ip_range}';
$txt['modlog_ac_ban_trigger_hostname'] = '<em>Vertsnavn:</em> {hostname}';

$txt['modlog_admin_log'] = 'Administrasjonslogg';
$txt['modlog_admin_log_desc'] = 'Nedenfor er en liste av administratorhandlinger som er logget på forumet.<br /><strong>Vennligst merk:</strong> Handlinger kan ikke fjernes fra denne loggen før de er minst tjuefire timer gamle.';
$txt['modlog_admin_log_no_entries_found'] = 'Det er for øyeblikket ikke logget noen administratorhandlinger.';

// Admin type strings.
$txt['modlog_ac_upgrade'] = 'Oppgradert forumet til versjon {version}';
$txt['modlog_ac_install'] = 'Installert versjon {version}';
$txt['modlog_ac_add_board'] = 'Opprettet nytt forum: &quot;{board}&quot;';
$txt['modlog_ac_edit_board'] = 'Redigert forum &quot;{board}&quot;';
$txt['modlog_ac_delete_board'] = 'Redigerte forum &quot;{boardname}&quot;';
$txt['modlog_ac_add_cat'] = 'Lagt til en ny kategori, &quot;{catname}&quot;';
$txt['modlog_ac_edit_cat'] = 'Redigert kategori &quot;{catname}&quot;';
$txt['modlog_ac_delete_cat'] = 'Slettet kategori &quot;{catname}&quot;';

$txt['modlog_ac_delete_group'] = 'Slettet gruppe &quot;{group}&quot;';
$txt['modlog_ac_add_group'] = 'Lagt til ny gruppe &quot;{group}&quot;';
$txt['modlog_ac_edited_group'] = 'Redigert gruppe &quot;{group}&quot;';
$txt['modlog_ac_added_to_group'] = 'Lagt til bruker &quot;{member}&quot; til gruppe &quot;{group}&quot;';
$txt['modlog_ac_removed_from_group'] = 'Fjernet bruker &quot;{member}&quot; fra gruppe &quot;{group}&quot;';
$txt['modlog_ac_removed_all_groups'] = 'Fjernet bruker &quot;{member}&quot; fra alle grupper';

$txt['modlog_ac_remind_member'] = 'Sendt ut en påminnelse til bruker &quot;{member}&quot; for å aktivere kontoen sin';
$txt['modlog_ac_approve_member'] = 'Godkjent/Aktivert kontoen til bruker &quot;{member}&quot;';
$txt['modlog_ac_newsletter'] = 'Sendt nyhetsbrev';

$txt['modlog_ac_install_package'] = 'Installert ny pakke: &quot;{package}&quot;, versjon {version}';
$txt['modlog_ac_upgrade_package'] = 'Oppgradert pakke: &quot;{package}&quot; til versjon {version}';
$txt['modlog_ac_uninstall_package'] = 'Avinstallert pakke: &quot;{package}&quot;, versjon {version}';

// Restore topic.
$txt['modlog_ac_restore_topic'] = 'Gjenopprettet emne &quot;{topic}&quot; fra &quot;{board}&quot; til &quot;{board_to}&quot;';
$txt['modlog_ac_restore_posts'] = 'Gjenopprettet innlegg fra &quot;{subject}&quot; til emne &quot;{topic}&quot; fra forum &quot;{board}&quot;.';

$txt['modlog_parameter_guest'] = '<em>Gjest</em>';

?>