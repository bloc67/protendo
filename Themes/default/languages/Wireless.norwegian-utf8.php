<?php
// Version: 2.0; Wireless

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['wireless_error_home'] = 'Hovedside';

$txt['wireless_options'] = 'Tilleggsinnstillinger';
$txt['wireless_options_login'] = 'Logg inn';
$txt['wireless_options_logout'] = 'Logg ut';

$txt['wireless_navigation'] = 'Navigering';
$txt['wireless_navigation_up'] = 'Opp et nivå';
$txt['wireless_navigation_next'] = 'Neste side';
$txt['wireless_navigation_prev'] = 'Forrige side';
$txt['wireless_navigation_index'] = 'Oversikt';
$txt['wireless_navigation_topic'] = 'Tilbake til emnet';

$txt['wireless_pm_inbox'] = 'PM innboks';
$txt['wireless_pm_inbox_new'] = 'PM innboks (<span style="color: red;">%1$d nye</span>)';
$txt['wireless_pm_by'] = 'av';
$txt['wireless_pm_add_buddy'] = 'Legg til venn';
$txt['wireless_pm_select_buddy'] = 'Velg en venn';
$txt['wireless_pm_search_member'] = 'Søk etter medlem';
$txt['wireless_pm_search_name'] = 'Navn';
$txt['wireless_pm_no_recipients'] = 'Ingen mottakere (ennå)';
$txt['wireless_pm_reply'] = 'Svar';
$txt['wireless_pm_reply_all'] = 'Svar til alle';
$txt['wireless_pm_reply_to'] = 'Svar til';

$txt['wireless_recent_unread_posts'] = 'Uleste emner';
$txt['wireless_recent_unread_replies'] = 'Uleste svar';

$txt['wireless_display_edit'] = 'Rediger';
$txt['wireless_display_moderate'] = 'Moderer';
$txt['wireless_display_sticky'] = 'Prioritér';
$txt['wireless_display_unsticky'] = 'Ikke Prioritér';
$txt['wireless_display_lock'] = 'Låse';
$txt['wireless_display_unlock'] = 'Åpne';

$txt['wireless_end_code'] = 'Slutt kode';
$txt['wireless_end_quote'] = 'Slutt sitat';

$txt['wireless_profile_pm'] = 'Send personlig beskjed';
$txt['wireless_ban_ip'] = 'Utesteng IP';
$txt['wireless_ban_hostname'] = 'Utesteng vertsnavn';
$txt['wireless_ban_email'] = 'Utesteng e-postdomene';
$txt['wireless_additional_info'] = 'Tilleggs informasjon';
$txt['wireless_go_to_full_version'] = 'Skift til full visning';

?>