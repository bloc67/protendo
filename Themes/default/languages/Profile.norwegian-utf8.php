<?php
// Version: 2.0; Profile

global $scripturl, $context;

$txt['no_profile_edit'] = 'Du har ikke tilgang til å endre dette medlemmets profil.';
$txt['website_title'] = 'Tittel på hjemmeside';
$txt['website_url'] = 'Nettadresse til hjemmeside';
$txt['signature'] = 'Signatur';
$txt['profile_posts'] = 'Innlegg';
$txt['change_profile'] = 'Endre profil';
$txt['delete_user'] = 'Slett medlem';
$txt['current_status'] = 'Gjeldende status:';
$txt['personal_text'] = 'Personlig tekst';
$txt['personal_picture'] = 'Personlig bilde';
$txt['no_avatar'] = 'Ingen avatar';
$txt['choose_avatar_gallery'] = 'Velg avatar fra galleriet';
$txt['picture_text'] = 'Avatar/tekst';
$txt['reset_form'] = 'Tilbakestill';
$txt['preferred_language'] = 'Foretrukket språk';
$txt['age'] = 'Alder';
$txt['no_pic'] = '(uten bilde)';
$txt['latest_posts'] = 'Siste innlegg fra: ';
$txt['additional_info'] = 'Tilleggsinformasjon';
$txt['avatar_by_url'] = 'Skriv inn adressen til din avatar. (eks.: <em>http://www.sidenmin.no/mittbilde.jpg</em>)';
$txt['my_own_pic'] = 'Last opp avatar fra nettadresse';
$txt['date_format'] = 'Dette valget vil påvirke visningen av datoen på hele forumet.';
$txt['time_format'] = 'Tidsformat';
$txt['display_name_desc'] = 'Dette er visningsnavnet som folk får se.';
$txt['personal_time_offset'] = 'Antall timer som skal legges til eller trekkes fra slik at forumet viser riktig tid.';
$txt['dob'] = 'Fødselsdag';
$txt['dob_month'] = 'Måned (MM)';
$txt['dob_day'] = 'Dag (DD)';
$txt['dob_year'] = 'År (ÅÅÅÅ)';
$txt['password_strength'] = 'For best sikkerhet bør du bruke seks eller flere tegn bestående av en kombinasjon av bokstaver, tall og symboler.';
$txt['include_website_url'] = 'Denne må fylles ut om du skriver en adresse under.';
$txt['complete_url'] = 'Dette må være en fullstendig adresse, husk http:// i starten.';
$txt['your_icq'] = 'Dette er ICQ-nummeret ditt.';
$txt['your_aim'] = 'Dette er kallenavnet ditt på AIM (AOL Instant Messenger).';
$txt['your_yim'] = 'Dette er kallenavnet ditt på YIM (Yahoo! Instant Messenger).';
$txt['sig_info'] = 'Signaturer vises på bunnen av innlegg og private meldinger. BBC-kode og smilefjes kan brukes i signaturen.';
$txt['max_sig_characters'] = 'Max tegn %1$d; tegn igjen:';
$txt['send_member_pm'] = 'Send medlemmet en personlig melding';
$txt['hidden'] = 'skjult';
$txt['current_time'] = 'Gjeldende tid på forumet';
$txt['digits_only'] = '\'Antall innlegg\' boksen kan kun inneholde tall.';

$txt['language'] = 'Språk';
$txt['avatar_too_big'] = 'Avatar-bildet er for stort, vennligst endre størrelsen og prøv på nytt (maks';
$txt['invalid_registration'] = 'Ugyldig verdi i registreringsdato. Gyldig eksempel:';
$txt['msn_email_address'] = 'Din e-postadresse på MSN';
$txt['current_password'] = 'Gjeldende passord';
// Don't use entities in the below string, except the main ones. (lt, gt, quot.)
$txt['required_security_reasons'] = 'Når du gjør endringer i profilen din er det av sikkerhetsgrunner påkrevd at du skriver inn ditt gjeldende passord.';

$txt['timeoffset_autodetect'] = '(Automatisk gjenkjenning)';

$txt['secret_question'] = 'Hemmelig spørsmål';
$txt['secret_desc'] = 'For å hjelpe deg med å få tilbake passordet ditt, skriv inn et spørsmål med et svar som <strong>kun</strong> du vet svaret på.';
$txt['secret_desc2'] = 'Vær lur når du velger, slik at ikke andre kan gjette svaret!';
$txt['secret_answer'] = 'Svar';
$txt['secret_ask'] = 'Still meg spørsmålet';
$txt['cant_retrieve'] = 'Du kan ikke få tilbake passordet ditt, men du kan lage et nytt et ved å følge lenken du fikk i din e-post.  Du har også valget med å sette et nytt passord ved å svaret på ditt hemmelige spørsmål.';
$txt['incorrect_answer'] = 'Beklager, men du spesifiserte ikke noen gyldig spørsmål/svar-kombinasjon i profilen din. Klikk på tilbake-knappen og bruk den vanlige metoden for å få tilbake ditt passord.';
$txt['enter_new_password'] = 'Vennligst skriv inn svar på spørsmålet ditt og passordet du ønsker å bruke. Passordet ditt vil bli endret om du svarer korrekt på spørsmålet.';
$txt['password_success'] = 'Passordet er nå endret.<br />Klikk <a href="' . $scripturl . '?action=login">her</a> for å logge inn.';
$txt['secret_why_blank'] = 'hvorfor er dette tomt?';

$txt['authentication_reminder'] = 'Identifikasjons-påminnelse';
$txt['password_reminder_desc'] = 'Hvis du har glemt dine innloggingsdetaljer, ikke bekymre deg, de kan du få av oss. For å starte denne prosessen vennligst skriv inn brukernavnet eller e-postadresse nedenfor.';
$txt['authentication_options'] = 'Velg ett av de to alternativene nedenfor';
$txt['authentication_openid_email'] = 'Send meg en påminnelse om min OpenID identitet på e-post';
$txt['authentication_openid_secret'] = 'Svar på mitt &quot;hemmelige spørsmål&quot; for å vise min OpenID identitet';
$txt['authentication_password_email'] = 'Send meg nytt passord på e-post';
$txt['authentication_password_secret'] = 'La meg sette et nytt passord ved å svare på mitt &quot;hemmelige spørsmål&quot;';
$txt['openid_secret_reminder'] = 'Vennligst skriv inn ditt svar på spørsmålet nedenfor. Hvis du svarer riktig vil OpenID identitet  bli vist.';
$txt['reminder_openid_is'] = 'Den OpenID identiteten som er tilknyttet kontoen din er:<br />&nbsp;&nbsp;&nbsp;&nbsp;<strong>%1$s</strong><br /><br />Vennligst noter dette for fremtidig referanse.';
$txt['reminder_continue'] = 'Fortsett';

$txt['current_theme'] = 'Gjeldende design';
$txt['change'] = '(endre)';
$txt['theme_preferences'] = 'Innstillinger for design';
$txt['theme_forum_default'] = 'Forumets basis design';
$txt['theme_forum_default_desc'] = 'Dette er basisdesignet, som vil endre seg etter administrators innstillinger eller forumet du er inne på.';

$txt['profileConfirm'] = 'Vil du virkelig slette dette medlemmet?';

$txt['custom_title'] = 'Egendefinert tittel';

$txt['lastLoggedIn'] = 'Sist aktiv';

$txt['notify_settings'] = 'Varslingsinnstillinger:';
$txt['notify_save'] = 'Lagre innstillinger';
$txt['notify_important_email'] = 'Motta kunngjøringer og vikige varslinger på e-post.';
$txt['notify_regularity'] = 'For emner og forum hvor jeg har aktivert varsling, informer meg om';
$txt['notify_regularity_instant'] = 'Umiddelbart';
$txt['notify_regularity_first_only'] = 'Umiddelbart - men bare for det første uleste svar';
$txt['notify_regularity_daily'] = 'Daglig';
$txt['notify_regularity_weekly'] = 'ukentlig';
$txt['auto_notify'] = 'Aktiver varsling når du lager et nytt emne eller svarer på et emne.';
$txt['notify_send_types'] = 'For emner og forum hvor jeg har aktivert varsling, informer meg om';
$txt['notify_send_type_everything'] = 'Alt som skjer';
$txt['notify_send_type_everything_own'] = 'Alt som skjer, dersom jeg selv startet emnet';
$txt['notify_send_type_only_replies'] = 'Kun svar';
$txt['notify_send_type_nothing'] = 'Ingenting';
$txt['notify_send_body'] = 'Når det sendes e-postvarsling om svar i et emne, inkluder selve svaret i e-posten. (Vennligst ikke svar på disse.)';

$txt['notifications_topics'] = 'Gjeldende emnevarslinger';
$txt['notifications_topics_list'] = 'Du blir varslet ved svar til følgende emner';
$txt['notifications_topics_none'] = 'Du har foreløpig ingen aktive varslinger på emner.';
$txt['notifications_topics_howto'] = 'For å motta varslinger fra et emne, klikk på knappen &quot;varsling&quot; mens du viser emnet.';
$txt['notifications_boards'] = 'Gjeldende forumvarslinger';
$txt['notifications_boards_list'] = 'Du blir varslet når det kommer nye emner i følgende forum';
$txt['notifications_boards_none'] = 'Du har foreløpig ingen aktive varslinger på forum.';
$txt['notifications_boards_howto'] = 'For å motta varslinger fra et forum, klikk på knappen &quot;varsling&quot; når du er på forsiden til forumet.';
$txt['notifications_update'] = 'Stopp varsling';

$txt['statPanel_showStats'] = 'Brukerstatistikk for: ';
$txt['statPanel_users_votes'] = 'Antall avgitte stemmer';
$txt['statPanel_users_polls'] = 'Antall avstemninger opprettet';
$txt['statPanel_total_time_online'] = 'Total tid innlogget';
$txt['statPanel_noPosts'] = 'Ingen innlegg er skrevet!';
$txt['statPanel_generalStats'] = 'Generell statistikk';
$txt['statPanel_posts'] = 'innlegg';
$txt['statPanel_topics'] = 'emner';
$txt['statPanel_total_posts'] = 'Innlegg totalt';
$txt['statPanel_total_topics'] = 'Emner totalt';
$txt['statPanel_votes'] = 'stemmer';
$txt['statPanel_polls'] = 'avstemninger';
$txt['statPanel_topBoards'] = 'Mest populære forum basert på innlegg';
$txt['statPanel_topBoards_posts'] = '%1$d innlegg av forumets %2$d innlegg (%3$01.2f%%)';
$txt['statPanel_topBoards_memberposts'] = '%1$d innlegg av medlemmets %2$d innlegg (%3$01.2f%%)';
$txt['statPanel_topBoardsActivity'] = 'Mest populære forum basert på aktivitet';
$txt['statPanel_activityTime'] = 'Tidspunkt for innlegg';
$txt['statPanel_activityTime_posts'] = '%1$d innlegg (%2$d%%)';
$txt['statPanel_timeOfDay'] = 'Tidspunkt';

$txt['deleteAccount_warning'] = 'Advarsel - Disse handlingene kan ikke omgjøres!';
$txt['deleteAccount_desc'] = 'Fra denne siden kan du slette medlemmets brukerkonto og innlegg.';
$txt['deleteAccount_member'] = 'Slett dette medlemmets brukerkonto';
$txt['deleteAccount_posts'] = 'Slett innlegg skrevet av dette medlemmet';
$txt['deleteAccount_none'] = 'Ingen';
$txt['deleteAccount_all_posts'] = 'Alle innlegg';
$txt['deleteAccount_topics'] = 'Emner og innlegg';
$txt['deleteAccount_confirm'] = 'Er du sikker på at du vil slette denne brukerkontoen?';
$txt['deleteAccount_approval'] = 'Vennligst merk at forumets moderatorer må godkjenne det før brukerkontoen blir slettet.';

$txt['profile_of_username'] = 'Profilen til %1$s';
$txt['profileInfo'] = 'Profilinformasjon';
$txt['showPosts'] = 'Vis innlegg';
$txt['showPosts_help'] = 'Denne delen lar deg se alle innlegg laget av dette medlemmet. Merk at du bare kan se innlegg gjort i områder du har tilgang til.';
$txt['showMessages'] = 'Meldinger';
$txt['showTopics'] = 'Emner';
$txt['showAttachments'] = 'Vedlegg';
$txt['statPanel'] = 'Vis statistikk';
$txt['editBuddyIgnoreLists'] = 'Venne/Ignoreringsliste';
$txt['editBuddies'] = 'Endre venneliste';
$txt['editIgnoreList'] = 'Rediger Ignoreringsliste';
$txt['trackUser'] = 'Spor medlem';
$txt['trackActivity'] = 'Aktivitet';
$txt['trackIP'] = 'IP-adresse';

$txt['authentication'] = 'Autentisering';
$txt['change_authentication'] = 'Fra denne seksjonen kan du endre hvordan du logger på forumet. Du kan velge å enten bruke en OpenID-konto for godkjenning din, eller alternativt bytte til bruke et brukernavn og passord.';

$txt['profileEdit'] = 'Endre profilen';
$txt['account_info'] = 'Dette er dine kontoinnstillinger. Denne siden inneholder all nødvendig informasjon som identifiserer deg på forumet. Av sikkerhetsgrunner må du skrive ditt gjeldende passord for å lagre endringer til denne informasjonen.';
$txt['forumProfile_info'] = 'Du kan endre opplysninger om deg selv på denne siden. Det du fyller ut her vil vises på de steder ' . $context['forum_name_html_safe'] . '. Det du ikke føler deg komfortabel til å vise for andre, kan du bare la stå tomt - ingenting av dette er påkrevd at du fyller ut.';
$txt['theme'] = 'Utseende- og layoutinnstillinger';
$txt['theme_info'] = 'Denne delen lar deg endre layout og utseende på forumet.';
$txt['notification'] = 'Varsling og e-post';
$txt['notification_info'] = 'Protendo lar deg bli varslet på e-post ved innlegg, nye emner og forumkunngjøringer. Du kan endre disse innstillingene her eller se over hvilke emner og seksjoner du mottar varsling for.';
$txt['groupmembership'] = 'Gruppemedlemskap';
$txt['groupMembership_info'] = 'Fra denne delen av profilen din kan du endre hvilke grupper du tilhører.';
$txt['ignoreboards'] = 'Ignorer forum valg';
$txt['ignoreboards_info'] = 'På denne siden kan du ignorere enkelte forum. Når et forum er ignorert, vil det nye innleggs indikatoren ikke vises på hovedsiden. Nye innlegg vil ikke vises ved hjelp av "uleste innlegg" søkeadressen (når du søker vil det ikke sjekke ut i de forumene), imidlertid  vil ignorerte forum fortsatt vises på hovedsiden og ved åpning vil det vises hvilke emner som har nye innlegg. Når du bruker "uleste svar" linken, vil nye innlegg i en ignorert forum  fortsatt bli vist.';
$txt['pmprefs'] = 'Personlige meldinger';

$txt['profileAction'] = 'Handlinger';
$txt['deleteAccount'] = 'Slett denne kontoen';
$txt['profileSendIm'] = 'Send en PM';
$txt['profile_sendpm_short'] = 'Send personlig melding';

$txt['profileBanUser'] = 'Utesteng medlemmet';

$txt['display_name'] = 'Visningsnavn';
$txt['enter_ip'] = 'Skriv inn IP (område)';
$txt['errors_by'] = 'Feilmeldinger fra';
$txt['errors_desc'] = 'Nedenfor er en liste over alle nylige feilmeldinger dette medlemmet har forårsaket/opplevd.';
$txt['errors_from_ip'] = 'Feilmeldinger fra IP (område)';
$txt['errors_from_ip_desc'] = 'Nedenfor er en liste over alle nylige feilmeldinger forårsaket av denne IP\'en (området).';
$txt['ip_address'] = 'IP-adresse';
$txt['ips_in_errors'] = 'IP-er brukt i feilmeldinger';
$txt['ips_in_messages'] = 'IP\'er brukt i sidste innlegg';
$txt['members_from_ip'] = 'Medlemmer fra IP-en (område)';
$txt['members_in_range'] = 'Medlemmer som kan være i samme område';
$txt['messages_from_ip'] = 'Innlegg skrevet fra IP (område)';
$txt['messages_from_ip_desc'] = 'Under er en liste over alle innlegg fra denne IP-en (området).';
$txt['most_recent_ip'] = 'Siste IP-adresser';
$txt['why_two_ip_address'] = 'Hvorfor er det to IP-adresser som er oppført?';
$txt['no_errors_from_ip'] = 'Ingen feilmeldinger fra IP-en (området) ble funnet';
$txt['no_errors_from_user'] = 'Ingen feilmeldinger fra medlemmet ble funnet';
$txt['no_members_from_ip'] = 'Ingen medlemmer fra IP-en (området) ble funnet';
$txt['no_messages_from_ip'] = 'Ingen innlegg fra IP-en (området) ble funnet';
$txt['none'] = 'Ingen';
$txt['own_profile_confirm'] = 'Er du sikker på at du vil slette deg selv fra forumet?';
$txt['view_ips_by'] = 'Vis IP\'er brukt av';

$txt['avatar_will_upload'] = 'Last opp en avatar';

$txt['activate_changed_email_title'] = 'Din E-post er endret';
$txt['activate_changed_email_desc'] = 'Du har endret din e-postadresse. For å bekrefte at den nye adressen er gyldig har du mottatt en e-post. Klikk på linken i din e-post for å aktivere medlemskontoen din.';

// Use numeric entities in the below three strings.
$txt['no_reminder_email'] = 'Kunne ikke sende e-post for påminnelse.';
$txt['send_email'] = 'Send en e-post til';
$txt['to_ask_password'] = 'med forespørsel om passord';

$txt['user_email'] = 'Brukernavn/E-post';

// Use numeric entities in the below two strings.
$txt['reminder_subject'] = 'Nytt passord på ' . $context['forum_name'];
$txt['reminder_mail'] = 'Denne e-posten ble sendt fordi funksjonen \'Glemt passord\' ble benyttet for din medlemskonto. For å endre passord, klikk på denne lenken';
$txt['reminder_sent'] = 'En mail har blitt send til din e-postadresse. Klikk på linken i mailen for å endre passord.';
$txt['reminder_openid_sent'] = 'Din nåværende OpenID identitet har blitt sendt til din e-postadresse.';
$txt['reminder_set_password'] = 'Skriv inn et passord';
$txt['reminder_password_set'] = 'Passordet ble endret';
$txt['reminder_error'] = '%1$s klarte ikke å svare korrekt på sitt spørsmål under et forsøk på å endre glemt passord.';

$txt['registration_not_approved'] = 'Beklager, men denne medlemskontoen har ikke blitt godkjent enda. Dersom du må endre din e-postadresse, klikk';
$txt['registration_not_activated'] = 'Beklager, men denne medlemskontoen har ikke blitt aktivert enda. Dersom du ikke har mottatt en aktiveringsmail, klikk';

$txt['primary_membergroup'] = 'Primær medlemsgruppe';
$txt['additional_membergroups'] = 'Flere medlemsgrupper';
$txt['additional_membergroups_show'] = '[ vis flere medlemsgrupper ]';
$txt['no_primary_membergroup'] = '(ingen primær medlemsgruppe)';
$txt['deadmin_confirm'] = 'Er du helt sikker på at du vil si fra deg din administrator-posisjon?';

$txt['account_activate_method_2'] = 'Kontoen krever reaktivering etter e-postskifte';
$txt['account_activate_method_3'] = 'Kontoen er ikke godkjent';
$txt['account_activate_method_4'] = 'Medlemskontoen venter på godkjennelse for sletting';
$txt['account_activate_method_5'] = 'Medlemskontoen er under aldersgrensen og venter på godkjenning';
$txt['account_not_activated'] = 'Medlemskontoen er for øyeblikket ikke aktivert';
$txt['account_activate'] = 'aktiver';
$txt['account_approve'] = 'aksepter';
$txt['user_is_banned'] = 'Medlemmet er for øyeblikket utestengt';
$txt['view_ban'] = 'Vis';
$txt['user_banned_by_following'] = 'Medlemmet er utestengt basert på følgende utestengelser';
$txt['user_cannot_due_to'] = 'Medlemmet kan ikke %1$s som følge av utestengelsen: &quot;%2$s&quot;';
$txt['ban_type_post'] = 'skrive';
$txt['ban_type_register'] = 'registrere seg';
$txt['ban_type_login'] = 'logge inn';
$txt['ban_type_access'] = 'få tilgang til forumet';

$txt['show_online'] = 'Vis andre min innloggings-status';

$txt['return_to_post'] = 'Gå tilbake til emnet etter innleggskriving som forvalg.';
$txt['no_new_reply_warning'] = 'Ikke gi advarsel om andre innlegg når du poster et innlegg.';
$txt['posts_apply_ignore_list'] = 'Skjul meldinger postet av medlemmene på min ignoreringsliste';
$txt['recent_posts_at_top'] = 'Vis de nyeste innleggene først.';
$txt['recent_pms_at_top'] = 'Vis de nyeste private meldingene først.';
$txt['wysiwyg_default'] = 'Vis WYSIWYG editor på innlegg side som standard';

$txt['timeformat_default'] = '(Standard for forumet)';
$txt['timeformat_easy1'] = 'Måned, dag, år, TT:MM:SS (12-timer)';
$txt['timeformat_easy2'] = 'Måned, dag, år, TT:MM:SS (24-timer)';
$txt['timeformat_easy3'] = 'ÅÅÅÅ-MM-DD, TT:MM:SS';
$txt['timeformat_easy4'] = 'DD Måned ÅÅÅÅ, TT:MM:SS';
$txt['timeformat_easy5'] = 'DD-MM-ÅÅÅÅ, TT:MM:SS';

$txt['poster'] = 'Forfatter';

$txt['board_desc_inside'] = 'Vis forumbeskrivelse inne i hvert forum.';
$txt['show_children'] = 'Vis underforum på alle sider inne i forumet, ikke bare den første.';
$txt['use_sidebar_menu'] = 'Bruk sidepanelmenyer i stedet for nedtrekksmenyer når dette er mulig.';
$txt['show_no_avatars'] = 'Ikke vis andre medlemmers avatarer.';
$txt['show_no_signatures'] = 'Ikke vis andre medlemmers signaturer.';
$txt['show_no_censored'] = 'La ord forbli usensurert.';
$txt['topics_per_page'] = 'Emner å vise per side:';
$txt['messages_per_page'] = 'Innlegg  å vise per side:';
$txt['per_page_default'] = 'forum standard';
$txt['calendar_start_day'] = 'Første ukedag i kalenderen';
$txt['display_quick_reply'] = 'Bruk hurtigsvar i emner: ';
$txt['display_quick_reply1'] = 'Nei, ikke i det hele tatt';
$txt['display_quick_reply2'] = 'Ja, men deaktivert som standard';
$txt['display_quick_reply3'] = 'Ja, helt aktivert';
$txt['display_quick_mod'] = 'Vis hurtigmoderasjonsknapper som';
$txt['display_quick_mod_none'] = 'Ikke vis.';
$txt['display_quick_mod_check'] = 'avkryssingsbokser.';
$txt['display_quick_mod_image'] = 'symboler.';

$txt['whois_title'] = 'Spor IP på en regional whois-server';
$txt['whois_afrinic'] = 'AfriNIC (Afrika)';
$txt['whois_apnic'] = 'APNIC (Asia Pacific region)';
$txt['whois_arin'] = 'ARIN (Nord-Amerika, deler av Karibien og Afrika sør for Sahara)';
$txt['whois_lacnic'] = 'LACNIC (Latin-Amerika og Karibien)';
$txt['whois_ripe'] = 'RIPE (Europa, Midt-Østen og deler av Afrika og Asia)';

$txt['moderator_why_missing'] = 'hvorfor er ikke moderator her?';
$txt['username_change'] = 'endre';
$txt['username_warning'] = 'For å endre dette medlemmets brukernavn må forumet også nullstille passordet. Nytt passord blir sendt på e-post til medlemmet sammen med det nye brukernavnet.';

$txt['show_member_posts'] = 'Vis medlemmets innlegg';
$txt['show_member_topics'] = 'Vis medlemmets emner';
$txt['show_member_attachments'] = 'Vis medlemmets vedlegg';
$txt['show_posts_none'] = 'Ingen innlegg har blitt skrevet enda.';
$txt['show_topics_none'] = 'Ingen emner har blitt skrevet enda.';
$txt['show_attachments_none'] = 'Ingen vedlegg har blitt lagt til ennå.';
$txt['show_attach_filename'] = 'Filnavn';
$txt['show_attach_downloads'] = 'Nedlastinger';
$txt['show_attach_posted'] = 'Skrevet';

$txt['showPermissions'] = 'Vis rettigheter';
$txt['showPermissions_status'] = 'Status for tillatelse';
$txt['showPermissions_help'] = 'Denne delen lar deg se alle tillatelser for medlemmet (nektet tillatelser er <del>overstreket</del>).';
$txt['showPermissions_given'] = 'Gitt av';
$txt['showPermissions_denied'] = 'Nektet av';
$txt['showPermissions_permission'] = 'Rettighet (nektede rettigheter er <del>overstreket</del>)';
$txt['showPermissions_none_general'] = 'Dette medlemmet har ingen generelle rettigheter.';
$txt['showPermissions_none_board'] = 'Dette medlemmet har ingen forumspesifikke rettigheter.';
$txt['showPermissions_all'] = 'Som en administrator har dette medlemmet alle rettigheter.';
$txt['showPermissions_select'] = 'Forumspesifikke rettigheter for';
$txt['showPermissions_general'] = 'Generelle rettigheter';
$txt['showPermissions_global'] = 'Alle forum';
$txt['showPermissions_restricted_boards'] = 'Begrensete fora';
$txt['showPermissions_restricted_boards_desc'] = 'Følgende fora er ikke tilgjengelige for dette medlemmet';

$txt['local_time'] = 'Lokal tid';
$txt['posts_per_day'] = 'per dag';

$txt['buddy_ignore_desc'] = 'Dette området tillater deg å opprettholde din kompis og ignoreringslisten for dette forumet. Legge til medlemmer i disse listene skal blant annet bidra til å kontrollere post og PM trafikk, avhengig av dine preferanser.';

$txt['buddy_add'] = 'Legg til venneliste';
$txt['buddy_remove'] = 'Fjern fra venneliste';
$txt['buddy_add_button'] = 'Legg til';
$txt['no_buddies'] = 'Din venneliste er for øyeblikket tom';

$txt['ignore_add'] = 'Legg til Ignoreringsliste';
$txt['ignore_remove'] = 'Fjerne fra ignoreringsliste';
$txt['ignore_add_button'] = 'Legg til';
$txt['no_ignore'] = 'Din ignoreringsliste er for tiden tom';

$txt['regular_members'] = 'Registrerte medlemmer';
$txt['regular_members_desc'] = 'Alle medlemmene av forumet er medlem av denne gruppen.';
$txt['group_membership_msg_free'] = 'Ditt medlemskap i gruppen ble oppdatert.';
$txt['group_membership_msg_request'] = 'Din forespørsel er sendt inn, vær snill å være tålmodig mens forespørselen behandles.';
$txt['group_membership_msg_primary'] = 'Din primære gruppe har blitt oppdatert';
$txt['current_membergroups'] = 'Gjeldende medlemsgrupper';
$txt['available_groups'] = 'Tilgjengelige grupper';
$txt['join_group'] = 'Bli medlem i gruppe';
$txt['leave_group'] = 'Forlate gruppe';
$txt['request_group'] = 'Be om medlemskap';
$txt['approval_pending'] = 'Godkjennelse avventer';
$txt['make_primary'] = 'Gjør den til primær gruppe';

$txt['request_group_membership'] = 'Gruppemedlemskaps forespørsel';
$txt['request_group_membership_desc'] = 'Før du kan bli med i denne gruppen, må ditt medlemskap godkjennes av moderator. Vennligst oppgi en grunn for å bli med i denne gruppen';
$txt['submit_request'] = 'Send inn forespørsel';

$txt['profile_updated_own'] = 'Din profil har blitt oppdatert.';
$txt['profile_updated_else'] = 'Profilen for %1$s har blitt oppdatert.';

$txt['profile_error_signature_max_length'] = 'Signaturen kan ikke være større enn %1$d tegn';
$txt['profile_error_signature_max_lines'] = 'Signaturen kan ikke være mer enn på %1$d linjer';
$txt['profile_error_signature_max_image_size'] = 'Bilder i signaturen din må ikke være bredere enn %1$dx%2$d piksler';
$txt['profile_error_signature_max_image_width'] = 'Bilder i signaturen din må ikke være bredere enn %1$d piksler';
$txt['profile_error_signature_max_image_height'] = 'Bilder i signaturen din må ikke være høyere enn %1$d piksler';
$txt['profile_error_signature_max_image_count'] = 'Du kan ikke ha mer enn %1$d bilder i signaturen din';
$txt['profile_error_signature_max_font_size'] = 'Tekst i signaturen kan ikke være større enn %1$s i størrelse';
$txt['profile_error_signature_allow_smileys'] = 'Du har ikke lov til å bruke smileys i signaturen din';
$txt['profile_error_signature_max_smileys'] = 'Du har ikke lov til å bruke mer enn %1$d smilefjes i signaturen din';
$txt['profile_error_signature_disabled_bbc'] = 'Følgende BBC er ikke tillatt i signaturen din: %1$s';

$txt['profile_view_warnings'] = 'Vis advarsler';
$txt['profile_issue_warning'] = 'Utstede en advarsel';
$txt['profile_warning_level'] = 'Advarselsnivå';
$txt['profile_warning_desc'] = 'Fra denne seksjonen kan du justere brukerens advarselsnivå og gi dem med en skriftlig advarsel om nødvendig. Du kan også spore deres advarselshistorie og vise virkningene av deres nåværende advarsels nivå som bestemmes av administrator.';
$txt['profile_warning_name'] = 'Medlemsnavn';
$txt['profile_warning_impact'] = 'Resultat';
$txt['profile_warning_reason'] = 'Årsak til advarsel';
$txt['profile_warning_reason_desc'] = 'Dette er nødvendig og vil bli logget.';
$txt['profile_warning_effect_none'] = 'Ingen.';
$txt['profile_warning_effect_watch'] = 'Brukeren vil bli lagt til moderators overvåkings liste.';
$txt['profile_warning_effect_own_watched'] = 'Du er på moderators overvåkings liste.';
$txt['profile_warning_is_watch'] = 'blir overvåket';
$txt['profile_warning_effect_moderation'] = 'Alle brukere innlegg vil bli moderert.';
$txt['profile_warning_effect_own_moderated'] = 'Alle dine innlegg vil bli moderert.';
$txt['profile_warning_is_moderation'] = 'innlegg er moderert';
$txt['profile_warning_effect_mute'] = 'Brukeren vil ikke kunne poste innlegg.';
$txt['profile_warning_effect_own_muted'] = 'Du vil ikke kunne skrive innlegg.';
$txt['profile_warning_is_muted'] = 'Kan ikke skrive innlegg';
$txt['profile_warning_effect_text'] = 'Nivå >= %1$d: %2$s';
$txt['profile_warning_notify'] = 'Send en melding';
$txt['profile_warning_notify_template'] = 'Velg mal:';
$txt['profile_warning_notify_subject'] = 'Varselets tittel';
$txt['profile_warning_notify_body'] = 'Varselets melding';
$txt['profile_warning_notify_template_subject'] = 'Du har mottatt en advarsel';
// Use numeric entities in below string.
$txt['profile_warning_notify_template_outline'] = '{MEMBER},' . "\n\n" . 'Du har mottatt en advarsel for %1$s. Vennligst opphøre disse aktivitetene og rette seg etter forum reglene ellers vil vi ta ytterligere tiltak.' . "\n\n" . '{REGARDS}';
$txt['profile_warning_notify_template_outline_post'] = '{MEMBER},' . "\n\n" . 'Du har mottatt en advarsel for %1$s in regards to the message:' . "\n" . '{MESSAGE}.' . "\n\n" . 'Vennligst opphøre disse aktivitetene og rette seg etter forum reglene ellers vil vi ta ytterligere tiltak.' . "\n\n" . '{REGARDS}';
$txt['profile_warning_notify_for_spamming'] = 'søppelpost';
$txt['profile_warning_notify_title_spamming'] = 'Søppelpost';
$txt['profile_warning_notify_for_offence'] = 'skrevet støtende innhold';
$txt['profile_warning_notify_title_offence'] = 'Skrevet støtende innhold';
$txt['profile_warning_notify_for_insulting'] = 'fornærmende andre brukere og/eller moderatorer';
$txt['profile_warning_notify_title_insulting'] = 'Fornærmede brukere/medarbeidere';
$txt['profile_warning_issue'] = 'Gi advarsel';
$txt['profile_warning_max'] = '(maks. 100)';
$txt['profile_warning_limit_attribute'] = 'Merk at du kan ikke justere denne brukerens nivå med mer enn %1$d%% i en 24 timers periode.';
$txt['profile_warning_errors_occured'] = 'Advarsel har ikke blitt sendt på grunn av følgende feil';
$txt['profile_warning_success'] = 'Advarsel ble utstedt';
$txt['profile_warning_new_template'] = 'Ny mal';

$txt['profile_warning_previous'] = 'Tidligere advarsler';
$txt['profile_warning_previous_none'] = 'Denne brukeren har ikke mottatt noen tidligere advarsler.';
$txt['profile_warning_previous_issued'] = 'Utstedt av';
$txt['profile_warning_previous_time'] = 'Dato';
$txt['profile_warning_previous_level'] = 'Poeng';
$txt['profile_warning_previous_reason'] = 'Grunn';
$txt['profile_warning_previous_notice'] = 'Se kunngjøring sendt til medlem';

$txt['viewwarning'] = 'Vis advarsler';
$txt['profile_viewwarning_for_user'] = 'Advarsel for %1$s';
$txt['profile_viewwarning_no_warnings'] = 'Ingen advarseler har blitt utstedt enda.';
$txt['profile_viewwarning_desc'] = 'Nedenfor er en oppsummering av alle advarslene som er gitt av forumet moderatorer.';
$txt['profile_viewwarning_previous_warnings'] = 'Tidligere advarsler';
$txt['profile_viewwarning_impact'] = 'Effekt av advarsel';

$txt['subscriptions'] = 'Betalet abonnementer';

$txt['pm_settings_desc'] = 'Fra denne siden kan du endre en rekke personlige meldingsvalg, inkludert hvordan meldinger skal vises, og hvem som kan sende dem til deg.';
$txt['email_notify'] = 'Motta varsling på e-post hver gang du mottar en personlig melding:';
$txt['email_notify_never'] = 'Aldri';
$txt['email_notify_buddies'] = 'Kun fra venner';
$txt['email_notify_always'] = 'Alltid';

$txt['pm_receive_from'] = 'Motta personlige meldinger fra:';
$txt['pm_receive_from_everyone'] = 'Alle medlemmer';
$txt['pm_receive_from_ignore'] = 'Alle medlemmer, unntatt de på min ignoreringsliste';
$txt['pm_receive_from_admins'] = 'Bare administratorer';
$txt['pm_receive_from_buddies'] = 'bare venner og administratorer';

$txt['copy_to_outbox'] = 'Lagre en kopi av hver personlig melding i mine sendte elementer som standard.';
$txt['popup_messages'] = 'Vis en popup når jeg mottar nye meldinger.';
$txt['pm_remove_inbox_label'] = 'Fjern innboks etiketten når en annen etikett blir lagt til.';
$txt['pm_display_mode'] = 'Vis personlige meldinger';
$txt['pm_display_mode_all'] = 'Alle på en gang';
$txt['pm_display_mode_one'] = 'En om gangen';
$txt['pm_display_mode_linked'] = 'Som en samtale';
// Use entities in the below string.
$txt['pm_recommend_enable_outbox'] = 'For å få mest mulig ut av denne innstillingen foreslår vi at du som standard aktiverer &amp;quot;Lagre en kopi av hver PM i min utboks som  standardinnstilling&amp;quot;\\n\\nDet vil bidra til at samtalene flyte bedre ettersom du kan se begge sider av samtalen.';

$txt['tracking'] = 'Sporing';
$txt['tracking_description'] = 'Denne delen gir deg mulighet til å vurdere visse profil handlinger utført på dette medlemets profil samt spore IP-adressen.';

$txt['trackEdits'] = 'Profilendringer';
$txt['trackEdit_deleted_member'] = 'Slettet medlem';
$txt['trackEdit_no_edits'] = 'Ingen endringer har så langt vært registrert for dette medlemmet.';
$txt['trackEdit_action'] = 'Felt';
$txt['trackEdit_before'] = 'Tidligere verdi';
$txt['trackEdit_after'] = 'Verdi etter';
$txt['trackEdit_applicator'] = 'Endret av';

$txt['trackEdit_action_real_name'] = 'Medlemsnavn';
$txt['trackEdit_action_usertitle'] = 'Egendefinert tittel';
$txt['trackEdit_action_member_name'] = 'Brukernavn';
$txt['trackEdit_action_email_address'] = 'E-postadresse';
$txt['trackEdit_action_id_group'] = 'Primær medlemsgruppe';
$txt['trackEdit_action_additional_groups'] = 'Flere medlemsgrupper';

?>