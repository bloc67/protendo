<?php
// Version: 2.0; Packages

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['package_proceed'] = 'Fortsett';
$txt['php_script'] = 'Modifikasjonsfilen ble pakket ut, men denne modifikasjonen inneholder et PHP-script som må kjøres før den vil fungere';
$txt['package_run'] = 'Kjør';
$txt['package_read'] = 'Les';
$txt['script_output'] = 'Utdata fra scriptet:';
$txt['additional_notes'] = 'Tilleggsopplysninger';
$txt['notes_file'] = 'Instruksjoner/Tilleggsopplysninger fra filen';
$txt['list_file'] = 'List opp filer i pakken';
$txt['files_archive'] = 'Filer i arkivet';
$txt['package_get'] = 'Hent pakker';
$txt['package_servers'] = 'Pakkeservere';
$txt['package_browse'] = 'Bla';
$txt['add_server'] = 'Legg til server';
$txt['server_name'] = 'Servernavn';
$txt['serverurl'] = 'Adresse';
$txt['no_packages'] = 'Ingen pakker ennå.';
$txt['download'] = 'Last ned';
$txt['download_success'] = 'Pakken ble lastet ned';
$txt['package_downloaded_successfully'] = 'Pakken ble lastet ned';
$txt['package_manager'] = 'Pakkebehandling';
$txt['install_mod'] = 'Installer modifikasjon';
$txt['uninstall_mod'] = 'Avinstaller modifikasjon';
$txt['sql_file'] = 'Modifikasjonsfilen ble pakket ut, denne modifikasjonen kommer også med en SQL-fil med endringer i databasen som må kjøres. Det er en god idé å kjøre den.';
$txt['sql_queries'] = 'SQL spørringer';
$txt['no_mods_installed'] = 'Ingen modifikasjoner er installert';
$txt['browse_installed'] = 'Se igjennom installerte modifikasjoner';
$txt['uninstall'] = 'Avinstaller';
$txt['delete_list'] = 'Slett modifikasjonsliste';
$txt['php_safe_mode'] = 'Beklager, serveren din har PHP i SAFE MODE. Denne funskjonen er ikke tilgjengelig i SAFE MODE. Beklager.';
$txt['lets_try_anyway'] = 'La meg prøve uansett.';

$txt['package_manager_desc'] = 'Fra pakkebehandlingen kan du laste ned og installere modifikasjoner til ditt forum ved hjelp av et enkelt grensesnitt.';
$txt['installed_packages_desc'] = 'Du kan bruke grensesnittet til å vise pakker som er installert på forumet og fjerne dem du ikke trenger lenger.';
$txt['download_packages_desc'] = 'Fra dette bilde kan du enten laste ned pakker fra en server eller laste opp en pakke fra din datamaskin.';

$txt['create_package'] = 'Opprett en ny pakke';
$txt['download_new_package'] = 'Last ned nye pakker';
$txt['view_and_remove'] = 'Vis og slett installerte pakker';
$txt['modification_package'] = 'Modifiseringspakker';
$txt['avatar_package'] = 'Avatarpakker';
$txt['language_package'] = 'Språkpakker';
$txt['unknown_package'] = 'Ukjente pakker';
$txt['use_avatars'] = 'Bruk avatarer';
$txt['add_languages'] = 'Legg til språk';
$txt['list_files'] = 'List opp alle filene';
$txt['remove'] = 'Slett';
$txt['package_type'] = 'Pakketype';
$txt['archiving'] = 'Arkiverer';
$txt['extracting'] = 'Pakker ut';
$txt['avatars_extracted'] = 'Avatarene har blitt pakket ut, du kan nå bruke dem.';
$txt['language_extracted'] = 'Språkpakken har blitt pakket ut, du kan nå bruke den (ved å velge den i innstillingene dine).';

$txt['mod_name'] = 'Navn på modifikasjon';
$txt['mod_version'] = 'Versjon';
$txt['mod_author'] = 'Laget av';
$txt['author_website'] = 'Hjemmeside';
$txt['package_no_description'] = 'Ingen beskrivelse';
$txt['package_description'] = 'Beskrivelse';
$txt['file_location'] = 'Lokasjon til filen';

$txt['package_installed_key'] = 'Installerte modifikasjoner:';
$txt['package_installed_current'] = 'gjeldende versjon';
$txt['package_installed_old'] = 'eldre versjon';
$txt['package_installed_warning1'] = 'Denne pakken er allerede installert og ingen oppgradering ble funnet!';
$txt['package_installed_warning2'] = 'Du bør avinstallere den gamle versjonen først for å unngå problemer eller spør den som har laget modifikasjonen til å lage en oppgradering fra din gamle versjon.';
$txt['package_installed_warning3'] = 'Husk alltid å ta regelmessige sikkerhetskopier av dine filer og databasen før du installerer modifikasjoner, spesielt betaversjoner.';
$txt['package_installed_extract'] = 'Pakker ut pakken';
$txt['package_installed_done'] = 'Pakken ble installert. Du skal nå kunne bruke funksjonene som modifikasjonen enten la til eller redigerte, eller ikke kunne bruke funksjonene den har fjernet.';
$txt['package_installed_redirecting'] = 'Viderekobler...';
$txt['package_installed_redirect_go_now'] = 'Viderekobl nå';
$txt['package_installed_redirect_cancel'] = 'Tilbake til pakkebehandling';

$txt['packages_latest'] = 'Nyeste pakker';
$txt['packages_latest_fetch'] = 'Prøver å hente informasjon om de mest populære og nyeste pakker fra www.simplemachines.org...';

$txt['package_upgrade'] = 'Oppgrader';
$txt['package_uninstall_readme'] = 'Avinstaller Readme';
$txt['package_install_readme'] = 'Installasjonsveiledning';
$txt['package_install_type'] = 'Type';
$txt['package_install_action'] = 'Gjøremål';
$txt['package_install_desc'] = 'Beskrivelse';
$txt['install_actions'] = 'Installeringshandlinger';
$txt['perform_actions'] = 'Installering av denne pakken vil utføre følgende handlinger:';
$txt['corrupt_compatible'] = 'Pakken du prøver å laste ned eller installere er enten skadet eller ikke kompatibel med denne versjonen av Protendo.';
$txt['package_create'] = 'Opprett';
$txt['package_move'] = 'Flytt';
$txt['package_delete'] = 'Slett';
$txt['package_extract'] = 'Pakk ut';
$txt['package_file'] = 'Fil';
$txt['package_tree'] = 'Tre';
$txt['execute_modification'] = 'Kjør modifikasjon';
$txt['execute_code'] = 'Kjør kode';
$txt['execute_database_changes'] = 'Tilpass databasen';

$txt['package_install_actions'] = 'Installasjonshandlinger for';
$txt['package_will_fail_title'] = 'En feil oppstod under installasjon';
$txt['package_will_fail_warning'] = 'Det oppstod minst en feil i testinstallasjonen av denne pakken.
	Det er <strong>virkelig</strong> anbefalt at du ikke fortsetter med installasjonen, med mindre du vet hva du driver med og har en veldig oppdatert sikkerhetskopi.
	Denne feilen kan ha med en konflikt mellom pakken du prøver å installere og en allerede eksisterende pakke, en feil i selve pakken, en pakke som krever en du ikke har installert enda eller en pakke som krever en annen versjon av Protendo.';
// Don't use entities in the below string.
$txt['package_will_fail_popup'] = 'Er du sikker på at du vil fortsette med installasjonen, selv om den ikke vil bli vellykket?';
$txt['package_will_fail_popup_uninstall'] = 'Er du sikker på at du ønsker å fortsette å avinstallere denne modifikasjonen, selv om den ikke vil avinstalleres fullstendig?';
$txt['package_install_now'] = 'Installer nå';
$txt['package_uninstall_now'] = 'Avinstaller nå';
$txt['package_other_themes'] = 'Installer i et annet tema';
$txt['package_other_themes_uninstall'] = 'Avinstaller i et annet tema';
$txt['package_other_themes_desc'] = 'For å bruke denne endringen i temaer annet enn standard, må pakkebehandleren gjøre ytterligere endringer i de andre temaer. Hvis du ønsker å installere denne endringen i andre temaer, vennligst velg disse temaene nedenfor.';
// Don't use entities in the below string.
$txt['package_theme_failure_warning'] = 'Minst en feil ble oppdaget under en testinstallasjon av dette temaet. Er du sikker på at du ønsker å forsøke installasjon?';

$txt['package_bytes'] = 'byte';

$txt['package_action_missing'] = '<strong class="error">Fant ikke filen</strong>';
$txt['package_action_error'] = '<strong class="error">Feil i modifiseringsprosessen</strong>';
$txt['package_action_failure'] = '<strong class="error">Test feilet</strong>';
$txt['package_action_success'] = '<strong>Vellykket test</strong>';
$txt['package_action_skipping'] = '<strong>Hopper over fil</strong> ';

$txt['package_uninstall_actions'] = 'Avinstalleringsoperajoner';
$txt['package_uninstall_done'] = 'Pakken har blitt avinstallert, den skal ikke lenger være i bruk.';
$txt['package_uninstall_cannot'] = 'Denne pakken kan ikke bli avinstallert, fordi det ikke finnes noen avinstallasjonsfil!<br /><br />Vennligst kontakt utvikler av modifikasjonen for mer informasjon.';

$txt['package_install_options'] = 'Installeringsalternativer';
$txt['package_install_options_ftp_why'] = 'Å bruke pakkebehandlerens FTP funksjonalitet er den enkleste måten å unngå å selv måtte manuelt chmod filene skrivbar via FTP for å få pakkebehandleren til å fungere.<br />Her kan du sette standardverdier for noen felt.';
$txt['package_install_options_ftp_server'] = 'FTP-server';
$txt['package_install_options_ftp_port'] = 'Port';
$txt['package_install_options_ftp_user'] = 'Brukernavn';
$txt['package_install_options_make_backups'] = 'Opprett sikkerhetskopier av de erstattede filene ved å sette en ~ på slutten av filnavnene.';

$txt['package_ftp_necessary'] = 'FTP Informasjon er Påkrevd';
$txt['package_ftp_why'] = 'Noen av filene pakkebehandleren trenger å endre er ikke skrivbar. Dette må endres ved å logge inn via FTP og bruke den til chmod eller opprette filer og mapper. Din FTP informasjonen kan bli midlertidig hurtigbufret for at pakkebehandleren skal utføre oppgaven. Merk: Du kan også gjøre dette manuelt ved hjelp av en FTP-klient - for å vise en liste over de berørte filene vennligst klikk <a href="#" onclick="%1$s">her</a>.';
$txt['package_ftp_why_file_list'] = 'Følgende filer må gjøres skrivbar for å fortsette installasjonen:';
$txt['package_ftp_why_download'] = 'For å kunne laste ned pakker må Packages mappen og tilhørende filer være skrivbare - og det er de ikke for øyeblikket. Pakkebehandleren kan bruke din FTP informasjon og forandre dette.';
$txt['package_ftp_server'] = 'FTP Server';
$txt['package_ftp_port'] = 'Port';
$txt['package_ftp_username'] = 'Brukernavn';
$txt['package_ftp_password'] = 'Passord';
$txt['package_ftp_path'] = 'Lokal bane til Protendo';
$txt['package_ftp_test'] = 'Test';
$txt['package_ftp_test_connection'] = 'Test tilkobling';
$txt['package_ftp_test_success'] = 'FTP-forbindelsen opprettet.';
$txt['package_ftp_test_failed'] = 'Kunne ikke kontakte serveren.';

// For a break, use \\n instead of <br />... and don't use entities.
$txt['package_delete_bad'] = 'Pakken du er i ferd med å slette er installert! Hvis du sletter den, vil du ikke være i stand til å avinstallere den senere.\\n\\nEr du sikker?';

$txt['package_examine_file'] = 'Se fil i pakken';
$txt['package_file_contents'] = 'Innhold i filen';

$txt['package_upload_title'] = 'Laste opp en pakke';
$txt['package_upload_select'] = 'Pakke til å laste opp';
$txt['package_upload'] = 'Laste opp';
$txt['package_uploaded_success'] = 'Pakken er lastet opp';
$txt['package_uploaded_successfully'] = 'Pakken har blitt lastet opp.';

$txt['package_modification_malformed'] = 'Feil eller ikke tillatt modifisering i filen.';
$txt['package_modification_missing'] = 'Filen ble ikke funnet.';
$txt['package_no_zlib'] = 'Beklager, men din PHP-konfigurasjon har ikke støtte for <strong>zlib</strong>.  Uten dette, kan ikke pakkebehandleren fungere.  Ta kontakt med din leverandør for mer informasjon.';

$txt['package_cleanperms_title'] = 'Rydd opp rettigheter';
$txt['package_cleanperms_desc'] = 'Dette skjermbilde lar deg tilbakestille rettighetene for filene i installasjonen. Dette er for å enten bedre sikkerheten eller ordne eventuelle rettighetsproblemer som kan oppstå under installasjon.';
$txt['package_cleanperms_type'] = 'Sett alle rettigheter på forumet slik at';
$txt['package_cleanperms_standard'] = 'Bare standardfilene er skrivbare.';
$txt['package_cleanperms_free'] = 'Alle filene er skrivbare.';
$txt['package_cleanperms_restrictive'] = 'Bare minimumsantallet av filer er skrivbare.';
$txt['package_cleanperms_go'] = 'OK';

$txt['package_download_by_url'] = 'Last ned pakke via nettadresse';
$txt['package_download_filename'] = 'Navnet på filen';
$txt['package_download_filename_info'] = 'Valgfri verdi. Bør brukes når nettadressen ikke ender på filnavnet. For eksempel:? Index.php mod=5';

$txt['package_db_uninstall'] = 'Fjern alle data knyttet til denne modifikasjonen.';
$txt['package_db_uninstall_details'] = 'Detaljer';
$txt['package_db_uninstall_actions'] = 'Merking av dette alternativet vil resultere i følgende databaseendringer';
$txt['package_db_remove_table'] = 'Slett tabell &quot;%1$s&quot;';
$txt['package_db_remove_column'] = 'Fjern kolonne &quot;%1$s&quot; fra &quot;%2$s&quot;';
$txt['package_db_remove_index'] = 'Fjern indeks &quot;%1$s&quot; fra &quot;%2$s&quot;';

$txt['package_advanced_button'] = 'Avansert';
$txt['package_advanced_options'] = 'Avanserte alternativer';
$txt['package_apply'] = 'Bruk';
$txt['package_emulate'] = 'Etterlign versjon';
$txt['package_emulate_revert'] = 'Tilbakestill';
$txt['package_emulate_desc'] = 'Noen ganger er pakkene låst til tidlige versjoner av Protendo, men er kompatibel med en nyere versjon. Her kan du velge å &quot;etterligne&quot; en annen Protendo versjon i pakkebehandleren.';

// Operations.
$txt['operation_find'] = 'Finn';
$txt['operation_replace'] = 'Erstatt';
$txt['operation_after'] = 'Legg til etter';
$txt['operation_before'] = 'Legg til før';
$txt['operation_title'] = 'Operasjoner';
$txt['operation_ignore'] = 'Ignorer feil';
$txt['operation_invalid'] = 'Operasjonen at du valgte er ugyldig.';

$txt['package_file_perms_desc'] = 'Du kan bruke denne delen til å gjennomgå skrivbar status av kritiske filer og mapper innenfor ditt forums mapper. Merk dette gjelder bare viktig forummapper og filer - bruke en FTP-klient for flere alternativer.';
$txt['package_file_perms_name'] = 'Fil/mappenavn';
$txt['package_file_perms_status'] = 'Gjeldende status';
$txt['package_file_perms_new_status'] = 'Ny status';
$txt['package_file_perms_status_read'] = 'Les';
$txt['package_file_perms_status_write'] = 'Skriv';
$txt['package_file_perms_status_execute'] = 'Utfør';
$txt['package_file_perms_status_custom'] = 'Tilpasset';
$txt['package_file_perms_status_no_change'] = 'Ingen endring';
$txt['package_file_perms_writable'] = 'Skrivbar';
$txt['package_file_perms_not_writable'] = 'Ikke akrivbar';
$txt['package_file_perms_chmod'] = 'chmod';
$txt['package_file_perms_more_files'] = 'Flere filer';

$txt['package_file_perms_change'] = 'Endre filrettigheter';
$txt['package_file_perms_predefined'] = 'Bruk forhåndsdefinerte rettighetsprofiler';
$txt['package_file_perms_predefined_note'] = 'Merk at dette kun gjelder de forhåndsdefinerte profilen til viktige Protendo mapper og filer.';
$txt['package_file_perms_apply'] = 'Bruk individuelle filrettighetsinnstillinger valgt ovenfor.';
$txt['package_file_perms_custom'] = 'Dersom &quot;Tilpasset&quot; har blitt valgt, bruke chmod verdien fra';
$txt['package_file_perms_pre_restricted'] = 'Begrenset - minimum filer skrivbare';
$txt['package_file_perms_pre_standard'] = 'Standard - viktige filer skrivbare';
$txt['package_file_perms_pre_free'] = 'Fritt - alle filene skrivbare';
$txt['package_file_perms_ftp_details'] = 'På de fleste servere er det bare mulig å endre filrettigheter ved hjelp av en FTP-konto. Vennligst skriv inn FTP detaljer nedenfor';
$txt['package_file_perms_ftp_retain'] = 'Merk: Protendo vil bare bruke passordet midlertidig for hjelp ved bruk av pakkebehandleren.';
$txt['package_file_perms_go'] = 'Utfør endringer';

$txt['package_file_perms_applying'] = 'Bruk endringene';
$txt['package_file_perms_items_done'] = '%1$d av %2$d elementer fullført';
$txt['package_file_perms_skipping_ftp'] = '<strong>Advarsel:</strong> Kunne ikke koble til FTP-server, forsøker å endre tillatelser uten. Dette vil <em>sannsynlig</em> feile - sjekk resultatene etter gjennomføringen og prøv igjen med korrekt FTP detaljer om nødvendig.';

$txt['package_file_perms_dirs_done'] = '%1$d av %2$d mapper fullført';
$txt['package_file_perms_files_done'] = '%1$d av %2$d filer fullført i gjeldende mappe';

$txt['chmod_value_invalid'] = 'Du har prøvd å legge inn en ugyldig chmod verdi. Chmod må være mellom 0444 og 0777';

$txt['package_restore_permissions'] = 'Gjenopprett filrettigheter';
$txt['package_restore_permissions_desc'] = 'Følgende filrettigheter ble endret av Protendo for å installere de valgte pakkene. Du kan tilbakestille disse filene til sin opprinnelige status ved å klikke &quot;Gjenopprett&quot; nedenfor.';
$txt['package_restore_permissions_restore'] = 'Gjenopprett';
$txt['package_restore_permissions_filename'] = 'Filnavn';
$txt['package_restore_permissions_orig_status'] = 'Opprinnelig status';
$txt['package_restore_permissions_cur_status'] = 'Gjeldende status';
$txt['package_restore_permissions_result'] = 'Resultat';
$txt['package_restore_permissions_pre_change'] = '%1$s (%3$s)';
$txt['package_restore_permissions_post_change'] = '%2$s (%3$s - var %2$s)';
$txt['package_restore_permissions_action_skipped'] = '<em>Hoppet over</em>';
$txt['package_restore_permissions_action_success'] = '<span style="color: green;">Vellykket</span>';
$txt['package_restore_permissions_action_failure'] = '<span class="error">Feilet</span>';
$txt['package_restore_permissions_action_done'] = 'Protendo har forsøkt å gjenopprette de valgte filene tilbake til sine opprinnelige filrettighet, resultatene kan sees nedenfor. Hvis en endring mislyktes, eller for en mer detaljert visning av filrettigheter, se seksjon <a href="%1$s">Filrettigheter</a>.';

$txt['package_file_perms_warning'] = 'Merk';
$txt['package_file_perms_warning_desc'] = '
	<li>Vær forsiktig når du endrer filrettighetene fra denne seksjonen - feil rettighet kan påvirke driften av ditt forum!</li>
	<li>På noen serverkonfigurasjoner kan feil velg av rettigheter stoppe Protendo fra å virke.</li>
	<li>Enkelte mapper som <em>attachments</em> må være skrivbar for å bruke denne funksjonaliteten.</li>
	<li>Denne funksjonaliteten er hovedsakelig aktuelt på ikke-Windows-baserte servere - det fungerer ikke som forventet på Windows i forhold rettigheter.</li>
	<li>Før du går videre sørg for at du har en FTP-klient installert i tilfelle du gjør en feil og må bruke FTP for å komme inn på serveren for å rett feilen.</li>';

$txt['package_confirm_view_package_content'] = 'Er du sikker på at du vil se på pakke-innholdet fra dette stedet:<br /><br />%1$s';
$txt['package_confirm_proceed'] = 'Fortsett';
$txt['package_confirm_go_back'] = 'Gå tilbake';

$txt['package_readme_default'] = 'Standard';
$txt['package_available_readme_language'] = 'Tilgjengelig Lesmeg språk:';

?>