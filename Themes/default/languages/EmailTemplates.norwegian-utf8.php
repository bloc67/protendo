<?php
// Version: 2.0; EmailTemplates

global $context, $birthdayEmails;

// Important! Before editing these language files please read the text at the top of index.english.php.
// Since all of these strings are being used in emails, numeric entities should be used.
// Do not translate anything that is between {}, they are used as replacement variables and MUST remain exactly how they are.
//   Additionally do not translate the @additioinal_parmas: line or the variable names in the lines that follow it.  You may
//   translate the description of the variable.  Do not translate @description:, however you may translate the rest of that line.
// Do not use block comments in this file, they will have special meaning.
$txt['scheduled_approval_email_topic'] = 'Følgende emner venter på godkjenning:';
$txt['scheduled_approval_email_msg'] = 'Følgende innlegg venter på godkjenning:';
$txt['scheduled_approval_email_attach'] = 'Følgende vedlegg venter på godkjenning:';
$txt['scheduled_approval_email_event'] = 'Følgende hendelser venter på godkjenning:';

$txt['emails'] = array(
	'resend_activate_message' => array(
		/*
			@additional_params: resend_activate_message
				REALNAME: The display name for the member receiving the email.
				USERNAME:  The user name for the member receiving the email.
				ACTIVATIONLINK:  The url link to activate the member's account.
				ACTIVATIONCODE:  The code needed to activate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				FORGOTPASSWORDLINK: The url to the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som medlem på {FORUMNAME}. Brukernavnet ditt er {USERNAME}. Hvis du glemmer passordet, kan du tilbakestille det ved å gå til {FORGOTPASSWORDLINK}.

Før du kan logge inn må du aktivere ditt medlemskap. For å gjøre dette, vennligst følg denne lenken:

{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),

	'resend_pending_message' => array(
		/*
			@additional_params: resend_pending_message
				REALNAME: The display name for the member receiving the email.
				USERNAME:  The user name for the member receiving the email.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Din forespørsel om å bli medlem på {FORUMNAME} er mottatt, {REALNAME}.

Brukernavnet du registrerte deg med var {USERNAME}.

Før du kan logge inn og begynne å bruke forumet, vil din forespørsel sjekkes og godkjennes manuelt.  Når dette skjer vil du motta en ny e-post fra denne adressen.

{REGARDS}',
	),
	'mc_group_approve' => array(
		/*
			@additional_params: mc_group_approve
				USERNAME: The user name for the member receiving the email.
				GROUPNAME: The name of the membergroup that the user was accepted into.
			@description: The request to join a particular membergroup has been accepted.
		*/
		'subject' => 'Gruppemedlemskap godkjent',
		'body' => '{USERNAME},

Din forespørsel om å delta i gruppen "{GROUPNAME}" på {FORUMNAME} har blitt akseptert, og din konto har blitt oppdatert til å inkludere denne nye gruppen.

{REGARDS}',
	),
	'mc_group_reject' => array(
		/*
			@additional_params: mc_group_reject
				USERNAME: The user name for the member receiving the email.
				GROUPNAME: The name of the membergroup that the user was rejected from.
			@description: The request to join a particular membergroup has been rejected.
		*/
		'subject' => 'Gruppemedlemskap avvist',
		'body' => '{USERNAME},

Din forespørsel om å delta i gruppen "{GROUPNAME}" på {FORUMNAME} har blitt avvist.

{REGARDS}',
	),
	'mc_group_reject_reason' => array(
		/*
			@additional_params: mc_group_reject_reason
				USERNAME: The user name for the member receiving the email.
				GROUPNAME: The name of the membergroup that the user was rejected from.
				REASON: Reason for the rejection.
			@description: The request to join a particular membergroup has been rejected with a reason given.
		*/
		'subject' => 'Gruppemedlemskap avvist',
		'body' => '{USERNAME},

Din forespørsel om å delta i gruppen "{GROUPNAME}" på {FORUMNAME} har blitt avvist.

Dette skyldes følgende grunn: {REASON}

{REGARDS}',
	),
	'admin_approve_accept' => array(
		/*
			@additional_params: admin_approve_accept
				NAME: The display name of the member.
				USERNAME: The user name for the member receiving the email.
				PROFILELINK: The URL of the profile page.
				FORGOTPASSWORDLINK: The URL of the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Velkommen, {NAME}!

Din medlemskonto er blitt aktivert manuelt av administrator. Du kan nå logge inn og skrive innlegg. Brukernavnet ditt er: {USERNAME}. Hvis du glemmer passordet, kan du tilbakestille det ved å gå til {FORGOTPASSWORDLINK}.

{REGARDS}',
	),
	'admin_approve_activation' => array(
		/*
			@additional_params: admin_approve_activation
				USERNAME: The user name for the member receiving the email.
				ACTIVATIONLINK:  The url link to activate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				ACTIVATIONCODE: The activation code.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Velkommen, {USERNAME}!

Din medlemskonto på {FORUMNAME} er blitt godkjent av administrator på forumet. Før du kan logge inn, må du aktivere kontoen ved å klikke på følgende lenke:

{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'admin_approve_reject' => array(
		/*
			@additional_params: admin_approve_reject
				USERNAME: The user name for the member receiving the email.
			@description:
		*/
		'subject' => 'Registrering avvist',
		'body' => '{USERNAME},

Din forespørsel om medlemskap på {FORUMNAME} er blitt avvist.

{REGARDS}',
	),
	'admin_approve_delete' => array(
		/*
			@additional_params: admin_approve_delete
				USERNAME: The user name for the member receiving the email.
			@description:
		*/
		'subject' => 'Konto slettet',
		'body' => '{USERNAME},

Din medlemskonto på {FORUMNAME} har blitt slettet.  Dette kan være fordi du aldri har aktivert kontoen din, i så fall kan du registrere deg på nytt.

{REGARDS}',
	),
	'admin_approve_remind' => array(
		/*
			@additional_params: admin_approve_remind
				USERNAME: The user name for the member receiving the email.
				ACTIVATIONLINK:  The url link to activate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				ACTIVATIONCODE: The activation code.
			@description:
		*/
		'subject' => 'Påminnelse om registrering',
		'body' => '{USERNAME},
Du har fremdeles ikke aktivert din medlemskonto på {FORUMNAME}.

Vennligst klikk på lenken nedenfor for å aktivere medlemskontoen:
{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'admin_register_activate' => array(
		/*
			@additional_params:
				USERNAME: The user name for the member receiving the email.
				ACTIVATIONLINK:  The url link to activate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				ACTIVATIONCODE: The activation code.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Takk for at du har registrert deg på {FORUMNAME}. Ditt brukernavn er: {USERNAME} og passordet ditt er: {PASSWORD}.

Før du kan logge inn, må du aktivere kontoen ved å klikke på følgende lenke:

{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'admin_register_immediate' => array(
		'subject' => 'Welcome to {FORUMNAME}',
		'body' => 'Thank you for registering at {FORUMNAME}. Your username is {USERNAME} and your password is {PASSWORD}.

{REGARDS}',
	),
	'new_announcement' => array(
		/*
			@additional_params: new_announcement
				TOPICSUBJECT: The subject of the topic being announced.
				MESSAGE: The message body of the first post of the announced topic.
				TOPICLINK: A link to the topic being announced.
			@description:

		*/
		'subject' => 'Ny kunngjøring: {TOPICSUBJECT}',
		'body' => '{MESSAGE}

Hvis du ikke ønsker å motta slike kunngjøringer, logg inn på forumet og fjern avkryssingen ved "Motta kunngjøringer og viktige meldinger på e-post." i din profil.

Du kan lese hele kunngjøringen ved å følge denne lenken:
{TOPICLINK}

{REGARDS}',
	),
	'notify_boards_once_body' => array(
		/*
			@additional_params: notify_boards_once_body
				TOPICSUBJECT: The subject of the topic causing the notification
				TOPICLINK: A link to the topic.
				MESSAGE: This is the body of the message.
				UNSUBSCRIBELINK: Link to unsubscribe from notifications.
			@description:
		*/
		'subject' => 'Nytt emne: {TOPICSUBJECT}',
		'body' => 'Et nytt innlegg, \'{TOPICSUBJECT}\', er kommet i en seksjon du har på din varslingsliste.

Du kan se det på
{TOPICLINK}

Flere emner lages gjerne, men du kommer ikke til å motta varsling på mer enn et av dem før du går tilbake til forumet og leser noen av dem.

Teksten i emnet er gjengitt nedenfor:
{MESSAGE}

Avslutt varsling av nye emner fra dette forumet ved å klikke på denne lenken:
{UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notify_boards_once' => array(
		/*
			@additional_params: notify_boards_once
				TOPICSUBJECT: The subject of the topic causing the notification
				TOPICLINK: A link to the topic.
				UNSUBSCRIBELINK: Link to unsubscribe from notifications.
			@description:
		*/
		'subject' => 'Nytt emne: {TOPICSUBJECT}',
		'body' => 'Et nytt innlegg, \'{TOPICSUBJECT}\', er kommet i en seksjon du har på din varslingsliste.

Du kan se det på
{TOPICLINK}

Flere emner lages gjerne, men du kommer ikke til å motta varsling på mer enn et av dem før du går tilbake til forumet og leser noen av dem.

Avslutt varsling av nye emner fra dette forumet ved å klikke på denne lenken:
{UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notify_boards_body' => array(
		/*
			@additional_params: notify_boards_body
				TOPICSUBJECT: The subject of the topic causing the notification
				TOPICLINK: A link to the topic.
				MESSAGE: This is the body of the message.
				UNSUBSCRIBELINK: Link to unsubscribe from notifications.
			@description:
		*/
		'subject' => 'Nytt emne: {TOPICSUBJECT}',
		'body' => 'Et nytt innlegg, \'{TOPICSUBJECT}\', er kommet i en seksjon du har på din varslingsliste.

Du kan se det på
{TOPICLINK}

Teksten i emnet er gjengitt nedenfor:
{MESSAGE}

Avslutt varsling av nye emner fra dette forumet ved å klikke på denne lenken:
{UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notify_boards' => array(
		/*
			@additional_params: notify_boards
				TOPICSUBJECT: The subject of the topic causing the notification
				TOPICLINK: A link to the topic.
				UNSUBSCRIBELINK: Link to unsubscribe from notifications.
			@description:
		*/
		'subject' => 'Nytt emne: {TOPICSUBJECT}',
		'body' => 'Et nytt innlegg, \'{TOPICSUBJECT}\', er kommet i en seksjon du har på din varslingsliste.

Du kan se det på
{TOPICLINK}

Avslutt varsling av nye emner fra dette forumet ved å klikke på denne lenken:
{UNSUBSCRIBELINK}

{REGARDS}',
	),
	'request_membership' => array(
		/*
			@additional_params: request_membership
				RECPNAME: The name of the person recieving the email
				APPYNAME: The name of the person applying for group membership
				GROUPNAME: The name of the group being applied to.
				REASON: The reason given by the applicant for wanting to join the group.
				MODLINK: Link to the group moderation page.
			@description:
		*/
		'subject' => 'Medlemskap i ny gruppe?',
		'body' => '{RECPNAME},

{APPYNAME} har bedt om medlemskap i gruppen "{GROUPNAME}". Brukeren har gitt følgende grunn:

{REASON}

Du kan godkjenne eller avvise denne søknaden ved å klikke på lenken nedenfor:

{MODLINK}

{REGARDS}',
	),
	'paid_subscription_reminder' => array(
		/*
			@additional_params: scheduled_approval
				REALNAME: The real (display) name of the person receiving the email.
				PROFILE_LINK: Link to profile of member receiving email where can renew.
				SUBSCRIPTION: Name of the subscription.
				END_DATE: Date it expires.
			@description:
		*/
		'subject' => 'Abonnement i ferd med å utløpe på {FORUMNAME}',
		'body' => '{REALNAME},

Et abonnement du abonnerer på {FORUMNAME} er i ferd med å utløpe. Dersom du valgte automatisk fornyelse når du startet abonnementet trenger du ikke gjør noe - ellers kan du vurdere å abonnere igjen. Detaljer er under:

Abonnement navn: {SUBSCRIPTION}
Utløper: {END_DATE}

Hvis du vil redigere dine abonnement gå til følgende nettadresse:
{PROFILE_LINK}

{REGARDS}',
	),
	'activate_reactivate' => array(
		/*
			@additional_params: activate_reactivate
				ACTIVATIONLINK:  The url link to reactivate the member's account.
				ACTIVATIONCODE:  The code needed to reactivate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
			@description:
		*/
		'subject' => 'Velkommen tilbake til {FORUMNAME}',
		'body' => 'For å godkjenne din e-postadresse på nytt, har din konto blitt deaktivert. Klikk følgende lenke for å aktivere den igjen:
{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'forgot_password' => array(
		/*
			@additional_params: forgot_password
				REALNAME: The real (display) name of the person receiving the reminder.
				REMINDLINK: The link to reset the password.
				IP: The IP address of the requester.
				MEMBERNAME:
			@description:
		*/
		'subject' => 'Nytt passord på {FORUMNAME}',
		'body' => 'Hei {REALNAME},
Denne e-posten ble sendt fordi funksjonen \'Glemt passordet\' ble benyttet for din medlemskonto. For å endre passord, klikk på denne lenken:
{REMINDLINK}

IP: {IP}
Brukernavn: {MEMBERNAME}

{REGARDS}',
	),
	'forgot_openid' => array(
		/*
			@additional_params: forgot_password
				REALNAME: The real (display) name of the person receiving the reminder.
				IP: The IP address of the requester.
				OPENID: The members OpenID identity.
			@description:
		*/
		'subject' => 'OpenID reminder for {FORUMNAME}',
		'body' => 'Dear {REALNAME},
Denne e-posten ble sendt fordi funksjonen \'glemt OpenID\' ble benyttet for din medlemskonto. Nedenfor er OpenID  hvor kontoen er knyttet til:
{OPENID}

IP: {IP}
Brukernavn: {MEMBERNAME}

{REGARDS}',
	),
	'scheduled_approval' => array(
		/*
			@additional_params: scheduled_approval
				REALNAME: The real (display) name of the person receiving the email.
				BODY: The generated body of the mail.
			@description:
		*/
		'subject' => 'Sammendrag av innlegg som venter på godkjenning fra {FORUMNAME}',
		'body' => '{REALNAME},

Denne e-posten inneholder et sammendrag av alle elementene som venter på godkjenning fra {FORUMNAME}.

{BODY}

Vennligst logg inn på forumet for å se på disse elementene.
{SCRIPTURL}

{REGARDS}',
	),
	'send_topic' => array(
		/*
			@additional_params: send_topic
				TOPICSUBJECT: The subject of the topic being sent.
				SENDERNAME: The name of the member sending the topic.
				RECPNAME: The name of the person receiving the email.
				TOPICLINK: A link to the topic being sent.
			@description:
		*/
		'subject' => 'Emne: {TOPICSUBJECT} (Fra: {SENDERNAME})',
		'body' => 'Hei {RECPNAME},
Jeg synes du bør ta en titt på "{TOPICSUBJECT}" på {FORUMNAME}.  For å lese det, klikk på denne lenken:

{TOPICLINK}

Takk,

{SENDERNAME}',
	),
	'send_topic_comment' => array(
		/*
			@additional_params: send_topic_comment
				TOPICSUBJECT: The subject of the topic being sent.
				SENDERNAME: The name of the member sending the topic.
				RECPNAME: The name of the person receiving the email.
				TOPICLINK: A link to the topic being sent.
				COMMENT: A comment left by the sender.
			@description:
		*/
		'subject' => 'Emne: {TOPICSUBJECT} (Fra: {SENDERNAME})',
		'body' => 'Hei {RECPNAME},
Jeg synes du bør ta en titt på "{TOPICSUBJECT}" på {FORUMNAME}.  For å lese det, klikk på denne lenken:

{TOPICLINK}

En kommentar har også blitt lagt til om dette emnet:
{COMMENT}

Takk,

{SENDERNAME}',
	),
	'send_email' => array(
		/*
			@additional_params: send_email
				EMAILSUBJECT: The subject the user wants to email.
				EMAILBODY: The body the user wants to email.
				SENDERNAME: The name of the member sending the email.
				RECPNAME: The name of the person receiving the email.
			@description:
		*/
		'subject' => '{EMAILSUBJECT}',
		'body' => '{EMAILBODY}',
	),
	'report_to_moderator' => array(
		/*
			@additional_params: report_to_moderator
				TOPICSUBJECT: The subject of the reported post.
				POSTERNAME: The report post's author's name.
				REPORTERNAME: The name of the person reporting the post.
				TOPICLINK: The url of the post that is being reported.
				REPORTLINK: The url of the moderation center report.
				COMMENT: The comment left by the reporter, hopefully to explain why they are reporting the post.
			@description: When a user reports a post this email is sent out to moderators and admins of that board.
		*/
		'subject' => 'Rapportert innlegg: {TOPICSUBJECT} av {POSTERNAME}',
		'body' => 'Følgende innlegg, "{TOPICSUBJECT}" av {POSTERNAME} har blitt rapportert av {REPORTERNAME} på et forum hvor du er modererator:

Emnet: {TOPICLINK}
Modereringssenter: {REPORTLINK}

Reporteren har gjort følgende kommentar:
{COMMENT}

{REGARDS}',
	),
	'change_password' => array(
		/*
			@additional_params: change_password
				USERNAME: The user name for the member receiving the email.
				PASSWORD: The password for the member.
			@description:
		*/
		'subject' => 'Nytt passord',
		'body' => 'Hei, {USERNAME}!

Dine innloggingsdata på {FORUMNAME} er forandret og ditt passord er blitt endret. Nedenfor er dine nye innloggingsdata.

Brukernavnet ditt er "{USERNAME}" og passordet ditt er "{PASSWORD}".

Du må gjerne endre det etter at du er logget inn ved å gå til profilen din, eller ved å gå til denne siden etter du er innlogget:
{SCRIPTURL}?action=profile

{REGARDS}',
	),
	'register_activate' => array(
		/*
			@additional_params: register_activate
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				PASSWORD: The password for the member.
				ACTIVATIONLINK:  The url link to reactivate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				ACTIVATIONCODE:  The code needed to reactivate the member's account.
				FORGOTPASSWORDLINK: The url to the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}. Hvis du har glemt passordet, kan du tilbakestille det ved å gå til {FORGOTPASSWORDLINK}.

Før du kan logge inn må du aktivere ditt medlemskap. For å gjøre dette, vennligst følg denne lenken:

{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'register_openid_activate' => array(
		/*
			@additional_params: register_activate
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				OPENID: The openID identity for the member.
				ACTIVATIONLINK:  The url link to reactivate the member's account.
				ACTIVATIONLINKWITHOUTCODE: The url to the page where the activation code can be entered.
				ACTIVATIONCODE:  The code needed to reactivate the member's account.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}. Du har valgt å godkjenne ved bruk av følgende OpenID identitet:
{OPENID}

Før du kan logge inn må du aktivere din konto. For å gjøre dette, vennligst følg denne lenken:

{ACTIVATIONLINK}

Skulle du få problemer med aktiveringen, kan du gå til {ACTIVATIONLINKWITHOUTCODE} og tast inn koden "{ACTIVATIONCODE}".

{REGARDS}',
	),
	'register_coppa' => array(
		/*
			@additional_params: register_coppa
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				PASSWORD: The password for the member.
				COPPALINK:  The url link to the coppa form.
				FORGOTPASSWORDLINK: The url to the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}. Hvis du har glemt passordet, kan du tilbakestille det ved å gå til {FORGOTPASSWORDLINK}.

Før du kan logge inn, kreves tillatelse fra dine foreldre/foresatte. Du kan få mer informasjon på lenken nedenfor:

{COPPALINK}

{REGARDS}',
	),
	'register_openid_coppa' => array(
		/*
			@additional_params: register_coppa
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				OPENID: The openID identity for the member.
				COPPALINK:  The url link to the coppa form.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}.

Du har valgt å godkjenne ved bruk av følgende OpenID identitet:
{OPENID}

Før du kan logge inn, kreves tillatelse fra dine foreldre/foresatte. Du kan få mer informasjon på lenken nedenfor:

{COPPALINK}

{REGARDS}',
	),
	'register_immediate' => array(
		/*
			@additional_params: register_immediate
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				PASSWORD: The password for the member.
				FORGOTPASSWORDLINK: The url to the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}. Hvis du har glemt passordet, kan du tilbakestille det ved å gå til {FORGOTPASSWORDLINK}.

{REGARDS}',
	),
	'register_openid_immediate' => array(
		/*
			@additional_params: register_immediate
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				OPENID: The openID identity for the member.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Du er nå registrert som bruker på {FORUMNAME}. Brukernavnet ditt er {USERNAME}.

Du har valgt å godkjenne ved bruk av følgende OpenID identitet:
{OPENID}

Du kan oppdatere profilen din ved å gå til denne siden etter du har logget inn:

{SCRIPTURL}?action=profile

{REGARDS}',
	),
	'register_pending' => array(
		/*
			@additional_params: register_pending
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				PASSWORD: The password for the member.
				FORGOTPASSWORDLINK: The url to the "forgot password" page.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Din forespørsel om å bli bruker på {FORUMNAME} er mottatt, {REALNAME}.

Brukernavnet du registrerte deg med er: {USERNAME}. Hvis du har glemt passordet, kan du endre det på {FORGOTPASSWORDLINK}.

Før du kan logge inn og begynne å bruke forumet, vil din forespørsel sjekkes og godkjennes manuelt.  Når dette skjer vil du motta en ny e-post fra denne adressen.

{REGARDS}',
	),
	'register_openid_pending' => array(
		/*
			@additional_params: register_pending
				REALNAME: The display name for the member receiving the email.
				USERNAME: The user name for the member receiving the email.
				OPENID: The openID identity for the member.
			@description:
		*/
		'subject' => 'Velkommen til {FORUMNAME}',
		'body' => 'Din forespørsel om å bli bruker på {FORUMNAME} er mottatt, {REALNAME}.

Brukernavnet du registrerte deg med er {USERNAME}.

Du har valgt å godkjenne ved bruk av følgende OpenID identitet:
{OPENID}

Før du kan logge inn og begynne å bruke forumet, vil din forespørsel sjekkes og godkjennes manuelt.  Når dette skjer vil du motta en ny e-post fra denne adressen.

{REGARDS}',
	),
	'notification_reply' => array(
		/*
			@additional_params: notification_reply
				TOPICSUBJECT:
				POSTERNAME:
				TOPICLINK:
				UNSUBSCRIBELINK:
			@description:
		*/
		'subject' => 'Svar på emne: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker har fått et nytt innlegg , skrevet av {POSTERNAME}.

Se innlegget her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_reply_body' => array(
		/*
			@additional_params: notification_reply_body
				TOPICSUBJECT:
				POSTERNAME:
				TOPICLINK:
				UNSUBSCRIBELINK:
				MESSAGE:
			@description:
		*/
		'subject' => 'Svar på emne: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker har fått et nytt innlegg , skrevet av {POSTERNAME}.

Se innlegget her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

Teksten i svaret er gjengitt nedenfor:
{MESSAGE}

{REGARDS}',
	),
	'notification_reply_once' => array(
		/*
			@additional_params: notification_reply_once
				TOPICSUBJECT:
				POSTERNAME:
				TOPICLINK:
				UNSUBSCRIBELINK:
			@description:
		*/
		'subject' => 'Svar på emne: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker har fått et nytt innlegg , skrevet av {POSTERNAME}.

Se innlegget her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

Flere svar kan komme, men du vil ikke få varsling om flere svar før du har lest emnet.

{REGARDS}',
	),
	'notification_reply_body_once' => array(
		/*
			@additional_params: notification_reply_body_once
				TOPICSUBJECT:
				POSTERNAME:
				TOPICLINK:
				UNSUBSCRIBELINK:
				MESSAGE:
			@description:
		*/
		'subject' => 'Svar på emne: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker har fått et nytt innlegg , skrevet av {POSTERNAME}.

Se innlegget her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

Teksten i svaret er gjengitt nedenfor:
{MESSAGE}

Flere svar kan komme, men du vil ikke få varsling om flere svar før du har lest emnet.

{REGARDS}',
	),
	'notification_sticky' => array(
		/*
			@additional_params: notification_sticky
			@description:
		*/
		'subject' => 'Emne satt som prioritert: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt satt som prioritet av {POSTERNAME}.

Se emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_lock' => array(
		/*
			@additional_params: notification_lock
			@description:
		*/
		'subject' => 'Emne stengt: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt stengt av {POSTERNAME}.

Se emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_unlock' => array(
		/*
			@additional_params: notification_unlock
			@description:
		*/
		'subject' => 'Emne åpnet: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt åpnet av {POSTERNAME}.

Se emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_remove' => array(
		/*
			@additional_params: notification_remove
			@description:
		*/
		'subject' => 'Emne fjernet: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt fjernet av {POSTERNAME}.

{REGARDS}',
	),
	'notification_move' => array(
		/*
			@additional_params: notification_move
			@description:
		*/
		'subject' => 'Emne flyttet: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt flyttet til en annet forum av {POSTERNAME}.

Se emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_merge' => array(
		/*
			@additional_params: notification_merged
			@description:
		*/
		'subject' => 'Emne slått sammen: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er slått sammen med et annet emne av {POSTERNAME}.

Se det nye sammenslåtte emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'notification_split' => array(
		/*
			@additional_params: notification_split
			@description:
		*/
		'subject' => 'Emne delt opp: {TOPICSUBJECT}',
		'body' => 'Et emne du overvåker er blitt delt opp i 2 eller flere nye emner av {POSTERNAME}.

Se det gjenstående emnet her: {TOPICLINK}

Stopp varsling til dette emnet ved å bruke lenken: {UNSUBSCRIBELINK}

{REGARDS}',
	),
	'admin_notify' => array(
		/*
			@additional_params: admin_notify
				USERNAME:
				PROFILELINK:
			@description:
		*/
		'subject' => 'Et nytt medlem har registrert seg',
		'body' => '{USERNAME} har akkurat registrert seg som et nytt medlem på ditt forum. Klikk lenken nedenunder for å se profilen til dette medlemmet.
{PROFILELINK}

{REGARDS}',
	),
	'admin_notify_approval' => array(
		/*
			@additional_params: admin_notify_approval
				USERNAME:
				PROFILELINK:
				APPROVALLINK:
			@description:
		*/
		'subject' => 'Et nytt medlem har registrert seg',
		'body' => '{USERNAME} har akkurat registrert seg som et nytt medlem på ditt forum. Klikk linken nedenunder for å se profilen til dette medlemmet.
{PROFILELINK}

Før dette medlemmet kan starte å skrive innlegg, må medlemmets konto være godkjent. Klikk linken nedenunder for å gå til godkjenning av medlemmer.
{APPROVALLINK}

{REGARDS}',
	),
	'admin_attachments_full' => array(
		/*
			@additional_params: admin_attachments_full
				REALNAME:
			@description:
		*/
		'subject' => 'Haster! Vedleggsmappen er nesten full',
		'body' => '{REALNAME},

Vedleggsmappen på {FORUMNAME} er nesten full. Gå til forumet for å løse dette problemet.

Når vedleggsmappen er full vil ikke brukerne kunne fortsette å legge til vedlegg eller laste opp egne avatarer (hvis aktivert).

{REGARDS}',
	),
	'paid_subscription_refund' => array(
		/*
			@additional_params: paid_subscription_refund
				NAME: Subscription title.
				REALNAME: Recipients name
				REFUNDUSER: Username who took out the subscription.
				REFUNDNAME: User's display name who took out the subscription.
				DATE: Today's date.
				PROFILELINK: Link to members profile.
			@description:
		*/
		'subject' => 'Refundert betalt abonnement',
		'body' => '{REALNAME},

Et medlem har mottatt en refusjon på et betalt abonnement. Nedenfor er detaljene i dette abonnement:

	Abonnement: {NAME}
	Brukernavn: {REFUNDNAME} ({REFUNDUSER})
	Dato: {DATE}

Du kan se dette medlemmets profil ved å klikke på lenken nedenfor:
{PROFILELINK}

{REGARDS}',
	),
	'paid_subscription_new' => array(
		/*
			@additional_params: paid_subscription_new
				NAME: Subscription title.
				REALNAME: Recipients name
				SUBEMAIL: Email address of the user who took out the subscription
				SUBUSER: Username who took out the subscription.
				SUBNAME: User's display name who took out the subscription.
				DATE: Today's date.
				PROFILELINK: Link to members profile.
			@description:
		*/
		'subject' => 'Nytt betalt abonnement',
		'body' => '{REALNAME},

Et medlem har kjøpt ett nytt abonnement. Nedenfor er detaljene for dette abonnementet:

	Abonnement: {NAME}
	Brukernavn: {SUBNAME} ({SUBUSER})
	E-postadresse: {SUBEMAIL}
	Pris: {PRICE}
	Dato: {DATE}

Du kan se dette medlemmets profil ved å klikke på lenken nedenfor:
{PROFILELINK}

{REGARDS}',
	),
	'paid_subscription_error' => array(
		/*
			@additional_params: paid_subscription_error
				ERROR: Error message.
				REALNAME: Recipients name
			@description:
		*/
		'subject' => 'Betalt abonnement, en feil har oppstått',
		'body' => '{REALNAME},

Følgende feil oppstod ved behandling av et betalt abonnement
------------------------------------------------------------
{ERROR}

{REGARDS}',
	),
);

/*
	@additional_params: happy_birthday
		REALNAME: The real (display) name of the person receiving the birthday message.
	@description: A message sent to members on their birthday.
*/
$birthdayEmails = array(
	'happy_birthday' => array(
		'subject' => 'Gratulerer med dagen fra {FORUMNAME}.',
		'body' => 'Kjære {REALNAME},

Vi på {FORUMNAME} ønsker å gratulere deg med dagen. Må denne dagen og det kommende år bli fyllt av glede.

{REGARDS}',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=2676">Thantos</a>',
	),
	'karlbenson1' => array(
		'subject' => 'På bursdagen din...',
		'body' => 'Vi kunne ha sendt deg et bursdagskort. Vi kunne ha sendt deg noen blomster eller en kake.

Men det gjør vi ikke!

Vi kunne også sendt deg en av de automatisk genererte meldinger for å gratulere deg med dagen!

Men det gjør vi ikke!

Vi skrev i stedet denne bursdagshilsen bare for deg.

Vi vil gjerne ønske deg en veldig spesiell bursdag.

{REGARDS}

//:: This message was automatically generated :://',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=63186">karlbenson</a>',
	),
	'nite0859' => array(
		'subject' => 'Gratulerer med dagen!',
		'body' => 'Dine venner på {FORUMNAME} ønsker å gratulere deg meg dagen, {REALNAME}. Hvis du ikke har gjort det nylig, vennligst besøk vår Forum sa de andre får muligheten til å ønske deg lykke til med dagen.

Selv om det er bursdagen din i dag, {REALNAME}, vi vil gjerne minne deg på at ditt nærvær på Forumet har vært den beste gaven til oss så langt.

De alle beste ønsker,
Dine venner på {FORUMNAME}',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=46625">nite0859</a>',
	),
	'zwaldowski' => array(
		'subject' => 'Gratulerer med dagen {REALNAME}',
		'body' => 'Kjære {REALNAME},

Nok et år i ditt liv har gått. Vi på {FORUMNAME} håper det har vært fylt med lykke, og ønsker deg lykke til i de kommende år.

{REGARDS}',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=72038">zwaldowski</a>',
	),
	'geezmo' => array(
		'subject' => 'Gratulerer med dagen, {REALNAME}!',
		'body' => 'Vet du hvem som har bursdag i dag, {REALNAME}?

Vi vet ... DEG!

Gratulerer med dagen!

Du er nå ett år eldre, men vi håper du er mye lykkeligere enn i fjor.

Nyt dagen din i dag, {REALNAME}!

- Fra dine venner på {FORUMNAME}',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=48671">geezmo</a>',
	),
	'karlbenson2' => array(
		'subject' => 'Gratulerer med dagen',
		'body' => 'Vi håper at bursdagen din er den beste noensinne skyet, sol eller uansett vær.
Spis bursdagskake og ha det moro, og fortell oss hva du har gjort.

Vi håper denne meldingen har gitt deg glede, og at den varer, til samme tid samme sted neste år.

{REGARDS}',
		'author' => '<a href="http://www.simplemachines.org/community/?action=profile;u=63186">karlbenson</a>',
	),
);

?>