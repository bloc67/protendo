<?php
// Version: 2.0; ManageSmileys

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['smiley_sets_save'] = 'Lagre endringer';
$txt['smiley_sets_add'] = 'Nytt smileysett';
$txt['smiley_sets_delete'] = 'Slett det valgte';
$txt['smiley_sets_confirm'] = 'Er du sikker på at du vil slette dette settet?\\n\\nMerk: Dette vil ikke slette bildene, bare valget.';

$txt['setting_smiley_sets_default'] = 'Standard smileysett';
$txt['setting_smiley_sets_enable'] = 'Aktiver at medlemmer kan velge smileyer.';
$txt['setting_smiley_enable'] = 'Aktiver egendefinerte smilefjes.';
$txt['setting_smileys_url'] = 'Basisadresse til alle smilefjes';
$txt['setting_smileys_dir'] = 'Basis-mappe for alle smileysett';
$txt['setting_messageIcons_enable'] = 'Aktiver egendefinerte innleggs-symboler';
$txt['setting_messageIcons_enable_note'] = '(ellers brukes standard innleggs-symbler .)';
$txt['groups_manage_smileys'] = 'Grupper har lov til å administrere smilefjes og meldings ikoner';

$txt['smiley_sets_name'] = 'Navn på settet';
$txt['smiley_sets_url'] = 'Adresse';
$txt['smiley_sets_default'] = 'Standard';

$txt['smiley_sets_latest'] = 'Smilefjes for øyeblikket';
$txt['smiley_sets_latest_fetch'] = 'Henter informasjon om smilefjes fra simplemachines.org...';

$txt['smileys_add_method'] = 'Bilde-kilde';
$txt['smileys_add_existing'] = 'Bruk eksisterende fil';
$txt['smileys_add_upload'] = 'Last opp nytt smilefjes';
$txt['smileys_add_upload_choose'] = 'Fil til å laste opp';
$txt['smileys_add_upload_choose_desc'] = 'Bilde som skal brukes for alle smilefjes-sett.';
$txt['smileys_add_upload_all'] = 'Samme bilde for alle sett';
$txt['smileys_add_upload_for1'] = 'Bilde for';
$txt['smileys_add_upload_for2'] = 'settet';

$txt['smileys_enable_note'] = '(ellers vil standard smilefjes bli brukt.)';
$txt['smileys_code'] = 'Kode';
$txt['smileys_filename'] = 'Filnavn';
$txt['smileys_description'] = 'Verktøytips eller beskrivelse';
$txt['smileys_remove'] = 'Slett';
$txt['smileys_save'] = 'Lagre endringer';
$txt['smileys_delete'] = 'Slett smilefjes';
// Don't use entities in the below string.
$txt['smileys_delete_confirm'] = 'Er du sikker på at du ønsker å slette dette smilefjeset?';
$txt['smileys_with_selected'] = 'Med valgte';
$txt['smileys_make_hidden'] = 'Skjul';
$txt['smileys_show_on_post'] = 'Vis i innleggs-skjema';
$txt['smileys_show_on_popup'] = 'Vis i popup';

$txt['smiley_settings_explain'] = 'Disse innstillingene lar deg forandre standard smileysett, velge om medlemmer skal få velge sine egne smilefjes, samt endre stier og innstillinger.';
$txt['smiley_editsets_explain'] = 'Smileysett er grupper av smilefjes som dine brukere kan velge å mellom. For eksempel kunne du hatt gule og røde smilefjes.<br />Her kan du endre navn og plassering for hvert smileysett. Husk imidlertid at alle sett deler samme smilefjes.';
$txt['smiley_editsmileys_explain'] = 'Skift ut dine smilefjes her ved å klikke på det du vil forandre. Husk at alle alle smilefjes må være tilstede i alle settene, ellers vil noen av dem være borte! Husk å lagre etter du har redigert ferdig!';
$txt['smiley_setorder_explain'] = 'Endre rekkefølgen på smilefjesene her.';
$txt['smiley_addsmiley_explain'] = 'Her kan du legge til et nytt smilefjes - enten fra en allerede eksisterende fil eller ved å laste opp en ny.';

$txt['smiley_set_select_default'] = 'Standard smileysett';
$txt['smiley_set_new'] = 'Lag et nytt smileysett';
$txt['smiley_set_modify_existing'] = 'Rediger eksisterende smileysett';
$txt['smiley_set_modify'] = 'Rediger';
$txt['smiley_set_import_directory'] = 'Importer smilefjes som allerede finnes i denne mappen';
$txt['smiley_set_import_single'] = 'Det er et smilefjes i dette smil-settet som ennå ikke er importert. Klikk';
$txt['smiley_set_import_multiple'] = 'Det er %1$d smilefjes i denne mappen som ennå ikke er importert. Klikk';
$txt['smiley_set_to_import_single'] = 'for å importere det nå.';
$txt['smiley_set_to_import_multiple'] = 'for å importere disse nå.';

$txt['smileys_location'] = 'Plassering';
$txt['smileys_location_form'] = 'Skjema for plassering';
$txt['smileys_location_hidden'] = 'Skjult';
$txt['smileys_location_popup'] = 'Vis i popup';
$txt['smileys_modify'] = 'Rediger';
$txt['smileys_not_found_in_set'] = 'Smilefjes ble ikke funnet i sett';
$txt['smileys_default_description'] = '(Sett inn en beskrivelse)';
$txt['smiley_new'] = 'Legg til nytt smilefjes';
$txt['smiley_modify_existing'] = 'Rediger smilefjes';
$txt['smiley_preview'] = 'Forhåndsvisning';
$txt['smiley_preview_using'] = 'med smileysettet';
$txt['smileys_confirm'] = 'Er du sikker på at du vil fjerne disse smilefjesene?\\n\\nMerk: Dette vil ikke fjerne bildene, bare valgene.';
$txt['smileys_location_form_description'] = 'Disse smilefjesene vil dukke opp ovenfor tekstområdet når det skrives et nytt innlegg på forumet eller en ny personlig melding.';
$txt['smileys_location_popup_description'] = 'Disse smilefjesene vil bli vist i et popup-vindu når brukeren trykker [fler]';
$txt['smileys_move_select_destination'] = 'Velg hvor smilefjes skal plasseres';
$txt['smileys_move_select_smiley'] = 'Velg hvilket smilefjes som skal flyttes';
$txt['smileys_move_here'] = 'Flytt smilefjeset hit';
$txt['smileys_no_entries'] = 'Det er for tiden ingen smilefjes konfigurert.';

$txt['icons_edit_icons_explain'] = 'Fra her kan du endre hvilke innleggsikon som er tilgjengelige på ditt forum. Du kan legge til, endre eller slette ikoner, samt begrense dem til valgte fora.';
$txt['icons_edit_icons_all_boards'] = 'Tilgjengelig i alle fora';
$txt['icons_board'] = 'Forum';
$txt['icons_confirm'] = 'Er du sikker på at du vil slette disse ikonene?\\n\\nMerk at dette vil kun nekte bruk av disse i nye innlegg, bildene blir ikke slette.';
$txt['icons_add_new'] = 'Legg til nytt ikon';

$txt['icons_edit_icon'] = 'Endre innleggsikon';
$txt['icons_new_icon'] = 'Nytt ikon';
$txt['icons_location_first_icon'] = 'Som første ikon';
$txt['icons_location_after'] = 'Etter';
$txt['icons_filename_all_gif'] = 'Alle filene må være i gif-format';
$txt['icons_no_entries'] = 'Det er for tiden ingen innleggs ikoner konfigurert.';

?>