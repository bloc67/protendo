<?php
// Version: 2.0; ManageSettings

global $scripturl;

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['modSettings_desc'] = 'Denne siden lar deg endre innstillingene for funksjoner, modifikasjoner, og grunnleggende alternativer i forumet ditt. Se <a href="' . $scripturl . '?action=admin;area=theme;sa=settings;th=%1$s;%3$s=%2$s">Tema innstillinger</a> for flere alternativer.  Klikk hjelpeikonene for mer informasjon om en innstilling.';
$txt['security_settings_desc'] = 'Denne siden lar deg angi alternativer spesifikt relatert til sikkerhet og moderering av forumet, inkludert anti-spam alternativer.';
$txt['modification_settings_desc'] = 'Denne siden inneholder innstillinger lagt til av andre modifikasjoner på ditt forum';

$txt['modification_no_misc_settings'] = 'Det er ennå ikke installert modifikasjoner som har lagt til innstillinger til dette området.';

$txt['pollMode'] = 'Avstemninger';
$txt['disable_polls'] = 'Deaktiver avstemninger';
$txt['enable_polls'] = 'Aktiver avstemninger';
$txt['polls_as_topics'] = 'Vis avstemninger som emner';
$txt['allow_guestAccess'] = 'Tillat at gjester å bla gjennom forumene';
$txt['userLanguage'] = 'La medlemmene få velge språk';
$txt['allow_editDisplayName'] = 'La medlemmene få redigere visningsnavn';
$txt['allow_hideOnline'] = 'La ikke-administratorer få skjule sin online-status';
$txt['guest_hideContacts'] = 'La medlemmene få skjule sin e-postadresse til gjester';
$txt['titlesEnable'] = 'Aktivere egendefinerte titler';
$txt['enable_buddylist'] = 'Aktiver venner / ignorerelisten';
$txt['default_personal_text'] = 'Standard personlig tekst';
$txt['number_format'] = 'Standard tallformat';
$txt['time_format'] = 'Standard tidsformat';
$txt['setting_time_offset'] = 'Generelt tidsavvik i forhold til serverens klokke <div class="smalltext">(legges til hvert medlems egne tidsavvik-innstilling.)</div>';
$txt['setting_default_timezone'] = 'Tidssone server';
$txt['failed_login_threshold'] = 'Terskel for mislykkede innlogginger';
$txt['lastActive'] = 'Terskel for pålogget tid';
$txt['trackStats'] = 'Loggfør daglig statistikk';
$txt['hitStats'] = 'Loggfør daglige sidetreff (statistikk må være aktivert)';
$txt['enableCompressedOutput'] = 'Aktiver kompriming av utdata';
$txt['disableTemplateEval'] = 'Deaktiver evaluering av maler';
$txt['databaseSession_enable'] = 'Bruk databasen for å lagre sesjoner';
$txt['databaseSession_loose'] = 'La nettlesere gå tilbake til sider i sitt lokale mellomlager';
$txt['databaseSession_lifetime'] = 'Sekunder før en ubrukt sesjon avsluttes';
$txt['enableErrorLogging'] = 'Aktiver loggføring av feil';
$txt['enableErrorQueryLogging'] = 'Inkluder databasespørring i feilloggen';
$txt['pruningOptions'] = 'Aktiver opprydding i loggoppføringer';
$txt['pruneErrorLog'] = 'Fjern feilloggoppføringer eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['pruneModLog'] = 'Fjern loggoppføringer over modereringer eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['pruneBanLog'] = 'Fjern loggoppføringer over utestengelser eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['pruneReportLog'] = 'Fjern loggoppføringene over moderatorrapporter eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['pruneScheduledTaskLog'] = 'Fjern loggoppføringene over planlagt oppgave eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['pruneSpiderHitLog'] = 'Fjern loggoppføringene over søkemotortreff eldre enn<div class="smalltext">(0 for å deaktivere)</div>';
$txt['cookieTime'] = 'Standard varighet på innloggingscookie (i minutter)';
$txt['localCookies'] = 'Aktiver lokal lagring av informasjonskapsler<div class="smalltext">(SSI vil ikke fungere skikkelig med dette aktivert.)</div>';
$txt['globalCookies'] = 'Benytt informasjonskapsler som er uavhengige av subdomener<div class="smalltext">(deaktiver lokal lagring av informasjonskapsler først!)</div>';
$txt['secureCookies'] = 'Tving cookies til å være sikree<div class="smalltext">(Dette gjelder bare hvis du bruker HTTPS - ikke bruk ellers!)</div>';
$txt['securityDisable'] = 'Deaktiver ekstra sikkerhet for administrasjon';
$txt['send_validation_onChange'] = 'Krev reaktivering etter endring av e-postadresse';
$txt['approveAccountDeletion'] = 'Krev godkjenning fra administrator når et medlem sletter sin konto';
$txt['autoOptMaxOnline'] = 'Maks antall brukere pålogget ved optimalisering<div class="smalltext">(0 for ubegrenset.)</div>';
$txt['autoFixDatabase'] = 'Reparer automatisk ødelagte tabeller';
$txt['allow_disableAnnounce'] = 'La brukere reservere seg mot kunngjøringer';
$txt['disallow_sendBody'] = 'Ikke tillat innleggstekst i varslinger';
$txt['queryless_urls'] = 'Bruk søkemotor-vennlige webadresser<div class="smalltext"><strong>Fungerer kun på Apache/Lighttpd-servere!</strong></div>';
$txt['max_image_width'] = 'Maks. bredde på opplastede bilder (0 = ubegrenset)';
$txt['max_image_height'] = 'Maks. høyde på opplastede bilder (0 = ubegrenset)';
$txt['enableReportPM'] = 'Tillat rapportering av personlige meldinger';
$txt['max_pm_recipients'] = 'Maksimalt antall mottakere tillatt til en personlig melding.<div class="smalltext">(0 for ingen grense, administrator er fritatt fra regel)</div>';
$txt['pm_posts_verification'] = 'Under dette antall innlegg må brukere skrive inn koden når de sender personlige meldinger.<div class="smalltext">(0 for ingen grense, administrator er fritatt fra regel)</div>';
$txt['pm_posts_per_hour'] = 'Antall personlige meldinger som en bruker kan sende i løpet av 1 time.<div class="smalltext">(0 for ingen grense, moderatorer er unntatt)</div>';
$txt['compactTopicPagesEnable'] = 'Begrens antall lenker for sidetall';
$txt['contiguous_page_display'] = 'Antall tilgrensende sider å vise';
$txt['to_display'] = 'til å vises';
$txt['todayMod'] = 'Aktiver forkortet datovisning';
$txt['today_disabled'] = 'Deaktivert';
$txt['today_only'] = 'Bare i dag';
$txt['yesterday_today'] = 'I dag &amp; I går';
$txt['topbottomEnable'] = 'Vis knapper for Til toppen/Til bunnen';
$txt['onlineEnable'] = 'Vise innlogget/ikke innlogget i innlegg og PM\'er';
$txt['enableVBStyleLogin'] = 'Vis hurtiginnlogging på alle sider';
$txt['defaultMaxMembers'] = 'Medlemmer per side i medlemslista';
$txt['timeLoadPageEnable'] = 'Vise tiden det tok å generere hver side';
$txt['disableHostnameLookup'] = 'Deaktiver søk etter vertsnavn';
$txt['who_enabled'] = 'Aktiver liste over Hvem er pålogget';
$txt['make_email_viewable'] = 'Tillat visning av e-postadresser';
$txt['meta_keywords'] = 'Meta søkeord assosiert med forumet<div class="smalltext">For søkemotorer. La stå tomt for standard.</div>';

$txt['karmaMode'] = 'Karma-modus';
$txt['karma_options'] = 'Deaktiver karma|Aktiver total karma|Aktiver positiv/negativ karma';
$txt['karmaMinPosts'] = 'Antall innlegg som trengs for å endre karma';
$txt['karmaWaitTime'] = 'Ventetid i timer';
$txt['karmaTimeRestrictAdmins'] = 'Bruk ventetid også for administratorer';
$txt['karmaLabel'] = 'Merkelapp for karma';
$txt['karmaApplaudLabel'] = 'Etikett for positiv karma';
$txt['karmaSmiteLabel'] = 'Etikett for negativ karma';

$txt['caching_information'] = '<div class="aligncenter underline"><strong>Viktig! Les dette FØR du aktiverer disse funksjonene.</strong></div><br />
	Protendo støtter mellomlagring gjennom bruk av akseleratorer. For øyeblikket støttes følgende akseleratorer:<br />
	<ul class="normallist">
		<li>APC</li>
		<li>eAccelerator</li>
		<li>Turck MMCache</li>
		<li>Memcached</li>
		<li>Zend Platform/Performance Suite (Ikke Zend Optimizer)</li>
		<li>XCache</li>
	</ul>
	Mellomlagring fungerer best hvis du har PHP kompilert med en av de ovennevnte akseleratorene, eller har memcache
	tilgjengelig. Hvis du ikke har noen installert, vil Protendo utføre fil hurtigbufring.<br /><br />
	Protendo utfører mellomlagring på en rekke nivåer. Jo høyere nivået av
	 mellomlagring, jo mer CPU-tid vil bli brukt for å hente bufret informasjon. Hvis hurtigbufring er tilgjengelig på maskinen din, anbefales det at du prøver mellomlagring på nivå 1 først.
	<br /><br />
	Merk at hvis du bruker memcached må du oppgi serverdetaljene i innstillingen nedenfor. Dette må legges inn som en kommaseparert liste
	som vist i eksempelet nedenfor:<br />
	&quot;server1,server2,server3:port,server4&quot;<br /><br />
	Merk at dersom ingen port er angitt vil Protendo bruke port 11211. Protendo vil forsøke å utføre enkel/tilfeldig lastbalansering over serveren.
	<br /><br />
	%1$s';

$txt['detected_no_caching'] = '<strong class="alert">Protendo kunne ikke finne noen av de kompatible aksleratorene på serveren.</strong>';
$txt['detected_APC'] = '<strong style="color: green">Protendo har oppdaget at du har APC installert.</strong>';
$txt['detected_eAccelerator'] = '<strong style="color: green">Protendo har oppdaget at du har eAccelerator installert.</strong>';
$txt['detected_MMCache'] = '<strong style="color: green">Protendo har oppdaget at du har MMCache installert.</strong>';
$txt['detected_Zend'] = '<strong style="color: green">Protendo har oppdaget at du har Zend installert.</strong>';
$txt['detected_Memcached'] = '<strong style="color: green">Protendo har oppdaget at din server har Memcached installert.</strong> ';
$txt['detected_XCache'] = '<strong style="color: green">Protendo har oppdaget at du har XCache installert.</strong>';

$txt['cache_enable'] = 'Minnelagringsnivå';
$txt['cache_off'] = 'Deaktivert';
$txt['cache_level1'] = 'Nivå 1 minnelagring (Anbefalt)';
$txt['cache_level2'] = 'Nivå 2 minnelagring';
$txt['cache_level3'] = 'Nivå 3 minnelagring (Ikke anbefalt)';
$txt['cache_memcached'] = 'Innstillinger for Memcache';

$txt['loadavg_warning'] = '<span class="error">Merk: Innstillingene under skal redigeres med forsiktighet. Settes noen av de for lavt kan dette gjøre ditt forum <strong>ubrukelig</strong>! Nåværende belastningsgjennomsnitt er <strong>%01.2f</strong></span>';
$txt['loadavg_enable'] = 'Aktiver lastbalanse av gjennomsnittlig belastning';
$txt['loadavg_auto_opt'] = 'Terskel for å deaktivere automatisk database optimalisering';
$txt['loadavg_search'] = 'Terskel for å deaktivere søke';
$txt['loadavg_allunread'] = 'Terskel for å deaktivere alle uleste emner';
$txt['loadavg_unreadreplies'] = 'Terskel for å deaktivere uleste svar';
$txt['loadavg_show_posts'] = 'Terskel for å deaktivere visning av brukerens innlegg';
$txt['loadavg_forum'] = 'Terskel for å deaktivere forumet <strong>fullstendig</strong>';
$txt['loadavg_disabled_windows'] = '<span class="error">Lastbalansering er ikke tilgjengelig på Windows.</span>';
$txt['loadavg_disabled_conf'] = '<span class="error">Lastbalansering er deaktivert av din leverandørs konfigurasjon.</span>';

$txt['setting_password_strength'] = 'Påkrevd styrke for brukernes passord';
$txt['setting_password_strength_low'] = 'Lav - 4 tegn minimum';
$txt['setting_password_strength_medium'] = 'Medium - kan ikke inneholde brukernavn';
$txt['setting_password_strength_high'] = 'Høy - må inneholde varierte tegn';

$txt['antispam_Settings'] = 'Antispam verifisering ';
$txt['antispam_Settings_desc'] = 'Denne delen lar deg sette opp verifiseringskontroller for å sikre brukeren er et menneske (og ikke en bot), og modifisere hvordan og hvor disse gjelder.';
$txt['setting_reg_verification'] = 'Krev verifikasjon på registreringssiden';
$txt['posts_require_captcha'] = 'Under dette antall innlegg må brukere skrive inn koden for å legge til et innlegg';
$txt['posts_require_captcha_desc'] = '(0 for ingen grense, moderatorer er unntatt)';
$txt['search_enable_captcha'] = 'Krever verifikasjon for at gjester skal kunne søke';
$txt['setting_guests_require_captcha'] = 'Gjestene må passere verifiseringen for å legge til et innlegg';
$txt['setting_guests_require_captcha_desc'] = '(Automatisk hvis du angir et minimumsantall for innlegg nedenunder)';
$txt['guests_report_require_captcha'] = 'Gjestene må passere verifiseringen ved rapportering av innlegg';

$txt['configure_verification_means'] = 'Konfigurer verifiseringsmetoder';
$txt['setting_qa_verification_number'] = 'Antall verifiseringensspørsmål brukeren må svare på';
$txt['setting_qa_verification_number_desc'] = '(0 for å deaktivere; spørsmål(ene) skrives under)';
$txt['configure_verification_means_desc'] = '<span class="smalltext">Nedenfor kan du angi hvilke antispamfunksjoner du ønsker å ha aktivert når en bruker trenger å verifisere at de er et menneske. Merk at brukeren må passere <em>alle</em> verifiseringene. Dersom du aktiverer både verifisering med bilde og spørsmål/svar, så må brukeren fullføre begge for å fortsette.</span>';
$txt['setting_visual_verification_type'] = 'Visualt verifiseringsbilde å vise';
$txt['setting_visual_verification_type_desc'] = 'Jo mer kompleks bildet er, jo vanskeligere er det for en bot å komme videre';
$txt['setting_image_verification_off'] = 'Slått av';
$txt['setting_image_verification_vsimple'] = 'Meget enkel - Ren tekst på bilde';
$txt['setting_image_verification_simple'] = 'Enkelt - Overlappende fargede bokstaver, ingen støy';
$txt['setting_image_verification_medium'] = 'Medium - Overlappende fargede bokstaver, med støy/linjer';
$txt['setting_image_verification_high'] = 'Høy - Vinklede bokstaver betydelig støy/linjer';
$txt['setting_image_verification_extreme'] = 'Ekstrem - Vinklede bokstaver, støy, linjer og blokker';
$txt['setting_image_verification_sample'] = 'Eksempel';
$txt['setting_image_verification_nogd'] = '<strong>Merk:</strong> at denne server ikke har GD funksjonen innstallert, og dermed vil de forskjellige nivåer av kompleksitet ikke bety noe.';
$txt['setup_verification_questions'] = 'Verifiseringsspørsmål';
$txt['setup_verification_questions_desc'] = '<span class="smalltext">Hvis du vil at brukerne skal svare på verifiseringsspørsmål for å stoppe spamroboter, bør du sette opp en rekke spørsmål i tabellen nedenfor. Du bør plukke relativt enkle spørsmål, svaret er ikke avhengig av korrekte store og små bokstaver. Du kan bruke BB koder i spørsmålene for formatering. For å fjerne et spørsmål, bare slette innholdet i den linjen.</span>';
$txt['setup_verification_question'] = 'Spørgsmål';
$txt['setup_verification_answer'] = 'Svar';
$txt['setup_verification_add_more'] = 'Legg til spørsmål';

$txt['moderation_settings'] = 'Innstillinger for moderering';
$txt['setting_warning_enable'] = 'Aktiver brukeradvarselssystem';
$txt['setting_warning_watch'] = 'Advarselsnivå for når holde øye med bruker<div class="smalltext">Nivå for når en skal begynne å holdes øye med brukeren - 0 for å deaktivere.</div>';
$txt['setting_warning_moderate'] = 'Advarselsnivå for innleggsmoderering<div class="smalltext">Nivå for når en bruker skal få alle innlegg moderert - 0 for å deaktivere.</div>';
$txt['setting_warning_mute'] = 'Advarselsnivå for å sperre bruker<div class="smalltext">Nivå for når en bruker ikke kan legge inn innlegg - 0 for å deaktivere.</div>';
$txt['setting_user_limit'] = 'Maksimum antall advarselspoeng per dag<div class="smalltext">Denne verdien er den maksimale antall advarselspoeng en enkelt moderator kan tildele til en bruker i en 24 timers periode - 0 for ingen grense.</div>';
$txt['setting_warning_decrement'] = 'Advarselspoeng å trekke fra på brukeren hver 24. time<div class="smalltext">Gjelder kun for brukere som ikke har blitt advart i løpet av de siste 24 timer - sett til 0 for å deaktivere.</div>';
$txt['setting_warning_show'] = 'Brukere som kan se advarselsstatus<div class="smalltext">Bestemmer hvem som kan se advarselsnivået på brukere i forumet.</div>';
$txt['setting_warning_show_mods'] = 'Bare moderatorer';
$txt['setting_warning_show_user'] = 'Moderatorer og advarte brukere';
$txt['setting_warning_show_all'] = 'Alle brukere';

$txt['signature_settings'] = 'Signaturinnstillinger';
$txt['signature_settings_desc'] = 'Bruk innstillingene på denne siden for å bestemme hvordan medlemmenes signaturer skal behandles i Protendo.';
$txt['signature_settings_warning'] = 'Merk at innstillingene ikke endres på eksisterende signaturer som standard. Klikk <a href="' . $scripturl . '?action=admin;area=featuresettings;sa=sig;apply;%2$s=%1$s">her</a> for å bruke reglene på alle eksisterende signaturer .';
$txt['signature_enable'] = 'Aktiver signaturer';
$txt['signature_max_length'] = 'Maksimalt antall tegn tillatt<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_max_lines'] = 'Maksimalt antall linjer<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_max_images'] = 'Maksimalt antall bilder<div class="smalltext">(0 for ubegrenset - unntatt smilefjes)</div>';
$txt['signature_allow_smileys'] = 'Tillat smilefjes i signaturer';
$txt['signature_max_smileys'] = 'Maksimalt antall smilefjes<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_max_image_width'] = 'Maksimal bredde på signaturbilder (piksler)<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_max_image_height'] = 'Maksimal høyde på signaturbilder (piksler)<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_max_font_size'] = 'Maksimal skriftstørrelse tillatt i signaturer<div class="smalltext">(0 for ubegrenset)</div>';
$txt['signature_bbc'] = 'Aktiverte BB-koder';

$txt['custom_profile_title'] = 'Egendefinerte profilfelt';
$txt['custom_profile_desc'] = 'Fra denne siden kan du opprette dine egen profilfelter som passer inn med dine egne krav';
$txt['custom_profile_active'] = 'Aktiv';
$txt['custom_profile_fieldname'] = 'Feltnavn';
$txt['custom_profile_fieldtype'] = 'Felttype';
$txt['custom_profile_make_new'] = 'Nytt felt';
$txt['custom_profile_none'] = 'Du har ikke opprettet noen egendefinert profilfelter ennå!';
$txt['custom_profile_icon'] = 'Ikon';

$txt['custom_profile_type_text'] = 'Tekst';
$txt['custom_profile_type_textarea'] = 'Stor bokstaver';
$txt['custom_profile_type_select'] = 'Nedtrekksliste';
$txt['custom_profile_type_radio'] = 'Radioknapp';
$txt['custom_profile_type_check'] = 'Avkryssingsboks';

$txt['custom_add_title'] = 'Legg til felt';
$txt['custom_edit_title'] = 'Rediger felt';
$txt['custom_edit_general'] = 'Visningsinnstillinger';
$txt['custom_edit_input'] = 'Inndata innstillinger';
$txt['custom_edit_advanced'] = 'Avanserte innstillinger';
$txt['custom_edit_name'] = 'Navn';
$txt['custom_edit_desc'] = 'Beskrivelse';
$txt['custom_edit_profile'] = 'Profilseksjon';
$txt['custom_edit_profile_desc'] = 'Delen av profil dette er redigert i.';
$txt['custom_edit_profile_none'] = 'Ingen';
$txt['custom_edit_registration'] = 'Vis på registrering';
$txt['custom_edit_registration_disable'] = 'Nei';
$txt['custom_edit_registration_allow'] = 'Ja';
$txt['custom_edit_registration_require'] = 'Ja, og krever oppføring';
$txt['custom_edit_display'] = 'Vis på emnevisning';
$txt['custom_edit_picktype'] = 'Felttype';

$txt['custom_edit_max_length'] = 'Maksimum lengde';
$txt['custom_edit_max_length_desc'] = '(0 for ingen grense)';
$txt['custom_edit_dimension'] = 'Dimensjoner';
$txt['custom_edit_dimension_row'] = 'Rader';
$txt['custom_edit_dimension_col'] = 'Kolonner';
$txt['custom_edit_bbc'] = 'Tillat BBC';
$txt['custom_edit_options'] = 'Alternativer';
$txt['custom_edit_options_desc'] = 'La alternativboksen være tom for å fjerne. Valgte boks er standardvalget.';
$txt['custom_edit_options_more'] = 'Mer';
$txt['custom_edit_default'] = 'Standard status';
$txt['custom_edit_active'] = 'Aktiv';
$txt['custom_edit_active_desc'] = 'Hvis ikke valgt vil ikke dette feltet bli vist til noen.';
$txt['custom_edit_privacy'] = 'Hvem';
$txt['custom_edit_privacy_desc'] = 'Hvem kan se og redigere dette feltet';
$txt['custom_edit_privacy_all'] = 'Brukere kan se dette feltet, eieren kan redigere det';
$txt['custom_edit_privacy_see'] = 'Brukere kan se dette feltet, bare administratorer kan redigere det';
$txt['custom_edit_privacy_owner'] = 'Brukere kan ikke se dette feltet, eier og administratorer kan redigere det';
$txt['custom_edit_privacy_none'] = 'Dette feltet er kun synlig for administratorer';
$txt['custom_edit_can_search'] = 'Søkbar';
$txt['custom_edit_can_search_desc'] = 'Kan dette feltet søkes på fra medlemslisten';
$txt['custom_edit_mask'] = 'Inndata format';
$txt['custom_edit_mask_desc'] = 'For tekstfelt kan en inndatamaske velges for å validere data.';
$txt['custom_edit_mask_email'] = 'Gyldig e-postadresse';
$txt['custom_edit_mask_number'] = 'Numerisk';
$txt['custom_edit_mask_nohtml'] = 'Ingen HTML';
$txt['custom_edit_mask_regex'] = 'Regex (Avansert)';
$txt['custom_edit_enclose'] = 'Vis inndataformat i teksten (valgfritt)';
$txt['custom_edit_enclose_desc'] = 'Vi anbefaler <strong>sterkt</strong> å bruke en inndatamaske for å validere hva brukerne skriver.';

$txt['custom_edit_placement'] = 'Velg plassering';
$txt['custom_edit_placement_standard'] = 'Standard (med tittel)';
$txt['custom_edit_placement_withicons'] = 'Med ikoner';
$txt['custom_edit_placement_abovesignature'] = 'Over signaturen';
$txt['custom_profile_placement'] = 'plassering';
$txt['custom_profile_placement_standard'] = 'Standard';
$txt['custom_profile_placement_withicons'] = 'Med ikoner';
$txt['custom_profile_placement_abovesignature'] = 'Over signaturen';

// Use numeric entities in the string below!
$txt['custom_edit_delete_sure'] = 'Er du sikker på at du vil slette dette feltet - alle relaterte brukerdata vil gå tapt!';

$txt['standard_profile_title'] = 'Standard profilfelt';
$txt['standard_profile_field'] = 'Felt';

$txt['core_settings_welcome_msg'] = 'Velkommen til din nye Forum';
$txt['core_settings_welcome_msg_desc'] = 'For å komme i gang foreslår vi at du velger hvilke av Protendos kjernefunksjoner du vil aktivere. Vi anbefaler at du kun aktiverer de funksjonene du trenger!';
$txt['core_settings_item_cd'] = 'Kalender';
$txt['core_settings_item_cd_desc'] = 'Aktivering av denne funksjonen vil åpne opp et utvalg av alternativer som gjør det mulig for dine brukere å vise kalender, legge til og vise hendelser, se brukernes fødselsdato på en kalender og mye, mye mer.';
$txt['core_settings_item_cp'] = 'Avansert profilfelt';
$txt['core_settings_item_cp_desc'] = 'Dette gir deg mulighet til å skjule standard profilfelt, legge til felter til registrering og opprette ny profilfelter for forumet ditt.';
$txt['core_settings_item_k'] = 'Karma';
$txt['core_settings_item_k_desc'] = 'Karma er en funksjon som viser populariteten til et medlem. Medlemmer, hvis tillatt, kan \'berømme\' eller \'straffe\'  øvrige medlemmer, som er hvordan deres popularitet blir beregnet';
$txt['core_settings_item_ml'] = 'Moderering, Administrasjon og brukerlogger';
$txt['core_settings_item_ml_desc'] = 'Aktiverer moderator og administrator logger for å vise hva som er gjort av alle de viktigste handlingene på forumet. Det gjør også at moderatorene kan se de viktige endringer brukeren gjør på profilen sin.';
$txt['core_settings_item_pm'] = 'Innleggsmoderering';
$txt['core_settings_item_pm_desc'] = 'Innleggsmoderering gjør at du kan velge grupper og forum hvor innlegg må godkjennes før de blir offentlige. Før du aktivere denne funksjonen må du passe på sette de aktuelle rettighetene i seksjonen for rettigheter.';
$txt['core_settings_item_ps'] = 'Betalte abonnementer';
$txt['core_settings_item_ps_desc'] = 'Betalte abonnement tillater brukere å betale for abonnementer på forumet som endrer medlemsgruppe på forumet og dermed endrer sine rettigheter.';
$txt['core_settings_item_rg'] = 'Rapportgenerering';
$txt['core_settings_item_rg_desc'] = 'Denne administratorfunksjonen tillater generering av rapporter (som kan skrives ut) for å presentere din nåværende forumsoppsett på en enkel måte - spesielt nyttig for store fora.';
$txt['core_settings_item_sp'] = 'Søkemotorsporing';
$txt['core_settings_item_sp_desc'] = 'Aktivering av denne funksjonen lar administratorer spore søkemotorer når de indekserer forumet.';
$txt['core_settings_item_w'] = 'Advarselssystem';
$txt['core_settings_item_w_desc'] = 'Denne funksjonaliteten gjør det mulig for administratorer og moderatorer å utstede advarsler til brukerne, det inkluderer også avansert funksjonalitet for automatisk fjerning av brukerrettigheter etter som advarselsnivået øker. Merk: For å dra full nytte av denne funksjonen bør funksjonen &quot;Innleggsmoderering&quot; være aktivert.';
$txt['core_settings_switch_on'] = 'Klikk for å aktivere';
$txt['core_settings_switch_off'] = 'Klikk for å deaktivere';
$txt['core_settings_enabled'] = 'Aktivert';
$txt['core_settings_disabled'] = 'Deaktivert';

$txt['languages_lang_name'] = 'Språknavn';
$txt['languages_locale'] = 'Språk';
$txt['languages_default'] = 'Standard';
$txt['languages_character_set'] = 'Tegnsett';
$txt['languages_users'] = 'Brukere';
$txt['language_settings_writable'] = 'Advarsel: Settings.php er ikke skrivbar så standard språkinnstilling kan ikke lagres.';
$txt['edit_languages'] = 'Rediger språk';
$txt['lang_file_not_writable'] = '<strong>Advarsel:</strong> Den primære språkfil (%1$s) er ikke skrivbar. Du må gjøre den skrivbar før du kan gjøre noen endringer.';
$txt['lang_entries_not_writable'] = '<strong>Advarsel:</strong> Språkfilen du ønsker å redigere (%1$s) er ikke skrivbar. Du må gjøre den skrivbar før du kan gjøre noen endringer.';
$txt['languages_ltr'] = 'Høyre til venstre';

$txt['add_language'] = 'Legg til språk';
$txt['add_language_smf'] = 'Last ned fra Simple Machines';
$txt['add_language_smf_browse'] = 'Skriv inn navnet på språket for å søke etter eller la stå tomt for å søke etter alle.';
$txt['add_language_smf_install'] = 'Installer';
$txt['add_language_smf_found'] = 'Følgende språk ble funnet. Klikk på lenken installer ved siden av språket du ønsker å installere, du vil da bli tatt til pakkebehandleren for å installere.';
$txt['add_language_error_no_response'] = ' Simple Machines nettside svarer ikke. Vennligst prøv igjen senere.';
$txt['add_language_error_no_files'] = 'Ingen filer ble funnet.';
$txt['add_language_smf_desc'] = 'Beskrivelse';
$txt['add_language_smf_utf8'] = 'UTF-8';
$txt['add_language_smf_version'] = 'Versjon';

$txt['edit_language_entries_primary'] = 'Nedenfor er de primære språkinnstillingene for denne språkpakken.';
$txt['edit_language_entries'] = 'Rediger språkoppføringer';
$txt['edit_language_entries_file'] = 'Velg oppføringer å redigere';
$txt['languages_dictionary'] = 'Ordbok';
$txt['languages_spelling'] = 'Stavekontroll';
$txt['languages_for_pspell'] = 'Dette er for <a href="http://www.php.net/function.pspell-new" target="_blank" class="new_win">pSpell</a> - hvis installert';
$txt['languages_rtl'] = 'Aktiver &quot;Høyre til Venstre&quot; modus';

$txt['lang_file_desc_index'] = 'Generelle strenger';
$txt['lang_file_desc_EmailTemplates'] = 'E-postmaler';

$txt['languages_download'] = 'Last ned språkpakke';
$txt['languages_download_note'] = 'Denne siden lister opp alle filene som finnes i språkpakken, og en del nyttig informasjon om hver enkelt fil. Alle filer som er merket vil bli kopiert.';
$txt['languages_download_info'] = '<strong>Merk:</strong>
	<ul class="normallist">
		<li>Filer som har status &quot;Ikke skrivbar&quot; betyr at Protendo ikke kan kopiere denne filen til mappen, og du må gjøre målet skrivbar. Bruker en FTP klient eller fylle ut informasjonen nederst på siden.</li>
		<li>Versjonsinformasjonen for en fil viser den siste Protendo versjonen som den ble oppdatert for. Hvis den er angitt i grønt, er det en nyere versjon enn det du har installert. Hvis gult indikerer dette det samme versjonsnummer som er installert. Rødt angir at du har en nyere versjon installert enn det som finnes i pakken.</li>
		<li>Dersom en fil allerede eksisterer på forumet ditt vil det i kolonnen &quot;Finnes allerede&quot; ha en av to verdier. &quot;Identisk&quot; indikerer at filen allerede finnes i en identisk form og trenger ikke bli overskrevet. &quot;Forskjellig&quot;  betyr at innholdet varierer på en eller annen måte, og overskrive er sannsynligvis den optimale løsningen.</li>
	</ul>';

$txt['languages_download_main_files'] = 'Hovedfiler';
$txt['languages_download_theme_files'] = 'Tema-relaterte filer';
$txt['languages_download_filename'] = 'Filnavn';
$txt['languages_download_dest'] = 'Mål';
$txt['languages_download_writable'] = 'Skrivbar';
$txt['languages_download_version'] = 'Versjon';
$txt['languages_download_older'] = 'Du har en nyere versjon av denne filen installert, overskriving er ikke anbefalt.';
$txt['languages_download_exists'] = 'Finnes allerede';
$txt['languages_download_exists_same'] = 'Identisk';
$txt['languages_download_exists_different'] = 'Forskjellig';
$txt['languages_download_copy'] = 'Kopier';
$txt['languages_download_not_chmod'] = 'Du kan ikke fortsette med installasjonen før alle valgte filer som skal kopieres er skrivbare.';
$txt['languages_download_illegal_paths'] = 'Pakken inneholder ulovlige stier - vennligst kontakt Simple Machines';
$txt['languages_download_complete'] = 'Installeringen fullført';
$txt['languages_download_complete_desc'] = 'Språkpakken installert. Vennligst klikk <a href="%1$s">her</a> for å gå tilbake til språksiden';
$txt['languages_delete_confirm'] = 'Er du sikker på at du vil slette dette språket?';

?>