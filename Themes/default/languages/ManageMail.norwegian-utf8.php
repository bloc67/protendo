<?php
// Version: 2.0; ManageMail

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['mailqueue_desc'] = 'Fra denne siden kan du konfigurere e-postinnstillingene, samt visning og administrasjon av den gjeldende e-post køen hvis det er aktivert.';

$txt['mail_type'] = 'Type e-post';
$txt['mail_type_default'] = '(standard for PHP)';
$txt['smtp_host'] = 'SMTP-server';
$txt['smtp_port'] = 'SMTP-port';
$txt['smtp_username'] = 'Brukernavn for SMTP';
$txt['smtp_password'] = 'Passord for SMTP';

$txt['mail_queue'] = 'Aktiver E-post kø';
$txt['mail_limit'] = 'Maksimum e-post  å sende per minutt';
$txt['mail_limit_desc'] = '(Sett denne til 0 for å deaktivere)';
$txt['mail_quantity'] = 'Maksimum antall e-poster å sende per sideinnlasting';

$txt['mailqueue_stats'] = 'Statistikk e-postkø';
$txt['mailqueue_oldest'] = 'Eldste mail';
$txt['mailqueue_oldest_not_available'] = 'I/T';
$txt['mailqueue_size'] = 'Lengde på kø';

$txt['mailqueue_age'] = 'Alder';
$txt['mailqueue_priority'] = 'Prioritet';
$txt['mailqueue_recipient'] = 'Mottaker';
$txt['mailqueue_subject'] = 'Tittel';
$txt['mailqueue_clear_list'] = 'Send e-post kø nå';
$txt['mailqueue_no_items'] = 'E-post køen er tom';
// Do not use numeric entities in below string.
$txt['mailqueue_clear_list_warning'] = 'Er du sikker på at du ønsker å sende hele e-post køen nå? Dette vil overstige alle grensene du har sett.';

$txt['mq_day'] = '%1.1f Dag';
$txt['mq_days'] = '%1.1f Dager';
$txt['mq_hour'] = '%1.1f Time';
$txt['mq_hours'] = '%1.1f Timer';
$txt['mq_minute'] = '%1$d Minutt';
$txt['mq_minutes'] = '%1$d Minutter';
$txt['mq_second'] = '%1$d Sekund';
$txt['mq_seconds'] = '%1$d Sekunder';

$txt['mq_mpriority_5'] = 'Svært lav';
$txt['mq_mpriority_4'] = 'Lav';
$txt['mq_mpriority_3'] = 'Normal';
$txt['mq_mpriority_2'] = 'Høy';
$txt['mq_mpriority_1'] = 'Svært høy';

$txt['birthday_email'] = 'Bursdagsmelding å bruke';
$txt['birthday_body'] = 'E-post melding';
$txt['birthday_subject'] = 'E-post emne';

?>