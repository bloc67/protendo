<?php
// Version: 2.0; ManageBoards

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['boards_and_cats'] = 'Behandle forum og kategorier';
$txt['order'] = 'Rekkefølge';
$txt['full_name'] = 'Navn';
$txt['name_on_display'] = 'Dette er navnet som vil vises.';
$txt['boards_and_cats_desc'] = 'Rediger kategoriene og forumene her. For flere moderatorer skriv dem som <em>&quot;brukernavn&quot;, &quot;brukernavn&quot;</em>. (dette må være brukernavn og ikke visningsnavn!)<br />For å opprette et nytt forum, klikk på knappen Legg til forum. For å gjøre det om til et underforum velger du "Underforum til..." i rullegardinslista når du legger til forumet.';
$txt['parent_members_only'] = 'Vanlige medlemmer';
$txt['parent_guests_only'] = 'Gjester';
$txt['catConfirm'] = 'Er du sikker på at du vil slette denne kategorien?';
$txt['boardConfirm'] = 'Er du sikker på at du vil slette dette forumet?';

$txt['catEdit'] = 'Rediger kategori';
$txt['collapse_enable'] = 'Sammenslåbar';
$txt['collapse_desc'] = 'Tillat medlemmer å slå sammen denne kategorien';
$txt['catModify'] = '(rediger)';

$txt['mboards_order_after'] = 'Etter ';
$txt['mboards_order_inside'] = 'Innenfor ';
$txt['mboards_order_first'] = 'Helt først';

$txt['mboards_new_board'] = 'Legg til forum';
$txt['mboards_new_cat_name'] = 'Ny kategori';
$txt['mboards_add_cat_button'] = 'Legg til kategori';
$txt['mboards_new_board_name'] = 'Nytt forum';

$txt['mboards_name'] = 'Navn';
$txt['mboards_modify'] = 'rediger';
$txt['mboards_permissions'] = 'rettigheter';
// Don't use entities in the below string.
$txt['mboards_permissions_confirm'] = 'Er du sikker på at du vil sette dette forumet til å benytte lokale rettigheter?';

$txt['mboards_delete_cat'] = 'Slett kategori';
$txt['mboards_delete_board'] = 'Slett forum';

$txt['mboards_delete_cat_contains'] = 'Sletting av denne kategorien vil også slette underliggende forum, inkludert alle emner, innlegg og vedlegg inne i hvert forum';
$txt['mboards_delete_option1'] = 'Slett kategori og alle tilhørende forum.';
$txt['mboards_delete_option2'] = 'Slett kategori men flytt alle tilhørende forum til';
$txt['mboards_delete_board_contains'] = 'Sletting av dette forumet vil også slette underliggende forum, inkludert alle emner, innlegg og vedlegg inne i hvert forum';
$txt['mboards_delete_board_option1'] = 'Slett forum og flytt alle underforum opp til kategorinivå.';
$txt['mboards_delete_board_option2'] = 'Slett forum og flytt alle underforum til';
$txt['mboards_delete_what_do'] = 'Vennligst velg hva du vil gjøre med disse fora';
$txt['mboards_delete_confirm'] = 'Bekreft';
$txt['mboards_delete_cancel'] = 'Avbryt';

$txt['mboards_category'] = 'Kategori';
$txt['mboards_description'] = 'Beskrivelse';
$txt['mboards_description_desc'] = 'En kort beskrivelse av forumet.';
$txt['mboards_groups'] = 'Tillatte medlemsgrupper';
$txt['mboards_groups_desc'] = 'Grupper med tilgang til dette forumet.<br /><em>Merk: Hvis et medlem er med i minst en av de avkryssede gruppene eller innleggsbaserte gruppene, vil han/hun ha tilgang til forumet.</em>';
$txt['mboards_groups_regular_members'] = 'Denne gruppen inneholder alle medlemmer som ikke er i noen primær gruppe.';
$txt['mboards_groups_post_group'] = 'Denne gruppen er en innleggsbasert gruppe.';
$txt['mboards_moderators'] = 'Moderatorer';
$txt['mboards_moderators_desc'] = 'Medlemmer med moderatorrettigheter på forumet. Merk at administratorer ikke trenger å listes her.';
$txt['mboards_count_posts'] = 'Tell innlegg';
$txt['mboards_count_posts_desc'] = 'Gjør slik at svar og emner øker medlemmers innleggsteller.';
$txt['mboards_unchanged'] = 'Uforandret';
$txt['mboards_theme'] = 'Design for forum';
$txt['mboards_theme_desc'] = 'Dette lar deg endre design for kun dette bestemte forumet.';
$txt['mboards_theme_default'] = '(generell standard)';
$txt['mboards_override_theme'] = 'Overstyr medlemmers valg av tema';
$txt['mboards_override_theme_desc'] = 'Bruk dette forumets tema selv om medlemmet ikke valgte å bruke standard-designet.';

$txt['mboards_redirect'] = 'Viderekoble til en web-adresse';
$txt['mboards_redirect_desc'] = 'Aktiver dette alternativet for å viderekoble alle som klikker på dette forumet til en annen web-adresse.';
$txt['mboards_redirect_url'] = 'Adresse for å omdirigere brukere til';
$txt['mboards_redirect_url_desc'] = 'For eksempel: &quot;http://www.simplemachines.org&quot;.';
$txt['mboards_redirect_reset'] = 'Nullstill telleren for redirigeringer';
$txt['mboards_redirect_reset_desc'] = 'Hvis du velger dette vil omadresseringer tilbakestille  telleren for dette forumet til null.';
$txt['mboards_current_redirects'] = 'For tiden: %1$s';
$txt['mboards_redirect_disabled'] = 'Merk: Forumet må være tomt for temaer å aktivere dette alternativet.';
$txt['mboards_redirect_disabled_recycle'] = 'Du kan ikke sette forumet der bliver brugt som papirkurv, til å være en omdirigerings-forum.';

$txt['mboards_order_before'] = 'Foran';
$txt['mboards_order_child_of'] = 'Under';
$txt['mboards_order_in_category'] = 'I kategori';
$txt['mboards_current_position'] = 'Gjeldende posisjon';
$txt['no_valid_parent'] = 'Forumet %1$s har ikke en gyldig plassering. Bruk funksjonen \'Finn og reparer alle feil\' for å rette dette.';

$txt['mboards_recycle_disabled_delete'] = 'Merk: Du må velge et alternativ forum som papirkurv eller deaktivere resirkulering før du kan slette dette forumet.';

$txt['mboards_settings_desc'] = 'Endre generelle innstillinger for forum og kategorier.';
$txt['groups_manage_boards'] = 'Medlemsgrupper med tilgang til å behandle forum og kategorier';
$txt['mboards_settings_submit'] = 'Lagre';
$txt['recycle_enable'] = 'Aktiver papirkurv-funksjon for slettede emner';
$txt['recycle_board'] = 'Forum som skal benyttes som papirkurv for slettede emner';
$txt['recycle_board_unselected_notice'] = 'Du har aktivert gjenvinning av emner uten å angi et forum for å plassere dem i. Denne funksjonen vil ikke være aktivert før du angir et forum til å plassere resirkulerte emner inn.';
$txt['countChildPosts'] = 'Tell også innlegg i delforum ved beregning av hovedforumets total';
$txt['allow_ignore_boards'] = 'Tillat å ignorere forumene';

$txt['mboards_select_destination'] = 'Velg en destinasjon for forumet \'<strong>%1$s</strong>\'';
$txt['mboards_cancel_moving'] = 'Avbryt flytting';
$txt['mboards_move'] = 'flytt';

$txt['mboards_no_cats'] = 'Det er for tiden ingen kategorier eller forum konfigurert.';

?>