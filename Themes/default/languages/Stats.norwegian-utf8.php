<?php
// Version: 2.0; Stats

global $context;

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['most_online'] = 'Flest pålogget';

$txt['stats_center'] = 'Statistikksenter';
$txt['general_stats'] = 'Generell statistikk';
$txt['top_posters'] = '10 mest aktive innleggskrivere';
$txt['top_boards'] = '10 mest aktive fora';
$txt['forum_history'] = 'Forumhistorikk (bruker forumets tidsavvik)';
$txt['stats_date'] = 'Dato (åååå/mm/dd)';
$txt['stats_new_topics'] = 'Nye emner';
$txt['stats_new_posts'] = 'Nye innlegg';
$txt['stats_new_members'] = 'Nye medlemmer';
$txt['page_views'] = 'Sidevisninger';
$txt['top_topics_replies'] = '10 mest aktive emner';
$txt['top_topics_views'] = '10 mest leste emner';
$txt['yearly_summary'] = 'Månedlig oppsummering';
$txt['smf_stats_14'] = 'Flest pålogget';
$txt['top_starters'] = 'Mest aktive emnestartere';
$txt['most_time_online'] = 'Lengst tid innlogget';
$txt['best_karma'] = 'Best karma';
$txt['worst_karma'] = 'Dårligst karma';
$txt['stats_more_detailed'] = 'mer detaljert &raquo;';

$txt['average_members'] = 'Snitt registreringer per dag';
$txt['average_posts'] = 'Snitt innlegg per dag';
$txt['average_topics'] = 'Snitt emner per dag';
$txt['average_online'] = 'Snitt pålogget per dag';
$txt['users_online'] = 'Brukere pålogget';
$txt['gender_ratio'] = 'Forhold mellom menn og kvinner på forumet';
$txt['users_online_today'] = 'Pålogget idag';
$txt['num_hits'] = 'Sidevisninger totalt';
$txt['average_hits'] = 'Snitt sidevisninger per dag';

$txt['ssi_comment'] = 'kommentar';
$txt['ssi_comments'] = 'kommentarer';
$txt['ssi_write_comment'] = 'Skriv kommentar';
$txt['ssi_no_guests'] = 'Du kan ikke velge et forum som ikke er tillatt for gjester. Kontroller forum-ID og pøv på nytt.';
$txt['xml_rss_desc'] = 'Direkte nyheter fra ' . $context['forum_name'];

?>