<?php
// Version: 2.0; Post

global $context;

$txt['post_reply'] = 'Skriv svar';
$txt['enter_verification_details'] = 'Fullfør utfyllede detaljer';
$txt['message_icon'] = 'Ikon for innlegg';
$txt['subject_not_filled'] = 'Tittelfeltet ble ikke fylt ut, det er et påkrevd felt.';
$txt['message_body_not_filled'] = 'Selve meldingen ble ikke fylt ut, det er påkrevd at du skriver noe der.';
// Use numeric entities in the below string.
$txt['regards_team'] = 'Med hilsen administratorene på '. $context['forum_name'] . '.';
$txt['add_bbc'] = 'Legg til BBC-kode';
$txt['bold'] = 'Fet';
$txt['italic'] = 'Kursiv';
$txt['underline'] = 'Understreket';
$txt['center'] = 'Midtstilt';
$txt['hyperlink'] = 'Sett inn link';
$txt['insert_email'] = 'Sett inn e-postlink';
$txt['bbc_code'] = 'Sett inn kode';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['bbc_quote'] = 'Sett inn sitat';
$txt['list'] = 'Sett inn liste';
$txt['list_unordered'] = 'Sett inn usortert liste';
$txt['list_ordered'] = 'Sett inn sortert liste';

$txt['change_color'] = 'Endre farge';
$txt['black'] = 'Svart';
$txt['red'] = 'Rød';
$txt['yellow'] = 'Gul';
$txt['pink'] = 'Rosa';
$txt['green'] = 'Grønn';
$txt['orange'] = 'Oransje';
$txt['purple'] = 'Fiolett';
$txt['blue'] = 'Blå';
$txt['beige'] = 'Beige';
$txt['brown'] = 'Brun';
$txt['teal'] = 'Mørk grønn';
$txt['navy'] = 'Sjøblå';
$txt['maroon'] = 'Mareng';
$txt['lime_green'] = 'Sitron Grønn';
$txt['white'] = 'Hvit';
$txt['disable_smileys'] = 'Deaktiver smilefjes';
$txt['dont_use_smileys'] = 'Ikke bruk smilefjes.';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['posted_on'] = 'Skrevet:';
$txt['standard'] = 'Standard';
$txt['thumbs_up'] = 'Tommel opp';
$txt['thumbs_down'] = 'Tommel ned';
$txt['excamation_point'] = 'Utropstegn';
$txt['question_mark'] = 'Spørsmåltegn';
$txt['lamp'] = 'Lampe';
$txt['add_smileys'] = 'Legg til smilefjes';
$txt['flash'] = 'Sett Flash';
$txt['ftp'] = 'Sett inn FTPkobling';
$txt['image'] = 'Sett inn bilde';
$txt['table'] = 'Sett inn tabell';
$txt['table_td'] = 'Sett inn tabell kolonne';
$txt['topic_notify_no'] = 'Det er ingen emner med varslingsmerke.';
$txt['marquee'] = 'Rullende tekst';
$txt['teletype'] = 'Skrivemaskin';
$txt['strike'] = 'Overstrek';
$txt['glow'] = 'Utvid';
$txt['shadow'] = 'Skygge';
$txt['preformatted'] = 'Forhåndsformatert tekst';
$txt['left_align'] = 'Venstrejuster';
$txt['right_align'] = 'Høyrejustert';
$txt['superscript'] = 'Hevet skrift';
$txt['subscript'] = 'Senket skrift';
$txt['table_tr'] = 'Sett inn tabell rad';
$txt['post_too_long'] = 'Din melding er for lang. Vennligst gå tilbake og forkorte den, og prøv igjen.';
$txt['horizontal_rule'] = 'Horisontal linje';
$txt['font_size'] = 'Skriftstørrelse';
$txt['font_face'] = 'Skrifttype';
$txt['toggle_view'] = 'Endre visning';
$txt['unformat_text'] = 'Fjern formatering';

$txt['rich_edit_wont_work'] = 'Din nettleser støtter ikke Rich Text redigering.';
$txt['rich_edit_function_disabled'] = 'Din nettleser støtter ikke denne funksjonen.';

// Use numeric entities in the below five strings.
$txt['notifyUnsubscribe'] = 'Deaktiver varsling på dette emnet ved å klikke her';

$txt['lock_after_post'] = 'Steng emnet etter dette innlegget';
$txt['notify_replies'] = 'Varsle meg ved svar.';
$txt['lock_topic'] = 'Steng dette emnet.';
$txt['shortcuts'] = 'snarveier: trykk alt+s for å lagre/legg til innlegg eller alt+p for å forhåndsvise';
$txt['shortcuts_firefox'] = 'Snarveier: trykk shift+alt+s for å lagre eller shift+alt+p for å forhåndsvise';
$txt['option'] = 'Alternativ';
$txt['reset_votes'] = 'Nullstill antall stemmer';
$txt['reset_votes_check'] = 'Klikk på dette for å tilbakestille alle stemmene til 0.';
$txt['votes'] = 'stemmer';
$txt['attach'] = 'Legg ved';
$txt['clean_attach'] = 'Slett vedlegg';
$txt['attached'] = 'Vedlegg';
$txt['allowed_types'] = 'Tillatte filtyper';
$txt['cant_upload_type'] = 'Du kan ikke laste opp en fil av denne filtypen. De tillate filtypene er';
$txt['uncheck_unwatchd_attach'] = 'Fjern avkryssingen ved de vedleggene du vil slette.';
$txt['restricted_filename'] = 'Det er et begrenset filnavn. Vennligst prøv et annet filnavn.';
$txt['topic_locked_no_reply'] = 'Advarsel: dette emnet er/vil bli stengt!<br />Bare admin og moderatorer kan svare.';
$txt['awaiting_approval'] = 'Venter på godkjenning';
$txt['attachment_requires_approval'] = 'Merk at alle filer som er lagt til, vil ikke vises før etter godkjennelse av en moderator.';
$txt['error_temp_attachments'] = 'Det er funnet vedlegg , som du har lagt ved tidligere, men som ikke er lagt inn. Disse vedleggene er nå vedlagt til dette innlegget. Hvis du ikke ønsker å inkludere dem i dette innlegget, kan du fjerne dem <a href="#postAttachment">her</a>.';
// Use numeric entities in the below string.
$txt['js_post_will_require_approval'] = 'Påminnelse: Dette innlegget vil ikke vises før det er godkjent av en moderator.';

$txt['enter_comment'] = 'Skriv kommentar';
// Use numeric entities in the below two strings.
$txt['reported_post'] = 'Rapportert innlegg';
$txt['reported_to_mod_by'] = 'av';
$txt['rtm10'] = 'Send';
// Use numeric entities in the below four strings.
$txt['report_following_post'] = 'Følgende innlegg, "%1$s" av';
$txt['reported_by'] = 'har blitt rapportert';
$txt['board_moderate'] = 'på et forum hvor du er modererator';
$txt['report_comment'] = 'Den som rapporterte har vedlagt følgende kommentar';

$txt['attach_restrict_attachmentPostLimit'] = 'Max total størrelse %1$dKB';
$txt['attach_restrict_attachmentSizeLimit'] = 'max individuell størrelse %1$dKB';
$txt['attach_restrict_attachmentNumPerPostLimit'] = '%1$d pr innlegg';
$txt['attach_restrictions'] = 'Restriksjoner:';

$txt['post_additionalopt'] = 'Vedlegg og andre alternativer';
$txt['sticky_after'] = 'Gjør emnet prioritert.';
$txt['move_after2'] = 'Flytt emnet.';
$txt['back_to_topic'] = 'Gå tilbake til emnet.';
$txt['approve_this_post'] = 'Godkjenn dette innlegget';

$txt['retrieving_quote'] = 'Henter sitat...';

$txt['post_visual_verification_label'] = 'Bekreftelse';
$txt['post_visual_verification_desc'] = 'Tast inn koden i bildet over for å legge til dette innlegget.';

$txt['poll_options'] = 'Avstemningsinnstillinger';
$txt['poll_run'] = 'La avstemningen vare i';
$txt['poll_run_limit'] = '(La stå tomt for ubegrenset.)';
$txt['poll_results_visibility'] = 'Resultat';
$txt['poll_results_anyone'] = 'Vis resultater til alle.';
$txt['poll_results_voted'] = 'Vis resultater bare etter at noen har stemt.';
$txt['poll_results_after'] = 'Vis resultater etter at avstemningen er avsluttet.';
$txt['poll_max_votes'] = 'Maksimalt antall stemmer per bruker';
$txt['poll_do_change_vote'] = 'Tillat brukere å endre stemme';
$txt['poll_too_many_votes'] = 'Du valgte for mange alternativer - for denne avstemning er antall maks %1$s';
$txt['poll_add_option'] = 'Legg til alternativ';
$txt['poll_guest_vote'] = 'Tillat gjester å stemme';

$txt['spellcheck_done'] = 'Stavekontrollen er fullført.';
$txt['spellcheck_change_to'] = 'Endre til:';
$txt['spellcheck_suggest'] = 'Forslag:';
$txt['spellcheck_change'] = 'Endre';
$txt['spellcheck_change_all'] = 'Endre alle';
$txt['spellcheck_ignore'] = 'Ignorer';
$txt['spellcheck_ignore_all'] = 'Ignorer alle';

$txt['more_attachments'] = 'flere vedlegg';
// Don't use entities in the below string.
$txt['more_attachments_error'] = 'Beklager, men du har ikke tilgang til ta med flere vedlegg.';

$txt['more_smileys'] = 'flere';
$txt['more_smileys_title'] = 'Ekstra smilefjes';
$txt['more_smileys_pick'] = 'Velg et smilefjes';
$txt['more_smileys_close_window'] = 'Steng vindu';

$txt['error_new_reply'] = 'Advarsel - mens du skrev har et nytt svar blitt lagt til emnet. Du vil kanskje ønske å forandre ditt innlegg.';
$txt['error_new_replies'] = 'Advarsel  - mens du skrev har %1$d nye svar blitt lagt til emnet. Du vil kanskje ønske å forandre ditt innlegg.';
$txt['error_new_reply_reading'] = 'Advarsel - mens du leste har et nytt svar blitt lagt til emnet. Du vil kanskje ønske å forandre ditt innlegg.';
$txt['error_new_replies_reading'] = 'Advarsel - mens du leste har %1$d nye svar blitt lagt til emnet. Du vil kanskje ønske å forandre ditt innlegg.';

$txt['announce_this_topic'] = 'Send en kunngjøring om dette emnet til medlemmene:';
$txt['announce_title'] = 'Send en kunngjøring';
$txt['announce_desc'] = 'Dette skjemaet lar deg sende en kunngjøring om dette emnet til utvalgte medlemsgrupper.';
$txt['announce_sending'] = 'Sender kunngjøring om emnet';
$txt['announce_done'] = 'Ferdig';
$txt['announce_continue'] = 'Fortsett';
$txt['announce_topic'] = 'Kunngjør emnet.';
$txt['announce_regular_members'] = 'Vanlige medlemmer';

$txt['digest_subject_daily'] = 'Daglig oppsummering';
$txt['digest_subject_weekly'] = 'Ukentlig oppsummering';
$txt['digest_intro_daily'] = 'Nedenfor er en oppsummering av all aktivitet i de forum og emner du abonnerer på, %1$s i dag. For å avslutte abonnementet kan du gå inn på linken nedenfor.';
$txt['digest_intro_weekly'] = 'Nedenfor er en oppsummering av all aktivitet i de forum og emner du abonnerer på, %1$s i denne uke. For å avslutte abonnementet kan du gå inn på linken nedenfor.';
$txt['digest_new_topics'] = 'Følgende emner har blitt startet';
$txt['digest_new_topics_line'] = '"%1$s" i "%2$s"';
$txt['digest_new_replies'] = 'Svar har blitt gjort i følgende emner';
$txt['digest_new_replies_one'] = '1 svar i "%1$s"';
$txt['digest_new_replies_many'] = '%1$d svar i "%2$s"';
$txt['digest_mod_actions'] = 'Følgende moderatorhandlinger har funnet sted';
$txt['digest_mod_act_sticky'] = '"%1$s" ble prioritert';
$txt['digest_mod_act_lock'] = '"%1$s" ble låst';
$txt['digest_mod_act_unlock'] = '"%1$s" ble åpnet';
$txt['digest_mod_act_remove'] = '"%1$s" ble fjernet';
$txt['digest_mod_act_move'] = '"%1$s" ble flyttet';
$txt['digest_mod_act_merge'] = '"%1$s" ble slått sammen';
$txt['digest_mod_act_split'] = '"%1$s" ble delt';

$txt['post_oppgave'] = 'Ny oppgave ';
$txt['post_prosjekt'] = 'Nytt prosjekt';
$txt['post_tilbud'] = 'Nytt tilbud';
$txt['post_kunde'] = 'Ny kunde';
$txt['post_faktura'] = 'Ny faktura';
$txt['post_produkt'] = 'Nytt produkt';

$txt['post_editoppgave'] = 'Endre oppgave ';
$txt['post_editprosjekt'] = 'Endre prosjekt';
$txt['post_edittilbud'] = 'Endre tilbud';
$txt['post_editkunde'] = 'Endre kunde';
$txt['post_editfaktura'] = 'Endre faktura';
$txt['post_editprodukt'] = 'Endre produkt';


?>