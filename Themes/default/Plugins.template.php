<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

function template_boardtypes()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings, $boardurl;
	
	echo '
		<form action="', $scripturl, '?action=admin;area=boardtypes;sa=install;' . $context['session_id'] . '=' . $context['session_var'] . '" method="post" accept-charset="', $context['character_set'], '">
			
			<h3 class="mainheader" style="padding-bottom: 0.5rem;">
				<input type="submit" value="', $txt['save'], '" name="plugin_install" class="button_submit floatright" />', $txt['plugins_installed'], '
			</h3>';

	$alternate = true;
	foreach($context['plugins_available'] as $xm)
	{
		echo '
			<div class="windowbg', $alternate ? '2' : '', '">
				<div class="bwgrid">
					<div class="bwcell2">
						<img class="imgstyle2" src="', $boardurl, '/Plugins/Boardtypes/' , $xm['name'] , '.jpg" alt="', $xm['name'], '" />
					</div>
					<div class="bwcell14"><div class="inner_right">
						<h4 class="blogheader"><a href="' , $scripturl , '?action=' , $xm['id'] , '">', !empty($xm['title']) ? $xm['title'] : $xm['name'], '</a></h4>
						<hr class="clear" /><div>', $xm['descr'], '</div>
						<div class="padding_vert">
							<input type="checkbox" class="check_me" name="plugin_installed_' , $xm['id'] , '" value="1" ' , !empty($xm['installed']) ? ' checked="checked"' : '' , ' /><span class="check_it"></span>&nbsp;' , $txt['active'] , '
							<br><input type="radio" name="plugin_hide_' , $xm['id'],'" value="0" ' , empty($xm['hide']) ? ' checked' : '' , ' /> ' , $txt['hide_menuitem_off'] , '
							<input type="radio" name="plugin_hide_' , $xm['id'],'" value="1" ' , !empty($xm['hide']) ? ' checked' : '' , ' /> ' , $txt['hide_menuitem_on'],'<br><br>
							<input type="text" name="plugin_name_' , $xm['id'] , '" style="width: 40%; margin: 0 0 0.4em 0; ' , !empty($xm['installed']) ? 'font-weight: bold;' : '' , '" value="' , !empty($xm['title']) ? $xm['title'] : $xm['name'], '" />&nbsp;' , $txt['title'],  ' 
							<hr>
							<a href="' , $scripturl , '?action=admin;area=boardtypesettings;sa=' . $xm['id'] . '"><em>...' , $txt['more'] , ' ' , $txt['settings'] , '</em></a>
						</div>
					</div></div>
				</div>
			</div>';

		$alternate = !$alternate;
	}
	echo '
			<div class="righttext">
				<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
				<input type="submit" value="', $txt['save'], '" name="plugin_install" class="button_submit floatright" />
			</div>
		</form>
	<br class="clear" />';
}

function template_runtimeplugs()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings, $boardurl;
	
	echo '
		<form action="', $scripturl, '?action=admin;area=runtime;sa=install;' . $context['session_id'] . '=' . $context['session_var'] . '" method="post" accept-charset="', $context['character_set'], '">
			<h3 class="mainheader">
				', $txt['runtime_installed'], '
			</h3>';

	$alternate = true;
	foreach($context['runtime_available'] as $xm)
	{
		echo '
			<div class="windowbg', $alternate ? '2' : '', '">
				<div class="bwgrid">
					<div class="bwcell2">
						<img class="imgstyle2" src="', $boardurl, '/Plugins/Runtime/' , $xm['name'] , '.jpg" alt="', $xm['name'], '" />
					</div>
					<div class="bwcell14"><div class="inner_right">
						<input type="submit" value="', $txt['save'], '" name="plugin_install" class="button_submit floatright" />
						<h4 class="blogheader"><a href="' , $scripturl , '?action=' , $xm['id'] , '">', !empty($xm['title']) ? $xm['title'] : $xm['name'], '</a></h4>
						<hr class="clear" /><div>', $xm['descr'], '</div>
						<div class="padding_vert">
							<input type="checkbox" name="plugin_installed_' , $xm['id'] , '" value="1" ' , !empty($xm['installed']) ? ' checked="checked"' : '' , ' />&nbsp;' , $txt['active'] , '
							<br><input type="radio" name="plugin_hide_' , $xm['id'],'" value="0" ' , empty($xm['hide']) ? ' checked' : '' , ' /> ' , $txt['hide_menuitem_off'] , '
							<input type="radio" name="plugin_hide_' , $xm['id'],'" value="1" ' , !empty($xm['hide']) ? ' checked' : '' , ' /> ' , $txt['hide_menuitem_on'],'<br><br>
							<input type="text" name="plugin_name_' , $xm['id'] , '" style="width: 40%; margin: 0 0 0.4em 0; ' , !empty($xm['installed']) ? 'font-weight: bold;' : '' , '" value="' , !empty($xm['title']) ? $xm['title'] : $xm['name'], '" />&nbsp;' , $txt['title'],  '
							<hr>
							<a href="' , $scripturl , '?action=admin;area=runtimesettings;sa=' . $xm['id'] . '">' , $txt['more'] , ' ' , $txt['settings'] , '</a>
						</div>
					</div></div>
				</div>
			</div>';

		$alternate = !$alternate;
	}
	echo '
			<div class="righttext">
				<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
				<input type="submit" value="', $txt['save'], '" name="plugin_install" class="button_submit floatright" />
			</div>
		</form>
	<br class="clear" />';
}



function template_sectionplugins()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings;
	
	echo '
		<form action="', $scripturl, '?action=admin;area=section;sa=install;' . $context['session_id'] . '=' . $context['session_var'] . '" method="post" accept-charset="', $context['character_set'], '">
			<input type="submit" value="', $txt['save'], '" name="plugin_install" class="button_submit floatright" />
			<h3 class="mainheader">
				',  $txt['section_plugins_available'] , '
			</h3>
			';
	foreach($context['sectionplugins_available'] as $xm)
	{
		echo '
		<div class="bwcell16">
			<div class="windowbg ' , !empty($xm['installed']) ? 'activecorner' : '' , '" style="clear: both; margin: 3px;">
				<input class="floatleft" type="checkbox" name="plugin_installed_' , $xm['id'] , '" value="1" ' , !empty($xm['installed']) ? ' checked="checked"' : '' , ' />
				<input type="text" name="plugin_name_' , $xm['id'] , '" style="width: 80%;' , !empty($xm['installed']) ? 'font-weight: bold;' : '' , '" value="' , !empty($xm['title']) ? $xm['title'] : $xm['name'], '" />
				<pre>' , print_r($xm) , '</pre>
			</div>
		</div>';
	}

	echo '
			<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
		</form>';
}

function template_versiondisplay()
{
	global $forum_version, $context, $txt;
	
	if($context['user']['is_logged'])
		echo '<div class="info3"><em>' , $forum_version , '</em></div>';
	else
		echo '<div class="info3"><em>' , $txt['guest_sidebar'] , '</em></div>';

}

function template_frontpage_options()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings;

	echo '
		<form action="', $scripturl, '?action=admin;area=frontpage;' , $context['session_var'] , '=' , $context['session_id'] , '" method="post" name="frontpageform" id="frontpageform" accept-charset="', $context['character_set'], '" enctype="multipart/form-data">
			<h3 class="mainheader">
				',  $txt['frontpage']. ' ' . $txt['settings'] , '
			</h3>
			<div class="windowbg">
				<div id="showbox4">';
		// shwo the interface here
		template_frontpage_blocks();
						
		echo '
				</div>
				<hr>
				<p>
					<input name="' . $context['session_var'] . '" value="' , $context['session_id'] , '" type="hidden" />
					<input type="submit" value="', $txt['save'], '" name="frontpagesub" id="frontpagesub" class="button_submit" />
				</p>
			</div>
		</form>';

}

function template_frontpage_blocks()
{
	global $modSettings, $scripturl, $settings, $txt, $context;

	
	echo '
	<div>
		<ul class="horiz_nav">
			<li><a href="' , $scripturl , '?action=admin;area=frontpage;addblock;' , $context['session_var'] , '=' , $context['session_id'] , '" class="button_submit">' , $txt['addblock'] , '</a></li>
			<li><a href="' , $scripturl , '?action=admin;area=frontpage;clearall;' , $context['session_var'] , '=' , $context['session_id'] , '" class="button_submit" onclick="return confirm(\'', $txt['clear_all_confirm'], '\');">' , $txt['clear_all'] , '</a></li>
		<ul>
	</div>
	<div class="bwgrid">';
	
	foreach($modSettings['frontpage']['blocks'] as $bl => $bdata)
	{
		$width = !empty($bdata['width']) ? ($bdata['width']>16 ? 16 : $bdata['width']) : 4;
		$number = !empty($bdata['number']) ? $bdata['number'] : 7;
		echo '
		<div class="bwcell' . $width . '" id="frontblockw_'.$bl.'"><div class="block_style boxshadow" id="frontblock_'.$bl.'">
			<a href="' , $scripturl , '?action=admin;area=frontpage;moveblockf=' , $bl , ';' , $context['session_var'] , '=' , $context['session_id'] , '"><img src="' , $settings['images_url'] , '/remooz/forward.png" alt="" style="float: right;" /></a>
			<a href="' , $scripturl , '?action=admin;area=frontpage;moveblock=' , $bl , ';' , $context['session_var'] , '=' , $context['session_id'] , '"><img src="' , $settings['images_url'] , '/remooz/back.png" alt="" style="float: right;" /></a>
			<a href="' , $scripturl , '?action=admin;area=frontpage;delblock=' , $bl , ';' , $context['session_var'] , '=' , $context['session_id'] , '" onclick="return confirm(\'', $txt['clear_all_confirm'], '\');"><img src="' , $settings['images_url'] , '/remooz/closebox.png" alt="" style="float: right; clear: right; margin-right: 0.5em;" /></a>
			<div>
				<input type="text" name="front_block_title_'.$bl.'" id="front_block_title_'.$bl.'" value="', !empty($bdata['title']) ? $bdata['title'] : '', '">
				<span class="helptext">' , $txt['title'] , '</span>
			</div>
			<div>
				<select name="front_block_type_'.$bl.'">
					<option value="0"' , empty($bdata['type'])  ? ' selected="selected"' : '', '>Plugin</option>
					<option value="1"' , !empty($bdata['type']) && $bdata['type']==1  ? ' selected="selected"' : '', '>Forum board(s)</option>
					<option value="2"' , !empty($bdata['type']) && $bdata['type']==2  ? ' selected="selected"' : '', '>BBC code</option>
					<option value="3"' , !empty($bdata['type']) && $bdata['type']==3  ? ' selected="selected"' : '', '>HTML code</option>
				</select>
				<span class="helptext">' , $txt['block_type'] , '</span>
			</div>
			<div>
				<textarea name="front_block_code_' . $bl . '">' , !empty($bdata['code']) ? $bdata['code'] : '', '</textarea>
				<span class="helptext">' , $txt['bodytext'] , '</span>
			</div>
			<div>
				<select name="front_block_plugin_'.$bl.'">	';
	
	foreach($modSettings['plugins'] as $s => $data)
		echo '	
					<option value="' , $s , '"' , !empty($bdata['plugin']) && $bdata['plugin'] == $s ? ' selected="selected"' : '', '>' , $data['menu']['title'] , '</option>';

	echo '
				</select>
			</div>
			<div>
				<div id="slider'.$bl.'2" class="mooslider"><div id="knob'.$bl.'2" class="mooknob2"></div></div>
				<input type="text" read-only name="front_block_width_'.$bl.'" id="front_block_width_'.$bl.'" class="round_input" value="'. $width.'">
			</div>
			<div>
				<div id="slider'.$bl.'" class="mooslider"><div id="knob'.$bl.'" class="mooknob"></div></div>
				<input type="text" read-only name="front_block_number_'.$bl.'" id="front_block_number_'.$bl.'" class="round_input" value="'. $number.'">
				<span class="smalltext">' , $txt['items_number'] , '</span>
				<span class="helptext">' , $txt['block_items'] , '</span>
			</div>
			<div>
				<select name="front_block_layout_'.$bl.'">
					<option value="0" ', empty($bdata['layout']) ? 'selected="selected"' : '','">Picture - title</option>
					<option value="1" ', !empty($bdata['layout']) && $bdata['layout'] == 1 ? 'selected="selected"' : '' , '">Titles only</option>
					<option value="2" ', !empty($bdata['layout']) && $bdata['layout'] == 2 ? 'selected="selected"' : '', '">Big Picture + Title + Time</option>
					<option value="3" ', !empty($bdata['layout']) && $bdata['layout'] == 3 ? 'selected="selected"' : '', '">Title + Time</option>
					<option value="4" ', !empty($bdata['layout']) && $bdata['layout'] == 4 ? 'selected="selected"' : '', '">Pictures only</option>
					<option value="5" ', !empty($bdata['layout']) && $bdata['layout'] == 5 ? 'selected="selected"' : '', '">Slideshow</option>
				</select>
				<span class="helptext">' , $txt['block_layout'] , '</span>
			</div>
		</div></div>
				<script type="text/javascript">
					//<![CDATA[
window.addEvent(\'domready\', function() {
	var SliderObject'.$bl.' = new Slider(\'slider'.$bl.'\', \'knob'.$bl.'\', {
		//options
		range: [1, 20],
		snap: false,
		steps: 20,
		initialStep: ' . $number . ',
		offset: 0,
		wheel: true,
		mode: \'horizontal\',
		//callback events
		onTick: function(pos){
			//this line is very necessary (left with horizontal)
			this.knob.setStyle(\'left\', pos);
		},
		onChange: function(step){
			$(\'front_block_number_' . $bl . '\').value = step;
		},
	});
});					
window.addEvent(\'domready\', function() {
	var SliderObject'.$bl.'2 = new Slider(\'slider'.$bl.'2\', \'knob'.$bl.'2\', {
		//options
		range: [4, 16],
		snap: false,
		steps: 12,
		initialStep: ' . $width . ',
		offset: 0,
		wheel: true,
		mode: \'horizontal\',
		//callback events
		onTick: function(pos){
			//this line is very necessary (left with horizontal)
			this.knob.setStyle(\'left\', pos);
		},
		onChange: function(step){
			$(\'front_block_width_' . $bl . '\').value = step;
			$(\'frontblockw_' . $bl . '\').className = \'bwcell\' + step;
		},
	});
});					
		$$(\'.mootext_resize\').addEvent(\'focus\', function(){ this.style.height = \'10em\'; });
		$$(\'.mootext_resize\').addEvent(\'blur\', function(){ this.style.height = \'2em\'; });
					//]]>
				</script>
		';
		
		if(!empty($bdate['newline']))
			echo '
	</div><div class="bwgrid">';
	}
	echo '
	</div>';	

}


function template_boardtypesettings()
{
	global $modSettings, $scripturl;

	if(!empty($_GET['sa']))
		$chosenplugin = $_GET['sa'];
	else
		$chosenplugin = '';

	echo '
	<ul class="quickbuttons" style="padding-left: 0;">';

	// add any from plugins
	if(!empty($modSettings['plugins']))
	{
		foreach($modSettings['plugins'] as $plugin => $data)
		{
			if(!empty($data['menu']) && empty($data['menu']['hide']))
			{
				echo '
		<li><a class="inside' , $chosenplugin==$data['action'] ? ' chosen' : '' , '" href="' . $scripturl . '?action=admin;area=boardtypesettings;sa=' . $data['action'] . '">' . $data['menu']['title'] . '</a></li>';
					
			}
		}
	}
	echo '
	</ul><hr>';

	if(empty($chosenplugin))
		echo '
	<div class="padding_vert">No plugin chosen.</div>';
}

function template_bsettings()
{
	global $context, $settings, $options, $scripturl, $txt;

	template_boardtypesettings();

	if(!isset($context['plugsettings']))
	{
		echo '<p><em>' , $txt['nosettings'] , '</em></p>';
		return;
	}
	echo '
	<h2 class="mainheader">
		 ', $context['plugsettings']['title'], '
	</h2>
	<div >
		<form action="', $context['plugsettings']['href'], '" method="post" accept-charset="', $context['character_set'], '" style="padding: 0; margin: 0;">
			<div >
				<div>
				<dl class="settings">';

	foreach ($context['plugsettings']['values'] as $setting)
	{
		// Is this a separator?
		if (empty($setting))
		{
			echo '
					</dl>
					<hr class="hrcolor" />
					<dl class="settings flow_auto">';
		}
		// A checkbox?
		elseif ($setting['type'] == 'checkbox')
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd class="radio_button">
							<input type="radio" name="', $setting['id'], '" id="', $setting['id'], '0"', empty($setting['value']) ? ' checked="checked"' : '', ' value="0" />
							<label for="', $setting['id'], '0" class="smalltext breadcrumb">', $txt['no'],'</label>
							<input type="radio" name="', $setting['id'], '" id="', $setting['id'], '1"', !empty($setting['value']) ? ' checked="checked"' : '', ' value="1" />
							<label for="', $setting['id'], '1" class="smalltext breadcrumb">', $txt['yes'],'</label> 
						</dd>';
		}
		// A list with options?
		elseif ($setting['type'] == 'list')
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd>
							<select name="', $setting['id'], '" id="', $setting['id'], '">';

			if(isset($setting['opts-range']))
			{
				for($a = $setting['opts-range'][0]; $a< ($setting['opts-range'][1]+1); $a++)
					echo '
							<option value="', $a, '"', $a == $setting['value'] ? ' selected="selected"' : '', '>', $a, '</option>';
			}
			else
			{
				foreach ($setting['options'] as $value => $label)
					echo '
							<option value="', $value, '"', $value == $setting['value'] ? ' selected="selected"' : '', '>', $label, '</option>';
			}

			echo '
							</select>
						</dd>';
		}
		// A list with options?
		elseif ($setting['type'] == 'radio')
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd>';

			foreach ($setting['options'] as $value => $label)
				echo '
							<input type="radio" name="', $setting['id'], '" id="', $setting['id'], '" value="', $value, '" ', $value == $setting['value'] ? ' checked="checked"' : '', '>&nbsp;', $label, '<br>';

			echo '
						</dd>';
		}
		// A list with options?
		elseif ($setting['type'] == 'images')
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd>';

			foreach ($setting['options'] as $value => $label)
				echo '<div style="margin: 10px 0;">
							<input type="radio" name="', $setting['id'], '" id="', $setting['id'], '" value="', $value, '" ', $value == $setting['value'] ? ' checked="checked"' : '', '>
							<a class="remooz" href="', (!empty($setting['imgpath']) ? $setting['imgpath'] : ''), $value, '">
								<img src="', (!empty($setting['imgpath']) ? $setting['imgpath'] : ''), $value, '" style="vertical-align: top; width: 90%; ', $value == $setting['value'] ? 'border: solid 1px red;' : '', '" alt="" />
							</a>
						</div>';

			echo '
						</dd>';
		}
		// A bigger box
		elseif ($setting['type'] == 'textarea')
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd style="position: relative;">
							<textarea name="', $setting['id'], '" id="', $setting['id'], '" style="width: 100%;height: 150px;"></textarea>
						</dd>';
			if(isset($setting['textboxlist']))
				makeTextListBox($setting['id'],$setting['value']);

		}
		// A regular input box, then?
		else
		{
			echo '
						<dt>
							<label for="', $setting['id'], '">', $setting['label'], '</label>:';

			if (isset($setting['description']))
				echo '<br />
							<span class="descr">', $setting['description'], '</span>';

			echo '
						</dt>
						<dd>
							<input type="text" name="', $setting['id'], '" id="', $setting['id'], '" value="', $setting['value'], '"', $setting['type'] == 'number' ? ' size="5"' : (empty($setting['size']) ? ' size="40"' : ' size="' . $setting['size'] . '"'), ' class="input_text" />
						</dd>';
		}
	}

	echo '
					</dl>
				</div><hr>
				<div class="righttext">
					<input type="submit" name="submit" value="', $txt['save'], '" class="button_submit" />
				</div>
			</div>
		
			<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
			<input type="hidden" name="boardtypesettings" value="1" />
		</form>
	</div>
	<br class="clear" />';
}

// The post boardcontrol template
function template_postboardcontrols()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings, $counter;

	echo '
			<dl class="settings narrow">';
	$help = '';
	foreach($context['boardtypes_controls'] as $what => $data)
	{
		$type = !empty($data['type']) ? $data['type'] : 'text';
		$data_type = !empty($data['data_type']) ? $data['data_type'] : 'str';
		if($type == 'textarea')
			echo '
					<dt>' , $data['title'] , ':</dt>	
					<dd>
						<textarea style="width: 84%; height: 100px;" name="' . $data_type  . 'boardtypevalue_' . $what . '">' . $data['value'] . '</textarea>
					</dd>';
		elseif($type == 'help')
			$help .= $data['text'];
		elseif($type == 'hidden')
			echo '<input type="hidden" name="' , $what, '" value="' . $data['value'] . '">';
		elseif($type == 'list')
		{
			echo '
					<dt>' , $data['title'] , ':</dt>	
					<dd>';
			if(isset($data['hidden']))
			{
				if(!empty($data['type_data']))
				{
					foreach($data['type_data'] as $ids => $id_data)
						if($data['value'] == $ids)
							echo '<p>' , $id_data , '</p>';
				}
			}
			else
			{
				echo '
						<select name="' . $data_type  . 'boardtypevalue_' . $what . '" id="' . $data_type  . 'boardtypevalue_' . $what . '">
							<option value="">- ' , $txt['no_one'] , ' -</option>';
				if(isset($data['opts-range']))
				{
					for($a = $data['opts-range'][0]; $a< ($data['opts-range'][1]+1); $a++)
						echo '<option value="' . $a . '"' , $data['value'] == $a ? ' selected' : '' , '>' , $a , '</option>';
				}
				else
				{
					foreach($data['type_data'] as $ids => $id_data)
						echo '<option value="' . $ids . '"' , $data['value'] == $ids ? ' selected' : '' , '>' , $id_data , '</option>';
				}
				echo '
						</select>';
			}
			echo '
					</dd>';
		}
		elseif($type == 'func' && !empty($data['func']))
			echo '
					<dt>' , $data['events'] , ':</dt>	
					<dd>
						' , call_user_func($data['func'], !empty($context['current_topic']) ? $context['current_topic'] : 0) , '
					</dd>';
		elseif($type == 'hide')
		{
			echo '
					<input type="hidden" name="' . $data_type  . 'boardtypevalue_' . $what . '" id="' . $data_type  . 'boardtypevalue_' . $what . '" value="' . $data['value'] . '">';
		}
		elseif($type == 'slist')
		{
			echo '
					<dt>' , $data['title'] , ':</dt>	
					<dd>
						<select name="' . $data_type  . 'boardtypevalue_' . $what . '" id="' . $data_type  . 'boardtypevalue_' . $what . '">
						<option value="">- ' , $txt['no_one'] , ' -</option>';
			if(!empty($data['type_data']))
			{
				foreach($data['type_data'] as $ids => $id_data)
					echo '<option value="' . $ids . '"' , $data['value'] == $ids ? ' selected' : '' , '>' , $id_data['fulltitle'] , '('.$ids.')</option>';
			}
			echo '
						</select>
						<div id="selectbox' , $what , '">         </div>';
			foreach($data['type_data'] as $id => $id_data)
				echo '<div id="select' , $what , '_' , $id , '" style="display: none;">' , $id_data['show'] , '</div>';
					
			echo '		
<script type="text/javascript">
	window.addEvent(\'domready\', function(){
		$(\'' . $data_type  . 'boardtypevalue_' . $what . '\').addEvent(\'change\',function(e){
			dat = $(\'select' . $what . '_\' + this.getSelected()[0].value).get(\'html\');
			$(\'selectbox' , $what , '\').set(\'html\',dat);
			e.stop();
		});        
	});
</script>		

					</dd>';
		}
		elseif($type == 'sradio')
		{
			echo '
					<dt>' , $data['title'] , ':</dt>	
					<dd>';
			if(!empty($data['type_data']))
			{
				foreach($data['type_data'] as $ids => $id_data)
				{
					if($id_data['season']==0)
						continue;

					if(!empty($data['saved'][$ids]['seen']))
						echo '
					<div style="clear: both;">
						<input type="checkbox" name="tvepi' . $ids  . '" ' , !empty($data['saved'][$ids]['seen']) ? ' checked="checked"' : '' , ' value="1">
						<select name="tvrate' . $ids . '">
							<option value="0">0</option>
							<option value="1"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 1 ? ' selected="selected"' : '' , '>1</option>
							<option value="2"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 2 ? ' selected="selected"' : '' , '>2</option>
							<option value="3"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 3 ? ' selected="selected"' : '' , '>3</option>
							<option value="4"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 4 ? ' selected="selected"' : '' , '>4</option>
							<option value="5"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 5 ? ' selected="selected"' : '' , '>5</option>
							<option value="6"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 6 ? ' selected="selected"' : '' , '>6</option>
							<option value="7"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 7 ? ' selected="selected"' : '' , '>7</option>
							<option value="8"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 8 ? ' selected="selected"' : '' , '>8</option>
							<option value="9"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 9 ? ' selected="selected"' : '' , '>9</option>
							<option value="10"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 10 ? ' selected="selected"' : '' , '>10</option>
						</select>
						<span class="middletext"><b>s' . $id_data['season']. ' e' . $id_data['episode']. ' | "' . $id_data['title'] . '"</b></span>
					</div>	';
					else
						echo '
					<div style="clear: both;"><hr>
						<input type="checkbox" name="tvepi' . $ids  . '" ' , !empty($data['saved'][$ids]['seen']) ? ' checked="checked"' : '' , ' value="1">
						<select name="tvrate' . $ids . '">
							<option value="0">0</option>
							<option value="1"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 1 ? ' selected="selected"' : '' , '>1</option>
							<option value="2"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 2 ? ' selected="selected"' : '' , '>2</option>
							<option value="3"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 3 ? ' selected="selected"' : '' , '>3</option>
							<option value="4"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 4 ? ' selected="selected"' : '' , '>4</option>
							<option value="5"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 5 ? ' selected="selected"' : '' , '>5</option>
							<option value="6"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 6 ? ' selected="selected"' : '' , '>6</option>
							<option value="7"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 7 ? ' selected="selected"' : '' , '>7</option>
							<option value="8"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 8 ? ' selected="selected"' : '' , '>8</option>
							<option value="9"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 9 ? ' selected="selected"' : '' , '>9</option>
							<option value="10"' , !empty($data['saved'][$ids]['rate']) && $data['saved'][$ids]['rate'] == 10 ? ' selected="selected"' : '' , '>10</option>
						</select>
						<span class="middletext">s' . $id_data['season']. ' e' . $id_data['episode']. ' | "' . $id_data['title'] . '"</span>
						<div style="padding: 10px 0;"><div style="background-color: #ddd; background-image: url(' .$id_data['image'] . '); background-size: contain; float: left; width: 70px; height: 50px; margin-right: 1em;"></div>
						<div class="smalltext">' . $id_data['overview']. '</div></div>
					</div>	';
				}
			}
			echo '		
					</dd>';
		}
		else
			echo '
					<dt>' , $data['title'] , ':</dt>	
					<dd>
						<input type="text" name="' . $data_type  . 'boardtypevalue_' . $what . '" value="' . $data['value'] . '" tabindex="', $context['tabindex']++, '" size="80" maxlength="80" class="input_text" />
					</dd>';
	}
	echo '
				</dl>';
}

// menu editing
function template_menu_front()
{
	global $forum_version, $context, $txt;
	
	echo 'MENU FRONT screen';

}

// menu editing
function template_menu_edit()
{
	global $forum_version, $context, $txt, $modSettings, $scripturl;
	
	echo '
	<h2 class="mainheader"> ', $txt['menuitems_edit'], '	</h2>
	<div >
		<form id="menuform" action="', $scripturl, '?action=admin;area=menuitems;sa=edit" method="post" accept-charset="', $context['character_set'], '" class="windowbg" style="padding: 1em; margin: 0;">
			<input type="hidden" name="locking" id="locking" value="">
			<div class="bwgrid">
				<div class="bwcell8"><div class="block_style">
					<ul id="sortable">';
	
	foreach($context['menu_buttons'] as $m => $menu)
	{
		if(empty($menu['top']))
			continue;

		echo '<li id="'.$m.'"><span>' . $menu['title'].'</span>';
		if(!empty($menu['sub_buttons']))
		{
			echo '<ul>';
			foreach($menu['sub_buttons'] as $s => $sub)
			{
				if(!empty($s))
					echo '<li id="'.$s.'" title="' . $sub['title'] . '"><span>' . $sub['title'] . '</span></li>';
			}
			echo '</ul>';
		}		
		echo '</li>';
	}
	echo '</ul>	
				</div></div>
				<div class="bwcell8"><div class="inner_right">
					<p><input name="newid" id="newid" value="" type="text"> <span class="helptext">ID</span></p> 
					<p><input name="newtitle" id="newtitle" value="" type="text">  <span class="helptext">' , $txt['title'] , '</span>	</p>
					<p><input name="newurl" id="newurl" value="" type="text">  <span class="helptext">URL</span>	</p>
					<p><input name="newtop" id="newtop" value="1" type="checkbox" checked>  <span class="helptext"><b>Top menu?</b></span>	</p>
					<input name="serialize" id="serialize" class="button_submit" value="' , $txt['save'] , '" type="submit"></p>
					<input type="hidden" name="serialized" id="serialized" />
					<span class="breadcrumb">
						<a href="' , $scripturl , '?action=admin;area=menuitems;sa=edit;clearmenu;', $context['session_var'], '=' , $context['session_id'] , '" onclick="return confirm(\'', $txt['clear_all_confirm'], '\');">' , $txt['clear_positions'] , '</a> &nbsp;|&nbsp;
						<a href="' , $scripturl , '?action=admin;area=menuitems;sa=edit;clearmenubuttons;', $context['session_var'], '=' , $context['session_id'] , '" onclick="return confirm(\'', $txt['clear_all_confirm'], '\');">' , $txt['clear_all_buttons'] , '</a>
					</span>
					<hr>
					<h3 class="mainheader">' , $txt['my_buttons']  , '</h3>';
	if(!empty($modSettings['newmenu']))
	{
		foreach($modSettings['newmenu'] as $men => $mdata)
		{
			echo '
			<div class="padding: 0 0 1em 0;">	
				<span class="floatright smalltext breadcrumb">
					<a href="' , $scripturl , '?action=admin;area=menuitems;sa=edit;changepos=', $men, '">' , isset($mdata['top']) ? $txt['set_top'] : $txt['set_user'] , '</a> &nbsp;&nbsp; 
					<a href="' , $scripturl , '?action=admin;area=menuitems;sa=edit;edit=', $men, '">' , $txt['modify'] , '</a> &nbsp;&nbsp; 
					<a href="' , $scripturl , '?action=admin;area=menuitems;sa=edit;clearmenuitem=', $men, '">' , $txt['delete'] , '</a>
				</span>
				<a href="' , $mdata['href'] , '">' , $mdata['title'] , '</a>
			</div>';
		}
	}
	echo '
				</div></div>
			</div>
			<input name="' . $context['session_var'] . '" value="' , $context['session_id'] , '" type="hidden" />
		</form>
	<script type="text/javascript">
		//<![CDATA[
	
		var LockIt = function() {
			sortIt.options.lock = ($(\'locking\').value.length == 0) ? null : $(\'locking\').value;
		};
		
		var sortIt;
		window.addEvent(\'domready\', function() {
			sortIt = new NestedSortables(\'sortable\', {
				collapse: false,
				events: {
					onStart: function(el){
						console.log(el);
						el.addClass(\'drag\');
					},
					onComplete: function(el) {
						el.removeClass(\'drag\');
					}
				}
			});
			$(\'serialize\').addEvent(\'click\', function(){
				$(\'serialized\').value = JSON.encode(sortIt.serialize());
				$(\'menuform\').submit();
			});
		});
		//]]>
	</script>
	</div>';
}

// menu editing
function template_menu_settings()
{
	global $forum_version, $context, $txt;
	
	echo 'Settings';

}

?>