<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function template_main()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings;

	echo '
	<h3 class="mainheader">', $context['page_title'], '</h3>
	<div id="statistics">
		<h4 class="headertexts"> ', $txt['general_stats'], '</h4>
		<div class="bwgrid">
			<div class="bwcell8">
				<div class="windowbg" style="margin-right: 1px;">
					<dl class="settings">
						<dt>', $txt['total_members'], ':</dt>
						<dd>', $context['show_member_list'] ? '<a href="' . $scripturl . '?action=mlist">' . $context['num_members'] . '</a>' : $context['num_members'], '</dd>
						<dt>', $txt['total_posts'], ':</dt>
						<dd>', $context['num_posts'], '</dd>
						<dt>', $txt['total_topics'], ':</dt>
						<dd>', $context['num_topics'], '</dd>
						<dt>', $txt['total_cats'], ':</dt>
						<dd>', $context['num_categories'], '</dd>
						<dt>', $txt['users_online'], ':</dt>
						<dd>', $context['users_online'], '</dd>
						<dt>', $txt['most_online'], ':</dt>
						<dd>', $context['most_members_online']['number'], ' - ', $context['most_members_online']['date'], '</dd>
						<dt>', $txt['users_online_today'], ':</dt>
						<dd>', $context['online_today'], '</dd>';

	if (!empty($modSettings['hitStats']))
		echo '
						<dt>', $txt['num_hits'], ':</dt>
						<dd>', $context['num_hits'], '</dd>';

	echo '
					</dl>
				</div>
			</div>
			<div class="bwcell8">
				<div class="windowbg2" style="margin-left: 1px;">
					<dl class="settings">
						<dt>', $txt['average_members'], ':</dt>
						<dd>', $context['average_members'], '</dd>
						<dt>', $txt['average_posts'], ':</dt>
						<dd>', $context['average_posts'], '</dd>
						<dt>', $txt['average_topics'], ':</dt>
						<dd>', $context['average_topics'], '</dd>
						<dt>', $txt['total_boards'], ':</dt>
						<dd>', $context['num_boards'], '</dd>
						<dt>', $txt['latest_member'], ':</dt>
						<dd>', $context['common_stats']['latest_member']['link'], '</dd>
						<dt>', $txt['average_online'], ':</dt>
						<dd>', $context['average_online'], '</dd>
						<dt>', $txt['gender_ratio'], ':</dt>
						<dd>', $context['gender']['ratio'], '</dd>';

	if (!empty($modSettings['hitStats']))
		echo '
						<dt>', $txt['average_hits'], ':</dt>
						<dd>', $context['average_hits'], '</dd>';

	echo '
					</dl>
				</div>
			</div>
		</div>
		<div class="bwgrid">
			<div class="bwcell8">
				<h4 class="headertexts"> ', $txt['top_posters'], '</h4>
				<div class="windowbg" style="margin: 1px 1px 0 0;">
					<dl class="settings narrow2">';

	foreach ($context['top_posters'] as $poster)
	{
		echo '
						<dt>
							', $poster['link'], '
						</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($poster['post_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $poster['post_percent'], '%;"></span>
									</span>';

		echo '				</span>
								<span class="bwcell4">
									<span class="floatright">', $poster['num_posts'], '</span>
								</span>
							</span>
						</dd>';
	}

	echo '
					</dl>
				</div>
			</div>
			<div class="bwcell8">
				<h4 class="headertexts">', $txt['top_boards'], '	</h4>
				<div class="windowbg2" style="margin: 1px 0 0 1px;">
					<dl class="settings">';

	foreach ($context['top_boards'] as $board)
	{
		echo '
						<dt>
							', $board['link'], '
						</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($board['post_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $board['post_percent'], '%;"></span>
									</span>';

		echo '	
								</span>
								<span class="bwcell4">
									<span class="floatright">', $board['num_posts'], '</span>
								</span>
							</span>
						</dd>';
	}

	echo '
					</dl>
				</div>
			</div>
		</div>
		<div class="bwgrid">
			<div class="bwcell8">
				<h4 class="headertexts">', $txt['top_topics_replies'], '</h4>
				<div class="windowbg" style="margin: 1px 1px 0 0;">
					<dl class="settings">';

	foreach ($context['top_topics_replies'] as $topic)
	{
		echo '
						<dt>
							', $topic['link'], '
						</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($topic['post_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $topic['post_percent'], '%;"></span>
									</span>';

		echo '
								</span>
								<span class="bwcell4">
									<span class="floatright">' . $topic['num_replies'] . '</span>
								</span>
							</span>
						</dd>';
	}
	echo '
					</dl>
				</div>
			</div>
			<div class="bwcell8">
				<h4 class="headertexts"> ', $txt['top_topics_views'], '</h4>
				<div class="windowbg2" style="margin: 1px 0 0 1px;">
					<dl class="settings">';

	foreach ($context['top_topics_views'] as $topic)
	{
		echo '
						<dt>', $topic['link'], '</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($topic['post_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $topic['post_percent'], '%;"></span>
									</span>';

		echo '
								</span>
								<span class="bwcell4">
									<span class="floatright">' . $topic['num_views'] . '</span>
								</span>
							</span>
						</dd>';
	}

	echo '
					</dl>
				</div>
			</div>
		</div>
		<div class="bwgrid">
			<div class="bwcell8">
				<h4 class="headertexts">', $txt['top_starters'], '</h4>
				<div class="windowbg" style="margin: 1px 1px 0 0;">
					<dl class="settings">';

	foreach ($context['top_starters'] as $poster)
	{
		echo '
						<dt>
							', $poster['link'], '
						</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($poster['post_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $poster['post_percent'], '%;"></span>
									</span>';

		echo '
								</span>
								<span class="bwcell4">
									<span class="floatright">', $poster['num_topics'], '</span>
								</span>
							</span>
						</dd>';
	}

	echo '
					</dl>
				</div>
			</div>
			<div class="bwcell8">
				<h4 class="headertexts">', $txt['most_time_online'], '</h4>
				<div class="windowbg2" style="margin: 1px 0 0 1px;">
					<dl class="settings">';

	foreach ($context['top_time_online'] as $poster)
	{
		echo '
						<dt>
							', $poster['link'], '
						</dt>
						<dd>
							<span class="bwgrid">
								<span class="bwcell12">';

		if (!empty($poster['time_percent']))
			echo '
									<span class="bar">
										<span style="width: ', $poster['post_percent'], '%;"></span>
									</span>';

		echo '	
								</span>
								<span class="bwcell4">
									<span class="floatright">', $poster['time_online'], '</span>
								</span>
							</span>
						</dd>';
	}

	echo '
					</dl>
				</div>
			</div>
		</div>
		<br class="clear" />
		<div class="flow_hidden">
			<h3 class="mainheader">', $txt['forum_history'], '</h3>';

	if (!empty($context['yearly']))
	{
		echo '
		<table border="0" width="100%" cellspacing="1" cellpadding="4" class="table_grid" id="stats">
			<thead>
				<tr valign="middle" align="center">
					<th class="headertexts" width="25%">', $txt['yearly_summary'], '</th>
					<th class="headertexts" width="15%">', $txt['stats_new_topics'], '</th>
					<th class="headertexts" width="15%">', $txt['stats_new_posts'], '</th>
					<th class="headertexts" width="15%">', $txt['stats_new_members'], '</th>
					<th class="headertexts" width="15%">', $txt['smf_stats_14'], '</th>';

		if (!empty($modSettings['hitStats']))
			echo '
					<th class="headertexts">', $txt['page_views'], '</th>';

		echo '
				</tr>
			</thead>
			<tbody>';

		foreach ($context['yearly'] as $id => $year)
		{
			echo '
				<tr valign="middle" align="center" id="year_', $id, '" class="windowbg">
					<th width="25%" style="text-align: left;">
						<img id="year_img_', $id, '" src="', $settings['images_url'], '/collapse.gif" alt="*" /> <a href="#year_', $id, '" id="year_link_', $id, '">', $year['year'], '</a>
					</th>
					<th width="15%">', $year['new_topics'], '</th>
					<th width="15%">', $year['new_posts'], '</th>
					<th width="15%">', $year['new_members'], '</th>
					<th width="15%">', $year['most_members_online'], '</th>';

			if (!empty($modSettings['hitStats']))
				echo '
					<th>', $year['hits'], '</th>';

			echo '
				</tr>';

			foreach ($year['months'] as $month)
			{
				echo '
				<tr class="windowbg" valign="middle" align="center" id="tr_month_', $month['id'], '">
					<td style="text-align: left;">
						<img src="', $settings['images_url'], '/', $month['expanded'] ? 'collapse.gif' : 'expand.gif', '" alt="" id="img_', $month['id'], '" /> <a id="m', $month['id'], '" href="', $month['href'], '" onclick="return doingExpandCollapse;">', $month['month'], ' ', $month['year'], '</a>
					</th>
					<td width="15%">', $month['new_topics'], '</th>
					<td width="15%">', $month['new_posts'], '</th>
					<td width="15%">', $month['new_members'], '</th>
					<td width="15%">', $month['most_members_online'], '</th>';

				if (!empty($modSettings['hitStats']))
					echo '
					<td>', $month['hits'], '</td>';

				echo '
				</tr>';

				if ($month['expanded'])
				{
					foreach ($month['days'] as $day)
					{
						echo '
				<tr class="windowbg2" valign="middle" align="center" id="tr_day_', $day['year'], '-', $day['month'], '-', $day['day'], '">
					<td class="stats_day">', $day['year'], '-', $day['month'], '-', $day['day'], '</td>
					<td>', $day['new_topics'], '</td>
					<td>', $day['new_posts'], '</td>
					<td>', $day['new_members'], '</td>
					<td>', $day['most_members_online'], '</td>';

						if (!empty($modSettings['hitStats']))
							echo '
					<td>', $day['hits'], '</td>';

						echo '
				</tr>';
					}
				}
			}
		}

		echo '
			</tbody>
		</table>
		</div>
	</div>
	<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/stats.js"></script>
	<script type="text/javascript"><!-- // --><![CDATA[
		var oStatsCenter = new smf_StatsCenter({
			sTableId: \'stats\',

			reYearPattern: /year_(\d+)/,
			sYearImageCollapsed: \'expand.gif\',
			sYearImageExpanded: \'collapse.gif\',
			sYearImageIdPrefix: \'year_img_\',
			sYearLinkIdPrefix: \'year_link_\',

			reMonthPattern: /tr_month_(\d+)/,
			sMonthImageCollapsed: \'expand.gif\',
			sMonthImageExpanded: \'collapse.gif\',
			sMonthImageIdPrefix: \'img_\',
			sMonthLinkIdPrefix: \'m\',

			reDayPattern: /tr_day_(\d+-\d+-\d+)/,
			sDayRowClassname: \'windowbg2\',
			sDayRowIdPrefix: \'tr_day_\',

			aCollapsedYears: [';

		foreach ($context['collapsed_years'] as $id => $year)
		{
			echo '
				\'', $year, '\'', $id != count($context['collapsed_years']) - 1 ? ',' : '';
		}

		echo '
			],

			aDataCells: [
				\'date\',
				\'new_topics\',
				\'new_posts\',
				\'new_members\',
				\'most_members_online\'', empty($modSettings['hitStats']) ? '' : ',
				\'hits\'', '
			]
		});
	// ]]></script>';
	}
}

?>