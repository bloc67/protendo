<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function theme_main()
	{
		$this->theme_display_above();
		$this->theme_poll();
		$this->theme_events();
		$this->theme_posts();
		$this->theme_display_below();
	}

	function theme_display_above()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings, $topic;

		$context['blogtype'] = false;
		
		// Let them know, if their report was a success!
		if ($context['report_sent'])
			echo '
		<div class="info" id="profile_success">	', $txt['report_sent'], '</div>';

		// Show the anchor for the top and for the first message. If the first message is new, say so.
		echo '
		<a id="top"></a>
		<a id="msg', $context['first_message'], '"></a>', $context['first_new_message'] ? '<a id="new"></a>' : '';
	}

	function theme_display_below()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		if($context['blogtype'])
			return;
	}

	function theme_poll()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		// Is this topic also a poll?
		if ($context['is_poll'])
		{
			echo '
			<div id="poll">
				<h2 class="mainheader">', $txt['poll'], '</h2>
				<div class="headertexts">';

			// Is the clock ticking?
			if (!empty($context['poll']['expire_time']))
				echo '
					<div class="floatright"><strong>', ($context['poll']['is_expired'] ? $txt['poll_expired_on'] : $txt['poll_expires_on']), ':</strong> ', $context['poll']['expire_time'], '</div>';

			echo '
					', $context['poll']['question'], '
				</div>
				<div class="windowbg">
					<div id="poll_options">';

			// Are they not allowed to vote but allowed to view the options?
			if ($context['poll']['show_results'] || !$context['allow_vote'])
			{
				// Show each option with its corresponding percentage bar.
				foreach ($context['poll']['options'] as $option)
				{
					echo '
						<div class="bwgrid">
							<div class="bwcell5">
								<div class="middletext" style="text-align: right; padding-right: 1em;">', $option['option'], '</div>
							</div>';

					if ($context['allow_poll_view'])
						echo '
							<div class="bwcell5">
								<div class="middletext statsbar', $option['voted_this'] ? ' voted' : '', '">', !empty($option['bar_ndt']) ? $option['bar_ndt'] : '&nbsp;' , '	</div>
							</div>
							<div class="bwcell6">
								<div style="padding-left: 1em;" class="smalltext">', $option['votes'], ' (', $option['percent'], '%)</div>
							</div>';

					echo '
						</div>';
				}

				if ($context['allow_poll_view'])
					echo '
						<hr><p class="smalltext"><span class="bar_my" style="width: 12px; height: 12px; display: inline-block; vertical-align: middle;"><span style="width: 100%; height: 12px;"></span></span> ' , $txt['myvote'] , '&nbsp;- ', $txt['poll_total_voters'], ': ', $context['poll']['total_votes'], '</p>';
			}
			// They are allowed to vote! Go to it!
			else
			{
				echo '
						<form action="', $scripturl, '?action=vote;topic=', $context['current_topic'], '.', $context['start'], ';poll=', $context['poll']['id'], '" method="post" accept-charset="', $context['character_set'], '">';

				// Show a warning if they are allowed more than one option.
				if ($context['poll']['allowed_warning'])
					echo '
							<p class="smallpadding">', $context['poll']['allowed_warning'], '</p>';

				echo '
							<ul class="reset">';

				// Show each option with its button - a radio likely.
				foreach ($context['poll']['options'] as $option)
					echo '
								<li class="middletext">', $option['vote_button'], ' <label for="', $option['id'], '">', $option['option'], '</label></li>';

				echo '
							</ul><br />
							<div class="submitbutton">
								<input type="submit" value="', $txt['poll_vote'], '" class="button_submit" />
								<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
							</div>
						</form>';
			}
			echo '
					</div>
				</div>
				<div id="pollmoderation">';

			// Build the poll moderation button array.
			$poll_buttons = array(
				'vote' => array('test' => 'allow_return_vote', 'text' => 'poll_return_vote', 'image' => 'poll_options.gif', 'lang' => true, 'url' => $scripturl . '?topic=' . $context['current_topic'] . '.' . $context['start']),
				'results' => array('test' => 'show_view_results_button', 'text' => 'poll_results', 'image' => 'poll_results.gif', 'lang' => true, 'url' => $scripturl . '?topic=' . $context['current_topic'] . '.' . $context['start'] . ';viewresults'),
				'change_vote' => array('test' => 'allow_change_vote', 'text' => 'poll_change_vote', 'image' => 'poll_change_vote.gif', 'lang' => true, 'url' => $scripturl . '?action=vote;topic=' . $context['current_topic'] . '.' . $context['start'] . ';poll=' . $context['poll']['id'] . ';' . $context['session_var'] . '=' . $context['session_id'], 'active' => true),
				'lock' => array('test' => 'allow_lock_poll', 'text' => (!$context['poll']['is_locked'] ? 'poll_lock' : 'poll_unlock'), 'image' => 'poll_lock.gif', 'lang' => true, 'url' => $scripturl . '?action=lockvoting;topic=' . $context['current_topic'] . '.' . $context['start'] . ';' . $context['session_var'] . '=' . $context['session_id']),
				'edit' => array('test' => 'allow_edit_poll', 'text' => 'poll_edit', 'image' => 'poll_edit.gif', 'lang' => true, 'url' => $scripturl . '?action=editpoll;topic=' . $context['current_topic'] . '.' . $context['start']),
				'remove_poll' => array('test' => 'can_remove_poll', 'text' => 'poll_remove', 'image' => 'admin_remove_poll.gif', 'lang' => true, 'custom' => 'onclick="return confirm(\'' . $txt['poll_remove_warn'] . '\');"', 'url' => $scripturl . '?action=removepoll;topic=' . $context['current_topic'] . '.' . $context['start'] . ';' . $context['session_var'] . '=' . $context['session_id']),
			);

			$context['themeobject']->theme_button_strip($poll_buttons, 'float: right;');

			echo '
					<br style="clear: both;" /><hr>
				</div><br>';
		}
	}
	function theme_events()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		// Does this topic have some events linked to it?
		if (!empty($context['linked_calendar_events']))
		{
			echo '
				<div class="linked_events">
					<h3 class="titlebg headerpadding">', $txt['calendar_linked_events'], '</h3>
					<div class="windowbg">
						<div class="content">
							<ul class="reset">';

			foreach ($context['linked_calendar_events'] as $event)
				echo '
								<li>
									', ($event['can_edit'] ? '<a href="' . $event['modify_href'] . '"> <img src="' . $settings['images_url'] . '/icons/modify_small.gif" alt="" title="' . $txt['modify'] . '" class="edit_event" /></a> ' : ''), '<strong>', $event['title'], '</strong>: ', $event['start_date'], ($event['start_date'] != $event['end_date'] ? ' - ' . $event['end_date'] : ''), '
								</li>';

			echo '
							</ul>
						</div>
					</div>
				</div>';
		}
	}

	function theme_posts()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		if($context['blogtype'])
			return;

		echo '
				<div class="floatright"> ', $context['themeobject']->theme_button_strip($context['normal_buttons'], '',true), '</div>
				<h2 class="mainheader">' , $context['topicname'] , '</h2>
				<div class="headertexts">', $txt['read'], ' ', $context['num_views'], ' ', $txt['times'], '</div>';

		// show its sticky/locked/unapproved
		if (!empty($settings['display_who_viewing']))
		{
			echo '
				<div class="headerarea">';

			// Show just numbers...?
			if ($settings['display_who_viewing'] == 1)
					echo count($context['view_members']), ' ', count($context['view_members']) == 1 ? $txt['who_member'] : $txt['members'];
			// Or show the actual people viewing the topic?
			else
				echo empty($context['view_members_list']) ? '0 ' . $txt['members'] : implode(', ', $context['view_members_list']) . ((empty($context['view_num_hidden']) || $context['can_moderate_forum']) ? '' : ' (+ ' . $context['view_num_hidden'] . ' ' . $txt['hidden'] . ')');

			// Now show how many guests are here too.
			echo $txt['who_and'], $context['view_num_guests'], ' ', $context['view_num_guests'] == 1 ? $txt['guest'] : $txt['guests'], $txt['who_viewing_topic'], '
				</div>';
		}

		echo '
				<div class="pagelinks">
					<div class="floatright">' , $context['previous_next'], '</div>
					', $context['page_index'], !empty($modSettings['topbottomEnable']) ? $context['menu_separator'] . ' &nbsp;&nbsp;<a href="#lastPost"><strong>' . $txt['go_down'] . '</strong></a>' : '', '
				</div>
				<form action="', $scripturl, '?action=quickmod2;topic=', $context['current_topic'], '.', $context['start'], '" method="post" accept-charset="', $context['character_set'], '" name="quickModForm" id="quickModForm" style="margin: 0;" onsubmit="return oQuickModify.bInEditMode ? oQuickModify.modifySave(\'' . $context['session_id'] . '\', \'' . $context['session_var'] . '\') : false">';

		$context['ignoredMsgs'] = array();
		$context['removableMessageIDs'] = array();
		$alternate = false;
		$first = true;
		// Get all the messages...
		while ($message = $context['get_message']())
		{
			$ignoring = false;
			$alternate = !$alternate;
			if ($message['can_remove'])
				$context['removableMessageIDs'][] = $message['id'];

			// Are we ignoring this message?
			if (!empty($message['is_ignored']))
			{
				$ignoring = true;
				$context['ignoredMsgs'][] = $message['id'];
			}

			// Show the message anchor and a "new" anchor if this message is new.
			if ($message['id'] != $context['first_message'])
				echo '
					<a id="msg', $message['id'], '" class="relative"></a>', $message['first_new'] ? '<a id="new"></a>' : '';

			echo '
					<div class="', $message['approved'] ? ($message['alternate'] == 0 ? 'windowbg' : 'windowbg2') : 'approvebg', ' ' , !$first ? 'bxwindows'  : 'bxwindows_first' , '">
						<div class="bwgrid">
							<div class="bwcell2">
								<div class="membercolumn">
									<a href="', $message['member']['href'], '"><b>' , $message['member']['name'] , '</b></a><br>';
			$first = false;

			if (!empty($message['member']['group']))
				echo '		
									<span class="smalltext">'. $message['member']['group'], '</span><br />';

			if (!empty($settings['show_user_images']) && empty($options['show_no_avatars']) && !empty($message['member']['avatar']['image']))
				echo '
									<div class="avatar_full" >
										<a id="avvy'.$message['id'].'" title="' . $message['member']['name'] . '" class="mPopup" popupid="memberinfo'. $message['id'] .'">
											', $message['member']['avatar']['image'], '
										</a>
									</div>';

			if (!empty($modSettings['onlineEnable']) && !$message['member']['is_guest'])
				echo '
									<div>', $context['can_send_pm'] ? '<a href="' . $message['member']['online']['href'] . '" title="' . $message['member']['online']['label'] . '" >' : '', '<span class="label_' . ($message['member']['online']['is_online'] ? 'online' : 'offline' ) . ' overlay_avatar"><b>' . $txt[($message['member']['online'] ? 'online' : 'offline' )] . '</b></span>', $context['can_send_pm'] ? '</a>' : '', '</div>';

			// stuff from here goes into the popup	
			echo '			
								</div>
								<div id="memberinfo'. $message['id'] .'" style="display: none;">';
			
			if (!empty($message['member']['title']))
				echo $message['member']['title'], '<br />';

			// Don't show these things for guests.
			if (!$message['member']['is_guest'])
			{
				if ((empty($settings['hide_post_group']) || $message['member']['group'] == '') && $message['member']['post_group'] != '')
					echo $message['member']['post_group'], '<br />';
				
				echo $message['member']['group_stars'], '<br />';

				// Show how many posts they have made.
				if (!isset($context['disabled_fields']['posts']))
					echo '<a href="' . $scripturl . '?actio=profile;area=showposts;u=', $message['member']['id'] , '">',$txt['member_postcount'], ': ', $message['member']['posts'], '</a><br />';


				// Show the member's gender icon?
				if (!empty($settings['show_gender']) && $message['member']['gender']['image'] != '' && !isset($context['disabled_fields']['gender']))
					echo $txt['gender'], ': ', $message['member']['gender']['text'], '<br />';

				// Show their personal text?
				if (!empty($settings['show_blurb']) && $message['member']['blurb'] != '')
					echo $message['member']['blurb'], '<br />';

		
				echo '
				<hr>';
				
				// This shows the popular messaging icons.
				if ($message['member']['has_messenger'] && $message['member']['can_view_profile'])
					echo '
								', !empty($message['member']['facebook']['link']) ? $message['member']['facebook']['link']  : '', '
								', !empty($message['member']['skype']['link']) ? $message['member']['skype']['link'] : '', '
								', !empty($message['member']['twitter']['link']) ? $message['member']['twitter']['link'] : '', '
								', !empty($message['member']['google']['link']) ? $message['member']['google']['link'] : '';

				// Show the profile, website, email address, and personal message buttons.
				if ($settings['show_profile_buttons'])
				{
						// Don't show the profile button if you're not allowed to view the profile.
					if ($message['member']['can_view_profile'])
						echo '<a href="', $message['member']['href'], '" title="' . $txt['view_profile'] . '" /><span class="social_icons profile_icon"><span></span></span></a>';

					// Don't show an icon if they haven't specified a website.
					if ($message['member']['website']['url'] != '' && !isset($context['disabled_fields']['website']))
						echo '<a href="', $message['member']['website']['url'], '" title="' . $message['member']['website']['title'] . '" target="_blank" class="new_win"><span class="social_icons website_icon"></span></a>';

					// Don't show the email address if they want it hidden.
					if (in_array($message['member']['show_email'], array('yes', 'yes_permission_override', 'no_through_forum')))
						echo '<a href="', $scripturl, '?action=emailuser;sa=email;msg=', $message['id'], '" rel="nofollow" title="' . $txt['email'] . '" /><span class="social_icons email_icon"></span></a>';

				}

				// Are we showing the warning status?
				if ($message['member']['can_see_warning'])
					echo '
								', $context['can_issue_warning'] ? '<a href="' . $scripturl . '?action=profile;area=issuewarning;u=' . $message['member']['id'] . '">' : '', $txt['user_warn_' . $message['member']['warning_status']], $context['can_issue_warning'] ? '</a>' : '', '<span class="warn_', $message['member']['warning_status'], '">', $txt['warn_' . $message['member']['warning_status']], '</span>';
			}
			// Otherwise, show the guest's email.
			elseif (!empty($message['member']['email']) && in_array($message['member']['show_email'], array('yes', 'yes_permission_override', 'no_through_forum')))
				echo '
									<a href="', $scripturl, '?action=emailuser;sa=email;msg=', $message['id'], '" rel="nofollow"><span class="social email"><span>EM</span></span></a>';

			echo '	
							</div>
						</div>
						<div class="bwcell14">
							<div style="padding-left: 2em;">
								<span class="greytext">' , $message['member']['name'],  ' ' , $txt['wrote'] , ' ' , blogformat($message['timestamp']) , '</span>
								<br>';
									
			// Ignoring this user? Hide the post.
			if ($ignoring)
				echo '
								<div class="info" id="msg_', $message['id'], '_ignored_prompt">
									', $txt['ignoring_user'], '
									<a href="#" id="msg_', $message['id'], '_ignored_link" style="display: none;">', $txt['show_ignore_user_post'], '</a>
								</div>';

			// Show the post itself, finally!
			echo '			
								<div class="post">';

			if (!$message['approved'] && $message['member']['id'] != 0 && $message['member']['id'] == $context['user']['id'])
				echo '
									<div class="info">', $txt['post_awaiting_approval'], '</div>';
			echo '
									<div class="inner" id="msg_', $message['id'], '"', '>', $message['body'], '</div>
								</div>';

			// Assuming there are attachments...
			if (!empty($message['attachment']))
			{
				echo '<br>';
				$last_approved_state = 1;
				foreach ($message['attachment'] as $attachment)
				{
					if (!$attachment['is_image'])
						continue;
					
						echo '
								<div class="floatleft">
									<div style="margin: 4px;">';

					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz TipBig" title="' , $attachment['name'], ' :: ' , $txt['filesize'] , ': ', $attachment['size'], $attachment['is_image'] ? '<br>' . $txt['width'].'/'. $txt['height'] . ': ' . $attachment['real_width'] . 'x' . $attachment['real_height']  : '', '<br>' . $txt['downloads']. ': '. $attachment['downloads'] . '<br>"><img src="', $attachment['micro']['href'], '"  alt="" id="thumb_', $attachment['id'], '" /></a>';
					else
						echo '
										<img src="' . $attachment['href'] . '" class="TipBig" alt="" title="' , $attachment['name'], ' :: ' , $txt['filesize'] , ': ', $attachment['size'], $attachment['is_image'] ? '<br>' . $txt['width'].'/'. $txt['height'] . ': ' . $attachment['real_width'] . 'x' . $attachment['real_height']  : '', '<br>' . $txt['downloads']. ': '. $attachment['downloads'] . '<br>" />';
		
				
					echo '
									</div>
								</div>';
				}

				echo '		
								<br style="clear: both;" />';
				// the other things
				foreach ($message['attachment'] as $attachment)
				{
					if ($attachment['is_image'])
						continue;

					echo '
								<div class="floatleft">
									<div style="margin: 8px 2px; padding: 0.5em; border: groove 2px #fff;">
										<a href="' . $attachment['href_safe'] . '"><span class="atts atts_' ,  $attachment['ext'], '"></span></a><br>';

					
					echo '
										<span class="smalltext"><b>' . $attachment['name'] . '</b><br>', $attachment['size'], $attachment['is_image'] ? ', ' . $attachment['real_width'] . 'x' . $attachment['real_height']  : '', '
										| ' . $attachment['downloads'] . ' </span>
									</div>
								</div>';
				}
			}
			echo '
								<hr style="margin-bottom: 8px; clear: both; ">';
			echo '
							<ul class="quickbuttons floatleft" style="padding-left: 4px;">';

			// Maybe we can approve it, maybe we should?
			if ($message['can_approve'])
				echo '
									<li class="approve_button"><a href="', $scripturl, '?action=moderate;area=postmod;sa=approve;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $message['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a></li>';
			// Can they reply? Have they turned on quick reply?
			if ($context['can_quote'])
				echo '
									<li class="quote_button"><a href="', $scripturl, '?action=post;quote=', $message['id'], ';topic=', $context['current_topic'], '.', $context['start'], ';last_msg=', $context['topic_last_message'], '"><span>', $txt['quote'], '</span></a></li>';
			// Can the user modify the contents of this post?
			if ($message['can_modify'])
				echo '
									<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $message['id'], ';topic=', $context['current_topic'], '.', $context['start'], '">', $txt['modify'], '</a></li>';
			// How about... even... remove it entirely?!
			if ($message['can_remove'])
				echo '
									<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $message['id'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');">', $txt['remove'], '</a></li>';
			// What about splitting it off the rest of the topic?
			if ($context['can_split'] && !empty($context['real_num_replies']))
				echo '
									<li class="split_button"><a href="', $scripturl, '?action=splittopics;topic=', $context['current_topic'], '.0;at=', $message['id'], '">', $txt['split'], '</a></li>';
			// Can we restore topics?
			if ($context['can_restore_msg'])
				echo '
									<li class="restore_button"><a href="', $scripturl, '?action=restoretopic;msgs=', $message['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['restore_message'], '</a></li>';
			// Show a checkbox for quick moderation?
			if (!empty($options['display_quick_mod']) && $options['display_quick_mod'] == 1 && $message['can_remove'])
				echo '
									<li class="inline_mod_check" style="display: none;" id="in_topic_mod_check_', $message['id'], '"></li>';
			// last edit
			if ($settings['show_modify'] && !empty($message['modified']['name']))
				echo '
								<li>
									<a id="edit' . $message['id'] . '"><span class="TipBig breadcrumb_style"><span class="social_icons report_icon"></span></span>
									'. $txt['last_edit']. ' '. $message['modified']['time']. ' '. $txt['by']. ' '. $message['modified']['name']. '</span>
									</a>
								</li>';

			echo '
								</ul>
								<ul class="quickbuttons floatright" style="margin-top: 0;">';

			// Maybe they want to report this post to the moderator(s)?
			if ($context['can_report_moderator'])
				echo '
									<li><a href="', $scripturl, '?action=reporttm;topic=', $context['current_topic'], '.', $message['counter'], ';msg=', $message['id'], '">' , $txt['report_to_mod'], '</a></li>';

			// Can we issue a warning because of this post?  Remember, we can't give guests warnings.
			if ($context['can_issue_warning'] && !$message['is_message_author'] && !$message['member']['is_guest'])
				echo '
									<li><a href="', $scripturl, '?action=profile;area=issuewarning;u=', $message['member']['id'], ';msg=', $message['id'], '">', $txt['issue_warning_post'], '</a></li>';

			// Show the IP to this user for this post - because you can moderate?
			if ($context['can_moderate_forum'] && !empty($message['member']['ip']))
				echo '
									<li><a href="', $scripturl, '?action=', !empty($message['member']['is_guest']) ? 'trackip' : 'profile;area=tracking;sa=ip;u=' . $message['member']['id'], ';searchip=', $message['member']['ip'], '">', $message['member']['ip'], '</a></li>';
			// Or, should we show it because this is you?
			elseif ($message['can_see_ip'])
				echo '
									<li><a href="', $scripturl, '?action=helpadmin;help=see_member_ip">', $message['member']['ip'], '</a></li>';
			// Okay, are you at least logged in?  Then we can show something about why IPs are logged...
			elseif (!$context['user']['is_guest'])
				echo '
									<li><a href="', $scripturl, '?action=helpadmin;help=see_member_ip">', $txt['logged'], '</a></li>';
			// Otherwise, you see NOTHING!
			else
				echo '	
									<li>', $txt['logged'],'</li>';

			echo '
								</ul>
								<br style="clear: both;" />
							</div>
						</div>
					</div>
				</div>';
		}
		echo '
			</form>
			<a id="lastPost"></a>
			<div class="pagelinks">
				<div class="floatright">', $context['previous_next'], '</div>
				', $context['page_index'], !empty($modSettings['topbottomEnable']) ? $context['menu_separator'] . ' &nbsp;&nbsp;<a href="#top"><strong>' . $txt['go_up'] . '</strong></a>' : '', '
			</div>';

		// Show the lower breadcrumbs.
		echo '
			<div style="overflow: hidden;">
				<div class="floatright">', $context['themeobject']->theme_button_strip($context['normal_buttons'], 'right'),'</div>
				<div id="display_jump_to">&nbsp;</div>
			</div><hr>
			<div id="moderationbuttons">', $context['themeobject']->theme_button_strip($context['mod_buttons'], 'bottom', array('id' => 'moderationbuttons_strip')), '</div>
		';

		if ($context['can_reply'])
		{
			echo '<br>
			<a id="quickreply"></a>
			<div id="quickreplybox">
				<h3 class="mainheader">', $txt['quick_reply'], '</h3>
				<div id="quickReplyOptions">
					', $context['is_locked'] ? '<p class="infobox smalltext">' . $txt['quick_reply_warning'] . '</p>' : '',
					$context['oldTopicError'] ? '<p class="infobox smalltext">' . sprintf($txt['error_old_topic'], $modSettings['oldTopicDays']) . '</p>' : '', '
					', $context['can_reply_approved'] ? '' : '<em>' . $txt['wait_for_approval'] . '</em>', '
					', !$context['can_reply_approved'] && $context['require_verification'] ? '<br />' : '', '
					<form action="', $scripturl, '?board=', $context['current_board'], ';action=post2" method="post" accept-charset="', $context['character_set'], '" name="postmodify" id="postmodify" onsubmit="submitonce(this);" style="margin: 0;">
						<input type="hidden" name="topic" value="', $context['current_topic'], '" />
						<input type="hidden" name="subject" value="', $context['response_prefix'], $context['subject'], '" />
						<input type="hidden" name="icon" value="xx" />
						<input type="hidden" name="from_qr" value="1" />
						<input type="hidden" name="notify" value="', $context['is_marked_notify'] || !empty($options['auto_notify']) ? '1' : '0', '" />
						<input type="hidden" name="not_approved" value="', !$context['can_reply_approved'], '" />
						<input type="hidden" name="goback" value="', empty($options['return_to_post']) ? '0' : '1', '" />
						<input type="hidden" name="last_msg" value="', $context['topic_last_message'], '" />
						<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
						<input type="hidden" name="seqnum" value="', $context['form_sequence_number'], '" />';


				// Is visual verification enabled?
				if ($context['require_verification'])
					echo '
						<strong>', $txt['verification'], ':</strong>', theme_control_verification($context['visual_verification_id'], 'quick_reply'), '<br />';

				echo '
						<div class="quickReplyContent">
							', theme_control_richedit($context['post_box_name'], 'smileyBox_message', 'bbcBox_message'),'
						</div>
						<div style="margin-top: 10px;">
							<input type="submit" name="post" value="', $txt['post'], '" onclick="return submitThisOnce(this);" accesskey="s" tabindex="', $context['tabindex']++, '" class="button_submit" />
							<input type="submit" name="preview" value="', $txt['preview'], '" onclick="return submitThisOnce(this);" accesskey="p" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				if ($context['show_spellchecking'])
					echo '
							<input type="button" value="', $txt['spell_check'], '" onclick="spellCheck(\'postmodify\', \'message\');" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				echo '			
						</div>
					</form>
				</div>
			</div>';
		}
		if ($context['show_spellchecking'])
			echo '
			<form action="', $scripturl, '?action=spellcheck" method="post" accept-charset="', $context['character_set'], '" name="spell_form" id="spell_form" target="spellWindow"><input type="hidden" name="spellstring" value="" /></form>
			<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/spellcheck.js"></script>';

		echo '
			<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/topic.js"></script>
			<script type="text/javascript"><!-- // --><![CDATA[';

		if (!empty($options['display_quick_mod']) && $options['display_quick_mod'] == 1 && $context['can_remove_post'])
			echo '
						var oInTopicModeration = new InTopicModeration({
							sSelf: \'oInTopicModeration\',
							sCheckboxContainerMask: \'in_topic_mod_check_\',
							aMessageIds: [\'', implode('\', \'', $context['removableMessageIDs']), '\'],
							sSessionId: \'', $context['session_id'], '\',
							sSessionVar: \'', $context['session_var'], '\',
							sButtonStrip: \'moderationbuttons\',
							sButtonStripDisplay: \'moderationbuttons_strip\',
							bUseImageButton: false,
							bCanRemove: ', $context['can_remove_post'] ? 'true' : 'false', ',
							sRemoveButtonLabel: \'', $txt['quickmod_delete_selected'], '\',
							sRemoveButtonImage: \'delete_selected.gif\',
							sRemoveButtonConfirm: \'', $txt['quickmod_confirm'], '\',
							bCanRestore: ', $context['can_restore_msg'] ? 'true' : 'false', ',
							sRestoreButtonLabel: \'', $txt['quick_mod_restore'], '\',
							sRestoreButtonImage: \'restore_selected.gif\',
							sRestoreButtonConfirm: \'', $txt['quickmod_confirm'], '\',
							sFormId: \'quickModForm\'
						});';

		echo '
						if (\'XMLHttpRequest\' in window)
						{
							var oQuickModify = new QuickModify({
								sScriptUrl: smf_scripturl,
								bShowModify: ', $settings['show_modify'] ? 'true' : 'false', ',
								iTopicId: ', $context['current_topic'], ',
								sTemplateBodyEdit: ', JavaScriptEscape('
									<div id="quick_edit_body_container" style="width: 90%">
										<div id="error_box" style="padding: 4px;" class="error"></div>
										<textarea class="editor" name="message" rows="12" style="' . ($context['browser']['is_ie8'] ? 'width: 635px; max-width: 100%; min-width: 100%' : 'width: 100%') . '; margin-bottom: 10px;" tabindex="' . $context['tabindex']++ . '">%body%</textarea><br />
										<input type="hidden" name="' . $context['session_var'] . '" value="' . $context['session_id'] . '" />
										<input type="hidden" name="topic" value="' . $context['current_topic'] . '" />
										<input type="hidden" name="msg" value="%msg_id%" />
										<div class="righttext">
											<input type="submit" name="post" value="' . $txt['save'] . '" tabindex="' . $context['tabindex']++ . '" onclick="return oQuickModify.modifySave(\'' . $context['session_id'] . '\', \'' . $context['session_var'] . '\');" accesskey="s" class="button_submit" />&nbsp;&nbsp;' . ($context['show_spellchecking'] ? '<input type="button" value="' . $txt['spell_check'] . '" tabindex="' . $context['tabindex']++ . '" onclick="spellCheck(\'quickModForm\', \'message\');" class="button_submit" />&nbsp;&nbsp;' : '') . '<input type="submit" name="cancel" value="' . $txt['modify_cancel'] . '" tabindex="' . $context['tabindex']++ . '" onclick="return oQuickModify.modifyCancel();" class="button_submit" />
										</div>
									</div>'), ',
								sTemplateSubjectEdit: ', JavaScriptEscape('<input type="text" style="width: 90%;" name="subject" value="%subject%" size="80" maxlength="80" tabindex="' . $context['tabindex']++ . '" class="input_text" />'), ',
								sTemplateBodyNormal: ', JavaScriptEscape('%body%'), ',
								sTemplateSubjectNormal: ', JavaScriptEscape('<a href="' . $scripturl . '?topic=' . $context['current_topic'] . '.msg%msg_id%#msg%msg_id%" rel="nofollow">%subject%</a>'), ',
								sTemplateTopSubject: ', JavaScriptEscape($txt['topic'] . ': %subject% &nbsp;(' . $txt['read'] . ' ' . $context['num_views'] . ' ' . $txt['times'] . ')'), ',
								sErrorBorderStyle: ', JavaScriptEscape('1px solid red'), '
							});

							aJumpTo[aJumpTo.length] = new JumpTo({
								sContainerId: "display_jump_to",
								sJumpToTemplate: "<label class=\"smalltext\" for=\"%select_id%\">', $context['jump_to']['label'], ':<" + "/label> %dropdown_list%",
								iCurBoardId: ', $context['current_board'], ',
								iCurBoardChildLevel: ', $context['jump_to']['child_level'], ',
								sCurBoardName: "', $context['jump_to']['board_name'], '",
								sBoardChildLevelIndicator: "==",
								sBoardPrefix: "=> ",
								sCatSeparator: "-----------------------------",
								sCatPrefix: "",
								sGoButtonLabel: "', $txt['go'], '"
							});
						}';

		if (!empty($context['ignoredMsgs']))
		{
			echo '
						var aIgnoreToggles = new Array();';

			foreach ($context['ignoredMsgs'] as $msgid)
			{
				echo '
						aIgnoreToggles[', $msgid, '] = new smc_Toggle({
							bToggleEnabled: true,
							bCurrentlyCollapsed: true,
							aSwappableContainers: [
								\'msg_', $msgid, '_extra_info\',
								\'msg_', $msgid, '\',
								\'msg_', $msgid, '_footer\',
								\'msg_', $msgid, '_quick_mod\',
								\'modify_button_', $msgid, '\',
								\'msg_', $msgid, '_signature\'

							],
							aSwapLinks: [
								{
									sId: \'msg_', $msgid, '_ignored_link\',
									msgExpanded: \'\',
									msgCollapsed: ', JavaScriptEscape($txt['show_ignore_user_post']), '
								}
							]
						});';
			}
		}

		echo '
					// ]]></script>';
	}

	function theme_blog_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl, $modSettings;


		if(!empty($context['frontblog'][0]['replies']))
		{
			echo '
			<br>
			<h2 class="mainheader">' , $txt['comments'] , '</h2>
			<div class="pagelinks">' , $context['page_index'] , '</div>
			<div id="comments">';
			
			$alt = true;
			// Get all the messages...
			while ($orig = $context['get_message']())
			{
				if($orig['id'] == $context['frontblog'][0]['id_msg'])
					continue;

				echo '
				<div class="bwgrid" style="clear: both;" id="msg', $orig['id'], '">
					<div class="bwcell1"' , !empty($orig['first_new']) ? ' id="new"' : '' , '>&nbsp;';


				// Assuming there are attachments...
				if (!empty($orig['attachment']))
				{
					echo '
						<div id="msg_', $orig['id'], '_footer" class="smalltext">
							<div>';

					$last_approved_state = 1;
					foreach ($orig['attachment'] as $attachment)
					{
						// Show a special box for unapproved attachments...
						if ($attachment['is_approved'] != $last_approved_state)
						{
							$last_approved_state = 0;
							echo '
								<fieldset>
									<legend>', $txt['attach_awaiting_approve'];

							if ($context['can_approve'])
								echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

							echo '
									</legend>';
						}

						if ($attachment['is_image'])
						{
							if ($attachment['thumbnail']['has_thumb'])
								echo '
									<a href="', $attachment['href'], '" class="remooz"><img style="height: 40px; " src="', $attachment['thumbnail']['href'], '" alt=""  /></a>&nbsp;';
							else
								echo '
									<img src="' . $attachment['href'] . '" alt="" style="height: 40px;" />';
						}
						else
							echo '
									<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

						if (!$attachment['is_approved'] && $context['can_approve'])
							echo '
									[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
					}

					// If we had unapproved attachments clean up.
					if ($last_approved_state == 0)
						echo '
								</fieldset>';

					echo '
							</div>
						</div>';
				}
				echo '
					</div>
					<div class="bwcell15">
						<div>
							<div>';

				if(empty($orig['member']['avatar']['image']))
					$orig['member']['avatar']['image'] = '<img class="avatar" src="'. $settings['images_url']. '/guest.png'. '" alt="" />';

				echo '
								<div class="comment' , $alt ? '2' : '' , (!empty($author) && $author==$orig['member']['id']) ? '_auth' : '' , '">
									<div class="smalltext greytext">
										<div class="floatleft comment_avatar"><a href="' , $orig['member']['href'] , '">' , $orig['member']['avatar']['image'] , '</a></div>
										<a href="' , $orig['member']['href'] , '">' , $orig['member']['name'] , '</a> ' , $txt['wrote'] , ' ', blogformat($orig['timestamp']) , ' 
									</div>
									<hr style="margin-bottom: 0;" />
									<div class="post">' , $orig['body'];
				
				// add something plugin specific, if set
				if(function_exists('plugin_extras'))
					plugin_extras($orig['id'], $orig['member']['id']);

				echo '
									</div>
								</div>';
				
				$alt = !$alt;
				
				echo '
								<ul class="quickbuttons" style="float: right; ">';

				// Maybe we can approve it, maybe we should?
				if ($orig['can_approve'])
					echo '
									<li class="approve_button"><a href="', $scripturl, '?action=moderate;area=postmod;sa=approve;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '"><span>', $txt['approve'], '</span></a></li>';

				// So... quick reply is off, but they *can* reply?
				if ($context['can_quote'])
					echo '
									<li class="quote_button"><a href="', $scripturl, '?action=post;quote=', $orig['id'], ';topic=', $context['current_topic'], '.', $context['start'], ';last_msg=', $context['topic_last_message'], '"><span>', $txt['quote'], '</span></a></li>';

				// Can the user modify the contents of this post?
				if ($orig['can_modify'])
					echo '
									<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

				// How about... even... remove it entirely?!
				if ($orig['can_remove'])
					echo '
									<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

				// What about splitting it off the rest of the topic?
				if ($context['can_split'] && !empty($context['real_num_replies']))
					echo '
									<li class="split_button"><a href="', $scripturl, '?action=splittopics;topic=', $context['current_topic'], '.0;at=', $orig['id'], '"><span>', $txt['split'], '</span></a></li>';

				echo '
								</ul>
							</div>
						</div>
					</div>
				</div>';
			}
			echo '
			</div>';
		}	
		if ($context['can_reply'])
		{
			echo '
			<br>
			<a id="quickreply"></a>
			<div  id="quickreplybox">
				<h3 class="mainheader">', $txt['quick_reply'], '</h3>
				', $context['is_locked'] ? '<p class="alert smalltext">' . $txt['quick_reply_warning'] . '</p>' : '',
				$context['oldTopicError'] ? '<p class="alert smalltext">' . sprintf($txt['error_old_topic'], $modSettings['oldTopicDays']) . '</p>' : '', '
				', $context['can_reply_approved'] ? '' : '<p><em>' . $txt['wait_for_approval'] . '</em></p>', '
				<hr>
				<div id="quickReplyOptions">
					<form action="', $scripturl, '?board=', $context['current_board'], ';action=post2" method="post" accept-charset="', $context['character_set'], '" name="postmodify" id="postmodify" onsubmit="submitonce(this);" style="margin: 0;">
						<input type="hidden" name="topic" value="', $context['current_topic'], '" />
						<input type="hidden" name="subject" value="', $context['response_prefix'], $context['subject'], '" />
						<input type="hidden" name="icon" value="xx" />
						<input type="hidden" name="from_qr" value="1" />
						<input type="hidden" name="notify" value="', $context['is_marked_notify'] || !empty($options['auto_notify']) ? '1' : '0', '" />
						<input type="hidden" name="not_approved" value="', !$context['can_reply_approved'], '" />
						<input type="hidden" name="goback" value="', empty($options['return_to_post']) ? '0' : '1', '" />
						<input type="hidden" name="last_msg" value="', $context['topic_last_message'], '" />
						<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />';

				// Guests just need more.
				if ($context['user']['is_guest'])
					echo '
						<strong>', $txt['name'], ':</strong> <input type="text" name="guestname" value="', $context['name'], '" size="25" class="input_text" tabindex="', $context['tabindex']++, '" />
						<strong>', $txt['email'], ':</strong> <input type="text" name="email" value="', $context['email'], '" size="25" class="input_text" tabindex="', $context['tabindex']++, '" /><br />';

				// Is visual verification enabled?
				if ($context['require_verification'])
					echo '
						<strong>', $txt['verification'], ':</strong>', theme_control_verification($context['visual_verification_id'], 'quick_reply'), '<br />';

				echo '
						<div class="quickReplyContent">
							', theme_control_richedit($context['post_box_name'], 'smileyBox_message', 'bbcBox_message'),'
						</div>
						<div class="righttext padding" style="margin-top: 10px;">
							<input type="submit" name="post" value="', $txt['post'], '" onclick="return submitThisOnce(this);" accesskey="s" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				if ($context['show_spellchecking'])
					echo '
							<input type="button" value="', $txt['spell_check'], '" onclick="spellCheck(\'postmodify\', \'message\');" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				echo '			
						</div><br>
					</form>
				</div>
			</div>';
		}
		else
			echo '
			<br class="clear" />';

		if ($context['show_spellchecking'])
			echo '
			<form action="', $scripturl, '?action=spellcheck" method="post" accept-charset="', $context['character_set'], '" name="spell_form" id="spell_form" target="spellWindow"><input type="hidden" name="spellstring" value="" /></form>
				<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/spellcheck.js"></script>';

		echo '
				<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/topic.js"></script>
				<script type="text/javascript"><!-- // --><![CDATA[';

		echo '
						if (\'XMLHttpRequest\' in window)
						{
							var oQuickModify = new QuickModify({
								sScriptUrl: smf_scripturl,
								bShowModify: ', $settings['show_modify'] ? 'true' : 'false', ',
								iTopicId: ', $context['current_topic'], ',
								sTemplateBodyEdit: ', JavaScriptEscape('
									<div id="quick_edit_body_container" style="width: 90%">
										<div id="error_box" style="padding: 4px;" class="error"></div>
										<textarea class="editor" name="message" rows="12" style="' . ($context['browser']['is_ie8'] ? 'width: 635px; max-width: 100%; min-width: 100%' : 'width: 100%') . '; margin-bottom: 10px;" tabindex="' . $context['tabindex']++ . '">%body%</textarea><br />
										<input type="hidden" name="' . $context['session_var'] . '" value="' . $context['session_id'] . '" />
										<input type="hidden" name="topic" value="' . $context['current_topic'] . '" /> 
										<input type="hidden" name="msg" value="%msg_id%" />
										<div class="righttext">
											<input type="submit" name="post" value="' . $txt['save'] . '" tabindex="' . $context['tabindex']++ . '" onclick="return oQuickModify.modifySave(\'' . $context['session_id'] . '\', \'' . $context['session_var'] . '\');" accesskey="s" class="button_submit" />&nbsp;&nbsp;' . ($context['show_spellchecking'] ? '<input type="button" value="' . $txt['spell_check'] . '" tabindex="' . $context['tabindex']++ . '" onclick="spellCheck(\'quickModForm\', \'message\');" class="button_submit" />&nbsp;&nbsp;' : '') . '<input type="submit" name="cancel" value="' . $txt['modify_cancel'] . '" tabindex="' . $context['tabindex']++ . '" onclick="return oQuickModify.modifyCancel();" class="button_submit" />
										</div>
									</div>'), ',
								sTemplateSubjectEdit: ', JavaScriptEscape('<input type="text" style="width: 90%;" name="subject" value="%subject%" size="80" maxlength="80" tabindex="' . $context['tabindex']++ . '" class="input_text" />'), ',
								sTemplateBodyNormal: ', JavaScriptEscape('%body%'), ',
								sTemplateSubjectNormal: ', JavaScriptEscape('<a href="' . $scripturl . '?topic=' . $context['current_topic'] . '.msg%msg_id%#msg%msg_id%" rel="nofollow">%subject%</a>'), ',
								sTemplateTopSubject: ', JavaScriptEscape($txt['topic'] . ': %subject% &nbsp;(' . $txt['read'] . ' ' . $context['num_views'] . ' ' . $txt['times'] . ')'), ',
								sErrorBorderStyle: ', JavaScriptEscape('1px solid red'), '
							});
						}';

		if (!empty($context['ignoredMsgs']))
		{
			echo '
						var aIgnoreToggles = new Array();';

			foreach ($context['ignoredMsgs'] as $msgid)
			{
				echo '
						aIgnoreToggles[', $msgid, '] = new smc_Toggle({
							bToggleEnabled: true,
							bCurrentlyCollapsed: true,
							aSwappableContainers: [
								\'msg_', $msgid, '_extra_info\',
								\'msg_', $msgid, '\',
								\'msg_', $msgid, '_footer\',
								\'msg_', $msgid, '_quick_mod\',
								\'modify_button_', $msgid, '\',
								\'msg_', $msgid, '_signature\'

							],
							aSwapLinks: [
								{
									sId: \'msg_', $msgid, '_ignored_link\',
									msgExpanded: \'\',
									msgCollapsed: ', JavaScriptEscape($txt['show_ignore_user_post']), '
								}
							]
						});';
			}
		}

		echo '
				// ]]></script>
				';
	}

	function theme_author($orig)
	{
		global $txt;

		$data = blogformat($orig['poster']['date_registered'], false);
		echo '
		<p>
			<img style="height: 70px;" src="' , $orig['poster']['avatar'] ,'" alt="" /><br>
			' , sprintf($txt['author_info'], $orig['poster']['name'], $data, $orig['poster']['posts']);

		if(!empty($orig['poster']['website_name']) && !empty($orig['poster']['website_url']))
			echo sprintf($txt['author_info_website'], $orig['poster']['name'], $orig['poster']['website_url'], $orig['poster']['website_name']);
		
		echo '
		</p>';
	}
}

?>