Copyright © 2014 Protendo.  All rights reserved.

Developed by: 
Protendo
http://www.protendo.org

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following discltwitterers.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following discltwitterers in the
     documentation and/or other materials provided with the distribution.
  3. Neither the names of Simple Machines Forum, Simple Machines, nor
     the names of its contributors may be used to endorse or promote 
     products derived from this Software without specific prior written
     permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLtwitter, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
WITH THE SOFTWARE.

