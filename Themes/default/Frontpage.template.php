<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

function template_main()
{
	global $modSettings, $context;

	createThemeObject('Frontpage');
	$context['subthemeobject']->template_main();
}

function template_show_blocks()
{
	global $modSettings, $context;

	createThemeObject('Frontpage');
	$context['subthemeobject']->template_show_block();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoFrontpage
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		$styles= array(
			'blog' => 'color: black; ',
			'moviereview' => 'color: white;',
			'photo' => ':color: black; ',
			'gallery' => 'color: black; ',
		
		);

		foreach($context['frontblog'] as $orig)
		{
			echo '
			<div class="bwgrid">
				<div class="bwcell5">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
						<img class="imgstyle2 boxhadow1" style="margin-top: 10px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.jpg' , '" alt="" />
					</a>
				</div>
				<div class="bwcell11">
					<div class="inner_right">
						<h3 class="blogheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="middletext greytext" style="display: block;clear: both;">
						' , blogformat(  $orig['timestamp'] ) , ' av <b>' . $orig['poster']['link'] . '</b> i ' . $orig['board']['link'] . ' </span>
						<hr><div class="post">				
						<div style="width: 20%; float: right; margin: 0.6em 0 0 1em;">
						<a href="' , $scripturl , '?action=' , $orig['board']['id_plugin'] , '" title="' , $orig['board']['id_plugin'], '">
						<img class="imgstyle2" style="margin-bottom: 0;" src="' . $boardurl . '/Plugins/Boardtypes/' , strtoupper(substr($orig['board']['id_plugin'],0,1)) , substr($orig['board']['id_plugin'],1) , '.jpg" />
						<span class="blogheader" style="text-align: center;margin: 0;padding: 0;">',strtoupper(substr($orig['board']['id_plugin'],0,1)) , substr($orig['board']['id_plugin'],1),'</span></a>
					</div>
					' , $orig['body'] , ' </div>
							<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
							' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
							<br>
						</div>
				</div>
			</div>
			<br />';
		}
	}

	function template_show_custom()
	{
		global $modSettings, $context, $scripturl;

		if(!empty($modSettings['frontpage']['showcustom']))
			echo $modSettings['frontpage']['showcustom'];
		else
			echo '
	<h3 class="mainheader" style="text-align: center;">Welcome to your Protendo installation!</h3>
	<img style="width: 100%;" class="padding_vert" src="Themes/default/images/install/6.jpg" alt="" />
	<p class="post padding_vert">Take some time to get familiarized with the options, although you will find that by using SMF you already have a good grasp of Protendo too. In Protendo you will have a full forum with boards and topics - but there is a difference in how you differentiate between boards, that is, you can collect them into special "boardtypes" that will show under a dedicated "action=" value as well being shown in a different manner than the forum posts. In addition many of the boardtypes have extra options tied to each topic, related to what kind of boardtype it is.
	</p>
	<p class="post padding_vert">If you want to change this frontpage, you can do that in <a href="' , $scripturl,  '?action=admin;area=frontpage;' , $context['session_var'], '=' , $context['session_id'] , '"><b>Frontpage</b></a>. You will have  options to show either a single or multiple boardtypes - or simply a HTML text like the one you are reading now. Beware that the boardtypes options won\'t show until you actually activated some - that can be done in <a href="' , $scripturl,  '?action=admin;area=boardtypes;' , $context['session_var'], '=' , $context['session_id'] , '"><b>Boardtypes.</b></a></p>
	<hr>
	<p class="post" style="text-align: center;"><em>Have fun and <a href="http://www.protendo.org"><b>and visit us</b></a> for info and news about Protendo.</em><br><br><a href="http://www.protendo.org" class="Tip" title="Visit the Protendo site"><img style="margin: 0 auto; width: 100px;" src="Themes/default/images/install/icon.png" alt="" /></a></p>';

	}
	function template_show_block()
	{
		global $modSettings, $context, $scripturl;

		echo '
		<div class="bwgrid">';
		
		foreach($modSettings['frontpage']['blocks'] as $bl => $block)
		{
			if(empty($block['type']))
			{
				echo '
			<div class="bwcell' , $block['width'] , ' blockcontainer">
				<div class="blockcontent">
					' , !empty($block['title']) ? '<h2 class="breadcrumb"><strong>' . $block['title'] . '</strong></h2><hr>' : '';
				$posts = fronttopics($block['plugin'],$block['number']);
				$go = 'layout_base_' . $block['layout'];
				$context['blockcode'] = $block['code'];
				$context['subthemeobject']->$go($posts, $block['width']);			
			echo '
				</div>
			</div>';
			}
			elseif(!empty($block['type']) && $block['type'] == 1)
			{
				echo '
			<div class="bwcell' , $block['width'] , ' blockcontainer">
				<div class="blockcontent">
					' , !empty($block['title']) ? '<h2 class="breadcrumb"><strong>' . $block['title'] . '</strong></h2><hr>' : '';
				$posts = frontforumtopics($block['number']);
				$context['subthemeobject']->layout_base_3($posts, $block['width']);			
			echo '
				</div>
			</div>';
			}
			elseif(!empty($block['type']) && $block['type'] == 2)
			{
				echo '
			<div class="bwcell' , $block['width'] , ' blockcontainer">
				<div class="blockcontent">
					' , !empty($block['title']) ? '<h2 class="breadcrumb"><strong>' . $block['title'] . '</strong></h2><hr>' : '', '
					<div>' , parse_bbc($block['code']) , '</div>';
			echo '
				</div>
			</div>';
			}
			elseif(!empty($block['type']) && $block['type'] == 3)
			{
				echo '
			<div class="bwcell' , $block['width'] , ' blockcontainer">
				<div class="blockcontent">
					' , !empty($block['title']) ? '<h2 class="breadcrumb"><strong>' . $block['title'] . '</strong></h2><hr>' : '', '
					<div>' , $block['code'] , '</div>';
			echo '
				</div>
			</div>';
			}
		}
		echo '
		</div>';
	}


	function layout_base_0($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		$size = $planned_width>8 ?  'big' : 'thumb';
		foreach($posts as $orig)
		{
			echo '
			<div class="bwgrid">
				<div class="bwcell5">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
						<img class="imgstyle2 boxshadow1" style="margin: 14px 20px 60px 0px;" src="' , !empty($orig['att'][$size]) ? $orig['att'][$size] : $settings['images_url'].'/noimage.jpg' , '" alt="" />
					</a>
				</div>
				<div class="bwcell11">
					<div class="inner_right">
						<h3 class="blogheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>	</h3>
						<span class="middletext greytext" style="display: block;clear: both;">
						' , blogformat($orig['timestamp']) , ' av <b>' . $orig['poster']['link'] . '</b> i ' . $orig['board']['link'] . ' </span>
						<hr>
						<div class="post">				
							' , $orig['body'] , ' 
						</div>
						<p class="greytext breadcrumb_style">
							<a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
						' , !empty($orig['replies']) ? '&nbsp;&nbsp;<a href="' . $orig['href'] . '">' . $orig['replies'] . ' ' . $txt['comments'] . '</a>' : '' , '
						</p>
						<br>
					</div>
				</div>
			</div>
			<br />';
		}
	}
	function layout_base_3($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		foreach($posts as $orig)
		{
			echo '
		<div>
			<h3 class="blogheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>	</h3>
			<span class="middletext greytext" style="display: block;clear: both;">
			' , blogformat($orig['timestamp']) , ' av <b>' . $orig['poster']['link'] . '</b> i ' . $orig['board']['link'] . ' </span>
		</div>';
		}
	}
	function layout_base_1($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		foreach($posts as $orig)
		{
			echo '
			<h3 class="subheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>	</h3>';
		}
	}
	function layout_base_2($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		$size = $planned_width>8 ?  'big' : 'thumb';
		foreach($posts as $orig)
		{
			echo '
			<div class="bwgrid">
				<div class="bwcell16">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
						<img class="imgstyle2" style="margin-top: 10px;" src="' , !empty($orig['att'][$size]) ? $orig['att'][$size] : $settings['images_url'].'/noimage.jpg' , '" alt="" />
					</a>
				</div>
				<div class="bwcell16">
					<div>
						<h3 class="blogheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>	</h3>
						<span class="middletext greytext" style="display: block;clear: both;">
						' , blogformat($orig['timestamp']) , ' av <b>' . $orig['poster']['link'] . '</b> i ' . $orig['board']['link'] . ' </span>
						<hr>
						<p class="greytext breadcrumb_style">
							<a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
						' , !empty($orig['replies']) ? '&nbsp;&nbsp;<a href="' . $orig['href'] . '">' . $orig['replies'] . ' ' . $txt['comments'] . '</a>' : '' , '
						</p>
					</div>
				</div>
			</div>';
		}
	}
	function layout_base_4($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		$size = 'micro';
		foreach($posts as $orig)
		{
			echo '
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
					<img style="margin: 4px 4px 0 0; height: 70px;" src="' , !empty($orig['att'][$size]) ? $orig['att'][$size] : $settings['images_url'].'/noimage.jpg' , '" alt="" />
				</a>';
		}
	}
	function layout_base_5($posts, $planned_width)
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl;

		$height = (!empty($context['blockcode']) && is_numeric($context['blockcode'])) ? $context['blockcode'] : 400;
		$size = 'original';
		echo '
		<div id="myslides" class="slideshow" style="height: ' , $context['blockcode'] , 'px;">';
		
		$first = true;
		foreach($posts as $orig)
		{
			echo '
			<span class="slide" style="height: ' , $context['blockcode'] , 'px; background-image: url(' , !empty($orig['att'][$size]) ? $orig['att'][$size] : $settings['images_url'].'/noimage.jpg' , ');">
				<a class="blogheader" href="' . $scripturl . '?topic='.$orig['topic'].'.0" >' , $orig['subject'] , '</a>
			</span>';
			$first = false;
		}
		echo '
		</div>
				<script type="text/javascript">
					//<![CDATA[
window.addEvent(\'domready\',function() {
	/* settings */
	var showDuration = 5000;
	var container = $(\'myslides\');
	var images = container.getElements(\'span.slide\');
	var currentIndex = 0;
	var interval;
	var toc = [];
	var tocWidth = 20;
	var tocActive = \'toc-active\';
	
	/* new: starts the show */
	var start = function() { interval = show.periodical(showDuration); };
	var stop = function() { $clear(interval); };
	/* worker */
	var show = function(to) {
		// setting up element with options
		images[currentIndex].set(\'tween\', { duration: 1200, transition: \'quad:out\' });
		images[currentIndex].fade(\'out\');
		toc[currentIndex].removeClass(tocActive);
		images[currentIndex = ($defined(to) ? to : (currentIndex < images.length - 1 ? currentIndex+1 : 0))].fade(\'in\');
		toc[currentIndex].addClass(tocActive);
	};
	
	/* new: control: table of contents */
	images.each(function(img,i){
		toc.push(new Element(\'a\',{
			text: \'\',
			href: \'#\',
			\'class\': \'toc\' + (i == 0 ? \' \' + tocActive : \'\'),
			events: {
				click: function(e) {
					if(e) e.stop();
					stop();
					show(i);
				}
			},
			styles: {
				left: ((i + 1) * (tocWidth + 5))
			}
		}).inject(container));
		if(i > 0) { img.set(\'opacity\',0); }
	});
	
	/* new: control: next and previous */
	var next = new Element(\'a\',{
		href: \'#\',
		id: \'next\',
		text: \'>\',
		events: {
			click: function(e) {
				if(e) e.stop();
				stop(); show();
			}
		}
	}).inject(container);
	var previous = new Element(\'a\',{
		href: \'#\',
		id: \'previous\',
		text: \'<\',
		events: {
			click: function(e) {
				if(e) e.stop();
				stop(); show(currentIndex != 0 ? currentIndex -1 : images.length-1);
			}
		}
	}).inject(container);
	
	/* new: control: start/stop on mouseover/mouseout */
	container.addEvents({
		mouseenter: function() { stop(); },
		mouseleave: function() { start(); }
	});

	/* start once the page is finished loading */
	window.addEvent(\'load\',function(){
		start();
	});
});					//]]>
				</script>
		
		';
	}
}

?>