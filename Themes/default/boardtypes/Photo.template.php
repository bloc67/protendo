<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Photo');
		$context['subthemeobject']->template_main();
	}
}
		
function template_photo()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Photo');
	$context['subthemeobject']->template_photo();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoPhoto
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		if(!empty($context['blogboards']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['gallery'] , '</h2><hr>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		echo '
		<div>' ,  $this->template_altgallery() , '</div>			
		<div class="pagelinks">' , $context['page_index'] , '</div>';
	}

	function template_altgallery()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '
		<div class="bwgrid">
			<div class="bwcell6">';
		
		foreach($context['blogboards'] as $blog)
			echo '
				<a class="new blue" href="' . $scripturl . '?board='.$blog['id_board'].'">' . $blog['name'] . '</a>';
		
		echo '
			</div>
			<div class="bwcell10"><div style="padding-left: 1em;">';

		foreach($context['frontblog'] as $orig)
		{
			echo '
				<a href="#imgcell' . $orig['topic'] . '" style="text-decoration: none;">
					<img src="' . (!empty($context['blogimages'][$orig['id_msg']]['micro']) ? $context['blogimages'][$orig['id_msg']]['micro'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 70px; margin: 2px;" />
				</a>';
		}
		echo '	
			</div></div>
		</div>
		<div class="pagelinks">' , $context['page_index'] , '</div>';
		
		foreach($context['frontblog'] as $orig)
		{
			echo '
		<div class="bwgrid" id="imgcell' . $orig['topic'] . '">
			<div class="bwcell16">
				<h3 class="blogheader"><a href="' . $scripturl . '?topic=' . $orig['topic'] .'">' , $orig['subject'] , '</a></h3>
					' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '') . '
				<hr>
			</div>
			<div class="bwcell6">
					' . ($orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '') . '
					<span class="breadcrumb_style">' , $orig['views'] , ' ' , $txt['views'] , '</span>
					<span class="breadcrumb_style">' , $orig['replies'] , ' ' , $txt['replies'] , '</span>
					<div class="padding_vert post">' , $orig['body'] , '</div>
			</div>
			<div class="bwcell10"><div style="padding: 10px 10px 10px 1em;">
				<a href="' . $scripturl . '?topic=' . $orig['topic'] .'">
					<img class="imgstyle2" src="' . (!empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 100%;" />
				</a>
			</div></div>
		</div>';
				
		}
	}
}
?>