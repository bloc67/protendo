<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Moviereview');
		$context['subthemeobject']->template_main();
	}
}
		
function template_interest()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Moviereview');
	$context['subthemeobject']->template_moviereview();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMoviereview
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['blogboards']) && !empty($context['normal_buttons']))
			echo '
	<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
	<h2 class="mainheader">' , $txt['moviereview'] , '</h2>';

		if(!isset($context['frontblog']))
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
			$this->template_moviereview();
	}

	function template_moviereview()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$first = true; 
		foreach($context['frontblog'] as $orig)
		{
			if($first)
				echo '
		<div class="bwgrid">
			<div class="bwcell8">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0"><span class="imgingress">' . $orig['subject']  . '</span>
					<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
				</a>
				' , $this->template_showmostrated() , '
			</div>
			<div class="bwcell8"><div class="inner_right">';
			else
				continue;
			
			$first = false;
		}
		foreach($context['frontblog'] as $orig)
		{
			echo '
				<div class="bwgrid">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
						<img class="imgstyle2" style="float: right; width: 100px; margin: 1em 0 0 1em;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
					</a>	
					<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
						' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
					' , !empty($orig['hiddentext']['myscore']) ? '<span class="notifier">' . $orig['hiddentext']['myscore'] . '</span>' : '' ,'
					</h3>
					<span class="breadcrumb" style="display: block;"></span>
					' , substr($orig['body'],0,250) , '... 
					<div class="padding_vert greytext breadcrumb_style">
						' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' ' , blogformat($orig['timestamp']) , ' |
						<a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
						' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					</div>
					<br>
				</div>
				<br />';
		}
		
		if(!empty($context['page_index']))
			echo '
				<div class="pagelinks">' , $context['page_index'] , '</div>';

		echo '
			</div></div>
		</div>';
	}

	function template_showmostrated()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '<br>
		<h3 class="largerfont">' , $txt['best_rated'] , '</h3><hr>';
		foreach($context['high_rated'] as $w => $data)
		{
			echo '
			<p>
				<span class="floatright">' , $data['value_int'] , '<small>/10</small></span>
				<a href="' , $scripturl , '?topic=' , $data['id_topic'] , '"><b>' , $data['subject'] , '</b></a> ' , $txt['by'] , ' <a href="' , $scripturl , '?action=profile;u=' , $data['id_member'] , '">' , $data['real_name'] , '</a>
			</p>';
		}
		
	}

	function template_showtotalwannasee()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '<br>
		<h3 class="largerfont">' , $txt['wannaseetitle'] , '</h3><hr>';
		foreach($context['most_wannasee'] as $w => $data)
		{
			echo '
			<p>
				<span class="floatright">' , $data['value_int'] , ' <small>' , $txt['members'] , '</small></span>
				<a href="' , $scripturl , '?topic=' , $data['id_topic'] , '"><b>' , $data['subject'] , '</b></a> ' , $txt['by'] , ' <a href="' , $scripturl , '?action=profile;u=' , $data['id_member'] , '">' , $data['real_name'] , '</a>
			</p>';
		}
	}
}
	

?>