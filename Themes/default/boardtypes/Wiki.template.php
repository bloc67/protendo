<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Wiki','pluginobject');
		$context['pluginobject']->template_wiki();
	}
}

function template_wiki()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Wiki','pluginobject');
	$context['pluginobject']->template_wiki();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoWiki
{
	function template_wiki()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		if(!isset($context['frontblog']))
		{
				echo '
			<hr><strong>', $txt['msg_alert_none'], '</strong>';
				return;
		}

		$alt = true;
		$first = true;
		foreach($context['frontblog'] as $orig)
		{
			if($first)
			{
				echo '
		<div class="bwgrid">
			<div class="windowbg' , $alt ? '' : '2' , '" style="overflow: hidden;">
				<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0"><img class="imgstyle2" style="margin-right: 1em; width: 100px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['micro']) ? $context['blogimages'][$orig['id_msg']]['micro'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
				<span class="messicons ' , isset($orig['hiddentext']['bg_status']) ? $modSettings['bg_classes'][$orig['hiddentext']['bg_status']] : 'mess_xx' , '" style="margin-bottom: -6px;"></span>&nbsp;&nbsp;
				<span class="greytext breadcrumb_style"><span style="min-width: 8em;display: inline-block;">' , blogformat($orig['timestamp']) , '</span> <a href="' . $orig['href'].'"><b>' , $orig['subject'] , '</b></a></span>
				' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
				
				<span class="greytext breadcrumb_style"><hr>' .  $orig['board']['link'] . ' | ' , $txt['posted_by'] , ' ' . $orig['poster']['link'] . ' | <a href="' , $orig['href'] , '#comments">' . $orig['replies'] . ' ' . $txt['replies'] . '</a> | ' . $orig['views'] . ' ' . $txt['views'] . '</span>
				' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
				<div><em class="middletext">' , strip_tags($orig['body']) , '</em></div>
			</div>
		</div>';
				$alt = !$alt;
				$first = false;
			}
			else
			{
				echo '
		<div class="bwgrid">
			<div class="windowbg' , $alt ? '' : '2' , '">
				<span class="messicons ' , isset($orig['hiddentext']['bg_status']) ? $modSettings['bg_classes'][$orig['hiddentext']['bg_status']] : 'mess_xx' , '" style="margin-bottom: -6px;"></span>&nbsp;&nbsp;
				<span class="greytext breadcrumb_style"><span style="min-width: 8em;display: inline-block;">' , blogformat($orig['timestamp']) , '</span> <a href="' . $orig['href'].'"><b>' , $orig['subject'] , '</b></a></span>
				' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
				
				<span class="greytext breadcrumb_style floatright">' .  $orig['board']['link'] . ' | ' , $txt['posted_by'] , ' ' . $orig['poster']['link'] . ' | <a href="' , $orig['href'] , '#comments">' . $orig['replies'] . ' ' . $txt['replies'] . '</a> | ' . $orig['views'] . ' ' . $txt['views'] . '</span>
				' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
			</div>
		</div>';
				$alt = !$alt;
			}
		}
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}
}
?>