<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Genealogy');
		$context['subthemeobject']->template_main();
	}
}
		
function template_genealogy()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Genealogy');
	$context['subthemeobject']->template_genealogy();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoGenealogy
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['genealogy'] , '</h2>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		$this->template_genealogy();
	}

	function template_genealogy()
	{
		global $context, $settings, $options, $txt, $scripturl;

		foreach($context['frontblog'] as $orig)
		{
			echo '
			<div class="bwgrid">
				<div class="bwcell5">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
						<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
					</a>
				</div>
				<div class="bwcell11">
					<div class="inner_right">
						<div id="bcontainer" class="floatleft">
							<div class="bdate">
								<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
							</div>
						</div>
						<h3 class="blogheader" style="padding-top: 0.3em;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: both;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' , blogformat($orig['timestamp']) , ' </span>
						<div class="post">' , $orig['body'] , ' </div>
							<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
							' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
							<br>
					</div>
				</div>
			</div>
			<br />';
		}
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}
}	

?>