<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function template_main()
{
	global $modSettings, $context;

	createThemeObject('Blog');
	$context['subthemeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoBlog
{
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;
		
		if(!empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , $context['themeobject']->theme_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['blog'] , ' ' , isset($_GET['author']) ? ' - '. $context['current_blog_author']  : '', '</h2>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		$this->theme_blog();
	}

	function theme_blog()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(isset($context['bloghistory']))
		{
			echo '
		<div class="bwgrid">
			<div class="bwcell3" style="border-right: solid 1px #ccc;">
				<ul class="reset">';
			
			foreach($context['bloghistory'] as $year => $months)
			{
				echo '
					<li><a href="' , $scripturl , '?action=blog' , isset($_GET['author']) ? ';author='. $_GET['author'].';y='.$year : '' , '">' , $year , '</a>
						<ul>';
				foreach($months as $month => $mm)
					echo '
							<li><a href="' , $scripturl , '?action=blog' , isset($_GET['author']) ? ';author='. $_GET['author'].';y='.$year.';m='.$month : '' , '">' , $txt['months_show'][$month] , '</a></li>';
				echo '
						</ul>
					</li>';
			}
			echo '				
			</div>
			<div class="bwcell13"><div class="inner_right">';

			echo '
			<div id="blog">';
			foreach($context['frontblog'] as $orig)
			{
				echo '
				<div class="bwgrid">
					<div class="bwcell16">
						<div>
							<div id="bcontainer" class="floatleft">
								<div class="bdate">
									<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
								</div>
							</div>
							<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
								' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
							</h3>
							<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: left;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' , blogformat($orig['timestamp']) , ' </span>
							<div class="post">
								<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
									<img class="floatright boxshadow1" style="width: 30%; margin: 0 20px 60px 60px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
								</a>
								' , $orig['body'] , ' 
								</div>
								<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
								' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
								<br>
						</div>
					</div>
				</div>
				<br />';
			}
			if(!empty($context['page_index']))
				echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
			echo '
			</div>';
			echo '
	</div></div>
</div>';
		}
		else
		{
			echo '
			<div id="blog">';
			foreach($context['frontblog'] as $orig)
			{
				echo '
				<div class="bwgrid">
					<div class="bwcell5">
						<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
							<img class="imgstyle2 boxshadow1" style="margin: 4px 20px 60px 20px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
						</a>
						<div style="text-align; right;">
							<h3 class="blogheader floatright"><a href="' , $scripturl, '?action=blog;author=', $orig['poster']['id'] , '">' , $orig['poster']['name'] , '</h3>
						</div>
					</div>
					<div class="bwcell11">
						<div class="inner_right">
							<div id="bcontainer" class="floatleft">
								<div class="bdate">
									<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
								</div>
							</div>
							<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
								' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
							</h3>
							<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: both;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' , blogformat($orig['timestamp']) , ' </span>
							<div class="post">' , $orig['body'] , ' </div>
								<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
								' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
								<br>
						</div>
					</div>
				</div>
				<br />';
			}
			if(!empty($context['page_index']))
				echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
			echo '
			</div>';
		}
	}
}

?>