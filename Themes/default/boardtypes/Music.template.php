<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Music');
		$context['subthemeobject']->template_main();
	}
}
		
function template_music()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Music');
	$context['subthemeobject']->template_music();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMusic
{	
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['music'] , '</h2>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		echo '
		<div class="bwgrid">
			<div class="bwcell16">' , $this->template_video() , '</div>
			<div class="bwcell16">
				' , $this->template_side_video() , '
			</div>
		</div>';
	}

	function template_side_video()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$alt = true;
		
		echo '
		<div class="windowbg middletext" style="overflow: hidden;">';

		echo '
		<h3 class="blogheader">' , $txt['categories'] , '</h3><hr>	';
		foreach($context['blogboards'] as $blog)
		{
			echo '
				<a class="new_style" href="' . $scripturl . '?board='.$blog['id_board'].'">' . $blog['name'] . ' [' . $blog['num_topics'] . ']</a>';
		}

		if(!empty($context['blogmostread']))
		{
			echo '
			<br><h3 class="blogheader">' , $txt['most_read'] , '</h3><hr>
			';
			foreach($context['blogmostread'] as $u)
				echo '<a href="' . $scripturl . '?topic=' . $u['id_topic'] . '">' . $u['subject'] . ' (' . $u['num_views'] . ')</a> ' . $txt['by'] . ' <a href="' . $scripturl . '?action=profile;u=' . $u['id_member'] . '">' . $u['real_name']. '</a><br>';
		}
		echo '
		</div>';
	}

	function template_video()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';

		echo '
		<div class="bwgrid">';
		$count = 0; $class=array('0' => '_first', '1' => '', '2' => '_third');
		foreach($context['frontblog'] as $orig)
		{
			// check if we got a imge from youtube, if not, use any blog
			$img = $settings['images_url'].'/novideo.png';
			if(!empty($orig['hiddentext']['clip']))
				$img = $context['blogimages'][$orig['id_msg']]['thumb'];

			echo '
			<div class="triplecell' . $class[$count] . '">
				<div class="bwgrid">
					<div class="padding_vert" style="padding-bottom: 0; margin-bottom: -5px;">
						<a href="' . $orig['href'].'"><img class="imgstyle1" src="' , $img , '" alt="" /></a>
					</div>
					<div class="bwcell16">
	';
			if(!empty($orig['hiddentext']['clip']))
				echo '<iframe width="100%" height="96"	src="http://www.youtube.com/embed/' . $orig['hiddentext']['clip'] . '?rel=0&autohide=0&modestbranding=1" frameborder="0" class="bbc_youtube" allowfullscreen></iframe>';

			echo '	<h3 class="textheader" style="margin: 0;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="breadcrumb_style" style="display: block; padding: 5px 0;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' . $orig['time'] . ' </span>
						' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
						<br>
					</div>
				</div>
			</div>';
			$count++;
			if($count>2)
			{
				echo '<br class="clear">';
				$count= 0;
			}
		}
		echo '
		</div>';
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}
}
?>