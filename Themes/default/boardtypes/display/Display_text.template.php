<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function theme_main()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('DisplayText','boardtypeobject');
	$context['boardtypeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$orig = $context['frontblog'][0];
		echo '
				<div class="bwgrid"><div class="bwcell7"><div class="inner_left">
					<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
				</div></div><div class="bwcell9">	
					<h3 class="textheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , !empty($orig['new']) && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<span class="breadcrumb_style" style="padding: 1px 10px;">' , $orig['time'] , '</span> 
					<span class="breadcrumb_style">&nbsp;&nbsp;' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> 
					<div class="textcolumns padding_vert">' , $orig['body'] , '...</div>
					&nbsp;' , !empty($orig['new']) && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
				';

		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			echo '
							<div id="msg_footer" class="attachments smalltext" style="clear: right;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';text-align: left;">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img title="' , $attachment['name'] , ' :: ' , $attachment['size'], '<br>' , $attachment['width'], 'x' , $attachment['height'], '" class="TipBig"style="height: 40px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img title="' , $attachment['name'] , ' :: ' , $attachment['size'], '<br>' , $attachment['width'], 'x' , $attachment['height'], '"  class="TipBig" src="' . $attachment['href'] . '" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" title="' , $attachment['name'] , ' :: ' , $attachment['size'], '"  class="TipBig" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
								</div>
							</div>';
		}
		echo '
					<ul class="quickbuttons " style="float: right; clear: right; ">';


			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
										<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
										<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

			echo '
					</ul>
					', $context['themeobject']->theme_button_strip($context['normal_buttons'], 'float: left;'), '
					<br style="clear: both;">';

			if(!empty($orig['hiddentext']['myscore']))
				echo '
					<span class="imdb_myscore Tip" title="' , $txt['myscore'] , '"><span style="width: ', $orig['hiddentext']['myscore']*10 , '%;"></span></span><span class="Tip" title="' , $txt['myscore'] , '">' , $orig['hiddentext']['myscore'].'/10</span>';
			
			if(!empty($orig['hiddentext']['trailer']))
			{
				$data = $orig['hiddentext']['trailer'];
				echo '
					<a id="imdb_trailer" youtubelink="' . $data . '" class="social_icons youtube_icon TipVideo" style="margin-left: 1em;"></a>';

			}
			echo '
		<hr />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'eft');
		echo '</div></div>';
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}
?>