<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay_gallery extends ProtendoDisplay
{
	function theme_main()
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		$orig = $context['frontblog'][0];
		echo '
		<div class="bwgrid" style="padding-top: 10px;">
			<div class="bwcell16">
				<img class="imgstyle2" style="margin: 0;" src="' , !empty($context['blogimages'][$orig['id_msg']]['original']) ? $context['blogimages'][$orig['id_msg']]['original'] : $settings['images_url'].'/noimage.png' , '" alt="" />
			</div>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']) && empty($orig['hideattached']))
		{
			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 150px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 150px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 150px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

		}
		echo '
			<div class="bwcell16">
				<div>
					<h2 class="mainheader">
					' , $orig['subject'] , '
					</h2>
					<span class="greytext middletext">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' , $txt['on'] , '  - ' , blogformat($orig['timestamp']) , '</span>
					<div class="post">
						' , $orig['body'] , '
					</div><hr>
					<div class="floatright" style="clear: right; padding: 0;">', $context['themeobject']->theme_button_strip($context['normal_buttons']), '</div>
					<ul class="quickbuttons" >';


		// Can the user modify the contents of this post?
		if ($context['can_modify'])
			echo '
						<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($context['can_remove_post'])
			echo '
						<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';


		echo '</ul>
				</div>
			</div>
		</div>
		<hr />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'eft');
		$this->theme_blog_comments($orig['poster']['id']);
	}
}
?>