<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay_moviereview extends ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		
		$orig = $context['frontblog'][0];

		echo '
		<div class="bwgrid" style="padding-top: 10px;">
			<div class="bwcell7">
				<img style="width: 100%; margin-bottom: 0;" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
				<div class="imdb">
					<h3 class="blogheader largerfont" style="text-align: center;">IMDB</h3>
						<dl style="clear: both;">
							<dt>' , $txt['imdb_rating'] , '</dt>
								<dd><b>' , $orig['hiddentext']['imdb_rating'] , '</b></dd>
							<dt>' , $txt['imdb_year'] , '</dt>
								<dd>' , $orig['hiddentext']['imdb_date'] , '</dd>
							<dt>' , $txt['imdb_plot'] , '</dt>
								<dd>' , $orig['hiddentext']['imdb_plot'] , '</dd>
							<dt>' , $txt['imdb_genres'] , '</dt>
								<dd>' , $orig['hiddentext']['imdb_genre'] , '</dd>
						</dl>
				</div>';

			echo '
			</div>
			<div class="bwcell9">
				<div style="padding: 0 0 0 2em;">
					<h2 class="blogheader">
						' , $orig['subject'] , '
					</h2>
					<span class="greytext breadcrumb_style">'. $txt['posted_by'] . ' <b>' . $orig['poster']['link'] . '</b> -  ' , blogformat($orig['timestamp']) , '</span><hr>
					<div class="post">
						' , $orig['body'],'
					</div><hr>
					<ul class="quickbuttons " style="float: right; clear: right; ">';

			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
										<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
										<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

			echo '
					</ul>
					', $context['themeobject']->theme_button_strip($context['normal_buttons']), '
					<hr style="clear: both;">
					<div class="scorepanel">';

		if(isset($orig['hiddentext']['myscore']))
			echo '
					<div><span class="bbc_rate5 largerfont"><b>' , $orig['hiddentext']['myscore'] , '</b></span><span class="breadcrumb smalltext floatleft">' , $txt['myscore'] , '</span></div>';
			
		if(isset($orig['hiddentext']['seenbefore']))
			echo '
					<div><span class="bbc_rate2 largerfont"><b>' , $orig['hiddentext']['seenbefore'] , '</b></span><span class="breadcrumb smalltext floatleft">' , $txt['seenbefore'] , '</span></div>';

		echo '
				</div>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			echo '<br style="clear: both;"><hr>
							<h3><strong>' , $txt['morepictures'] , '</strong></h3>
							<div id="msg_footer" class="attachments smalltext" style="clear: both; margin-top: 0.5em;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';text-align: left;">';

			$last_approved_state = 1; $first = true; $second = false;
			foreach ($orig['attachment'] as $attachment)
			{
				if($first)
				{
					$first = false;
					continue;
				}
				
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 100px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
				
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
								</div>
							</div>';
		}
		echo '
		
					</div>
			</div>
		</div>
		<br />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'float: right;',false);
		$this->theme_blog_comments($orig['poster']['id']);
	}
}
?>