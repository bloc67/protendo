<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function theme_main()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('DisplayPhoto','boardtypeobject');
	$context['boardtypeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function theme_main()
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		$orig = $context['frontblog'][0];
		echo '
		<div class="bwgrid" style="padding-top: 10px;">
			<div class="bwcell8">
				<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['original']) ? $context['blogimages'][$orig['id_msg']]['original'] : $settings['images_url'].'/noimage.png' , '" alt="" />
			</div>
			<div class="bwcell8">
				<div style="padding: 0 0 0 2em;">
					<h2 class="textheader">
					' , $orig['subject'] , '
					</h2>
					<span class="greytext middletext">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' , $txt['on'] , '  - ' , blogformat($orig['timestamp']) , '</span>
					<div class="post">
						' , $orig['body'] , '
					</div><hr>';
		echo '
								<ul class="quickbuttons" >';

		// Can the user modify the contents of this post?
		if ($context['can_modify'])
			echo '
									<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($context['can_remove_post'])
			echo '
									<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';


		echo '</ul>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			echo '
							<div id="msg_footer" class="attachments smalltext" style="clear: both;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';text-align: right;">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 120px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 70px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 70px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
							</div>
							</div>';
		}

		echo '
				</div>
			</div>
		</div>
		<br />';
		echo '<div class="floatright" style="clear: both; padding: 0.5em 0;">', $context['themeobject']->theme_button_strip($context['normal_buttons']), '</div>';
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		echo '<br style="clear: both;" />';
		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}

	function template_exif($info)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		echo '
		<h3 class="blogheader">EXIF</h3>
		<div class="exif">
			<dl>';
		
		$what = array('model','exposureTime','fnumber','isoEquiv','Width','Height','flashUsed');
		
		foreach($what as $w)
			if(!empty($info[$w]))
				echo '<dt>' . $w . ':</dt><dd>' . $info[$w] . '</dd>';

		echo '
			</dl>
		</div>';
	}
}
?>