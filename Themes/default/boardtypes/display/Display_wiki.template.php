<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		
		$orig = $context['frontblog'][0];

		echo '
		<div class="bwgrid">
			<div class="windowbg" style="overflow: hidden;">
				
				<span class="messicons ' , !empty($orig['hiddentext']['bg_status']) ? $modSettings['bg_classes'][$orig['hiddentext']['bg_status']] : 'mess_xx' , '" style="margin: 6px 1em 0 0; float: left;"></span>
				<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a></h3>
				<span class="greytext breadcrumb_style">' , blogformat($orig['timestamp']) , '</span> 
							' , !empty($orig['new']) && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
				
				<span class="greytext breadcrumb_style"><hr>' .  $orig['board']['link'] . ' | ' , $txt['posted_by'] , ' ' . $orig['poster']['link'] . ' | <a href="' , $orig['href'] , '#comments">' . $orig['replies'] . ' ' . $txt['replies'] . '</a> | ' . $orig['views'] . ' ' . $txt['views'] . '</span>';

		echo '<br><br>
		<span class="middletext">
			<span style="width: 11em; display: inline-block;">' , $txt['version'] , ':</span> <b>' ,  isset($orig['hiddentext']['bg_version']) && isset($modSettings['bg_versions'][$orig['hiddentext']['bg_version']]) ? $modSettings['bg_versions'][$orig['hiddentext']['bg_version']] : '', '</b><br>
			<span style="width: 11em; display: inline-block;">' , $txt['category'] , ':</span> <b>' ,  isset($orig['hiddentext']['bg_category']) && isset($modSettings['bg_categories'][$orig['hiddentext']['bg_category']]) ? $modSettings['bg_categories'][$orig['hiddentext']['bg_category']] : '', '</b><br>
			<span style="width: 11em; display: inline-block;">' , $txt['status'] , ':</span>  <b>' , isset($orig['hiddentext']['bg_status']) && isset($modSettings['bg_texts'][$orig['hiddentext']['bg_status']]) ? $modSettings['bg_texts'][$orig['hiddentext']['bg_status']] : '' , '</b><br>
			<span style="width: 11em; display: inline-block;">' , $txt['solvedversion'] , ':</span> <b> ' ,  isset($orig['hiddentext']['bg_solvedversion'])  ? $orig['hiddentext']['bg_solvedversion'] : '', '</b>
		</span>';

		// if we got the boardcontrols, that means we can change it on the spot.
		if(isset($context['boardtypes_controls'] ))
		{
			echo '
			<hr style="clear: both;">
			<div style="float: left; width: 58%; margin-right: 1%;">' , $orig['body'] , '</div>		
			<div style="width: 40%; float: right; padding-top: 0;">
				<h4 class="headertexts">' , $txt['update'] , '</h4>
				<form action="', $scripturl, '?action=bugtracker;updatedisplay" method="post" accept-charset="', $context['character_set'], '" name="bchange"  class="windowbg2" style="margin: 0; padding: 1em;" enctype="multipart/form-data">';
			template_postboardcontrols();

			if(!empty($context['boardtype_controls_topic']))
				echo '
				<input type="hidden" name="boardtype_controls_topic" value="', $context['boardtype_controls_topic'], '" />';

			echo '
					<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
					<hr><input type="submit" name="boardtypechange" value="', $txt['save'], '" class="button_submit" />
				</form>
			</div>';
		}
		else
			echo '
				<div>' , $orig['body'] , '</div>	';	
		
		echo '<br style="clear: both;"><br>
			</div>
		</div>';

		echo '
					<ul class="quickbuttons " style="clear: both; margin: 0; padding: 0; ">';

			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
										<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
										<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

			echo '
					</ul>
					', $context['themeobject']->theme_button_strip($context['normal_buttons']);
		
		// Assuming there are attachments...
		if (!empty($orig['attachment']) && empty($orig['hideattached']))
		{
			echo '
							<div id="msg_footer" class="attachments smalltext" style="clear: both;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], ';image" id="link_', $attachment['id'], '" class="remooz"><img style="height: 40px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], ';image" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . ';image" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
								</div>
							</div>';
		}
		echo '
		<hr />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'eft');
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}
?>