<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		
		$orig = $context['frontblog'][0];

		echo '
		<div class="bwgrid" style="padding-top: 10px;">
			<div class="bwcell7">
				<img style="width: 100%; margin-bottom: 0;" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
				<div class="info">
					<h3 class="blogheader largerfont" style="text-align: center;">' , $orig['hiddentext']['tvseries_title'] , '</h3>
						<dl class="settings narrow">
							<dt>Plot:</dt>
								<dd>' , $orig['hiddentext']['tvseries_overview'] , '</dd>
						</dl>
				</div>
			</div>
			<div class="bwcell9">
				<div style="padding: 0 0 0 2em;">
					<h2 class="blogheader">
						' , $orig['subject'] , '
					</h2>
					<span class="greytext breadcrumb_style">'. $txt['posted_by'] . ' <b>' . $orig['poster']['link'] . '</b> -  ' , blogformat($orig['timestamp']) , '</span>
					<div class="post">';

		// get the ratings
		if(!empty($orig['hiddentext']['episodes']))
		{
			$total = $number = 0;
			$eps = unserialize($orig['hiddentext']['episodes']);
			foreach($eps as $e => $dat)
			{
				$number++;
				$total = $total + $dat['rate'];
			}
			$orig['hiddentext']['tvscore'] = ceil($total/$number);
		}

		if(isset($orig['hiddentext']['tvscore']))
			echo '
					<span class="floatright bbc_rate largerfont Tip" title="Score" style="margin: 0 0 0 .4em;"><b>' , $orig['hiddentext']['tvscore'] , '</b></span>';
			
		echo '
						' , $orig['body'], '
					</div><hr>
					<ul class="quickbuttons " style="float: right; clear: right; ">';

			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
										<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
										<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

			echo '
					</ul>
					', $context['themeobject']->theme_button_strip($context['normal_buttons']), '
					';



		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			echo '<br style="clear: both;">
							<div id="msg_footer" class="attachments smalltext" style="clear: both; margin-top: 0.5em;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';text-align: left;">';

			$last_approved_state = 1; $first = true; $second = false;
			foreach ($orig['attachment'] as $attachment)
			{
				if($first)
				{
					$first = false;
					continue;
				}
				
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 100px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
				
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
								</div>
							</div>';
		}
		echo '
		<hr class="clear">';
		echo '
		
					</div>
			</div>
		</div>
		<br />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'float: right;',false);

	}
	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}
?>