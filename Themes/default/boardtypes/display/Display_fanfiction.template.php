<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings, $boardurl, $topic;

		$orig = $context['frontblog'][0];
		
		if(!empty($context['blogtype_buttons']))
			echo '
		<div class="floatright"> ', $context['themeobject']->theme_button_strip($context['blogtype_buttons']), '</div>';

		echo '
		<div class="bwgrid">
			';
		if(!empty($context['blogimages'][$orig['id_msg']]['original']))
			echo '
			<img alt="" class="imgstyle1" itemprop="image" src="' , $context['blogimages'][$orig['id_msg']]['original'] , '"><span class="shadow2"></span>';

		echo '
			<h2 class="blogheader" itemprop="name">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
			</h2>
			<div class="breadcrumb_style" style="margin: 1em 0 0 0;">' . $txt['posted_by'] . ' ' .  $orig['poster']['link'] . ' | ' . $orig['time'] . ' ' . $txt['in'] . ' <b>' .  $orig['board']['link'] . '</b></div>
			<hr>';

		if(!empty($orig['hiddentext']['lead']))
			echo '
			<p class="ingress" itemprop="description">', $orig['hiddentext']['lead'] , '</p><br><hr>';
		
		if(!empty($orig['body']))
			echo '
			<div class="post">' , $orig['body'], '</div>
			<hr>';
		
		if(!empty($orig['hiddentext']['summary']))
			echo '
			<div><em>', $orig['hiddentext']['summary'] , '</em></div>';

		if(!empty($orig['hiddentext']['sources']))
			echo '
			<div class="smalltext"><em>', $orig['hiddentext']['sources'] , '</em></div>';

		echo '
			<hr class="clear">
			<span class="floatright">';
		
		// facebook share
		if(!empty($modSettings['article_facebook']))
			echo '
				<a target="_blank" href="http://www.facebook.com/sharer.php?p[url]=' , urlencode($scripturl.'?topic='.$orig['topic']), '&p[title]=' , urlencode($orig['subject']) , '&p[images][0]=' , urlencode($context['blogimages'][$orig['id_msg']]['thumb']) , '"><span class="social_icons facebook_icon"></span></a>';
		// twitter share
		if(!empty($modSettings['article_twitter']))
			echo '
				<a target="_blank" href="https://twitter.com/intent/tweet?original_referer=' , urlencode($scripturl.'?topic='.$orig['topic']), '&source=tweetbutton&text=' , urlencode($orig['subject']) , '&url=' , urlencode($scripturl.'?topic='.$orig['topic']), '"><span class="social_icons twitter_icon"></span></a>';
		// google share
		if(!empty($modSettings['article_google']))
			echo '
				<a target="_blank" href="https://plus.google.com/share?url=' , urlencode($scripturl.'?topic='.$orig['topic']) , '"><span class="social_icons google_icon"></span></a>';
		// pinterest share
		if(!empty($modSettings['article_pinterest']))
			echo '
				<a target="_blank" href="http://www.pinterest.com/pin/create/button/?url=' , urlencode($scripturl.'?topic='.$orig['topic']) , '&media=' , urlencode($context['blogimages'][$orig['id_msg']]['thumb']) , '&description=' , urlencode($orig['subject']) , '" data-pin-do="buttonPin" data-pin-config="above"><span class="social_icons pinterest_icon"></span></a>';
		

		echo '
			</span>
			<ul class="quickbuttons" style="padding-left: 0;">';

		// Can the user modify the contents of this post?
		if ($context['can_modify'])
			echo '
				<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($context['can_remove_post'])
			echo '
				<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';


		echo '
			</ul>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			echo '
			<h3 class="blogheader">' , $txt['attachments'] , '</h3>
			<div id="msg_', $orig['id_msg'], '_footer" class="smalltext padding_vert" style="clear: both;">
				<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', '">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
						<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 40px; " src="', $attachment['micro']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
						<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
						<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
						[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}
			echo '
				</div>
			</div>';
		}

		echo '
			<hr>
			<div class="align_right">' , $context['themeobject']->theme_button_strip($context['mod_buttons'],'eft') , '</div>
		</div>';
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}
?>