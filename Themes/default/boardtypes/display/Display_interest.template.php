<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function theme_main()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('DisplayInterest','boardtypeobject');
	$context['boardtypeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$orig= $context['frontblog'][0];
		echo '
		<div>
				<div class="bwgrid">
					<div class="bwcell6">
						<a href="' , $context['blogimages'][$orig['id_msg']]['original'] , '" class="remooz"><img class="imgstyle1" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png' , '" alt="" style="width: 100%; margin-top: 5px;" /></a>';
		// Assuming there are attachments...
		if (!empty($orig['attachment']) && empty($orig['hideattached']))
		{
			echo '
							<div id="msg_footer" class="attachments smalltext" style="clear: both;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';text-align: right;">';

			$first = true;
			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				if($first)
				{
					$first = false;
					continue;
				}
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="width: 100%; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 120px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 70px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';
			echo '</div></div>';
		}
		echo '
					</div>
					<div class="bwcell10"><div style="padding-left: 1em;"><div class="gcolumn">
						<a href="' , $orig['poster']['href'] , '"><img class="roundavatar" style="float: left; margin: 1em;" src="' , $orig['poster']['avatar'], '" alt="" /></a>
						<h3 class="blogheader"><a href="' , $scripturl , '?topic=' , $orig['topic'] , '">' , $orig['subject'] , '</a>
							' , !empty($orig['new']) && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="breadcrumb_style">
							' , $txt['posted_by']  , ' ' , $orig['poster']['link'] , ' | ' , $orig['time'] , ' 
						</span>
						<p style="padding: 0.5em 0;">' , $orig['body'] , '</p>
						<hr>
						<p class="floatright greytext breadcrumb_style">
							<a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' , $orig['board']['link'] , '
							' , !empty($orig['new']) && !empty($orig['replies']) ? '<br><span class="new" style="display: inline-block; margin: 5px 0;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '

						</p>
						<ul class="quickbuttons floatleft" style="padding-left: 0;">';


			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
							<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
							<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';


			echo '</ul>
					<div  style="clear: both; padding: 0;">', $context['themeobject']->theme_button_strip($context['normal_buttons']), '</div>
					<br><div id="topline">&nbsp;</div>';
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		echo $this->template_showreplies() , '</div>
					</div></div>
				</div>
			</div>';

		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
		
	}

	function template_showreplies()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

		// Get all the messages...
		while ($post = $context['get_message']())
		{
			if($post['id'] == $context['frontblog'][0]['id_msg'])
				continue;

			echo '
			<div class="bwgrid" style="padding: 0 2em 1em 2em;">
				<div class="bwcell1">
					<div style="padding: 0.1em 1em 0.2em 0;">
						<a href="' , $post['member']['href'] , '"><img class="imgstyle1" src="' , $post['member']['avatar']['href'], '" alt="" /></a>
					</div>
				</div>
				<div class="bwcell15">
					<a href="' , $post['member']['href'] , '" class="middletext"><b>' , $post['member']['name'], '</b></a>
					<div class="smalltext greytext" style="padding-bottom: 1em;">' , blogformat($post['timestamp']) , '</div>
					<div id="qpost' . $post['id'] . '" class="post">' , $post['body'] , '</div><hr>
										<ul class="quickbuttons" style="padding-left: 0;">';

				// Maybe we can approve it, maybe we should?
				if ($post['can_approve'])
					echo '
											<li class="approve_button"><a href="', $scripturl, '?action=moderate;area=postmod;sa=approve;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $post['id'], ';', $context['session_var'], '=', $context['session_id'], '"><span>', $txt['approve'], '</span></a></li>';

				// So... quick reply is off, but they *can* reply?
				if ($context['can_quote'])
					echo '
											<li class="quote_button"><a href="', $scripturl, '?action=post;quote=', $post['id'], ';topic=', $context['current_topic'], '.', $context['start'], ';last_msg=', $context['topic_last_message'], '"><span>', $txt['quote'], '</span></a></li>';

				// Can the user modify the contents of this post?
				if ($post['can_modify'])
					echo '
											<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $post['id'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

				// How about... even... remove it entirely?!
				if (!empty($post['can_remove_post']))
					echo '
											<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $post['id'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

				// What about splitting it off the rest of the topic?
				if ($context['can_split'] && !empty($context['real_num_replies']))
					echo '
								<li class="split_button"><a href="', $scripturl, '?action=splittopics;topic=', $context['current_topic'], '.0;at=', $post['id'], '"><span>', $txt['split'], '</span></a></li>';

				echo '
							</ul>
					</div>
			</div>';
		}
		if ($context['can_reply'])
		{
			echo '<br>
			<a id="quickreply"></a>
			<div id="quickreplybox">
				<h3 class="mainheader">', $txt['quick_reply'], '</h3>
				<div id="quickReplyOptions">
					', $context['is_locked'] ? '<p class="infobox smalltext">' . $txt['quick_reply_warning'] . '</p>' : '',
					$context['oldTopicError'] ? '<p class="infobox smalltext">' . sprintf($txt['error_old_topic'], $modSettings['oldTopicDays']) . '</p>' : '', '
					', $context['can_reply_approved'] ? '' : '<em>' . $txt['wait_for_approval'] . '</em>', '
					', !$context['can_reply_approved'] && $context['require_verification'] ? '<br />' : '', '
					<form action="', $scripturl, '?board=', $context['current_board'], ';action=post2" method="post" accept-charset="', $context['character_set'], '" name="postmodify" id="postmodify" onsubmit="submitonce(this);" style="margin: 0;">
						<input type="hidden" name="topic" value="', $context['current_topic'], '" />
						<input type="hidden" name="subject" value="', $context['response_prefix'], $context['subject'], '" />
						<input type="hidden" name="icon" value="xx" />
						<input type="hidden" name="from_qr" value="1" />
						<input type="hidden" name="notify" value="', $context['is_marked_notify'] || !empty($options['auto_notify']) ? '1' : '0', '" />
						<input type="hidden" name="not_approved" value="', !$context['can_reply_approved'], '" />
						<input type="hidden" name="goback" value="', empty($options['return_to_post']) ? '0' : '1', '" />
						<input type="hidden" name="last_msg" value="', $context['topic_last_message'], '" />
						<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
						<input type="hidden" name="seqnum" value="', $context['form_sequence_number'], '" />';


				// Is visual verification enabled?
				if ($context['require_verification'])
					echo '
						<strong>', $txt['verification'], ':</strong>', template_control_verification($context['visual_verification_id'], 'quick_reply'), '<br />';

				echo '
						<div class="quickReplyContent">
							', template_control_richedit($context['post_box_name'], 'smileyBox_message', 'bbcBox_message'),'
						</div>
						<div style="margin-top: 10px;">
							<input type="submit" name="post" value="', $txt['post'], '" onclick="return submitThisOnce(this);" accesskey="s" tabindex="', $context['tabindex']++, '" class="button_submit" />
							<input type="submit" name="preview" value="', $txt['preview'], '" onclick="return submitThisOnce(this);" accesskey="p" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				if ($context['show_spellchecking'])
					echo '
							<input type="button" value="', $txt['spell_check'], '" onclick="spellCheck(\'postmodify\', \'message\');" tabindex="', $context['tabindex']++, '" class="button_submit" />';

				echo '			
						</div>
					</form>
				</div>
			</div>';
		}
	}
}

?>