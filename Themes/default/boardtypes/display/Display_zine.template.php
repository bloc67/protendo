<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings, $boardurl, $topic;

		$orig = $context['frontblog'][0];
		
		if(!empty($context['blogtype_buttons']))
			echo '
		<div class="floatright"> ', $context['themeobject']->theme_button_strip($context['blogtype_buttons']), '</div>';

		echo '
		<div class="bwgrid">
			';
		if(!empty($context['blogimages'][$orig['id_msg']]['original']))
			echo '
			<img alt="" class="imgstyle1" src="' , $context['blogimages'][$orig['id_msg']]['original'] , '">';

		echo '
			<h2 class="blogheader">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
			</h2>
			<div class="breadcrumb_style" style="margin: 1em 0 0 0;">' . $txt['posted_by'] . ' ' .  $orig['poster']['link'] . ' | ' . $orig['time'] . ' ' . $txt['in'] . ' <b>' .  $orig['board']['link'] . '</b></div>';

		if(!empty($orig['hiddentext']['ingress']))
			echo '
			<p class="ingress">', $orig['hiddentext']['ingress'] , '</p>';
		
		echo '
			<div class="post">' , $orig['body'], '</div>
			<ul class="quickbuttons" style="padding-left: 0;">';

		// Can the user modify the contents of this post?
		if ($context['can_modify'])
			echo '
				<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($context['can_remove_post'])
			echo '
				<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

		echo '
			</ul>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']) && empty($orig['hideattached']))
		{
			echo '
			<h3 class="blogheader">' , $txt['attachments'] , '</h3>
			<div id="msg_', $orig['id_msg'], '_footer" class="smalltext padding_vert" style="clear: both;">
				<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', '">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
						<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 40px; " src="', $attachment['micro']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
						<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
						<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
						[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}
			echo '
				</div>
			</div>';
		}

		echo '
			<hr>
			<div class="align_right">' , 	$context['themeobject']->theme_button_strip($context['mod_buttons']), '</div>
		</div>';
	}

	function template_boardtype_comments($author)
	{
		// just run with the bog type comment
		template_blog_comments($author);
	}
}
function template_inserted()
{
	global $settings;

	if(!file_exists($settings['theme_dir'] . '/images/insertbg/zine.jpg'))
		return;

	echo '
	<div  class="insertbg" style="clear: both; height: 298px; background-image: url(' . $settings['images_url'] . '/insertbg/zine.jpg);"></div>';
	
}
?>