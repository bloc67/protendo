<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$orig = $context['frontblog'][0];

		if(!empty($orig['hiddentext']['vimeo']))
			$code = '<iframe id="videolock" src="https://player.vimeo.com/video/' . $orig['hiddentext']['vimeo'] . '" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>';
		else
		{
			if(!empty($orig['hiddentext']['trailer']))
				$code = '<iframe id="videolock" src="http://www.youtube.com/embed/'. $orig['hiddentext']['trailer'] . '?&autoplay=0&controls=1&rel=0" frameborder="0" allowfullscreen></iframe>';
			else
				$code = '<img src="'. $settings['images_url'] . '/novideo.png" alt="" style="width: 100%;" />';
		}

		echo '
		<div class="bwgrid" style="padding-top: 10px;">
			<div id="videocont">
				' , $code , '
			</div>
			<div class="windowbg2" style="clear: both; margin-top: 1em;">
				<h2 class="blogheader">' , $orig['subject'] , '	</h2>
				<span class="greytext breadcrumb_style">'. $txt['posted_by'] . ' <b>' . $orig['poster']['link'] . '</b> -  ' , blogformat($orig['timestamp']) , '</span><hr>
				<div class="post">' , $orig['body'] , '</div><br>
				<ul class="quickbuttons " style="float: right; clear: right; ">';


		// Can the user modify the contents of this post?
		if ($context['can_modify'])
			echo '
					<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($context['can_remove_post'])
			echo '
					<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

		echo '
				</ul>';

		// Assuming there are attachments...
		if (!empty($orig['attachment']))
		{
			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 40px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

		}
		echo '
			', $context['themeobject']->theme_button_strip($context['normal_buttons']), '
				<hr style="clear: both;">		
				', $context['themeobject']->theme_button_strip($context['mod_buttons']), '
			</div>
		</div>';
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}

?>