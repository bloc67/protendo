<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function theme_main()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('DisplayGenealogy','boardtypeobject');
	$context['boardtypeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoDisplay
{
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
		
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		
		$orig = $context['frontblog'][0];

		echo '
		<div class="bwgrid" style="padding-top: 10px;" id="blogarticle">
			<div class="bwcell5">
				<img class="imgstyle2" style="max-width: 100%;" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
			</div>
			<div class="bwcell11">
				<div style="padding: 0 0 0 2em;">
						<div id="bcontainer" class="floatleft">
							<div class="bdate">
								<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
							</div>
						</div>
						<h3 class="blogheader" style="padding-top: 0.3em;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , !empty($orig['new']) && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: both;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . '  -  ' , blogformat($orig['timestamp']) , ' </span>
					<div class="post">
						' , $orig['body'] , '
					</div>
					<br>
					<ul class="quickbuttons " style="clear: both; margin: 0; padding: 0; ">';


			// Can the user modify the contents of this post?
			if ($context['can_modify'])
				echo '
										<li class="modify_button"><a href="', $scripturl, '?action=post;msg=', $orig['id_msg'], ';topic=', $context['current_topic'], '.', $context['start'], '"><span>', $txt['modify'], '</span></a></li>';

			// How about... even... remove it entirely?!
			if ($context['can_remove_post'])
				echo '
										<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;topic=', $context['current_topic'], '.', $context['start'], ';msg=', $orig['id_msg'], ';', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

			echo '
					</ul>
					', $context['themeobject']->theme_button_strip($context['normal_buttons']);
		
		// Assuming there are attachments...
		if (!empty($orig['attachment']) && empty($orig['hideattached']))
		{
			echo '
							<div id="msg_footer" class="attachments smalltext" style="clear: both;">
								<div style="overflow: ', $context['browser']['is_firefox'] ? 'visible' : 'auto', ';">';

			$last_approved_state = 1;
			foreach ($orig['attachment'] as $attachment)
			{
				// Show a special box for unapproved attachments...
				if ($attachment['is_approved'] != $last_approved_state)
				{
					$last_approved_state = 0;
					echo '
									<fieldset>
										<legend>', $txt['attach_awaiting_approve'];

					if ($context['can_approve'])
						echo '&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=all;mid=', $orig['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve_all'], '</a>]';

					echo '</legend>';
				}

				if ($attachment['is_image'])
				{
					if ($attachment['thumbnail']['has_thumb'])
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img style="height: 40px; " src="', $attachment['thumbnail']['href'], '" alt="" id="thumb_', $attachment['id'], '" /></a>&nbsp;';
					else
						echo '
										<a href="', $attachment['href'], '" id="link_', $attachment['id'], '" class="remooz"><img src="' . $attachment['href'] . '" alt="" style="height: 40px;" /></a>';
				}
				else
					echo '
										<a href="' . $attachment['href'] . '"><img src="' . $settings['images_url'] . '/file.png" style="height: 40px;" /></a>&nbsp;';

				if (!$attachment['is_approved'] && $context['can_approve'])
					echo '
										[<a href="', $scripturl, '?action=attachapprove;sa=approve;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['approve'], '</a>]&nbsp;|&nbsp;[<a href="', $scripturl, '?action=attachapprove;sa=reject;aid=', $attachment['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['delete'], '</a>] ';
			}

			// If we had unapproved attachments clean up.
			if ($last_approved_state == 0)
				echo '
									</fieldset>';

			echo '
								</div>
							</div>';
		}

		echo '
			
					</div>
			</div>
		</div>
		<hr />';
		$context['themeobject']->theme_button_strip($context['mod_buttons'],'eft');
	}

	function template_boardtype_comments($author)
	{
		global $topic_info, $context, $settings, $options, $txt, $scripturl;

		// just run with the bog type comment
		$context['subthemeobject']->template_blog_comments($author);
	}
}
?>