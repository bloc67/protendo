<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Gallery');
		$context['subthemeobject']->template_main();
	}
}
		
function template_altgallery()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Gallery');
	$context['subthemeobject']->template_altgallery();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoGallery
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		if(!empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['gallery'] , '</h2>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		echo '
		<div>' , $this->template_altgallery() , '</div>			
		<div class="pagelinks">' , $context['page_index'] , '</div>';
	}


	function template_altgallery()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '
		<div>';

		foreach($context['blogboards'] as $blog)
			echo '
				<a class="new blue" href="' . $scripturl . '?board='.$blog['id_board'].'">' . $blog['name'] . '</a>';
		
		echo '
		</div>
		<div class="bwgrid" style="margin-top: 6px;">';
		
		$count = 0; $class=array('0' => '_first', '1' => '', '2' => '_third');
		foreach($context['frontblog'] as $orig)
		{
			
			echo '
			<div class="triplecell' . $class[$count] . '">
				<a href="' . $scripturl . '?topic=' . $orig['topic'] .'"><img src="' . (!empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 100%;" /></a><span class="shadow2"></span>
				<h3 class="breadcrumb_style"><a href="' . $scripturl . '?topic=' . $orig['topic'] .'"><b>' , $orig['subject'] , '</b></a>
					' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '') . '</h3>
				<br>
			</div>';
			$count++;
			if($count>2)
				$count = 0;
		}
		echo '
		</div>
		<div>';

		foreach($context['blogboards'] as $blog)
			echo '
				<a class="new blue" href="' . $scripturl . '?board='.$blog['id_board'].'">' . $blog['name'] . '</a>';
		
		echo '
		</div>';
	}
}

?>