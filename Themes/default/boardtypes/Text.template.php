<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Text');
		$context['subthemeobject']->template_main();
	}
}
		
function template_text()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Text');
	$context['subthemeobject']->template_text();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoText
{
	function template_main()
	{
		$this->template_text();
	}

	function template_text()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;

			
		if(!empty($context['normal_buttons']))
			echo '
			<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['text'] , '</h2>';
		
		if(!isset($context['frontblog']))
		{
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		echo '
		<div class="pagelinks">
				 ', $context['page_index'] ,  '
		</div>';

		if(count($context['frontblog'])<1)
		{
			echo '
		<div class="headerarea"><strong class="largetext">', $txt['msg_alert_none'], '</strong></div>';
		}
		else
		{
			foreach($context['frontblog'] as $orig)
			{
				echo '
				<div class="padding_vert" style="margin: 2px; overflow: hidden; position: relative;">
					<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 2em 5px 0;position: relative;"><img class="imgstyle3" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage300x200.png' , '" alt="" />
					<span class="imgingress">' , $context['blogimages'][$orig['id_msg']]['filename'],'</span>
					</a>
					<h3 class="textheader2"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<span class="smalltext">' , $orig['time'] , ' 
					| ' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , ' </span> 
					<div class="textcolumns padding_vert">' , $orig['body'] , '...</div>
					&nbsp;' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="smalltext">' , $orig['replies'] , ' ' , $txt['replies'] , ' | ' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
			}

		}
		echo '
		<div class="headerarea2">' , $this->article_cats() , '</div>
		<div class="pagelinks">
			 ', $context['page_index'], '
		</div>';
	}

	function article_cats()
	{
		global $context, $settings, $options, $scripturl, $txt;
		
		$items = array();
		foreach($context['blogboards'] as $art)
			$items[] = '<a class="breadcrumb_style" style="font-size: 1.1em;" href="' . $scripturl . '?board=' . $art['id_board'] . '">' . $art['name'] . '</a>';
		
		echo implode(" | ", $items);
	}
}

?>