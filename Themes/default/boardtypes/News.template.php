<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('News');
		$context['subthemeobject']->template_main();
	}
}
		
function template_news()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('News');
	$context['subthemeobject']->template_news();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoNews
{	
	function template_main()
	{
		$this->template_news();
	}

	function template_news()
	{
		global $context, $settings, $options, $scripturl, $txt;

			if(!empty($context['normal_buttons']))
				echo '
			<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['news'] , '</h2><hr>';

		if(!isset($context['frontblog']))
		{
				echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
				return;
		}

		if(count($context['frontblog'])<1)
		{
			echo '
		<div class="headerarea"><strong class="largetext">', $txt['msg_alert_none'], '</strong></div>';
		}
		else
		{
			$count = 0;
			foreach($context['frontblog'] as $orig)
			{
				if(!empty($orig['hiddentext']['newsarticle_title']))
					echo '
							<div class="windowbg4" style="overflow: hidden;">
								<img src="' , $orig['hiddentext']['newsarticle_image'] , '" alt="" style="float: left; width: 15%; margin-right: 1em;" />
								<p><a href="' ,$orig['hiddentext']['newsarticle'] ,'" target="_blank"><b>', $orig['hiddentext']['newsarticle_title'] , '</b></a><br>
									&quot;', $orig['hiddentext']['newsarticle_text'] , '&quot;
								</p>
							</div>';
				echo '
				<div>
					<h3 class="textheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
						&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					
					
					<span class="breadcrumb_style">' , $orig['time'] , '</span> 
					<span class="breadcrumb_style">' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> 
					<div class="padding_vert">
						<a class="floatright" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 0 5px 2em; width: 30%;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
						<div class="padding_vert">' , $orig['body'] , '...</div>
					</div>
					&nbsp;' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="breadcrumb_style">' , $orig['replies'] , ' ' , $txt['replies'] , ' | </span>
					<span class="breadcrumb_style">' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
				break;
			}
			$count = 1;
			echo '
		<div class="bwgrid">
			<div class="bwcell10">';
			
			$b  = array_shift($context['frontblog']);
			$split = 3;

			foreach($context['frontblog'] as $orig)
			{
				if($count==$split)
					echo '
			</div><div class="bwcell6"><div class="inner_right">';

				// left column
				if($count<$split)
					echo '
				<div class="bwgrid">
					<h3 class="blogheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 2em 0 0;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
					<span class="breadcrumb_style" style="padding-left: 0; margin-left: 0;">' , $orig['time'] , '<br>' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> 
					<div class="padding_vert">' , $orig['body'] , '...</div>
					' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="breadcrumb_style" style="padding-left: 0; margin-left: 0;">' , $orig['replies'] , ' ' , $txt['replies'] , ' | ' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
				
				// last..and also setting up the last column in half
				if($count==$split)
					echo '
				<div class="bwgrid"><div class="bwcell16"><div class="windowbg">	';
				
				if($count>$split || $count==$split)
					echo '
				<div class="bwgrid">
					<h3 class="blogheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 0 10px 0;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
					<span class="breadcrumb_style" style="padding-left: 0; margin-left: 0;">' , $orig['time'] , '</span><br> 
					' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> <hr>
					<div class="middletext padding_both">' , substr($orig['body'],0,80) , '...</div>
				</div>';
				
				$count++;
			}
			// ok, we ar done, time for the extra stuff
			echo '
				</div></div></div></div>
			</div>
		</div>';
		}
	}	
}
?>