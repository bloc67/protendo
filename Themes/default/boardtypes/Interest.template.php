<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('Interest');
		$context['subthemeobject']->template_main();
	}
}
		
function template_interest()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Interest');
	$context['subthemeobject']->template_interest();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoInterest
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		if(!empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['interest'] , '</h2>';

		if(!isset($context['frontblog']))
		{
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
			return;
		}
		$this->template_interest();
	}

	function template_interest()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$count = 0;
		
		echo '
	<div class="bwgrid">';

		$cols = array(0=>'',1=>'',2=>'');
		foreach($context['frontblog'] as $p => $d)
		{
			$cols[$count][] = $p;
			$count++;
			if($count>2)
				$count=0;
		}
		
		foreach($cols as $what => $g)
		{
			$gcols = array(0 => ' gleft' , 1 => '', 2 => ' gright');
			echo '
		<div class="bwcell33">';
			
			foreach($g as $w)
			{
				$orig = $context['frontblog'][$w];
				echo '
			<div class="gcolumn' , $gcols[$what] , '">
				<div class="bwgrid">
					<a href="' . $orig['poster']['href'] . '"><img class="roundavatar floatleft" src="' . $orig['poster']['avatar']. '" alt="" style="margin: 0px 1em 1em 0;" /></a>
					<h3 class="blogheader"><a href="' . $scripturl . '?topic=' . $orig['topic'] . '"><b>' . $orig['subject'] . '</b></a>
						' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '') . '
					</h3>
					<span class="smalltext">
						' . $txt['posted_by'] . ' ' . $orig['poster']['link'] . '  ' , blogformat($orig['timestamp'], true) , ' 
					</span>
				</div>
				<div class="post">' . $orig['body'] . '</div>
				<a href="' . $scripturl . '?topic=' . $orig['topic'] . '"><img class="imgstyle1" src="' . (!empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 100%;" /></a>
				<p class="greytext breadcrumb_style">
					<a href="' . $orig['href'] . '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> 
					' . ($orig['new'] && !empty($orig['replies']) ? ' <a class="new" style="display: inline-block; margin: 5px 0;" href="' . $orig['new_href'] . '">' . $orig['replies'] . ' ' . $txt['new'] . '</a>' : '') . '
					| ' . $orig['views'] . ' ' . $txt['views'] . ' | ' . $orig['board']['link'] . '
				</p>
				<div style="padding: 0 0 0 0.2em;">' , $this->template_showreplies($orig['topic'], $orig['id_msg'], $orig['replies']) , '</div>
			</div>';
			}
			echo '
		</div>';
		}

		echo '
	</div>';
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}

	function template_showreplies($t, $msg, $total = 0, $var = false)
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!isset($context['frontblog_replies'][$t]))
			return; 
		$temp = array_reverse($context['frontblog_replies'][$t]);
		foreach($temp as $post)
		{
			if($post['id_msg']==$msg)
				continue;

			echo '
			<div class="bwgrid" style="line-height: 1.2em; margin: 1em 0;">
				<div class="bwcell2">
					<div style="padding: 0.1em 0.5em 0.2em 0.1em;">
						<a href="' , $scripturl , '?action=profile;u=' , $post['id_member'] , '"><img class="imgstyle_round" src="' , $post['avvy'], '" alt="" /></a>
					</div>
				</div>
				<div class="bwcell14">
					<a href="' , $scripturl , '?action=profile;u=' , $post['id_member'] , '" class="middletext"><b>' , $post['poster_name'], '</b></a>
					<div class="smalltext greytext">' , blogformat($post['poster_time']) , '</div>
					<div id="qpost' . $post['id_msg'] . '" class="post middletext">' , substr(strip_tags($post['body']),0,200) , '...</div>
				</div>
			</div>';
		}
		// more replies ?
		if($total>3)
			echo '
			<div class="windowbg2"><a class="smalltext" href="' ,  $scripturl, '?topic=' . $t . '">' , sprintf($txt['theremore'], ($total-3)) , '</a></div>';
	}
}

?>