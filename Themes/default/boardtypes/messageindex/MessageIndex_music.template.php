<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexMusic','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexMusic
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $board;

		echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['current_boardname'] , '</h2>
		<div class="pagelinks">' , $context['page_index'] , '</div>';

		if(sizeof($context['frontblog']) == 0)
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
		{
			echo '
			<div class="bwgrid">';
			$count = 0; $class=array('0' => '_first', '1' => '', '2' => '_third');
			foreach($context['frontblog'] as $orig)
			{
				// check if we got a imge from youtube, if not, use any blog
				$img = $settings['images_url'].'/novideo.png';
				if(!empty($orig['hiddentext']['clip']))
					$img = $context['blogimages'][$orig['id_msg']]['thumb'];

		
				echo '
				<div class="triplecell' . $class[$count] . '">
					<div class="bwgrid">
						<div class="padding_vert">
							<a href="' . $orig['href'].'"><img class="imgstyle1" src="' , $img , '" alt="" /></a>
						</div>
						<div class="bwcell16">
							<h3 class="textheader" style="margin: 0;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
								' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
							</h3>
							<span class="breadcrumb_style" style="display: block; padding: 5px 0;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' . $orig['time'] . ' </span>
							' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
							<br>
						</div>
					</div>
				</div>';
				$count++;
				if($count>2)
				{
					echo '<br class="clear">';
					$count= 0;
				}
			}
			echo '
			</div>';
			echo '
			<div class="pagelinks">' , $context['page_index'] , '</div><br>';
		}
	}
}

?>