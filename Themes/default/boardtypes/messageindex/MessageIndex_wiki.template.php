<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexWiki','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexWiki
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $board;

		echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['current_boardname'] , '</h2>
		<div class="pagelinks">' , $context['page_index'] , '</div>';

		if(sizeof($context['frontblog']) == 0)
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
		{
			$alt = true;
			$classes = array(
				'0' => 'mess_prog0',
				'1' => 'mess_prog20',
				'2' => 'mess_prog100',
				'3' => 'mess_solved',
			);
			foreach($context['frontblog'] as $orig)
			{
				echo '
			<div class="bwgrid">
				<div class="windowbg' , $alt ? '' : '2' , '">
					<span class="messicons ' , !empty($orig['hiddentext']['bg_status']) ? $classes[$orig['hiddentext']['bg_status']] : 'mess_xx' , '" style="margin-bottom: -6px;"></span>&nbsp;&nbsp;
					<span class="greytext breadcrumb_style"><span style="min-width: 8em;display: inline-block;">' , blogformat($orig['timestamp']) , '</span> <a href="' . $orig['href'].'"><b>' , $orig['subject'] , '</b></a></span>
					' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
					
					<span class="greytext breadcrumb_style floatright">' , $txt['posted_by'] , ' ' . $orig['poster']['link'] . ' | <a href="' , $orig['href'] , '#comments">' . $orig['replies'] . ' ' . $txt['replies'] . '</a> | ' . $orig['views'] . ' ' . $txt['views'] . '</span>
					' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
				</div>
			</div>';
			$alt = !$alt;
			}
			echo '
			<div class="pagelinks">' , $context['page_index'] , '</div><br>';
		}
	}
}

?>