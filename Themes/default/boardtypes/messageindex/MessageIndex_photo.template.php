<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexPhoto','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexPhoto
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		if(!empty($options['messageindex_altgallery']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['name'] , '</h2>
		<div class="pagelinks">
				 ', $context['page_index'] ,  '
		</div>
		<div>' ,  $this->template_altgallery() , '</div>';
		else
			echo '
		<div class="floatright" >' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $txt['gallery'] , '</h2>
		<div class="pagelinks">' , $context['page_index'] , '</div>
		<div class="bwgrid">
		<div class="bwcell12">' ,  $this->template_gallery() , '</div>
			<div class="bwcell4">
				<div style="padding-left: 1em;">' , $this->template_side_gallery() , '</div>
			</div>
		</div>
		<div class="pagelinks">' , $context['page_index'] , '</div>';
	}

	function template_side_gallery()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$alt = true;
		
		echo '
		<div style="padding: 0.1em 1em;">';

		if(!empty($context['blogmostread']))
		{
			echo '
			<h3 class="blogheader">' , $txt['most_read'] , '</h3><hr>
			';
			foreach($context['blogmostread'] as $u)
				echo '<a href="' . $scripturl . '?topic=' . $u['id_topic'] . '">' . $u['subject'] . ' (' . $u['num_views'] . ')</a> ' . $txt['by'] . ' <a href="' . $scripturl . '?action=profile;u=' . $u['id_member'] . '">' . $u['real_name']. '</a><br>';
		}

		echo '
		</div>';
	}

	function template_gallery()
	{
		global $context, $settings, $options, $txt, $scripturl;

		$rows=array('1' => '', '2' => '', '3' => '', '4' => '');
		$count=1;
		echo '
		<div class="bwgrid">';
		foreach($context['frontblog'] as $orig)
		{
			if($count>4)
				$count=1;

			$rows[$count] .= '
					<a href="' . (!empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png') . '" class="remooz">
					<img class="imgstyle" src="' . (!empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png') . '" alt="" style="margin: 0; padding: 0;width: 100%;border: solid 1px #222;" />
					<span class="img_ingress"><p style="padding: 1em 1.5em;">' . (substr($orig['body'],0,70)) . '...</p></span>
					</a><br>
				<a href="' . $scripturl . '?topic=' . $orig['topic'] .'">
						<span class="breadcrumb_style smalltext">' . $orig['subject'] . '</span>
					</a>
					' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '') . '
					<br>
					' . ($orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '&nbsp;') . '
					<br>
					';	
				$count++;
		}
		foreach(array(1,2,3,4) as $a)
			echo '
			<div class="bwcell4" style="position: relative;">
			' . $rows[$a] . '
			</div>';
		
		echo '
		</div>';

	}

	function template_altgallery()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '
		<div class="bwgrid">';

		foreach($context['frontblog'] as $orig)
		{
			echo '
				<a href="#imgcell' . $orig['topic'] . '" style="text-decoration: none;">
					<img src="' . (!empty($context['blogimages'][$orig['id_msg']]['micro']) ? $context['blogimages'][$orig['id_msg']]['micro'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 70px; margin: 2px;" />
				</a>';
		}
		echo '	
		</div><hr>';
			
		foreach($context['frontblog'] as $orig)
		{
			echo '
		<div class="bwgrid" id="imgcell' . $orig['topic'] . '">
			<div class="bwcell16">
				<h3 class="blogheader"><a href="' . $scripturl . '?topic=' . $orig['topic'] .'">' , $orig['subject'] , '</a></h3>
					' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '') . '
				<hr>
			</div>
			<div class="bwcell6">
					' . ($orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '') . '
					<span class="new">' , $orig['views'] , ' ' , $txt['views'] , '</span>
					<span class="new">' , $orig['replies'] , ' ' , $txt['replies'] , '</span>
					<p class="middletext">' , $orig['body'] , '</p>
			</div>
			<div class="bwcell10"><div style="padding: 10px 10px 10px 1em;">
				<a href="' . $scripturl . '?topic=' . $orig['topic'] .'">
					<img class="imgstyle2" src="' . (!empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 100%;" />
				</a>
			</div></div>
		</div>';
				
		}

		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}
}

?>