<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

function template_main()
{
	global $context, $settings, $options, $scripturl, $modSettings, $txt;

	echo '
	  <h2 class="mainheader">' , $context['name'] , '</h2>';
	if(!isset($context['frontblog']))
	{
		echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
		return;
	}
	echo '
	<div class="pagelinks">
		 ', $context['page_index'] ,  '
	</div>
	<div class="bwgrid">';

	$count = 0; $class=array('0' => 'doublecell_first', '1' => 'doublecell_second');
	foreach($context['frontblog'] as $orig)
	{
		echo '
		<div class="', $class[$count] , '">
	<div style="overflow: hidden; font-family: arial;">
		<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="text-decoration: none;margin-bottom: 1em;display: block; overflow: hidden; height: 600px; background: url(' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png' , ') no-repeat #000 top left; background-size: cover; width: 100%;">&nbsp;</a>
		<div>
			<div class="floatright new">' , $orig['replies'] , ' ' , $txt['replies'] , ' / ' , $orig['views'] , ' ' , $txt['views'] , '</div>
			<h3 class="blogheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
			' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
			</h3>
			<div class="post">' , substr($orig['body'],0, 400) , '...</div>
			<div class="greytext breadcrumb_style padding_vert"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
				' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
			</div>
		</div>
	</div>
		</div>';
		$count++;
		if($count>1)
			$count = 0;
	}
	echo '
	</div>
	<div class="pagelinks">
		 ', $context['page_index'], '
	</div>';

	echo '<br style="clear: both;" />';
}

function template_childboards() { return; }
function template_description() { return; }
function template_topicsheader() { return; }
function template_topicsfooter() { return; }
function template_topics() { return; }
function template_messageindex_below() { return; }
function template_inserted()
{
	global $settings;

	if(!file_exists($settings['theme_dir'] . '/images/insertbg/zine.jpg'))
		return;

	echo '
	<div  class="insertbg" style="clear: both; height: 298px; background-image: url(' . $settings['images_url'] . '/insertbg/zine.jpg);"></div>';
	
}

?>