<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindex
{
	function theme_main()
	{
		global $context, $settings, $options, $scripturl, $modSettings, $txt;

		if(!empty($context['normal_buttons']))
			echo '
		<div style="float: right;">
			' , $context['themeobject']->theme_button_strip($context['normal_buttons']), '
		</div>';

		echo '
		<h2 class="mainheader">', $context['name'] , '</h2>
		<div class="pagelinks" style="margin-top: 6px;">
			', $context['page_index'] , $context['menu_separator'] . '<a href="#bot">' . $txt['go_down'] . '</a>
		</div>';

		if (!empty($context['description']))
			echo '
		<p class="headerarea">', $context['description'] , '</p>';

		if(sizeof($context['frontblog']) == 0)
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
		{
			echo '
		<div class="bwgrid">';
			$count = 0; $classgrid=array('_first','','_third');
			foreach($context['frontblog'] as $orig)
			{
				echo '
			<div class="triplecell' , $classgrid[$count] , '">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="display: block; overflow: hidden; height: 200px; background: url(' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png' , ') no-repeat white; background-size: cover;">&nbsp;</a>
				<h3 class="blogheader" style="margin: 8px 0 2px 0; ">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
				</h3>
				<span class="breadcrumb_style">' . $txt['posted_by'] . ' ' .  $orig['poster']['link'] . ' |
				' . $orig['time'] . ' ' . $txt['in'] . ' <b>' .  $orig['board']['link'] . '</b></span>
				<div class="post "' , $count<2 ? '' : ' style="min-height: 5em;"' , '>' , substr($orig['body'],0,($count<3 ? '300' : '90')) , '...</div>
				' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
				<br>
			</div>';
				$count++;
				if($count == 3)
					$count=0;
			}
			echo '
		</div>';
		}
		echo '
			<div class="pagelinks" style="clear: both;">', $context['page_index'], '	</div><br>';
	}
}

?>