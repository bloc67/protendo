<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexNews','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexNews
{
	function template_main()
	{
		global $context, $settings, $options, $scripturl, $txt;

		echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['name'] , '</h2>';

		if(count($context['frontblog'])<1)
		{
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		}
		else
		{
			$count = 0;
			foreach($context['frontblog'] as $orig)
			{
				echo '
				<div>
					<h3 class="textheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
						&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					
					
					<span class="breadcrumb_style">&nbsp;' , $orig['time'] , '</span> 
					<span class="breadcrumb_style">&nbsp;&nbsp;' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> 
					<div class="padding_vert">
						<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 25px 15px 0;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>				
						<div class="padding_both">' , $orig['body'] , '...</div>
					</div>
					&nbsp;' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="breadcrumb_style">' , $orig['replies'] , ' ' , $txt['replies'] , ' | </span>
					<span class="breadcrumb_style">' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
				break;
			}
			$count = 1;
			echo '<br class="clear">
		<div class="bwgrid">
			<div class="doublecell_first">';
			
			$b  = array_shift($context['frontblog']);
			$split = 3;
			foreach($context['frontblog'] as $orig)
			{
				if($count==$split)
					echo '
			</div><div class="doublecell_second">';

				// left column
				if($count<$split)
					echo '
				<div>
					<h3 class="blogheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 2em 0 0;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
					<span class="breadcrumb_style" style="padding: 1px 10px;">' , $orig['time'] , '</span> 
					<span class="breadcrumb_style">&nbsp;&nbsp;' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> 
					<div class="padding_vert">' , $orig['body'] , '...</div>
					&nbsp;' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="breadcrumb_style">' , $orig['replies'] , ' ' , $txt['replies'] , ' | </span> 
					<span class="breadcrumb_style">' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
				
				if($count>$split || $count==$split)
					echo '
				<div>
					<h3 class="blogheader"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 0 10px 0;"><img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" /></a>
					<span class="breadcrumb_style">' , $orig['time'] , '</span><br> 
					<span class="breadcrumb_style">' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , '</span> <hr>
					<div class="middletext">' , substr($orig['body'],0,200) , '...</div>
				</div>';
				
				$count++;
			}
			// ok, we ar done, time for the extra stuff
			echo '
			</div>
		</div>';
		}
		echo '
		<div class="pagelinks">
			 ', $context['page_index'], '
		</div>';
	}
}

?>