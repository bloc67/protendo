<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexText','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexText
{
	function template_main()
	{
		global $context, $settings, $options, $scripturl, $txt;

		echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['name'] , '</h2>
		<div class="pagelinks">
			 ', $context['page_index'] ,  '
		</div>';

		if(count($context['frontblog'])<1)
		{
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		}
		else
		{
			foreach($context['frontblog'] as $orig)
			{
				echo '
				<div class="padding_vert" style="margin: 2px; overflow: hidden; position: relative;">
					<a class="floatleft" href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="margin: 0 2em 5px 0;position: relative;"><img class="imgstyle3" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
					<span class="imgingress">' , $context['blogimages'][$orig['id_msg']]['filename'],'</span>
					</a>
					<h3 class="textheader2"><a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					&nbsp;' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
					</h3>
					<span class="smalltext">' , $orig['time'] , ' 
					| ' , $txt['posted_by'] , ' '  , $orig['poster']['link'] , ' </span> 
					<div class="textcolumns padding_vert">' , $orig['body'] , '...</div>
					&nbsp;' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
					<span class="smalltext">' , $orig['replies'] , ' ' , $txt['replies'] , ' | ' , $orig['views'] , ' ' , $txt['views'] , '</span>
				</div>';
			}

		}
		echo '
		<div class="pagelinks">
			 ', $context['page_index'], '
		</div>';
	}
}

?>