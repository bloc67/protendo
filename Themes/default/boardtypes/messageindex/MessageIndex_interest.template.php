<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('MessageindexInterest','boardtypeobject');
		$context['boardtypeobject']->template_main();
	}
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindexInterest
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $modSettings;
		
		echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['name'] , '</h2>';

		$count = 0;
		
		echo '
	<div class="bwgrid">';

		$cols = array(0=>'',1=>'',2=>'');
		foreach($context['frontblog'] as $p => $d)
		{
			$cols[$count][] = $p;
			$count++;
			if($count>2)
				$count=0;
		}
		
		foreach($cols as $what => $g)
		{
			$gcols = array(0 => ' gleft' , 1 => '', 2 => ' gright');
			echo '
		<div class="bwcell33">';
			
			foreach($g as $w)
			{
				$orig = $context['frontblog'][$w];
				echo '
			<div class="gcolumn' , $gcols[$what] , '">
				<div class="bwgrid">
					<a href="' . $orig['poster']['href'] . '"><img class="roundavatar floatleft" src="' . $orig['poster']['avatar']. '" alt="" style="margin: 0px 1em 1em 0;" /></a>
					<h3 class="breadcrumb_style"><a href="' . $scripturl . '?topic=' . $orig['topic'] . '"><b>' . $orig['subject'] . '</b></a>
						' . ($orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '') . '
					</h3>
					<span class="middletext">
						' . $txt['posted_by'] . ' ' . $orig['poster']['link'] . ' <span class="greytext"> </span> ' , blogformat($orig['timestamp'], true) , ' 
					</span>
				</div>
				<div class="post">' . $orig['body'] . '</div>
				<a href="' . $scripturl . '?topic=' . $orig['topic'] . '"><img class="imgstyle1" src="' . (!empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png') . '" alt="" style="width: 100%;" /></a>
				<p class="greytext breadcrumb_style">
					<a href="' . $orig['href'] . '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> 
					' . ($orig['new'] && !empty($orig['replies']) ? ' <a class="new" style="display: inline-block; margin: 5px 0;" href="' . $orig['new_href'] . '">' . $orig['replies'] . ' ' . $txt['new'] . '</a>' : '') . '
					| ' . $orig['views'] . ' ' . $txt['views'] . ' | ' . $orig['board']['link'] . '
				</p>
			</div>';
			}
			echo '
		</div>';
		}

		echo '
	</div>';
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}
}

?>