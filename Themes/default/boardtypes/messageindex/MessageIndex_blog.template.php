<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageindex
{
	function theme_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $board;

		echo '
		<div class="floatright">' , $context['themeobject']->theme_button_strip($context['normal_buttons']), '</div>
		<h2 class="mainheader">' , $context['current_boardname'] , '</h2>
		<div class="pagelinks">' , $context['page_index'] , '</div>';

		if(empty($options['messageindex_blogtitles']))
			$this->show_fullposts();
		else
			$this->show_titlesonly();
	}
		
	function show_fullposts()
	{
		global $context, $settings, $options, $txt, $scripturl, $board;

		if(sizeof($context['frontblog']) == 0)
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
		{
			echo '<br><div id="blog">';
			foreach($context['frontblog'] as $orig)
			{
				if(empty($options['messageindex_imagetop']))
					echo '
				<div class="bwgrid">
					<div class="bwcell5">
						<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
							<img class="imgstyle2 boxshadow1" style="margin: 4px 20px 60px 20px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
						</a>
					</div>
					<div class="bwcell11">
						<div class="inner_right">
						<div id="bcontainer" class="floatleft">
							<div class="bdate">
								<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
							</div>
						</div>
						<h3 class="blogheader" style="padding-top: 0.3em;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: both;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' . $orig['time'] . ' </span>
							<div class="post">' , $orig['body'] , ' </div>
							<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
							' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
							<br>
						</div>
					</div>
				</div>
				<br />';
				else
					echo '
				<div class="bwgrid">
					<div class="bwcell8">
						<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
							<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/noimage.png' , '" alt="" />
						</a>
					</div>
					<div class="bwcell8">
						<div style="padding: 0 0 0 3em;">
						<div id="bcontainer" class="floatleft">
							<div class="bdate">
								<span class="daydate">' , date("d",$orig['timestamp']) , ' </span><p><span class="topdate">' , date("M",$orig['timestamp']) , '</span><span>' , date("Y",$orig['timestamp']) , '</span></p>
							</div>
						</div>
						<h3 class="blogheader" style="padding-top: 0.3em;"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
							' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
						</h3>
						<span class="middletext greytext" style="padding: 0.5em 0; display: block;clear: both;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> ' . $txt['in'] . ' ' . $orig['board']['link'] . ' -  ' . $orig['time'] . ' </span>
							<div class="post">' , $orig['body'] , ' </div>
							<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
							' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '</p>
							<br>
						</div>
					</div>
				</div>
				<br />';
			}
			echo '
			</div>';
		}
		echo '
		<div class="pagelinks">' , $context['page_index'] , '</div><br>';
	}

	function show_titlesonly()
	{
		global $context, $settings, $options, $txt, $scripturl, $board;

		foreach($context['frontblog'] as $orig)
		{
			echo '
		<div class="bwgrid">
			<div style="padding: 0 0 0 2em;">
				<div id="bcontainer" class="floatleft">
					<div class="bdate">
						<p>' , date("d",$orig['timestamp']) , ' <span>' , date("M",$orig['timestamp']) , '</span></p>
					</div>
				</div>
				<h3 class="mainheader"><a title="' , $txt['preview'] , ' :: ' , strip_tags($orig['body']) , '" class="TipBig" href="' . $orig['href'].'">' , $orig['subject'] , '</a>
				' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
				</h3>
				<span class="greytext breadcrumb_style" style="display: block; ">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b> -  ' . $orig['time'] . ' &nbsp;&nbsp;/ <a href="' , $orig['href'] , '#comments"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '</span>
				' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '<br>
		</div>
		</div>';
		}
		echo '
		<div class="pagelinks">' , $context['page_index'] , '</div><br>';
	}
	function trueblogDate($tid)
	{
		echo '<div class="greytext smalltext" style="display: block; padding: 3px 0; text-align: center;">' , date("Y",$tid) , '</div><div class="infobox2" style="font-size: 11px; margin: 0 auto; padding: 4px; text-align: center;">' , date("H:m",$tid) , '</div>';
	}
}

?>