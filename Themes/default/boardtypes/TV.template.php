<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
if(!function_exists('template_main'))
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		createThemeObject('TV');
		$context['subthemeobject']->template_main();
	}
}
		
function template_tv()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('TV');
	$context['subthemeobject']->template_tv();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoTV
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['blogboards']) && !empty($context['normal_buttons']))
			echo '
	<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
	<h2 class="mainheader">TVserier</h2>';

		if(!isset($context['frontblog']))
			echo '
	<hr><strong>', $txt['msg_alert_none'], '</strong>';
		else
			$this->template_tv();
	}

	function template_tv()
	{
		global $context, $settings, $options, $txt, $scripturl;

		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
		
		echo '
		<div class="bwgrid" style="margin-top: 8px;">';
		foreach($context['frontblog'] as $orig)
		{
			// get the ratings
			$ephtml = '';
			$total = 0; $number = 0;
			if(!empty($orig['hiddentext']['episodes']))
			{
				$eps = unserialize($orig['hiddentext']['episodes']);
				$ephtml = '';
				foreach($eps as $e => $dat)
				{
					$number++;
					$total = $total + $dat['rate'];
					$ephtml .= '<span class="notice">' . $e . '</span>';
				}
				$orig['hiddentext']['tvscore'] = ceil($total/$number);
			}

			echo '
			<div class="bwcell16">	
				<div class="bwgrid">
					<div class="bwcell5">';

			echo '
						<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
							<img class="imgstyle2" style="margin-top: 10px;" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
						</a>	';

			if(isset($orig['hiddentext']['tvscore']))
				echo '
						<span class="imdb_myscore" style="margin-left: .5em;"><span style="width: ', $orig['hiddentext']['tvscore']*10 , '%;"></span></span>' , $orig['hiddentext']['tvscore'].'/10';
				
			if(isset($ephtml))
				echo '
						<hr><p>Episoder sett:'. $ephtml.'</p>';
			echo '
					</div>
					<div class="bwcell11">
						<div style="padding: 0 0 0 2em;">
							<h3 class="blogheader"><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>
								' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier" style="display: inline-block;">&nbsp;</span>' : '' , '
							</h3>';


			echo '
							
							<span class="breadcrumb" style="display: block;">' , $txt['posted_by'] , ' <b>' . $orig['poster']['link'] . '</b>  -  ' . $orig['time'] . ' </span>
							<div class="post">' , $orig['body'] , ' </div>
							<p class="greytext breadcrumb_style"><a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
								' , $orig['new'] && !empty($orig['replies']) ? '&nbsp;&nbsp;<span class="new" style="display: inline-block;"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
							</p>
							<br>';
			if(!empty($orig['hiddentext']['tvseries_overview']))
				echo '
						<div class="greytext"><em>' , $orig['hiddentext']['tvseries_overview'].'</em></div>';
			echo '
						</div>
					</div>
				</div>
			</div>';
		}
		echo '
		<br class="clear"><br></div>';
		if(!empty($context['page_index']))
			echo '<div class="pagelinks">' , $context['page_index'] , '</div>';
	}

	function template_showmostrated()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '<br>
		<h3 class="largerfont">' , $txt['best_rated'] , '</h3><hr>';
		foreach($context['high_rated'] as $w => $data)
		{
			echo '
			<p>
				<span class="floatright">' , $data['value_int'] , '<small>/10</small></span>
				<a href="' , $scripturl , '?topic=' , $data['id_topic'] , '"><b>' , $data['subject'] , '</b></a> ' , $txt['by'] , ' <a href="' , $scripturl , '?action=profile;u=' , $data['id_member'] , '">' , $data['real_name'] , '</a>
			</p>';
		}
		
	}

	function template_showtotalwannasee()
	{
		global $context, $settings, $options, $txt, $scripturl;

		echo '<br>
		<h3 class="largerfont">' , $txt['wannaseetitle'] , '</h3><hr>';
		foreach($context['most_wannasee'] as $w => $data)
		{
			echo '
			<p>
				<span class="floatright">' , $data['value_int'] , ' <small>' , $txt['members'] , '</small></span>
				<a href="' , $scripturl , '?topic=' , $data['id_topic'] , '"><b>' , $data['subject'] , '</b></a> ' , $txt['by'] , ' <a href="' , $scripturl , '?action=profile;u=' , $data['id_member'] , '">' , $data['real_name'] , '</a>
			</p>';
		}
	}
}
	

?>