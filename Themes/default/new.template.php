<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// Initialize the template... mainly little settings.
function pre_template()
{
	global $context, $settings, $options, $txt, $scripturl;

	$settings['use_default_images'] = 'never';
	$settings['doctype'] = 'html';
	$settings['theme_version'] = '1.0';
	$settings['message_index_preview'] = true;
	$settings['subtheme'] = '{THEMECLASS}';

	$settings['fixedheader'] = '
		<script type="text/javascript">
			/* fixed header */
			window.addEvent(\'domready\',function() {
				new SmoothScroll({duration:50});
				$(\'fixedtopheader\').set(\'opacity\',\'0\').setStyle(\'display\',\'block\');
				var ss = new ScrollSpy({
					min: 50,
					onEnter: function(position,enters) {
						$(\'fixedtopheader\').fade(\'in\');
					},
					onLeave: function(position,leaves) {
						$(\'fixedtopheader\').fade(\'out\');
					},
					container: window
				});
			});		
		</script>';	
}

/* *************** */
/* the class definitions */
/* *************** */
class {THEMECLASS}Index extends ProtendoIndex
{
	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '<a id="top"></a>
		<div id="topheader">
			<div class="mainwidth">
				<div class="floatleft">
					<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" /></a></h1>
				</div>
				<div id="user_section" class="floatright">';

		$this->theme_menu();
		echo '
				</div>
				<hr style="clear: both;">';

		if(!empty($context['insecure']))
			echo '
				<div class="info2" style="margin: 5px 0;">'. $context['insecure'] . '</div>';

		// Is the forum in maintenance mode?
		if ($context['in_maintenance'] && $context['user']['is_admin'])
			echo '
				<div class="info2" style="margin: 5px 0;">'. $txt['maintain_mode_on'] . '</div>';
			
		echo '
				<div class="floatleft">' , $this->theme_linktree() , '</div>
				<div class="floatright" style="position:relative;">
					<div class="floatright quickbutton_single">
						<span id="sidebar_toggle_container">
							<span id="sidebar_toggle" class="triangle_' , !empty($options['usersidebar']) ? 'right' : 'left' , '"></span>
							<span style="white-space: nowrap; position: absolute; "></span>
						</span>';

		if(!empty($context['user']['avatar']['image']))
			echo '
						<span class="floatright mini_avatar" style="background-image: url(' , $context['user']['avatar']['href'] , ');"></span>';
		if ($context['user']['is_logged'])
		{
			echo '	
						<a id="pms" href="' , $scripturl , '?action=pm">', $txt['pm_short'], '<span class="' , $context['user']['unread_messages']>0 ? 'notice' : 'noticegrey' , '">' . $context['user']['unread_messages'] . '</span>' , '</a>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
						<a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">' , $txt['approve_members_waiting'] , $context['unapproved_members']>0 ? '<span class="notice">'.$context['unapproved_members'].'</span>' : $context['unapproved_members'] , '</a>';

			echo '
						<a href="', $scripturl, '?action=unread">', $txt['unread'] , '<span class="', $context['unread_topics_user']>0 ? 'notice' : 'noticegrey' , '">'. $context['unread_topics_user'] . '</span></a>';
			
			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
						<a href="', $scripturl, '?action=moderate;area=reports">', $txt['mc_reported_posts'] , '<span class="notice">' , $context['open_mod_reports'], '</span></a>';
		}
		echo '	</div>
				</div>
			</div>
			<br class="clear" />
		</div>
		
		
		
		
		<div id="fixedtopheader">
			<div class="mainwidth">
				<div class="floatleft">
					<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" style="height: 22px;" /></a></h1>
				</div>
				<div id="user_section" class="floatright">';

		$this->theme_menu();
		echo '
				</div>
			</div>
			<br class="clear" />
		</div>

		
		
		<div id="main_content">
			<div class="mainwidth" style="padding: 0;"><div class="' , !empty($options['usersidebar']) ? 'showme' : 'hideme' , '" id="sbar">
				<div>' , $this->theme_user_below() , function_exists('theme_insertbg') ? '<div style="height: 1em; overflow: hidden;"></div>' : '' , '</div>
			</div></div>';

		if(!empty($context['current_plugin']))
			theme_insertbg();

		echo '
			<div class="mainwidth">';
	}

	function theme_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '	
			</div>
		</div>
		<div id="bottomfooter">
			<div class="mainwidth smalltext" style="padding: 1em;">
				', theme_copyright();

		// Show the load time?
		if ($context['show_load_time'])
			echo '
					| ', $txt['page_created'], $context['load_time'], $txt['seconds_with'], $context['load_queries'], $txt['queries'];

		echo '
			</div>
		</div>';
	}

}

?>