<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function template_main()
{
	global $context, $settings, $options, $txt, $scripturl;

	echo '
	<div id="recent" class="main_section">
		<div class="cat_bar">
			<h3 class="catbg">
				<span><img src="', $settings['images_url'], '/post/xx.gif" alt="" class="icon" />',$txt['recent_posts'],'</span>
			</h3>
		</div>
		<div class="pagesection">
			<span>', $txt['pages'], ': ', $context['page_index'], '</span>
		</div>';

	foreach ($context['posts'] as $post)
	{
		echo '
			<div class="', $post['alternate'] == 0 ? 'windowbg' : 'windowbg2', ' core_posts">
				
				<div class="content">
					<div class="counter">', $post['counter'], '</div>
					<div class="topic_details">
						<h5>', $post['board']['link'], ' / ', $post['link'], '</h5>
						<span class="smalltext">&#171;&nbsp;', $txt['last_post'], ' ', $txt['by'], ' <strong>', $post['poster']['link'], ' </strong> ', $txt['on'], '<em> ', $post['time'], '</em>&nbsp;&#187;</span>
					</div>
					<div class="list_posts">', $post['message'], '</div>
				</div>';

		if ($post['can_reply'] || $post['can_mark_notify'] || $post['can_delete'])
			echo '
				<div class="quickbuttons_wrap">
					<ul class="reset smalltext quickbuttons">';

		// If they *can* reply?
		if ($post['can_reply'])
			echo '
						<li class="reply_button"><a href="', $scripturl, '?action=post;topic=', $post['topic'], '.', $post['start'], '"><span>', $txt['reply'], '</span></a></li>';

		// If they *can* quote?
		if ($post['can_quote'])
			echo '
						<li class="quote_button"><a href="', $scripturl, '?action=post;topic=', $post['topic'], '.', $post['start'], ';quote=', $post['id'], '"><span>', $txt['quote'], '</span></a></li>';

		// Can we request notification of topics?
		if ($post['can_mark_notify'])
			echo '
						<li class="notify_button"><a href="', $scripturl, '?action=notify;topic=', $post['topic'], '.', $post['start'], '"><span>', $txt['notify'], '</span></a></li>';

		// How about... even... remove it entirely?!
		if ($post['can_delete'])
			echo '
						<li class="remove_button"><a href="', $scripturl, '?action=deletemsg;msg=', $post['id'], ';topic=', $post['topic'], ';recent;', $context['session_var'], '=', $context['session_id'], '" onclick="return confirm(\'', $txt['remove_message'], '?\');"><span>', $txt['remove'], '</span></a></li>';

		if ($post['can_reply'] || $post['can_mark_notify'] || $post['can_delete'])
			echo '
					</ul>
				</div>';

		echo '
			</div>';

	}

	echo '
		<div class="pagelinks">
			<span>', $txt['pages'], ': ', $context['page_index'], '</span>
		</div>
	</div>';
}

function template_unread()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings;

	if ($settings['show_mark_read'])
	{
		// Generate the button strip.
		$mark_read = array(
			'markread' => array('text' => !empty($context['no_board_limits']) ? 'mark_as_read' : 'mark_read_short', 'image' => 'markread.gif', 'lang' => true, 'url' => $scripturl . '?action=markasread;sa=' . (!empty($context['no_board_limits']) ? 'all' : 'board' . $context['querystring_board_limits']) . ';' . $context['session_var'] . '=' . $context['session_id']),
		);

	}
	if (!empty($context['topics']) && !$context['showing_all_topics'])
		$mark_read['readall'] = array('text' => 'unread_topics_all', 'image' => 'markreadall.gif', 'lang' => true, 'url' => $scripturl . '?action=unread;all' . $context['querystring_board_limits'], 'active' => true);

	if(sizeof($context['topics']) == 0)
		echo '
	<h2><em>', $context['showing_all_topics'] ? $txt['msg_alert_none'] : $txt['unread_topics_visit_none'], '</em></h2>';
	else
	{
		if (!empty($mark_read))
			echo '<div class="floatright">' ,template_button_strip($mark_read), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['unread_forum'] , '</h2>
		<div style="clear: both;" class="pagelinks">
			', $context['page_index'], '
		</div>
		<div class="bwgrid">
		<div id="messageindex">
			<table class="table_grid" style="width: 100%;">
				<tbody>';
			$alt=true;

		foreach($context['topics'] as $topic)
		{

			if(!empty($topic['board']['id_plugin']))
				continue;

				// We start with locked and sticky topics.
			if ($topic['is_sticky'] && $topic['is_locked'])
				$color_class = 'locked_sticky windowbg';
			// Sticky topics should get a different color, too.
			elseif ($topic['is_sticky'])
				$color_class = 'stickybg windowbg';
			// Locked topics get special treatment as well.
			elseif ($topic['is_locked'])
				$color_class = 'lockedbg windowbg';
			// Last, but not least: regular topics.
			else
				$color_class = 'windowbg';

			// Some columns require a different shade of the color class.
			if($alt)
				$color_class .= '2';
			
			$alternate_class = $color_class;

			echo '
				<tr class="', $color_class, '">
					<td>
						<span class="messicons mess_' , $topic['first_post']['icon'] , '"></span>
					</td>
					<td style="padding: 0 1em;">
						', $topic['is_sticky'] ? '<strong>' : '', '<span id="msg_' . $topic['first_post']['id'] . '"><a href="', $topic['first_post']['href'], '"  title="' , $topic['first_post']['preview'],  '" class="Tip">' ,$topic['first_post']['subject'], '</a></span>
						<a href="' . $topic['new_href'] . '"><span class="new">' . $txt['new'] . '</span></a>', $topic['is_sticky'] ? '</strong>' : '','
							<br><span class="smalltext greytext">', $txt['started_by'], ' ', $topic['first_post']['member']['link'], '</span>
							<small id="pages' . $topic['first_post']['id'] . '">', $topic['pages'], '</small>
							
						</div>
					</td>
					<td>
						', $topic['replies'], ' | ', $topic['views'], '
					</td>
					<td>
						<a href="', $topic['last_post']['href'], '"><img style="float: left; margin: 8px 8px 5px 0;" src="', $settings['images_url'], '/icons/last_post.png" alt="', $txt['last_post'], '" /></a>
						', $topic['last_post']['time'], '<br />
						', $txt['by'], ' ', $topic['last_post']['member']['link'], '
					</td>
				</tr>';
			$alt = !$alt;
		}
		echo '
			</tbody>
		</table></div><br>
		<h2 class="mainheader">' , $txt['unread_nonforum'] , '</h2>
		<div class="bwgrid">';

		foreach($context['topics'] as $orig)
		{
			if(empty($orig['board']['id_plugin']))
				continue;

			template_unread_nonforum($orig);
		}
		echo '
		</div>
		<div style="clear: both;" class="pagelinks">', $context['page_index'], '	</div>';
	}
}

function template_replies()
{
	global $context, $settings, $options, $txt, $scripturl, $modSettings;

	if ($settings['show_mark_read'])
	{
		// Generate the button strip.
		$mark_read = array(
			'markread' => array('text' => !empty($context['no_board_limits']) ? 'mark_as_read' : 'mark_read_short', 'image' => 'markread.gif', 'lang' => true, 'url' => $scripturl . '?action=markasread;sa=' . (!empty($context['no_board_limits']) ? 'all' : 'board' . $context['querystring_board_limits']) . ';' . $context['session_var'] . '=' . $context['session_id']),
		);

	}
	if (!empty($context['topics']) && !$context['showing_all_topics'])
		$mark_read['readall'] = array('text' => 'unread_topics_all', 'image' => 'markreadall.gif', 'lang' => true, 'url' => $scripturl . '?action=unread;all' . $context['querystring_board_limits'], 'active' => true);

	if(sizeof($context['topics']) == 0)
		echo '
	<h2><em>', $context['showing_all_topics'] ? $txt['msg_alert_none'] : $txt['unread_topics_visit_none'], '</em></h2>';
	else
	{
		if (!empty($mark_read))
			echo '<div class="floatright">' ,template_button_strip($mark_read), '</div>';

		echo '
		<h2 class="mainheader">' , $txt['unread_forum'] , '</h2>
		<div style="clear: both;" class="pagelinks">', $context['page_index'], '	</div>
		<div class="bwgrid">
		<table class="table_grid" style="width: 100%;">
				<thead>
					<tr>
						<td scope="col" class="headertexts" style="width:2%">&nbsp;</td>
						<td scope="col" class="headertexts" style="text-align: left;"><a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=subject', $context['sort_by'] == 'subject' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['subject'], $context['sort_by'] == 'subject' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a> / <a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=starter', $context['sort_by'] == 'starter' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['started_by'], $context['sort_by'] == 'starter' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a></td>
						<td scope="col" class="headertexts" style="text-align: left;width:14%;"><a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=replies', $context['sort_by'] == 'replies' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['replies'], $context['sort_by'] == 'replies' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a> / <a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=views', $context['sort_by'] == 'views' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['views'], $context['sort_by'] == 'views' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a></td>
						<td scope="col" class="headertexts" style="width:30%;"><a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=last_post', $context['sort_by'] == 'last_post' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['last_post'], $context['sort_by'] == 'last_post' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a></td>
					</tr>
				</thead>
				<tbody>';
			$alt=true;

		foreach($context['topics'] as $topic)
		{

			if(!empty($topic['board']['id_plugin']))
				continue;

				// We start with locked and sticky topics.
			if ($topic['is_sticky'] && $topic['is_locked'])
				$color_class = 'locked_sticky windowbg';
			// Sticky topics should get a different color, too.
			elseif ($topic['is_sticky'])
				$color_class = 'stickybg windowbg';
			// Locked topics get special treatment as well.
			elseif ($topic['is_locked'])
				$color_class = 'lockedbg windowbg';
			// Last, but not least: regular topics.
			else
				$color_class = 'windowbg';

			// Some columns require a different shade of the color class.
			if($alt)
				$color_class .= '2';
			
			$alternate_class = $color_class;

			echo '
				<tr class="' , $color_class, '">
					<td>
						<span class="messicons mess_' , $topic['first_post']['icon'] , '"></span>
					</td>
					<td style="padding: 0 1em;">
						', $topic['is_sticky'] ? '<strong>' : '', '<span id="msg_' . $topic['first_post']['id'] . '"><a href="', $topic['first_post']['href'], '"  title="' , $topic['first_post']['preview'],  '">' ,$topic['first_post']['subject'], '</a></span>
						<a href="' . $topic['new_href'] . '"><span class="new">' . $txt['new'] . '</span></a>', $topic['is_sticky'] ? '</strong>' : '','
							<span class="smalltext greytext">', $txt['started_by'], ' ', $topic['first_post']['member']['link'], '</span>
							<small id="pages' . $topic['first_post']['id'] . '">', $topic['pages'], '</small>
							
					</td>
					<td>
						', $topic['replies'], ' | 	', $topic['views'], ' 
					</td>
					<td>
						<a href="', $topic['last_post']['href'], '"><img style="float: left; margin: 8px 8px 5px 0;" src="', $settings['images_url'], '/icons/last_post.png" alt="', $txt['last_post'], '" /></a>
						', $topic['last_post']['time'], '<br />
						', $txt['by'], ' ', $topic['last_post']['member']['link'], '
					</td>
				</tr>';
			$alt = !$alt;
		}
		echo '
			</tbody>
		</table><br>
		<h2 class="mainheader">' , $data['menu']['title'] , '</h2>
		<div class="bwgrid">';

		foreach($context['topics'] as $orig)
		{
			if(empty($orig['board']['id_plugin']))
				continue;

			template_unread_nonforum($orig);
		}
		echo '
		</div>
		<div style="clear: both;" class="pagelinks">', $context['page_index'], '	</div>';
	}
}

function template_unread_nonforum($orig)
{
	global $scripturl,$context, $settings, $txt;

	echo '
		<div class="bwcell4"><div class="inner_left">
			<a href="' . $orig['new_href'].'" style="margin-bottom: 0.5em; display: block; overflow: hidden; height: 160px; background: url(' , !empty($context['blogimages'][$orig['first_post']['id']]['thumb']) ? $context['blogimages'][$orig['first_post']['id']]['thumb'] : $settings['images_url'].'/noimage.png' , ') no-repeat white; background-size: contain;">&nbsp;</a>
			<strong><a href="' . $orig['new_href'].'">' , substr($orig['subject'],0,20) , strlen($orig['subject'])>20 ? '...' : '', '</a></strong>
			<div style="height: 70px;"">' , substr(strip_tags($orig['last_post']['preview']),0,60) , '... </div><hr>
			<div class="breadcrumb_style">
				' . $orig['replies'] . ' ' . $txt['replies'] . ' | ' . $orig['views'] . ' ' . $txt['views'] . ' |
				<a href="' . $orig['new_href'].'"><b>' , $txt['read'] , ' ', $txt['more'] , '</b></a>
			</div><br><br>
		</div></div>';
}

function template_unread_gallery($orig)
{
	global $scripturl,$context, $settings, $txt;

		echo '
	<div class="bwcell2">
		<a href="' . $scripturl . '?topic='.$orig['id'].'.0" style="margin-bottom: 0.5em; display: block; overflow: hidden; height: 200px; background: url(' , !empty($context['frontthumbs'][$orig['first_post']['id']]) ? $context['frontthumbs'][$orig['first_post']['id']] : $settings['images_url'].'/noimage.png' , ') no-repeat white; background-size: cover;">&nbsp;</a>
	</div>';
}

?>