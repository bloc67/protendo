<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function template_main()
{
	global $modSettings, $context;

	createThemeObject('TPArticle');
	$context['subthemeobject']->theme_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoTPArticle
{
	function theme_main()
	{
		global $context, $settings, $options, $scripturl, $txt;

		if(!empty($context['blogboards']) && !empty($context['normal_buttons']))
			echo '
		<div class="floatright">' , template_button_strip($context['normal_buttons'],'',true), '</div>';

		echo '
		  <h2 class="mainheader">' , $txt['articles'] , '</h2>';
		if(!isset($context['frontblog']))
		{
			echo '
		<div class="headerarea2"><strong>', $txt['msg_alert_none'], '</strong></div>';
			return;
		}
		echo '
		<div class="pagelinks">
			', $context['page_index'] ,  '
		</div><br>';

		if(count($context['frontblog'])<1)
		{
			echo '
		<hr><strong>', $txt['msg_alert_none'], '</strong>';
		}
		else
		{
			// the top dog first
			foreach($context['frontblog'] as $id => $w)
			{
				$orig = $w;
				break;		
			}
			echo '
		<div class="bwgrid">
			<div class="doublecell_first">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0"><img src="' , !empty($context['blogimages'][$orig['id_msg']]['big']) ? $context['blogimages'][$orig['id_msg']]['big'] : $settings['images_url'].'/no_image.png' , '" class="imgstyle1" /></a><span class="shadow2"></span>
				<h3 class="blogheader" style="margin: 0 0 2px 0; ">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
				</h3>
				<span class="breadcrumb_style">' . $txt['posted_by'] . ' ' .  $orig['poster']['link'] . ' |
				' . $orig['time'] . ' ' . $txt['in'] . ' <b>' .  $orig['board']['link'] . '</b></span>
				<div class="post">' , $orig['body'], '...</div>
				' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
			</div>
			<div class="doublecell_second">
				<h3 class="blogheader">' , $txt['most_read'] , '</h3>
				<ul class="links">';
			
			foreach($context['blogmostread'] as $b => $bdata)
				echo '
					<li><span class="breadcrumb_style greytext floatright" style="margin-top: 5px;">' , $bdata['num_views'] , ' ' , $txt['views'] , '</span><a href="' , $scripturl,'?topic=' , $bdata['id_topic'] , '">' , $bdata['subject'] , '</a></li>';

			echo '
				</ul>
			</div>
		</div>
		<hr><br>
		<div class="bwgrid">';

			$first =true; $count = 0; $classgrid=array('_first','','_third');
			foreach($context['frontblog'] as $orig)
			{
				if($first)
				{
					$first = false;
					continue;
				}

				echo '
			<div class="triplecell' , $classgrid[$count] , '">
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0" style="display: block; overflow: hidden; height: 200px; background: url(' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/no_image.png' , ') no-repeat white; background-size: cover;">&nbsp;</a>
				<h3 class="blogheader" style="margin: 8px 0 2px 0; ">
					<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">' , $orig['subject'] , '</a>
					' , $orig['new'] && empty($orig['new_from']) ? '<span class="notifier">&nbsp;</span>' : '' , '
				</h3>
				<span class="breadcrumb_style">' . $txt['posted_by'] . ' ' .  $orig['poster']['link'] . ' |
				' . $orig['time'] . ' ' . $txt['in'] . ' <b>' .  $orig['board']['link'] . '</b></span>
				<div class="post "' , $count<2 ? '' : ' style="min-height: 5em;"' , '>' , substr($orig['body'],0,($count<3 ? '300' : '90')) , '...</div>
				' , $orig['new'] && !empty($orig['replies']) ? '<span class="new"><a href="' . $orig['new_href'] . '">' . $orig['replies'] . ' '  . $txt['new'] . ' ' . $txt['comments'] . '</a></span>' : '' , '
				<br>
			</div>';
				$count++;
				if($count == 3)
					$count=0;
			}
			echo '
		</div>';
		}
		echo '
		<h2 class="mainheader">' , $txt['categories'] , '</h2>
		<div class="headerarea2">' , $this->article_cats() , '</div>
		<div class="pagelinks">', $context['page_index'], '	</div>';
	}

	function article_cats()
	{
		global $context, $settings, $options, $scripturl, $txt;
		
		$items = array();
		foreach($context['blogboards'] as $art)
			$items[] = '<a href="' . $scripturl . '?board=' . $art['id_board'] . '">' . $art['name'] . '</a>';
		
		echo implode(" | ", $items);
	}
}

?>