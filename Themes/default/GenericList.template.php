<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
function template_show_list($list_id = null)
{
	global $context, $settings, $options, $scripturl, $txt, $modSettings;

	// Get a shortcut to the current list.
	$list_id = $list_id === null ? $context['default_list'] : $list_id;
	$cur_list = &$context[$list_id];

	// These are the main tabs that is used all around the template.
	if (!empty($settings['use_tabs']) && isset($cur_list['list_menu'], $cur_list['list_menu']['show_on']) && ($cur_list['list_menu']['show_on'] == 'both' || $cur_list['list_menu']['show_on'] == 'top'))
		template_create_list_menu($cur_list['list_menu'], 'top');

	if (isset($cur_list['form']))
		echo '
	<form action="', $cur_list['form']['href'], '" method="post"', empty($cur_list['form']['name']) ? '' : ' name="' . $cur_list['form']['name'] . '" id="' . $cur_list['form']['name'] . '"', ' accept-charset="', $context['character_set'], '">';

	// Show the title of the table (if any).
	if (!empty($cur_list['title']))
		echo '
		<h3 class="mainheader">', $cur_list['title'], '</h3>';

	if (isset($cur_list['additional_rows']['top_of_list']))
		template_additional_rows('top_of_list', $cur_list);

	if (isset($cur_list['additional_rows']['after_title']))
	{
		template_additional_rows('after_title', $cur_list);
	}

	if (!empty($cur_list['items_per_page']) || isset($cur_list['additional_rows']['bottom_of_list']))
	{
		// Show the page index (if this list doesn't intend to show all items).
		if (!empty($cur_list['items_per_page']))
			echo '
		<div class="pagelinks">', $cur_list['page_index'], '</div>';

		if (isset($cur_list['additional_rows']['above_column_headers']))
			echo '
		<div class="info2">', template_additional_rows('above_column_headers', $cur_list), '</div>';
	}

	echo '
		<div class="bwtable">';

	// Show the column headers.
	$header_count = count($cur_list['headers']);
	if (!($header_count < 2 && empty($cur_list['headers'][0]['label'])))
	{
		echo '
		<div class="bwrow">';

		// Loop through each column and add a table header.
		$i = 0;
		foreach ($cur_list['headers'] as $col_header)
		{
			$i ++;
			if (empty($col_header['class']) && $i == 1)
				$col_header['class'] = 'first_th';
			elseif (empty($col_header['class']) && $i == $header_count)
				$col_header['class'] = 'last_th';

			echo '
				<div class="bwcol" style="', !empty($col_header['style']) ? $col_header['style'] : '' , ';"><div class="headertexts">', empty($col_header['href']) ? '' : '<a href="' . $col_header['href'] . '" rel="nofollow">', empty($col_header['label']) ? '&nbsp;' : $col_header['label'], empty($col_header['href']) ? '' : '</a>', empty($col_header['sort_image']) ? '' : ' <span class="arrow_' . $col_header['sort_image'] . '"></span>', '</div></div>';
		}

		echo '
		</div>';
	}
	echo '
	</div>';

	// Show a nice message informing there are no items in this list.
	if (empty($cur_list['rows']) && !empty($cur_list['no_items_label']))
		echo '
		<div class="info2">', $cur_list['no_items_label'], '</div>';
	// Show the list rows.
	elseif (!empty($cur_list['rows']))
	{
		echo '
		<div class="bwtable">';
		$alternate = false;
		foreach ($cur_list['rows'] as $id => $row)
		{
			echo '
			<div class="bwrow">';
			foreach ($row as $row_data)
				echo '
				<div class="bwcol windowbg', $alternate ? '2' : '', '"', empty($row_data['style']) ? '' : ' style="' . $row_data['style'] . ' margin: 1px 0;"', '>', $row_data['value'], '</div>';

			echo '
			</div>';
			$alternate = !$alternate;
		}
	}

	echo '
		</div>';

	if (!empty($cur_list['items_per_page']) || isset($cur_list['additional_rows']['below_table_data']) || isset($cur_list['additional_rows']['bottom_of_list']))
	{
		// Show the page index (if this list doesn't intend to show all items).
		if (!empty($cur_list['items_per_page']))
			echo '
	<div class="pagelinks">', $cur_list['page_index'], '</div>';

		if (isset($cur_list['additional_rows']['below_table_data']))
			template_additional_rows('below_table_data', $cur_list);

		if (isset($cur_list['additional_rows']['bottom_of_list']))
			template_additional_rows('bottom_of_list', $cur_list);
	}

	if (isset($cur_list['form']))
	{
		foreach ($cur_list['form']['hidden_fields'] as $name => $value)
			echo '
			<input type="hidden" name="', $name, '" value="', $value, '" />';

		echo '
	</form>';
	}

	// Tabs at the bottom.  Usually bottom alligned.
	if (!empty($settings['use_tabs']) && isset($cur_list['list_menu'], $cur_list['list_menu']['show_on']) && ($cur_list['list_menu']['show_on'] == 'both' || $cur_list['list_menu']['show_on'] == 'bottom'))
		echo '
	<div class="quickbuttons">', template_create_list_menu($cur_list['list_menu'], 'bottom'), '</div>';

	if (isset($cur_list['javascript']))
		echo '
	<script type="text/javascript"><!-- // --><![CDATA[
		', $cur_list['javascript'], '
	// ]]></script>';
}

function template_additional_rows($row_position, $cur_list)
{
	global $context, $settings, $options;

	foreach ($cur_list['additional_rows'][$row_position] as $row)
		echo '
			<div class="additional_row', empty($row['class']) ? '' : ' ' . $row['class'], '"', empty($row['style']) ? '' : ' style="' . $row['style'] . '"', '>', $row['value'], '</div>';
}

function template_create_list_menu($list_menu, $direction = 'top')
{
	global $context, $settings;

	// Are we using right-to-left orientation?
	$first = $context['right_to_left'] ? 'last' : 'first';
	$last = $context['right_to_left'] ? 'first' : 'last';

	echo '
	<ul class="quickbuttons">';

	foreach ($list_menu['links'] as $link)
		echo '
		<li><a class="' , $link['is_selected'] ? ' chosen' : '' , '" href="', $link['href'], '">', $link['label'], '</a></li>';			

	echo '
	</ul>';
}

?>