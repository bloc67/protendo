<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
/* *************** */
/* the class definitions */
/* *************** */
class Protendoindex
{
	function theme_frame_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings, $boardurl;

		// Show right to left and the character set for ease of translating.
		echo '
	<!DOCTYPE html>
	<html', $context['right_to_left'] ? ' dir="rtl"' : '', '>
	<head>
		<link rel="stylesheet" type="text/css" href="', $settings['default_theme_url'], '/css/framework.css?pro1" />
		<link rel="stylesheet" type="text/css" href="', $settings['theme_url'], '/css/index.css?pro1" />
		' , $context['right_to_left'] ? '<link rel="stylesheet" type="text/css" href="'. $settings['theme_url']. '/css/rtl.css" />' : '' ,'
		<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/script.js"></script>

		<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
		<script type="text/javascript">
			window.cookieconsent_options = {"message":"' . $txt['eucookie_text'] . '","' . $txt['eucookie_dismiss'] . '":"' . $txt['eucookie_ok'] . '","' . $txt['eucookie_learnmore'] . '":"' . $txt['eucookie_moreinfo'] . '","link":null,"theme": false};
		</script>

		<script type="text/javascript" src="' , $settings['default_theme_url'] , '/javascript/cookieconsent.min.js"></script>
		<!-- End Cookie Consent plugin -->

		<script type="text/javascript"><!-- // --><![CDATA[
			var smf_theme_url = "', $settings['theme_url'], '";
			var smf_default_theme_url = "', $settings['default_theme_url'], '";
			var smf_images_url = "', $settings['images_url'], '";
			var smf_scripturl = "', $scripturl, '";
			var smf_iso_case_folding = ', $context['server']['iso_case_folding'] ? 'true' : 'false', ';
			var smf_charset = "', $context['character_set'], '";
			var ajax_notification_text = "', $txt['ajax_in_progress'], '";
			var ajax_notification_cancel_text = "', $txt['modify_cancel'], '";
		// ]]></script>

		<link rel="shortcut icon" type="image/x-icon" href="', $boardurl , '/favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=', $context['character_set'], '" />
		<meta name="description" content="', $context['page_title_html_safe'], '" />
		', !empty($context['meta_keywords']) ? '	<meta name="keywords" content="' . $context['meta_keywords'] . '" />' : '', '
		<title>', $context['page_title_html_safe'], '</title>
		', !empty($context['robot_no_index']) ? '<meta name="robots" content="noindex" />' : '' , '
		', !empty($context['canonical_url']) ? '<link rel="canonical" href="'. $context['canonical_url']. '" />' : '';

		// If RSS feeds are enabled, advertise the presence of one.
		if (!empty($modSettings['xmlnews_enable']) && (!empty($modSettings['allow_guestAccess']) || $context['user']['is_logged']))
			echo '
		<link rel="alternate" type="application/rss+xml" title="', $context['forum_name_html_safe'], ' - ', $txt['rss'], '" href="', $scripturl, '?type=rss;action=.xml" />
		<link rel="alternate" type="application/rss+xml" title="', $context['forum_name_html_safe'], ' - ', $txt['rss'], ' - News" href="', $scripturl, '?type=rss;action=.xml;sa=news" />';

		// Output any remaining HTML headers. (from mods, maybe?)
		echo $context['html_headers'];
		mootools_addon();
		theme_set();
		
		if(isset($settings['fixedheader']))
			echo $settings['fixedheader'];
		else
			echo '
		<script type="text/javascript">
			/* fixed header */
			window.addEvent(\'domready\',function() {
				new SmoothScroll({duration:50});
				$(\'fixedtopheader\').set(\'opacity\',\'0\').setStyle(\'display\',\'block\');
				var ss = new ScrollSpy({
					min: 50,
					onEnter: function(position,enters) {
						$(\'fixedtopheader\').fade(\'in\');
					},
					onLeave: function(position,leaves) {
						$(\'fixedtopheader\').fade(\'out\');
					},
					container: window
				});
			});		
		</script>';	

		echo '
		<script type="text/javascript">
			/* goto button */
			window.addEvent(\'domready\',function() {
				new SmoothScroll({duration:200});
				$(\'gototop\').set(\'opacity\',\'0\').setStyle(\'display\',\'block\');
				var ss = new ScrollSpy({
					min: 400,
					onEnter: function(position,enters) {
						$(\'gototop\').fade(\'in\');
					},
					onLeave: function(position,leaves) {
						$(\'gototop\').fade(\'out\');
					},
					container: window
				});
			});		
		</script>
	</head>
	<body>';
	}

	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '<a id="top"></a>
		<div id="topheader">
			<div class="mainwidth">
				<div class="floatleft">
					<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" /></a></h1>
				</div>
				<div id="user_section" class="floatright">';

		$this->theme_menu();
		echo '
				</div>
				<hr style="clear: both;">';

		if(!empty($context['insecure']))
			echo '
				<div class="info2" style="margin: 5px 0;">'. $context['insecure'] . '</div>';

		// Is the forum in maintenance mode?
		if ($context['in_maintenance'] && $context['user']['is_admin'])
			echo '
				<div class="info2" style="margin: 5px 0;">'. $txt['maintain_mode_on'] . '</div>';
			
		echo '
				<div class="floatleft">' , $this->theme_linktree() , '</div>
				<div class="floatright" style="position:relative;">
					<div class="floatright quickbutton_single">
						<span id="sidebar_toggle_container">
							<span id="sidebar_toggle" class="triangle_' , !empty($options['usersidebar']) ? 'right' : 'left' , '"></span>
							<span style="white-space: nowrap; position: absolute; "></span>
						</span>';

		if(!empty($context['user']['avatar']['image']))
			echo '
						<span class="floatright mini_avatar" style="background-image: url(' , $context['user']['avatar']['href'] , ');"></span>';
		if ($context['user']['is_logged'])
		{
			echo '	
						<a id="pms" href="' , $scripturl , '?action=pm">', $txt['pm_short'], '<span class="' , $context['user']['unread_messages']>0 ? 'notice' : 'noticegrey' , '">' . $context['user']['unread_messages'] . '</span>' , '</a>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
						<a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">' , $txt['approve_members_waiting'] , $context['unapproved_members']>0 ? '<span class="notice">'.$context['unapproved_members'].'</span>' : $context['unapproved_members'] , '</a>';

			echo '
						<a href="', $scripturl, '?action=unread">', $txt['unread'] , '<span class="', $context['unread_topics_user']>0 ? 'notice' : 'noticegrey' , '">'. $context['unread_topics_user'] . '</span></a>';
			
			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
						<a href="', $scripturl, '?action=moderate;area=reports">', $txt['mc_reported_posts'] , '<span class="notice">' , $context['open_mod_reports'], '</span></a>';
		}
		echo '	</div>
				</div>
			</div>
			<br class="clear" />
		</div>
		
		
		
		
		<div id="fixedtopheader">
			<div class="mainwidth">
				<div class="floatleft">
					<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" style="height: 22px;" /></a></h1>
				</div>
				<div id="user_section" class="floatright">';

		$this->theme_menu();
		echo '
				</div>
			</div>
			<br class="clear" />
		</div>

		
		
		<div id="main_content">
			<div class="mainwidth" style="padding: 0;"><div class="' , !empty($options['usersidebar']) ? 'showme' : 'hideme' , '" id="sbar">
				<div>' , $this->theme_user_below() , function_exists('theme_insertbg') ? '<div style="height: 1em; overflow: hidden;"></div>' : '' , '</div>
			</div></div>';

		if(!empty($context['current_plugin']))
			theme_insertbg();

		echo '
			<div class="mainwidth">';
	}

	function theme_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '	
			</div>
		</div>
		<div id="bottomfooter">
			<div class="mainwidth smalltext" style="padding: 1em;">
				', theme_copyright();

		// Show the load time?
		if ($context['show_load_time'])
			echo '
					| ', $txt['page_created'], $context['load_time'], $txt['seconds_with'], $context['load_queries'], $txt['queries'];

		echo '
			</div>
		</div>';
	}

	function theme_frame_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		if(!empty($settings['trackingcode']))
			echo $settings['trackingcode'];

		echo '

	<a href="#top" id="gototop" class="button_submit">' , $txt['go_up'] , '</a>
</body></html>';
	}

	// Show a linktree. This is that thing that shows "My Community | General Category | General Discussion"..
	function theme_linktree($force_show = false)
	{
		global $context, $settings, $options, $shown_linktree, $scripturl, $txt;

		// If linktree is empty, just return - also allow an override.
		if (empty($context['linktree']) || (!empty($context['dont_default_linktree']) && !$force_show))
			return;

		echo '
		<div id="linktree" class="breadcrumb">
			<ul class="horiz_nav">';

		// Each tree item has a URL and name. Some may have extra_before and extra_after.
		foreach ($context['linktree'] as $link_num => $tree)
		{
			echo '
				<li', ($link_num == count($context['linktree']) - 1) ? ' class="last"' : '', '>';

			// Show something before the link?
			if (isset($tree['extra_before']))
				echo $tree['extra_before'];

			if ($link_num != 0)
				echo '
					<span class="dividers">/</span>';

			// Show the link, including a URL if it should have one.
			echo $settings['linktree_link'] && isset($tree['url']) ? '
					<a href="' . $tree['url'] . '"><span>' . $tree['name'] . '</span></a>' : '<span>' . $tree['name'] . '</span>';

			// Show something after the link...?
			if (isset($tree['extra_after']))
				echo $tree['extra_after'];

			echo '
				</li>';
		}

		echo '
			</ul>
		</div>';

		$shown_linktree = true;
	}

	// Show the menu up top. Something like [home] [help] [profile] [logout]...
	function theme_menu()
	{
		global $context, $settings, $options, $scripturl, $txt;

		echo '
	<div id="topmenustyle">	
		<ul class="horiz_nav">';

		// Note: Menu markup has been cleaned up to remove unnecessary spans and classes.
		foreach ($context['menu_buttons'] as $act => $button)
		{
			if(!isset($button['top']))
				continue;

			echo '
			<li', !empty($button['sub_buttons']) ? ' class="subsections"' :'', '>
				<a class="', $button['active_button'] ? 'active ' : '', 'toplevel"', !empty($button['id']) ? ' id="' . $button['id'] . '"' : '',  !empty($button['href']) ? ' href="' . $button['href'] . '"' : '', ' ', isset($button['target']) ? 'target="' . $button['target'] . '"' : '', '>
					', $button['title'], '
				</a>';
			if (!empty($button['sub_buttons']))
			{
				// make number of items style abit better. 
				$total = count($button['sub_buttons']);
				if($total > 8)
					$cols_style = ' subcols' . (floor(count($button['sub_buttons'])/8));
				else
					$cols_style = '';
				
				if($total>0)
				{
					echo '
					<ul class="horiz_nav' . $cols_style . '">';

					foreach ($button['sub_buttons'] as $ch => $childbutton)
					{
						if(!empty($ch))
							echo '
						<li>
							<a href="', $childbutton['href'], '" ' , isset($childbutton['target']) ? 'target="' . $childbutton['target'] . '"' : '', '>
								', $childbutton['title'], '
							</a>
						</li>';
					}
						echo '
					</ul>';
				}
			}
			echo '
			</li>';
		}
		echo '
		</ul>
	</div>';
	}

	// Generate a strip of buttons.
	function theme_button_strip($button_strip, $style = '', $extra = false)
	{
		global $settings, $context, $txt, $scripturl;

		// List the buttons in reverse order for RTL languages.
		if ($context['right_to_left'])
			$button_strip = array_reverse($button_strip, true);

		// Create the buttons...
		$buttons = array();
		foreach ($button_strip as $key => $value)
		{
			if (!isset($value['test']) || !empty($context[$value['test']]))
				$buttons[] = '
					<li><a class="inside' . (isset($value['active']) ? ' chosen' : '') . '" href="' . $value['url'] . '"><span>' . $txt[$value['text']] . '</span></a></li>';
		}

		// No buttons? No button strip either.
		if (empty($buttons))
			return;

		echo '
		<div class="quickbuttons"' , !empty($style) ? ' style="' . $style . '"' : '' , '>
			<ul>', implode('', $buttons), '</ul>
		</div>';
	}

	// render the copyright bar, if specified
	function theme_copyrightbar()
	{
		global $settings;

		if(!empty($settings['custom_copyright']))
			echo $settings['custom_copyright'];
	}

	// render the extrainfo bar, if specified
	function theme_extrainfo()
	{
		global $settings;

		if(!empty($settings['extra_info']))
			echo $settings['extra_info'];

	}

	function theme_button_strip_line($buttons)
	{
		global $settings, $context, $txt, $scripturl;
		
		if(empty($buttons))
			return;
		
		$all = array();
		foreach($buttons as $b)
			$all[] = '<a href="'. $b['url'] .'">' . $b['label'] . '</a>';
		
		echo implode(" | ", $all);
	}

	function theme_user_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings, $boardurl;

		echo '
		<div id="sidebar">';
		if ($context['user']['is_logged'])
		{
			echo '
			<h3 class="mainheader">
				<a href="' , $scripturl , '?action=profile">', $context['user']['name'], '</a>
			</h3>
			<hr>
			<ul class="qbuttons">
				<li><a href="' ,$scripturl ,  '?action=pm">' , $txt['personal_messages'] , '</a></li>';
			
			if(!empty($context['user']['unread_messages']))
				echo '	
				<li class="note"><a class="active" href="' , $scripturl , '?action=pm" title="', $txt['pm_short'], ': ', $context['user']['unread_messages'], '">',$context['user']['unread_messages'],'</a></li>';
			
			// Is the forum in maintenance mode?
			if ($context['in_maintenance'] && $context['user']['is_admin'])
				echo '
				<li><a id="maintenace" href="' , $scripturl , '?action=admin"><strong>', $txt['maintenance']. '</strong></a></li>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
				<li><a id="approved" href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">', $txt['approvals'], '</a>	</li>
				<li class="note"><a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve" class="active">', $context['unapproved_members'], '</a></li>';
			
			echo '
				<li><a id="unread" href="', $scripturl, '?action=unread">', $txt['unread_topics_visit'], '</a></li>';

			if(!empty($context['unread_topics_user']))				
				echo '
				<li class="note"><a href="', $scripturl, '?action=unread" class="active">', $context['unread_topics_user'] , '</a></li>';
			
			echo '
				<li><a id="unread" href="', $scripturl, '?action=unreadreplies">', $txt['replies'], '</a></li>';

			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
				<li><a id="modreports" href="', $scripturl, '?action=moderate;area=reports"><strong>', $txt['reports'], '</strong></a></li>
				<li class="note"><a class="active" href="', $scripturl, '?action=moderate;area=reports">', $context['open_mod_reports'], '</a></li>';
			
			if (!empty($context['unapproved_posts']) && $context['show_open_reports'])
				echo '
				<li><a id="modreports" href="', $scripturl, '?action=moderate;area=reports">', $txt['unapproved_posts'], '</a></li>
				<li class="note"><a class="active" href="', $scripturl, '?action=moderate;area=reports">', $context['unapproved_posts'], '</a></li>';
			
		}
		
		foreach ($context['menu_buttons'] as $act => $button)
		{
			if(isset($button['top']))
				continue;

			if(!empty($button['title']))
				echo '
				<li><a class="', $button['active_button'] ? 'active' : '', '" href="', $button['href'], '" ', isset($button['target']) ? 'target="' . $button['target'] . '"' : '', '>', $button['title'], '</a></li>';
		}
		echo '
			</ul>
		</div>';
	}
	function theme_generic_menu_dropdown()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		// What one are we rendering?
		$context['cur_menu_id'] = isset($context['cur_menu_id']) ? $context['cur_menu_id'] + 1 : 1;
		$menu_context = &$context['menu_data_' . $context['cur_menu_id']];

		echo '
	<div id="adm_menu">';

		foreach ($menu_context['sections'] as $section_id => $section)
		{
			foreach ($section['areas'] as $i => $area)
			{
				$url = $i;
				break;
			}
			
			if($section_id == $menu_context['current_section'])
			{	
				echo '
		<ul class="current">
			<li><a id="' , $section_id , '" href="' . $menu_context['base_url'] . ';area=' . $url . '">', $section['title'],'</a></li>
		</ul>
		<ul class="sub_active">';

				foreach ($section['areas'] as $i => $area)
				{
					// Not supposed to be printed?
					if (empty($area['label']))
						continue;

					echo '
			<li>';

					// Is this the current area, or just some area?
					if ($i == $menu_context['current_area'])
					{
						echo '
				<a class="chosen" href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

						if (empty($context['tabs']))
							$context['tabs'] = isset($area['subsections']) ? $area['subsections'] : array();
					}
					else
						echo '
				<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

					echo '
			</li>';
				}
				echo '
		</ul>';
			}
			else
			{
				echo '
		<ul class="select">
			<li><a id="' , $section_id , '" href="' . $menu_context['base_url'] . ';area=' . $url . '">', $section['title'],'</a>
				<ul class="sub">';

				foreach ($section['areas'] as $i => $area)
				{
					// Not supposed to be printed?
					if (empty($area['label']))
						continue;

					echo '
					<li>';

					// Is this the current area, or just some area?
					if ($i == $menu_context['current_area'])
					{
						echo '
						<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

						if (empty($context['tabs']))
							$context['tabs'] = isset($area['subsections']) ? $area['subsections'] : array();
					}
					else
						echo '
						<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">', $area['label'], '</a>';

					echo '
					</li>';
				}
				echo '
				</ul>
			</li>
		</ul>';
			}
		}

		echo '
	</div>
	<div style="clear: both; overflow: hidden;" id="admarea">';

		// It's possible that some pages have their own tabs they wanna force...
		if (!empty($context['tabs']))
			echo '
		<div>' , $this->theme_generic_menu_tabs($menu_context) , '</div>';
	}

	function theme_generic_menu_dropdown_below()
	{
		echo '
	</div>';
	}

	function theme_generic_menu_tabs(&$menu_context)
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		// Handy shortcut.
		$tab_context = &$menu_context['tab_data'];

		// Exactly how many tabs do we have?
		foreach ($context['tabs'] as $id => $tab)
		{
			// Can this not be accessed?
			if (!empty($tab['disabled']))
			{
				$tab_context['tabs'][$id]['disabled'] = true;
				continue;
			}

			// Did this not even exist - or do we not have a label?
			if (!isset($tab_context['tabs'][$id]))
				$tab_context['tabs'][$id] = array('label' => $tab['label']);
			elseif (!isset($tab_context['tabs'][$id]['label']))
				$tab_context['tabs'][$id]['label'] = $tab['label'];

			// Has a custom URL defined in the main admin structure?
			if (isset($tab['url']) && !isset($tab_context['tabs'][$id]['url']))
				$tab_context['tabs'][$id]['url'] = $tab['url'];
			// Any additional paramaters for the url?
			if (isset($tab['add_params']) && !isset($tab_context['tabs'][$id]['add_params']))
				$tab_context['tabs'][$id]['add_params'] = $tab['add_params'];
			// Has it been deemed selected?
			if (!empty($tab['is_selected']))
				$tab_context['tabs'][$id]['is_selected'] = true;
			// Does it have its own help?
			if (!empty($tab['help']))
				$tab_context['tabs'][$id]['help'] = $tab['help'];
			// Is this the last one?
			if (!empty($tab['is_last']) && !isset($tab_context['override_last']))
				$tab_context['tabs'][$id]['is_last'] = true;
		}

		// Find the selected tab
		foreach ($tab_context['tabs'] as $sa => $tab)
		{
			if (!empty($tab['is_selected']) || (isset($menu_context['current_subsection']) && $menu_context['current_subsection'] == $sa))
			{
				$selected_tab = $tab;
				$tab_context['tabs'][$sa]['is_selected'] = true;
			}
		}

		echo '
		<ul class="quickbuttons" style="overflow: hidden; padding-left: 5px;">';
		
		// Print out all the items in this tab.
		foreach ($tab_context['tabs'] as $sa => $tab)
		{
			if (!empty($tab['disabled']) || empty($tab['label']))
				continue;

			if (!empty($tab['is_selected']))
				echo '
			<li><a class="chosen inside" href="', isset($tab['url']) ? $tab['url'] : $menu_context['base_url'] . ';area=' . $menu_context['current_area'] . ';sa=' . $sa, $menu_context['extra_parameters'], isset($tab['add_params']) ? $tab['add_params'] : '', '"><span class="firstlevel">', $tab['label'], '</span></a></li>';
			else
				echo '
			<li><a class="inside" href="', isset($tab['url']) ? $tab['url'] : $menu_context['base_url'] . ';area=' . $menu_context['current_area'] . ';sa=' . $sa, $menu_context['extra_parameters'], isset($tab['add_params']) ? $tab['add_params'] : '', '"><span class="firstlevel">', $tab['label'], '</span></a></li>';
		}

		echo '
		</ul><hr>';
	}
	function theme_extra_settings($boardtype)
	{
		global $context, $modSettings, $settings;
		// theme specific functions towards ALL boardtypes
		
		// get the images present..
		$opts = bloqs_listing($settings['theme_dir'].'\images\insertbg','jpg',true);
		$choices = array();
		foreach($opts as $o => $w)
		{
			if(strtolower(substr($w['filename'],strlen($w['filename'])-4,4))=='.jpg')
				$choices[$w['filename']] = $w['filename'];
		}
		
		// background pictures, disregard which boardtype
		$context['plugsettings']['values'][] = array(	
			'id' => $boardtype.'_' . $settings['theme_protendo_id'] . '_insertbg',
			'label' => 'Theme-specific background for "'.$boardtype.'"',
			'description' => 'Background used just above the content, stretching all width. There is an option to not use one.',
			'value' => isset($modSettings[$boardtype.'_' . $settings['theme_protendo_id'] . '_insertbg']) ? $modSettings[$boardtype.'_' . $settings['theme_protendo_id'] . '_insertbg'] : 'no_image.jpg',
			'imgpath' => $settings['images_url'].'/insertbg/',
			'type' => 'images',
			'options' => $choices,
		);
	}
}



function theme_insertbg()
{
	global $modSettings, $settings,$context;

	if(!empty($modSettings[$context['current_plugin'].'_'.$settings['theme_protendo_id'].'_insertbg']) && $modSettings[$context['current_plugin'].'_'.$settings['theme_protendo_id'].'_insertbg']!='no_image.jpg')
	{
		if(file_exists($settings['theme_dir'] . '/images/insertbg/'.$modSettings[$context['current_plugin'].'_'.$settings['theme_protendo_id'].'_insertbg']))
			echo '
	<div class="insertbg" style="clear: both; height: 290px; background-repeat: none; background-image: url(' . $settings['theme_url'] . '/images/insertbg/'.$modSettings[$context['current_plugin'].'_'.$settings['theme_protendo_id'].'_insertbg'].');"></div>';
	}
}
?>