<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// This function displays all the stuff you get with a richedit box - BBC, smileys etc.
function theme_control_richedit($editor_id, $smileyContainer = null, $bbcContainer = null)
{
	global $context, $settings, $options, $txt, $modSettings, $scripturl;

	$editor_context = &$context['controls']['richedit'][$editor_id];

	echo '
	<div class="bwgrid" style="margin-bottom: 0.5em;">
		<div class="bwcell16">
			<div id="smiley_buttons">';
	// Show the smileys.
	if (!empty($context['smileys']['postform']) && !$editor_context['disable_smiley_box'] && $smileyContainer !== null)
	{
		foreach ($context['smileys'] as $location => $smileyRows)
		{
			foreach ($smileyRows as $smileyRow)
			{
				foreach ($smileyRow['smileys'] as $smiley)
				{
					echo '
				&nbsp;<span class="smilies ', $smiley['filename'] , ' smileylink" title="' , $smiley['description'] , '" onclick="replaceText(\'' , $smiley['code'] , '\', ' , $editor_id , ')" style="cursor: pointer;"></span>';
				}
			}
		}
	}
	echo '
			</div>
		</div>
	</div>
	<div class="bwgrid">
		<div class="bwcell16">
			<textarea class="editor" name="', $editor_id, '" id="', $editor_id, '" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onchange="storeCaret(this);" tabindex="', $context['tabindex']++, '" style="margin-top: 4px; width: 100%; height: ', $editor_context['height'], '; ', isset($context['post_error']['no_message']) || isset($context['post_error']['long_message']) ? 'border: 1px solid red;' : '', '">', $editor_context['value'], '</textarea>
		</div>
		<div class="bwcell16">
			<span class="bbc_menu">
						<span id="bbctrigger2" class="mPopup breadcrumb_style roundbox" title="Special" popup-data="0,0,500" popupid="bSpecial">Special</span>
						<span id="bSpecial" style="display: none;" class="inline_blocks">
							<span class="bbc_post-it" style="float: none;" title="Post-It" onclick="surroundText(\'[postit]\',\'[/postit]\', ' , $editor_id , ')">Post-It</span>
							<span class="bbc_pullquote" style="float: none:" title="Pullquote" onclick="surroundText(\'[pullquote]\',\'[/pullquote]\', ' , $editor_id , ')">Pullquote</span>
							<span class="bbc_question" title="Question" onclick="surroundText(\'[question]\',\'[/question]\', ' , $editor_id , ')">Question</span>
							<span class="bbc_lead" title="Lead" onclick="surroundText(\'[lead]\',\'[/lead]\', ' , $editor_id , ')">Lead Text</span>
							<span class="bbc_conclusion" title="Conclusion" onclick="surroundText(\'[conclusion]\',\'[/conclusion]\', ' , $editor_id , ')">Conclusion</span>
							<span class="bbc_spoiler buton" style="width: 8em;" title="Spoiler" onclick="surroundText(\'[spoiler]\',\'[/spoiler]\', ' , $editor_id , ')">Spoiler</span>
						</span>
						<span id="bbctrigger3"  title="Formatting" class="mPopup breadcrumb_style roundbox" popupleft="0" popupid="bFormat">Formatting</span>
						<span id="bFormat" style="display: none;" class="inline_blocks">
							<span class="buton" title="Bold" onclick="surroundText(\'[b]\',\'[/b]\', ' , $editor_id , ')"><strong>Bold</strong></span>
							<span class="buton" onclick="surroundText(\'[i]\',\'[/i]\', ' , $editor_id , ')"><em>Italic</em></span>
							<span class="buton" onclick="surroundText(\'[s]\',\'[/s]\', ' , $editor_id , ')"><del>Strike-out</del></span>
						</span>
						<span id="bbctrigger4"  title="Objects" class="mPopup breadcrumb_style roundbox" popupid="bObjects">Objects</span>
						<span id="bObjects" style="display: none;" class="inline_blocks">
							<span class="social_icons youtube_icon"  style="float: none;width: 60px;" onclick="replaceText(\'[youtube][/youtube]\', ' , $editor_id , ')"></span>
							<span class="buton" onclick="replaceText(\'[img][/img]\', ' , $editor_id , ')" style="float: none;">Image</span>
							<span class="buton" onclick="replaceText(\'[tabular type=1]Caption[/tabular]\n[headers]header 1|header 2|header 3[/headers]\n[row1]me|you|us[/row1]\n[row2]mine|yours|ours[/row2]\n[row1]a|b|x[/row1]\n[endtabular]\n\', ' , $editor_id , ')">
								<table class="bbc_tabular bbc_tablecolorset1"><caption>Caption</caption><thead><tr><th>header 1</th><th>header 2</th><th>header 3</th></tr></thead><tbody><tr class="even"><td>me</td><td>you</td><td>us</td></tr> <tr class="odd"><td>mine</td><td>yours</td><td>ours</td></tr> <tr class="even"><td>a</td><td>b</td><td>x</td></tr> </tbody></table>
							</span>
							<span class="buton" onclick="replaceText(\'[tabular type=2]Caption[/tabular]\n[headers]header 1|header 2|header 3[/headers]\n[row1]me|you|us[/row1]\n[row2]mine|yours|ours[/row2]\n[row1]a|b|x[/row1]\n[endtabular]\n\', ' , $editor_id , ')">
								<table class="bbc_tabular bbc_tablecolorset2"><caption>Caption</caption><thead><tr><th>header 1</th><th>header 2</th><th>header 3</th></tr></thead><tbody><tr class="even"><td>me</td><td>you</td><td>us</td></tr> <tr class="odd"><td>mine</td><td>yours</td><td>ours</td></tr> <tr class="even"><td>a</td><td>b</td><td>x</td></tr> </tbody></table>
							</span>
							<span class="buton" onclick="replaceText(\'[tabular type=3]Caption[/tabular]\n[headers]header 1|header 2|header 3[/headers]\n[row1]me|you|us[/row1]\n[row2]mine|yours|ours[/row2]\n[row1]a|b|x[/row1]\n[endtabular]\n\', ' , $editor_id , ')">
								<table class="bbc_tabular bbc_tablecolorset3"><caption>Caption</caption><thead><tr><th>header 1</th><th>header 2</th><th>header 3</th></tr></thead><tbody><tr class="even"><td>me</td><td>you</td><td>us</td></tr> <tr class="odd"><td>mine</td><td>yours</td><td>ours</td></tr> <tr class="even"><td>a</td><td>b</td><td>x</td></tr> </tbody></table>
							</span>
							<span class="buton" onclick="replaceText(\'[tabular type=4]Caption[/tabular]\n[headers]header 1|header 2|header 3[/headers]\n[row1]me|you|us[/row1]\n[row2]mine|yours|ours[/row2]\n[row1]a|b|x[/row1]\n[endtabular]\n\', ' , $editor_id , ')">
								<table class="bbc_tabular bbc_tablecolorset4"><caption>Caption</caption><thead><tr><th>header 1</th><th>header 2</th><th>header 3</th></tr></thead><tbody><tr class="even"><td>me</td><td>you</td><td>us</td></tr> <tr class="odd"><td>mine</td><td>yours</td><td>ours</td></tr> <tr class="even"><td>a</td><td>b</td><td>x</td></tr> </tbody></table>
							</span>
						</span>
						<span id="bbctrigger5"  title="Headers" class="mPopup breadcrumb_style roundbox" popupid="bHeaders">Headers</span>
						<span id="bHeaders" style="display: none;" class="inline_blocks">
							<span class="buton" class="bbc_h1" style="margin: 8px 0;" onclick="surroundText(\'[h1]\',\'[/h1]\', ' , $editor_id , ')">Header1</span>
							<span class="buton" class="bbc_h2" style="margin: 8px 0;" onclick="surroundText(\'[h2]\',\'[/h2]\', ' , $editor_id , ')">Header2</span>
							<span class="buton" class="bbc_h3" style="margin: 8px 0;" onclick="surroundText(\'[h3]\',\'[/h3]\', ' , $editor_id , ')">Header3</span>
							<span class="buton" class="bbc_h4" style="margin: 8px 0;" onclick="surroundText(\'[h4]\',\'[/h4]\', ' , $editor_id , ')">Header4</span>
						';

	if (!isset($context['disabled_tags']['font']))
		echo '
							<span class="buton" style="font-family: Courier;"  onclick="surroundText(\'[font=courier]\',\'[/font]\', ' , $editor_id , ')">Courier</span>
							<span class="buton" style="font-family: Arial;" onclick="surroundText(\'[font=arial]\',\'[/font]\', ' , $editor_id , ')">Arial</span>
							<span class="buton" style="font-family: Impact;"   onclick="surroundText(\'[font=impact]\',\'[/font]\', ' , $editor_id , ')">Impact</span>
							<span class="buton" style="font-family: Verdana;"   onclick="surroundText(\'[font=verdana]\',\'[/font]\', ' , $editor_id , ')">Verdana</span>
							<span class="buton" style="font-family: Times New Roman;"   onclick="surroundText(\'[font=times new roman]\',\'[/font]\', ' , $editor_id , ')">Times New Roman</span>
							<span class="buton" style="font-family: Georgia;"   onclick="surroundText(\'[font=georgia]\',\'[/font]\', ' , $editor_id , ')">Georgia</span>
							<span class="buton" style="font-family: Trebuchet MS;"   onclick="surroundText(\'[font=trebuchet ms]\',\'[/font]\', ' , $editor_id , ')">Trebuchet MS</span>
							<span class="buton" style="font-family: Comic Sans MS;"   onclick="surroundText(\'[font=comic sans ms]\',\'[/font]\', ' , $editor_id , ')">Comic Sans MS</span>
						';

	if (!isset($context['disabled_tags']['size']))
		echo '
							<span class="buton" style="font-size: 0.7em; line-height: 1.2em;"  onclick="surroundText(\'[size=1]\',\'[/size]\', ' , $editor_id , ')">8pt</span>
							<span class="buton" style="font-size: 1.0em;line-height: 1.2em;"  onclick="surroundText(\'[size=2]\',\'[/size]\', ' , $editor_id , ')">10pt</span>
							<span class="buton" style="font-size: 1.35em;line-height: 1.2em;"  onclick="surroundText(\'[size=3]\',\'[/size]\', ' , $editor_id , ')">12pt</span>
							<span class="buton" style="font-size: 1.45em;line-height: 1.2em;"   onclick="surroundText(\'[size=4]\',\'[/size]\', ' , $editor_id , ')">14pt</span>
							<span class="buton" style="font-size: 2.0em;line-height: 1.2em;"   onclick="surroundText(\'[size=5]\',\'[/size]\', ' , $editor_id , ')">18pt</span>
							<span class="buton" style="font-size: 2.65em;line-height: 1.2em;"   onclick="surroundText(\'[size=6]\',\'[/size]\', ' , $editor_id , ')">24pt</span>
							<span class="buton" style="font-size: 3.95em;line-height: 1.2em;"   onclick="surroundText(\'[size=7]\',\'[/size]\', ' , $editor_id , ')">36pt</span>
						';

	echo '
						</span>';
	if (!isset($context['disabled_tags']['color']))
		echo '
						<span id="bbctrigger8"  title="Color" class="mPopup breadcrumb_style roundbox" popupid="bColor">Color</span>
						<span id="bColor" style="display: none;" class="inline_blocks">
							<span class="buton" style="color: black;"  onclick="surroundText(\'[color=black]\',\'[/color]\', ' , $editor_id , ')">Black</span>
							<span class="buton" style="color: red;"  onclick="surroundText(\'[color=red]\',\'[/color]\', ' , $editor_id , ')">Red</span>
							<span class="buton" style="color: blue;"  onclick="surroundText(\'[color=blue]\',\'[/color]\', ' , $editor_id , ')">Blue</span>
							<span class="buton" style="color: yellow;"  onclick="surroundText(\'[color=yellow]\',\'[/color]\', ' , $editor_id , ')">Yellow</span>
							<span class="buton" style="color: orange;"  onclick="surroundText(\'[color=orange]\',\'[/color]\', ' , $editor_id , ')">Orange</span>
							<span class="buton" style="color: green;"  onclick="surroundText(\'[color=green]\',\'[/color]\', ' , $editor_id , ')">Green</span>
							<span class="buton" style="color: beige;"  onclick="surroundText(\'[color=beige]\',\'[/color]\', ' , $editor_id , ')">Beige</span>
							<span class="buton" style="color: purple;"  onclick="surroundText(\'[color=purple]\',\'[/color]\', ' , $editor_id , ')">Purple</span>
							<span class="buton" style="color: brown;"  onclick="surroundText(\'[color=brown]\',\'[/color]\', ' , $editor_id , ')">Brown</span>
							<span class="buton" style="color: navy;"  onclick="surroundText(\'[color=navy]\',\'[/color]\', ' , $editor_id , ')">Navy</span>
							<span class="buton" style="color: white;"  onclick="surroundText(\'[color=white]\',\'[/color]\', ' , $editor_id , ')">White</span>
						</span>';

	echo '	
			</span>
		</div>
	</div>
	<div id="mpopup_attach" style="position:relative; overflow: hidden;"></div>
	<script type="text/javascript">
	
		window.addEvent(\'domready\', function(){
		var mpopup = new Element(\'div\',{\'id\': \'mpopup\'}).inject(mpopup_attach);
		var mpopupclose = new Element(\'div\',{\'id\': \'mpopupclose\'}).inject(mpopup_attach);
		mpopup.setStyles({
			\'opacity\':0
		});
		mpopupclose.setStyles({
			\'opacity\':0
		});

		$$(\'.mPopup\').addEvents({
			\'click\': function(e){
				tx = this.get(\'popupid\');
				rx = this.get(\'title\');
				mx = $(tx).get(\'html\');
				mpopup.set(\'html\',\'<b>\'+rx+\'</b><div>\'+mx+\'</div>\');
				mpopup.fade(\'in\');
				mpopupclose.fade(\'in\');
			},
		});
		mpopupclose.addEvents({
			\'click\': function(){
				mpopup.fade(\'out\');
				mpopup.set(\'html\',\'\');
				mpopupclose.fade(\'hide\');
			}
		});
	}); 
	</script>
	<input type="hidden" name="', $editor_id, '_mode" id="', $editor_id, '_mode" value="0" />';
}

function theme_control_richedit_buttons($editor_id)
{
	global $context, $settings, $options, $txt, $modSettings, $scripturl;

	$editor_context = &$context['controls']['richedit'][$editor_id];

	echo '
		<input type="submit" value="', isset($editor_context['labels']['post_button']) ? $editor_context['labels']['post_button'] : $txt['post'], '" tabindex="', $context['tabindex']++, '" onclick="return submitThisOnce(this);" accesskey="s" class="button_submit" />';

	if ($editor_context['preview_type'])
		echo '
		<input type="submit" name="preview" value="', isset($editor_context['labels']['preview_button']) ? $editor_context['labels']['preview_button'] : $txt['preview'], '" tabindex="', $context['tabindex']++, '" onclick="', $editor_context['preview_type'] == 2 ? 'return event.ctrlKey || previewPost();' : 'return submitThisOnce(this);', '" accesskey="p" class="button_submit" />';

	if ($context['show_spellchecking'])
		echo '
		<input type="button" value="', $txt['spell_check'], '" tabindex="', $context['tabindex']++, '" onclick="oEditorHandle_', $editor_id, '.spellCheckStart();" class="button_submit" />';
}

// What's this, verification?!
function theme_control_verification($verify_id, $display_type = 'all', $reset = false)
{
	global $context, $settings, $options, $txt, $modSettings;

	$verify_context = &$context['controls']['verification'][$verify_id];

	// Keep track of where we are.
	if (empty($verify_context['tracking']) || $reset)
		$verify_context['tracking'] = 0;

	// How many items are there to display in total.
	$total_items = count($verify_context['questions']) + ($verify_context['show_visual'] ? 1 : 0);

	// If we've gone too far, stop.
	if ($verify_context['tracking'] > $total_items)
		return false;

	// Loop through each item to show them.
	for ($i = 0; $i < $total_items; $i++)
	{
		// If we're after a single item only show it if we're in the right place.
		if ($display_type == 'single' && $verify_context['tracking'] != $i)
			continue;

		if ($display_type != 'single')
			echo '
			<div id="verification_control_', $i, '" class="verification_control">';

		// Do the actual stuff - image first?
		if ($i == 0 && $verify_context['show_visual'])
		{
			if ($context['use_graphic_library'])
				echo '
				<img src="', $verify_context['image_href'], '" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '" />';
			else
				echo '
				<img src="', $verify_context['image_href'], ';letter=1" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_1" />
				<img src="', $verify_context['image_href'], ';letter=2" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_2" />
				<img src="', $verify_context['image_href'], ';letter=3" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_3" />
				<img src="', $verify_context['image_href'], ';letter=4" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_4" />
				<img src="', $verify_context['image_href'], ';letter=5" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_5" />
				<img src="', $verify_context['image_href'], ';letter=6" alt="', $txt['visual_verification_description'], '" id="verification_image_', $verify_id, '_6" />';

			echo '
				<div class="smalltext" style="margin: 4px 0 8px 0;">
					<a href="', $verify_context['image_href'], ';sound" id="visual_verification_', $verify_id, '_sound" rel="nofollow">', $txt['visual_verification_sound'], '</a> / <a href="#" id="visual_verification_', $verify_id, '_refresh">', $txt['visual_verification_request_new'], '</a>', $display_type != 'quick_reply' ? '<br />' : '', '<br />
					', $txt['visual_verification_description'], ':', $display_type != 'quick_reply' ? '<br />' : '', '
					<input type="text" name="', $verify_id, '_vv[code]" value="', !empty($verify_context['text_value']) ? $verify_context['text_value'] : '', '" size="30" tabindex="', $context['tabindex']++, '" class="input_text" />
				</div>';
		}
		else
		{
			// Where in the question array is this question?
			$qIndex = $verify_context['show_visual'] ? $i - 1 : $i;

			echo '
				<div class="smalltext">
					', $verify_context['questions'][$qIndex]['q'], ':<br />
					<input type="text" name="', $verify_id, '_vv[q][', $verify_context['questions'][$qIndex]['id'], ']" size="30" value="', $verify_context['questions'][$qIndex]['a'], '" ', $verify_context['questions'][$qIndex]['is_error'] ? 'style="border: 1px red solid;"' : '', ' tabindex="', $context['tabindex']++, '" class="input_text" />
				</div>';
		}

		if ($display_type != 'single')
			echo '
			</div>';

		// If we were displaying just one and we did it, break.
		if ($display_type == 'single' && $verify_context['tracking'] == $i)
			break;
	}

	// Assume we found something, always,
	$verify_context['tracking']++;

	// Tell something displaying piecemeal to keep going.
	if ($display_type == 'single')
		return true;
}

?>