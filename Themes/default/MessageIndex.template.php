<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */


/* *************** */
/* the class definitions */
/* *************** */
class ProtendoMessageIndex
{
	function theme_main()
	{
		$this->theme_messageindex_above();
		$this->theme_childboards();
		$this->theme_topicsheader();
		$this->theme_description();
		$this->theme_topics();
		$this->theme_topicsfooter();
	}

	function theme_messageindex_above()
	{
		echo '
		<a id="top"></a>';
	}

	function theme_childboards()
	{
		global $context, $settings, $options, $scripturl, $modSettings, $txt, $board_info;

		if (!empty($context['boards']) && (!empty($options['show_children']) || $context['start'] == 0))
		{
			echo '
		<h2 class="mainheader">' . $txt['parent_boards'] . '</h2>
		<div class="headertexts">
			<div class="bwgrid">
				<div class="bwcell1">&nbsp;</div>
				<div class="bwcell6" style="text-align: left;">
					' . $txt['board'] . '
				</div>
				<div class="bwcell3" style="text-align: center;">
					' . $txt['topics'] . ' | ' . $txt['posts'] . '
				</div>
				<div class="bwcell6" style="text-align: center;"> 
					', $txt['last_post'], '
				</div>
			</div>
		</div>';

			$alt=true;
			foreach($context['boards'] as $bd)
			{
				// decide the helping texts, easier to figure out in advance.
				if($bd['new'] || $bd['children_new'])
					$alttext = $txt['new_posts'];
				elseif($bd['is_redirect'])
					$alttext = $txt['redirect_board'];
				else
					$alttext = $txt['old_posts'];

				echo '
		<div class="windowbg' , $alt ? '2' : '' , ' bxwindows">
			<div class="bwgrid">
				<div class="bwcell1">
					<a href="' . $bd['href'] . '" title="' . $alttext . '" class="' , !empty($options['messageindex_preview']) ? '' : 'Tip ' , ($bd['new'] || $bd['children_new']) ? 'on' . ($bd['children_new'] ? '2' : '') : 'off' , $bd['is_redirect'] ? ' redirect' : '', '">&nbsp;</a>
				</div>
				<div class="bwcell7"><div style="padding: 0 1em;">
					<h3><a href="' . $bd['href'] . '"><b>' . $bd['name'] . '</b></a></h3>
					' , !empty($bd['description']) ? '<div class="middletext greytext">' . $bd['description'] . '</div>' : '' , '
				</div></div>
				<div class="bwcell3" style="margin-top: 5px; white-space: nowrap;">' . $bd['topics'] . ' | ' . $bd['posts'] . '</div>
				<div class="bwcell5">
					<div style="padding: 0 0 0 1em;">'; 
			
				if (!empty($bd['last_post']['id']))
					echo '
						<div class="bwgrid">
							<div class="bwcell3">
								<a href="' . $bd['last_post']['href'] .'"><span class="arrow" style="margin: 10px auto 0 auto;"></span></a>
							</div>
							<div class="bwcell13 middletext">
								<strong>'. $bd['last_post']['member']['link'] , ' ', $txt['in'], ' ', $bd['last_post']['link'], '</strong>
								<br />', $bd['last_post']['time'],'
							</div>
						</div>';
				else
					echo '
						<div class="bwgrid">
							<div class="bwcell3">
								<span class="arrow" style="margin: 10px auto 0 auto;"></span>
							</div>
							<div class="bwcell13 middletext" style="margin-top: 10px;">
								', $txt['not_applicable'] , '
							</div>
						</div>';

				echo '
					</div>
				</div>';

				// Show the "Child Boards: ". (there's a link_children but we're going to bold the new ones...)
				if (!empty($bd['children']))
				{
					// Sort the links into an array with new boards bold so it can be imploded.
					$children = array();
					/* Each child in each board's children has:
							id, name, description, new (is it new?), topics (#), posts (#), href, link, and last_post. */
					foreach ($bd['children'] as $child)
					{
						if (!$child['is_redirect'])
							$child['link'] = '<a href="' . $child['href'] . '" ' . ($child['new'] ? 'class="new_posts" ' : '') . 'title="' . ($child['new'] ? $txt['new_posts'] : $txt['old_posts']) . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')">' . $child['name'] . ($child['new'] ? '</a> <a href="' . $scripturl . '?action=unread;board=' . $child['id'] . '" title="' . $txt['new_posts'] . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')"><img src="' . $settings['lang_images_url'] . '/new.gif" class="new_posts" alt="" />' : '') . '</a>';
						else
							$child['link'] = '<a href="' . $child['href'] . '" title="' . comma_format($child['posts']) . ' ' . $txt['redirects'] . '">' . $child['name'] . '</a>';

						// Has it posts awaiting approval?
						if ($child['can_approve_posts'] && ($child['unapproved_posts'] || $child['unapproved_topics']))
							$child['link'] .= ' <a href="' . $scripturl . '?action=moderate;area=postmod;sa=' . ($child['unapproved_topics'] > 0 ? 'topics' : 'posts') . ';brd=' . $child['id'] . ';' . $context['session_var'] . '=' . $context['session_id'] . '" title="' . sprintf($txt['unapproved_posts'], $child['unapproved_topics'], $child['unapproved_posts']) . '" class="moderation_link">(!)</a>';

						$children[] = $child['new'] ? '<strong>' . $child['link'] . '</strong>' : $child['link'];
					}
					echo '
			</div>
			<div class="bwgrid">
				<div class="bwcell16 middletext">
					<hr />
					<h4 style="display: inline-block; padding-left: 6%;"><strong style="padding-left: 1em;">', $txt['parent_boards'], '</strong></h4>: ', implode(' | ', $children), '
				</div>';
				}

				echo ' 
			</div>
		</div>'; 
				
				$alt = !$alt;
			}
			echo '<br>';
		}
	}

	function theme_topicsheader()
	{
		global $context, $settings, $options, $scripturl, $modSettings, $txt, $board_info;

		if (!$context['no_topic_listing'])
			echo '
		<div class="floatright" >', $context['themeobject']->theme_button_strip($context['normal_buttons'], '', true), '</div>
		<h2 class="mainheader">' , $board_info['name'] , '</h2>';
	}

	function theme_description()
	{
		global $context;

		if ($context['description'] != '' && !$context['no_topic_listing'])
			echo '<div class="headerarea">' , $context['description'].'</div>';
	}

	function theme_topics()
	{
		global $context, $settings, $options, $scripturl, $modSettings, $txt, $board_info;

		if (!empty($context['topics']))
			echo '
		<div class="pagelinks">
			', $context['page_index'], !empty($modSettings['topbottomEnable']) ? $context['menu_separator'] . '&nbsp;&nbsp;<a href="#bot">' . $txt['go_down'] . '</a>' : '', '
		</div>';
		
		if (!empty($settings['display_who_viewing']))
		{
			echo '
		<div class="headerarea2 middletext">';
			if ($settings['display_who_viewing'] == 1)
				echo count($context['view_members']), ' ', count($context['view_members']) === 1 ? $txt['who_member'] : $txt['members'];
			else
				echo empty($context['view_members_list']) ? '0 ' . $txt['members'] : implode(', ', $context['view_members_list']) . ((empty($context['view_num_hidden']) or $context['can_moderate_forum']) ? '' : ' (+ ' . $context['view_num_hidden'] . ' ' . $txt['hidden'] . ')');
			echo $txt['who_and'], $context['view_num_guests'], ' ', $context['view_num_guests'] == 1 ? $txt['guest'] : $txt['guests'], $txt['who_viewing_board'], '
		</div>';
		}

		if (!$context['no_topic_listing'])
		{

			// If Quick Moderation is enabled start the form.
			if (!empty($context['can_quick_mod']) && $options['display_quick_mod'] > 0 && !empty($context['topics']))
				echo '
		<form action="', $scripturl, '?action=quickmod;board=', $context['current_board'], '.', $context['start'], '" method="post" accept-charset="', $context['character_set'], '" class="clear" name="quickModForm" id="quickModForm">';


			// Are there actually any topics to show?
			if (!empty($context['topics']))
			{
				echo '
			<div class="headertexts">
				<div class="bwgrid">
					<div class="bwcell1">&nbsp;</div>
					<div class="bwcell8">
						<a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=subject', $context['sort_by'] == 'subject' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['subject'], $context['sort_by'] == 'subject' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a> / <a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=starter', $context['sort_by'] == 'starter' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['started_by'], $context['sort_by'] == 'starter' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a>
					</div>
					<div class="bwcell2"><a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=replies', $context['sort_by'] == 'replies' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['replies'], $context['sort_by'] == 'replies' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a> / <a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=views', $context['sort_by'] == 'views' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['views'], $context['sort_by'] == 'views' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a></div>
					<div class="bwcell5">';

				// Show a "select all" box for quick moderation?
				if (!empty($context['can_quick_mod']) && $options['display_quick_mod'] == 1)
					echo '
						<input type="checkbox" onclick="invertAll(this, this.form, \'topics[]\');" class="input_check" style="float: right; margin: 2px 2px 0 8px;" />';

				echo '
						<a href="', $scripturl, '?board=', $context['current_board'], '.', $context['start'], ';sort=last_post', $context['sort_by'] == 'last_post' && $context['sort_direction'] == 'up' ? ';desc' : '', '">', $txt['last_post'], $context['sort_by'] == 'last_post' ? ' <img src="' . $settings['images_url'] . '/sort_' . $context['sort_direction'] . '.gif" alt="" />' : '', '</a>
					</div>
				</div>
			</div>';
			}
			// No topics.... just say, "sorry bub".
			else
				echo '
					<div class="headerarea"><strong class="largetext">', $txt['msg_alert_none'], '</strong></div>';

			// If this person can approve items and we have some awaiting approval tell them.
			if (!empty($context['unapproved_posts_message']))
				echo '
			<div class="windowbg2">', $context['unapproved_posts_message'], '</div>';

			$alt=true;
			foreach ($context['topics'] as $topic)
			{
				if ($context['can_approve_posts'] && $topic['unapproved_posts'])
					$color_class = !$topic['approved'] ? 'approvetbg' : 'approvebg';
				else
					$color_class = 'windowbg';

				if($alt)
					$color_class .= '2';
				
				$alternate_class = $color_class;

				echo '
			<div class="' . $color_class. ' bxwindows">
				', $topic['is_sticky'] ? '<span class="sticky_corner Tip" title="' . $txt['sticky_topic'] . '"></span>' : '', '
				', !$topic['approved'] ? '<span class="approved_corner Tip" title="' . $txt['mc_unapproved_poststopics'] . '"></span>' : '', '
				<div class="bwgrid">
					<div class="bwcell1"><span class="messicons mess_' , $topic['first_post']['icon'] , '" style="margin: 5px 0 0 10px;"></span></div>
					<div class="bwcell8">
						<a href="', $topic['first_post']['href'], '"  title="' , !empty($options['messageindex_preview']) ? '' : $txt['first_post_preview']. ' :: '. $topic['first_post']['preview'],  '" class="' , !empty($options['messageindex_preview']) ? '' : 'TipBig', '">
							' , $topic['new'] && $context['user']['is_logged'] ? '<strong>' : '', $topic['first_post']['subject'], $topic['new'] && $context['user']['is_logged'] ? '</strong>' : '','
						</a>
						', (!$context['can_approve_posts'] && !$topic['approved'] ? '&nbsp;<em>(' . $txt['awaiting_approval'] . ')</em>' : ''), '
						<br><span class="smalltext greytext">', $txt['started_by'], ' ', $topic['first_post']['member']['link'], '</span>
						<small id="pages' . $topic['first_post']['id'] . '">', $topic['pages'], '</small>
					</div>
					<div class="bwcell2 middletext">
						', $topic['replies'], ' | ', $topic['views'], ' 
					</div>
					<div class="bwcell5">
						<div class="bwgrid">
							<div class="bwcell3">
								<a href="' . $topic['last_post']['href'] .'"><span class="arrow" style="margin: 10px auto 0 auto;"></span></a>
							</div>
							<div class="bwcell13 middletext">';
				// Show the quick moderation options?
				if (!empty($context['can_quick_mod']))
					echo '
								<input type="checkbox" name="topics[]" value="', $topic['id'], '" class="input_check" style="float: right; margin: 2px 0 0 8px;" />';
				echo '
								', $topic['last_post']['time'], '<br />
								', $txt['by'], ' ', $topic['last_post']['member']['link'], '
							</div>
						</div>
					</div>
				</div>
			</div>';
				$alt = !$alt;
			}
		}
		echo '
		<a id="bot"></a>';
	}

	function theme_topicsfooter()
	{
		global $context, $settings, $options, $scripturl, $modSettings, $txt, $board_info;

		if (!empty($context['topics']))
			echo '
		<div class="pagelinks">', $context['page_index'], !empty($modSettings['topbottomEnable']) ? $context['menu_separator'] . '&nbsp;&nbsp;<a href="#top">' . $txt['go_up'] . '</a>' : '', '</div>';
		
		echo '
		<div class="headertexts" style="padding-left: 2em;padding-right: 2em;">
			' , !empty($context['can_quick_mod']) && $options['display_quick_mod'] == 1 && !empty($context['topics']) ? '<span class="floatright">' . $txt['quickmod_header'] . '</span>' : '' ,'
			' , $txt['jump_to'] , '
		</div>
		<div class="windowbg" style="overflow: hidden;">';
		if (!empty($context['can_quick_mod']) && $options['display_quick_mod'] == 1 && !empty($context['topics']))
		{
			echo '
			<div class="floatright">
			<select class="qaction" name="qaction"', $context['can_move'] ? ' onchange="this.form.moveItTo.disabled = (this.options[this.selectedIndex].value != \'move\');"' : '', '>
				<option value="">--------</option>', $context['can_remove'] ? '
				<option value="remove">' . $txt['quick_mod_remove'] . '</option>' : '', $context['can_lock'] ? '
				<option value="lock">' . $txt['quick_mod_lock'] . '</option>' : '', $context['can_sticky'] ? '
				<option value="sticky">' . $txt['quick_mod_sticky'] . '</option>' : '', $context['can_move'] ? '
				<option value="move">' . $txt['quick_mod_move'] . ': </option>' : '', $context['can_merge'] ? '
				<option value="merge">' . $txt['quick_mod_merge'] . '</option>' : '', $context['can_restore'] ? '
				<option value="restore">' . $txt['quick_mod_restore'] . '</option>' : '', $context['can_approve'] ? '
				<option value="approve">' . $txt['quick_mod_approve'] . '</option>' : '', $context['user']['is_logged'] ? '
				<option value="markread">' . $txt['quick_mod_markread'] . '</option>' : '', '
			</select>';
			// Show a list of boards they can move the topic to.
			if ($context['can_move'])
			{
				echo '
			<select class="qaction" id="moveItTo" name="move_to" disabled="disabled">';

				foreach ($context['move_to_boards'] as $category)
				{
					echo '
				<optgroup label="', $category['name'], '">';
					foreach ($category['boards'] as $board)
							echo '
					<option value="', $board['id'], '"', $board['selected'] ? ' selected="selected"' : '', '>', $board['child_level'] > 0 ? str_repeat('==', $board['child_level'] - 1) . '=&gt;' : '', ' ', $board['name'], '</option>';
					echo '
				</optgroup>';
				}
				echo '
			</select>';
			}
			echo '
			<input type="submit" value="', $txt['quick_mod_go'], '" onclick="return document.forms.quickModForm.qaction.value != \'\' &amp;&amp; confirm(\'', $txt['quickmod_confirm'], '\');" class="button_submit qaction" />
			</div>';
		}
		echo '
			<div id="message_index_jump_to">&nbsp;</div>
			<script type="text/javascript"><!-- // --><![CDATA[
				if (typeof(window.XMLHttpRequest) != "undefined")
					aJumpTo[aJumpTo.length] = new JumpTo({
						sContainerId: "message_index_jump_to",
						sJumpToTemplate: "<label class=\"smalltext\" for=\"%select_id%\">', $context['jump_to']['label'], ':<" + "/label> %dropdown_list%",
						iCurBoardId: ', $context['current_board'], ',
						iCurBoardChildLevel: ', $context['jump_to']['child_level'], ',
						sCurBoardName: "', $context['jump_to']['board_name'], '",
						sBoardChildLevelIndicator: "==",
						sBoardPrefix: "=> ",
						sCatSeparator: "-----------------------------",
						sCatPrefix: "",
						sGoButtonLabel: "', $txt['quick_mod_go'], '"
					});
			// ]]></script>
		</div>';

		// Finish off the form - again.
		if (!empty($context['can_quick_mod']) && $options['display_quick_mod'] > 0 && !empty($context['topics']))
			echo '
		<input type="hidden" name="' . $context['session_var'] . '" value="' . $context['session_id'] . '" />
		</form><br>';
	}
}

?>