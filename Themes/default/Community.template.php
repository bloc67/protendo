<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 

function template_main()
{
	global $modSettings, $context;

	createThemeObject('Community');
	$context['subthemeobject']->template_main();
}

/* *************** */
/* the class definitions */
/* *************** */
class ProtendoCommunity
{
	function template_main()
	{
		global $context, $settings, $options, $txt, $scripturl, $boardurl, $modSettings;

		echo '
		<div class="bwgrid">';
		foreach($context['plugs'] as $plug => $plugdata)
		{
			$orig = $context['multipost'][$plug];
			echo '
			<div class="bwcell33"><div style="padding: 4px 2em 4px 0;">
				<h2 class="mainheader"><a href="' . $scripturl . '?action=' . $plug . '">' , $modSettings['plugins'][$plug]['menu']['title'] , '</a></h2>
				<a href="' . $scripturl . '?topic='.$orig['topic'].'.0">
					<img class="imgstyle2" src="' , !empty($context['blogimages'][$orig['id_msg']]['thumb']) ? $context['blogimages'][$orig['id_msg']]['thumb'] : $settings['images_url'].'/noimage.png' , '" alt="" />
				</a>
				<h3 class="blogheader" ><a href="' . $orig['href'].'">' , $orig['subject'] , '</a>	</h3>
				<span class="middletext greytext" style="display: block;clear: both;">
				' , blogformat($orig['timestamp']) , ' av <b>' . $orig['poster']['link'] . '</b> i ' . $orig['board']['link'] . ' </span>
				<hr>
				<div class="post">				
					' , $orig['body'] , ' 
				</div>
				<p class="greytext breadcrumb_style">
					<a href="' , $orig['href'] , '"><b>' . $orig['replies'] . ' ' . $txt['replies'] . '</b></a> | ' . $orig['views'] . ' ' . $txt['views'] . '
				' , !empty($orig['replies']) ? '&nbsp;&nbsp;<a href="' . $orig['href'] . '">' . $orig['replies'] . ' ' . $txt['comments'] . '</a>' : '' , '
				</p>
			</div></div>';
		}

		echo '
		</div>';
	}
}

?>