<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// Initialize the template... mainly little settings.
function pre_template()
{
	global $context, $settings, $options, $txt, $scripturl;

	$settings['theme_protendo_id'] = 'urbanlife';
	$settings['subtheme'] = 'Urbanlife';
}

/* *************** */
/* the class definitions */
/* *************** */
class UrbanlifeIndex extends ProtendoIndex
{
	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '
		<header class="forumwidth" id="topheader">
			<div class="cols">
				<div class="floatleft">
					<h1 id="forumtitle">
						<a href="', $scripturl, '">
							', $context['forum_name'], '
							<span class="hideme"><span>
						</a>
					</h1>' , 
					!empty($settings['site_slogan']) ? '<em id="siteslogan">' . $settings['site_slogan'] . '</span>' : '' , '
				</div>
				<div class="floatleft">
					' , $this->theme_menu() , '
				</div>
				<div class="floatright">
					<div class="floatleft">
						' , 	($context['allow_search']) ? $this->template_quicksearch() : '' , '
						' , $this->theme_userarea() , '
					</div>
				</div>
			</div>
			<span class="clear" style="display: block;"></span>
		</header>
		<div id="breadcrumb">
			<div id="linktree">
				<div class="forumwidth">' , $this->theme_linktree(), '</div>	
			</div>';
		
		if(!empty($settings['enable_news']) && !empty($context['random_news_line']))
			echo '
			<div id="news_container" class="urb_', empty($options['collapse_news_fader']) ? 'show' : 'hide', ' forumwidth">
				<h2>', $txt['news'], '</h2>
				', $this->template_news_fader(), '
			</div>';

		echo '
		</div>
		<article>';

		if(!empty($context['urbanlife_settings']['usefullwidth']))
			echo '
			<div>';
		else
			echo '
			<div class="forumwidth">';
	}

	function theme_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '
			</div>
		</article>
		<footer><div class="forumwidth">
			<menu id="bottom_menu">
				' , $this->template_menu_bottom() , '
			</menu>
			<div id="copyrite_section">
				<div class="cols">
					<div class="col5">
						', theme_copyright(), 
						$context['show_load_time'] ? '<p>'. $context['load_time']. 's | '. $context['load_queries'] . 'q</p>' : '' ,'
					</div>
					<div class="col5 col_right">	
						<a href="#top">' , $txt['go_up'] , '</a>
						', !empty($modSettings['xmlnews_enable']) && (!empty($modSettings['allow_guestAccess']) || $context['user']['is_logged']) ? ' | <a href="' . $scripturl . '?action=.xml;type=rss" class="new_win">' . $txt['rss'] . '</a>' : '', '
					</div>
				</div>
			</div>
		</div></footer>';
	}

	function theme_userarea()
	{
		global $context, $settings, $scripturl, $txt, $modSettings;

		echo '
		<div class="mydivider floatleft">';
		// Show log in form to guests.
		if (!empty($context['show_login_bar']))
		{
			echo '
			<script src="', $settings['default_theme_url'], '/scripts/sha256.js"></script>
			<form style="padding-top: 3em;" action="', $scripturl, '?action=login2;quicklogin" method="post" accept-charset="UTF-8" ', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\');"' : '', '>
				<div id="password_login">
					<input type="text" name="user" size="10" class="input_text" placeholder="', $txt['username'], '" />
					<input type="password" name="passwrd" size="10" class="input_password" placeholder="', $txt['password'], '" />
					<select name="cookielength">
						<option value="60">', $txt['one_hour'], '</option>
						<option value="1440">', $txt['one_day'], '</option>
						<option value="10080">', $txt['one_week'], '</option>
						<option value="43200">', $txt['one_month'], '</option>
						<option value="-1" selected="selected">', $txt['forever'], '</option>
					</select>
					<input type="submit" value="', $txt['login'], '" class="button_submit" />
				</div>
				<input type="hidden" name="hash_passwrd" value="" />
				<input type="hidden" name="old_hash_passwrd" value="" />
				<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
				<input type="hidden" name="', $context['login_token_var'], '" value="', $context['login_token'], '" />';

			if (!empty($modSettings['enableOpenID']))
				echo '
				<a class="button_submit top_button" href="', $scripturl, '?action=login;openid"><img src="' . $settings['images_url'] . '/openid.png" title="' . $txt['openid'] . '" alt="' . $txt['openid'] . '" /></a>';
			echo '
			</form>';
		}
		else
			$this->template_menu_profile();
		
		echo '
		</div>';
	}

	public function template_quicksearch()
	{
		global $context, $settings, $scripturl, $txt, $modSettings;

			echo '
				<form id="search_form" action="', $scripturl, '?action=search;sa=results" method="post" accept-charset="UTF-8">
					<input type="text" name="search" value="" id="quicksearch_input" style="width: 6em;" placeholder="', $txt['search'], '" />';

			// Search within current topic?
			if (!empty($context['current_topic']))
				echo '
					<input type="hidden" name="topic" value="', $context['current_topic'], '" />';
			// If we're on a certain board, limit it to this board ;).
			elseif (!empty($context['current_board']))
				echo '
					<input type="hidden" name="brd[', $context['current_board'], ']" value="', $context['current_board'], '" />';

			echo '
					<input type="hidden" name="advanced" value="0" />
				</form>';
	}

	// Show the menu up top. Something like [home] [help] [profile] [logout]...
	public function theme_linktree($default = 'linktree')
	{
		global $context, $settings;

		if(!empty($settings['enable_news']) && !empty($context['random_news_line']))
			echo '			
					<img class="floatright" id="newsupshrink" src="', $settings['images_url'], '/collapse.gif" alt="*" title="', $txt['upshrink_description'], '" align="bottom" style="display: none;" />';

		// If linktree is empty, just return - also allow an override.
		if (empty($context[$default]))
			return;

		echo '
					<ul class="horiz_menu" id="linktree">';

		foreach ($context[$default] as $link_num => $tree)
		{
			echo '
						<li>';

			if (isset($tree['extra_before']))
				echo $tree['extra_before'];

			// Show the link, including a URL if it should have one.
			echo $settings['linktree_link'] && isset($tree['url']) ? '<a href="' . $tree['url'] . '">' . $tree['name'] . '</a>' : $tree['name'];

			// Show something after the link...?
			if (isset($tree['extra_after']))
				echo $tree['extra_after'];

			echo '
						</li>';
		}

		echo '
					</ul>';
	}
	function theme_menu()
	{
		global $context, $txt;

		echo '	
					<nav id="menu-wrap">
						<ul id="menu">';


		foreach ($context['menu_buttons'] as $act => $button)
		{
			if(!isset($button['top']))
				continue;

			echo '
							<li id="button_', $act, '" class="groundlevel">
								<a class="', !empty($button['active_button']) ? ' active' : '' , '" href="', $button['href'], '"', isset($button['target']) ? ' target="' . $button['target'] . '"' : '', '>', $button['title'],  '</a>';

			// Any 2nd level menus?
			if (!empty($button['sub_buttons']))
			{
				echo '
								<ul>';
				foreach ($button['sub_buttons'] as $childbutton)
				{
					echo '
									<li>
										<a href="', $childbutton['href'], '" ', isset($childbutton['target']) ? 'target="' . $childbutton['target'] . '"' : '', '>', $childbutton['title'], !empty($childbutton['sub_buttons']) ? '...' : '','</a>';

					// 3rd level menus :)
					if (!empty($childbutton['sub_buttons']))
					{
						echo '
										<ul>';

						foreach ($childbutton['sub_buttons'] as $grandchildbutton)
							echo '
											<li>
												<a href="', $grandchildbutton['href'], '" ', isset($grandchildbutton['target']) ? 'target="' . $grandchildbutton['target'] . '"' : '', '>', $grandchildbutton['title'], '</a>
											</li>';

						echo '
										</ul>';
					}
					echo '
									</li>';
				}
				echo '
								</ul>';
			}
			echo '
							</li>';
		}
		echo '
						</ul>
					</nav>';
	}

	public function template_news_fader()
	{
		global $settings, $context, $options;

			echo '
		<p>', $context['random_news_line'], '</p>';
	}
	public function template_menu_profile()
	{
		global $context, $txt;

		$button = $context['menu_buttons']['profile'];
		echo '	
					<nav id="menu-wrap-profile">
						<ul id="menu-profile">
							<li id="profile-button_profile" class="groundlevel">
								<a href="', $button['href'], '">';
		
		if(!empty($context['user']['messages']))
			echo '
									<span class="pm-notice">' , $context['user']['messages'] , '</span>';
		else
			echo '
									<span class="pm-notice">' , $context['user']['messages'] , '</span>';

		if(!empty($context['user']['avatar']['href']))
			echo '
									<img src="' , $context['user']['avatar']['href'] , '" class="topavatar" alt="" />';
		else
			echo '
									<img src="' , $settings['images_url'] , '/default_avatar.png" class="topavatar" alt="" />';



		echo '				</a>	';

		if (!empty($button['sub_buttons']))
		{
			echo '
								<ul>
									<li>
										<a href="', $scripturl, '?action=pm">', $txt['pm_short'], '</a>
									</li>
								';

			foreach ($button['sub_buttons'] as $childbutton)
			{
				echo '
									<li>
										<a href="', $childbutton['href'], '" ', isset($childbutton['target']) ? 'target="' . $childbutton['target'] . '"' : '', '>', $childbutton['title'], '</a>
									</li>';
			}
			echo '
									<li>
										<hr>
									</li>
									<li>
										<a href="', $scripturl, '?action=unread">', $txt['unread_pro'], '</a>
									</li>
									<li>
										<a href="', $scripturl, '?action=unreadreplies">', $txt['replies_pro'], '</a>
									</li>
								</ul>';
		}
		echo '
							</li>
						</ul>
					</nav>';
	}

	public function template_menu_bottom()
	{
		global $context, $txt;

		echo '
				<ul id="bottom_menu" class="horiz_menu">';

		foreach ($context['menu_buttons'] as $act => $button)
		{
			if($act == 'profile')
				continue;

			echo '
					<li><a class="', !empty($button['active_button']) ? ' active' : '', '" href="', $button['href'], '">', $button['title'], '</a></li>';
		}
		echo '
				</ul>';
	}

}

// set a gradient inline style - used for infographics that need dynamic values for the percentages
function getGradient($option,$front = 'ffffff', $back = '008000')
{
	echo 'background: -moz-linear-gradient(bottom,  #' , $front , ' 0%, #' , $front , ' ', $option, '%, #' , $back , ' ', $option, '%, #' , $back , ' 100%); background: -webkit-gradient(linear, left bottom, left top, color-stop(0%,#' , $front , '), color-stop(', $option, '%,#' , $front , '), color-stop(', $option, '%,#' , $back , '), color-stop(100%,#' , $back , ')); background: -webkit-linear-gradient(bottom,  #' , $front , ' 0%,#' , $front , ' ', $option, '%,#' , $back , ' ', $option, '%,#' , $back , ' 100%); 	background: -o-linear-gradient(bottom,  #' , $front , ' 0%,#' , $front , ' ', $option, '%,#' , $back , ' ', $option, '%,#' , $back , ' 100%); background: -ms-linear-gradient(bottom,  #' , $front , ' 0%,#' , $front , ' ', $option, '%,#' , $back , ' ', $option, '%,#' , $back , ' 100%); background: linear-gradient(to top,  #' , $front , ' 0%,#' , $front , ' ', $option, '%,#' , $back , ' ', $option, '%,#' , $back , ' 100%);'; 
}
?>