<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */
 
// Initialize the template... mainly little settings.
function pre_template()
{
	global $context, $settings, $options, $txt, $scripturl;

	$settings['use_default_images'] = 'never';
	$settings['doctype'] = 'html';
	$settings['theme_version'] = '1.0';
	$settings['message_index_preview'] = true;
	$settings['subtheme'] = 'Bubbles';

	$settings['fixedheader'] = '
		<script type="text/javascript">
			/* fixed header */
			window.addEvent(\'domready\',function() {
				new SmoothScroll({duration:50});
				var ss = new ScrollSpy({
					min: 70,
					onEnter: function(position,enters) {
						$(\'topheader\').className = \'blue\';
					},
					onLeave: function(position,leaves) {
						$(\'topheader\').className = \'\';
					},
					container: window
				});
			});		
		</script>';	// any extra buttons we need from the theme, we set here

	// any extra buttons we need from the theme, we set here
	/*
	$settings['theme_buttons'] = array(
		'boardindex' => array(
			'testme' => array('text' => 'progress', 'lang' => true, 'url' => $scripturl . '?action=profile;' ),
		),
	);
	*/
}

class BubblesIndex extends ProtendoIndex
{

	function theme_content_above()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '
		<div id="topheader">
			<div class="mainwidth">
				<div class="floatleft">
					<h1><a title="' , $context['forum_name'] , '" href="' , $scripturl , '"><img src="' , !empty($settings['header_logo_url']) ?  $settings['header_logo_url'].'"' : $settings['images_url']. '/logo.png" id="mylogo"' , ' alt="" /></a></h1>
					' , !empty($settings['site_slogan']) ?  '<em id="siteslogan">' . $settings['site_slogan'] . '</em>' : '' , '
				</div>
				<div id="user_section" class="floatright">';
		
		if ($context['user']['is_logged'])
			echo '
		<span class="breadcrumb_style" style="white-space: nowrap; ">', $txt['you_are_logged'], ' <a href="' , $scripturl , '?action=profile">', $context['user']['name'], '</a></span>';
			
			echo '	
				</div>
				' , $this->theme_menu(), '
			</div>
		</div>
		' , function_exists('template_inserted') ? template_inserted() : '' , '
		<div id="main_content">
			<div class="mainwidth">
						<div style="position: relative;">';

		if (!$context['user']['is_logged'])
			echo '
						<script type="text/javascript" src="', $settings['default_theme_url'], '/scripts/sha1.js"></script>
						<form id="guest_form" class="windowbg2" action="', $scripturl, '?action=login2" method="post" accept-charset="', $context['character_set'], '" ', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\');"' : '', '>
							<input type="text" name="user" size="10" class="guest_login"  style="width: 22%;" />
							<input type="password" name="passwrd" size="10" class="guest_login"  style="width: 22%;" />
							<select name="cookielength" class="guest_login" style="width: 22%;">
								<option value="60">', $txt['one_hour'], '</option>
								<option value="1440">', $txt['one_day'], '</option>
								<option value="10080">', $txt['one_week'], '</option>
								<option value="43200">', $txt['one_month'], '</option>
								<option value="-1" selected="selected">', $txt['forever'], '</option>
							</select>
							<input type="submit" value="', $txt['login'], '" class="button_submit"  style="line-height: 1.5em; width: 15%;" />
							<input type="hidden" name="hash_passwrd" value="" />
							<a href="' , $scripturl , '?action=login">' , $txt['more'] ,  '<a>
						</form>
						<script type="text/javascript">
							reveal_login = new Fx.Reveal($(\'guest_form\'));
							window.addEvent(\'domready\', function() {
								$(\'show_guest_form\').addEvent(\'click\', function(e){
										e.stop();
										reveal_login.toggle();
								});
							});
						</script>';
		else
			echo '
					<div class="floatright" style="position:relative; margin-top: 0;">
						<div class="floatright quickbutton_single">
							<span id="sidebar_toggle_container">
								<span id="sidebar_toggle" class="triangle_' , !empty($options['usersidebar']) ? 'right' : 'left' , '"></span>
								<span style="white-space: nowrap; position: absolute; "></span>
							</span>';

		if(!empty($context['user']['avatar']['image']))
			echo '
						<span class="floatright mini_avatar" style="background-image: url(' , $context['user']['avatar']['href'] , ');"></span>';
		
		if ($context['user']['is_logged'])
		{
			echo '	
							<a id="pms" href="' , $scripturl , '?action=pm">', $txt['pm_short'], '<span class="' , $context['user']['unread_messages']>0 ? 'notice' : 'noticegrey' , '">' . $context['user']['unread_messages'] . '</span>' , '</a>';

			// Are there any members waiting for approval?
			if (!empty($context['unapproved_members']))
				echo '
							<a href="', $scripturl, '?action=admin;area=viewmembers;sa=browse;type=approve">' , $txt['approve_members_waiting'] , $context['unapproved_members']>0 ? '<span class="notice">'.$context['unapproved_members'].'</span>' : $context['unapproved_members'] , '</a>';

			echo '
							<a href="', $scripturl, '?action=unread">', $txt['unread'] , '<span class="', $context['unread_topics_user']>0 ? 'notice' : 'noticegrey' , '">'. $context['unread_topics_user'] . '</span></a>';
			
			if (!empty($context['open_mod_reports']) && $context['show_open_reports'])
				echo '
							<a href="', $scripturl, '?action=moderate;area=reports">', $txt['mc_reported_posts'] , '<span class="notice">' , $context['open_mod_reports'], '</span></a>';
		}
		echo '	
						</div>
					</div>';
		
		$this->theme_linktree();
		echo '
					<div><div class="' , !empty($options['usersidebar']) ? 'showme' : 'hideme' , '" id="sbar">
						<div>' , $this->theme_user_below() , '</div>
					</div></div>';
		if(!empty($context['insecure']))
			echo '
		<div class="info2" style="margin-top: 5px; clear: both; ">'. $context['insecure'] . '</div>';

		// Is the forum in maintenance mode?
		if ($context['in_maintenance'] && $context['user']['is_admin'])
			echo '
		<div class="info2" style="margin-top: 5px; clear: both;">'. $txt['maintain_mode_on'] . '</div>';
	}

	function theme_content_below()
	{
		global $context, $settings, $options, $scripturl, $txt, $modSettings;

		echo '	
						</div>
			</div>
		</div>';

		// Or perhaps the additional infobar?
		if (!empty($settings['extra_info']))
			echo '
		<div id="extrainfo_section">
			<div class="mainwidth">' , $this->theme_extrainfo() , '</div>
		</div>';

		// Show the "Powered by" and "Valid" logos, as well as the copyright. Remember, the copyright must be somewhere!
		echo '
		<div id="bottomfooter">
			<div class="mainwidth smalltext" style="padding: 1em;">
				', theme_copyright();

		// Show the load time?
		if ($context['show_load_time'])
			echo '
					| ', $txt['page_created'], $context['load_time'], $txt['seconds_with'], $context['load_queries'], $txt['queries'];

		echo '
			</div>
		</div>';
	}



	// Show the menu up top. Something like [home] [help] [profile] [logout]...
	function theme_menu()
	{
		global $context, $settings, $options, $scripturl, $txt;

		echo '
	<div id="topmenustyle">	
		<ul class="horiz_nav">';

		// Note: Menu markup has been cleaned up to remove unnecessary spans and classes.
		foreach ($context['menu_buttons'] as $act => $button)
		{
			if(!isset($button['top']))
				continue;

			echo '
			<li', !empty($button['sub_buttons']) ? ' class="subsections"' :'', '>
				<a class="', $button['active_button'] ? 'active ' : '', 'toplevel"', !empty($button['id']) ? ' id="' . $button['id'] . '"' : '',  !empty($button['href']) ? ' href="' . $button['href'] . '"' : '', ' ', isset($button['target']) ? 'target="' . $button['target'] . '"' : '', '>
					', $button['title'], '
				</a>';
			if (!empty($button['sub_buttons']))
			{
				echo '
				<ul class="horiz_nav">';

				foreach ($button['sub_buttons'] as $childbutton)
				{
					echo '
					<li ', !empty($childbutton['sub_buttons']) ? 'class="subsections"' :'', '>
						<a href="', $childbutton['href'], '" ' , isset($childbutton['target']) ? 'target="' . $childbutton['target'] . '"' : '', '>
							', $childbutton['title'], '
						</a>
					</li>';
				}
					echo '
				</ul>';
			}
			echo '
			</li>';
		}
		echo '
		</ul>
	</div>';
	}

}
?>