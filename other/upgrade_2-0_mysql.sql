/******************************************************************************/
--- Adding new member fields.
/******************************************************************************/

---# Adding Google.
ALTER TABLE {$db_prefix}members
ADD COLUMN google varchar(255) NOT NULL default '';
---#

---# Adding Twitter
ALTER TABLE {$db_prefix}members
ADD COLUMN twitter varchar(255) NOT NULL default '';
---#

---# Adding facebook.
ALTER TABLE {$db_prefix}members
ADD COLUMN facebook varchar(255) NOT NULL default '';
---#

---# Adding Skype.
ALTER TABLE {$db_prefix}members
ADD COLUMN skype varchar(255) NOT NULL default '';
---#

---# Adding Why Register column.
ALTER TABLE {$db_prefix}members
ADD COLUMN why_register tinytext NOT NULL;
---#

/******************************************************************************/
--- Adding new topic fields.
/******************************************************************************/

---# Adding action_type .
ALTER TABLE {$db_prefix}topics
ADD COLUMN action_type tinytext NOT NULL;
---#

---# Adding action_id .
ALTER TABLE {$db_prefix}topics
ADD COLUMN action_id int(11) NOT NULL;
---#

---# Adding progress .
ALTER TABLE {$db_prefix}topics
ADD COLUMN progress tinyint(4) NOT NULL DEFAULT '-1';
---#

/******************************************************************************/
--- Adding new board fields.
/******************************************************************************/

---# Adding boardtypes .
ALTER TABLE {$db_prefix}boards
ADD COLUMN id_plugin tinytext NOT NULL;
---#

---# Adding last topic id
ALTER TABLE {$db_prefix}boards
ADD COLUMN id_last_topic int(11) NOT NULL DEFAULT '0';
---#

---# Adding sections .
ALTER TABLE {$db_prefix}boards
ADD COLUMN id_section tinytext NOT NULL;
---#

---# Adding sections .
ALTER TABLE {$db_prefix}boards
ADD COLUMN show_on_index tinyint(4) NOT NULL DEFAULT '0';
---#

/******************************************************************************/
--- Adding new message fields.
/******************************************************************************/

---# Adding hiddentext.
ALTER TABLE {$db_prefix}messages
ADD COLUMN hiddentext text NOT NULL;
---#

/******************************************************************************/
--- Adding new attachment fields.
/******************************************************************************/

---# Adding id_micro.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN id_micro int(11) NOT NULL DEFAULT '0';
---#

---# Adding id_big.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN id_big int(11) NOT NULL DEFAULT '0';
---#

---# Adding id_big.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN exif text NOT NULL;
---#

/******************************************************************************/
--- various updates for Protendo
/******************************************************************************/

---# Some settings.
REPLACE INTO {$db_prefix}settings
	(variable, value)
VALUES
	('xmlnews_maxlen', '649'),
	('why_register_pattern', '0');
	('plugins_installed', '');
	('settings_updated', '0'),
	('last_mod_report_action', '0'),
	('search_floodcontrol_time', '5'),
	('next_task_time', UNIX_TIMESTAMP());
---#

---# Adding the table sections
CREATE TABLE  {$db_prefix}sections (
`id_section` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`title` TEXT NOT NULL ,
`boards` TEXT NOT NULL ,
`def_layout` text COLLATE utf8_unicode_ci NOT NULL,
`layout` text COLLATE utf8_unicode_ci NOT NULL,
`boardtypes` text COLLATE utf8_unicode_ci NOT NULL,
`show_in_menu` tinyint(4) NOT NULL,
`show_plugins` tinyint(4) NOT NULL,
`pluginorder` text COLLATE utf8_unicode_ci NOT NULL,
`titles` text COLLATE utf8_unicode_ci NOT NULL,
`preset` int(11) NOT NULL,
 PRIMARY KEY (`id_section`)
) ENGINE = MYISAM;

---#

---# Adding the table likes
CREATE TABLE {$db_prefix}likes (
  `id_likes` int(11) NOT NULL AUTO_INCREMENT,
  `id_msg` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `id_board` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  `who` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_likes`)
) ENGINE=MyISAM;
---#

CREATE TABLE IF NOT EXISTS {$db_prefix}boardtype_data (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `id_plugin` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id_topic` int(11) NOT NULL,
  `id_msg` int(11) NOT NULL,
  `value_str` text COLLATE utf8_unicode_ci NOT NULL,
  `value_int` int(11) NOT NULL,
  `changed` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `datatype` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id_member` int(11) NOT NULL,
  `is_integer` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

