/******************************************************************************/
--- Adding new member fields.
/******************************************************************************/

---# Adding Google.
ALTER TABLE {$db_prefix}members
ADD COLUMN google varchar(255) NOT NULL default '';
---#

---# Adding Twitter
ALTER TABLE {$db_prefix}members
ADD COLUMN twitter varchar(255) NOT NULL default '';
---#

---# Adding facebook.
ALTER TABLE {$db_prefix}members
ADD COLUMN facebook varchar(255) NOT NULL default '';
---#

---# Adding Skype.
ALTER TABLE {$db_prefix}members
ADD COLUMN skype varchar(255) NOT NULL default '';
---#

---# Adding Why Register column.
ALTER TABLE {$db_prefix}members
ADD COLUMN why_register tinytext NOT NULL;
---#

/******************************************************************************/
--- Adding new topic fields.
/******************************************************************************/

---# Adding action_type .
ALTER TABLE {$db_prefix}topics
ADD COLUMN action_type tinytext NOT NULL;
---#

---# Adding action_id .
ALTER TABLE {$db_prefix}topics
ADD COLUMN action_id int(11) NOT NULL;
---#

---# Adding progress .
ALTER TABLE {$db_prefix}topics
ADD COLUMN progress tinyint(4) NOT NULL DEFAULT '-1';
---#

/******************************************************************************/
--- Adding new board fields.
/******************************************************************************/

---# Adding boardtypes .
ALTER TABLE {$db_prefix}boards
ADD COLUMN id_plugin tinytext NOT NULL;
---#

---# Adding last topic id
ALTER TABLE {$db_prefix}boards
ADD COLUMN id_last_topic int(11) NOT NULL DEFAULT '0';
---#

---# Adding show_on_boardindex setting .
ALTER TABLE {$db_prefix}boards
ADD COLUMN show_on_index tinyint NOT NULL DEFAULT '0';
---#

/******************************************************************************/
--- Adding new message fields.
/******************************************************************************/



/******************************************************************************/
--- Adding new attachment fields.
/******************************************************************************/

---# Adding id_micro.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN id_micro int(11) NOT NULL DEFAULT '0';
---#

---# Adding id_big.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN id_big int(11) NOT NULL DEFAULT '0';
---#

---# Adding exif.
ALTER TABLE {$db_prefix}attachments
ADD COLUMN exif text NOT NULL;
---#

/******************************************************************************/
--- various updates for Protendo
/******************************************************************************/

---# Some setting updates.
REPLACE INTO {$db_prefix}settings
	(variable, value)
VALUES
	('xmlnews_maxlen', '649'),
	('why_register_pattern', '0');
	('plugins_installed', '');
	('settings_updated', '0'),
	('last_mod_report_action', '0'),
	('search_floodcontrol_time', '5'),
	('next_task_time', UNIX_TIMESTAMP());
---#

---# Adding boardtype_data table.
CREATE TABLE IF NOT EXISTS {$db_prefix}boardtype_data (
  id_data int(11) NOT NULL AUTO_INCREMENT,
  id_plugin tinytext NOT NULL,
  id_topic int(11) NOT NULL default '0',
  id_msg int(11) NOT NULL default '0',
  value_str text NOT NULL,
  value_int int(10) NOT NULL default '0',
  created int(10) NOT NULL default '0',
  changed int(10) NOT NULL default '0',
  id_member int(10) NOT NULL default '0',
  datatype tinytext NOT NULL,
  is_integer tinyint(3) NOT NULL default '0',
  PRIMARY KEY (id_data),
) ENGINE=MyISAM;
---#

