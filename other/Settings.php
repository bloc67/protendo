<?php

/**
 * @name		Protendo
 * @copyright  www.protendo.org
 * @version 1.0.1
 * copyright 2013-2016 BHK http://www.bjornhkristiansen.com
 * license http://www.protendo.org
 *
 * based on SMF
 * @author Simple Machines
 * copyright 2011 Simple Machines
 * license http://www.simplemachines.org/about/smf/license.php BSD
 */

$maintenance = 0;		
$mtitle = 'Maintenance Mode';		

$mmessage = 'Okay faithful users...we\'re attempting to restore an older backup of the database...news will be posted once we\'re back!';		
$mbname = 'My Community';		

$language = 'english';		

$language = 'english';		# The default language file set for the forum.
$boardurl = 'http://127.0.0.1/protendo';		# URL to your forum's folder. (without the trailing /!)
$atturl = $boardurl. '/attachments';		# URL to your forum's folder. (without the trailing /!)
$webmaster_email = 'noreply@myserver.com';		# Email address to send emails from. (like noreply@yourdomain.com.)
$cookiename = 'ProtendoCookie11';		# Name of the cookie to set for authentication.

########## Database Info ##########
$db_type = 'mysql';
$db_server = 'localhost';
$db_name = 'protendo';
$db_user = 'root';
$db_passwd = '';
$ssi_db_user = '';
$ssi_db_passwd = '';
$db_prefix = 'protendo_';
$db_persist = 0;
$db_error_send = 1;

// Note: These directories do not have to be changed unless you move things.
$boarddir = dirname(__FILE__);		# The absolute path to the forum's folder. (not just '.'!)
$plugindir = dirname(__FILE__) . '/Plugins';		# The absolute path to the forum's folder. (not just '.'!)
$sourcedir = dirname(__FILE__) . '/Sources';		# Path to the Sources directory.
$cachedir = dirname(__FILE__) . '/cache';		# Path to the cache directory.
// Note: You shouldn't touch these settings.
$db_last_error = 0;

if (file_exists(dirname(__FILE__) . '/install.php'))
{
	header('Location: http' . (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on' ? 's' : '') . '://' . (empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] . (empty($_SERVER['SERVER_PORT']) || $_SERVER['SERVER_PORT'] == '80' ? '' : ':' . $_SERVER['SERVER_PORT']) : $_SERVER['HTTP_HOST']) . (strtr(dirname($_SERVER['PHP_SELF']), '\\', '/') == '/' ? '' : strtr(dirname($_SERVER['PHP_SELF']), '\\', '/')) . '/install.php'); exit;
}

// Make sure the paths are correct... at least try to fix them.
if (!file_exists($boarddir) && file_exists(dirname(__FILE__) . '/agreement.txt'))
	$boarddir = dirname(__FILE__);
if (!file_exists($sourcedir) && file_exists($boarddir . '/Sources'))
	$sourcedir = $boarddir . '/Sources';
if (!file_exists($plugindir) && file_exists($boarddir . '/Plugins'))
	$plugindir = $boarddir . '/Plugins';
if (!file_exists($cachedir) && file_exists($boarddir . '/cache'))
	$cachedir = $boarddir . '/cache';

$db_character_set = 'utf8';

?>