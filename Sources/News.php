<?php

/**
 * @name			Protendo
 * @copyright		bjornhkristiansen.com
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo '<div style="border: red solid 1px; margin: 1em; padding: 1em; background: #fff5f5; color: #400;">You are not allowed to run this file.</div>'; die(); }

function News()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$context['get_blog_users'] = true;
	$txt['news'] = $modSettings['plugins']['news']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('news');
	$context['included_boards'] = implode(",",$b);
	initial_default('news');

	gettopics('news', '', 5, true, false,'300');
	loadtemplate('News');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'news';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('news',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'news';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('news',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'newsarticle' => array(
			'title' => $txt['newsarticle'],
			'value' => isset($context['hiddentext']['newsarticle']) ? $context['hiddentext']['newsarticle'] : 0,
			'data_type' => 'str',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	post2_default($topc, '', 'news');

	$html = file_get_contents($hiddentext['str']['newsarticle']);
	$title = findtaginhtml($html,'og:title', 'content');
	if(empty($title))
		$title = findtaginhtml($html,'<title>');
	updateboardtypevalue('news', $topc, 'newsarticle_title', $title, false);
	
	$text = findtaginhtml($html,'og:description', 'content');
	if(empty($text))
		$text = findtaginhtml($html,'name="description"', 'content');
	updateboardtypevalue('news', $topc, 'newsarticle_text', $text, false);
	
	$img = findtaginhtml($html,'og:image', 'content');
	if(empty($img))
		$img = '';
	
	if(!empty($img))
		updateboardtypevalue('news', $topc, 'newsarticle_image', $img, false);
}

?>