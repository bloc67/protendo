<?php

/**
 * @name			Protendo
 *
 * based on SMF
 * Simple Machines http://www.simplemachines.org
 * copyright 2011 Simple Machines
 * license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

class standard_search
{
	// This is the last version of PROTENDO that this was tested on, to protect against API changes.
	public $version_compatible = 'PROTENDO 1.0';

	// This won't work with versions of PROTENDO less than this.
	public $min_smf_version = 'PROTENDO 1.0';

	// Standard search is supported by default.
	public $is_supported = true;

	// Method to check whether the method can be performed by the API.
	public function supportsMethod($methodName, $query_params = null)
	{
		// Always fall back to the standard search method.
		return false;
	}
}

?>