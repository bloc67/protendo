<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function TPArticle()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	require_once($sourcedir. '/Plugins.php');
	loadtemplate('runtime/TPArticle');

	// what are we fetching.. a category, a single one, or the frontpage?
	if(!empty($_GET['c']) && is_numeric($_GET['c']))
	{
		$context['tpcats'] = TParticle_cats();
		$context['tparticles'] = TParticle_category($_GET['c']);
	}
	elseif(!empty($_GET['a']) && is_numeric($_GET['a']))
	{
		TParticle_article($_GET['a']);
	}
	else
	{

	}
}

function TParticle_category($id_cat)
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	// Find all articles, check for pages.
	$request = $smcFunc['db_query']('substring', '
			SELECT
				COUNT(*) as total 
			FROM {db_prefix}tp_articles AS tpa
				LEFT JOIN {db_prefix}tp_variables AS tpv ON (tpa.category = tpv.id)
				LEFT JOIN {db_prefix}members AS m ON (m.id_member = tpa.author_id)
			WHERE tpv.type = "category"
			AND tpa.category = ' . $id_cat . '
			'
		);
	
	if($smcFunc['db_num_rows']($request)>0)
		$total_blogs = $smcFunc['db_fetch_assoc']($request);
	else
		return;
	
	$smcFunc['db_free_result']($request);
}

function TParticle_cats()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$request = $smcFunc['db_query']('substring', '
		SELECT tpv.* FROM {db_prefix}tp_variables AS tpv
		WHERE tpv.type = "category"');
	
	$cats = array();
	if($smcFunc['db_num_rows']($request)>0)
	{	
		while($row = $smcFunc['db_fetch_assoc']($request))
			$cats[$row['id']] = $row;
	
		$smcFunc['db_free_result']($request);
	}
	return $cats;
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,10)=='tparticle_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=tparticle');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=tparticle' ,
		'title' => $txt['settings'],
		'values' => array(	
			array(
				'id' => 'tparticle_facebook',
				'label' => $txt['share_facebook'],
				'description' => $txt['share_facebook_descr'],
				'type' => 'checkbox',
				'value' => !empty($modSettings['article_facebook']) ? 1 : 0,
			),
			array(
				'id' => 'tparticle_twitter',
				'label' => $txt['share_twitter'],
				'type' => 'checkbox',
				'description' => $txt['share_twitter_descr'],
				'value' => !empty($modSettings['article_twitter']) ? 1 : 0,
			),
			array(
				'id' => 'tparticle_google',
				'label' => $txt['share_google'],
				'type' => 'checkbox',
				'description' => $txt['share_google_descr'],
				'value' => !empty($modSettings['article_google']) ? 1 : 0,
			),
			array(
				'id' => 'tparticle_pinterest',
				'label' => $txt['share_pinterest'],
				'type' => 'checkbox',
				'description' => $txt['share_pinterest_descr'],
				'value' => !empty($modSettings['article_pinterest']) ? 1 : 0,
			),
		),
	);
}

?>