<?php

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }


function FrontPage()
{
	global $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $scripturl, $settings;

	require_once($sourcedir . '/Plugins.php');
	$context['page_title'] = $context['forum_name'];
	$context['page_title_html_safe']	= $context['forum_name'];

	$context['sub_template'] = 'show_blocks';
	loadtemplate('Frontpage');
}

function community()
{
	global $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $scripturl, $settings;

	require_once($sourcedir . '/Plugins.php');

	// get all active plugins, all boards within and then 3 latest. Also the most popular 
	$request = $smcFunc['db_query']('substring', '
		SELECT
			b.id_board, b.name, b.num_topics, b.id_plugin
		FROM {db_prefix}boards AS b
		WHERE b.id_plugin != ""
			AND {query_wanna_see_board}
		ORDER BY b.id_plugin, b.board_order DESC',
		array(
		)
	);

	$context['plugs'] = array(); $ps = array(); 
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		$context['plugs'][$row['id_plugin']] = $row;
		$ps[] = $row['id_plugin'];
	}
	getmultipletopics($ps, 3, 300);
	$context['page_title'] = 'Community';
	$context['page_title_html_safe']	= 'Community';
	loadtemplate('Community');
}

function showstart()
{
}

function fronttopics($plugins = '', $limit = 5)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	if(empty($plugins) || (!empty($plugins) && $plugins == 'forum'))
		return;

	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$first = true; $latest = 0;
	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,mem.id_member, mem.avatar,
			SUBSTRING(m.body,1,500) as body, t.num_replies, t.num_views,b.id_plugin,
			IFNULL(mem.real_name, m.poster_name) AS poster_name ,
			IFNULL(a.id_attach, 0) AS ID_ATTACH, a.filename, a.attachment_type as attachmentType
		FROM {db_prefix}topics AS t
			LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
			LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
		WHERE b.id_plugin = "' . $plugins . '"
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
		ORDER BY t.id_first_msg DESC
		LIMIT ' . $limit ,
		array(
			'current_member' => $user_info['id'],
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'is_approved' => 1,
		)
	);
	$posts = array(); 
	
	$ids=array(); $ts=array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		// Censor the subject.
		censorText($row['subject']);

		if($first)
		{
			$latest = $row['id_msg'];
			$first = false;
		}
		
		$avatar = $row['avatar'] == '' ? ($row['ID_ATTACH'] > 0 ? (empty($row['attachmentType']) ? $scripturl . '?action=dlattach;attach=' . $row['ID_ATTACH'] . ';type=avatar' : $modSettings['custom_avatar_url'] . '/' . $row['filename']) : '') : (stristr($row['avatar'], 'http://') || stristr($row['avatar'], '@') ? $row['avatar'] : $modSettings['avatar_url'] . '/' . htmlspecialchars($row['avatar']));
		if(empty($avatar))
			$avatar = $settings['images_url'].'/guest.png';

		// Build the array.
		$posts[$row['id_topic']] = array(
			'board' => array(
				'id' => $row['id_board'],
				'id_plugin' => $row['id_plugin'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'topic' => $row['id_topic'],
			'poster' => array(
				'id' => $row['id_member'],
				'name' => $row['poster_name'],
				'href' => empty($row['id_member']) ? '' : $scripturl . '?action=profile;u=' . $row['id_member'],
				'link' => empty($row['id_member']) ? $row['poster_name'] : '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['poster_name'] . '</a>',
				'avatar' => $avatar,
			),
			'hiddentext' => array(),
			'replies' => $row['num_replies'],
			'views' => $row['num_views'],
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'body' => parse_bbc($row['body'].'...',true),
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'href' => $scripturl . '?topic=' . $row['id_topic'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '" rel="nofollow">' . $row['subject'] . '</a>',
		);
		if($user_info['is_guest'])
			$posts[$row['id_topic']]['new'] = false;
		
		$ids[$row['id_msg']] = $row['id_msg'];
		$ts[] = $row['id_topic'];
	}
	$smcFunc['db_free_result']($request);
	$img = attgallery_var($ids);
	foreach($posts as $p => $pp)
	{	
		if(isset($img[$pp['id_msg']]))
			$posts[$p]['att'] = $img[$pp['id_msg']];
	}
	return $posts;
}

function frontforumtopics($limit = 5)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;


	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$first = true; $latest = 0;
	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,mem.id_member, 
			t.num_replies, t.num_views,
			IFNULL(mem.real_name, m.poster_name) AS poster_name
		FROM {db_prefix}topics AS t
			LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
		WHERE b.id_plugin = ""
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
		ORDER BY t.id_first_msg DESC
		LIMIT ' . $limit ,
		array(
			'current_member' => $user_info['id'],
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'is_approved' => 1,
		)
	);
	$posts = array(); 
	
	$ids=array(); $ts=array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		// Censor the subject.
		censorText($row['subject']);

		if($first)
		{
			$latest = $row['id_msg'];
			$first = false;
		}
		
		// Build the array.
		$posts[$row['id_topic']] = array(
			'board' => array(
				'id' => $row['id_board'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'topic' => $row['id_topic'],
			'poster' => array(
				'id' => $row['id_member'],
				'name' => $row['poster_name'],
				'href' => empty($row['id_member']) ? '' : $scripturl . '?action=profile;u=' . $row['id_member'],
				'link' => empty($row['id_member']) ? $row['poster_name'] : '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['poster_name'] . '</a>',
			),
			'replies' => $row['num_replies'],
			'views' => $row['num_views'],
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'body' => parse_bbc($row['body'],true),
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'href' => $scripturl . '?topic=' . $row['id_topic'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '" rel="nofollow">' . $row['subject'] . '</a>',
		);
		$posts[$row['id_topic']]['new'] = false;
		
	}
	$smcFunc['db_free_result']($request);
	return $posts;
}


?>