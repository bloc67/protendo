<?php

/**
 * @name		Protendo
 * @copyright  www.protendo.org
 * @version 1.0.1
 * copyright 2013-2016 BHK http://www.bjornhkristiansen.com
 * license http://www.protendo.org
 *
 * based on SMF
 * @author Simple Machines
 * copyright 2011 Simple Machines
 * license http://www.simplemachines.org/about/smf/license.php BSD
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

// Show the board index!
function BoardIndex()
{
	global $options, $plugindir, $txt, $user_info, $sourcedir, $modSettings, $context, $settings, $scripturl;

	loadTemplate('BoardIndex');


	// Set a canonical URL for this page.
	$context['canonical_url'] = $scripturl . '?action=forum';

	// Do not let search engines index anything if there is a random thing in $_GET.
	if (!empty($_GET))
		$context['robot_no_index'] = true;

	// some options, first categories as tabs!
	if(!empty($options['boardindex_tabs']))
		$context['miniprofile_settings'][] = array('setting' => 'boardindex_tabs', 'name' => $txt['option_cat_tabs_not'], 'url' => 'action=forum');
	else
		$context['miniprofile_settings'][] = array('setting' => 'boardindex_tabs', 'name' => $txt['option_cat_tabs'],'url' => 'action=forum');
	
	// then showing RSS icons/links
	if(!empty($options['boardindex_rss']))
		$context['miniprofile_settings'][] = array('setting' => 'boardindex_rss', 'name' => $txt['hide_rss'], 'url' => 'action=forum');
	else
		$context['miniprofile_settings'][] = array('setting' => 'boardindex_rss', 'name' => $txt['show_rss'],'url' => 'action=forum');

	// Retrieve the categories and boards.
	require_once($sourcedir . '/Subs-BoardIndex.php');
	if(!empty($_GET['cat']) && is_numeric($_GET['cat']))
		$context['single_category'] = $_GET['cat'];
		
	$boardIndexOptions = array(
		'include_categories' => true,
		'base_level' => 0,
		'parent_id' => 0,
		'set_latest_post' => true,
		'countChildPosts' => !empty($modSettings['countChildPosts']),
		'single_category' => !empty($context['single_category']) ? $context['single_category'] : 0,
	);

	$context['categories'] = getBoardIndex($boardIndexOptions);

	// Get the user online list.
	require_once($sourcedir . '/Subs-MembersOnline.php');
	$membersOnlineOptions = array(
		'show_hidden' => allowedTo('moderate_forum'),
		'sort' => 'log_time',
		'reverse_sort' => true,
	);
	$context += getMembersOnlineStats($membersOnlineOptions);

	$context['show_buddies'] = !empty($user_info['buddies']);

	// Are we showing all membergroups on the board index?
	if (!empty($settings['show_group_key']))
		$context['membergroups'] = cache_quick_get('membergroup_list', 'Subs-Membergroups.php', 'cache_getMembergroupList', array());

	// Track most online statistics? (Subs-MembersOnline.php)
	if (!empty($modSettings['trackStats']))
		trackStatsUsersOnline($context['num_guests'] + $context['num_spiders'] + $context['num_users_online']);

	// Retrieve the latest posts if the theme settings require it.
	if (isset($settings['number_recent_posts']) && $settings['number_recent_posts'] > 1)
	{
		$latestPostOptions = array(
			'number_posts' => $settings['number_recent_posts'],
		);
		$context['latest_posts'] = cache_quick_get('boardindex-latest_posts:' . md5($user_info['query_wanna_see_board'] . $user_info['language']), 'Subs-Recent.php', 'cache_getLastPosts', array($latestPostOptions));
	}

	$settings['display_recent_bar'] = !empty($settings['number_recent_posts']) ? $settings['number_recent_posts'] : 0;
	$settings['show_member_bar'] &= allowedTo('view_mlist');
	$context['show_stats'] = allowedTo('view_stats') && !empty($modSettings['trackStats']);
	$context['show_member_list'] = allowedTo('view_mlist');
	$context['show_who'] = allowedTo('who_view') && !empty($modSettings['who_enabled']);

	// Load the calendar?
	if (!empty($modSettings['cal_enabled']) && allowedTo('calendar_view'))
	{
		// Retrieve the calendar data (events, birthdays, holidays).
		$eventOptions = array(
			'include_holidays' => $modSettings['cal_showholidays'] > 1,
			'include_birthdays' => $modSettings['cal_showbdays'] > 1,
			'include_events' => $modSettings['cal_showevents'] > 1,
			'num_days_shown' => empty($modSettings['cal_days_for_index']) || $modSettings['cal_days_for_index'] < 1 ? 1 : $modSettings['cal_days_for_index'],
		);
		$context += cache_quick_get('calendar_index_offset_' . ($user_info['time_offset'] + $modSettings['time_offset']), 'Subs-Calendar.php', 'cache_getRecentEvents', array($eventOptions));

		// Whether one or multiple days are shown on the board index.
		$context['calendar_only_today'] = $modSettings['cal_days_for_index'] == 1;

		// This is used to show the "how-do-I-edit" help.
		$context['calendar_can_edit'] = allowedTo('calendar_edit_any');
	}
	else
		$context['show_calendar'] = false;

	$context['page_title'] = $context['forum_name'] . ' - ' . $txt['forum'];

	// add to the linktree
	$context['linktree'][] = array(
				'url' => $scripturl . '?action=forum',
				'name' => $txt['forum'],
			);

	// any mods/plugins must be run now
	if(isset($context['plugin']['boardindex']))
	{
		global $plugindir;

		foreach($context['plugin']['boardindex'] as $plug => $data)
		{
			require_once($plugindir.'\\'. $data['plugin'] . '\\' . $data['source_file'].'.php');			
			require_once($plugindir.'\\'. $data['plugin'] . '\\' . $data['template_file'].'.template.php');			
			call_user_func($data['source_function']);			
		}
	}
	
	$context['boardindex_buttons']=array();
	
	// Show the mark all as read button?
	if ($context['user']['is_logged'])
	{
		if ($settings['show_mark_read'])
			// Mark read button.
			$context['boardindex_buttons']['markread'] = array('text' => 'mark_as_read', 'image' => 'markread.gif', 'lang' => true, 'url' => $scripturl . '?action=markasread;sa=all;' . $context['session_var'] . '=' . $context['session_id']);
	}
	
	addButtons('boardindex','boardindex_buttons');

	// fetch the things for the fader
	require_once($sourcedir. '/Plugins.php');
	$quicknews=true;
	
	if(!empty($settings['show_newsfader']))
	{
		$context['user_fader']=true;
		$context['fader_options'] = array(
			'duration' => 4000,
			'container_id' => 'news_fader',
			'items_class' => '.items',
			'quicknews' => $quicknews,
			);
		inlineCSS('.toc','newsfader_height', 'margin-top', ($settings['newsfader_height']+30).'px;', '380px;' );
	}
}

function template_main()
{
	global $context, $settings, $txt;

	createThemeObject('BoardIndex');
	$context['subthemeobject']->theme_main();
}

// Collapse or expand a category
function CollapseCategory()
{
	global $user_info, $sourcedir, $context;

	// Just in case, no need, no need.
	$context['robot_no_index'] = true;

	checkSession('request');

	if (!isset($_GET['sa']))
		fatal_lang_error('no_access', false);

	// Check if the input values are correct.
	if (in_array($_REQUEST['sa'], array('expand', 'collapse', 'toggle')) && isset($_REQUEST['c']))
	{
		// And collapse/expand/toggle the category.
		require_once($sourcedir . '/Subs-Categories.php');
		collapseCategories(array((int) $_REQUEST['c']), $_REQUEST['sa'], array($user_info['id']));
	}

	// And go back to the board index.
	BoardIndex();
}

?>