<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function VersionDisplay()
{
	global $context;

	loadtemplate('Plugins');
	$context['sub_template'] = 'versiondisplay';
}

function gettopics($plugin, $brd = '', $limit = 0, $return = false, $plugin_gallery=false, $textcount = 0)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	$context['current_plugin'] = $plugin;
	$context['start'] = (!empty($_GET['start']) && is_numeric($_GET['start'])) ? $_GET['start'] : 0;
	$bstring = '';	

	// if speficied a year...within that year then
	if(!empty($_GET['y']) && is_numeric($_GET['y']))
	{
		$starttime = mktime(0,0,0,1,1,$_GET['y']);
		$endtime = mktime(23,59,59,12,31,$_GET['y']);

		if(!empty($_GET['m']) && is_numeric($_GET['m']))
		{
			$starttime = mktime(0,0,0,$_GET['m'],1,$_GET['y']);
			$endtime = mktime(23,59,59,$_GET['m'],(cal_days_in_month(CAL_GREGORIAN,$_GET['m'],$_GET['y'])), $_GET['y']);
		}
		$bstring .= ' AND m.poster_time < '. $endtime . ' AND m.poster_time > '. $starttime;
	}

	if(!empty($_GET['author']) && is_numeric($_GET['author']))
		$bstring .= ' AND m.id_member = '. $_GET['author'];

	if(!empty($brd) && is_numeric($brd))
		$bstring .= ' AND b.id_board =' . $brd;

	if(isset($_GET['param']) && is_numeric($_GET['param']) && isset($context['db_param'][$_GET['param']]))
	{
		$param = ';param='. $_GET['param'];
		$extratable = '
						LEFT JOIN {db_prefix}boardtype_data AS btd ON (t.id_topic = btd.id_topic)
		';
		$bstring .= ' AND btd.datatype ="' . $context['db_param'][$_GET['param']]['datatype'].'" AND btd.value_str ="' . $context['db_param'][$_GET['param']]['str'].'" ';
	}

	// Set the variables up for the template.
	$context['can_mark_notify'] = allowedTo('mark_notify') && !$user_info['is_guest'];
	$context['can_post_new'] = allowedTo('post_new') || ($modSettings['postmod_active'] && allowedTo('post_unapproved_topics'));
	$context['can_moderate_forum'] = allowedTo('moderate_forum');
	$context['can_approve_posts'] = allowedTo('approve_posts');

	if(!empty($context['userblog_id']))
		$bstring .= ' AND m.id_member = '. $context['userblog_id'] ;
	
	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
			SELECT
				COUNT(*) as total 
			FROM {db_prefix}topics AS t
				LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
				LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
				' . (!empty($extratable) ? $extratable : '' ) . '
			WHERE b.id_plugin = "' . $plugin . '"
				AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
				AND t.approved = {int:is_approved}
				AND m.approved = {int:is_approved}' : '') . '
				' . $bstring . '
			ORDER BY t.id_first_msg DESC
			',
			array(
				'is_approved' => 1,
			)
		);
	
	if($smcFunc['db_num_rows']($request)>0)
		$total_blogs = $smcFunc['db_fetch_assoc']($request);
	else
		return;
	
	$smcFunc['db_free_result']($request);
	
	if(empty($total_blogs['total']))
		return;

	if(empty($context['start']))
		$context['start'] = 0;

	if(empty($limit))
	{
		if(!empty($brd))
			$context['page_index'] = constructPageIndex($h = $scripturl . '?board=' . $brd . (!empty($param) ? $param : '') . ';start=%1$d', $context['start'] , $total_blogs['total'], 10, true);
		else
			$context['page_index'] = constructPageIndex($h = $scripturl . '?action=' . $plugin . (!empty($param) ? $param : '') . ';start=%1$d', $context['start'] , $total_blogs['total'], 10, true);
		
		$context['start'] = $_REQUEST['start'];
	}
	else
	{
		if(!empty($brd))
			$context['page_index'] = constructPageIndex($h = $scripturl . '?board=' . $brd . (!empty($param) ? $param : '') . ';start=%1$d', $context['start'] , $total_blogs['total'], $limit, true);
		else
			$context['page_index'] = constructPageIndex($h = $scripturl . '?action=' . $plugin . (!empty($param) ? $param : '') . ';start=%1$d', $context['start'] , $total_blogs['total'], $limit, true);
		
		$context['start'] = $_REQUEST['start'];
	}
	
	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$first = true; $latest = 0;
	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,mem.id_member, mem.avatar,
			SUBSTRING(m.body,1,' . (!empty($textcount) ? $textcount : '1800') . ') as body, t.num_replies, t.num_views,b.id_section,b.id_plugin,
			' . ($user_info['is_guest'] ? '0' : 'IFNULL(lt.id_msg, IFNULL(lmr.id_msg, -1)) + 1') . ' AS new_from,
			IFNULL(mem.real_name, m.poster_name) AS poster_name ,ml.id_msg_modified,
			IFNULL(a.id_attach, 0) AS ID_ATTACH, a.filename, a.attachment_type as attachmentType
		FROM {db_prefix}topics AS t
			INNER JOIN {db_prefix}messages AS ml ON (ml.id_msg = t.id_last_msg)
			LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)' . ($user_info['is_guest'] ? '' : '
			LEFT JOIN {db_prefix}log_topics AS lt ON (lt.id_topic = t.id_topic AND lt.id_member = {int:current_member})
			LEFT JOIN {db_prefix}log_mark_read AS lmr ON (lmr.id_board = b.id_board AND lmr.id_member = {int:current_member})'). '		
			LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
			' . (!empty($extratable) ? $extratable : '' ) . '
		WHERE b.id_plugin = "' . $plugin . '"
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
			' . $bstring . '
		ORDER BY t.id_first_msg DESC
		LIMIT ' . (empty($context['start']) ? '0' : $context['start']) . ', ' . (!empty($limit) ? $limit : '10') ,
		array(
			'current_member' => $user_info['id'],
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'min_message_id' => $modSettings['maxMsgID'] - 35 * min(15, 5),
			'is_approved' => 1,
		)
	);
	$context['front'. ($return ? $plugin : 'blog')] = array(); 
	
	$ids=array(); $ts=array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		// Censor the subject.
		censorText($row['subject']);

		if($first)
		{
			$latest = $row['id_msg'];
			$first = false;
		}
		
		$avatar = $row['avatar'] == '' ? ($row['ID_ATTACH'] > 0 ? (empty($row['attachmentType']) ? $scripturl . '?action=dlattach;attach=' . $row['ID_ATTACH'] . ';type=avatar' : $modSettings['custom_avatar_url'] . '/' . $row['filename']) : '') : (stristr($row['avatar'], 'http://') || stristr($row['avatar'], '@') ? $row['avatar'] : $modSettings['avatar_url'] . '/' . htmlspecialchars($row['avatar']));
		if(empty($avatar))
			$avatar = $settings['images_url'].'/guest.png';

		// Build the array.
		$context['front'. ($return ? $plugin : 'blog')][$row['id_topic']] = array(
			'board' => array(
				'id' => $row['id_board'],
				'id_plugin' => $row['id_plugin'],
				'id_section' => $row['id_section'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'topic' => $row['id_topic'],
			'poster' => array(
				'id' => $row['id_member'],
				'name' => $row['poster_name'],
				'href' => empty($row['id_member']) ? '' : $scripturl . '?action=profile;u=' . $row['id_member'],
				'link' => empty($row['id_member']) ? $row['poster_name'] : '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['poster_name'] . '</a>',
				'avatar' => $avatar,
			),
			'hiddentext' => array(),
			'replies' => $row['num_replies'],
			'views' => $row['num_views'],
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'body' => parse_bbc($row['body'],true),
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'href' => $scripturl . '?topic=' . $row['id_topic'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '" rel="nofollow">' . $row['subject'] . '</a>',
			'new' => $row['new_from'] <= $row['id_msg_modified'],
			'new_from' => $row['new_from'],
			'new_href' => $scripturl . '?topic=' . $row['id_topic'] . '.msg' . $row['new_from'] . '#new',
		);
		if(!empty($_GET['author']) && is_numeric($_GET['author']))		
			$context['current_blog_author'] = $row['poster_name'];

		if($user_info['is_guest'])
			$context['front'. ($return ? $plugin : 'blog')][$row['id_topic']]['new'] = false;
		
		$ids[$row['id_msg']] = $row['id_msg'];
		$ts[] = $row['id_topic'];
		if(!empty($brd) && is_numeric($brd))
		{
			$context['current_boardname'] = $row['board_name'];
			$context['current_board'] = $row['id_board'];
		}
	}
	$smcFunc['db_free_result']($request);
	
	// get any boardtype data
	$request = $smcFunc['db_query']('', 'SELECT * FROM {db_prefix}boardtype_data WHERE id_topic IN (' . implode(",",$ts) .')');
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($brow = $smcFunc['db_fetch_assoc']($request))
		{
			if($brow['is_integer']>0)
				$context['front'. ($return ? $plugin : 'blog')][$brow['id_topic']]['hiddentext'][$brow['datatype']] = $brow['value_int'];
			else
				$context['front'. ($return ? $plugin : 'blog')][$brow['id_topic']]['hiddentext'][$brow['datatype']] = $brow['value_str'];
		}
		$smcFunc['db_free_result']($request);
	}

	if(!$plugin_gallery)
		attgallery($ids, $return);
	else
		attgallery_gallery($ids, $return, $plugin_gallery);

	// check if we need some replies here
	if(!empty($context['blog_fetch_replies']))
	{
		// first, get all ids by msg from the topic id's
		$request = $smcFunc['db_query']('', '
		SELECT
			m.id_topic, m.id_msg
		FROM {db_prefix}messages AS m
		WHERE m.id_topic IN (' . implode(",",$ts) . ')
		ORDER BY m.id_msg DESC');

		$tm=array();
		if($smcFunc['db_num_rows']($request)>0)
		{
			while ($row = $smcFunc['db_fetch_assoc']($request))
			{
				if(!isset($tm[$row['id_topic']]))
					$tm[$row['id_topic']] = array();
				
				$tm[$row['id_topic']][] = $row['id_msg'];
			}
			$smcFunc['db_free_result']($request);
		}		
		// keep only 3 of each
		$tms = array(); 
		foreach($tm as $t => $data)
			$tms[$t] = array_slice($tm[$t], 0, 3); 
		
		$msgs = array();
		foreach($tms as $a)
		{
			foreach($a as $b)
				$msgs[] = $b; 
		}

		if(count($msgs)>0)
		{
			// fetch the messages of those 3 of each
			$request = $smcFunc['db_query']('', '
			SELECT
				m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,mem.id_member, mem.avatar,	m.body, 
				IFNULL(mem.real_name, m.poster_name) AS poster_name ,m.poster_time,
				IFNULL(a.id_attach, 0) AS ID_ATTACH, a.filename, a.attachment_type as attachmentType
			FROM {db_prefix}messages AS m
				LEFT JOIN {db_prefix}topics AS t ON (m.id_topic = t.id_topic)
				LEFT JOIN {db_prefix}boards AS b ON (b.id_board = m.id_board)
				LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
				LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
			WHERE m.id_msg IN (' . implode(",",$msgs) . ')
				ORDER BY m.id_msg DESC');
			
			$context['frontblog_replies'] = array();
			if($smcFunc['db_num_rows']($request)>0)
			{
				while ($row = $smcFunc['db_fetch_assoc']($request))
				{
					$avatar = $row['avatar'] == '' ? ($row['ID_ATTACH'] > 0 ? (empty($row['attachmentType']) ? $scripturl . '?action=dlattach;attach=' . $row['ID_ATTACH'] . ';type=avatar' : $modSettings['custom_avatar_url'] . '/' . $row['filename']) : '') : (stristr($row['avatar'], 'http://') || stristr($row['avatar'], '@') ? $row['avatar'] : $modSettings['avatar_url'] . '/' . htmlspecialchars($row['avatar']));
					if(empty($avatar))
						$avatar = $settings['images_url'].'/guest.png';
					$row['avvy'] = $avatar;
					$row['body'] = parse_bbc($row['body']);
					$row['time'] = timeformat($row['poster_time']);
					if(!isset($context['frontblog_replies'][$row['id_topic']]))
						$context['frontblog_replies'][$row['id_topic']] = array();
					$context['frontblog_replies'][$row['id_topic']][$row['id_msg']] = $row;
				}
				$smcFunc['db_free_result']($request);
			}		
		}
	}
	
	// need to try the cache first on this..
	if(!empty($context['get_blog_mostread']))
	{
		if (($context['blogmostread'] = cache_get_data('getblogmostread'.$plugin)) == null)
		{
			$request = $smcFunc['db_query']('substring', '
					SELECT t.id_topic, m.subject, t.num_views, m.id_member, mem.real_name  
					FROM {db_prefix}topics AS t
						LEFT JOIN {db_prefix}boards AS b ON (t.id_board = b.id_board)
						LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
						LEFT JOIN {db_prefix}members AS mem ON (m.id_member = mem.id_member)
					WHERE b.id_plugin = "' . $plugin . '"
						AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
						AND t.approved = {int:is_approved}
						AND m.approved = {int:is_approved}' : '') . '
						' . $bstring . '
					ORDER BY t.num_views DESC
					LIMIT 10',
					array(
						'is_approved' => 1,
					)
				);
			$context['blogmostread'] = array();
			if($smcFunc['db_num_rows']($request)>0)
			{
				while($row = $smcFunc['db_fetch_assoc']($request))
					$context['blogmostread'][] = $row;

				$smcFunc['db_free_result']($request);
				
			}
			cache_put_data('getblogmostread'.$plugin, $context['blogmostread']);
		}
	}	
	// need to try the cache first on this..
	if(!empty($context['get_blog_history']) && !empty($_GET['author']) && is_numeric($_GET['author']))
	{
		$request = $smcFunc['db_query']('substring', '
					SELECT m.poster_time  
					FROM {db_prefix}topics AS t
						LEFT JOIN {db_prefix}boards AS b ON (t.id_board = b.id_board)
						LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
					WHERE b.id_plugin = "' . $plugin . '"
						AND m.id_member = ' . $_GET['author'] . '
						AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
						AND t.approved = {int:is_approved}
						AND m.approved = {int:is_approved}' : '') . '
						' . $bstring . '
					ORDER BY m.poster_time DESC
					LIMIT 150',
					array(
						'is_approved' => 1,
					)
				);
		$context['bloghistory'] = array();
		if($smcFunc['db_num_rows']($request)>0)
		{
			while($row = $smcFunc['db_fetch_assoc']($request))
			{
				$year = date("Y",$row['poster_time']);
				$month = date("n",$row['poster_time']);
				if(!isset($context['bloghistory'][$year]))
					$context['bloghistory'][$year] = array();

				$context['bloghistory'][$year][$month] = 1;
			}
			$smcFunc['db_free_result']($request);
		}
	}	
	return $latest;
}

function getmultipletopics($plugs = array(), $limit = 0, $textcount = 0)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	$bstring = '';	
	
	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$request = $smcFunc['db_query']('substring', '
		SELECT
			MAX(m.id_msg) as id_msg, b.id_plugin
		FROM {db_prefix}messages AS m
			LEFT JOIN {db_prefix}topics AS t ON (m.id_msg = t.id_topic)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
			LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
		WHERE b.id_plugin IN ("' . implode('","', $plugs) . '")
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
			' . $bstring . '
			GROUP BY b.id_plugin
		'  ,
		array(
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'is_approved' => 1,
		)
	);
	$pp = array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$pp[] = $row['id_msg'];
	
	$smcFunc['db_free_result']($request);

	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,mem.id_member, mem.avatar,
			SUBSTRING(m.body,1,' . (!empty($textcount) ? $textcount : '1800') . ') as body, t.num_replies, t.num_views,b.id_section,b.id_plugin,
			IFNULL(mem.real_name, m.poster_name) AS poster_name,
			IFNULL(a.id_attach, 0) AS ID_ATTACH, a.filename, a.attachment_type as attachmentType
		FROM {db_prefix}messages AS m
			LEFT JOIN {db_prefix}topics AS t ON (m.id_msg = t.id_topic)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
			LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
		WHERE m.id_msg IN ("' . implode('","', $pp) . '")
		ORDER BY 1'
	);
	
	$context['multipost'] = $plugs; 
	$ids=array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		// Censor the subject.
		censorText($row['subject']);

		$avatar = $row['avatar'] == '' ? ($row['ID_ATTACH'] > 0 ? (empty($row['attachmentType']) ? $scripturl . '?action=dlattach;attach=' . $row['ID_ATTACH'] . ';type=avatar' : $modSettings['custom_avatar_url'] . '/' . $row['filename']) : '') : (stristr($row['avatar'], 'http://') || stristr($row['avatar'], '@') ? $row['avatar'] : $modSettings['avatar_url'] . '/' . htmlspecialchars($row['avatar']));
		if(empty($avatar))
			$avatar = $settings['images_url'].'/guest.png';

		// Build the array.
		$context['multipost'][$row['id_plugin']] = array(
			'board' => array(
				'id' => $row['id_board'],
				'id_plugin' => $row['id_plugin'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'topic' => $row['id_topic'],
			'poster' => array(
				'id' => $row['id_member'],
				'name' => $row['poster_name'],
				'href' => empty($row['id_member']) ? '' : $scripturl . '?action=profile;u=' . $row['id_member'],
				'link' => empty($row['id_member']) ? $row['poster_name'] : '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['poster_name'] . '</a>',
				'avatar' => $avatar,
			),
			'replies' => $row['num_replies'],
			'views' => $row['num_views'],
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'body' => parse_bbc($row['body'],true),
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'href' => $scripturl . '?topic=' . $row['id_topic'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '" rel="nofollow">' . $row['subject'] . '</a>',
		);
		$ids[$row['id_msg']] = $row['id_msg'];
	}
	$smcFunc['db_free_result']($request);
	attgallery($ids);
	return;
}

function getboards($plugin)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	$b=array();
	// Find all the boards
	$request = $smcFunc['db_query']('', '
		SELECT b.id_board, b.name, b.num_topics 
		FROM {db_prefix}boards as b
		WHERE b.id_plugin = "' . $plugin . '"
			AND {query_wanna_see_board}
			ORDER BY b.id_last_msg DESC
			LIMIT 15');

	$context['blogboards'] = array();
	
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($row = $smcFunc['db_fetch_assoc']($request))
		{
			$context['blogboards'][] = $row;
			$b[] = $row['id_board'];
		}
		$smcFunc['db_free_result']($request);
	}
	// can we create new posts then?
	// we need to compare, if any is allowed the button is needed
	$allowedboards = boardsAllowedTo('can_post_new');
	$can_post = false;
	foreach($b as $brd)
	{
		if(in_array($brd,$allowedboards))
		{
			$can_post = true;
			break;
		}
	}
	// finally
	if($can_post || $user_info['is_admin'])
		$context['normal_buttons'] = array(
			'new_topic' => array('text' => 'new_topic', 'lang' => true, 'url' => $scripturl . '?action=post;plugin=' . $plugin));
	
	return $b;
}

function moveElement(&$array, $a, $b) {
    $out = array_splice($array, $a, 1);
    array_splice($array, $b, 0, $out);
}

function getusers($plugin)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	$b=array();
	// Find all the boards
	$request = $smcFunc['db_query']('', '
		SELECT b.id_board, b.name, b.num_topics 
		FROM {db_prefix}boards as b
		WHERE b.id_plugin = "' . $plugin . '"
			AND {query_wanna_see_board}
			ORDER BY b.id_last_msg DESC
			LIMIT 15');

	$context['blogboards'] = array();
	
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($row = $smcFunc['db_fetch_assoc']($request))
		{
			$context['blogboards'][] = $row;
			$b[] = $row['id_board'];
		}
		$smcFunc['db_free_result']($request);
	}
	// can we create new posts then?
	// we need to compare, if any is allowed the button is needed
	$allowedboards = boardsAllowedTo('can_post_new');
	$can_post = false;
	foreach($b as $brd)
	{
		if(in_array($brd,$allowedboards))
		{
			$can_post = true;
			break;
		}
	}
	// finally
	if($can_post || $user_info['is_admin'])
		$context['normal_buttons'] = array(
			'new_topic' => array('text' => 'new_topic', 'lang' => true, 'url' => $scripturl . '?action=post;plugin=' . $plugin));
	
	return $b;
}

/* ############################*/

function getoriginal($t, $att = 0)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $topicinfo;

	if(empty($topicinfo))
	{
		// Get all the important topic info.
		$request = $smcFunc['db_query']('', '
			SELECT
				t.num_replies, t.num_views, t.locked, t.is_sticky,
				t.id_member_started, t.id_first_msg, t.id_last_msg, t.approved, t.unapproved_posts
			FROM {db_prefix}topics AS t
			WHERE t.id_topic = ' . $t . '
			LIMIT 1'
		);
		if ($smcFunc['db_num_rows']($request) == 0)
			fatal_lang_error('not_a_topic', false);
		$topicinfo = $smcFunc['db_fetch_assoc']($request);
		$smcFunc['db_free_result']($request);
	}
	$context['start']=0;
		
	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$attached = array();

	// Fetch attachments.
	if (!empty($modSettings['attachmentEnable']) && allowedTo('view_attachments'))
	{
		$request = $smcFunc['db_query']('', '
			SELECT
				a.id_attach, a.id_folder, a.id_msg, a.filename, a.file_hash, IFNULL(a.size, 0) AS filesize, a.downloads, a.approved, a.id_thumb,a.id_micro,a.id_big,a.exif,
				micro.filename as micro_filename, big.filename as big_filename,
				a.width, a.height' . (empty($modSettings['attachmentShowImages']) || empty($modSettings['attachmentThumbnails']) ? '' : ',
				IFNULL(thumb.id_attach, 0) AS id_thumb, thumb.width AS thumb_width, thumb.height AS thumb_height, thumb.filename as thumb_filename') . '
			FROM {db_prefix}attachments AS a' . (empty($modSettings['attachmentShowImages']) || empty($modSettings['attachmentThumbnails']) ? '' : '
				LEFT JOIN {db_prefix}attachments AS thumb ON (thumb.id_attach = a.id_thumb)
				LEFT JOIN {db_prefix}attachments AS micro ON (micro.id_attach = a.id_micro)
				LEFT JOIN {db_prefix}attachments AS big ON (big.id_attach = a.id_big)
				') . '
			WHERE a.id_msg = '. $topicinfo['id_first_msg'] . '
				AND a.attachment_type = {int:attachment_type}',
			array(
				'attachment_type' => 0,
				'is_approved' => 1,
			)
		);
		$temp = array();
		while ($row = $smcFunc['db_fetch_assoc']($request))
		{
			if(!empty($row['exif']))
				$row['exif'] = unserialize($row['exif']);
			
			if (!$row['approved'] && $modSettings['postmod_active'] && !allowedTo('approve_posts') && (!isset($all_posters[$row['id_msg']]) || $all_posters[$row['id_msg']] != $user_info['id']))
				continue;

			$temp[$row['id_attach']] = $row;

			if (!isset($attached[$row['id_msg']]))
				$attached[$row['id_msg']] = array();
		}
		$smcFunc['db_free_result']($request);

		// This is better than sorting it with the query...
		ksort($temp);

		foreach ($temp as $row)
			$attached[$row['id_msg']][] = $row;
	}

	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, b.name AS board_name,
			m.body, t.num_replies,
			IFNULL(mem.real_name, m.poster_name) AS poster_name, 
			mem.id_member, mem.date_registered, mem.posts, mem.buddy_list, mem.website_title, mem.website_url, 
			mem.google, mem.skype, mem.facebook, mem.twitter,mem.member_ip, mem.member_ip2,
			mem.avatar,
			IFNULL(a.id_attach, 0) AS ID_ATTACH, a.filename, a.attachment_type as attachmentType
		FROM {db_prefix}topics AS t
			LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
			LEFT JOIN {db_prefix}members AS mem ON (mem.id_member = m.id_member)
			LEFT JOIN {db_prefix}attachments AS a ON (a.id_member = mem.id_member)
		WHERE m.id_topic = ' . $t . '
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
		LIMIT 1',
		array(
			'current_member' => $user_info['id'],
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'is_approved' => 1,
		)
	);
	$context['frontblog'] = array(); $ids=array();
	$context['singleblog'] = true;
	$row = $smcFunc['db_fetch_assoc']($request);

	// Censor the subject.
	censorText($row['subject']);

	$avatar = $row['avatar'] == '' ? ($row['ID_ATTACH'] > 0 ? (empty($row['attachmentType']) ? $scripturl . '?action=dlattach;attach=' . $row['ID_ATTACH'] . ';type=avatar' : $modSettings['custom_avatar_url'] . '/' . $row['filename']) : '') : (stristr($row['avatar'], 'http://') || stristr($row['avatar'], '@') ? $row['avatar'] : $modSettings['avatar_url'] . '/' . htmlspecialchars($row['avatar']));
	if(empty($avatar))
		$avatar = $settings['images_url'].'/guest.png';

	// Build the array.
	$context['frontblog'][] = array(
			'attachment' => loadAttachmentContext($row['id_msg'], $attached),
			'board' => array(
				'id' => $row['id_board'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'topic' => $row['id_topic'],
			'poster' => array(
				'id' => $row['id_member'],
				'avatar' => $avatar,
				'posts' => $row['posts'],
				'date_registered' => $row['date_registered'],
				'is_buddy' => in_array($context['user']['id'],explode(",",$row['buddy_list'])),
				'website_url' => $row['website_url'],
				'website_title' => $row['website_title'],
				'google' => 'https://plus.google.com/' . $row['google'],
				'twitter' => 'https://twitter.com/' . urlencode($row['twitter']),
				'facebook' => 'http://www.facebook.com/' . $row['facebook'],
				'skype' => 'skype:'. urlencode($row['skype']). '?userinfo',
				'name' => $row['poster_name'],
				'href' => empty($row['id_member']) ? '' : $scripturl . '?action=profile;u=' . $row['id_member'],
				'link' => empty($row['id_member']) ? $row['poster_name'] : '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['poster_name'] . '</a>'
			),
			'replies' => $row['num_replies'],
			'hiddentext' => array(),
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'body' => parse_bbc($row['body'],true),
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'href' => $scripturl . '?topic=' . $row['id_topic'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '">' . $row['subject'] . '</a>',
		);
	

	$context['user']['started'] = $row['id_member'];
	$ids[] = $row['id_msg'];
	$smcFunc['db_free_result']($request);
	
	if(empty($att))
		attgallery($ids);
	else
		attsingle($att);

	$context['page_title'] = $context['forum_name'] . ' - ' . $row['subject'];

	$buttons = array();
	
	$common_permissions = array(
		'can_approve' => 'approve_posts',
		'can_ban' => 'manage_bans',
		'can_sticky' => 'make_sticky',
		'can_merge' => 'merge_any',
		'can_split' => 'split_any',
		'can_mark_notify' => 'mark_any_notify',
		'can_send_topic' => 'send_topic',
		'can_send_pm' => 'pm_send',
		'can_report_moderator' => 'report_any',
		'can_moderate_forum' => 'moderate_forum',
		'can_issue_warning' => 'issue_warning',
		'can_restore_topic' => 'move_any',
		'can_restore_msg' => 'move_any',
	);
	foreach ($common_permissions as $contextual => $perm)
		$context[$contextual] = allowedTo($perm);

	// Permissions with _any/_own versions.  $context[YYY] => ZZZ_any/_own.
	$anyown_permissions = array(
		'can_move' => 'move',
		'can_lock' => 'lock',
		'can_delete' => 'remove',
		'can_reply' => 'post_reply',
		'can_reply_unapproved' => 'post_unapproved_replies',
	);
	foreach ($anyown_permissions as $contextual => $perm)
		$context[$contextual] = allowedTo($perm . '_any') || ($context['user']['started'] && allowedTo($perm . '_own'));

	$context['can_mark_notify'] &= !$context['user']['is_guest'];
	$context['can_sticky'] &= !empty($modSettings['enableStickyTopics']);
	$context['can_reply'] &= empty($topicinfo['locked']) || allowedTo('moderate_board');
	$context['can_reply_unapproved'] &= $modSettings['postmod_active'] && (empty($topicinfo['locked']) || allowedTo('moderate_board'));
	$context['can_issue_warning'] &= in_array('w', $context['admin_features']) && $modSettings['warning_settings'][0] == 1;
	$context['can_reply_approved'] = $context['can_reply'];
	$context['can_reply'] |= $context['can_reply_unapproved'];
	$context['can_quote'] = $context['can_reply'] && (empty($modSettings['disabledBBC']) || !in_array('quote', explode(',', $modSettings['disabledBBC'])));
	$context['can_mark_unread'] = !$user_info['is_guest'] && $settings['show_mark_read'];

	$context['can_modify'] = (allowedTo('moderate_board') && allowedTo('modify_any') || (allowedTo('modify_own') && $row['id_member'] == $user_info['id']));

	$context['can_send_topic'] = (!$modSettings['postmod_active'] || $topicinfo['approved']) && allowedTo('send_topic');

	// Start this off for quick moderation - it will be or'd for each post.
	$context['can_remove_post'] = allowedTo('delete_any') || (allowedTo('delete_replies') && $context['user']['started']);

	$context['current_topic'] = $t;
	
	// okay, then we go forward with the normal ones...	
	$context['blogtype_buttons'] = array(
		'reply' => array('test' => 'can_reply', 'text' => 'reply', 'image' => 'reply.gif', 'lang' => true, 'url' => $scripturl . '?action=post;topic=' . $context['current_topic'] . '.' . $context['start']),
		'notify' => array('test' => 'can_mark_notify', 'text' => $context['is_marked_notify'] ? 'unnotify' : 'notify', 'image' => ($context['is_marked_notify'] ? 'un' : '') . 'notify.gif', 'lang' => true, 'custom' => 'onclick="return confirm(\'' . ($context['is_marked_notify'] ? $txt['notification_disable_topic'] : $txt['notification_enable_topic']) . '\');"', 'url' => $scripturl . '?action=notify;sa=' . ($context['is_marked_notify'] ? 'off' : 'on') . ';topic=' . $context['current_topic'] . '.' . $context['start'] . ';' . $context['session_var'] . '=' . $context['session_id']),
		'mark_unread' => array('test' => 'can_mark_unread', 'text' => 'mark_unread', 'image' => 'markunread.gif', 'lang' => true, 'url' => $scripturl . '?action=markasread;sa=topic;t=' . $context['mark_unread_time'] . ';topic=' . $context['current_topic'] . '.' . $context['start'] . ';' . $context['session_var'] . '=' . $context['session_id'],'active' => true),
		'send' => array('test' => 'can_send_topic', 'text' => 'send_topic', 'image' => 'sendtopic.gif', 'lang' => true, 'url' => $scripturl . '?action=emailuser;sa=sendtopic;topic=' . $context['current_topic'] . '.0'),
		'print' => array('text' => 'print', 'image' => 'print.gif', 'lang' => true, 'custom' => 'rel="new_win nofollow"', 'url' => $scripturl . '?action=printpage;topic=' . $context['current_topic'] . '.0'),
	);
	if($context['is_marked_notify'])
		$context['blogtype_buttons']['notify']['active'] = true;

	// get any boardtype data
	$request = $smcFunc['db_query']('', 'SELECT * FROM {db_prefix}boardtype_data WHERE id_topic = ' . $row['id_topic']);
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($brow = $smcFunc['db_fetch_assoc']($request))
		{
			if($brow['is_integer']>0)
				$context['frontblog'][0]['hiddentext'][$brow['datatype']] = $brow['value_int'];
			else
				$context['frontblog'][0]['hiddentext'][$brow['datatype']] = $brow['value_str'];
		}
		$smcFunc['db_free_result']($request);
	}
}

/* ############################*/

function do_default_Display($plug, $t)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir;

	$bigplug = strtoupper(substr($plug,0,1)) . substr($plug,1);
	$context['current_plugin'] = $plug;
	getoriginal($t);
	return;
}

/* ############################*/

function do_default_MessageIndex($plug, $t)
{
	global $board,$context;

	$context['current_plugin'] = $plug;
	$context['included_boards'] = $board;
	gettopics($plug, $board);
}

/* ############################*/

// setting up the plugin
function initial_default($plug, $limit = 0)
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	// overides?
	if(!empty($settings[$plug.'title']))
		$txt[$plug] = $settings[$plug. 'title'];
	
	$context['linktree'][] = array(
		'name' => $txt[$plug],	
		'url' => $scripturl.'?action='.$plug,	
	);

	$context['page_title'] = $context['forum_name'] . ' - ' . $txt[$plug];
	if(!empty($_GET['u']) &&  is_numeric($_GET['u']))
		$context['userblog_id'] = $_GET['u'];

	if($limit > 0)
		$latest = gettopics($plug, '', $limit);
	else
		$latest = gettopics($plug);

	// can we admin the plugin section? basically the typical settings for it
	$context['is_' . $plug .'_admin'] = allowedTo('moderate_forum');
	return $latest;
}

/* ############################*/
function recentgallery($plug, $start, $brd = 0, $not_image = false, $include_topic = false)
{
	global $smcFunc, $context, $modSettings, $scripturl, $txt, $settings;

	$attachment_ext = array('jpg','png','jpeg');
	$start = isset($_GET['start']) && is_numeric($_GET['start']) ? $_GET['start'] : 0;
	
	// Lets sample how many first.
	$request = $smcFunc['db_query']('', '
		SELECT
			COUNT(att.id_attach) as total
		FROM {db_prefix}attachments AS att
			INNER JOIN {db_prefix}messages AS m ON (m.id_msg = att.id_msg)
			INNER JOIN {db_prefix}topics AS t ON (t.id_topic = m.id_topic)
			INNER JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
		WHERE b.id_plugin="' . $plug . '"
			' . (!empty($brd) ? 'AND b.id_board='.$brd : '') . '
			' . (!$not_image ?
			'AND att.width>0
			AND att.height>0
			AND att.attachment_type = 0
			AND att.fileext IN ({array_string:attachment_ext})' : '') .
			(!$modSettings['postmod_active'] || allowedTo('approve_posts') ? '' : '
			AND t.approved = 1
			AND m.approved = 1
			AND att.approved = 1') . '
			AND t.id_first_msg = m.id_msg
		',
		array(
			'attachment_ext' => $attachment_ext,
		)
	);
	$total = $smcFunc['db_fetch_assoc']($request);
	$smcFunc['db_free_result']($request);

	if(empty($brd))
		$context['page_index'] = constructPageIndex($scripturl . '?action=' . $plug . ';start=%1$d', $start, $total['total'], 25, true);
	else
		$context['page_index'] = constructPageIndex($scripturl . '?board=' . $brd . ';start=%1$d', $start, $total['total'], 25, true);

	// Lets build the query.
	$request = $smcFunc['db_query']('', '
		SELECT
			att.fileext, att.downloads, att.id_attach, att.id_msg, att.filename,t.id_topic, m.subject, SUBSTRING(m.body, 1, 185) AS preview,t.num_replies, t.num_views,
			att.width, att.height' . 
			(empty($modSettings['attachmentShowImages']) || empty($modSettings['attachmentThumbnails']) ? '' : ', IFNULL(thumb.id_attach, 0) AS id_thumb, thumb.width AS thumb_width, thumb.height AS thumb_height,
			thumb.filename as thumb_filename,micro.filename as micro_filename,big.filename as big_filename
			') . '
			' . ($include_topic ? ', SUBSTRING(m.body,1,400) as body ' :	'' ) . '
		FROM {db_prefix}attachments AS att
			INNER JOIN {db_prefix}messages AS m ON (m.id_msg = att.id_msg)
			INNER JOIN {db_prefix}topics AS t ON (t.id_topic = m.id_topic)
			LEFT JOIN {db_prefix}attachments AS thumb ON (thumb.id_attach = att.id_thumb)
			LEFT JOIN {db_prefix}attachments AS micro ON (micro.id_attach = att.id_micro)
			LEFT JOIN {db_prefix}attachments AS big ON (big.id_attach = att.id_big)
			INNER JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
		WHERE b.id_plugin = "' . $plug . '"
			' . (!empty($brd) ? 'AND b.id_board='.$brd : '') . '
			' . (!$not_image ?
			'AND att.width>0
			AND att.height>0
			AND att.attachment_type = 0
			AND att.fileext IN ({array_string:attachment_ext})' : '') .
			(!$modSettings['postmod_active'] || allowedTo('approve_posts') ? '' : '
			AND t.approved = 1
			AND m.approved = 1
			AND att.approved = 1') . '
			AND t.id_first_msg = m.id_msg
		ORDER BY att.id_attach DESC
		LIMIT ' . $start. ',25',
		array(
			'attachment_ext' => $attachment_ext,
		)
	);

	$context['topics_used'] = array();
	
	// We have something.
	// if we need the topics, merge it differently..
	$context['gallery_items'] = array(); $temp = array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		$filename = preg_replace('~&amp;#(\\d{1,7}|x[0-9a-fA-F]{1,6});~', '&#\\1;', htmlspecialchars($row['filename']));
		$att = strpos($filename, ".");
		if(!$include_topic)
		{
			$context['gallery_items'][$row['id_attach']] = array(
				'file' => array(
					'filename' => $filename,
					'href' => $scripturl . '?action=dlattach;topic=' . $row['id_topic'] . '.0;attach=' . $row['id_attach'],
					'id' => $row['id_attach'],
					'downloads' => $row['downloads'],
					'fileext' => $row['fileext'],
				),
				'topic' => array(
					'id' => $row['id_topic'],
					'href' => $scripturl . '?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . ';att=' . $row['id_attach'] . '#msg' . $row['id_msg'],
					'subject' => $row['subject'], 
					'preview' => strip_tags(parse_bbc($row['preview'],false)), 
					'replies' => $row['num_replies'], 
					'views' => $row['num_views'], 
				),
			);

			if(!empty($row['width']))
			{
				$id_thumb = empty($row['id_thumb']) ? $row['id_attach'] : $row['id_thumb'];
				$context['gallery_items'][$row['id_attach']]['file']['image'] = array(
						'id' => $id_thumb,
						'width' => $row['width'],
						'height' => $row['height'],
						'img' => '<img src="' . $atturl . '/' . $row['filename'] . '" />',
						'thumb' => '<img src="' . $atturl . '/' . $row['thumb_filename'] . '" alt="' . $filename . '" />',
						'href' => $atturl . '/' . $row['filename'],
						'link' => '<a href="' . $atturl . '/' . $row['filename'] . '"><img src="' . $atturl . '/' . $row['thumb_filename'] . '" alt="' . $filename . '" /></a>',
						'big_href' => $atturl . '/' . $row['filename'],
					);
			}
		}
		else
		{
			if(!isset($context['gallery_items'][$row['id_topic']]))
				$context['gallery_items'][$row['id_topic']] = array(
					'atts' => array(),
					'topic' => array(
						'id' => $row['id_topic'],
						'href' => $scripturl . '?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . ';att=' . $row['id_attach'] . '#msg' . $row['id_msg'],
						'subject' => $row['subject'], 
						'preview' => strip_tags(parse_bbc($row['preview'],false)), 
						'replies' => $row['num_replies'], 
						'views' => $row['num_views'], 
						'body' => parse_bbc($row['body']),
					),
				);

			$context['gallery_items'][$row['id_topic']]['atts'][$row['id_attach']] = array(
				'file' => array(
					'filename' => $filename,
					'href' => $atturl . '/' . $row['filename'],
					'id' => $row['id_attach'],
					'downloads' => $row['downloads'],
					'fileext' => $row['fileext'],
				),
			);

			if(!empty($row['width']))
			{
				$id_thumb = empty($row['id_thumb']) ? $row['id_attach'] : $row['id_thumb'];
				$context['gallery_items'][$row['id_topic']]['atts'][$row['id_attach']]['file']['image'] = array(
						'id' => $id_thumb,
						'width' => $row['width'],
						'height' => $row['height'],
						'img' => '<img src="' . $atturl . '/' . $row['filename'] . '" alt="' . $filename . '" />',
						'thumb' => '<img src="' . $atturl . '/' . $row['thumb_filename'] . '" alt="' . $filename . '" />',
						'href' => $atturl . '/' . $row['thumb_filename'],
						'link' => '<a href="' . $atturl . '/' . $row['filename'] . '"><img src="' . $atturl . '/' . $row['thumb_filename'] . '" alt="' . $filename . '" /></a>',
						'big_href' => $atturl . '/' . $row['filename'],
					);
			}
		}
		$context['topics_used'][$row['id_topic']] = $row['id_topic'];
	}
	$smcFunc['db_free_result']($request);

	
	// Set the variables up for the template.
	$context['can_post_new'] = allowedTo('post_new') || ($modSettings['postmod_active'] && allowedTo('post_unapproved_topics'));
	$context['can_moderate_forum'] = allowedTo('moderate_forum');
	$context['can_approve_posts'] = allowedTo('approve_posts');
}

function ModifyPluginSettings()
{
	global $txt, $settings, $modSettings, $context;

	// Format: 'sub-action' => array('function', 'permission')
	$subActions = array(
		'install' => array('PluginInstall', 'admin_forum'),
	);

	isAllowedTo('admin_forum');
	PluginInstall();
	loadtemplate('Plugins');
}

function RuntimePluginManage()
{
	global $context, $settings, $modSettings, $scripturl, $sourcedir, $boarddir, $txt;

	$context['page_title'] = $context['forum_name'] . ' - '.$txt['runtime_plugins'] ;
	$installed = array();
	if(isset($modSettings['runtime']))
	{
		foreach($modSettings['runtime'] as $plu => $data)
			$installed[] = $plu;
	}

	// check for changes
	if(!empty($_POST['plugin_install']))
	{
		checksession('post');
		$saved = array(); $names = array(); $actions=array(); $sections = array();
		
		foreach($_POST as $what => $where)
		{
			if(substr($what, 0,17)=='plugin_installed_')
			{
				$whatID = substr($what, 17);
				$saved[] = $whatID;
				$names[$whatID] = $_POST['plugin_name_' . $whatID];
				if(!empty($modSettings['plugins'][$whatID]['menu']['title']))
					$modSettings['plugins'][$whatID]['menu']['title'] = $names[$whatID];

				$modSettings['plugins'][$whatID]['menu']['hide'] = !empty($_POST['plugin_hide_' . $whatID]) ? 1 : 0;
			}
			elseif(substr($what, 0,12)=='plugin_sect_')
			{
				$whatID = substr($what, 12);
				if(!empty($_POST['plugin_sect_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sect_' . $whatID]] = array(
					'action' => $_POST['plugin_sect_' . $whatID],
					'name' => $_POST['plugin_sectname_' . $whatID],
				);
			}
			elseif(substr($what, 0,14)=='plugin_sectx1_')
			{

				$whatID = substr($what, 14);
				if(!empty($_POST['plugin_sectx1_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sectx1_' . $whatID]] = array(
					'action' => $_POST['plugin_sectx1_' . $whatID],
					'name' => $_POST['plugin_sectnamex1_' . $whatID],
				);
			}
			elseif(substr($what, 0,14)=='plugin_sectx2_')
			{
				$whatID = substr($what, 14);
				if(empty($_POST['plugin_sectx2_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sectx2_' . $whatID]] = array(
					'action' => $_POST['plugin_sectx2_' . $whatID],
					'name' => $_POST['plugin_sectnamex2_' . $whatID],
				);
			}
		}
		$installed=$saved;
	}

	$items = bloqs_listing('Plugins/Runtime', 'xml');

	// build the setting up
	$loaded = array();

	$context['runtime_available'] = array();
	foreach($items as $xml)
	{
		$temp = xml2array(file_get_contents($xml['path'].'/'.$xml['filename']));
		$context['runtime_available'][$xml['id']] = $temp['runtimeplugin'];

		if(in_array($context['runtime_available'][$xml['id']]['id'], $installed))
		{
			$context['runtime_available'][$xml['id']]['title'] = isset($modSettings['runtime'][$xml['id']]['menu']['title']) ? $modSettings['runtime'][$xml['id']]['menu']['title'] : $names[$xml['id']];
			$context['runtime_available'][$xml['id']]['installed'] = 1;
			$context['runtime_available'][$xml['id']]['hide'] = !empty($modSettings['runtime'][$xml['id']]['menu']['hide']) ? 1 : 0;
			// build it up
			$loaded[$xml['id']] = array(
				'sourcefile' => $context['runtime_available'][$xml['id']]['sourcefile'],
				'descr' => $context['runtime_available'][$xml['id']]['descr'],
				'function' => $context['runtime_available'][$xml['id']]['function'],
				'action' => !empty($actions[$xml['id']]) ? $actions[$xml['id']] : $xml['id'],
				'menu' => array(
					'title' => !empty($modSettings['runtime'][$xml['id']]['menu']['title']) ? $modSettings['runtime'][$xml['id']]['menu']['title'] : $names[$xml['id']],
					'top' => true,
					'href' => $scripturl . '?action='. (!empty($actions[$xml['id']]) ? $actions[$xml['id']] : $xml['id']),
					'hide' => !empty($modSettings['runtime'][$xml['id']]['menu']['hide']) ? 1 : 0,
				),
			);
		}
		else
			$context['runtime_available'][$xml['id']]['installed'] = 0;
	
	}
	if(isset($saved))
	{
		// set the modSetting
		$modSettings['runtime'] = $loaded;
		$change = array(
			'runtime_installed' => serialize($loaded),
			'runsections_added' => serialize($sections),
		);
		updateSettings($change, false);
	}
	loadtemplate('Plugins');
	$context['sub_template'] = 'runtimeplugs';
}

function PluginInstall()
{
	global $context, $settings, $modSettings, $scripturl, $sourcedir, $boarddir, $txt;

	$context['page_title'] = $context['forum_name'] . ' - '.$txt['boardtypes'] ;
	$installed = array();
	if(isset($modSettings['plugins']))
	{
		foreach($modSettings['plugins'] as $plu => $data)
			$installed[] = $plu;
	}

	// check for changes
	if(!empty($_POST['plugin_install']))
	{
		checksession('post');
		$saved = array(); $names = array(); $actions=array(); $sections = array();
		
		foreach($_POST as $what => $where)
		{
			if(substr($what, 0,17)=='plugin_installed_')
			{
				$whatID = substr($what, 17);
				$saved[] = $whatID;
				$names[$whatID] = $_POST['plugin_name_' . $whatID];
				if(!empty($modSettings['plugins'][$whatID]['menu']['title']))
					$modSettings['plugins'][$whatID]['menu']['title'] = $names[$whatID];

				$modSettings['plugins'][$whatID]['menu']['hide'] = !empty($_POST['plugin_hide_' . $whatID]) ? 1 : 0;
			}
			elseif(substr($what, 0,12)=='plugin_sect_')
			{
				$whatID = substr($what, 12);
				if(!empty($_POST['plugin_sect_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sect_' . $whatID]] = array(
					'action' => $_POST['plugin_sect_' . $whatID],
					'name' => $_POST['plugin_sectname_' . $whatID],
				);
			}
			elseif(substr($what, 0,14)=='plugin_sectx1_')
			{

				$whatID = substr($what, 14);
				if(!empty($_POST['plugin_sectx1_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sectx1_' . $whatID]] = array(
					'action' => $_POST['plugin_sectx1_' . $whatID],
					'name' => $_POST['plugin_sectnamex1_' . $whatID],
				);
			}
			elseif(substr($what, 0,14)=='plugin_sectx2_')
			{
				$whatID = substr($what, 14);
				if(empty($_POST['plugin_sectx2_' . $whatID]))
					$sections[$whatID][$_POST['plugin_sectx2_' . $whatID]] = array(
					'action' => $_POST['plugin_sectx2_' . $whatID],
					'name' => $_POST['plugin_sectnamex2_' . $whatID],
				);
			}
		}
		$installed=$saved;
	}

	$items = bloqs_listing('Plugins/Boardtypes', 'xml');

	// build the setting up
	$loaded = array();

	$context['plugins_available'] = array();
	foreach($items as $xml)
	{
		$temp = xml2array(file_get_contents($xml['path'].'/'.$xml['filename']));
		$context['plugins_available'][$xml['id']] = $temp['boardtype'];

		if(in_array($context['plugins_available'][$xml['id']]['id'], $installed))
		{
			$context['plugins_available'][$xml['id']]['title'] = isset($modSettings['plugins'][$xml['id']]['menu']['title']) ? $modSettings['plugins'][$xml['id']]['menu']['title'] : $names[$xml['id']];
			$context['plugins_available'][$xml['id']]['installed'] = 1;
			$context['plugins_available'][$xml['id']]['hide'] = !empty($modSettings['plugins'][$xml['id']]['menu']['hide']) ? 1 : 0;
			// build it up
			$loaded[$xml['id']] = array(
				'sourcefile' => $context['plugins_available'][$xml['id']]['sourcefile'],
				'descr' => $context['plugins_available'][$xml['id']]['descr'],
				'function' => $context['plugins_available'][$xml['id']]['function'],
				'action' => !empty($actions[$xml['id']]) ? $actions[$xml['id']] : $xml['id'],
				'menu' => array(
					'title' => !empty($modSettings['plugins'][$xml['id']]['menu']['title']) ? $modSettings['plugins'][$xml['id']]['menu']['title'] : $names[$xml['id']],
					'top' => true,
					'href' => $scripturl . '?action='. (!empty($actions[$xml['id']]) ? $actions[$xml['id']] : $xml['id']),
					'hide' => !empty($modSettings['plugins'][$xml['id']]['menu']['hide']) ? 1 : 0,
				),
			);
		}
		else
			$context['plugins_available'][$xml['id']]['installed'] = 0;
	
	}
	if(isset($saved))
	{
		// set the modSetting
		$modSettings['plugins'] = $loaded;
		$change = array(
			'plugins_installed' => serialize($loaded),
			'plugsections_added' => serialize($sections),
		);
		updateSettings($change, false);
	}
	$context['sub_template'] = 'boardtypes';
}

function bloqs_listing($folder, $type, $usefolder = false)
{
	global $boarddir, $boardurl;

	if($usefolder)
		$path = $folder;
	else
		$path = $boarddir. '/' .$folder;

	$dir = dir($path);
	$entries = array();
	while ($entry = $dir->read())
		$entries[] = $entry;
	$dir->close();

	natcasesort($entries);
	$listing1 = array();

	foreach ($entries as $entry)
	{
		// Skip all dot files, including .htaccess.
		if (substr($entry, strlen($entry)-4, 4) != '.'.$type || is_dir($path . '/' . $entry))
			continue;

		$listing1[] = array(
			'filename' => $entry,
			'id' => strtolower(substr($entry, 0, strlen($entry)-4)),
			'is_writable' => is_writable($path . '/' . $entry),
			'last_modified' => timeformat(filemtime($path . '/' . $entry)),
			'path' => $path,
		);
	}
	return $listing1;
}

function post2_default($topc=0, $fields = array(), $plugin ='')
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	$data = array();
	// determine if its a new topic, or existing being edited. In that case: is it first message?
	foreach($fields as $plug)
	{
		if(isset($_POST['plugin_'.$plug]))
			$data[] = $plug . '="' . $_POST['plugin_'.$plug].'"';
	}
	return;
}

function getusertopics($plugin, $limit = 0, $user)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc,$atturl;

	$context['start']=0;
	$bstring = '';	

	if(!empty($user) && is_numeric($user))
		$bstring .= 'AND m.id_member = '. $user;
	else
		return;

	if (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0)
		$exclude_boards = array($modSettings['recycle_board']);
	else
		$exclude_boards = array();

	$first = true; $latest = 0;
	// Find all the posts in distinct topics.  Newer ones will have higher IDs.
	$request = $smcFunc['db_query']('substring', '
		SELECT
			m.poster_time, m.subject, m.id_topic, m.id_msg, b.id_board, IFNULL(att.id_micro,NULL) as id_micro, t.num_replies,b.name as board_name
		FROM {db_prefix}topics AS t
			LEFT JOIN {db_prefix}attachments AS att ON (t.id_first_msg = att.id_msg AND att.attachment_type=0 AND att.mime_type != "" )
			LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			LEFT JOIN {db_prefix}boards AS b ON (b.id_board = t.id_board)
		WHERE b.id_plugin = "' . $plugin . '"
			' . (empty($exclude_boards) ? '' : '
			AND b.id_board NOT IN ({array_int:exclude_boards})') . '
			AND {query_wanna_see_board}' . ($modSettings['postmod_active'] ? '
			AND t.approved = {int:is_approved}
			AND m.approved = {int:is_approved}' : '') . '
			' . $bstring . '
		ORDER BY t.id_first_msg DESC
		LIMIT ' . (!empty($limit) ? $limit : '10') ,
		array(
			'current_member' => $user_info['id'],
			'exclude_boards' => empty($exclude_boards) ? '' : $exclude_boards,
			'min_message_id' => $modSettings['maxMsgID'] - 35 * min(15, 5),
			'is_approved' => 1,
		)
	);
	$context['frontuserblog'] = array(); 
	
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		// Censor the subject.
		censorText($row['subject']);
		// Build the array.
		$context['frontuserblog'][$row['id_topic']] = array(
			'board' => array(
				'id' => $row['id_board'],
				'name' => $row['board_name'],
				'href' => $scripturl . '?board=' . $row['id_board'],
				'link' => '<a href="' . $scripturl . '?board=' . $row['id_board'] . '">' . $row['board_name'] . '</a>'
			),
			'micro' => empty($row['id_micro']) ? $settings['images_url'].'/noimage.png'  : $atturl . '/' . $row['micro_filename'],
			'topic' => $row['id_topic'],
			'replies' => $row['num_replies'],
			'subject' => $row['subject'],
			'id_msg' => $row['id_msg'],
			'short_subject' => shorten_subject($row['subject'], 25),
			'time' => timeformat($row['poster_time']),
			'timestamp' => forum_time(true, $row['poster_time']),
			'new_href' => $scripturl . '?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . '#new',
		);
		if($user_info['is_guest'])
			$context['frontuserblog'][$row['id_topic']]['new'] = false;
		
	}
	$smcFunc['db_free_result']($request);
	return;
}


function FrontpageSettings()
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	isAllowedTo('manage_forum');

	// we need the fresh frontapge blokcs..not the cached ones..
	$modSettings['frontpage'] = unserialize(getFreshSettings('frontpage_settings'));

	
	// a subaction?
	if(!empty($_GET['sa']))
		$sa = $_GET['sa'];
	else
		$sa = '';

	$context['linktree'][] = array(
		'name' => $txt['frontpage'],	
		'url' => $scripturl.'?action=admin;area=frontpage',	
	);
	// have we added any blocks then?
	if(isset($_GET['addblock']))
	{
		checksession('get');
		$modSettings['frontpage']['blocks'][] = array(
			'plugin' => '',
			'type' => '',
			'title' => '',
			'number' => '',
			'layout' => '',
			'code' => '',
			'width' => '',
		);
		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change,true);
		redirectexit('action=admin;area=frontpage');
	}
	// move the block
	if(isset($_GET['moveblock']))
	{
		checksession('get');
		if(is_numeric($_GET['moveblock']))
		{
			if(isset($modSettings['frontpage']['blocks'][$_GET['moveblock']]))
			{	
				if($_GET['moveblock']>0)
					moveElement($modSettings['frontpage']['blocks'], $_GET['moveblock'], $_GET['moveblock']-1);
			}
		}	
		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change,true);
		redirectexit('action=admin;area=frontpage');
	}
	// move the block forward
	if(isset($_GET['moveblock']))
	{
		checksession('get');
		if(is_numeric($_GET['moveblockf']))
		{
			if(isset($modSettings['frontpage']['blocks'][$_GET['moveblockf']]))
			{	
				if($_GET['moveblockf'] < count($modSettings['frontpage']['blocks']))
					moveElement($modSettings['frontpage']['blocks'], $_GET['moveblockf'], $_GET['moveblockf']+1);
			}
		}	
		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change,true);
		redirectexit('action=admin;area=frontpage');
	}

	// clear all?
	if(isset($_GET['clearall']))
	{
		checksession('get');
		$modSettings['frontpage']['blocks'] = array();
		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change,true);
		redirectexit('action=admin;area=frontpage');
	}

	// delete one?
	if(isset($_GET['delblock']) && is_numeric($_GET['delblock']))
	{
		checksession('get');
		
		if(isset($modSettings['frontpage']['blocks'][$_GET['delblock']]))
			unset($modSettings['frontpage']['blocks'][$_GET['delblock']]);
		
		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change,true);
		redirectexit('action=admin;area=frontpage');
	}


	if(!empty($_POST['frontpagesub']))
	{
		checksession('post');
		$modSettings['frontpage']['choice'] = 3; 	
		// go through the POST
		foreach($_POST as $key => $val)
		{
			if(substr($key,0,19) == 'front_block_plugin_')
				$modSettings['frontpage']['blocks'][substr($key,19)]['plugin'] = $val;				
			elseif(substr($key,0,19) == 'front_block_number_')
				$modSettings['frontpage']['blocks'][substr($key,19)]['number'] = $val;				
			elseif(substr($key,0,18) == 'front_block_width_')
				$modSettings['frontpage']['blocks'][substr($key,18)]['width'] = $val;				
			elseif(substr($key,0,19) == 'front_block_layout_')
				$modSettings['frontpage']['blocks'][substr($key,19)]['layout'] = $val;				
			elseif(substr($key,0,17) == 'front_block_type_')
				$modSettings['frontpage']['blocks'][substr($key,17)]['type'] = $val;				
			elseif(substr($key,0,18) == 'front_block_title_')
				$modSettings['frontpage']['blocks'][substr($key,18)]['title'] = $val;				
			elseif(substr($key,0,17) == 'front_block_code_')
				$modSettings['frontpage']['blocks'][substr($key,17)]['code'] = $val;				
		}

		$change = array('frontpage_settings' => serialize($modSettings['frontpage']));
		updateSettings($change);
		redirectexit('action=admin;area=frontpage');
	}
	loadtemplate('Plugins');
	$context['sub_template'] = 'frontpage_options';
	$context['page_title'] = $context['forum_name'] . ' - ' . $txt['frontpage'] .' ' . $txt['settings'];
	$context['page_title_html_safe'] = $context['forum_name'] . ' - ' . $txt['frontpage'] .' ' . $txt['settings'];
}

function PluginSettings()
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir;

	isAllowedTo('manage_forum');

	// a subaction?
	if(!empty($_GET['sa']))
		$sa = $_GET['sa'];
	else
		$sa = '';

	$context['linktree'][] = array(
		'name' => $txt['boardtypes'],	
		'url' => $scripturl.'?action=admin;area=boardtypes',	
	);

	$context['linktree'][] = array(
		'name' => $txt['settings'],	
		'url' => $scripturl.'?action=admin;area=boardtypesettings',	
	);
	if(empty($sa))
	{
		loadtemplate('Plugins');
		$context['sub_template'] = 'boardtypesettings';
	}
	else
	{
		loadtemplate('Plugins');
		require_once($sourcedir . '/boardtypes/'. strtoupper(substr($sa,0,1)).substr($sa,1). '.php');
		
		if(function_exists('BSettings'))
			BSettings();
		
		loadtemplate('boardtypes/'. strtoupper(substr($sa,0,1)).substr($sa,1));
		$context['sub_template'] = 'bsettings';
	}
}

function RuntimePluginSettings()
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir;

	isAllowedTo('manage_forum');

	// a subaction?
	if(!empty($_GET['sa']))
		$sa = $_GET['sa'];
	else
		$sa = '';

	$context['linktree'][] = array(
		'name' => $txt['runtime'],	
		'url' => $scripturl.'?action=admin;area=runtime',	
	);

	$context['linktree'][] = array(
		'name' => $txt['settings'],	
		'url' => $scripturl.'?action=admin;area=runtimesettings',	
	);
	if(empty($sa))
	{
		loadtemplate('Plugins');
		$context['sub_template'] = 'runtimesettings';
	}
	else
	{
		loadtemplate('Plugins');
		require_once($sourcedir . '/runtime/'. strtoupper(substr($sa,0,1)).substr($sa,1). '.php');
		
		if(function_exists('BSettings'))
			BSettings();
		
		loadtemplate('runtime/'. strtoupper(substr($sa,0,1)).substr($sa,1));
		$context['sub_template'] = 'rbsettings';
	}
}

function extract_hiddentext()
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc;

	$request = $smcFunc['db_query']('','
	SELECT b.id_plugin, ms.hiddentext, ms.id_member,ms.id_topic FROM {db_prefix}messages as ms
	LEFT JOIN {db_prefix}topics as t ON (ms.id_msg = t.id_first_msg)
	LEFT JOIN {db_prefix}boards as b ON (t.id_board = b.id_board)
	WHERE 1');
	if($smcFunc['db_num_rows']($request)>0)
	{
		$count=0; $hiddentext = array();
		while($row = $smcFunc['db_fetch_assoc']($request))
		{
			$h = unserialize($row['hiddentext']);
			if($h!== false)
			{
				$row['hiddentext'] = $h;
				$hiddentext[] = $row;
			}
		}
		$smcFunc['db_free_result']($request);
	}

	foreach($hiddentext as $s => $wi)
	{
		foreach($wi['hiddentext'] as $w)
		{
			$d = explode("=",$w);
			$what = $d[0]; $data = $d[1];
			
			if(is_numeric($data))
				$request = $smcFunc['db_query']('','
			INSERT INTO {db_prefix}boardtype_data 
				(id_plugin,
				id_topic,
				value_int,
				datatype,
				id_member,
				is_integer) 
				VALUES("' . $wi['id_plugin'] . '",
				"' .$wi['id_topic'] . '",
				"' .$data . '",
				"' .$what . '",
				"' .$wi['id_member'] . '",
				"1"
				)');
			else
				$request = $smcFunc['db_query']('','
			INSERT INTO {db_prefix}boardtype_data 
				(id_plugin,
				id_topic,
				value_str,
				datatype,
				id_member,
				is_integer) 
				VALUES("' . $wi['id_plugin'] . '",
				"' .$wi['id_topic'] . '",
				"' .$data . '",
				"' .$what . '",
				"' .$wi['id_member'] . '",
				"0"
				)');
			$count++;
		}
	}	
	echo 'Inserted ' . $count . ' variables';
	die();
}

function findtaginhtml($text, $tag, $content = '')
{
	if(!empty($content))
	{
		$tagpos = strpos($text, $tag);
		$start = strpos($text, $content,$tagpos);
		$startcontent = strpos($text, '"', $start);
		$endcontent = strpos($text, '"', $startcontent+1);
		$what = substr($text, $startcontent+1, $endcontent-$startcontent-1);
		return $what;
	}	
	return;
}

function updateboardtypevalue($id_plugin, $topic, $datatype, $value, $is_integer = false, $disregard_member = false)
{
	global $smcFunc,$context;
	
	if($disregard_member)
		$request = $smcFunc['db_query']('', '
			SELECT id_data, value_int, value_' . ($is_integer ? 'int' : 'str') . ', id_member, datatype FROM {db_prefix}boardtype_data 
			WHERE id_plugin = "' .$id_plugin . '"
			AND id_topic = ' . $topic . '
			AND datatype = "' . $datatype . '"
			LIMIT 1');
	else
		$request = $smcFunc['db_query']('', '
			SELECT id_data, value_int, value_' . ($is_integer ? 'int' : 'str') . ', id_member, datatype FROM {db_prefix}boardtype_data 
			WHERE id_plugin = "' .$id_plugin . '"
			AND id_topic = ' . $topic . '
			AND datatype = "' . $datatype . '"
			AND id_member = ' . $context['user']['id'] . ' LIMIT 1');
	
	if($smcFunc['db_num_rows']($request)>0)
	{
		$row= $smcFunc['db_fetch_assoc']($request);
		$smcFunc['db_free_result']($request);
		$request = $smcFunc['db_query']('', '
		UPDATE {db_prefix}boardtype_data SET value_' . ($is_integer ? 'int = {int:val}' : 'str = {string:vals}') . ' WHERE id_data = ' . $row['id_data'] . ' LIMIT 1',
			array(
			'val' => $value, 
			'vals' => $value, 
			)
		);
	}
	else
	{
		$request = $smcFunc['db_query']('', '
		INSERT INTO {db_prefix}boardtype_data (id_plugin, id_topic, id_member, datatype, value_' . ($is_integer ? 'int' : 'str') . ') VALUES("' . $id_plugin . '",' . $topic . ', ' . $context['user']['id'] . ',{string:data_type}, {string:value})',
			array(
				'data_type' => $datatype,
				'value' => $value,
			)
		);
	}
}

function getIMDBinfo($tt)
{
	$ta = file_get_contents('http://m.imdb.com/title/'.$tt.'/');
	$return = array();
	$return['imdb_title'] = tagfetch($ta,'<h1>','</h1>');
	$return['imdb_plot'] = tagfetch($ta,'<p itemprop="description">','</p>');
	$return['imdb_rating'] = tagfetch($ta,'<span class="inline-block text-left vertically-middle">','</small>');

	$return['imdb_rating'] = str_replace(array("/10"),array("/10&nbsp;-&nbsp;"),$return['imdb_rating']);

	$return['imdb_genre'] = tagfetch($ta,'<h3>Genres</h3>','</section>',false);
	$return['imdb_date'] = tagfetch($ta,'Release Date:</h3>','</span>');
	$return['imdb_contry'] = tagfetch($ta,'Country of Origin</h3>','</section>');
	return $return;
}	

function tagfetch($text,$tag, $endtag, $allspaces = true, $extract='')
{
	$startpos = strpos($text,$tag);
	$endpos = strpos($text,$endtag, $startpos);
	$portion = substr($text, $startpos + strlen($tag), ($endpos - $startpos));
	$trimmed = trim(strip_tags($portion));
	
	if($allspaces)
		$return = preg_replace('/^\s+|\n|\r|\s+$/m', '', $trimmed);
	else
		$return = preg_replace('/^\s+|\n|\s+$/m', '', $trimmed);
	return $return;
}

function checkboardtype($brd, $datatype)
{
	global $smcFunc;

	// get any boardtype data
	$request = $smcFunc['db_query']('', 'SELECT * FROM {db_prefix}boardtype_data as d 
	LEFT JOIN {db_prefix}topics as t ON (d.id_topic = t.id_topic)
	WHERE t.id_board = ' . $brd . '
	AND d.datatype = "' . $datatype.  '"
	LIMIT 1
	');
	if($smcFunc['db_num_rows']($request)>0)
	{
		$row = $smcFunc['db_fetch_assoc']($request);
		$smcFunc['db_free_result']($request);
		return $row;
	}
}	

function makeTextListBox($id, $tags)
{
	$tagstring = 'tbox' . $id;
	
	if(!empty($tags) && is_array($tags))
		foreach($tags as $t)
			$tagstring .= '.add(\'' . $t . '\')';

	echo '
	<script type="text/javascript" charset="utf-8">		
		window.addEvent(\'load\', function(){
			// With custom adding keys 
			var tbox' , $id , ' = new TextboxList(\'' , $id , '\', {bitsOptions:{editable:{addKeys: 188}}});
			' . $tagstring . ';
	});
	</script>';
}

function getMemberGroups()
{
	global $smcFunc;

	$return = array();
	// Get the basic group data.
	$request = $smcFunc['db_query']('', '
		SELECT id_group, group_name
		FROM {db_prefix}membergroups
		WHERE min_posts = -1
		ORDER BY group_name ASC');
	
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$return[$row['id_group']] = $row['group_name'];
	
	$smcFunc['db_free_result']($request);

	return $return;
}

function ManageMenu()
{
	global $txt, $settings, $modSettings, $context;

	$sa = !empty($_GET['sa']) ? $_GET['sa'] : '';
	
	$subActions = array(
		'edit' => 'ManageMenuEdit',
		'settings' => 'ManageMenuSettings',
	);
	isAllowedTo('admin_forum');
	loadtemplate('Plugins');
	
	if($sa == 'edit')
		ManageMenuEdit();
	elseif($sa == 'settings')
		ManageMenuSettings();
	else
		ManageMenuEdit();
}

function ManageMenuEdit()
{
	global $txt, $settings, $modSettings, $context;

	isAllowedTo('admin_forum');

	if(!empty($_POST['newid']) && !empty($_POST['newtitle']) && !empty($_POST['newurl']))
	{	
		checksession('post');
		$id = trim(pclean($_POST['newid']));
		$title = $_POST['newtitle'];
		$url = $_POST['newurl'];
		if(empty($modSettings['newmenu']))
			$modSettings['newmenu'] = array();
		
		$modSettings['newmenu'][$id] = array(
			'title' => $title,
			'href' => $url,
			'show' => true,
			'top' => true,
		);
		// add it to ordering too
		if(!empty($modSettings['menuorder']))
		{
			$modSettings['menuorder'][] = array(
				'id' => $id,
				'title' => $title,
				'children' => array(),
			);
			$change = array(
				'newmenu_values' => serialize($modSettings['newmenu']),
				'menuorder_values' => serialize($modSettings['menuorder']),
			);
		}
		else
			$change = array(
				'newmenu_values' => serialize($modSettings['newmenu']),
			);

		updateSettings($change,true);
		redirectexit('action=admin;area=menuitems');
	}
	if(isset($_POST['serialized']))
	{	
		checksession('post');
		$modSettings['menuorder'] = json_decode($_POST['serialized'], true);
		$change = array(
			'menuorder_values' => serialize($modSettings['menuorder']),
		);
		updateSettings($change,true);
		redirectexit('action=admin;area=menuitems');
	}
	if(isset($_GET['clearmenu']))
	{	
		checksession('GET');
		$modSettings['menuorder'] = '';
		$change = array(
			'menuorder_values' => serialize($modSettings['menuorder']),
		);
		updateSettings($change,true);
		redirectexit('action=admin;area=menuitems');
	}
	if(isset($_GET['clearmenubuttons']))
	{	
		checksession('GET');
		$modSettings['newmenu'] = array();
		$change = array(
			'newmenu_values' => serialize($modSettings['newmenu']),
		);
		updateSettings($change,true);
		redirectexit('action=admin;area=menuitems');
	}
	$context['sub_template'] = 'menu_edit';
}

function ManageMenuSettings()
{
	global $txt, $settings, $modSettings, $context;

	isAllowedTo('admin_forum');
	$context['sub_template'] = 'menu_settings';
}

?>