<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Music()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$txt['music'] = $modSettings['plugins']['music']['menu']['title'];
	$context['use_youtubepopup'] = true;
	// if fetchinga users blog posts: don't fetch all users
	if(!isset($_GET['u']))
		$context['get_blog_users'] = true;
	else
		$context['get_blog_archive'] = true;
	
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('music');
	$context['included_boards'] = implode(",",$b);
	initial_default('music');
	loadtemplate('boardtypes/Music');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'music';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('music',$t);
	// we got some data?
	if(!empty($context['hiddendata_array']))
	{
		foreach($context['hiddendata_array'] as $what => $data)
		{
			if($what=='clip')
			{
				$context['clip'] = $data;
			}
		}
	}
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'music';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('music',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'clip' => array(
			'title' => $txt['clip'],
			'value' => isset($context['hiddentext']['clip']) ? $context['hiddentext']['clip'] : '',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'music',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,6)=='music_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=music');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=music' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('music');
}

function template_music()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Music');
	$context['subthemeobject']->theme_main();
}

?>