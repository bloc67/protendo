<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Gallery()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir;

	$context['get_blog_users'] = true;
	$txt['article'] = $modSettings['plugins']['gallery']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('gallery');
	$context['included_boards'] = implode(",",$b);
	$latest = initial_default('gallery');
	$context['gallery_latest'][$latest] = attgallery_return($latest);

	// gallery stuff
	if(!empty($options['messageindex_altgallery']))
		$context['miniprofile_settings'][] = array('setting' => 'messageindex_altgallery', 'name' => $txt['messageindex_imagetop'], 'url' => 'action=gallery');
	else
		$context['miniprofile_settings'][] = array('setting' => 'messageindex_altgallery', 'name' => $txt['messageindex_imageleft'],'url' => 'action=gallery');

	loadtemplate('boardtypes/Gallery');
}

// single gallery function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'gallery';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('gallery',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['get_blog_users'] = true;
	$context['active_plugin'] = 'gallery';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('gallery',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'gallery',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,8)=='gallery_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=gallery');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=gallery' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('gallery');
}

function template_gallery()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Gallery');
	$context['subthemeobject']->theme_main();
}

?>