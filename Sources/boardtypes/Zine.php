<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Zine()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$txt['zine'] = $modSettings['plugins']['zine']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('zine');
	$context['included_boards'] = implode(",",$b);
	initial_default('zine',30);
	loadtemplate('boardtypes/Zine');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'zine';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('zine',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'zine';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('zine',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'zine',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,5)=='zine_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=zine');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=zine' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('zine');
}

function template_zine()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Zine');
	$context['subthemeobject']->theme_main();
}

?>