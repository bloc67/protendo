<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Bugtracker()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	// are we coming from Display quick-form?
	if(isset($_GET['updatedisplay']))
		updatefromDisplay();
	
	getBGdata();

	// get all bugs for stats
	$request = $smcFunc['db_query']('substring', '
			SELECT id_data, value_str, datatype 
			FROM {db_prefix}boardtype_data
			WHERE datatype = "bg_status"
			ORDER BY id_data DESC');
	if($smcFunc['db_num_rows']($request)>0)
	{
		$context['bg_stats'] = array(
			'status' => array(
				'0'=> 0,
				'1'=> 0,
				'2'=> 0,
				'3'=> 0,
				), 
			'total' => 0,
		);
		while($row = $smcFunc['db_fetch_assoc']($request))
		{
			$context['bg_stats']['total'] = $smcFunc['db_num_rows']($request);
			$context['bg_stats']['status'][$row['value_str']]++;
		}
		$smcFunc['db_free_result']($request);
	}
	$context['db_param'] = array(
			'0' => array(
				'datatype' => 'bg_status',
				'str' => 0,
			),
			'1' => array(
				'datatype' => 'bg_status',
				'str' => 1,
			),
			'2' => array(
				'datatype' => 'bg_status',
				'str' => 2,
			),
			'3' => array(
				'datatype' => 'bg_status',
				'str' => 3,
			),
		);

	
	$txt['bugtracker'] = $modSettings['plugins']['bugtracker']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('bugtracker');
	$context['included_boards'] = implode(",",$b);
	initial_default('bugtracker');
	loadtemplate('boardtypes/Bugtracker');
}

function updatefromDisplay()
{
	global $user_info,$sourcedir, $context, $modSettings, $txt;

	require_once($sourcedir. '/Plugins.php');
	// can we actually update it?
	$as = !empty($modSettings['bugtracker_group']) ? $modSettings['bugtracker_group'] : 9999;
	if(allowedTo('moderate_forum') || in_array($as, $user_info['group']))
	{
		checksession('post');
		$t = $_POST['boardtype_controls_topic'];
		foreach(array('strboardtypevalue_bg_category','strboardtypevalue_bg_status','strboardtypevalue_bg_version','strboardtypevalue_bg_solvedversion') as $what)
		{
			if(isset($_POST[$what]))
				updateboardtypevalue('bugtracker', $t, substr($what,18), $_POST[$what],false,true);			
		}
	}
	redirectexit('topic='.$t);
}

function getBGdata()
{
	global $sourcedir, $context, $modSettings, $txt;

	$modSettings['bg_classes'] = array(
		'0' => 'mess_prog0',
		'1' => 'mess_prog20',
		'2' => 'mess_prog100',
		'3' => 'mess_solved',
	);
	$modSettings['bg_texts'] = array(
		'0' => 'Open',
		'1' => 'feedback',
		'2' => 'solved',
		'3' => 'closed',
	);
	$modSettings['bg_versions'] = unserialize($modSettings['bugtracker_versions']);
	$modSettings['bg_categories'] = unserialize($modSettings['bugtracker_categories']);
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	getBGdata();
	$context['active_plugin'] = 'bugtracker';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('bugtracker',$t);

	if(!empty($modSettings['bugtracker_categories']))
		$categories = unserialize($modSettings['bugtracker_categories']);
	else
		$categories = array();

	if(!empty($modSettings['bugtracker_versions']))
		$versions = unserialize($modSettings['bugtracker_versions']);
	else
		$versions = array();

	// set up the extra controls
	$context['boardtype_controls_topic'] = $t;
	$as = !empty($modSettings['bugtracker_group']) ? $modSettings['bugtracker_group'] : 9999;
	if(allowedTo('moderate_forum') || in_array($as, $user_info['group']))
	{
		$context['boardtypes_controls'] = array(
			'bg_category' => array(
				'title' => $txt['category'],
				'type' => 'list',
				'value' => isset($context['frontblog'][0]['hiddentext']['bg_category']) ? $context['frontblog'][0]['hiddentext']['bg_category'] : '',
				'data_type' => 'str',
				'type_data' => $categories,
			),
			'bg_status' => array(
				'title' => $txt['status'],
				'type' => 'list',
				'value' => isset($context['frontblog'][0]['hiddentext']['bg_status']) ? $context['frontblog'][0]['hiddentext']['bg_status'] : '',
				'data_type' => 'str',
				'type_data' => array(
					0 => 'Open',
					1 => 'Acknowledged',
					2 => 'Solved',
					3 => 'Closed',
				),
			),
			'bg_version' => array(
				'title' => $txt['version'],
				'type' => 'list',
				'value' => isset($context['frontblog'][0]['hiddentext']['bg_version']) ? $context['frontblog'][0]['hiddentext']['bg_version'] : '',
				'data_type' => 'str',
				'type_data' => $versions,
			),
			'bg_solvedversion' => array(
				'title' => $txt['version'],
				'type' => 'list',
				'value' => isset($context['frontblog'][0]['hiddentext']['bg_solvedversion']) ? $context['frontblog'][0]['hiddentext']['bg_solvedversion'] : '',
				'data_type' => 'str',
				'type_data' => $versions,
			),
		);
		loadtemplate('Plugins');
	}
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	getBGdata();

	$context['active_plugin'] = 'bugtracker';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('bugtracker',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// we don't want to change the icon by a user- since its tied to the status :)
	$context['no_messageicons'] = true;
	
	if(!empty($modSettings['bugtracker_categories']))
		$categories = unserialize($modSettings['bugtracker_categories']);
	else
		$categories = array();

	if(!empty($modSettings['bugtracker_versions']))
		$versions = unserialize($modSettings['bugtracker_versions']);
	else
		$versions = array();

	// set up the extra controls
	$as = !empty($modSettings['bugtracker_group']) ? $modSettings['bugtracker_group'] : 9999;

	// are we assigned, or a moderator?
	if(allowedTo('moderate_forum') || in_array($as, $user_info['group']))
		$context['boardtypes_controls'] = array(
			'bg_category' => array(
				'title' => $txt['category'],
				'type' => 'list',
				'value' => isset($context['hiddentext']['bg_category']) ? $context['hiddentext']['bg_category'] : '',
				'data_type' => 'str',
				'type_data' => $categories,
			),
			'bg_status' => array(
				'title' => $txt['status'],
				'type' => 'list',
				'value' => isset($context['hiddentext']['bg_status']) ? $context['hiddentext']['bg_status'] : '',
				'data_type' => 'str',
				'type_data' => array(
					0 => 'Open',
					1 => 'Acknowledged',
					2 => 'Solved',
					3 => 'Closed',
				),
			),
			'bg_version' => array(
				'title' => $txt['version'],
				'type' => 'list',
				'value' => isset($context['hiddentext']['bg_version']) ? $context['hiddentext']['bg_version'] : '',
				'data_type' => 'str',
				'type_data' => $versions,
			),
			'bg_solvedversion' => array(
				'title' => $txt['version'],
				'type' => 'list',
				'value' => isset($context['hiddentext']['bg_solvedversion']) ? $context['hiddentext']['bg_solvedversion'] : '',
				'data_type' => 'str',
				'type_data' => $versions,
			),
			'plugin' => array(
				'text' => '',
				'type' => 'hide',
				'value' => 'bugtracker',
			),
		);
	else
	{
		// only allow it on first creation..moderators can change later
		if(!isset($context['hiddentext']['bg_category']))
			// set up the extra controls
			$context['boardtypes_controls'] = array(
				'bg_category' => array(
					'title' => $txt['category'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_category']) ? $context['hiddentext']['bg_category'] : '',
					'data_type' => 'str',
					'type_data' => $categories,
				),
				'bg_version' => array(
					'title' => $txt['version'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_version']) ? $context['hiddentext']['bg_version'] : '',
					'data_type' => 'str',
					'type_data' => $versions,
				),
				'bg_solvedversion' => array(
					'title' => $txt['version'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_solvedversion']) ? $context['hiddentext']['bg_solvedversion'] : '',
					'data_type' => 'str',
					'type_data' => $versions,
					'hidden' => true,
				),
				'plugin' => array(
					'text' => '',
					'type' => 'hide',
					'value' => 'bugtracker',
				),
			);
		else
			$context['boardtypes_controls'] = array(
				'bg_category' => array(
					'title' => $txt['category'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_category']) ? $context['hiddentext']['bg_category'] : '',
					'data_type' => 'str',
					'hidden' => true,
					'type_data' => $categories,
				),
				'bg_version' => array(
					'title' => $txt['version'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_version']) ? $context['hiddentext']['bg_version'] : '',
					'data_type' => 'str',
					'type_data' => $versions,
					'hidden' => true,
				),
				'bg_solvedversion' => array(
					'title' => $txt['version'],
					'type' => 'list',
					'value' => isset($context['hiddentext']['bg_solvedversion']) ? $context['hiddentext']['bg_solvedversion'] : '',
					'data_type' => 'str',
					'type_data' => $versions,
					'hidden' => true,
				),
				'plugin' => array(
					'text' => '',
					'type' => 'hide',
					'value' => 'bugtracker',
				),
			);
	}
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		$val = explode(",", $_POST['bugtracker_categories']);
		$ver = explode(",", $_POST['bugtracker_versions']);
		$grp = $_POST['bugtracker_group'];

		$change = array(
			'bugtracker_categories' => serialize($val),
			'bugtracker_versions' => serialize($ver),
			'bugtracker_group' => $grp,
		);
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(!in_array($what,array('bugtracker_categories','bugtracker_versions','bugtracker_group')) && substr($what,0,11)=='bugtracker_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=bugtracker');
	}
	
	if(!empty($modSettings['bugtracker_categories']))
		$cats = unserialize($modSettings['bugtracker_categories']);
	else
		$cats = array();

	if(!empty($modSettings['bugtracker_versions']))
		$vers = unserialize($modSettings['bugtracker_versions']);
	else
		$vers = array();

	// get the membergroups
	$groups = getMemberGroups();

	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=bugtracker' ,
		'title' => $txt['settings'],
		'values' => array(	
			array(
				'id' => 'bugtracker_categories',
				'label' => $txt['categories'],
				'description' => $txt['categories_list'],
				'value' => $cats,
				'textboxlist' => true,
				'type' => 'textarea',
			),
			array(
				'id' => 'bugtracker_versions',
				'label' => $txt['version'],
				'description' => $txt['version_list'],
				'value' => $vers,
				'textboxlist' => true,
				'type' => 'textarea',
			),
			array(
				'id' => 'bugtracker_group',
				'label' => $txt['bg_group'],
				'description' => $txt['bg_group_list'],
				'value' => isset($modSettings['bugtracker_group']) ? $modSettings['bugtracker_group'] : '',
				'type' => 'list',
				'options' => $groups,
			),
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('bugtracker');
}



function template_bugtracker()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Bugtracker');
	$context['subthemeobject']->theme_main();
}

?>