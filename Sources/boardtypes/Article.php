<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Article()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$context['get_blog_users'] = true;
	$context['get_blog_mostread'] = true;
	$txt['article'] = $modSettings['plugins']['article']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('article');
	$context['included_boards'] = implode(",",$b);
	initial_default('article');
	loadtemplate('boardtypes/Article');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'article';
	$context['bodytags'] = 'itemscope itemtype="http://schema.org/Article"';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('article',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'article';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('article',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'lead' => array(
			'title' => $txt['lead'],
			'type' => 'textarea',
			'value' => isset($context['hiddentext']['lead']) ? $context['hiddentext']['lead'] : '',
		),
		'summary' => array(
			'title' => $txt['summary'],
			'type' => 'textarea',
			'value' => isset($context['hiddentext']['summary']) ? $context['hiddentext']['summary'] : '',
		),
		'sources' => array(
			'title' => $txt['sources'],
			'type' => 'text',
			'value' => isset($context['hiddentext']['sources']) ? $context['hiddentext']['sources'] : '',
		),
		'help' => array(
			'text' => $txt['articlehelp'],
			'type' => 'help',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'article',
		),
	);
}


// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,8)=='article_')
			{
				$change[$what] = $val;
			}
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=article');
	}
	$existing = getFreshSettings(array('article_facebook','article_twitter','article_google','article_pinterest'),true);
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=article' ,
		'title' => $txt['settings'],
		'values' => array(	
			array(
				'id' => 'article_facebook',
				'label' => $txt['share_facebook'],
				'description' => $txt['share_facebook_descr'],
				'type' => 'checkbox',
				'value' => !empty($existing['article_facebook']) ? 1 : 0,
			),
			array(
				'id' => 'article_twitter',
				'label' => $txt['share_twitter'],
				'type' => 'checkbox',
				'description' => $txt['share_twitter_descr'],
				'value' => !empty($existing['article_twitter']) ? 1 : 0,
			),
			array(
				'id' => 'article_google',
				'label' => $txt['share_google'],
				'type' => 'checkbox',
				'description' => $txt['share_google_descr'],
				'value' => !empty($existing['article_google']) ? 1 : 0,
			),
			array(
				'id' => 'article_pinterest',
				'label' => $txt['share_pinterest'],
				'type' => 'checkbox',
				'description' => $txt['share_pinterest_descr'],
				'value' => !empty($existing['article_pinterest']) ? 1 : 0,
			),
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('article');
}

function template_article()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Article');
	$context['subthemeobject']->theme_main();
}

?>