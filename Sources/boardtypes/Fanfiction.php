<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Fanfiction()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$txt['fanfiction'] = $modSettings['plugins']['fanfiction']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('fanfiction');
	$context['included_boards'] = implode(",",$b);
	initial_default('fanfiction');
	loadtemplate('boardtypes/Fanfiction');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'fanfiction';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('fanfiction',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'fanfiction';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('fanfiction',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'fanfiction',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');

		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(!in_array($what,array('bugtracker_categories','bugtracker_versions','bugtracker_group')) && substr($what,0,11)=='fanfiction_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=fanfiction');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=fanfiction' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('fanfiction');
}

function template_fanfiction()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Fanfiction');
	$context['subthemeobject']->theme_main();
}

?>