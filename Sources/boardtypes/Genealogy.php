<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Genealogy()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$txt['genealogy'] = $modSettings['plugins']['genealogy']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('genealogy');
	$context['included_boards'] = implode(",",$b);
	initial_default('genealogy');
	loadtemplate('boardtypes/Genealogy');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'genealogy';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('genealogy',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'genealogy';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('genealogy',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// round up all males and females for mother,father,children
	$females = array(); $males = array();

	$request = $smcFunc['db_query']('substring', '
			SELECT d.id_data, d.value_str, d.datatype, m.subject, t.id_topic 
			FROM {db_prefix}boardtype_data as d
			LEFT JOIN {db_prefix}topics as t ON (t.id_topic = d.id_topic)
			LEFT JOIN {db_prefix}messages as m ON (t.id_first_msg = m.id_msg)
			WHERE datatype = "ge_status"
			ORDER BY id_data DESC');
	
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($row = $smcFunc['db_fetch_assoc']($request))
		{
			if(empty($row['value_str']))
				$females['id_topic'] = $row['subject']; 
			else
				$males['id_topic'] = $row['subject']; 
		}
		$smcFunc['db_free_result']($request);
	}
		
	$context['boardtypes_controls'] = array(
		'ge_gender' => array(
			'title' => $txt['status'],
			'type' => 'list',
			'value' => isset($context['hiddentext']['ge_gender']) ? $context['hiddentext']['ge_gender'] : '0',
			'data_type' => 'str',
			'type_data' => array(
				0 => 'Kvinne',
				1 => 'Mann',
			),
		),
		'ge_mother' => array(
			'title' => 'Biologisk mor',
			'type' => 'list',
			'value' => isset($context['hiddentext']['ge_mother']) ? $context['hiddentext']['ge_mother'] : '',
			'data_type' => 'str',
			'type_data' => $females,
		),
		'ge_father' => array(
			'title' => 'Biologisk far',
			'type' => 'list',
			'value' => isset($context['hiddentext']['ge_father']) ? $context['hiddentext']['ge_father'] : '',
			'data_type' => 'str',
			'type_data' => $males,
		),
		'ge_birthdate' => array(
			'title' => 'F&oslash;dselsdato',
			'type' => 'number',
			'value' => isset($context['hiddentext']['ge_birthdate']) ? $context['hiddentext']['ge_birthdate'] : '',
			'data_type' => 'str',
			'type_data' => ''
		),
		'ge_birthplace' => array(
			'title' => 'F&oslash;dselsted',
			'type' => 'text',
			'value' => isset($context['hiddentext']['ge_birthplace']) ? $context['hiddentext']['ge_birthplace'] : '',
			'data_type' => 'str',
			'type_data' => $males,
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'genealogy',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,10)=='genealogy_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=genealogy');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=genealogy' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('genealogy');
}

function template_genealogy()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Genealogy');
	$context['subthemeobject']->theme_main();
}

?>