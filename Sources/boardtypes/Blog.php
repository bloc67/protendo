<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Blog()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$txt['blog'] = $modSettings['plugins']['blog']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('blog');
	$context['included_boards'] = implode(",",$b);
	$context['get_blog_mostread'] = true;
	
	if(!empty($_GET['author']) && is_numeric($_GET['author']))
		$context['get_blog_history'] = 1;
	
	initial_default('blog');
	loadtemplate('boardtypes/Blog');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'blog';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('blog',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'blog';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('blog',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'blog',
		),
		'timeday' => array(
			'title' => $txt['day'],
			'type' => 'list',
			'value' => isset($context['hiddentext']['timeday']) ? $context['hiddentext']['timeday'] : '',
			'opts-range' => array(1,31),
		),
		'timemonth' => array(
			'title' => $txt['month'],
			'type' => 'list',
			'value' => isset($context['hiddentext']['timemonth']) ? $context['hiddentext']['timemonth'] : '',
			'type_data' => array(
				1 => $txt['months_show'][1],
				2 => $txt['months_show'][2],
				3 => $txt['months_show'][3],
				4 => $txt['months_show'][4],
				5 => $txt['months_show'][5],
				6 => $txt['months_show'][6],
				7 => $txt['months_show'][7],
				8 => $txt['months_show'][8],
				9 => $txt['months_show'][9],
				10 => $txt['months_show'][10],
				11 => $txt['months_show'][11],
				12 => $txt['months_show'][12],
			),
		),
		'timeyear' => array(
			'title' => $txt['year'],
			'type' => 'list',
			'value' => isset($context['hiddentext']['timeyear']) ? $context['hiddentext']['timeyear'] : '',
			'opts-range' => array(2000, date("Y", time())),
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir, $smcFunc;

	require_once($sourcedir. '/Plugins.php');
	// check if we got imdb info in cache
	if(!empty($hiddentext['str']['timeday']) && !empty($hiddentext['str']['timemonth']) && !empty($hiddentext['str']['timeyear']))
	{
		$newtime = mktime(12,30,0,$hiddentext['str']['timemonth'],$hiddentext['str']['timeday'],$hiddentext['str']['timeyear']);
		$request = $smcFunc['db_query']('','
		UPDATE {db_prefix}messages SET poster_time = ' . $newtime . '	WHERE id_msg =' . $msg);
	}
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,5)=='blog_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=blog');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=blog' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('blog');
}

function template_blog()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Blog');
	$context['subthemeobject']->theme_main();
}

?>