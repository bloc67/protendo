<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function MovieReview()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$txt['moviereview'] = $modSettings['plugins']['moviereview']['menu']['title'];
	$context['use_youtubepopup'] = true;
	// if fetchinga users blog posts: don't fetch all users
	if(!isset($_GET['u']))
		$context['get_blog_users'] = true;
	else
		$context['get_blog_archive'] = true;
	
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('moviereview');
	$context['included_boards'] = implode(",",$b);
	initial_default('moviereview');
	loadtemplate('boardtypes/MovieReview');

	// fetch the highest rated ones
	$request = $smcFunc['db_query']('', '
		SELECT dt.value_int, dt.id_topic, mem.real_name, mem.id_member, ms.subject  

		FROM {db_prefix}boardtype_data AS dt 
		LEFT JOIN {db_prefix}topics AS t ON (dt.id_topic = t.id_topic)
		LEFT JOIN {db_prefix}messages AS ms ON (ms.id_msg = t.id_first_msg)
		LEFT JOIN {db_prefix}members AS mem ON (ms.id_member = mem.id_member)
		WHERE dt.id_plugin = "moviereview"
			AND dt.datatype = "myscore"
			AND t.id_board != ' . $modSettings['recycle_board'] . '
		ORDER BY dt.value_int DESC
		LIMIT 16'
	);
	$context['high_rated'] = array(); 
	if($smcFunc['db_num_rows']($request)>0)
	{
		while($row= $smcFunc['db_fetch_assoc']($request))
		{
			$context['high_rated'][] = $row;		
		}
		$smcFunc['db_free_result']($request);
	}
}


// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $topic, $smcFunc;
	global $modSettings, $txt;

	$context['active_plugin'] = 'moviereview';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('moviereview',$t);

	// get tagged others
	if(!empty($context['frontblog'][0]['hiddentext']['tags']))
	{
		$tags = explode(",",$context['frontblog'][0]['hiddentext']['tags']);
		$bstring = array();
		foreach($tags as $t)
			$bstring[] = 'FIND_IN_SET("' . $t . '", b.value_str)';

		$request = $smcFunc['db_query']('', '
			SELECT b.id_topic,m.subject,m.id_msg, b.value_str 
			FROM {db_prefix}boardtype_data AS b 
				LEFT JOIN {db_prefix}topics AS t ON (t.id_topic = b.id_topic)
				LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
			WHERE b.id_plugin = "moviereview"
				AND b.datatype = "tags"
				AND (' . implode(" OR ", $bstring) .')
			ORDER BY b.id_topic DESC
			LIMIT 40');
		
		if($smcFunc['db_num_rows']($request)>0)
		{
			$ids = array();
			$context['frontblog'][0]['hiddentext']['tags_data'] = array();
			while($row = $smcFunc['db_fetch_assoc']($request))
			{
				$ids[] = $row['id_msg'];
				$context['frontblog'][0]['hiddentext']['tags_data'][$row['id_topic']] = $row;
			}
			$smcFunc['db_free_result']($request);
			attgallery($ids);
		}
	}
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'moviereview';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('moviereview',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'myscore' => array(
			'title' => $txt['myscore'],
			'value' => isset($context['hiddentext']['myscore']) ? $context['hiddentext']['myscore'] : 0,
			'data_type' => 'int',
		),
		'seenbefore' => array(
			'title' => $txt['seenbefore'],
			'value' => isset($context['hiddentext']['seenbefore']) ? $context['hiddentext']['seenbefore'] : 0,
			'data_type' => 'int',
		),
		'imdb' => array(
			'title' => $txt['imdb'],
			'value' => isset($context['hiddentext']['imdb']) ? $context['hiddentext']['imdb'] : '',
			'data_type' => 'str',
		),
		'tags' => array(
			'title' => 'Tags',
			'value' => isset($context['hiddentext']['tags']) ? $context['hiddentext']['tags'] : '',
			'data_type' => 'str',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'moviereview',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');

	// check if we got imdb info in cache
	if(empty($hiddentext['str']['imdb']))
		return;

	updateboardtypevalue('moviereview', $topc, 'imdb', $hiddentext['str']['imdb'], false);
	$info = getIMDBinfo($hiddentext['str']['imdb']);
	foreach($info as $p => $val)
		updateboardtypevalue('moviereview', $topc, $p, $val, false);

}


function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,12)=='moviereview_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=moviereview');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=moviereview' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('moviereview');
}

function template_moviereview()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Moviereview');
	$context['subthemeobject']->theme_main();
}

?>