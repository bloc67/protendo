<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Photo()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir;

	$context['get_blog_users'] = true;
	$txt['photo'] = $modSettings['plugins']['gallery']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('photo');
	$context['included_boards'] = implode(",",$b);
	$latest = initial_default('photo');
	$context['photo_latest'][$latest] = attgallery_return($latest);

	loadtemplate('boardtypes/Photo');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'photo';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('photo',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['get_blog_users'] = true;
	$context['active_plugin'] = 'photo';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('photo',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'photo',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,5)=='photo_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=photo');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=photo' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('photo');
}

function template_photo()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Photo');
	$context['subthemeobject']->theme_main();
}

?>