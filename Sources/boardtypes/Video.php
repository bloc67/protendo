<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Video()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$txt['video'] = $modSettings['plugins']['video']['menu']['title'];
	$context['use_youtubepopup'] = true;
	// if fetchinga users blog posts: don't fetch all users
	if(!isset($_GET['u']))
		$context['get_blog_users'] = true;
	else
		$context['get_blog_archive'] = true;
	
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('video');
	$context['included_boards'] = implode(",",$b);
	initial_default('video');
	loadtemplate('boardtypes/Video');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'video';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('video',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'video';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('video',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'trailer' => array(
			'title' => $txt['trailer'],
			'value' => isset($context['hiddentext']['trailer']) ? $context['hiddentext']['trailer'] : '',
		),
		'vimeo' => array(
			'title' => 'Vimeo ID',
			'value' => isset($context['hiddentext']['vimeo']) ? $context['hiddentext']['vimeo'] : '',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'video',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,6)=='video_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=video');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=video' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('video');
}

function template_video()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Video');
	$context['subthemeobject']->theme_main();
}

?>