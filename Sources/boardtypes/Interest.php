<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Interest()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$context['blog_fetch_replies'] = true;
	$context['get_blog_users'] = true;
	$txt['interest'] = $modSettings['plugins']['interest']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('interest');
	$context['included_boards'] = implode(",",$b);
	$latest = initial_default('interest');

	loadtemplate('boardtypes/Interest');
}

function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'interest';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('interest',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'interest';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('interest',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'interest',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,9)=='interest_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=interest');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=interest' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('interest');
}

function template_interest()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Interest');
	$context['subthemeobject']->theme_main();
}

?>