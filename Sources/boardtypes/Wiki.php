<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Wiki()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$context['get_blog_users'] = true;
	$txt['wiki'] = $modSettings['plugins']['wiki']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('wiki');
	$context['included_boards'] = implode(",",$b);
	initial_default('wiki');
	loadtemplate('boardtypes/Wiki');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'wiki';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('wiki',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'wiki';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('wiki',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'lead' => array(
			'title' => $txt['lead'],
			'type' => 'textarea',
			'value' => isset($context['hiddentext']['lead']) ? $context['hiddentext']['lead'] : '',
		),
		'summary' => array(
			'title' => $txt['summary'],
			'type' => 'textarea',
			'value' => isset($context['hiddentext']['summary']) ? $context['hiddentext']['summary'] : '',
		),
		'sources' => array(
			'title' => $txt['sources'],
			'type' => 'text',
			'value' => isset($context['hiddentext']['sources']) ? $context['hiddentext']['sources'] : '',
		),
		'help' => array(
			'text' => $txt['articlehelp'],
			'type' => 'help',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'wiki',
		),
	);
}


// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,5)=='wiki_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=wiki');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=wiki' ,
		'title' => $txt['settings'],
		'values' => array(	
			array(
				'id' => 'wiki_facebook',
				'label' => $txt['share_facebook'],
				'description' => $txt['share_facebook_descr'],
				'type' => 'checkbox',
				'value' => !empty($modSettings['wiki_facebook']) ? 1 : 0,
			),
			array(
				'id' => 'wiki_twitter',
				'label' => $txt['share_twitter'],
				'type' => 'checkbox',
				'description' => $txt['share_twitter_descr'],
				'value' => !empty($modSettings['wiki_twitter']) ? 1 : 0,
			),
			array(
				'id' => 'wiki_google',
				'label' => $txt['share_google'],
				'type' => 'checkbox',
				'description' => $txt['share_google_descr'],
				'value' => !empty($modSettings['wiki_google']) ? 1 : 0,
			),
			array(
				'id' => 'wiki_pinterest',
				'label' => $txt['share_pinterest'],
				'type' => 'checkbox',
				'description' => $txt['share_pinterest_descr'],
				'value' => !empty($modSettings['wiki_pinterest']) ? 1 : 0,
			),
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('wiki');
}

function template_wiki()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Wiki');
	$context['subthemeobject']->theme_main();
}

?>