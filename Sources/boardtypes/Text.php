<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Text()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$context['get_blog_users'] = true;
	$txt['text'] = $modSettings['plugins']['text']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('text');
	$context['included_boards'] = implode(",",$b);
	$latest = initial_default('text');
	$context['gallery_latest'][$latest] = attgallery_return($latest);

	loadtemplate('boardtypes/Text');
}

// single support function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'text';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('text',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['get_blog_users'] = true;
	$context['active_plugin'] = 'text';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('text',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');

	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'text',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,5)=='text_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=text');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=text' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('text');
}

function template_text()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Text');
	$context['subthemeobject']->theme_main();
}

?>