<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function Activity($frontpage = false)
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info, $settings;

	$txt['activity'] = $modSettings['plugins']['activity']['menu']['title'];
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('activity');
	$context['included_boards'] = implode(",",$b);
	initial_default('activity');
	loadtemplate('boardtypes/Activity');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'activity';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('activity',$t);
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context, $modSettings, $txt;

	$context['active_plugin'] = 'activity';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('activity',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'activity_date' => array(
			'title' => $txt['activity_date'],
			'type' => 'date',
			'value' => isset($context['hiddentext']['activity_date']) ? $context['hiddentext']['activity_date'] : '',
		),
		'activity_distance' => array(
			'title' => $txt['activity_distance'],
			'type' => 'distance',
			'value' => isset($context['hiddentext']['activity_distance']) ? $context['hiddentext']['activity_distance'] : '',
		),
		'activity_duration' => array(
			'title' => $txt['activity_duration'],
			'type' => 'duration',
			'value' => isset($context['hiddentext']['activity_duration']) ? $context['hiddentext']['activity_duration'] : '',
		),
		'activity_duration' => array(
			'title' => $txt['activity_duration'],
			'type' => 'duration',
			'value' => isset($context['hiddentext']['activity_duration']) ? $context['hiddentext']['activity_duration'] : '',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'activity',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	return;	
}

function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');

		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(!in_array($what,array('bugtracker_categories','bugtracker_versions','bugtracker_group')) && substr($what,0,9)=='activity_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=activity');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=activity' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('activity');
}



function template_activity()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('Activity');
	$context['subthemeobject']->theme_main();
}

?>