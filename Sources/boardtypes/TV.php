<?php

/**
 * @name			Protendo
 * @copyright		protendo.org
 * @version 1.0.0
 */

if (!defined('PROTENDO'))
	{ echo ERROR1.'You are not allowed to run this file directly. Please use the correct path.'.ERROR2; die(); }

function TV()
{
	global $scripturl, $modSettings, $boarddir, $smcFunc, $txt, $db_character_set, $context, $sourcedir, $user_info;

	$txt['tv'] = $modSettings['plugins']['tv']['menu']['title'];
	$context['use_youtubepopup'] = true;
	
	require_once($sourcedir. '/Plugins.php');
	$b = getboards('tv');
	$context['included_boards'] = implode(",",$b);
	initial_default('tv');
	loadtemplate('boardtypes/TV');
}

// single blog function
function pre_Display($t)
{
	global $sourcedir, $context, $topic, $smcFunc;

	$context['active_plugin'] = 'tv';
	require_once($sourcedir. '/Plugins.php');
	do_default_Display('tv',$t);
	$context['tvseries'] = getEpisodes($context['frontblog'][0]['hiddentext']['series'],'');
}

// messageindex
function pre_MessageIndex($t)
{
	global $sourcedir, $context;

	$context['active_plugin'] = 'tv';
	require_once($sourcedir. '/Plugins.php');
	do_default_MessageIndex('tv',$t);
}

// Post
function pre_Post($brd, $topc)
{
	global $context, $settings, $scripturl, $txt, $db_prefix, $user_info;
	global $modSettings, $smcFunc, $sourcedir, $board;

	require_once($sourcedir. '/Plugins.php');
	// check if other topics in the board specified has
	if(!empty($_GET['board']))
		$brd = $_GET['board'];
	
	// if not, check the GET series then
	if(empty($context['hiddentext']['series']) && !empty($_GET['series']) && is_numeric($_GET['series']))
		$context['hiddentext']['series'] = $_GET['series'];
	
	// get the seriesinfo
	if(!empty($context['hiddentext']['series']))
	{
		if(($stream = cache_get_data('tv'. $context['hiddentext']['series'], 86400)) == null)
		{
			$apikey = '3D91EF97C31D58A5';
			$output = file_get_contents('http://thetvdb.com/api/' . $apikey . '/series/' . $context['hiddentext']['series'] . '/all/');
			$stream = xml2array($output);
			cache_put_data('tv'. $context['hiddentext']['series'], $stream, 86400);
		}
		$seriesinfo = array();
		foreach($stream['Data']['Episode'] as $epi => $data)
		{
			$seriesinfo[$epi] = array(
				'id' => $epi,	
				'title' => $data['EpisodeName'],	
				'fulltitle' => '<b>Episode ' .$data['Combined_episodenumber'] . '</b> sesong ' .$data['Combined_season'] . '-  &quot;' . $data['EpisodeName']. '&quot;',
				'show' => '<p>' . (empty($data['Overview']) ? '' : $data['Overview']). '</p>',
				'episode' => $data['Combined_episodenumber'],	
				'season' => $data['Combined_season'],	
				'overview' => empty($data['Overview']) ? '' : $data['Overview'],	
				'image' => empty($data['filename']) ? $settings['images_url'].'/noimage.png' : 'http://thetvdb.com/banners/_cache/'. $data['filename'],
				);
		}
		$context['subject'] = $stream['Data']['Series']['SeriesName'];
	}
	else
		$seriesinfo = array();

	if(isset($context['hiddentext']['episodes']))
	{	
		$eps = unserialize($context['hiddentext']['episodes']);
		$epi = $context['hiddentext']['episodes'];
		unset($context['hiddentext']['episodes']);
	}
	
	// set up the extra controls
	$context['boardtypes_controls'] = array(
		'savedata' => array(
			'type' => 'hidden',
			'value' => $epi,
		),
		'episodes' => array(
			'type' => 'sradio',
			'title' => 'Episode',
			'saved' => isset($eps) ? $eps : '',
			'data_type' => 'str',
			'type_data' => $seriesinfo,
		),
		'series' => array(
			'title' => 'Serie',
			'value' => isset($context['hiddentext']['series']) ? $context['hiddentext']['series'] : '',
			'data_type' => 'str',
		),
		'plugin' => array(
			'text' => '',
			'type' => 'hide',
			'value' => 'tv',
		),
	);
}

// Post2
function pre_Post2($brd, $topc, $msg, $hiddentext)
{
	global $sourcedir;

	require_once($sourcedir. '/Plugins.php');
	$eps = array();
	foreach($_POST as $what => $value)
	{
		if(substr($what,0,5) == 'tvepi')
		{
			$episode = substr($what,5);
			$eps[$episode] = array(
				'seen' => '1',	
			);
			if(!empty($_POST['tvrate'.$episode]))
				$eps[$episode]['rate'] = $_POST['tvrate'.$episode];
			
		}
	}
	updateboardtypevalue('tv', $topc, 'episodes', serialize($eps), false);
	//function updateboardtypevalue($id_plugin, $topic, $datatype, $value, $is_integer = false)

	$info = getSeriesInfo($hiddentext['str']['series'],'');
	foreach($info as $p => $val)
		updateboardtypevalue('tv', $topc, $p, $val, false);
}

function getSeriesInfo($series, $ep)
{
	// cache the series
	if(($stream = cache_get_data('tv'. $series, 86400)) == null)
	{
		$apikey = '3D91EF97C31D58A5';
		$output = file_get_contents('http://thetvdb.com/api/' . $apikey . '/series/' . $series . '/all/');
		$stream = xml2array($output);
		cache_put_data('tv'. $series, $stream, 86400);
	}
	// ok, find the series info	
	$return = array();
	$return['tvseries_title'] = $stream['Data']['Series']['SeriesName'];
	$return['tvseries_overview'] = $stream['Data']['Series']['Overview'];
	$return['tvseries_actors'] = $stream['Data']['Series']['Actors'];
	$return['tvseries_genre'] = $stream['Data']['Series']['Genre'];

	return $return;

}
function getEpisodes($series)
{
	// cache the series
	if(($stream = cache_get_data('tv'. $series, 86400)) == null)
	{
		$apikey = '3D91EF97C31D58A5';
		$output = file_get_contents('http://thetvdb.com/api/' . $apikey . '/series/' . $series . '/all/');
		$stream = xml2array($output);
		cache_put_data('tv'. $series, $stream, 86400);
	}
	return $stream;
}
function BSettings()
{
	global $context, $scripturl, $txt, $modSettings, $settings;

	$context['use_textboxlist'] = 1;
	
	if(!empty($_POST['boardtypesettings']))
	{
		checksession('post');
		
		// get theme-specific
		foreach($_POST as $what => $val)
		{
			if(substr($what,0,3)=='tv_')
				$change[$what] = $val;
		}	
		updateSettings($change);
		redirectexit('action=admin;area=boardtypesettings;sa=tv');
	}
	
	$context['plugsettings'] = array(
		'href' => $scripturl.'?action=admin;area=boardtypesettings;sa=tv' ,
		'title' => $txt['settings'],
		'values' => array(	
		),
	);
	// add theme specific ones as well
	$context['themeobject']->theme_extra_settings('tv');
}

function template_tv()
{
	global $context, $settings, $options, $txt, $scripturl;

	createThemeObject('TV');
	$context['subthemeobject']->theme_main();
}

?>